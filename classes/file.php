<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/varia.php");
include_once(SERVER_ROOT."/../classes/config.php");

/** File system methods
 * 
 * Most file management operations are specified as a relative path inside PhPeace installation directory.
 * When necessary, and additional boolean parameter (is_abs) indicates the the file path is absolute
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class FileManager
{

	/**
	 * The absolute path to current PhPeace installation
	 * @var string
	 */
	public $path;
	
	/**
	 * The file where cookies are stored for CURL sessions
	 * @var string
	 */
	public $cookie_jar;
	
	/**
	 * @var boolean	Whether PHP safe_mode is active or not
	 */
	private $safe_mode;
	
	/**
	 * @var boolean	Whether there are any open_basedir restrictions in place
	 */
	private $open_basedir;

	/**
	 * Proxy to be used for CURL connections
	 * @var string
	 */
	public $curl_proxy = "";

	/**
	 * UA to be used for web connections
	 * @var string
	 */
	private $user_agent = "";
	
	/**
	 * Initialize local properties 
	 */
	function __construct($load_config=true)
	{
		$this->path = realpath(SERVER_ROOT."/../");
		$this->cookie_jar = "{$this->path}/cache/cookie.txt";
		$v = new Varia();
		$this->safe_mode = $v->SafeMode();
    	$this->open_basedir = ini_get("open_basedir")!="";
    	if($load_config)
    	{
			$conf = new Configuration();
			if($conf->Get("proxy"))
			{
				$proxy_name = $conf->Get("proxy_name");
				$proxy_port = $conf->Get("proxy_port");
				if($proxy_name!="")
				{
					$this->curl_proxy = $proxy_name . ($proxy_port!=""? ":{$proxy_port}":"");
				}
			}
			$user_agent = $conf->Get("user_agent");
			if($user_agent=="")
			{
				include_once(SERVER_ROOT."/../classes/phpeace.php");
				$user_agent = "PhPeace " . PHPEACE_VERSION . " bot";
			}
			$this->user_agent = $user_agent;
    	}
	}

	/**
	 * Encode in base64 the content of a file
	 * 
	 * @param string	$filename	File to encode
	 * @param boolean	$is_abs		Whether the filename is absolute
	 * @return string
	 */
	public function Base64Encode($filename,$is_abs=FALSE)
	{
		if (!$is_abs)
			$filename = "$this->path/$filename";
		return chunk_split(base64_encode(file_get_contents($filename)));
	}
	
	/**
	 * Browse to a webpage and return its content
	 * 
	 * @param string	$url			URL
	 * @param boolean 	$long_wait		Whether to wait for some time (30 minutes) or not (30 seconds)
	 * @param boolean 	$follow_redir	Whether to follow HTTP redirects
	 * @param boolean 	$catch_error	Whether to catch errors or ignore them
	 * @param boolean	$use_cookie		Whether to use a cookie
	 * @return mixed					Web page content
	 */
	public function Browse($url,$long_wait=false,$follow_redir=false,$catch_error=true,$use_cookie=false)
	{
		$timeout = $long_wait? 1800 : 30;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_VERBOSE, 0 );
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		if($this->curl_proxy!="")
			curl_setopt($ch, CURLOPT_PROXY, $this->curl_proxy);
		if($follow_redir && !$this->safe_mode && !$this->open_basedir)
		{
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		}
		else 
		{
			curl_setopt($ch, CURLOPT_FAILONERROR, 1 );		
		}
		if(strpos($url,"https")!==false)
		{
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 		
		}
		if($use_cookie)
		{
			curl_setopt($ch, CURLOPT_COOKIESESSION, 0); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
		}
		$res = curl_exec($ch);
		if ($catch_error && curl_error($ch)!="")
			UserError("Curl Error",array("url"=>$url,"error"=>curl_error($ch)));
		curl_close ($ch);
		return $res;
	}

	/**
	 * Change ownership for all files in a directory 
	 * 
	 * @param string	$directory	Directory path
	 * @param string	$user		User to change ownership to
	 * @param string	$group		Group to change ownership to
	 * @param boolean	$recurse	Whether to descend recursively in the subdirectories
	 */
	public function ChownRecursive($directory,$user,$group,$recurse=TRUE)
	{
		if ($directory!="")
		{
			if (file_exists($directory) && !is_link($directory))
			{
				if ($recurse && is_dir($directory))
				{
					$handle = opendir($directory);
					while(false !== ($filename = readdir($handle)))
					{
						if ($filename != "." && $filename != ".." && is_dir("$directory/$filename")) {
							$this->ChownRecursive($directory."/".$filename,$user,$group,$recurse);
						}
						elseif ($filename != "." && $filename != ".." && !@is_link("$directory/$filename")) {
							chown("$directory/$filename",$user);
							chgrp("$directory/$filename",$group);
						}
					}
					closedir($handle);
				}
				chown($directory,$user);
				chgrp($directory,$group);
			}
		}
	}
	
	/**
	 * The MIME content-type associated to a file extension
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function ContentType($extension)
	{
		switch($extension)
		{
			case "avi": $type="video/x-msvideo"; break;
			case "doc": $type="application/msword"; break;
			case "epub": $type="application/epub+zip"; break;
			case "gif": $type = "image/gif"; break;
			case "gz": $type = "application/x-gzip"; break;
			case "htm":
			case "html": $type = "text/html"; break;
			case "jpeg":
			case "jpg": $type = "image/jpeg"; break;
			case "mp3": $type="audio/mpeg"; break;
			case "mpeg":
			case "mpg":
			case "mpe": $type="video/mpeg"; break;
			case "mov": $type="video/quicktime"; break;
			case "pdf": $type = "application/pdf"; break;
			case "png": $type = "image/png"; break;
			case "ppt": $type="application/vnd.ms-powerpoint"; break;
			case "tgz": $type = "application/x-gzip"; break;
			case "txt": $type = "text/plain"; break;
			case "xls": $type="application/vnd.ms-excel"; break;
			case "wav": $type="audio/x-wav"; break;
			case "zip": $type = "application/zip"; break;
			default: $type = "application/octet-stream"; break;
		}
		return $type;
	}

	/**
	 * Copy a file: this is currently implemented as a smylink, as most copy operations are 
	 * from the uploads directory to the public ones.
	 * 
	 * @param string	$orig		Original filename (relative)
	 * @param string	$copy		Target filename (relative)
	 * @param boolean	$overwrite	Whether to overwrite the target file (or symlink) if it already exists
	 * @return string
	 */
	public function Copy( $orig, $copy, $overwrite=true )
	{
		$return = true;
		$copy_abs = "$this->path/$copy";
		$orig_abs = "$this->path/$orig";
//		if ((@is_link($copy_abs) && !file_exists(@readlink($copy_abs))) || (@file_exists($orig_abs)=="1" && $overwrite))
			$this->Delete($copy_abs,true);
		if (!(@file_exists($orig_abs)==1))
		{
			UserError("Original file not found",array('orig'=>$orig,'copy'=>$copy),512);
			$return = false;
		}
		if (!(@file_exists($copy_abs)==1) && !$overwrite )
			UserError("Destination file exists but overwrite is FALSE",array('orig'=>$orig,'copy'=>$copy));
		if (!(@file_exists($copy_abs)==1 && @file_exists($orig_abs)==1) )
		{
			if($this->safe_mode || $this->open_basedir)
			{
				symlink($orig_abs,$copy_abs);			
			}
			else 
			{
				$num_slashes = substr_count($copy,"/");
				for($i=0;$i<$num_slashes;$i++)
					$orig = "../$orig";
				symlink($orig,$copy_abs);
			}
		}
		return $return;
	}

	/**
	 * Delete a file
	 * 
	 * @param string	$filename	File name
	 * @param boolean	$is_abs		Whether the filename is absolute 
	 */
	public function Delete( $filename,$is_abs=FALSE )
	{
		if (!$is_abs)
			$filename = "$this->path/$filename";
		if (file_exists($filename)==1 || is_link($filename))
			unlink($filename);
	}
	
	/**
	 * Execute a specific action on a directory
	 * 
	 * @param string $path		Directory path (relative)
	 * @param string $action	Action to execute: delete, clean, check
	 */
	public function DirAction($path,$action)
	{
		if ($action=="delete")
			$this->DirDelete($path);
		if ($action=="clean")
			$this->DirClean($path);
		if ($action=="check")
			$this->DirCreate($path);
	}

	/**
	 * Delete all files in a directory
	 * (not recursively)
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 */
	public function DirClean($directory,$is_abs=FALSE)
	{
		if (!$is_abs)
			$directory = "$this->path/$directory";
		if (file_exists($directory))
		{
			$files = $this->DirFiles($directory,true);
			foreach($files as $file)
				$this->Delete($file,TRUE);
		}
		else
			$this->DirCreate($directory,TRUE);
	}

	/**
	 * Create a directory if it does not exists
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 * @param integer	$mod		Directory octal mode (default to 0775)
	 */
	public function DirCreate($directory,$is_abs=FALSE,$mod=0775)
	{
		if (!$is_abs)
			$directory = "$this->path/$directory";
		if (!file_exists($directory)) {
			$oldmask = umask(0);
			mkdir($directory,$mod);
			umask($oldmask);
		}
	}

	/**
	 * Delete a directory if it exists
	 * (not recursive)
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 */
	public function DirDelete( $directory,$is_abs=FALSE )
	{
		if (!$is_abs)
			$directory = "$this->path/$directory";
		if (file_exists($directory))
		{
			$this->DirClean($directory,TRUE);
			rmdir($directory);
		}
	}

	/**
	 * Get all files in a directory
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 * @return	array				List of files (full path)
	 */
	public function DirFiles($directory,$is_abs=FALSE)
	{
		if (!$is_abs)
			$directory = "$this->path/$directory";
		$files = array();
		$handle = opendir($directory);
		if($handle!==false)
		{
			while(false !== ($filename = readdir($handle)))
			{
				if ($filename != "." && $filename != ".."  && $filename != ".svn" && (!is_dir($directory."/".$filename)))
				{
					$files[] = $directory . "/" . $filename;
				}
			}
			closedir($handle);
		}
		return $files;
	}

	/**
	 * Recursively delete a directory and all its content (including its subdirectories)
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 */
	public function DirRemove($directory,$is_abs=FALSE)
	{
		if ($directory!="")
		{
			if (!$is_abs)
				$directory = "$this->path/$directory";
			if (file_exists($directory))
			{
				$handle = opendir($directory);
				while(false !== ($filename = readdir($handle)))
					if ($filename != "." && $filename != ".." && is_dir("$directory/$filename"))
						$this->DirRemove($directory."/".$filename,TRUE);
					elseif ($filename != "." && $filename != "..")
						$this->Delete("$directory/$filename",TRUE);
				closedir($handle);
				$this->DirDelete($directory,TRUE);
			}
		}
	}

	/**
	 * Calculate the total size of a directory (in bytes)
	 * 
	 * @param string	$directory	Directory path
	 * @param boolean	$is_abs		Whether the path is absolute
	 * @return	integer				Size (in bytes)
	 */
	public function DirSize($directory,$is_abs=false)
	{
		if (!$is_abs)
			$directory = "$this->path/$directory";
		$size = 0;
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file)
		{
		    $size += $file->getSize();
		}
		return $size;
	}
	
	/**
	 * Execute a specific action on a topic path
	 * It is used to initialize a topic path and create all its necessary subdirectories
	 * 
	 * @param string 	$topic_path	The directory name of the topic
	 * @param string 	$action		Action to execute: check, clean, delete
	 * @param boolean	$has_domain	Whether the topic has its own separate domain
	 */
	public function DirTopic($topic_path,$action,$has_domain=false)
	{
		if($topic_path!="")
		{
			$conf = new Configuration();
			$tpaths = array();
			$conf_paths = $conf->Get("paths");
			foreach($conf_paths as $path)
			{
				if (strpos($path,"/")===0)
				{
					$tpath = substr($path,1,strrpos($path,"/")-1);
					$tpaths[] = $tpath;
				}
			}
			$utpaths = array_unique($tpaths);
			foreach($utpaths as $utpath)
			{
				$this->DirAction("$topic_path/$utpath",$action);
			}
			if($has_domain)
			{
				if($action=="delete")
				{
					$this->Delete("$topic_path/favicon.ico");
					$this->Delete("$topic_path/robots.txt");
				}
				$this->DirAction("$topic_path/tools",$action);
			}
			if($action=="delete")
			{
				$this->Delete("$topic_path/.htaccess");
				$this->Delete("$topic_path/.htpasswd");
				$this->Delete("$topic_path/topic_info.xml");
				$this->Delete("$topic_path/index.html");
			}
			$this->DirAction($topic_path,$action);
		}
		else 
			UserError("Topic missing path",array());
	}

	/**
	 * Download a file from a URL and save it locally
	 *  
	 * @param string	$from		URL to download from
	 * @param string	$to			Filename to save (relative)
	 * @param boolean	$is_critical	Whether this operation is critical or not. If critical, a critical error is raised which will stop script execution
	 * @return integer				HTTP code
	 */
	public function DownLoad($from,$to,$is_critical=false,$timeout=0)
	{
		$ch = curl_init($from);
		$fp = fopen("$this->path/$to", "w");
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		if($timeout>0) {
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
		}
		if(!$this->safe_mode && !$this->open_basedir)
		{
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		}
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		if($this->curl_proxy!="")
			curl_setopt($ch, CURLOPT_PROXY, $this->curl_proxy);
		if(strpos($from,"https")!==false)
		{
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 		
		}
		curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		if (curl_errno($ch)>0)
		{
			UserError("Curl Download Error",array("url"=>$from,"error"=>curl_error($ch)),$is_critical?256:512);
		}
		curl_close($ch);
		fclose($fp);
		return (int)$http_code;
	}
	
	/**
	 * Check if a file exists
	 * 
	 * @param string 	$filename	File name
	 * @param boolean	$is_abs		Whether the filename is absolute
	 * @return boolean				Whether the file exists or not
	 */
	public function Exists($filename,$is_abs=false)
	{
		if(!$is_abs)
			$filename = "$this->path/$filename";
		return file_exists($filename);
	}

	/**
	 * Get the extension from a filename
	 * 
	 * @param string $filename	File name
	 * @return string			Extension
	 */
	public function Extension($filename)
	{
		return strtolower(substr($filename,strrpos($filename,".")+1));
	}
	
	/**
	 * Get a local file
	 * (used to dynamically transfer local uploads)
	 * 
	 * @param string	$path		Filename (relative)
	 * @param array		$headers	HTTP headers
	 */
	public function GetLocalFile($path,$headers=array())
	{
		if ($this->Exists($path))
		{
			$filename = $this->path . "/" . $path;
			if(!isset($headers['Content-type']))
			{
				$mime = $this->MimeType($path);
				header("Content-type: $mime");			
			}
			header('Content-Type: application/force-download');
			header("Content-Length: " . filesize($filename));

			$base = basename($filename);
			$info = pathinfo($filename);
			header("Content-Disposition: attachment; filename=\"$base\"");
			header("Pragma: public");
			header("Cache-Control: max-age=0"); 
			foreach($headers as $header_key=>$header_value)
			{
				header($header_key .": " .$header_value);
			}
			readfile($filename);
		}
		else
		{
			header('HTTP/1.0 404 Not Found');
			exit;
		}
	}
	
	/**
	 * Get information about a file, such as its size, extension and path parts
	 *  
	 * @param string 	$filename	File name
	 * @param boolean	$is_abs		Whether the filename is absolute
	 * @return array
	 */
	public function FileInfo($filename,$is_abs=false)
	{
		if(!$is_abs)
			$filename = "$this->path/$filename";
		$info = array('xname'=>"file_info",'exists'=>file_exists($filename));
		if ($info['exists'])
		{
			$filesize = filesize($filename);
			$info['kb'] = floor($filesize/1024);
			$path_parts = pathinfo($filename);
			$info['name'] = $path_parts["basename"];
			$info['format'] = $path_parts["extension"];
			$info['size'] = $filesize;
		}
		return $info;
	}
	
	/**
	 * Get the UNIX timestamp of last time a file has been changed
	 *  
	 * @param string 	$filename	File name (relative)
	 * @return integer				UNIX timestamp of last modified time
	 */
	public function FileMTime($filename)
	{
		return filemtime("$this->path/$filename");
	}
	
	/**
	 * Remove invalid characters from a file name and make it lower case
	 * 
	 * @param string $filename	File name
	 * @return string			Sanitized file name
	 */
	public function FixFileName($filename)
	{
		$filename = str_replace(" ","_",$filename);
		$filename = str_replace("(","",$filename);
		$filename = str_replace(")","",$filename);
		$filename = str_replace("'","",$filename);
		$filename = str_replace("\$","",$filename);
		$filename = str_replace("&","",$filename);
		$filename = str_replace("\"","",$filename);
		return strtolower($filename);
	}
	
	/**
	 * Copy a file (hardcopy, not symlink)
	 * 
	 * @param string $orig	Original file name (relative)
	 * @param string $new	Target file name (relative)
	 * @param boolean	$overwrite	Whether to overwrite the target file (or symlink) if it already exists
	 */
	public function HardCopy($orig,$new,$overwrite=true)
	{
		$orig_abs = "$this->path/$orig";
		$new_abs = "$this->path/$new";
		if (@file_exists($orig_abs)==1)
		{
			if($overwrite || !(@file_exists($new_abs)))
			{
				copy($orig_abs,$new_abs);
			}
		}
		else
			UserError("Original file not found",array('orig'=>$orig));
	}

	/**
	 * Calculate the MD5 hash of a specific file
	 * 
	 * @param string $filename	File name (relative)
	 * @return string			32-character hash
	 */
	public function Hash($filename)
	{
		return md5_file("{$this->path}/$filename");
	}
	
	/**
	 * Get HTTP code of URL
	 * 
	 * @param string $url	URL
	 * @return integer		HTTP code
	 */
	public function HttpCode($url) {
		$http_code = 1;
		$url_array = parse_url($url);
		set_error_handler("ErrorTrash");
		$err = "";
		$fp = fsockopen($url_array['host'], $url_array['port'], $err, $err, 5);
		if($fp) {
			fputs($fp, "GET {$url_array['path']}?{$url_array['query']} HTTP/1.0\r\n\r\n\r\n");
			$got = fgets($fp, 256);
			$parts = array();
			if (preg_match("@HTTP/1.(.) ([0-9]*) (.*)@i", $got, $parts) > 0) {
				$http_code = (int)$parts[2];
			}
			fclose ($fp);
		}
		set_error_handler("ErrorHandler");
		return $http_code;
	}
	
	/**
	 * Get information about an image file (such as its dimensions and size)
	 * 
	 * @param string 	$filename	File name (relative)
	 * @return array
	 */
	public function ImageInfo($filename)
	{
		$info = $this->ImageSize($filename);
		$info['kb'] = $this->ImageFileSize($filename);
		return $info;
	}

	/**
	 * Get the dimensions (in pixel) of an image file
	 * 
	 * @param string 	$filename	File name (relative)
	 * @return array
	 */
	public function ImageSize($filename)
	{
		$size = getimagesize("{$this->path}/$filename");
		return array('width'=>$size[0],'height'=>$size[1],'mime'=>$size['mime']);
	}
	
	/**
	 * Get the file size of an image file
	 * 
	 * @param string 	$filename	File name (relative)
	 * @return array
	 */
	 public function ImageFileSize($filename)
	{
		return floor(filesize("$this->path/$filename")/1024);
	}

	/**
	 * Check if a file is allowed, depending on its type
	 * Size file extensions are not allowed for security reasons.
	 * Furthermore, depending on the file type their extensions is checked agains the allowed ones
	 * (which are configurable)
	 * 
	 * @param string 	$filename	File name (relative)
	 * @param string	$file_type
	 * @param boolean	$is_admin
	 * @return boolean
	 */
	public function IsGood($filename,$file_type,$is_admin)
	{
		$is_good = false;
		if($filename!="")
		{
			$file_ext = $this->Extension($filename);
			$bad_exts = array('cgi','pl','php3','php','exe','pif','bat','scr','com','asp','jsp','shtml','vbs');
			if(in_array($file_ext,$bad_exts))
			{
				UserError("Upload Error",array("Wrong extension"=>$file_ext));
			}
			else 
			{
                if (is_array($file_type))
                {
					foreach($file_type as $type)
                    {
                        if (strtolower(trim($type)) == strtolower($file_ext))
                        {
                            $is_good = true;
                            break;
                        }
                    }    
                }
                else
                {
					$conf = new Configuration();
					$doc_exts = $conf->Get("doc_files");
					$img_exts = $conf->Get("image_files");
					$audio_exts = $conf->Get("audio_files");
					$video_exts = $conf->Get("video_files");
					$cal_exts = array('ics','ical');
                    switch($file_type)
                    {
                        case "any":
                            $is_good = true;
                        break;
                        case "cal":
                        	$is_good = $is_admin? true : in_array($file_ext,$cal_exts);
                        break;
                        case "doc":
							$is_good = $is_admin? true : in_array($file_ext,$doc_exts) || in_array($file_ext,$img_exts) || in_array($file_ext,$audio_exts) || in_array($file_ext,$video_exts);
                        break;
                        case "img":
							$is_good = in_array($file_ext,$img_exts);
                        break;
                        case "aud":
							$is_good = in_array($file_ext,$audio_exts);
                        break;
                        case "vid":
							$is_good = in_array($file_ext,$video_exts);
                        break;
                        case "tar":
                            $is_good = true;
                        break;
                        default:
                            UserError("File type not defined",array("file"=>$filename));
                    }
                }
			}
		}
		return $is_good;
	}
	
	/**
	 * Get the maximum file size (in bytes) as configured
	 *  
	 * @return	integer
	 */
	public function MaxFileSize()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		return  $ini->Get('max_file_size');
	}
	
	/**
	 * Get the mime type of a file, using the available operating system instructions
	 *
	 * @param string $filename	File name (relative)
	 * @return string
	 */
	private function MimeType($filename)
	{
		$filename = "$this->path/$filename";
		$mime_type = "application/octet-stream";
		if(!$this->safe_mode)
		{
			$mime = trim( `file -bi $filename` );
			if($mime!="")
				$mime_type = $mime;
		}
		if (function_exists('mime_content_type'))
		{
			$mime = mime_content_type($filename);
			if($mime!="")
				$mime_type = $mime;
		}
		return $mime_type;		
	}

	/**
	 * Move a file that has just been uploaded to its permanent location under the uploads directory
	 * The temporary location for uploaded files is configured in php.ini
	 *  
	 * @param string $upload	Temporary file name assigned by PHP 
	 * @param string $dest		File destination (relative)
	 */
	public function MoveUpload($upload,$dest)
	{
		$dest = "$this->path/$dest";
		move_uploaded_file($upload, $dest);
		chmod($dest, 0644);
	}
	
	/**
	 * Send HTTP post request to a URL
	 * 
	 * @param string 	$host		URL hostname
	 * @param string 	$page		URL page
	 * @param array 	$data		Data to post
	 * @param boolean 	$use_cookie	Whether to use cookies or not
	 * @param integer 	$timeout	Timeout in seconds
	 * @param boolean 	$json		Whether to encode payload in JSON
	 * @return string				Page response
	 */
	public function PostData($host,$page,$data,$use_cookie=false,$timeout=10,$json=false)
	{
		$content = false;
		if ($host!="" && is_array($data) && count($data)>0)
		{
			if($json) {
				$params = json_encode($data);
			} else {
				$counter = 1;
				$params = "";
				$tot_data = count($data);
				foreach($data as $key => $value)
				{
					$params .= "$key=$value";
					if ($counter < $tot_data)
						$params .= "&";
						$counter ++;
				}
			}
	 		$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST,1);
	   		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			curl_setopt($ch, CURLOPT_URL,$host.$page);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_VERBOSE, 0 );
			curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
			if($this->curl_proxy!="") {
				curl_setopt($ch, CURLOPT_PROXY, $this->curl_proxy);
			}
			if($json) {
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT,$timeout);
			if($use_cookie)
			{
				curl_setopt($ch, CURLOPT_COOKIESESSION, 0); 
				curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
			}
			$content = curl_exec($ch);
			if (curl_error($ch)!="")
				UserError("Curl Error Post",array("host"=>$host,"page"=>$page,"error"=>curl_error($ch)));
			curl_close ($ch);
		}
		return $content;
	}
	
	/**
	 * Shell script to be executed after every file operation
	 * The script name is configurable in system settings
	 * Useful to launch synchronization scripts across multiple frontends
	 */
	public function PostUpdate()
	{
		$va = new Varia();
		$conf = new Configuration();
		$script = $conf->Get("post_update_script");
		if($script!="") // Should check if script exists too?
		{
			$va->SetTimeLimit(900);
			$va->Exec($script);
			if($va->return_status!=0)
			{
				UserError("Post update script failed",array("script"=>$script,"return_code"=>$va->return_status),512);
			}
		}
	}
	
	/**
	 * Get the absolute path of a file
	 * 
	 * @param string	$filename	File name (relative)
	 * @return string
	 */
	public function RealPath($filename)
	{
		return "$this->path/$filename";
	}

	/**
	 * Rename (i.e. move) a file
	 * 
	 * @param string 	$old_name		Current file name (relative)
	 * @param string	$new_name		New file name (relative)
	 * @param boolean 	$overwrite		Whether to overwrite the new file if it already exists
	 * @param boolean 	$force			Whether to force the operation even if the original does not exists
	 * @param boolean 	$is_old_abs		Whether the current file name is absolute or not 
	 * @param boolean 	$catch_errors	Whether to catch errors
	 * @return boolean					Operation successful or not
	 */
	public function Rename($old_name,$new_name,$overwrite=false,$force=false,$is_old_abs=false,$catch_errors=true)
	{
		$old_name2 = $is_old_abs?  $old_name : "$this->path/$old_name";
		$new_name2 = "$this->path/$new_name";
		$rename = FALSE;
		if ((file_exists($old_name2) || $force) && (!file_exists($new_name2) || $overwrite))
		{
			rename($old_name2,$new_name2);
			$rename = TRUE;
		}
		elseif($catch_errors)
		{
			if (!file_exists($old_name2))
				UserError("File rename failed",array('File not found'=>$old_name));
			elseif (file_exists($new_name2))
				UserError("File rename failed",array('Cannot overwrite'=>$new_name));
		}
		return $rename;
	}

	/**
	 * Get file size (in kylobytes)
	 * 
	 * @param string $filename	File name (relative)
	 * @param boolean $format	Format the output (adding "Kb")
	 * @return string
	 */
	public function Size($filename,$format = true)
	{
		$size = "0";
		$filename = "$this->path/$filename";
		if (file_exists($filename))
		{
			$size = floor(filesize($filename)/1024);
			if ($format)
				$size .= " Kb";
		}
		return $size;
	}

	/**
	 * Extract a TAR archive
	 * 
	 * @param string $filename		TAR file name (relative)
	 * @param string $destination	Destination directory (relative)
	 */
	public function TarExtract($filename,$destination)
	{
		include_once("Archive/Tar.php");
		$tar = new Archive_Tar("$this->path/$filename");
		$return = $tar->extract("$this->path/$destination");
		if(!$return && isset($tar->error_object) && isset($tar->error_object->message)) {
			UserError("Tar extraction error",array("filename"=>$filename,"destination"=>$destination,"error"=>$tar->error_object->message));
		}
	}

	/**
	 * Get the list of all files present in a TAR archive
	 * 
	 * @param string $filename	TAR file name (relative)
	 * @return array
	 */
	public function TarListContent($filename)
	{
		include_once("Archive/Tar.php");
		$tar = new Archive_Tar("$this->path/$filename");
		return $tar->listContent();
	}

	/**
	 * Read a text file and return its content in an array of lines
	 * 
	 * @param string $filename	File name (relative)
	 * @return array
	 */
	public function TextFileLines($filename)
	{
		$text = array();
		if ($this->Exists($filename))
		{
			$fp = @fopen ("$this->path/$filename", "r");
			if ($fp)
			{
				while (!feof ($fp))
				{
					$text[] = fgets($fp, 2048);
				}
			}
			fclose($fp);
		}
		return $text;
	}

	/**
	 * Read a text file and return its content
	 * 
	 * @param string	$filename	File name
	 * @param boolean	$is_abs		Whether the filename is absolute 
	 * @return string
	 */
	public function TextFileRead($filename,$is_abs=false)
	{
		if(!$is_abs)
			$filename = "$this->path/$filename";
		$text = "";
		if ($this->Exists($filename,true))
		{
			$fp = @fopen ($filename, "r");
			if ($fp)
				while (!feof ($fp))
					$text .= fgets($fp, 1024);
			fclose($fp);
		}
		return $text;
	}
	
	/**
	 * Read a text file and return its content in an array of lines
	 * 
	 * @param string $filename	File name (relative)
	 * @return array
	 */
	public function TextFileReadArray($filename)
	{
		$lines = array();
		if ($this->Exists($filename))
		{
			$lines = file($this->RealPath($filename));
		}
		return $lines;
	}
	
	/**
	 * Set access and modification times of a file
	 * 
	 * @param string $filename	File name (relative)
	 */
	public function Touch($filename)
	{
		touch("$this->path/$filename");
	}

	/**
	 * Get a local file
	 * 
	 * @param string $filename	File name (relative)
	 * @return boolean			Operation successful
	 */
	public function Transfer($file)
	{
		$file2 = "$this->path/$file";
		$status = FALSE;
		if (!is_file($file2) or connection_status()!=0)
		{
			header("HTTP/1.0 404 Not Found");
			echo "<p>File not found: $file</p>";
			return ($status);
		}
		$filename = basename($file);
		header("Content-type: " . $this->ContentType($this->Extension($filename)) );
		header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\"" );
		header("Content-Length: " . filesize($file2) );
		header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0, public");
		header("Pragma: no-cache");
		$fp = fopen($file2, 'rb');
		if ($fp)
		{
			while(!feof($fp) and (connection_status()==0))
			{
				print(fread($fp, 1024*8));
				flush();
			}
			$status = (connection_status()==0);
			fclose($fp);
		}
		return ($status);
	}

	/**
	 * Write content to a specific file
	 * 
	 * @param string	$filename	File to write
	 * @param string	$content	Content to write
	 * @param string	$mode		File mode (w=write, a=append)
	 * @param boolean	$is_abs		Whether the filename is absolute
	 */
	public function WritePage($filename, $content, $mode="w", $is_abs=false)
	{
		if(!$is_abs)
			$filename = "$this->path/" . ltrim($filename,"/");
		if (!is_dir($filename))
		{
			$fp = fopen($filename, $mode);
			flock($fp,LOCK_EX);
			fwrite($fp, $content);
			flock($fp,LOCK_UN);
			fclose($fp);
			chmod($filename, 0664);
		}
		unset($content);
	}

	/**
	 * Write a compressed (zip) file
	 * 
	 * @param string	$filename	File to write
	 * @param string	$content	Content to write
	 * @param string	$mode		File mode (w=write, a=append)
	 */
	public function WriteZip($filename, $content, $mode="w")
	{
		$filename = "$this->path/" . ltrim($filename,"/");
		if (!is_dir($filename))
		{
			$fp = gzopen($filename, $mode);
			gzwrite($fp, $content);
			gzclose($fp);
			chmod($filename, 0664);
		}
	}
}
?>
