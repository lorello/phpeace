<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/texthelper.php");
include_once(SERVER_ROOT."/../classes/varia.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/irl.php");
include_once(SERVER_ROOT."/../classes/modules.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
include_once(SERVER_ROOT."/../classes/payment.php");
include_once(SERVER_ROOT."/../classes/xmlhelper.php");
include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/datetime.php");
include_once(SERVER_ROOT."/../classes/geo.php");

class Layout
{	
	/** 
	 * @var Topic */
	public $topic;
	
	/** 
	 * @var PageTypes */
	public $pt;

	/** 
	 * @var IRL */
	public $irl;

	/** 
	 * @var Ini */
	public $ini;
	
	/** 
	 * @var Topics */
	public $groups;

	/**
	 * @var TextHelper */
	public $th;

	/**
	 * @var Translator */
	public $tr;

	/**
	 * @var DateTimeHelper */
	private $dt;
	
	/**
	 * @var Configuration */
	public $conf;

	/** 
	 * @var XmlHelper */
	public $xh;

	public $pub_web;
	public $global = false;
	public $live;
	public $article;
	public $cache_topic;
	public $cache_common;
	public $id_style;
	public $id_type;
	public $subtype;
	public $newsletter_ready = false;

	/** 
	 * @var Ontology */
	private $ontology;

	/** 
	 * @var Comments */
	private $comments;

	private $id_language;
	private $doctype;
	private $profiling;
	private $preview;
	private $protected;
	private $modules;
	private $item;
	private $item_type;
	private $xml_link;
	private $id_module;
	private $module;
	private $records_per_page;
	private $records_per_page_default;
	private $currency;
	private $feed_dateformat;
	private $current_page_params = array();
	private $current_page_global = false;
	private $upload_host;
	private $highslide;
	public $docs_covers_size;

	function __construct($preview=false,$protected=false,$live=true)
	{
		$this->ini = new Ini;
		$this->irl = new IRL($protected);
		$this->pub_web = $this->ini->Get('pub_web');
		$this->id_language = $this->ini->Get('id_language');
		$this->profiling = $this->ini->Get('profiling');
		$this->doctype = $this->ini->GetModule("layout","xsl_output_doctype",XSL_DEFAULT_OUTPUT_DOCTYPE);
		$this->th = new TextHelper;
		$this->conf = new Configuration();
		$this->modules = Modules::AvailableModules();
		$this->irl->ModulesActiveSet($this->modules);
		$this->pt = new PageTypes();
		$this->preview = $preview;
		$this->protected = $protected;
		$this->live = $live;
		$this->pt->ft->LayoutMode($preview,$protected,$live);
		$this->records_per_page = $this->conf->Get("records_per_page");
		$this->records_per_page_default = $this->records_per_page;
		$this->upload_host = $this->conf->Get("upload_host");
		$this->highslide = $this->conf->Get("highslide");
		$this->docs_covers_size = $this->conf->Get("docs_covers_size");
		$id_currency = $this->ini->Get("default_currency");
		$p = new Payment();
		$currencies = $p->currencies;
		$this->currency = $currencies[$id_currency];
		$this->xh = new XmlHelper($this->preview && !$this->protected);
		unset($p);
	}

	public function __destruct()
	{
		unset($this->ini);
		if(isset($this->irl))
			$this->irl->__destruct();
		unset($this->irl);
		unset($this->th);
		unset($this->conf);
		unset($this->pt);
		unset($this->xh);
		if(isset($this->ontology))
			unset($this->ontology);
		if(isset($this->topic) && is_object($this->topic))
		{
			$this->topic->__destruct();
			unset($this->topic);
		}
		if(isset($this->groups))
			unset($this->groups);
		if(isset($this->comments))
			unset($this->comments);
		if(isset($this->tr))
		{
			$this->tr->__destruct();
			unset($this->tr);
		}
	}

	function __clone()
	{
		foreach ($this as $key => $val)
		{
			if (is_object($val) || (is_array($val)))
			{
				$this->{$key} = unserialize(serialize($val));
			}
		}
	}

    private function Article($id_article)
	{
		if(isset($this->cache_common))
		{
			$common = $this->cache_common;
			$common['publish']['id'] = $id_article;
		}
		else 
			$common = $this->Common($id_article,$this->id_type);
		$topic = isset($this->cache_topic)? $this->cache_topic : $this->TopicCommon();
		$article = $this->ArticleContent($id_article);
		if($article['article']['available']=="0" && $this->protected && $this->live)
		{
			$id_p = (int)$topic['topic']['user']['id'];
			if(!$id_p>0)
			{
				$article['article']['login'] = $this->LoginFirst($this->topic->id);
			}
		}
		$this->current_page_params['id'] = $id_article;
		if($this->protected)
			$this->current_page_params['protected'] = $article['article']['available']=="0";
		return array_merge($common,$topic,$article);
	}
	
	private function ArticleAuthor($item)
	{
		$author = array();
		if ($item['author']=="" && $item['id_user']>0)
		{
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User;
			$u->id = $item['id_user'];
			$user = $u->UserGet();
			$author['name'] = $user['name'];
			$author['id_user'] = $item['id_user'];
			if($user['user_show'])
				$this->Urlify($author,"user",array('u'=>$item['id_user']),true);
			elseif($user['email_visible'])
				$author['email'] = $user['email'];
			if($user['photo_visible'])
			{
				$thumb = array();
				$thumb['xname'] = "image";
				$img_sizes = $this->conf->Get("img_sizes");
				$thumb['width'] = $img_sizes[0];
				$this->Urlify($thumb,"user_image",array('id'=>$item['id_user']),TRUE );
				$author['image'] = $thumb;
			}
			$author['user_show'] = $user['user_show'];
			$author['signature'] = array('xvalue'=>$user['signature']);
			unset($u);
		}
		else
		{
			$author['name'] = $item['author'];
		}
		$author['notes'] = $item['author_notes'];
		return $author;
	}

	private function ArticleBox($id_box)
	{
		$abox = array();
		if (isset($this->item) && $this->item_type=="box")
		{
			$box = $this->item;
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article(0);
			$box = $a->BoxGet($id_box);
			unset($a);
		}
		$box['popup'] = 0;
		$abox = $this->ArticleBoxItem($box);
		if(isset($this->cache_common))
		{
			$common = $this->cache_common;
			$common['publish']['id'] = $id_box;
		}
		else 
			$common = $this->Common($id_box,$this->id_type);
		return array_merge($common,array('box'=>$abox));
	}

	private function ArticleBoxItem($box)
	{
		$abox = array('xname'=>"box",'id'=>$box['id_box'],'title'=>$box['title'],'align'=>$box['align'],'id_type'=>$box['id_type'],
			'popup'=>$box['popup'],'is_html'=>$box['is_html'],'id_width'=>$box['width'],'show_title'=>$box['show_title']);
		if ($box['popup']==1)
		{
			$abox['z_width'] = $box['z_width'];
			$abox['z_height'] = $box['z_height'];
			$abox['label'] = $this->tr->Translate("popsup");
			$this->Urlify($abox,"article_box",array('id'=>$box['id_box'],'id_topic'=>$this->topic->id));
		}
		$content = ($box['is_html'])? $box['content'] : $this->Htmlise($box['content'],false);
		$abox['content'] = $this->ExplodeMarkers($content);
		$abox['notes'] = array('xvalue'=>$this->Htmlise($box['notes'],false));
		return $abox;
	}

	public function ArticleContent($id_article,$with_features=true,$with_related_content=true,$with_full_content_info=false)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$art = (isset($this->article))? $this->article : $a->ArticleGet();
		if($art['id_article']!=$id_article)
			$art = $a->ArticleGet();
		$hidden_fields = array();
		if($art['id_template']>0)
		{
			include_once(SERVER_ROOT."/../classes/template.php");
			$te = new Template("article");
			$template = $te->TemplateGet($art['id_template']);
			$hidden_fields = $template['hidden_fields'];
		}
		$article = $this->ArticleItem($art,"article",$hidden_fields);
		if($art['id_template']>0)
		{
			$article['id_template'] = $art['id_template'];
			$template_values = $a->TemplateValues($art['id_template'],false,false);
			if(count($template_values)>0)
			{
				$tvalues = array();
				foreach($template_values as $template_value)
				{
					$tvalues['tp_'.$template_value['id_template_param']] = $this->TemplateValueItem($template_value);
				}
				$article['template_values'] = $tvalues;
			}
		}
		$article['headline_visible'] = $art['headline_visible'];
		if (!$art['id_topic']>0)
			UserError("Topic not set for article $id_article",array('id_topic'=>$art['id_topic'],'id_subtopic'=>$art['id_subtopic']),256,true);
		if (!$art['id_subtopic']>0 && !$this->preview)
			UserError("Subtopic not set for article $id_article",array('id_topic'=>$art['id_topic'],'id_subtopic'=>$art['id_subtopic']));
		$article_keywords = $this->Keywords($id_article,"article",false);
		$article['keywords'] = array_merge($this->Keywords($art['id_topic'],"topic"), $this->Keywords($art['id_subtopic'],"subtopic"), $article_keywords );
		$article['text-align'] = $a->alignment[$art['align']];
		$article['id_language'] = $art['id_language'];
		if ($this->ini->Get("licences") && $art['id_licence'] > 0 && (!($art['id_template']>0 && in_array("licence",$hidden_fields))))
			$article['licence'] = $this->Licence($art['id_licence'],$article['author']['name']);
		if(!($art['id_template']>0 && in_array("content",$hidden_fields)))
		{
			$content = ($art['is_html'])? $art['content'] : $this->Htmlise($art['content']);
			if($this->id_type=="14" && !$this->global)
				$content = preg_replace('/(\<a href=")(.*?)"\>(([^http|www].*?)(?=\<\/a\>))/i', "<u>$4</u> [$2]", $content);
			$article['content'] = $this->ExplodeMarkers($content);
		}
		if($with_full_content_info)
		{
			$full_content = array();
			$full_content['align'] = $art['align'];
			$full_content['is_html'] = $art['is_html'];
			$full_content['id_licence'] = $art['id_licence'];
			$full_content['content'] = array('xvalue'=>$art['content']);
			$full_content['notes'] = array('xvalue'=>$art['notes']);
			$article['full_content'] = $full_content;
		}
		if(!($art['id_template']>0 && in_array("notes",$hidden_fields)))
			$article['notes'] = array('xvalue'=>$this->Htmlise($art['notes'],false));
		if($this->IsModuleActive("translations"))
		{
			$translator = $a->Translator();
			$languages = $this->tr->Translate("languages");
			if($translator['id_translation']>0)
			{
				$tra = array();
				if($translator['translator']!="")
				{
					$tra['translator'] = $translator['translator'];
					$tra['disclaimer'] = array('xvalue'=>$this->tr->TranslateParams("translation_note",array($translator['translator'],$this->pub_web,$this->ini->Get("title"),$this->topic->name)));
				}
				else 
				{
					$tra['translator'] = $translator['user_name'];
					$tra['id_user'] = $translator['id_user'];
					include_once(SERVER_ROOT."/../classes/user.php");
					$u = new User;
					$u->id = $translator['id_user'];
					$user = $u->UserGet();
					if($user['user_show']) {
					    $this->Urlify($tra,"user",array('t'=>$translator['id_user']),true);
					    $tra['disclaimer'] = array('xvalue'=>$this->tr->TranslateParams("translation_note2",array($translator['user_name'],$tra['url'],$this->ini->Get("title"),$this->topic->name,$translator['id_user'])));
					} else {
					    $tra['disclaimer'] = array('xvalue'=>$this->tr->TranslateParams("translation_note3",array($translator['user_name'],$this->ini->Get("title"),$this->topic->name,$translator['id_user'])));
					}
				}
				if($translator['id_rev']>0) {
					$tra['revision'] = $translator['rev_name'];
				}
				if($translator['id_ad']>0) {
					$tra['adaptation'] = $translator['ad_name'];
				}
				if($translator['id_article']>0)
				{
					$atr = new Article($translator['id_article']);
					$atr->ArticleLoad();
					if($atr->visible)
					{
						$l2 = new Layout($this->preview,$this->protected,$this->live);
						$l2->TopicInit($atr->id_topic);
						$l2->TranslatorInit($this->id_language,$this->topic->id_style);
						$original = array('name'=>$atr->headline);
						$l2->Urlify($original,"article",array('id'=>$translator['id_article'],'id_topic'=>$atr->id_topic));
						$l2->__destruct();
						unset($l2);
						$tr_language = $languages[$translator['id_language']];
						$original['label'] = $this->tr->TranslateParams("translation_original",array($tr_language));
						$tra['original'] = $original;
					}
				}
				if($translator['comments']!="")
					$tra['comments'] = array('label'=>$this->tr->Translate("translation_notes"),'xvalue'=>$this->Htmlise($translator['comments'],false));
				$article['translation'] = $tra;
			}
			$atras = $a->TranslationsPub();
			if(count($atras)>0)
			{
				$translations = array('label'=>$this->tr->Translate("translations"));
				foreach($atras as $atra)
				{
					$aitem = $this->ArticleItem($atra);
					$aitem['tr_language'] = $languages[$atra['id_language_trad']];
					$translations['at_'.$atra['id_article']] = $aitem;
				}
				$article['translations'] = $translations;
			}
		}

		if(!($art['id_template']>0 && in_array("images",$hidden_fields)))
		{
			$images = $a->ImageGetAll();
			if (count($images)>0)
			{
				$aimages = array('id_article'=>$id_article); 
				foreach($images as $image)
				{
					$image['id_article'] = $id_article;
					$image_array = $this->ImageItem($image);
					$image_array['format'] = $image['format'];
					if ($image['zoom'])
					{
						$image_zoom = array('width'=>$image['orig_width'],'height'=>$image['orig_height'],'label'=>$this->tr->Translate("zoom_click"),'label_close'=>$this->tr->Translate("close_window"));
						$this->Urlify($image_zoom,"image_orig",array('id'=>$image['id_image'],'format'=>$image['format']));
						if ($this->ini->Get("licences") && $image['id_licence'] > 0)
							$image_zoom['licence'] = $this->Licence($image['id_licence'],$image['author']);
						$image_array['image_zoom'] = $image_zoom;
						$image_popup = array('width'=>$image['orig_width'],'height'=>$image['orig_height'],'label'=>$this->tr->Translate("popsup"));
						$this->Urlify($image_popup,"image_zoom",array('id'=>$image['id_image'],'id_article'=>$id_article,'id_topic'=>$art['id_topic']) );
						$image_array['image_popup'] = $image_popup;
					}
					if($image['id_image']==$art['id_image']) {
						$image_array['associated'] = 1;
						$img_sizes = $this->conf->Get("img_sizes");
						$img_sizes_keys = array_reverse(array_keys($img_sizes));
						$og_image = array('width'=>$img_sizes[$img_sizes_keys[0]]);
						$og_image['height'] = floor($image['orig_height'] * $img_sizes[$img_sizes_keys[0]] / $image['orig_width']);
						$this->Urlify($og_image,"image",array('id'=>$image['id_image'],'size'=>$img_sizes_keys[0],'format'=>$image['format'],'id_article'=>$image['id_article']));
						$image_array['og_image'] = $og_image;
					}
					$aimages['i_' . $image['id_image']] = $image_array;
				}
				$article['images'] = $aimages;
			}
		}
	
		if(!($art['id_template']>0 && in_array("boxes",$hidden_fields)))
		{
			$boxes = $a->BoxGetAll();
			if (count($boxes)>0)
			{
				$aboxes = array('id_article'=>$id_article);
				foreach($boxes as $box)
				{
					$abox = $this->ArticleBoxItem($box);
					if($with_full_content_info)
						$abox['full_content'] = array('xvalue'=>$box['content']);
					$aboxes['b_'.$box['id_box']] = $abox;
				}
				$article['boxes'] = $aboxes;
			}
		}

		if(!($art['id_template']>0 && in_array("docs",$hidden_fields)))
		{
			$docs = $a->DocGetAll();
			if (count($docs)>0)
			{
				$adocs = array('id_article'=>$id_article); 
				foreach($docs as $doc)
					$adocs['d_'.$doc['id_doc']] = $this->ArticleDocItem($doc,"article_doc",$id_article,$art['id_topic']);
				$article['docs'] = $adocs;
			}
		}
					
		if(!($art['id_template']>0 && in_array("related",$hidden_fields)))
		{
			$related = $a->RelatedAll();
			if (count($related)>0)
			{
				$arelated = array();
				foreach($related as $rel)
				{
					if($rel['with_content'] && $with_related_content)
					{
						$article_content = $this->ArticleContent($rel['id_article'],false,false);
						$rel_item = $article_content['article'];
						$rel_item['xname'] = "item";
					}
					else 
					{
						$rel_item = $this->ArticleItem($rel,"item");
					}
					$rel_item['with_content'] = $with_related_content? $rel['with_content'] : "0";
					$arelated['id'.$rel['id_article']] = $rel_item;
				}
				$article['related'] = $arelated;
			}
		}

		if($this->IsModuleActive("books"))
		{
			$abooks = array();
			$books = $a->Books();
			foreach($books as $book)
				$abooks['b_'.$book['id_book']] = $this->BookItem($book,"item");
			$article['books'] = $abooks;
		}
		
		include_once(SERVER_ROOT."/../classes/comments.php");
		$this->comments = new Comments("article",$id_article);
		$comments = $this->Comments($art['allow_comments'],$art['topic_allow_comments']);
		$article['comments'] = $comments;

		$art_array = array();
		$art_array['article'] = $article;
		$art_array['subtopic'] = array('id'=>$art['id_subtopic']);
		if($with_features)
		{
		    $real_keywords = array();
		    foreach($article_keywords as $article_keyword) {
		        if($article_keyword['id_type']!=4 && $article_keyword['id_type']!=6) {
		            $real_keywords[] = $article_keyword;
		        }
		    }
		    $feature_params = array('id_article'=>$art['id_article'],'id_subtopic'=>$art['id_subtopic'],'id_topic'=>$art['id_topic'],'id_topic_group'=>$art['id_topic_group'],'keywords'=>$real_keywords);
			$art_array['features'] = $this->Features($this->id_type,0,$feature_params);
		}
		unset($a);
		return $art_array;
	}
	
	private function ArticleDocItem( $doc,$res_type="article_doc",$id_article=0,$id_topic=0)
	{
		$params = array('id'=>$doc['id_doc'],'format'=>$doc['format']);
		$filename = $this->irl->PathAbs($res_type,$params);
		$d = array();
		$d['xname'] = "doc";
		$d['id'] = $doc['id_doc'];
		$d['seq'] = $doc['seq'];
		$d['id_subtopic_form'] = $doc['id_subtopic_form'];
		$d['title'] = $doc['title'];
		$d['id_language'] = $doc['id_language'];
		$d['id_licence'] = $doc['id_licence'];
		$d['description'] = array('xvalue'=>$doc['description']);
		$d['source'] = array('xvalue'=>$doc['source']);
		$d['author'] = $doc['author'];
		if ($this->ini->Get("licences") && $doc['id_licence'] > 0)
			$d['licence'] = $this->Licence($doc['id_licence'],$d['author']);
		$d['file_info'] = $this->FileInfo($filename);
		$this->Urlify($d,$res_type,$params,$res_type=="org_doc");
		if($this->docs_covers_size > -1)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			if($fm->Exists("uploads/docs/covers/{$doc['id_doc']}.jpg"))
			{
				$cover = array();
				$cover['xname'] = "cover";
				$img_sizes = $this->conf->Get("img_sizes");
				$cover['width'] = $img_sizes[$this->docs_covers_size];
				$this->Urlify($cover,"doc_cover",array('id'=>$doc['id_doc'],'format'=>$doc['format'],'size'=>$this->docs_covers_size),TRUE );
				$d['cover'] = $cover;
			}
		}
		if($doc['id_subtopic_form']>0 && $id_article>0 && $id_topic>0)
		{
			$this->TopicInit($id_topic);
			$subtopic = $this->topic->SubtopicGet($doc['id_subtopic_form']);
			if($subtopic['id_type']==$this->topic->subtopic_types['contact'])
			{
				$d['subtopic'] = $this->SubtopicItem($subtopic,0,true,$doc['id_doc']);
			}
		}
		return $d;
	}
	
	public function ArticleItem($item,$name="article",$hidden_fields=array())
	{
		$template = count($hidden_fields)>0;
		$this->TopicInit($item['id_topic']);
		$article = array();
		$article['xname'] = $name;
		$article['type'] = "article";
		$article['id'] = $item['id_article'];
		$article_params = array('id'=>$item['id_article'],'id_topic'=>$item['id_topic']);
		if($item['jump']!="")
		{
			$article['jump'] = $this->th->String2Url($item['jump']);
			$article_params['jump'] = $article['jump'];
		}
		if($this->protected)
		{
			$article_params['protected'] = $item['available']=="0";
		}
		$this->Urlify($article,"article",$article_params);
		$article['is_main'] = $item['is_main'];
		$article['id_template'] = $item['id_template'];
		$article['id_language'] = $item['id_language'];
		$article['available'] = $item['available'];
		$article['show_date'] = $item['show_date'];
		$article['show_author'] = $item['show_author'];
		$article['highlight'] = $item['highlight'] == "1"? '1':'0';
		$article['id_subtopic'] = $item['id_subtopic'];
		if ($item['id_subtopic']>0)
			$article['breadcrumb'] = $this->SubtopicPath($item['id_subtopic']);
		$topic = array('id'=>($this->topic->id),'name'=>($this->topic->name),'id_group'=>($this->topic->id_group));
		$this->Urlify($topic,"topic_home",array('id_topic'=>$this->topic->id));
		$article['topic'] = $topic;
		if ($item['show_author'] && !($template && in_array("author",$hidden_fields)))
			$article['author'] = $this->ArticleAuthor(array('author'=>$item['author'],'author_notes'=>$item['author_notes'],'id_user'=>$item['id_user']));
		if (!($template && in_array("source",$hidden_fields)))
		{
			$source = array('show'=>$item['show_source'],'description'=>array('xvalue'=>$this->Htmlise($item['source'],false)));
			if ($item['original'] != "" && $item['original'] != "0000-00-00" && $item['original'] != "1970-01-01")
				$source['date'] = $this->FormatDate($item['original'],FALSE);
			$article['source'] = $source;
		}
		if(is_numeric($item['total_score']))
			$article['score'] = $item['total_score'];
		$ts = $item['written_ts'];
		$article['ts'] = $ts;
		if ($ts>0)
		{
			$article['date_iso8601'] = date("c",$ts);
			$article['display_date'] = $this->FormatDate($ts);
			$article['day'] = date("d",$ts);
			$article['month'] = date("n",$ts);
			$article['month_name'] = $this->FormatMonth($ts);
			$article['year'] = date("Y",$ts);
		}
		if (!($template && in_array("halftitle",$hidden_fields)))
			$article['halftitle'] = array('xvalue'=>$item['halftitle']);
		$article['headline'] = array('xvalue'=>$item['headline']);
		if (!($template && in_array("subhead",$hidden_fields)))
			$article['subhead'] = array('xvalue'=>$this->Htmlise($item['subhead'],false));
		$article['keywords'] = $this->Keywords($item['id_article'],"article",false);	
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($item['id_article']);
		if($name=="item")
		{
			$image = $a->Image();
			$image['associated'] = true;
			if($image['id_image']>0)
				$article['image'] = $this->ImageItem($image);
		}
		else
		{
			$images = $a->ImageGetAll();
			if (count($images)>0)
			{
				$art_images = array();
				foreach($images as $image)
				{
					$image['id_article'] = $item['id_article'];
					$art_images['i_' . $image['id_image']] = $this->ImageItem($image);
				}
				$article['images'] = $art_images;
				unset($art_images);
			}
			unset($images);
		}
		unset($a);
		return $article;
	}

	private function ArticleCommentsItem($item,$name="article")
	{
		$article = $this->ArticleItem($item,$name);
		if($item['hdate_ts']>0)
			$article['hdate'] = $this->FormatDate($item['hdate_ts']);
		$this->Urlify($article,"comments",array('id_item'=>$item['id_article'],'id_r'=>5,'id_topic'=>$item['id_topic']),true);
		unset($article['image']);
		return $article;
	}

	private function AssoDetails($item)
	{
		include_once(SERVER_ROOT."/../modules/assos.php");
		$asso = new Asso($item['id_ass']);
		$org = $this->AssoItem($item);
		if(isset($org['thumb']))
		{
			$image = array();
			$image['xname'] = "image";
			$image['id'] = $item['id_ass'];
			$img_sizes = $this->conf->Get("img_sizes");
			$image_size = $this->conf->Get("orgs_size");
			$image['width'] = $img_sizes[$image_size];
			$this->Urlify($image,"org_image",array('id'=>$item['id_ass'],'size'=>$image_size,'format'=>'jpg'),TRUE );
			$org['image'] = $image;
			if($asso->orgs_orig)
			{
				$image = array();
				$image['xname'] = "image_orig";
				$image['id'] = $item['id_ass'];
				include_once(SERVER_ROOT."/../classes/file.php");
				$fm = new FileManager();
				$filename = $this->irl->PathAbs("org_image_orig",array('id'=>$item['id_ass']));
				$img_orig_size = $fm->ImageSize($filename);
				$image['width'] = $img_orig_size['width'];
				$image['height'] = $img_orig_size['height'];
				$this->Urlify($image,"org_image_orig",array('id'=>$item['id_ass'],'size'=>$image_size,'format'=>'jpg'),TRUE );
				$org['image_orig'] = $image;			
			}
		}
		if($item['tel']!="")
			$org['phone'] = $item['tel'];
		if($item['fax']!="")
			$org['fax'] = $item['fax'];
		if($item['ccp']!="")
			$org['ccp'] = $item['ccp'];
		if($item['note']!="")
			$org['description'] = array('xvalue'=>$item['note']);
		if($item['referente']!="")
			$org['contact'] = $item['referente'];
		if($item['pubblicaz']!="")
			$org['publish'] = $item['pubblicaz'];
		$keywords = $asso->Keywords();
		$o_kword = array();
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$row = $asso->Get(true);
		$docs = array();
		$num_docs = $asso->Docs($docs,false);
		if($num_docs>0)
		{
			$odocs = array();
			foreach($docs as $doc)
				$odocs['d'.$doc['id_doc']] =$this->ArticleDocItem($doc,"org_doc");
			$org['docs'] = $odocs;
		}
		$v = new Varia();
		$params = $v->Deserialize($row['kparams']);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->LoadTree(ASSO_KEYWORD_TYPE);
		$kparams = $k->Params(0,ASSO_KEYWORD_TYPE,true);
		if(count($kparams)>0)
		{
			$kitem = $this->KeywordItem(array('id_keyword'=>"0",'name'=>""));
			foreach($kparams as $kparam)
			{
				$pkey = "kp_" . $kparam['id_keyword_param'];
				if(is_array($params) && array_key_exists($pkey,$params))
				{
					$kvalue = $this->ParamValue($kparam['type'],"",$params[$pkey]);
					$o_kparams['p_'.$pkey] = array('xname'=>"param",'id'=>$kparam['id_keyword_param'],'name'=>$kparam['label'],'type'=>$kparam['type'],'value'=>$kvalue);
				}
			}
			$kitem['params'] = $o_kparams;
			$o_kword['k_0'] = $kitem;
		}
		foreach($keywords as $keyword)
		{
			$kitem = $this->KeywordItem($keyword);
			$kpaths = array();
			$parents = $o->KeywordPath($keyword['id_keyword']);
			foreach($parents as $parent)
			{
				$pitem = $this->KeywordItem($parent);
				$this->Urlify($pitem,"orgs",array('id'=>$parent['id_keyword'],'subtype'=>"keyword"),TRUE);
				$kpaths['k_'.$parent['keyword']] = $pitem;
			}
			$kitem['path'] = $kpaths;
			$o_kparams = array();
			$kparams = $k->Params($keyword['id_keyword'],ASSO_KEYWORD_TYPE,true);
			foreach($kparams as $kparam)
			{
				$pkey = "kp_" . $kparam['id_keyword_param'];
				if(is_array($params) && array_key_exists($pkey,$params))
				{
					$kvalue = $this->ParamValue($kparam['type'],"",$params[$pkey]);
					$o_kparams['p_'.$pkey] = array('xname'=>"param",'id'=>$kparam['id_keyword_param'],'name'=>$kparam['label'],'type'=>$kparam['type'],'value'=>$kvalue);
				}
			}
			$kitem['params'] = $o_kparams;
			$o_kword['k_'.$keyword['id_keyword']] = $kitem;
		}
		$org['keywords'] =  $o_kword;
		return $org;
	}
	
	private function AssoItem($item,$name="asso")
	{
		$org = array();
		$org['xname'] = $name;
		$org['id'] = $item['id_ass'];
		$org['type'] = "asso";
		$org['name'] = $item['nome'];
		$org['name2'] = $item['nome2'];
		$org['address'] = $item['indirizzo'];
		$org['postcode'] = $item['cap'];
		$org['town'] = $item['citta'];
		$org['geo_name'] = $item['geo_name'];
		$org['asso_type'] = $item['ass_tipo'];
		if($item['email']!="")
			$org['email'] = $item['email'];
		if($item['sito']!="")
		    $org['website'] = strpos($item['sito'],'http')===false? 'http://' . $item['sito'] : $item['sito'];
		$this->Urlify($org,"orgs",array('id'=>$item['id_ass'],'subtype'=>"org"),TRUE);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($fm->Exists("uploads/orgs/0/{$item['id_ass']}.jpg"))
		{
			$thumb = array();
			$thumb['xname'] = "thumb";
			$thumb['id'] = $item['id_ass'];
			$img_sizes = $this->conf->Get("img_sizes");
			$thumb_size = $this->conf->Get("orgs_thumb");
			$thumb['width'] = $img_sizes[$thumb_size];
			$this->Urlify($thumb,"org_thumb",array('id'=>$item['id_ass'],'size'=>$thumb_size),TRUE );
			$org['thumb'] = $thumb;
		}
		$v = new Varia();
		$params = $v->Deserialize($item['kparams']);
		if(is_array($params) && count($params)>0)
		{
			include_once(SERVER_ROOT."/../modules/assos.php");	
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$kparams = $k->Params(0,ASSO_KEYWORD_TYPE,true,false,true);
			$o_kparams = array();
			if(count($kparams)>0)
			{
				foreach($kparams as $kparam)
				{
					$pkey = "kp_" . $kparam['id_keyword_param'];
					if(is_array($params) && array_key_exists($pkey,$params))
					{
						$kvalue = $this->ParamValue($kparam['type'],"",$params[$pkey]);
						if($kvalue!="")
							$o_kparams['p_'.$pkey] = array('xname'=>"param",'id'=>$kparam['id_keyword_param'],'name'=>$kparam['label'],'type'=>$kparam['type'],'value'=>$kvalue);
					}
				}
			}
			$asso = new Asso($item['id_ass']);
			$keywords = $asso->Keywords();
			foreach($keywords as $keyword)
			{
				$kparams = $k->Params($keyword['id_keyword'],ASSO_KEYWORD_TYPE,true,false,true);
				foreach($kparams as $kparam)
				{
					$pkey = "kp_" . $kparam['id_keyword_param'];
					if(is_array($params) && array_key_exists($pkey,$params))
					{
						$kvalue = $this->ParamValue($kparam['type'],"",$params[$pkey]);
						if($kvalue!="")
							$o_kparams['p_'.$pkey] = array('xname'=>"param",'id'=>$kparam['id_keyword_param'],'name'=>$kparam['label'],'type'=>$kparam['type'],'value'=>$kvalue);
					}
				}
			}
			$org['params'] = $o_kparams;		
		}
		return $org;
	}
	
	private function AssoSearch($params,$page)
	{
		include_once(SERVER_ROOT."/../modules/assos.php");
		$as = new Assos();
		$o_array =  array();
		$otypes = array();
		foreach($as->Types() as $type)
		{
			$otypes['t_' . $type['id_ass_tipo'] ] = array('xname'=>"type",'id'=>$type['id_ass_tipo'],'type'=>$type['ass_tipo']);
		}
		$o_array['types'] = $otypes;
		if ($params['from']=="org_search")
		{
			$search_terms = array('name'=>$params['name'],'town'=>$params['town'],'text'=>$params['text'],'address'=>$params['address'],'id_geo'=>$params['id_geo'],'id_assotype'=>$params['id_assotype'],'id_k'=>$params['id_k'],'advanced'=>$params['advanced']);
			$kparams = array();
			$stparams = array();
			foreach($params as $pkey=>$pvalue)
			{
				if (substr($pkey,0,3)=="kp_" && $pvalue!="")
				{
					$kparams[$pkey] = $pvalue;
					$stparams['kp'.$pkey] = array('xname'=>"kparam",'key'=>$pkey,'value'=>$pvalue);
				}
			}
			if(count($stparams)>0)
				$search_terms['kparams'] = $stparams;
			$o_array['search_terms'] = $search_terms;
			$orows = $as->SearchPub($params,$kparams);
			$items = $this->Pager($orows);
			$o_array['items'] = $items[$page-1];
		}
		return $o_array;
	}

	private function AssoWrapper($params,$page)
	{
		include_once(SERVER_ROOT."/../modules/assos.php");
		$as = new Assos();
		if ($this->subtype=="" )
			$this->subtype = "home";
		if($params['advanced']==1)
			$this->subtype = "search_advanced";
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$this->TopicCommon());
		$orgs = array('path'=>$as->org_path);
		$orgs['access_protected'] = $as->orgs_access_protected? "1":"0";
		if($as->orgs_access_protected)
		{
			$id_p = (int)$pagetype['topic']['user']['id'];
			if(!$id_p>0)
			{
				$orgs['login'] = $this->LoginFirst($as->id_topic);
			}
		}
		if($as->orgs_upload)
		{
			$orgs['upload'] = $as->orgs_upload;
			$maxfilesize = $this->ini->Get('max_file_size');
			$orgs['max_file_size'] = $maxfilesize;
			$orgs['max_file_size_kb'] = floor($maxfilesize/1024);
		}
		$this->Urlify($orgs,"orgs",array('id_topic'=>$this->topic->id),true);
		$search_params = array();
		$this->Urlify($search_params,"orgs",array('subtype'=>"search",'id_topic'=>$this->topic->id,'from'=>$params['from'],'search_terms'=>$params),true);
		$orgs['search'] = $search_params;
		$search_adv_params = array('label'=>$this->tr->Translate("search_advanced"));
		$this->Urlify($search_adv_params,"orgs",array('subtype'=>"search_advanced",'id_topic'=>$this->topic->id),true);
		$orgs['search_advanced'] = $search_adv_params;
		$insert_params = array();
		$this->Urlify($insert_params,"orgs",array('subtype'=>"insert",'id_topic'=>$this->topic->id),true);
		$orgs['insert'] = $insert_params;
		$type_params = array();
		$this->Urlify($type_params,"orgs",array('subtype'=>"type",'id_topic'=>$this->topic->id),true);
		$orgs['type'] = $type_params;
		$kw_params = array();
		$this->Urlify($kw_params,"orgs",array('subtype'=>"keyword",'id_topic'=>$this->topic->id,'id'=>0),true);
		$orgs['keywords'] = $kw_params;
		switch($this->subtype)
		{
			case "home":
			break;
			case "org":
				if($params['id']>0)
				{
					$asso = new Asso($params['id']);
					$row = $asso->Get(true);
					$orgs['org'] = $this->AssoDetails($row);
				}
			break;
			case "keyword":
				include_once(SERVER_ROOT."/../classes/ontology.php");
				$o = new Ontology;
				$o->LoadTree(ASSO_KEYWORD_TYPE);
				include_once(SERVER_ROOT."/../classes/keyword.php");
				$k = new Keyword();
				$row = $k->KeywordGet($params['id']);
				$kitem = $this->KeywordItem($row);
				if($params['id']>0)
				{
					$kpaths = array();
					$parents = $o->KeywordPath($params['id']);
					foreach($parents as $parent)
					{
						$pitem = $this->KeywordItem($parent);
						$this->Urlify($pitem,"orgs",array('id'=>$parent['id_keyword'],'subtype'=>"keyword"),TRUE);
						$kpaths['k_'.$parent['id_keyword']] = $pitem;
					}
					$kitem['path'] = $kpaths;				
				}
				$kchildren = array();
				$children = $o->KeywordChildren($params['id']);
				foreach($children as $child)
				{
					$citem = $this->KeywordItem($child);
					$citem['orgs_count'] = $as->KeywordCount($child['id_keyword']);
					$this->Urlify($citem,"orgs",array('id'=>$child['id_keyword'],'subtype'=>"keyword"),TRUE);
					$kchildren['k_'.$child['id_keyword']] = $citem;
				}
				$kitem['children'] = $kchildren;
				$uses = array();
				$num_orgs = $as->KeywordUse($uses,$params['id'],true);
				$orgs_paged = $this->Page($uses,$num_orgs);
				$kitem['items'] = $orgs_paged;
				$this->Urlify($kitem,"orgs",array('id'=>$params['id'],'subtype'=>"keyword"),true);
				$orgs['keyword'] = $kitem;
			break;
			case "type":
				$types = $as->Types();
				$otypes = array();
				foreach($types as $type)
				{
					$otype = array('xname'=>"type",'id'=>$type['id_ass_tipo'],'name'=>$type['ass_tipo']);
					$this->Urlify($otype,"orgs",array('from'=>"org_search",'search_terms'=>array('id_assotype'=>$type['id_ass_tipo']),'subtype'=>"search"),TRUE);
					$otypes['t_'.$type['id_ass_tipo']] = $otype;
				}
				$orgs['types'] = $otypes;
			break;
			case "search":
			case "search_advanced":
                $pagetype['geo'] = $this->Geo($as->geo_location);
				$orgs = array_merge($orgs,$this->AssoSearch($params,$page));
				include_once(SERVER_ROOT."/../classes/ontology.php");
				$this->ontology = new Ontology;
				$this->ontology->LoadTree(ASSO_KEYWORD_TYPE);
				$keywords = $this->AssoKeywordsRecurse(0);
				$orgs['keywords'] = $keywords;
				if($this->subtype=="search_advanced")
				{
					$kparams = array();
					$ksparams = $as->Params(false,true);
					foreach($ksparams as $ksparam)
					{
						$kparam = array('xname'=>"kparam");
						$kparam['id'] = $ksparam['id_keyword_param'];
						$kparam['label'] = $ksparam['label'];
						$kparam['type'] = $ksparam['type'];
						if($kparam['type']=="dropdown" || $kparam['type']=="mchoice" || $kparam['type']=="dropdown_open" || $kparam['type']=="mchoice_open" || $kparam['type']=="radio" )
						{
							$pvalues = explode(",",$ksparam['params']);
							$subparams = array();
							$counter = 1;
							foreach($pvalues as $pvalue)
							{
								$subparams['sp'.$counter] = array('xname'=>"subparam",'pos'=>$counter,'value'=>trim($pvalue));
								$counter ++;
							}
							$kparam['subparams'] = $subparams;
						}
						$kparams['kp_'.$ksparam['id_keyword_param']] = $kparam;
					}
					$orgs['kparams'] = $kparams;
				}
			break;
			case "insert":
				include_once(SERVER_ROOT."/../classes/ontology.php");
				$this->ontology = new Ontology;
				$this->ontology->LoadTree(ASSO_KEYWORD_TYPE);
				if($params['id']>0)
				{
					$asso = new Asso($params['id']);
					$row = $asso->Get(false);
					$orgs['org'] = $this->AssoItem($row);
					if($params['kparams']>0)
					{
						$o_kword = array('id'=>$params['id']);
						$keywords = $asso->Keywords();
						include_once(SERVER_ROOT."/../classes/keyword.php");
						$k = new Keyword();
						foreach($keywords as $keyword)
						{
							$kitem = $this->KeywordItem($keyword);
							$kpaths = array();
							$parents = $this->ontology->KeywordPath($keyword['id_keyword']);
							foreach($parents as $parent)
							{
								$pitem = $this->KeywordItem($parent);
								$this->Urlify($pitem,"orgs",array('id'=>$parent['id_keyword'],'subtype'=>"keyword"),TRUE);
								$kpaths['k_'.$parent['keyword']] = $pitem;
							}
							$kitem['path'] = $kpaths;
							$o_kparams = array();
							$kparams = $k->Params($keyword['id_keyword'],ASSO_KEYWORD_TYPE);
							foreach($kparams as $kparam)
							{
								$pkey = "kp_" . $kparam['id_keyword_param'];
								$o_kparams['p_'.$pkey] = $this->KeywordCustomParam($kparam);
							}
							$kitem['params'] = $o_kparams;
							$o_kword['k_'.$keyword['id_keyword']] = $kitem;
						}
						$orgs['keywords'] =  $o_kword;
					}
					else
					{
						$keywords = $this->AssoKeywordsRecurse(0);
						$orgs['keywords'] = $keywords;
					}
				}
				else
				{
					$pagetype['geo'] = $this->Geo($as->geo_location);
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$kparams = $k->Params(0,ASSO_KEYWORD_TYPE,true);
					if(count($kparams)>0)
					{
						$kitem = $this->KeywordItem(array('id_keyword'=>"0",'name'=>""));
						foreach($kparams as $kparam)
						{
							$pkey = "kp_" . $kparam['id_keyword_param'];
							$o_kparams['p_'.$pkey] = $this->KeywordCustomParam($kparam);
						}
						$kitem['params'] = $o_kparams;
						$orgs['k_0'] = $kitem;
					}
					$orgs = array_merge($orgs,$this->AssoSearch($params,$page));
				}
			break;
		}
		$pagetype['orgs'] = $orgs;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_topic'=>($as->id_topic)));
		return $pagetype;
	}
	
	private function KeywordCustomParam($kparam)
	{
		$kparray = array('xname'=>"param",'id'=>$kparam['id_keyword_param'],'label'=>$kparam['label'],'type'=>$kparam['type']);
		if($kparam['type']=="dropdown" || $kparam['type']=="mchoice" || $kparam['type']=="dropdown_open" || $kparam['type']=="mchoice_open" || $kparam['type']=="radio")
		{
			$pvalues = explode(",",$kparam['params']);
			$subparams = array();
			$counter = 1;
			foreach($pvalues as $pvalue)
			{
				$subparams['sp'.$counter] = array('xname'=>"subparam",'pos'=>$counter,'value'=>trim($pvalue));
				$counter ++;
			}
			$kparray['subparams'] = $subparams;
		}
		if($kparam['type']=="geo")
		{
			$kparray['geo'] = $this->Geo($kparam['params']);
		}
		if($kparam['type']=="date")
		{
			$ts = time();
			$kparray['dom'] = $this->dt->DayOfMonth($ts);
			$kparray['month'] = $this->dt->MonthNumber($ts);
			$kparray['month_name'] = $this->dt->Month($ts);
			$kparray['year'] = $this->dt->Year($ts);
			$months = array();
			$counter = 1;
			foreach($this->tr->Translate("month") as $month)
			{
				$months['m_' . $counter] = array('xname'=>"month",'n'=>$counter,'name'=>$month);
				$counter ++;
			}
			$kparray['months'] = $months;
		}
		return $kparray;
	}
	
	private function AssoKeywordsRecurse($id_keyword)
	{
		$keywords = array();
		$children = $this->ontology->KeywordChildren($id_keyword);
		foreach($children as $child)
		{
			$citem = $this->KeywordItem($child);
			$citem['keywords'] = $this->AssoKeywordsRecurse($child['id_keyword']);
			$keywords['k_'.$child['id_keyword']] = $citem;
		}
		return $keywords;
	}
	
	private function AudioDetails($item,$with_embed=false)
	{
		$audio = $this->AudioItem($item);
		$audio['views'] = $item['views'];
		$audio['auto_start'] = $item['auto_start'];
		$audio['hash'] = $item['hash'];
		$audio_enc = array();
			$this->Urlify($audio_enc,"audio_enc_link",array('hash'=>$item['hash'],'id'=>$item['id_audio']),true);
		$audio['enc'] = $audio_enc;
		$audio_xml = array();
			$this->Urlify($audio_xml,"audio_xml",array('hash'=>$item['hash']),true);
		$audio['xml'] = $audio_xml;
		$audio['download'] = ($item['download'])? "1":"0";
		if($item['link']!="")
			$audio['link'] = $item['link'];
		if($item['source']!="")
			$audio['source'] = array('xvalue'=>$this->Htmlise($item['source'],false));
		if($item['description']!="")
			$audio['description'] = array('xvalue'=>$this->Htmlise($item['description'],false));
		if ($this->ini->Get("licences") && $item['id_licence'] > 0)
			$audio['licence'] = $this->Licence($item['id_licence'],$item['author']);
		return $audio;
	}
	
	private function AudioItem($item,$name="audio")
	{
		$aud = array();
		$aud['xname'] = $name;
		if ($name!="audio")
			$aud['type'] = "audio";
		$aud['id'] = $item['id_audio'];
		$aud['title'] = $item['title'];
		$aud['date'] = $this->FormatDate($item['insert_date_ts']);
		if($item['length']>0)
		{
			$aud['length'] = $this->FormatMinutes($item['length']);
			$aud['seconds'] = $item['length'];
		}
		if($item['author']!="")
			$aud['author'] = $item['author'];
		$this->Urlify($aud,"media",array('subtype'=>"audio",'hash'=>$item['hash'],'id_topic'=>$this->topic->id),true);
		return $aud;
	}
	
	public function BannerWrapper($id_banner,$id_group,$is_async=false,$id_div="",$json=false)
	{
		include_once(SERVER_ROOT."/../classes/banners.php");
		$b = new Banners;
		$banner = $b->MemGet($id_banner,$id_group,$this->conf->Get("caching")=="apc");
		if($json) {
			if ($banner['id_banner']>0) {
				header('Content-Type: application/json');
				foreach ($banner as $key => $value) {
					if (is_int($key)) {
						unset($banner[$key]);
					}
				}
				$banner['src'] = "{$this->pub_web}/banners/{$banner['id_banner']}.{$banner['format']}";
				$banner['real_link'] = $banner['link'];
				$banner['link'] = "$this->pub_web/tools/click.php?id={$banner['id_banner']}";
							
			} else {
				$banner = array('error'=>true);
			}
			return $this->JsonEncode($banner);
		} else {
			$well_formed = substr($this->doctype,0,5)!="html4"  || $is_async;
			if ($banner['id_banner']>0)
			{
				$ban = "<a rel=\"nofollow\" href=\"$this->pub_web/tools/click.php?id={$banner['id_banner']}\"";
				if($banner['popup'])
					$ban .= " target=\"_blank\" ";
				$ban .= " title=\"" . strip_tags(htmlspecialchars($banner['alt_text'])) . "\">";
				$filename = "$this->pub_web/banners/{$banner['id_banner']}.{$banner['format']}";
				if ($banner['height']>0)
					$height_attr = " height=\"{$banner['height']}\"";
				if ($banner['width']>0)
					$width_attr = " width=\"{$banner['width']}\"";
				$ban .= "<img src=\"$filename\" border=\"0\" $width_attr $height_attr alt=\"" . strip_tags(htmlspecialchars($banner['alt_text'])) . "\" " . ($well_formed? "/>":">");
				$ban .= "</a>";
			}
			else
				$ban = "<img src=\"$this->pub_web/logos/error.gif\" width=\"1\" height=\"1\" border=\"0\" alt=\"error\" " . ($well_formed? "/>":">");
			return $this->Javascript($ban,true,$is_async,$id_div);
		}
	}
	
	private function Book($id_book)
	{
		include_once(SERVER_ROOT."/../modules/books.php");
		$b = new Book($id_book);
		$row = $b->BookGet();
		$book = $this->BooksPublisher($row['id_publisher'],$row['id_category']);
		if ($row['approved'])
		{
			$bk = $this->BookItem($row);
			$bk['description'] = array('xvalue'=>$this->Htmlise($row['description']));
			$bk['isbn'] = $row['isbn'];
			$bk['pages'] = $row['pages'];
			$bk['series'] = $row['series'];
			$bk['lang_code'] = $this->tr->languages[$row['id_language']];
			$languages = $this->tr->Translate("languages");
			$bk['lang'] = $languages[$row['id_language']];
			$bk['notes'] = array('xvalue'=>$row['notes']);
			if ($row['id_article']>0)
			{
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($row['id_article']);
				$bk['article'] = $this->ArticleItem($a->ArticleGet());
			}
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			if($fm->Exists("uploads/covers/" . $this->conf->Get("covers_thumb") . "/" . $row['id_book'] . "." . $this->conf->Get("convert_format")))
			{
				$image = array();
				$image['xname'] = "image";
				$image['id'] = $row['id_book'];
				$img_sizes = $this->conf->Get("img_sizes");
				$image['width'] = $img_sizes[$this->conf->Get("covers_size")];
				$this->Urlify($image,"cover",array('id'=>$row['id_book'],'format'=>$row['cover_format'],'size'=>($this->conf->Get("covers_size"))),TRUE);
				$bk['image'] = $image;
			}
			$bk['keywords'] = $this->Keywords($id_book,"book");
			
			$bb = new Books();
			$bconfig = $bb->TopicConfig($this->topic->id);
			$force_topic = $bconfig['show_reviews']=="2";
			$force_language = ($bconfig['show_reviews']=="3")? $this->topic->id_language : 0;
			
			$reviews_i = array();
			$num1 = $b->Reviews($reviews_i,$this->subtype!="reviews",true,$this->topic->id,$force_topic,$force_language,false);
			$reviews = array();
			$this->Urlify($reviews,"books",array('id'=>$id_book,'subtype'=>"reviews",'id_topic'=>$this->topic->id),TRUE);
			$reviews['tot_imp_reviews'] = $num1;
			foreach($reviews_i as $review)
				$reviews['r'.$review['id_review']] = $this->BookReviewItem($review);
			$reviews_a = array();
			$reviews['tot_reviews'] = $b->Reviews($reviews_a,false,true,$this->topic->id,$force_topic,$force_language,false);
			$review_insert = array();
			$this->Urlify($review_insert,"books",array('id'=>$id_book,'subtype'=>"review_insert",'id_topic'=>$this->topic->id),TRUE);
			$id_topic = ($this->topic->profiling)? $this->topic->id : 0;
			$review_insert['login'] = $this->LoginFirst($id_topic);
			$reviews['insert'] = $review_insert;
			if($this->subtype=="review_insert")
			{
				include_once(SERVER_ROOT."/../classes/resources.php");
				$r = new Resources();
				$rparams = $r->Params("review",true);
				if(count($rparams)>0)
				{
					$params = array();
					foreach($rparams as $rparam)
					{
						$label = $this->tr->TranslateTry($rparam['label']);
						$parray = array('xname'=>"param",'id'=>$rparam['id_resource_param'],'label'=>$label,'type'=>$rparam['type']);
						if($rparam['type']=="dropdown" || $rparam['type']=="mchoice" || $rparam['type']=="dropdown_open" || $rparam['type']=="mchoice_open" || $rparam['type']=="radio" )
						{
							$pvalues = explode(",",$rparam['params']);
							$subparams = array();
							$counter = 1;
							foreach($pvalues as $pvalue)
							{
								$subparams['sp'.$counter] = array('xname'=>"subparam",'pos'=>$counter,'value'=>trim($pvalue));
								$counter ++;
							}
							$parray['subparams'] = $subparams;
						}
						$params['r_'.$rparam['id_resource_param']] = $parray;
					}
					$reviews['params'] = $params;
				}
			}
			$bk['reviews'] = $reviews;
		}
		$book['book'] = $bk;
		return $book;
	}
	
	private function BookReviewItem($item, $name="review")
	{
		$review = array();
		$review['xname'] = $name;
		if ($name!="review")
			$review['type'] = "review";
		$review['id'] = $item['id_review'];
		$review['name'] = $item['name'];
		$review['vote'] = $item['vote'];
		$review['date'] = $this->FormatDate($item['insert_date_ts']);
		$review['content'] = array('xvalue'=>$item['review']);
		
		$v = new Varia();
		$params = $v->Deserialize($item['rparams']);

		$o_rparams = array();
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		$rparams = $r->Params("review",true);
		foreach($rparams as $rparam)
		{
			$pkey = "rp_" . $rparam['id_resource_param'];
			if(is_array($params) && array_key_exists($pkey,$params))
			{
				$pvalue = $this->ParamValue($rparam['type'],"",$params[$pkey]);
				$o_rparams['p_'.$pkey] = array('xname'=>"param",'id'=>$rparam['id_resource_param'],'name'=>$rparam['label'],'type'=>$rparam['type'],'value'=>$pvalue);
			}
		}
		$review['params'] = $o_rparams;
		
		return $review;
		
	}
	
	private function BookReviewItemWithBook($item, $name="review")
	{
		$review =$this->BookReviewItem($item,$name);
		$book = $this->BookItemSmall($item);
		if($item['hdate_ts']>0)
			$review['hdate'] = $this->FormatDate($item['hdate_ts']);
		$this->Urlify($book,"books",array('id'=>$item['id_book'],'id_review'=>$item['id_review'],'subtype'=>"reviews",'id_topic'=>$this->topic->id),TRUE);
		unset($book['thumb']);
		$review['book'] = $book;
		return $review;
	}
	
	private function BooksCategory($id_category,$id_publisher,$page)
	{
		$cat = $this->BooksPublisher($id_publisher,$id_category);
		include_once(SERVER_ROOT."/../modules/books.php");
		$bb = new Books();
		$books = $this->Pager($bb->BooksAll(array('id_publisher'=>$id_publisher,'id_category'=>$id_category)));
		$cat['books'] = $books[$page-1];
		return $cat;
	}
	
	private function BookCategoryItem( $item,$name = "category" )
	{
		$bc = array();
		$bc['xname'] = $name;
		if ($name!="category")
			$bc['type'] = "category";
		$bc['id'] = $item['id_category'];
		$bc['name'] = $item['name'];
		$bc['id_publisher'] = $item['id_publisher'];
		$bc['id_image'] = $item['id_image'];
		$bc['description'] = array('xvalue'=>$item['description']);
		$bc['is_selected'] = ($item['selected'])? 1:0;
		$bc['books'] = $item['books'];
		$this->Urlify($bc,"books",array('id'=>$item['id_category'],'id_topic'=>$this->topic->id,'id_publisher'=>$item['id_publisher'],'subtype'=>"category"),TRUE);
		return $bc;
	}
		
	private function BookItem($item,$name="book")
	{
		$book = $this->BookItemSmall($item,$name);
		$book['price'] = $item['price'];
		$book['price_format'] = $this->PriceFormat($item['price']);
		if($item['id_publisher']>0)
			$book = array_merge($book,$this->BooksPublisher($item['id_publisher'],$item['id_category'],false));
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($fm->Exists("uploads/covers/" . $this->conf->Get("covers_thumb") . "/" . $item['id_book'] . "." . $this->conf->Get("convert_format")))
		{
			$thumb = array();
			$thumb['xname'] = "thumb";
			$thumb['id'] = $item['id_book'];
			$img_sizes = $this->conf->Get("img_sizes");
			$thumb['width'] = $img_sizes[$this->conf->Get("covers_thumb")];
			$this->Urlify($thumb,"cover",array('id'=>$item['id_book'],'format'=>$item['cover_format'],'size'=>($this->conf->Get("covers_thumb"))),TRUE );
			$book['thumb'] = $thumb;
		}
		return $book;
	}

	private function BookItemBig($item,$name="book")
	{
		$book = $this->BookItem($item,$name);
		$book['description'] = array('xvalue'=>$item['description']);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($fm->Exists("uploads/covers/" . $this->conf->Get("covers_thumb") . "/" . $item['id_book'] . "." . $this->conf->Get("convert_format")))
		{
			$image = array();
			$image['xname'] = "image";
			$image['id'] = $item['id_book'];
			$this->Urlify($image,"cover",array('id'=>$item['id_book'],'format'=>$item['cover_format'],'size'=>($this->conf->Get("covers_size"))),TRUE);
			$book['image'] = $image;
		}
		return $book;
	}
	
	private function BookItemSmall($item,$name="book")
	{
		$book = array();
		$book['xname'] = $name;
		$book['id'] = $item['id_book'];
		$book['type'] = "book";
		$book['author'] = $item['author'];
		$book['title'] = $item['title'];
		$book['catalog'] = $item['catalog'];
		if($item['p_year']>0)
			$book['year'] = $item['p_year'];
		$book['summary'] = array('xvalue'=>$this->Htmlise($item['summary'],false));
		$months = $this->tr->Translate("month");
		$book['month'] = $months[$item['p_month']-1];
		$this->Urlify($book,"books",array('id'=>$item['id_book'],'id_topic'=>$this->topic->id,'subtype'=>"book"),TRUE);
		return $book;
	}

	private function BookSearch($unescaped_query)
	{
		$unescaped_query = $this->th->UtfClean($unescaped_query);
		$strlen = $this->th->StringLength($unescaped_query);
		$min_str_length = $this->conf->Get("min_str_length");
		$search = array();
		if ($strlen>=$min_str_length)
		{
			$search  = array('q'=>$unescaped_query);
			include_once(SERVER_ROOT."/../modules/books.php");
			$bb = new Books();
			$books = array();
			$search_words = $this->th->SearchWords($unescaped_query,$this->id_language);
			$num_books = $bb->SearchPub($books,$search_words);
			$books_paged = $this->Page($books,$num_books);
			$search['books'] = $books_paged;
			$this->Urlify($search,"books",array('q'=>$unescaped_query,'subtype'=>"search",'id_topic'=>$this->topic->id),true);
		}
		return $search;
	}

	private function BooksHome()
	{
		$bhome = array();
		$items = array();
		include_once(SERVER_ROOT."/../modules/books.php");
		$bb = new Books();
		$bconfig = $bb->TopicConfig($this->topic->id);
		$books_home_type = $bconfig['books_home_type'];
		$bb->HomepageContent($items,$this->topic->id,$books_home_type,false);
		switch($books_home_type)
		{
			case "0":
				$publishers = array();
				foreach($items as $item)
					$publishers['p_' . $item['id_publisher']] = $this->BooksPublisherItem($item);
				$bhome = $publishers;
			break;
			case "1":
				$id_publisher = $items[0]['id_item'];
				if ($id_publisher>0)
					$bhome = $this->BooksPublisher($id_publisher);
			break;
			case "2":
				$books = array();
				foreach($items as $item)
					$books['b_' . $item['id_book']] = $this->BookItem($item,"item");
				$bhome = $books;
			break;
		}
		return $bhome;
	}
	
	private function BooksPublisher($id_publisher,$id_category=0,$with_categories=true)
	{
		if(!$id_publisher>0)
			UserError("Missing publisher",array());
		include_once(SERVER_ROOT."/../modules/books.php");
		$p = new Publisher;
		$row = $p->PublisherGet($id_publisher);
		$publisher = $this->BooksPublisherItem($row);
		if($with_categories)
		{
			$categories = array();
			$rows2 = $p->CategoriesAll($id_publisher);
			foreach($rows2 as $row2)
			{
				if($row2['id_category']==$id_category)
					$row2['selected'] = TRUE;
				$categories['c_' . $row2['id_category']] = $this->BookCategoryItem($row2);
			}
			$publisher['categories'] = $categories;
		}
		else
		{
			$category = $p->CategoryGet($id_category,$id_publisher);
			$publisher['category'] = $this->BookCategoryItem($category);
		}
		$pub['publisher'] = $publisher;
		return $pub;
	}
	
	private function BooksPublisherItem($item)
	{
		$pub = array();
		$pub['xname'] = "publisher";
		$pub['id'] = $item['id_publisher'];
		$pub['name'] = $item['name'];
		$pub['address'] = $item['address'];
		$pub['town'] = $item['town'];
		$pub['website'] = $item['website'];
		$pub['email'] = $item['email'];
		$pub['phone'] = $item['phone'];
		$pub['fax'] = $item['fax'];
		$pub['zipcode'] = $item['zipcode'];
		$pub['id_prov'] = $item['id_prov'];
		$pub['notes'] = array('xvalue'=>$item['notes']);
		$this->Urlify($pub,"books",array('id'=>$item['id_publisher'],'id_topic'=>$this->topic->id,'subtype'=>"publisher"),TRUE);
		return $pub;
	}
	
	private function BooksWrapper($params,$page)
	{
		if ($this->subtype=="" )
		{
			if ($params['subtype']!="")
				$this->subtype = $params['subtype'];
			else
			{
				$this->subtype = "home";
				if ($params['id_publisher']>0)
					$this->subtype = "publisher";
				if ($params['id_category']>0)
					$this->subtype = "category";
				if ($params['id']>0)
					$this->subtype = "book";
			}
		}
		$topic = ($this->topic->id>0)? $this->TopicCommon() : array();
		$books = array();
		$search_params = array();
		$this->Urlify($search_params,"books",array('subtype'=>"search",'id_topic'=>$this->topic->id),true);
		switch($this->subtype)
		{
			case "home":
				$books = $this->BooksHome();
			break;
			case "publisher":
				$books = $this->BooksPublisher($params['id_publisher']);
			break;
			case "category":
				$books = $this->BooksCategory($params['id_category'],$params['id_publisher'],$page);
			break;
			case "book":
			case "reviews":
			case "review_insert":
				$books = $this->Book($params['id']);
			break;
			case "search":
				$books['search'] = array_merge($search_params,$this->BookSearch($params['q']));
			break;
		}
		if($this->subtype!="search")
			$books['search'] = $search_params;
		include_once(SERVER_ROOT."/../modules/books.php");
		$bb = new Books();
		$bconfig = $bb->TopicConfig($this->topic->id);
		$books['hometype'] = $bconfig['books_home_type'];
		$books['show_reviews'] = $bconfig['show_reviews'];
		$books['allow_reviews'] = $bconfig['books_reviews'];
		$this->Urlify($books,"books",array('subtype'=>"home",'id_topic'=>$this->topic->id),true);
		$pagetype['books'] = $books;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_book'=>$params['id'],'id_topic'=>$this->topic->id));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function CampaignItem($item,$name="campaign")
	{
		$id_campaign = $item['id_topic_campaign'];
		$camp = array();
		$camp['xname'] = $name;
		$camp['id'] = $item['id_topic_campaign'];
		$camp['type'] = "campaign";
		$camp['title'] = $item['name'];
		$camp['active'] = $item['active'];
		$camp['funding'] = $item['money'];
		$camp['promoter'] = $item['promoter'];
		if($item['hdate_ts']>0)
			$camp['hdate'] = $this->FormatDate($item['hdate_ts']);
		if($item['id_person']>0)
			$camp['id_person'] = $item['id_person'];
		$camp['start_date'] = $this->FormatDate($item['start_date_ts']);
		if($item['topic_name']!="")
			$camp['topic'] = $item['topic_name'];
		$camp['description'] = array('xvalue'=>$item['description']);
		$camp['orgs_sign'] = $item['orgs_sign'];
		$this->Urlify($camp,"campaign",array('id'=>$id_campaign,'id_topic'=>$item['id_topic']),true);
		$sign_person = array();
		$this->Urlify($sign_person,"campaign",array('id'=>$id_campaign,'id_topic'=>$item['id_topic'],'subtype'=>"sign_person"),true);
		$signatures_person = array();
		$this->Urlify($signatures_person,"campaign",array('id'=>$id_campaign,'id_topic'=>$item['id_topic'],'subtype'=>"signatures_person"),true);
		$camp['sign'] = array('person'=>$sign_person);
		$camp['signatures'] = array('person'=>$signatures_person);
		if (isset($item['persons']))
			$camp['stats'] = array('persons'=>$item['persons']);
		if($item['orgs_sign']>0)
		{
			$sign_org = array();
			$this->Urlify($sign_org,"campaign",array('id'=>$id_campaign,'id_topic'=>$item['id_topic'],'subtype'=>"sign_org"),true);
			$camp['sign']['org'] = $sign_org;
			$signatures_org = array();
			$this->Urlify($signatures_org,"campaign",array('id'=>$id_campaign,'id_topic'=>$item['id_topic'],'subtype'=>"signatures_org"),true);
			$camp['signatures']['org'] = $signatures_org;
			if (isset($item['persons']))
				$camp['stats']['orgs'] = $item['orgs'];
		}
		return $camp;
	}
	
	private function CampaignSign($id_campaign)
	{
		include_once(SERVER_ROOT."/../classes/campaign.php");
		$c = new Campaign($id_campaign);
		$row = $c->CampaignGet();
		$campaign = $this->CampaignItem($row);
		$user = $this->PeopleUser();
		$trm4 = new Translator($this->id_language,4,false,$this->topic->id_style);
		if($row['users_type']=="0" && !$user['id']>0)
		{
			$campaign['login'] = $this->LoginFirst($row['id_topic']);
		}
		else
		{
			$campaign['contact_warning'] = array('xvalue'=>$this->tr->Translate("contact_warning"));
			$campaign['submit_warning'] = array('xvalue'=>$this->tr->Translate("campaign_submit_warning"));
			$campaign['submit'] = $this->pub_web . "/" . $this->ini->Get("campaign_path") . "/actions.php";
			$privacy_text = $this->PrivacyText();
			$campaign['privacy_warning'] = array('xvalue'=>$privacy_text);
			$campaign['pre_sign'] = array('xvalue'=>$row['pre_sign']);
			if($row['notify']!="")
			{
				$campaign['notify'] = $trm4->TranslateParams("notify_notification",array($row['notify']));
			}
			if($row['money'])
				$campaign['funding'] = $this->Funding(1,$id_campaign,$row['amount'],$row['editable']);		
		}
		return $campaign;
	}
	
	private function CampaignWrapper($params)
	{
		$topic = ($params['id_topic']>0)? $topic = $this->TopicCommon() : array();
		if ($this->subtype=="" )
			$this->subtype = ($params['subtype']!="")? $params['subtype'] : (($params['id']>0)?"info":"index");
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		switch($this->subtype)
		{
			case "index":
				$campaigns = array();
				$this->Urlify($campaigns,"campaign",array('id_topic'=>$params['id_topic']),true);
				include_once(SERVER_ROOT."/../classes/campaigns.php");
				$rows = array();
				$num = Campaigns::ActiveCampaigns($rows,$params['id_topic']);
				$campaigns['items'] = $this->Page($rows,$num);
				$pagetype['campaigns'] = $campaigns;
				$campaigns_open = $this->topic->row['campaigns_open'];
				if($campaigns_open!="0")
				{
					$insert = array('label'=>$this->tr->Translate("campaign_new"));
					$this->Urlify($insert,"campaign",array('id_topic'=>$params['id_topic'],'subtype'=>"insert"),true);
					$pagetype['campaigns_insert'] = $insert;
				}
			break;
			case "info":
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($params['id']);
				$row = $c->CampaignGet();
				$row['persons'] = $c->PersonsApproveCounter( 1 );
				$num_orgs = 0;
				if($row['orgs_sign'])
				{
					$orgs = array();
					$num_orgs = $c->OrgsApprove( $orgs, 1 );
				}
				$row['orgs'] = $num_orgs;
				$pagetype['campaign'] = $this->CampaignItem($row);
				if ($params['thanks']>0)
				{
					$pagetype['campaign']['thanks'] = array('xvalue'=>$row['thanks']);
					$payment = $c->DonationPending();
					if($row['money'] && $payment['id_payment']>0)
					{
						$pagetype['campaign']['payment'] = $this->PaymentInfo($payment,$this->tr->Translate("campaign") . " " . $row['name']);
					}
					if($row['notify']!="")
					{
						$trm4 = new Translator($this->id_language,4,false,$this->topic->id_style);
						$pagetype['campaign']['notify'] = $trm4->TranslateParams("notification_sent",array($row['notify']));
					}
				}
				else 
				{
					$pagetype['campaign']['description_long'] = array('xvalue'=>($row['is_html']?$row['description_long']:$this->Htmlise($row['description_long']))  );
				}
				if ($params['ca']=="0")
					$pagetype['campaign']['comment'] = array('xvalue'=>$this->tr->Translate("comment_approval"));
				$vips = $c->Vips();
				if(count($vips)>0)
				{
					$trm4 = new Translator($this->id_language,4,false,$this->topic->id_style);
					$cvips = array('label'=>$trm4->Translate("vips"));
					foreach($vips as $vip)
						$cvips['p_'.$vip['id_p']] = array('xname'=>"person",'id'=>$vip['id_p'],'name'=>$vip['name']);
					$pagetype['campaign']['vips'] = $cvips;
				}
			break;
			case "sign_person":
			case "sign_org":
				$pagetype['campaign'] = $this->CampaignSign($params['id']);
				$pagetype['geo'] = $this->Geo();
			break;
			case "signatures_person":
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($params['id']);
				$row = $c->CampaignGet();
				$pagetype['campaign'] = $this->CampaignItem($row);
				if($row['signatures_per_page']>0)
				{
					$records_per_page = $this->records_per_page;
					$this->RecordsPerPage($row['signatures_per_page']);
				}
				$rows = array();
				$num = $c->PersonsApprove( $rows, 1 );
				$pagetype['campaign']['items'] = $this->Page($rows,$num,$row['signatures_per_page']);
				if($row['signatures_per_page']>0)
				{
					$this->RecordsPerPage($records_per_page);
				}
			break;
			case "signatures_org":
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($params['id']);
				$row = $c->CampaignGet();
				$pagetype['campaign'] = $this->CampaignItem($row);
				if($row['signatures_per_page']>0)
				{
					$records_per_page = $this->records_per_page;
					$this->RecordsPerPage($row['signatures_per_page']);
				}
				$rows = array();
				$num = $c->OrgsApprove( $rows, 1 );
				$pagetype['campaign']['items'] = $this->Page($rows,$num,$row['signatures_per_page']);
				if($row['signatures_per_page']>0)
				{
					$this->RecordsPerPage($records_per_page);
				}
			break;
			case "insert":
				$campaigns_open = $this->topic->row['campaigns_open'];
				$insert = array('label'=>$this->tr->Translate("campaign_new"),'users'=>$campaigns_open);
				$insert['submit'] = $this->pub_web . "/" . $this->ini->Get("campaign_path") . "/actions.php";
				$privacy_text = $this->PrivacyText();
				$insert['privacy_warning'] = array('xvalue'=>$privacy_text);
				if($campaigns_open=="1")
				{
					$insert['login'] = $this->LoginFirst($this->topic->id);
				}
				$pagetype['campaign_insert'] = $insert;
			break;
		}
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_campaign'=>$params['id'],'id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function CommentItem( $item, $name="comment" )
	{
		$comment = array();
		$comment['xname'] = $name;
		$comment['type'] = "comment";
		$comment['id'] = $item['id_comment'];
		$comment['date_ts'] = $item['date'];
		$comment['date'] = $this->FormatDate($item['date']);
		$comment['author'] = $item['author'];
		$comment['title'] = $item['title'];
		if($name!="parent")
		{
			if($item['text']!="")
				$comment['text'] = array('xvalue'=>$this->Htmlise($item['text'],false));
			$this->Urlify($comment,"comments",array('id'=>$item['id_comment'],'id_item'=>$this->comments->id_item,'id_r'=>$this->comments->id_type,'id_topic'=>$this->topic->id,'subtype'=>"comment"),true);
			$reply_params = array();
			$this->Urlify($reply_params,"comments",array('id_parent'=>$item['id_comment'],'id_item'=>$this->comments->id_item,'id_r'=>$this->comments->id_type,'id_topic'=>$this->topic->id,'subtype'=>"insert"),true);
			$comment['reply'] = $reply_params;
		}
		return $comment;
	}

	private function Comments($item_allow,$topic_allow)
	{
		$insert_right = ($item_allow && (($this->comments->id_type!="5" && $this->comments->id_type!="10") || (($this->comments->id_type=="5" || $this->comments->id_type=="10") && $topic_allow>0)));
		$comments = array();
		$comments['xname'] = "comments";
		$counter = $this->comments->CommentsCount(1);
		$comments['num'] = $counter;
		$comments['id_type'] = $this->comments->id_type;
		$comments['id_item'] = $this->comments->id_item;
		$comments['item_allow'] = $item_allow;
		$comments['active'] = ($insert_right || $counter>0)? "1":"0";
		if($insert_right)
		{
			$insert_params = array('label'=>$this->tr->Translate("comments_insert"));
			$this->Urlify($insert_params,"comments",array('id_item'=>$this->comments->id_item,'id_r'=>$this->comments->id_type,'id_topic'=>$this->topic->id,'subtype'=>"insert"),true);
			$comments['insert'] = $insert_params;
		}
		if($counter>0)
			$this->Urlify($comments,"comments",array('id_item'=>$this->comments->id_item,'id_r'=>$this->comments->id_type,'id_topic'=>$this->topic->id),true);
		return $comments;
	}

	private function CommentsRecurse($comments)
	{
		$subs = array();
		foreach($comments as $comment)
		{
			$sub = $this->CommentItem($comment);
			$sub['xname'] = "comment";
			$children = $this->comments->Children($comment['id_comment']);
			if (count($children)>0)
				$sub['comments'] = $this->CommentsRecurse($children);
			$subs['c_' . $comment['id_comment']] = $sub;
		}
		return $subs;
	}
	
	private function CommentsWrapper($id_resource,$id_item,$params)
	{
		$this->subtype = ($params['subtype']!="")? $params['subtype'] : "browse";
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		$co = array_merge($this->Common($id_item,$this->id_type),$this->TopicCommon());
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		include_once(SERVER_ROOT."/../classes/comments.php");
		$type = $r->TypeLookup($id_resource);
		if(!$type) {
			UserError('Resource type not found', array('id_resource'=>$id_resource),256,true);
		}
		$this->comments = new Comments($type,$id_item);
		$this->current_page_params['id_item'] = $this->comments->id_item;
		$this->current_page_params['id_r'] = $this->comments->id_type;
		$comments = array();
		$comments['xname'] = "comments";
		$comments['type'] = $type;
		switch($type)
		{
			case "article":
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($id_item);
				$art = (isset($this->article))? $this->article : $a->ArticleGet();
				if($art['id_article']!=$id_item)
					$art = $a->ArticleGet();
				$article = $this->ArticleItem($art);
				$comments = $this->Comments($art['allow_comments'],$art['topic_allow_comments']);
				$article['comments'] = $comments;
				$comments['article'] = $article;
				$comments['title'] = $art['headline'];
			break;
			case "question":
				include_once(SERVER_ROOT."/../classes/polls.php");
				$pl = new Polls();
				$pq = $pl->QuestionGet($id_item);
				$poll = $pl->PollGet($pq['id_poll']);
				$question = $this->PollQuestionItem($pq);
				$comments = $this->Comments($poll['comments']>0,1);
				$question['comments'] = $comments;
				$comments['poll'] = $this->PollItem($poll);
				$comments['question'] = $question;
				$comments['title'] = $poll['title'];
			break;
			case "thread":
				include_once(SERVER_ROOT."/../classes/forum.php");
				$f = new Forum(0);
				$thread_row = $f->ThreadGet($id_item);
				$f->id = $thread_row['id_topic_forum'];
				$forum = $f->ForumGet();
				$thread = $this->ThreadItem($thread_row);
				$comments = $this->Comments($forum['comments']>0 && $forum['active']=="1",1);
				$thread['comments'] = $comments;
				$comments['forum'] = $this->ForumItem($forum);
				$comments['thread'] = $thread;
				$comments['title'] = $thread['title'];
			break;
		}
		if($this->subtype=="browse")
		{
			$this->comments->LoadTree();
			$children = $this->comments->Children(0);
			$comments['tree'] = $this->CommentsRecurse($children);
		}
		if($this->subtype=="comment")
		{
			$this->comments->LoadTree();
			$row = $this->comments->CommentGet($params['id']);
			$comments['comment'] = $this->CommentItem($row);
			$children = $this->comments->Children($params['id']);
			$comments['tree'] = $this->CommentsRecurse($children);
		}
		if($this->subtype=="insert")
		{
			$user = $this->PeopleUser();
			$login_required = false;
			if($type=="article" && !$user['id']>0 && $art['topic_allow_comments']=="1")
				$login_required = true;
			if($type=="question" && (($poll['comments']=="1" && $user['auth']!="1") || ($poll['comments']=="2" && !$user['id']>0) ))
				$login_required = true;
			if($login_required)
			{
				$comments['login'] = $this->LoginFirst($this->topic->id);
			}
			if($params['id_parent']>0)
			{
				$row = $this->comments->CommentGet($params['id_parent']);
				$parent = $this->CommentItem($row,"parent");
				$parent['reply_title'] =  (substr($row['title'],0,3)!="Re:")? "Re: {$row['title']}" : $row['title'];
				$comments['parent'] = $parent;
			}
			$privacy_text = $this->PrivacyText();	
			$comments['privacy_warning'] = array('xvalue'=>$privacy_text);
		}
		$co['comments'] = $comments;
		$feature_params = array('id_topic'=>$art['id_topic']);
		$co['features'] = $this->Features($this->id_type,0,$feature_params);
		return $co;
	}

	public function CommonCache($type)
	{
		$this->global = false;
		$this->id_type = $this->pt->types[$type];
		$this->cache_common = $this->Common(0,$this->id_type);
	}
	
	private function PreviewLinks($type,$id)
	{
		$preview = array();
		include_once(SERVER_ROOT."/../classes/modules.php");
		if (Modules::AmIAdmin(10) || ($this->topic->id>0 && $this->topic->edit_layout && $this->topic->AmIAdmin()))
		{
			$type0 = array_search(0,$this->pt->types);
			$type20 = array_search(20,$this->pt->types);
			$preview['edit'] = $this->EditLink($type,array('id'=>$id));
			$preview['xml'] = $this->xml_link;
			$gen = array();
			$edit_params = ($this->module!="")? array('module'=>$type,'id_style'=>0) : array('id_type'=>$this->id_type,'id_style'=>0);
			$gen['x_0'] = array('xname'=>"xsl",'type'=>$type20,'edit'=>$this->EditLink("xsl",array('id_type'=>20,'id_style'=>0)));
			$gen['x_1'] = array('xname'=>"xsl",'type'=>$type0,'edit'=>$this->EditLink("xsl",array('id_type'=>-1,'id_style'=>0)));
			$gen['x_2'] = array('xname'=>"xsl",'type'=>$type,'edit'=>$this->EditLink("xsl",$edit_params));
			$gen['c_1'] = array('xname'=>"css",'type'=>$type0,'edit'=>$this->EditLink("css",array('id_type'=>-1,'id_style'=>0)));
			$gen['c_2'] = array('xname'=>"css",'type'=>$type,'edit'=>$this->EditLink("css",$edit_params));
			$features1 = $this->pt->ft->FeatureGetByTypeOnly(0,0,0);
			if($this->global)
			{
				$features2 = $this->pt->ft->GlobalFeatureGetByType($this->id_type);
			}
			elseif($this->id_type=="0" && $this->id_module>0)
			{
				$features2 = $this->pt->ft->FeatureGetByType($this->id_type,$this->id_module,0);
			}
			else 
			{
				$features2 = $this->pt->ft->FeatureGetByType($this->id_type,0,0);
			}
			$features_gen = array();
			foreach($features1 as $f1)
			{
				$features_gen['f1_'.$f1['id_feature']] = array('xname'=>"feature",'type'=>$type0,'id'=>$f1['id_feature'],'name'=>$f1['name'],'edit'=>$this->EditLink("feature",array('id'=>$f1['id_feature'],'id_type'=>0,'id_style'=>0)));
			}
			foreach($features2 as $f2)
			{
				$features_gen['f2_'.$f2['id_feature']] = array('xname'=>"feature",'type'=>$type,'id'=>$f2['id_feature'],'name'=>$f2['name'],'edit'=>$this->EditLink("feature",array('id'=>$f2['id_feature'],'id_style'=>0)));
			}
			$gen['features'] = $features_gen;
			$preview['generic'] = $gen;
			$style = array();
			if (!$this->global && $this->id_style>0)
			{
				$style['id'] = $this->id_style;
				$edit_params = ($this->module!="")? array('module'=>$type) : array();
				$style['x_3'] = array('xname'=>"xsl",'type'=>$type20,'edit'=>$this->EditLink("xsl",array('id_type'=>20)));
				$style['x_4'] = array('xname'=>"xsl",'type'=>$type0,'edit'=>$this->EditLink("xsl",array('id_type'=>0)));
				$style['x_5'] = array('xname'=>"xsl",'type'=>$type,'edit'=>$this->EditLink("xsl",$edit_params));
				$style['c_3'] = array('xname'=>"css",'type'=>$type0,'edit'=>$this->EditLink("css",array('id_type'=>0)));
				$style['c_4'] = array('xname'=>"css",'type'=>$type,'edit'=>$this->EditLink("css",$edit_params));
				$features3 = $this->pt->ft->FeatureGetByTypeOnly(0,0,$this->id_style);
				if($this->id_type=="0" && $this->id_module>0)
				{
					$features4 = $this->pt->ft->FeatureGetByTypeOnly(0,$this->id_module,$this->id_style);
				}
				else 
				{
					$features4 = $this->pt->ft->FeatureGetByTypeOnly($this->id_type,0,$this->id_style);
				}
				$features_sty = array();
				foreach($features3 as $f3)
				{
					$features_sty['f3_'.$f3['id_feature']] = array('xname'=>"feature",'type'=>$type0,'id'=>$f3['id_feature'],'name'=>$f3['name'],'edit'=>$this->EditLink("feature",array('id'=>$f3['id_feature'],'id_type'=>0)));
				}
				foreach($features4 as $f4)
				{
					$features_sty['f4_'.$f4['id_feature']] = array('xname'=>"feature",'type'=>$type,'id'=>$f4['id_feature'],'name'=>$f4['name'],'edit'=>$this->EditLink("feature",array('id'=>$f4['id_feature'])));
				}
				$style['features'] = $features_sty;
			}
			$preview['style'] = $style;
		}
		return $preview;
	}
	
	public function Common($id,$id_type)
	{
		$type = ($this->module!="")? $this->module : array_search($id_type,($this->global)? $this->pt->gtypes: $this->pt->types);
		$common = array();
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$phpeace = new PhPeace;
		$site = array();
		$site['title'] = $this->ini->Get("title");
		$site['description'] = $this->ini->Get("description");
		$site['phpeace'] = $phpeace->version;
		$site['build'] = $phpeace->build;
		$site['base'] = $this->pub_web;
		$site['admin'] = $this->ini->Get('admin_web');
		$languages = $this->tr->languages;
		$site['lang'] = $languages[$this->ini->Get("id_language")];
		$site['lang_code'] = $this->tr->lang_code;
		$site['profiling'] = $this->profiling;
		$site['encoding'] = $this->conf->Get("encoding");
		$site['upload_host'] = $this->upload_host!=""? "http://{$this->upload_host}" : $this->pub_web;
		$site['ui'] = $this->conf->Get("ui");
		$site['cdn'] = $this->conf->Get("cdn");
		$site['assets_domain'] = $site['cdn']!=""? $site['cdn'] : $this->pub_web;
		if($this->conf->Get("recaptcha")) 
		{
			$site['captcha'] = 'recaptcha';
			$site['recaptcha_public_key'] = $this->conf->Get("recaptcha_public_key");
			$site['recaptcha_version'] = $this->conf->Get("recaptcha_version");
		}
        $site['google_api_key'] = $this->conf->Get("google_api_key");
        $site['server_date_time'] = date("Y-m-d H:i:s");
		$site['tracking'] = "0";
		if(($this->conf->Get("track") || $this->conf->Get("track_all")) && !$this->preview)
		{
			$site['tracking'] = "1";
		}
		$site['dr'] = $this->conf->Get("dr_site")? "1":"0";
		$this->Urlify($site,"homepage",array(),TRUE);
		$rss_link = array();
		$id_rss = $type=='map' && $id>0? $id:0;
		$this->Urlify($rss_link,"rss",array('subtype'=>"topic_group",'id'=>$id_rss),true);
		$site['rss'] = $rss_link;
		$site['geo'] = $this->ini->Get("geo_location");
		$search_params = array('label'=>$this->tr->Translate("search_engine"),'name'=>$this->tr->Translate("search"),'path'=>$this->ini->Get("search_path"));
		$this->Urlify($search_params,"search",array(),TRUE);
		$site['search'] = $search_params;
		$geosearch_params = array('name'=>$this->tr->Translate("geo_search"));
		$this->Urlify($geosearch_params,"geosearch",array('id'=>0),true);
		$site['geosearch'] = $geosearch_params;
		$galleries_params = array('name'=>$this->tr->Translate("galleries"));
		$this->Urlify($galleries_params,"galleries",array(),true);
		$site['galleries'] = $galleries_params;
		$events_params = array('label'=>$this->tr->Translate("calendar"),'today_ts'=>time(),'today'=>$this->FormatDate(time()),'path'=>$this->ini->Get("events_path"));
		$this->Urlify($events_params,"events",array('subtype'=>'next'),TRUE);
		$site['events'] = $events_params;
		$quotes_params = array('label'=>$this->tr->Translate("quotes"),'path'=>$this->ini->Get("quotes_path"));
		$this->Urlify($quotes_params,"quotes",array(),TRUE);
		$site['quotes'] = $quotes_params;
		if ($this->IsModuleActive("books"))
		{
			$books_params = array('path'=>$this->ini->Get("books_path"));
			$this->Urlify($books_params,"books",array('id_topic'=>$this->topic->id),TRUE);
			$site['books'] = $books_params;
		}
		if ($this->IsModuleActive("lists"))
		{
			$lists_params = array('path'=>$this->ini->Get("lists_path"));
			$this->Urlify($lists_params,"lists",array(),TRUE);
			$site['lists'] = $lists_params;
		}
		$site['tools'] = array('path'=>"tools");
		$feeds_params = array('label'=>'Feeds','path'=>"feeds",'type'=>$this->ini->Get("feed_type"));
		$this->Urlify($feeds_params,"feeds",array(),TRUE);
		$site['feeds'] = $feeds_params;
		$people = array();
		if($this->profiling)
		{
			$people = $this->PeopleLinks(($this->topic->profiling)? $this->topic->id : 0);
		}
		$people['submit'] = $this->pub_web . "/" . $this->ini->Get("users_path") . "/actions.php";
		$site['people'] = $people;
		$map = array('label'=>$this->tr->Translate("map"));
		$this->Urlify($map,"map",array(),true);
		$site['map'] = $map;
		if($this->conf->Get("fb_app_id")!="")
		{
			$site['fb_app_id'] = $this->conf->Get("fb_app_id");
		}
		$common['site'] = $site;
		if ($this->preview)
		{
			$common['preview'] = $this->PreviewLinks($type,$id);
		}
		$static = ($this->IsStatic())? "1" : "0";
		$img_sizes = $this->conf->Get("img_sizes");
		$rewrite = ($this->conf->Get("mod_rewrite"))? "1" : "0";
		$publish = array('id_type'=>$id_type,'type'=>$type,'subtype'=>$this->subtype,
			'global'=>(($this->global)?"1":"0"),'live'=>(($this->live)?"1":"0"),'id'=>$id,'preview'=>$this->preview,'thumb_size'=>($img_sizes[0]),
			'convert_format'=>($this->conf->Get("convert_format")),'style'=>$this->id_style,'static'=>$static,'protected'=>$this->protected,'id_language'=>($this->id_language),
			'rewrite'=>$rewrite);
		if($this->conf->Get("debug"))
			$publish['debug'] = "1";
		if($this->conf->Get("dev"))
			$publish['dev'] = "1";
		if($this->conf->Get("ui")) {
			$publish['ui'] = "1";
		} else {
			$css_version = $this->ini->GetModule("layout","css_version","");
			$publish['css_version'] = (strlen($css_version)>0)? "?v=$css_version":"";
			$js_version = $this->ini->GetModule("layout","js_version","");
			$publish['js_version'] = (strlen($js_version)>0)? "?v=$js_version":"";
		}
		if($this->highslide)
			$publish['highslide'] = "1";
		$common['publish'] = $publish;
		$common['c_features'] = $this->Features(0,0,array());
		$common['graphics'] = $this->Graphics();
		$common['labels'] = $this->Labels();
		if($this->live)
		{
			if($this->profiling)
			{
				$common['user'] = $this->PeopleUser(false);
			}
			$common['postback'] = $this->PostBack();
			$common['posterrors'] = $this->PostErrors();
		}
		return $common;
	}
	
	private function IsStatic()
	{
		if($this->global)
		{
			$is_static = true;
			if($this->id_type=="0")
			{
				$hometype = $this->ini->GetModule("homepage","hometype",0);
				$is_static = $hometype=="0";
			}
		}
		else 
		{
			$is_static = ($this->id_type=="1" || $this->id_type=="2" || $this->id_type=="3" || $this->id_type=="4" || $this->id_type=="10" || $this->id_type=="16" || $this->id_type=="17");
		}
		return $is_static;
	}
	
	public function CurrentPage($type,$is_global,$additional_params=array())
	{
		$current_page = array();
		$page_array = array();
		$params = array_merge($this->current_page_params,$additional_params);
		$this->Urlify($page_array,$type,$params,$is_global);
		$current_page = array('url'=>$page_array['url'],'url_encoded'=>urlencode($page_array['url']));
		return $current_page;
	}
	
	private function Day($id_topic,$id_group,$ts)
	{
		if (!isset($ts) || $ts==0)
			$ts = time();
		$e_array =  array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ee->id_topic = $id_topic;
		$ee->id_group = $id_group;
		$events = $ee->EventsDay($ts);
		$ev = array();
		foreach($events as $event)
		{
			$ev['e_' . $event['id_event'] ] = $this->EventItem($event);
		}
		$this->TopicInit($id_topic);
		$ev['info'] = $this->EventsInfo($ts);
		$e_array['events'] = $ev;
		return $e_array;
	}

	private function DayItem($ts)
	{
		$day = array();
		$day['dow'] = $this->dt->Dow($ts);
		$day['dow_name'] = $this->dt->DayOfWeek($ts);
		$day['dom'] = $this->dt->DayOfMonth($ts);
		$day['month'] = $this->dt->MonthNumber($ts);
		$day['month_name'] = $this->dt->Month($ts);
		$day['year'] = $this->dt->Year($ts);
		$day['label'] = $this->FormatDate($ts);
		$today_ts_midnight = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$offset = floor(($ts - $today_ts_midnight)/SECONDS_PER_DAY);
		$this->Urlify($day,"events",array('id_topic'=>$this->topic->id,'offset'=>$offset,'subtype'=>"day"),TRUE);
		return $day;
	}
	
	private function DodcWrapper($params)
	{
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$topic);
		$trm25 = new Translator($this->tr->id_language,25,false,$this->topic->id_style);
		$pdodc = array('label'=>$trm25->Translate("dodc"));
		$pparams = array('subtype'=>$this->subtype);
		$this->Urlify($pdodc, 'dodc', array('id_topic'=>$this->topic->id), true);
		include_once(SERVER_ROOT."/../modules/dodc.php");
		$dodc = new DodContractors();
		if($this->subtype=="")
			$this->subtype = "home";
		switch($this->subtype)
		{
			case "home":
			break;
			case "contract":
				$contract = $dodc->ContractGet($params['id']);
				if(isset($contract['id_contract'])) {
					$contract['xname'] = 'dodc_contract';
					$contract['iamount'] = number_format($contract['iamount'],0);
					$contractor = $dodc->ContractorGet($contract['id_contractor']);
					$contractor['xname'] = 'dodc_contractor';
					$this->Urlify($contractor,"dodc",array('subtype'=>'contractor','id' => $contractor['id_contractor']),true);
					$contract['contractor'] = $contractor;
					$office = $dodc->OfficeGet($contract['id_office']);
					$office['xname'] = 'dodc_office';
					$this->Urlify($office,"dodc",array('subtype'=>'office','id' => $office['id_office']),true);
					$contract['office'] = $office;
				}
				$pdodc['contract'] = $contract;
				break;
			case "contractor":
				$contractors = array();
				$this->Urlify($contractors,"dodc",array('subtype'=>'contractors'),true);
				$contractor = $dodc->ContractorGet($params['id']);
				if(isset($contractor['id_contractor'])) {
					$pparams['id'] = $contractor['id_contractor'];
					$this->Urlify($contractor,"dodc",$pparams,true);
					$contractor['xname'] = 'dodc_contractor';
					$rows = array();
					$num = $dodc->Contracts( $rows, $contractor['id_contractor'], true );
					array_walk($rows, function(&$item){
						$item['type'] = $item['item_type'] = 'dodc_contract';
						$item['iamount'] = number_format($item['iamount'],0);
						$item['xname'] = 'item';
						return;
					});
						$contractor['items'] = $this->Page($rows,$num);
				}
				$pdodc['contractors'] = $contractors;
				$pdodc['contractor'] = $contractor;
				break;
			case "contractors":
				$contractors = array();
				$this->Urlify($contractors,"dodc",$pparams,true);
				$rows = array();
				$num = $dodc->Contractors( $rows, 'IT' );
				array_walk($rows, function(&$item){
					$item['type'] = $item['item_type'] = 'dodc_contractor';
					$item['total'] = number_format($item['total'],0);
					$item['xname'] = 'item';
					return;
				});
				$contractors['items'] = $this->Page($rows,$num);
				$pdodc['contractors'] = $contractors;
			break;
			case "office":
				$offices = array();
				$this->Urlify($offices,"dodc",array('subtype'=>'offices'),true);
				$office = $dodc->OfficeGet($params['id']);
				if(isset($office['id_office'])) {
					$pparams['id'] = $office['id_office'];
					$this->Urlify($office,"dodc",$pparams,true);
					$office['total'] = number_format($office['total'],0);
					$office['xname'] = 'dodc_office';
					$rows = array();
					$num = $dodc->ContractsOffice( $rows, 0, $office['id_office'], true );
					array_walk($rows, function(&$item){
						$item['type'] = $item['item_type'] = 'dodc_contract';
						$item['iamount'] = number_format($item['iamount'],0);
						$item['xname'] = 'item';
						return;
					});
					$office['items'] = $this->Page($rows,$num);
				}
				$pdodc['offices'] = $offices;
				$pdodc['office'] = $office;
				break;
			case "offices":
				$offices = array();
				$this->Urlify($offices,"dodc",$pparams,true);
				$rows = array();
				$num = $dodc->Offices($rows);
				array_walk($rows, function(&$item){
					$item['type'] = 'dodc_office';
					$item['total'] = number_format($item['total'],0);
					$item['xname'] = 'item';
					return;
				});
				$offices['items'] = $this->Page($rows,$num);
				$pdodc['offices'] = $offices;
			break;
		}
		$db =& Db::globaldb();
		$db->select_db($this->conf->Get("dbconf")['database']);
		$pagetype['dodc'] = $pdodc;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_topic'=>($this->topic->id)));
		return $pagetype;
	}
	
	public function DoctypeHTML5Fix(&$html)
	{
		$doctype = $this->ini->GetModule("layout","xsl_output_doctype",XSL_DEFAULT_OUTPUT_DOCTYPE);
		if($doctype=="html5")
		{
			$this->xh->StripDoctype($html,"<!DOCTYPE html>");
		}
	}

	public function EbookWrapper($params)
	{
		include_once(SERVER_ROOT."/../modules/ebook.php");
		$eb = new EBook();
		$eb_array = array();
		$publish = array();
		$publish['subtype'] = $this->subtype;
		$publish['ts'] = time();
		$publish['type'] = $this->module;
		$publish['image_format'] = EBook::EbookImageFormat;
		$publish['image_mimetype'] = $eb->image_mimetype;
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$publish['phpeace'] = PHPEACE_VERSION;
		$publish['pub_web'] = $this->pub_web;
		$eb_array['publish'] = $publish;
		$id_ebook = (int)$params['id'];
		$ebook = $eb->EbookGet($id_ebook);
		$eb_data = array();
		$eb_data['xname'] = "ebook";
		$eb_data['id'] = $id_ebook;
		$eb_data['id_type'] = $ebook['id_type'];
		$eb_data['rights'] = $ebook['rights'];
		$eb_array['ebook'] = $eb_data;
		if($ebook['id_book']>0)
		{
			$eb_array = array_merge($eb_array,$this->Book($ebook['id_book']));
		}
		switch($this->subtype)
		{
			case "subtopic":
				$id_subtopic = (int)$params['id_subtopic'];
				if($id_subtopic>0)
				{
					$subtopic = $this->topic->SubtopicGet($id_subtopic);
					if(isset($subtopic['id_subtopic']) && ($subtopic['id_type']!=$this->topic->subtopic_types['url']) && $this->topic->SubtopicPublishable($subtopic['id_type']))
					{
						$subtopic['id'] = $id_subtopic;
						$eb_array['subtopic'] = $this->SubtopicItem($subtopic);
						$articles = $this->topic->SubtopicArticles($id_subtopic,$subtopic['sort_by']);
						if(count($articles)>0)
						{
							$ebook_articles = array();
							include_once(SERVER_ROOT."/../classes/article.php");
							foreach($articles as $article)
							{
								$article_content = $this->ArticleContent($article['id_article'],false);
								$ebook_articles['ebc'.$article['id_article']] = $article_content['article'];
							}
							$eb_array['articles'] = $ebook_articles;
						}
					}
				}
			break;
			case "article":
				$id_article = (int)$params['id_article'];
				if($id_article>0)
				{
					include_once(SERVER_ROOT."/../classes/article.php");
					$article_content = $this->ArticleContent($id_article,false);
					$eb_array['article'] = $article_content['article'];
				}
			break;
		}
		return $eb_array;
	}
	
	private function EcommerceOrderItem($item,$name="order")
	{
		$order = array();
		$order['xname'] = $name;
		$order['id'] = $item['id_order'];
		$order['type'] = "order";
		$order['date'] = $this->FormatDate($item['order_date_ts']);
        $order['payment_date'] = $this->FormatDate($item['payment_date_ts']);
		$order['total'] = $item['total'];
		$trm27 = new Translator($this->id_language,27,false,$this->id_style);
		$statuses = $trm27->Translate("order_status");
		$order['id_status'] = $item['id_status'];
		$order['status'] = $statuses[$item['id_status']];
		$this->Urlify($order,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"order",'id'=>$item['id_order']),TRUE);
		return $order;
	}
	
	private function EcommerceProductItem($item,$name="product")
	{
		$prod = array();
		$prod['xname'] = $name;
		$prod['id'] = $item['id_product'];
		$prod['type'] = "product";
		$prod['name'] = $item['name'];
        $prod['description'] = $item['description'];
		$prod['qty'] = $item['quantity'];
        $prod['stock'] = $item['stock'];
		$prod['price'] = $item['price'];
        $prod['sub_total'] = sprintf("%01.2f", $prod['qty'] * $prod['price']);
		$this->Urlify($prod,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"product",'id'=>$item['id_product']),TRUE);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$ec = new eCommerce();
		if($fm->Exists("uploads/products/0/" . $item['id_product'] . "." . $this->conf->Get("convert_format")))
		{
			$thumb = array();
			$thumb['xname'] = "thumb";
			$thumb['id'] = $item['id_product'];
			$img_sizes = $this->conf->Get("img_sizes");
			$thumb['width'] = $img_sizes[$ec->product_thumb];
			$this->Urlify($thumb,"product_image",array('id'=>$item['id_product'],'size'=>0),true);
			$prod['thumb'] = $thumb;
		}
		return $prod;
	}
	
	private function EcommerceProductDetails($id_product)
	{
		$ec = new eCommerce();
		$row = $ec->ProductGet($id_product);
		$prod = $this->EcommerceProductItem($row);
		$prod['description'] = array('xvalue'=>$row['description']);
		$fm = new FileManager();
		if($fm->Exists("uploads/products/1/" . $id_product . "." . $this->conf->Get("convert_format")))
		{
			$image = array();
			$image['xname'] = "image";
			$image['id'] = $id_product;
			$img_sizes = $this->conf->Get("img_sizes");
			$image['width'] = $img_sizes[$ec->product_image];
			$this->Urlify($image,"product_image",array('id'=>$id_product,'size'=>1,'format'=>'jpg'),true);
			$prod['image'] = $image;
		}
		$order = array();		
		$this->Urlify($order,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"order_preview",'id'=>$id_product),TRUE);
		$prod['order'] = $order;
		return $prod;
	}
	
	private function EcommerceProducts($page)
	{
		$ec = new eCommerce();
		$rows = array();
		$ec->Products($rows,false,true);
		$products = $this->Pager($rows,6);
		return $products[$page-1];
	}
	
	private function EcommerceWrapper($params,$page)
	{
		include_once(SERVER_ROOT."/../classes/ecommerce.php");
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$topic);
		$ec = new eCommerce();
		$trm27 = new Translator($this->id_language,27,false,$this->id_style);
		$ecomm = array('currency'=>$ec->currency,'label'=>($trm27->Translate("eshop")));
		$products_list = array('label'=>$trm27->Translate("products"));
		$this->Urlify($products_list,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"products"),TRUE);
		$ecomm['products_list'] = $products_list;
		$orders_list = array('label'=>$trm27->Translate("orders"));
		$this->Urlify($orders_list,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"orders"),TRUE);
		$ecomm['orders_list'] = $orders_list;
		if($this->subtype=="")
			$this->subtype = "home";
		switch($this->subtype)
		{
			case "home":
			break;
			case "product":
				$ecomm['product'] = $this->EcommerceProductDetails($params['id']);
                $ecomm['product']['description']['xvalue'] = stripcslashes(nl2br($ecomm['product']['description']['xvalue']));
                $ecomm['shopping_cart'] = $this->GetShoppingCart();
        	break;
			case "products":
                $ecomm['products'] = $this->EcommerceProducts($page);
                $ecomm['shopping_cart'] = $this->GetShoppingCart();
			break;
			case "order_preview":
                $id_order = 0;
                include_once(SERVER_ROOT."/../classes/session.php");
                $session = new Session();
                if ($session->IsVarSet('shopping_cart_order_id'))
                    $id_order = $session->Get('shopping_cart_order_id'); 
                
                if ($id_order == 0)
                {
                    $user = $this->PeopleUser();
                    if($user['auth']!="1")
                    {
                        $login_first = array('label'=>$this->tr->Translate("login_first"));
                        $this->Urlify($login_first,"people",array('subtype'=>"login",'jumpback'=>true,'id_topic'=>$this->topic->id),true);
                        $ecomm['login'] = $login_first;
                    }
                    else
                    {
                        $pagetype['geo'] = $this->Geo();
                        if (count($pagetype['postback']) > 0)
                        {
                            $user_details = array();   
                            foreach($pagetype['postback'] as $value)
                            {
                                $user_details[$value['name']] = $value['value'];
                            }         
                        }
                        else
                        {
                            include_once(SERVER_ROOT."/../classes/people.php");
                            $pe = new People();
                            $user_details = $pe->UserGetDetailsById($user['id']);
                        }     
                        $pagetype['user']['phone'] = $user_details['phone'];
                        $pagetype['user']['address'] = $user_details['address'];
                        $pagetype['user']['postcode'] = $user_details['postcode'];
                        $pagetype['user']['town'] = $user_details['town'];
                        $pagetype['user']['address_notes'] = $user_details['address_notes'];
                        $pagetype['user']['id_geo'] = $user_details['id_geo'];
                        
                        $ecomm['product'] = $this->GetShoppingCart(true, $id_order);   
                        $ecomm['product']['order_confirm_label'] = $trm27->Translate("order_confirm");
                        $ecomm['product']['delivery_notes_label'] = $trm27->Translate("delivery_notes");   
                    }
                    break;
                }
                else
                {
                    $url = "order.php?id=$id_order&id_topic={$this->topic->id}";  
                    header("Location: $url ");
                    exit();
                }
            case "shopping_cart":
                $ecomm['shopping_cart'] = $this->GetShoppingCart(true);   
                if (count($ecomm['shopping_cart']['shopping_cart_items']) == 0)
                {
                    include_once(SERVER_ROOT."/../classes/formhelper.php");
                    $fh = new FormHelper(false,0,false);
                    $url = $fh->JumpBackGet();     
                    if($url == "") 
                    {
                        $url = "products.php";
                        if ($this->topic->id > 0)
                            $url .=  "?id_topic=" . $this->topic->id;
                    }
                    header("Location: {$url}");
                    exit();
                }
            break;
			case "order_complete":
			case "order":
				$user = $this->PeopleUser();
				$id_order = $params['id'];
				if($ec->IsUserAuthorized($user['id'],$id_order))
				{
					include_once(SERVER_ROOT."/../classes/people.php");
					$pe = new People();
					$user_details = $pe->UserGetDetailsById($user['id']);
					$pagetype['user']['phone'] = $user_details['phone'];
					$pagetype['user']['address'] = $user_details['address'];
					$pagetype['user']['postcode'] = $user_details['postcode'];
					$pagetype['user']['town'] = $user_details['town'];
					$pagetype['user']['address_notes'] = $user_details['address_notes'];
					$pagetype['user']['id_geo'] = $user_details['id_geo'];
					$geo = new Geo();
					$pagetype['user']['geo_name'] = $geo->GeoGet($user_details['id_geo'],$geo->geo_location);
					$pagetype['user']['geo_name_label'] = $this->tr->Translate($geo->Label());
					if($this->subtype=="order_complete")
					{
						$message = array();
                        if($params['response_code']=="0")
                        {
                            $ec->OrderPaid($id_order,$params['id_transaction'],$this->topic->id);
                            $message['type'] = "notice";
                            $message['content'] = array('xvalue'=>$trm27->TranslateParams("payment_successful",array($params['id_transaction'])));
                        }
                        else 
                        {
                            $message['type'] = "error";
                            $message['content'] = array('xvalue'=>$ec->PaymentProcessError($params['response_code']));
                        }
					}
                    $row = $ec->OrderGet($id_order,true);
                    $order = $this->EcommerceOrderItem($row);
                    $order['delivery_notes'] = array('label'=>$trm27->Translate("delivery_notes"),'xvalue'=>$row['delivery_notes']);
                    $products = $ec->OrderProducts($id_order);
                    $o_products = array();
                    foreach($products as $product)
                    {
                        $o_products['p'.$product['id_product']] = $this->EcommerceProductItem($product,"item");
                    }
                    $order['products'] = $o_products;
                    if (count($message) > 0)
                        $order['message'] = $message;
					$ecomm['order'] = $order;
				}
			break;
			case "orders":
				$user = $this->PeopleUser();
				if($user['auth']!="1")
				{
					$login_first = array('label'=>$this->tr->Translate("login_first"));
					$this->Urlify($login_first,"people",array('subtype'=>"login",'jumpback'=>true,'id_topic'=>$this->topic->id),true);
					$ecomm['login'] = $login_first;
				}
				else
				{
					$rows = $ec->OrdersPerson($pagetype['user']['id']);
					$orders = $this->Pager($rows);
					$ecomm['orders'] = $orders[$page-1];
				}
			break;
		}
		$pagetype['ecomm'] = $ecomm;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_topic'=>($this->topic->id)));
		return $pagetype;
	}
    
    private function GetShoppingCart($with_image=false, $id_order=0)
    {
        include_once(SERVER_ROOT."/../classes/session.php");
        $session = new Session();

       if ($id_order > 0)
        {
            $ec = new eCommerce();
            $products = $ec->OrderProducts($id_order); 
            $shopping_cart = array();   
            foreach($products as $item)
            {
                $id_product = $item['id_product'];            
                $shopping_cart['item'.$id_product] = $item;
            }       
        }
        else
        {
            if ($session->IsVarSet('shopping_cart'))
                $shopping_cart = $session->Get('shopping_cart');
            else
                $shopping_cart = array();
        }

        $total = 0;
        $item_count = 0;
        foreach($shopping_cart as &$item)
        {
            $item_count++;
            if ($with_image)
                $item = $this->EcommerceProductItem($item);
            else
                $item['xname'] = 'product';
            $total += $item['sub_total'];   
        }
        $link = array();
        $this->Urlify($link,"ecommerce",array('id_topic'=>$this->topic->id,'subtype'=>"order_preview",'id'=>0), true);
        return array('shopping_cart_items'=>$shopping_cart, 'item_count'=>$item_count, 'total'=>sprintf("%01.2f", $total), 'link'=>$link);
    }
	
	private function EditLink($type,$params)
	{
		$link = "/";
		switch($type)
		{
			case "xsl":
				if($params['module']!="")
				{				
					$module = Modules::ModuleGetId($params['module']);
					$id_style = (isset($params['id_style']))? $params['id_style'] : $this->id_style;
					$link .= "layout/xsl_module.php?id={$module['id_module']}&id_style=$id_style";
				}
				else
				{
					$id_type = (isset($params['id_type']))? $params['id_type'] : $this->id_type;
					if ($id_type==-1)
						$id_type = 0;
					$id_style = (isset($params['id_style']))? $params['id_style'] : $this->id_style;
					if ($this->global && $params['id_type']!=-1 && $params['id_type']!=20)
						$link .= "layout/xsl_global.php?id=$id_type";
					else
						$link .= "layout/xsl.php?id=$id_type&id_style=$id_style";
				}
			break;
			case "css":
				if($params['module']!="")
				{
					$module = Modules::ModuleGetId($params['module']);
					$id_style = (isset($params['id_style']))? $params['id_style'] : $this->id_style;
					$link .= "layout/css_module.php?id={$module['id_module']}&id_style=$id_style";
				}
				else
				{
					$id_type = (isset($params['id_type']))? $params['id_type'] : $this->id_type;
					if ($id_type==-1)
						$id_type = 0;
					$id_style = (isset($params['id_style']))? $params['id_style'] : $this->id_style;
					if ($this->global && $params['id_type']!=-1)
						$link .= "layout/css_global.php?id=$id_type";
					else
						$link .= "layout/css.php?id=$id_type&id_style=$id_style";
				}
			break;
			case "feature":
				$id_type = (isset($params['id_type']))? $params['id_type'] : $this->id_type;
				$id_style = (isset($params['id_style']))? $params['id_style'] : $this->id_style;
				if ($this->global)
					$link .= "layout/feature_global.php?id={$params['id']}&id_type=$id_type";
				else
					$link .= "layout/feature.php?id={$params['id']}&id_type=$id_type&id_style=$id_style";
			break;
			case "article":
				$link .= "articles/article.php?id=" . $params['id'] . "&w=topics&id_topic=".$this->topic->id;
			break;
			case "subtopic":
				$link .= "topics/subtopic.php?id=" . $params['id'] . "&id_topic=".$this->topic->id;
			break;
			case "topic_home":
				$link .= "topics/homepage.php?id=".$this->topic->id;
			break;
		}
		return $link;
	}
	
	private function Error404($params)
	{
		$e = $this->Common(0,$this->id_type);
		if ($this->topic->id > 0)
			$e = array_merge($e,$this->TopicCommon());
		$e['features'] = $this->Features($this->id_type,0,array());
		$e['error'] = $params;
		return $e;
	}
	
	private function Event($id_event,$ts)
	{
		$e_array =  array();
		if($id_event>0)
		{
			include_once(SERVER_ROOT."/../classes/event.php");
			$e = new Event();
			$event = $e->EventGet($id_event);
			if ($event['approved'])
			{
				if ($event['id_topic']>0)
				{
					$this->TopicInit($event['id_topic']);
					$e_array = array_merge($e_array,$this->TopicCommon());
				}
				$ev = $this->EventItem($event);
				if($event['has_image']>0) {
					$image = array();
					$image['xname'] = "image";
					$image['id'] = $id_event;
					$img_sizes = $this->conf->Get("img_sizes");
					$image['width'] = $img_sizes[3];
					$image['format'] = 'jpg';
					$image['align'] = '0';
					$image['zoom'] = '2';
					$this->Urlify($image,"event_image",array('id'=>$id_event,'size'=>3,'format'=>'jpg'),TRUE );
					$image_zoom = array('label'=>$this->tr->Translate("zoom_click"),'label_close'=>$this->tr->Translate("close_window"));
					$this->Urlify($image_zoom,"event_image_orig",array('id'=>$id_event,'format'=>'jpg'));
					$image['image_zoom'] = $image_zoom;
					$ev['image'] = $image;
				}
				$ev['start_time'] = $this->FormatTime($event['start_date_ts']);
				$ev['start_iso8601'] = date("c",$event['start_date_ts']);
				$ev['allday'] = $event['allday'];
				$ev['length'] = $event['length'];
				if($event['length']>0) {
					$ev['end_iso8601'] = date("c",$event['start_date_ts'] + $event['length']*3600);
				} else {
					$ev['end_iso8601'] = date("Y-m-d",$event['start_date_ts']);
				}
				$ev['address'] = $event['address'];
				$ev['place_details'] = array('xvalue'=>$event['place_details']);
				$ev['description'] = array('xvalue'=> $event['is_html']? $event['description'] : $this->Htmlise($event['description']) );
				$ev['description_text'] = $event['description'];
				if($event['is_html']) {
					$this->th->Html2Text($ev['description_text'],true);
				}
				$ev['description_text'] = str_replace('"','&quot;',$ev['description_text']);
				$ev['contact_name'] = $event['contact_name'];
				$ev['email'] = $event['email'];
				$ev['link'] = $event['link'];
				$ev['facebook_id'] = $event['facebook_id'];
				$ev['important'] = $event['important'];
				$ev['portal'] = $event['portal'];
				$ev['export'] = $this->pub_web . '/' . $this->ini->Get("events_path") . "/export.php?format=ical&type=event&id={$id_event}";
				$ev['export_escaped'] = urlencode($ev['export']);
				$ev['export_webcal'] = preg_replace('#^http(s)?://#','webcal://',$ev['export']);
				$description = $ev['description_text'];
				if($ev['link']!='') {
					$description .= "\n\n$link";
				}
				$end_date_ts = $event['start_date_ts'] + (($event['length']==0?1:$event['length'])*3600);
				$google_location = urlencode("{$event['address']}, {$event['place']}");
				if($event['latitude']!='' && $event['longitude']!='' && $event['latitude!=0']) {
					$google_location .= "&ll={$event['latitude']},{$event['longitude']}";
				}
				$ev['export_google'] = $this->irl->GoogleCalendarLink($ev['title'],$description,$event['start_date_ts'],$end_date_ts,$event['allday'],$google_location);
				$ev['gmaps'] = "https://www.google.com/maps/search/?api=1&hl={$this->tr->lang_code}&query=" . $google_location;
				if ($event['id_article']>0)
				{
					$art = array();	
					$art['id'] = $event['id_article'];
					include_once(SERVER_ROOT."/../classes/article.php");
					$a = new Article($event['id_article']);
					$a->ArticleLoad();
					$l2 = $this;
					$l2->TopicInit($a->id_topic);
					$art['topic'] = array('id'=>($l2->topic->id),'name'=>($l2->topic->name));
					if($a->jump!="")
						$art['jump'] = $this->th->String2Url($a->jump);
					$l2->Urlify($art,"article",array('id'=>$event['id_article'],'id_topic'=>($l2->topic->id),'jump'=>$art['jump']));
					$art['headline'] = array('xvalue'=>$a->headline);
					$ev['article'] = $art;
				}
			}
			$e_array['events'] = array('info'=>$this->EventsInfo( ($ts>0)?$ts:$event['start_date_ts'] ));
			$e_array['event'] = $ev;
		}
		return $e_array;
	}

	private function EventInsert()
	{
		$e_array =  array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ev = array();
		$etypes = array();
		foreach($ee->Types() as $type)
		{
			$etypes['t_' . $type['id_event_type'] ] = array('xname'=>"type",'id'=>$type['id_event_type'],'type'=>$type['type']);
		}
		$ev['event_types'] = $etypes;
		$ev['info'] = $this->EventsInfo();
		$e_array['events'] = $ev;
		$months = array();
		$counter = 1;
		foreach($this->tr->Translate("month") as $month)
		{
			$months['m_' . $counter] = array('xname'=>"month",'n'=>$counter,'name'=>$month);
			$counter ++;
		}
		$e_array['months'] = $months;
		$e_array['geo'] = $this->Geo();
			$privacy_text = $this->PrivacyText();
			$e_array['privacy_warning'] = array('xvalue'=>$privacy_text);
		return $e_array;
	}

	private function EventItem( $item, $name="event" )
	{
		$event = array();
		$event['xname'] = $name;
		$event['type'] = "event";
		$event['id'] = $item['id_event'];
		$event['start_ts'] = $item['start_date_ts'];
		$event['start'] = date('Y-m-d',$item['start_date_ts']);
		$event['start_date'] = $this->FormatDate($item['start_date_ts']);
		$event['year'] = date("Y",$item['start_date_ts']);
		$event['month'] = date("n",$item['start_date_ts']);
		$event['month_name'] = $this->FormatMonth($item['start_date_ts']);
		$event['month_short'] = substr($event['month_name'],0,3);
		$event['day'] = date("j",$item['start_date_ts']);
		$event['day_name'] = $this->dt->DayNameByTs($item['start_date_ts']);
		$event['day_short'] = substr($event['day_name'],0,3);
		$event['address'] = $item['address'];
		$event['place'] = $item['place'];
		$event['id_geo'] = $item['id_geo'];
		$event['geo_name'] = $item['geo_name'];
		$event['event_type'] = $item['type'];
		$event['title'] = $item['title'];
		$this->th->Html2Text($item['description']);
		$event['desc'] = $this->th->StringPortion($item['description'], 150);
		if($item['hdate_ts']>0)
			$event['hdate'] = $this->FormatDate($item['hdate_ts']);
		$this->TopicInit($item['id_topic']);
		$event['id_topic'] = $item['id_topic'];
		$eparams = array('id'=>$item['id_event'],'id_topic'=>$event['id_topic'],'subtype'=>"event");
		if($item['has_image']>0) {
			$image = array();
			$image['xname'] = "image";
			$image['id'] = $item['id_event'];
			$img_sizes = $this->conf->Get("img_sizes");
			$image['width'] = $img_sizes[1];
			$image['image_ratio'] = $item['image_ratio'];
			$image['format'] = 'jpg';
			$this->Urlify($image,"event_image",array('id'=>$item['id_event'],'size'=>1,'format'=>'jpg'),TRUE );
			$event['image'] = $image;
		}
		$this->Urlify($event,"events",$eparams,TRUE);
		return $event;
	}

	private function EventSearch($params,$id_topic,$id_group)
	{
		$unescaped_query = $this->th->UtfClean($params['q']);
		$strlen = $this->th->StringLength($unescaped_query);
		$min_str_length = $this->conf->Get("min_str_length");
		$e_array =  array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ev = array();
		$search_words = $this->th->SearchWords($unescaped_query,$this->id_language);
		if($this->conf->Get('calendar_version')==1) {
			$ev['search'] = array('q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'id_etype'=>$params['id_etype']);
			$ee->id_topic = $id_topic;
			$ee->id_group = $id_group;
			if ($strlen>=$min_str_length || ($params['id_etype']>0) || ($params['id_geo']>0))
			{
				$events = array();
				$num_events = $ee->Search($events,$search_words,$params);
				$ev['items'] = $this->Page($events,$num_events);
				$this->Urlify($ev['search'],"events",array('id_topic'=>$id_topic,'subtype'=>"search",'q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'id_etype'=>$params['id_etype'],'period'=>$params['period']),true);
			}
			$etypes = array();
			foreach($ee->Types() as $type)
			{
				$etypes['t_' . $type['id_event_type'] ] = array('xname'=>"type",'id'=>$type['id_event_type'],'type'=>$type['type']);
			}
			$ev['event_types'] = $etypes;
			$this->TopicInit($id_topic);
			$ev['info'] = $this->EventsInfo();
			$e_array['events'] = $ev;
			$months = array();
			$counter = 1;
			foreach($this->tr->Translate("month") as $month)
			{
				$months['m_' . $counter] = array('xname'=>"month",'n'=>$counter,'name'=>$month);
				$counter ++;
			}
			$e_array['months'] = $months;
		} else {
			if ($strlen>=$min_str_length || ($params['id_geo']>0))
			{
				$events = array();
				$params['period'] = '1';
				$num_events = $ee->Search($events,$search_words,$params);
				$ev['future'] = array('q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'period'=>$params['period']);
				$ev['future']['items'] = $this->Page($events,$num_events);
				$ev['future']['items']['label'] = $this->tr->Translate("events_future");;
				$this->Urlify($ev['future'],"events",array('subtype'=>"search",'q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'tab'=>'future','period'=>$params['period']),true);
				$events = array();
				$params['period'] = '2';
				$num_events = $ee->Search($events,$search_words,$params);
				$ev['past'] = array('q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'period'=>$params['period']);
				$ev['past']['items'] = $this->Page($events,$num_events);
				$ev['past']['items']['label'] = $this->tr->Translate("events_past");;
				$this->Urlify($ev['past'],"events",array('subtype'=>"search",'q'=>$unescaped_query,'id_geo'=>$params['id_geo'],'tab'=>'past','period'=>$params['period']),true);
			}
			$ev['info'] = $this->EventsInfo();
			$e_array['events'] = $ev;
		}
		return $e_array;
	}

	private function Events($id_topic,$id_group,$limit)
	{
		$limit = ($limit>0)? $limit : $this->records_per_page_default;
		$e_array =  array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ee->id_topic = $id_topic;
		$ee->id_group = $id_group;
		$events = $ee->Next($limit,0,$id_topic=="0");
		$ev = array();
		foreach($events as $event)
		{
			$ev['e_' . $event['id_event'] ] = $this->EventItem($event);
		}
		$this->TopicInit($id_topic);
		$ev['info'] = $this->EventsInfo();
		$e_array['events'] = $ev;
		return $e_array;
	}

	private function EventsMonth($id_topic,$ts)
	{
		if (!isset($ts) || $ts==0)
			$ts = time();
		$e_array =  array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		if($id_topic>0)
			$ee->id_topic = $id_topic;
		$events = $ee->EventsMonth($ts);
		$ev = array();
		foreach($events as $event)
		{
			$ev['e_' . $event['id_event'] ] = $this->EventItem($event);
		}
		$this->TopicInit($id_topic);
		$ev['info'] = $this->EventsInfo($ts);
		$e_array['events'] = $ev;
		return $e_array;
	}

	private function EventsInfo($ts = 0)
	{
		if (!isset($ts) || $ts==0)
			$ts = time();
		$einfo = array();
		$einfo['time'] = $this->TimeItem($ts);
		if ($this->ini->Get("events_insert"))
		{
			$insert = array();
			$insert_params = array('id_topic'=>$this->topic->id,'subtype'=>"insert");
			$this->Urlify($insert,"events",$insert_params,true);
			$einfo['insert'] = $insert;
		}
		$esearch = array('label'=>$this->tr->Translate("search_advanced"));
		$this->Urlify($esearch,"events",array('id_topic'=>$this->topic->id,'subtype'=>"search"),TRUE);
		$einfo['search'] = $esearch;
		$today_ts_midnight = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$einfo['today'] = $this->DayItem($today_ts_midnight);
		$einfo['prev_day'] = $this->DayItem($ts - 86400);
		$einfo['curr_day'] = $this->DayItem($ts);
		$einfo['next_day'] = $this->DayItem($ts + 86400);
		$einfo['week'] = $this->WeekCurrent($ts);
		$einfo['month'] = $this->MonthSummary($ts);
		$einfo['history'] = $this->EventsRecurring($ts);
		$rss = array();
		$this->Urlify($rss,"rss",array('subtype'=>"events"),true);
		$einfo['rss'] = $rss;
		return $einfo;
	}
	
	private function EventsRecurring($ts)
	{
		include_once(SERVER_ROOT."/../classes/cache.php");
		$v = new Varia();
		$cache = new Cache();
		$cache->ttl = 720;
		$date = getdate($ts);
		$id = sprintf("%02d_%02d",$date['mon'],$date['mday']);
		if($cache->Exists("r_event",$id))
		{
			$erec = $v->Deserialize($cache->Get("r_event",$id));
		}
		else 
		{
			$cache->Update("r_event",$id);
			$erec = array();
			include_once(SERVER_ROOT."/../classes/events.php");
			$ee = new Events();
			$events = $ee->RecurringEvents($ts);
			foreach($events as $event)
			{
				$erec['e_' . $event['id_event'] ] = $this->RecurringEventItem($event);
			}
			$cache->Set("r_event",$id,$v->Serialize($erec,false));
		}
		return $erec;
	}
	
	private function EventsRss()
	{
		$rss = array();
		$rss['xname'] = "rss";
		$rss['title'] = $this->tr->Translate("calendar") . " " . $this->ini->Get("title");
		$rss['description'] = $this->tr->Translate("next_events");
		$rss['link'] = $this->irl->PublicUrlGlobal("rss",array('subtype'=>"events"));
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$events = $ee->Next(15,0,true);
		$items = array('counter'=>count($events));
		foreach($events as $event)
		{
			$items['e'.$event['id_event']] = $this->RssItem($event,0);
		}
		$rss['items'] = $items;
		return $rss;
	}

	private function EventsWrapper($params)
	{
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		$id_group = (int)$topic['topic']['id_group'];
		if ($this->subtype=="" )
			$this->subtype = ($params['subtype']!="")? $params['subtype'] : "day";
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		switch($this->subtype)
		{
			case "next":
				$pagetype = $this->Events($params['id_topic'],$id_group,$params['limit']);
			break;
			case "month":
				$pagetype = $this->EventsMonth($params['id_topic'],$params['ts']);
				$this->current_page_params['subtype'] = "month";
				$this->current_page_params['month'] = date("Ym",$params['ts']);
			break;
			case "day":
				$pagetype = $this->Day($params['id_topic'],$id_group,$params['ts']);
				$this->current_page_params['subtype'] = "date";
				$this->current_page_params['date'] = date("Ymd",$params['ts']);
			break;
			case "event":
				$pagetype = $this->Event($params['id'],$params['ts']);
			break;
			case "insert":
				if($this->ini->Get("events_insert"))
					$pagetype = $this->EventInsert();
				else 
					$this->Stop();
			break;
			case "search":
				$pagetype = $this->EventSearch($params,$params['id_topic'],$id_group);
			break;
		}
		$pagetype['geo'] = $this->Geo();
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_event'=>$params['id'],'id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function ExplodeMarkers($content)
	{
		$exploded = preg_split('((\[\[[Img|Box|Gra|Vid|Aud]+[0-9]*\]\]))',$content,-1,PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$return = array();
		$counter = 1;
		foreach($exploded as $fragment)
		{
			if(strlen($fragment)>STRING_CHUNK)
			{
				$chunks = $this->th->StringChunk($fragment,STRING_CHUNK);
				foreach($chunks as $chunk)
				{
					$return['e_' . $counter] = $this->Fragment($chunk,$counter);
					$counter ++;
				}
			}
			else 
			{
				$return['e_' . $counter] = $this->Fragment($fragment,$counter);
				$counter ++;			
			}
		}
		return $return;
	}

	private function Features($id_type,$id_module,$pagetype_params,$is_global=FALSE)
	{
		$functions = $this->tr->Translate("page_functions");
		if($is_global)
		{
			$rows = $this->pt->ft->GlobalFeatureGetByType($id_type);
		}
		else 
		{
			$rows = $this->pt->ft->FeatureGetByType($id_type,$id_module,$this->id_style);
			if($id_type==0 && !$this->id_style>0)
			{
				$g_rows = $this->pt->ft->GlobalFeatureGetByType(7);
				$rows = array_merge($rows,$g_rows);
			}
		}
		$features = array();
		if (count($rows)>0)
		{
			$v = new Varia();
			$l2 = clone $this;
			$l2->TopicInit($this->topic->id,true);
			foreach($rows as $row)
			{
				$l2->pt->ft->TopicInit($this->topic->id);
				if($this->ConditionEvaluate($row['condition_id'],$row['condition_var'],$row['condition_type'],$row['condition_value']))
				{
					$feature_params = isset($row['params'])? $v->Deserialize($row['params']) : array();
					$irows = $l2->pt->ft->PageFunction($row['id_function'],$feature_params,$pagetype_params);
					$items = array();
					$seq = 1;
					foreach($irows as $irow)
					{
						$irow['item_type'] = $l2->pt->ft->item_type;
						$items['i_' . $seq] = $l2->Item($irow);
						$seq++;
					}
					$features['ft_' . $row['id_feature'] ] = array('xname'=>"feature",'id'=>$row['id_feature'],'name'=>$row['name'],'id_user'=>$row['id_user'],'id_function'=>$row['id_function'],'function'=>$functions[$row['id_function']],'params_in'=>$pagetype_params,'params'=>$feature_params,'info'=>($l2->pt->ft->info),'type'=>($l2->pt->ft->item_type),'items'=>$items,'description'=>$row['description']);				
				}
			}
			unset($l2);
			unset($v);
			$this->RecordsPerPage($this->records_per_page);
		}
		return $features;
	}

	private function ConditionEvaluate($condition_id,$condition_var,$condition_type,$condition_value)
	{
		$return = true;
		if($condition_id>0)
		{
			$return = ($condition_id == ($this->current_page_params['id']));
		}
		if($condition_var!="" && $condition_type>0)
		{
			$return = false;
			switch($condition_type)
			{
				case "1":
					$return = isset($_GET[$condition_var]);
				break;
				case "2":
					$return = (int)$_GET[$condition_var] < (int)$condition_value;
				break;
				case "3":
					$return = $_GET[$condition_var] == $condition_value;
				break;
				case "4":
					$return = (int)$_GET[$condition_var] > (int)$condition_value;
				break;
				case "5":
					$return = !isset($_GET[$condition_var]);
				break;
			}
		}
		return $return;
	}
	
	private function FeedBack()
	{
		$feedback = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$messages = $session->Get("feedback");
		$counter = 1;
		if(is_array($messages) && count($messages)>0)
		{
			foreach($messages as $message)
			{
				if($message['tra'])
				{
					if($message['id_module']==$this->tr->id_module)
						$text = $this->tr->TranslateParams($message['text'],$message['params']);
					else 
					{
						$trm = new Translator($this->tr->id_language,$message['id_module'],false,$this->topic->id_style);
						$text = $trm->TranslateParams($message['text'],$message['params']);
					}
				}
				else 
					$text = $message['text'];
				$feedback['f_' . $counter] = array('xname'=>"message",'type'=>$message['type'],'text'=>$text);
				$counter ++;
			}
		}
		$session->Delete("feedback");
		return $feedback;
	}
	
	private function Feeds()
	{
		$feeds = $this->Common(0,$this->id_type);
		$feeds['features'] = $this->Features($this->id_type,0,array(),TRUE);
		include_once(SERVER_ROOT."/../classes/topics.php");
		$this->groups = new Topics;
		$this->groups->gh->LoadTree();
		$children = $this->groups->gh->th->GroupChildren(0);
		$feeds['site']['groups'] = $this->GroupRecurse($children,false);
		$rss = array();
		$this->Urlify($rss,"rss",array('subtype'=>"events"),true);
		$feeds['site']['events']['rss'] = $rss;
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology();
		$kfeeds = array();
		$o->KeywordsFeeds($kfeeds,false,true);
		foreach($kfeeds as $kfeed)
		{
			$krss = array('xname'=>"rss",'id'=>$kfeed['id_keyword'],'name'=>$kfeed['description']);
			$this->Urlify($krss,"rss",array('id'=>$kfeed['id_keyword'],'subtype'=>"keyword"),true);
			$feeds['site']['keywords']['k_'.$kfeed['id_keyword']] = $krss;
		}
		return $feeds;
	}
	
	private function FileInfo($filename)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$info = $fm->FileInfo($filename);
		$try = $this->tr->TranslateTry("format_" . $info['format']);
		if ($try!=("format_" . $info['format']))
			$info['format_info'] = array('xvalue'=>sprintf($try,$this->ini->Get("pub_web")));
		return $info;
	}
	
	private function Form($id_subtopic,$id_form)
	{
		include_once(SERVER_ROOT."/../classes/forms.php");
		$fo = new Forms();
		$row = $fo->FormGet($id_form);
		$form_upload = false;
		$form = array();
		$form['name'] = $row['name'];
		$form['id_form'] = $id_form;
		$form['require_auth'] = $row['require_auth'];
		if(isset($this->current_page_params['id_doc']) && $this->current_page_params['id_doc']>0)
		{
			$form['id_doc'] = $this->current_page_params['id_doc'];
			include_once(SERVER_ROOT."/../classes/doc.php");
			$d = new Doc($form['id_doc']);
			if($d->FormCheck($id_subtopic))
			{
				$doc = $d->DocGet();
				$form['doc'] = $this->ArticleDocItem($doc,"article_doc",$id_article,$this->topic->id);
				$form['doc']['download_label'] = $this->tr->Translate("download_form");
			}
		}
		$user = $this->PeopleUser();
		if(($form['require_auth']=="1" && $user['auth']!="1") || ($form['require_auth']=="2" && !$user['id']>0) )
		{
			$form['login'] = $this->LoginFirst($form['id_topic']);
		}				
		if($this->conf->Get("recaptcha"))
		{
			$form['captcha'] = $row['captcha'];		
		}
		$params = $fo->Params($id_form);
		$fparams = array();
		parse_str($_SERVER['QUERY_STRING'],$qs);
		foreach($params as $param)
		{
			if(isset($qs['pa'.$param['id_param']]))
				$param['value'] = $qs['pa'.$param['id_param']];
			if($param['type']=="link" && $param['value']=='referer')
			{
				include_once(SERVER_ROOT."/../classes/varia.php");
				$param['value'] = Varia::Referer();
			}
			$fparams['fp_' . $param['id_param']] =$this->FormParamItem($param,$row['multilanguage']);
			if($param['type']=="upload")
			{
				$form_upload = true;
			}
		}
		$form['params'] = $fparams;
		$form['contact_warning'] = array('xvalue'=>$this->tr->Translate("contact_warning"));
		if($row['privacy_warning'])
		{
			$privacy_text = $this->PrivacyText();
			$form['privacy_warning'] = array('xvalue'=>$privacy_text);
		}
		if($row['profiling']=="2")
			$form['funding'] = $this->Funding(2,$id_form,$row['amount'],$row['editable']);
		$payment = $this->topic->DonationPending();
		if($payment['id_payment']>0)
			$form['payment'] = $this->PaymentInfo($payment,$this->topic->name . " - " . $row['name']);
		return $form;
	}
	
	private function FormParamItem($item,$translate)
	{
		$param = array();
		$param['xname'] = "param";
		$param['id'] = $item['id_param'];
		$param['type'] = $item['type'];
		if($item['type']=="text")
			$param['use'] = $item['type_params'];
		if($item['type']=="geo")
			$param['geo'] = $this->Geo($item['type_params']);
		else
			$param['label'] = ($translate)? $this->tr->Translate($item['label']) : $item['label'];
		if($item['type']=="dropdown" || $item['type']=="mchoice" || $item['type']=="dropdown_open" || $item['type']=="mchoice_open" || $item['type']=="radio")
		{
			$pvalues = explode(",",$item['type_params']);
			$subparams = array();
			$counter = 1;
			foreach($pvalues as $pvalue)
			{
				$subparams['sp'.$counter] = array('xname'=>"subparam",'value'=>$pvalue);
				$counter ++;
			}
			$param['subparams'] = $subparams;
		}
		$param['mandatory'] = $item['mandatory'];
		if($item['value']!="")
			$param['value'] = $item['value'];
		return $param;
	}

	public function FormatDate($timestamp, $is_ts=TRUE)
	{
		return $this->dt->FormatDate($timestamp, $is_ts);
	}

	private function FormatTime($timestamp)
	{
		return $this->dt->FormatTime($timestamp);
	}

	private function FormatMinutes($seconds)
	{
		$work_time = getdate($seconds);
		return sprintf("%02d:%02d",$work_time['minutes'],$work_time['seconds']);
	}

	public function FormatMonth($timestamp)
	{
		return $this->dt->FormatMonth($timestamp,false);
	}
	
	private function ForumDetails($item)
	{
		$forum = $this->ForumItem($item);
		if($item['id_topic_forum']>0)
		{
			$f = new Forum($item['id_topic_forum']);
			$threads = array();
			$num = $f->ThreadsApprove( $threads, 1);
			$forum['items'] = $this->Page($threads,$num);
			if($item['users_type']>0)
			{
				$forum_insert = array('label'=>$this->tr->Translate("thread_insert"));
				$this->Urlify($forum_insert,"forum",array('id'=>$item['id_topic_forum'],'id_topic'=>$item['id_topic'],'subtype'=>"thread_insert"),true);
				$forum['insert'] = $forum_insert;
			}
		}
		return $forum;
	}
	
	private function ForumItem($item,$name="forum")
	{
		$id_forum = $item['id_topic_forum'];
		$forum = array();
		$forum['xname'] = $name;
		$forum['id'] = $item['id_topic_forum'];
		$forum['type'] = "forum";
		$forum['title'] = $item['name'];
		$forum['active'] = $item['active'];
		$forum['start_date'] = $this->FormatDate($item['start_date_ts']);
		if($item['topic_name']!="")
			$forum['topic'] = $item['topic_name'];
		$forum['description'] = array('xvalue'=>$this->Htmlise($item['description'],false));
		$this->Urlify($forum,"forum",array('id'=>$id_forum,'id_topic'=>$item['id_topic']),true);
		return $forum;
	}
	
	private function ForumWrapper($params)
	{
		include_once(SERVER_ROOT."/../classes/forum.php");
		$topic = ($params['id_topic']>0)? $topic = $this->TopicCommon() : array();
		if ($this->subtype=="" )
			$this->subtype = ($params['subtype']!="")? $params['subtype'] : ($params['id']>0?"info":"list");
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		$forums = array('label'=>$this->tr->Translate("forums"));
		$this->Urlify($forums,"forum",array('id_topic'=>$params['id_topic']),true);
		switch($this->subtype)
		{
			case "list":
				$f = new Forum(0);
				$rows = array();
				$num = $f->ActiveForums($rows,$params['id_topic']);
				$forums['items'] = $this->Page($rows,$num);
			break;
			case "info":
				$f = new Forum($params['id']);
				$row = $f->ForumGet();
				if($params['id']>0)
					$pagetype['forum'] = $this->ForumDetails($row);
			break;
			case "thread_insert":
				$f = new Forum($params['id']);
				$row = $f->ForumGet();
				$forum = $this->ForumItem($row);
				$forum['ask_source'] = $row['source'];
				$forum['users_type'] = $row['users_type'];
				$user = $this->PeopleUser();
				if(($row['users_type']=="1" && $user['auth']!="1") || ($row['users_type']=="2" && !$user['id']>0) )
				{
					$forum['login'] = $this->LoginFirst($row['id_topic']);
				}				
				$forum['submit'] = $this->pub_web . "/" . $this->ini->Get("forum_path") . "/actions.php";
				$privacy_text = $this->PrivacyText();
				$forum['privacy_warning'] = array('xvalue'=>$privacy_text);
				$pagetype['forum'] = $forum;
			break;
			case "thread":
				$f = new Forum($params['id']);
				$row = $f->ForumGet();
				$forum = $this->ForumItem($row);
				if($params['id_thread']>0)
				{
					$thread = $f->ThreadGet($params['id_thread']);
					$forum['thread'] = $this->ThreadDetails($thread,$row);
				}
				$pagetype['forum'] = $forum;
			break;
		}
		$pagetype['forums'] = $forums;
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_forum'=>$params['id'],'id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function Fragment($content,$seq)
	{
		$fragment = array('xname'=>"fragment",'seq'=>$seq);
		$match = array();
		if (preg_match('(\[\[(Img|Box|Gra|Vid|Vth|Aud)([0-9]*)\]\])',$content,$match))
		{
			$type = strtolower($match[1]);
			$id_media = (int)$match[2];
			if(($type=="vid" || $type=="vth" || $type=="aud") && $id_media>0)
			{
				if($this->IsModuleActive("media"))
				{
					if($type=="vid" || $type=="vth")
					{
						include_once(SERVER_ROOT."/../classes/video.php");
						$vi = new Video();
						$video = $vi->VideoGet($id_media,true);
						if($video['id_video']>0)
						{
							$fragment['video'] = $type=="vid"? $this->VideoDetails($video) : $this->VideoItem($video);
						}
					}
					if($type=="aud")
					{
						include_once(SERVER_ROOT."/../classes/audio.php");
						$au = new Audio();
						$audio = $au->AudioGet($id_media,true);
						if($audio['id_audio']>0)
						{
							$fragment['audio'] = $this->AudioDetails($audio);
						}
					}
				}
			}
			if($type=='gra' && $this->conf->Get("ui")=='1' && $id_media>0) {
				include_once(SERVER_ROOT."/../classes/graphic.php");
				$gr = new Graphic($id_media);
				$gra = $gr->GraphicGet();
				$fragment['format'] = $gra['format'];
				$fragment['width'] = $gra['width'];
				$fragment['height'] = $gra['height'];
			} 
			$fragment['type'] = $type;
			$fragment['id'] = $id_media;
		}
		else
		{
			$fragment['type'] = "text";
			$fragment['xvalue'] = $content;
		}
		return $fragment;
	}
	
	private function FriendSend($id_article)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$friend = array_merge($this->Common($id_article,$this->id_type),$this->TopicCommon());
		$friend['article'] = $this->ArticleItem($a->ArticleGet());
		return $friend;
	}
	
	private function Funding($id_type,$id_item,$item_amount,$editable)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$default_currency = true;
		$currencies = $p->currencies;
		$currency = $currencies[$this->ini->Get("default_currency")];
		$funding = array('label'=>$this->tr->Translate("funding_label"));
		$caccaounts = array('using'=>$this->tr->Translate("using"));
		$accounts = $p->AccountsUse($id_type,$id_item);
		$account_types = $this->tr->Translate("account_types");
		foreach($accounts as $account)
		{
			$caccaount = array('xname'=>"account");
			$caccaount['id'] = $account['id_use'];
			$caccaount['id_type'] = $account['id_type'];
			if($account['id_type']=="3" || $account['id_type']=="4")
			{
				$caparams = $p->AccountParamsGet($account['id_account']);
				if(trim($caparams['paypal_currencies'])!="")
				{
					$acurrencies = explode(",",$caparams['paypal_currencies']);
					if(is_array($acurrencies) && count($acurrencies)>0)
					{
						$cacurrencies = array();
						foreach($acurrencies as $key=>$value)
						{
							$cacurrency = array('xname'=>"currency",'code'=>$value);
							if($value==$currency)
								$cacurrency['default'] = "1";
							$cacurrencies['c_'.$key] = $cacurrency;
						}
						$caccaount['currencies'] = $cacurrencies;
						$default_currency = false;
					}
				}
				if($account['id_type']=="4" && $caparams['email']!="")
				{
					$unsub_url = "https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=" . urlencode($caparams['email']);
					$funding['delete_subscription'] = $this->tr->TranslateParams("paypal_unsubscribe",array($unsub_url));
				}
			}
			$caccaount['id_account'] = $account['id_account'];
			$caccaount['type'] = $account_types[$account['id_type']];
			$caccaounts['c'.$account['id_use']] = $caccaount;
		}
		$funding['accounts'] = $caccaounts;
		$amounts = explode(",",$item_amount);
		if(!is_array($amounts) || count($amounts)==0)
		{
			UserError("Funding values not set ",array("id_type"=>$id_type,"id_item"=>$id_item,"id_topic"=>($this->topic->id)));
			$amounts = array();
		}
		$camounts = array('editable'=>$editable);
		if($editable && count($amounts)>1)
			$camounts['or'] = $this->tr->Translate("or");
		foreach($amounts as $amount)
		{
			$camount = array('xname'=>"amount");
			if(strpos($amount,"d")!==false)
			{
				$amount = str_replace("d","",$amount);
				$camount['selected'] = "1";
			}
			$camount['value'] = $amount;
			if($default_currency)
				$camount['currency'] = $currency;
			if(is_numeric($amount))
				$camounts['a'.$amount] = $camount;
		}
		$funding['amounts'] = $camounts;
		return $funding;
	}
	
	private function Gallery($gallery)
	{
		$item = $gallery;
		$item['id_item'] = $gallery['id_gallery'];
		$item['name'] = $gallery['title'];
		$gal = $this->GalleryItemDetails($item);
		return $gal;
	}

	private function GalleryImage($id_image,$id_gallery,$id_subtopic)
	{
		$this->current_page_params['id_gallery'] = $id_gallery;
		$this->current_page_params['id_subtopic'] = $id_subtopic;
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$image = $g->ImageGet($id_image);
		$pos = $g->ImagePosition($id_image);
		return $this->GalleryImageContent($image,$id_subtopic,$pos);
	}
	
	private function GalleryImageContent($image,$id_subtopic,$position)
	{
		if(isset($this->cache_common))
		{
			$gi = $this->cache_common;
			$gi['publish']['id'] = $image['id_image'];
		}
		else 
			$gi = $this->Common($image['id_image'],$this->id_type);
		if ($this->topic->id>0)
		{
			$topic_common = isset($this->cache_topic)? $this->cache_topic : $this->TopicCommon();
			$gi = array_merge($gi,$topic_common);
			if ($id_subtopic>0)
			{
				$subtopic = $this->topic->SubtopicGet($id_subtopic);
				$sub = $this->SubtopicItem($subtopic);
				$page = floor(($position - 1)/$this->topic->records_per_page) + 1;
				$sub['breadcrumb'] = $this->SubtopicPath($id_subtopic,$page);
				$sub['subtopics'] = $this->SubtopicSubtopics($id_subtopic);
				$image['id_subtopic'] = $id_subtopic;
				$gi['subtopic'] = $sub;
			}
		}
		$gi['g'.$image['id_gallery'].'i'.$image['id_image']] = $this->TopicImageItem($image,FALSE);
		$feature_params = array('id_subtopic'=>$id_subtopic,'id_gallery'=>$image['id_gallery']);
		$gi['features'] = $this->Features($this->id_type,0,$feature_params);
		return $gi;
	}

	public function GalleryImageItem($image, $is_thumb, $name="gallery_image")
	{
		return $this->GenericImageItem($image,$is_thumb,true,$name,"galleries_image");
	}

	private function GalleryItem($item,$name="gallery")
	{
		$gallery = array();
		$gallery['xname'] = $name;
		$gallery['id'] = $item['id_item'];
		$gallery['name'] = $item['name'];
		$gallery['type'] = "gallery";
		$gallery['description'] = array('xvalue'=>($item['captions_html']? $this->th->Htmlise($item['description'],false) : $item['description']));
		if($item['id_subtopic']>0 && $this->topic->id>0 && !$this->global)
			$this->Urlify($gallery,"subtopic",array('id_topic'=>$this->topic->id,'id'=>$item['id_subtopic']));
		else
			$this->Urlify($gallery,"galleries",array('id_gallery'=>$item['id_item'],'subtype'=>"gallery",'page'=>$item['page']),true);
		return $gallery;
	}
	
	private function GalleryItemDetails($item,$name="gallery")
	{
		$gallery = $this->GalleryItem($item,$name);
		if(isset($item['counter']))
			$gallery['counter'] = $item['counter'];
		if($item['show_date'] && $item['released_ts']>0)
			$gallery['date'] = $this->FormatDate($item['released_ts']);
		if($item['author']!="")
			$gallery['author'] = $item['author'];
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($item['id_item']);
		$slideshow = array('label'=>$this->tr->Translate("slideshow"));
		$this->Urlify($slideshow,"galleries",array('id_gallery'=>$item['id_item'],'subtype'=>"slideshow"),true);
		$gallery['slideshow'] = $slideshow;
		$image = $g->Image();
		if($image['id_image']>0)
		{
			$img_sizes = $this->conf->Get("img_sizes");
			$gimage= array('xname'=>"aimage",'caption'=>$image['caption'],'id'=>$image['id_image'],'format'=>$image['format'],'width'=>$img_sizes[$image['thumb_size']],'height'=>$image['thumb_height'],'size'=>$image['thumb_size']);
			$this->Urlify($gimage,"galleries_image",array('id'=>$image['id_image'],'format'=>$image['format'],'image_size'=>$image['thumb_size'],'id_gallery'=>$item['id_item']),true);
			$gallery['aimage'] = $gimage;
		}
		if($item['slideinfo'])
		{
			$gallery['shuffle'] = ($g->slides_order=="4" || $item['shuffle'])? "true":"false";
			$gallery['xml'] = "$this->pub_web/" . $this->irl->paths['graphic'] . "galleries/{$g->id}/images.xml";
			$img_sizes = $this->conf->Get("img_sizes");
			$gallery['width'] = $img_sizes[$g->image_size];
			$gallery['height'] = $image['image_height'];
			if($item['audio']!="")
				$gallery['audio'] = $item['audio'];
			if($item['show_captions'])
				$gallery['show_captions'] = "1";
			if($this->IsModuleActive("media"))
				$gallery['watermark'] = "$this->pub_web/" . $this->irl->paths['graphic'] . "videos/watermark.png";
		//	$gallery['counter'] = count($g->gimages);
		}
		if ($this->ini->Get("licences") && $g->id_licence > 0)
			$gallery['licence'] = $this->Licence($g->id_licence,$gallery['author']);
		return $gallery;
	}
	
	private function GalleryWrapper($params,$page)
	{
		$this->subtype = "group";
		$id = $params['id_group'];
		$id_group = $id;
		$this->current_page_params['id_group'] = $params['id_group'];
		if($params['id_gallery']>0)
		{
			$this->subtype = $params['subtype']=="slideshow"? "slideshow":"gallery";
			$id = $params['id_gallery'];
			include_once(SERVER_ROOT."/../classes/gallery.php");
			$g = new Gallery($id);
			if(!$g->visible)
				$this->Stop();
			$g->LoadImages();
			$id_group = $g->id_group;
			$this->current_page_params['id_gallery'] = $params['id_gallery'];
		}
		if($params['id_image']>0)
		{
			$this->subtype = "image";
			$id = $params['id_image'];
			$this->current_page_params['id_image'] = $params['id_image'];
		}
		$this->current_page_params['subtype'] = $this->subtype;
		$gallery = $this->Common($id,$this->id_type);
		include_once(SERVER_ROOT."/../classes/galleries.php");
		$this->groups = new Galleries;
		$gallery['tree'] = $this->GroupTree(0);
		if($id_group>0)
		{
			$group = $this->groups->gh->GroupGet($id_group);
			$gallery['group'] = $this->GroupItem($group,false,true);
		}
		else
		{
			$hgalleries = $this->groups->Homepage();
			$homepage = array();
			foreach($hgalleries as $hgallery)
			{
				if($hgallery['counter']>0)
					$homepage['g'.$hgallery['id_item']] = $this->GalleryItemDetails($hgallery);
			}
			$gallery['group']['galleries'] = $homepage;
		}
		if($params['id_gallery']>0)
		{
			if($params['id_image']>0)
			{
				$g_row = $g->row;
				$g_row['page'] = $g->ImagePage($params['id_image']);
				$gallery['gallery'] = $this->Gallery($g_row);
				$image = $g->ImageGet($params['id_image']);
				if($image['id_image']>0)
					$gallery['gallery']['image'] = $this->GalleryImageItem($image,false,"image");
				else 
					$this->Stop();
			}
			else
			{
				$g_item = $g->row;
				$images = $g->Images();
				if($this->subtype=="slideshow")
				{
					$g_item['slideinfo'] = true;
					$gallery['gallery'] = $this->Gallery($g_item);
					$g_images = $this->Pager($images,100);
					$gallery['gallery']['images'] = $g_images[0];
				}
				else 
				{
					$gallery['gallery'] = $this->Gallery($g_item);
					$this->records_per_page = $g->images_per_page;
					$g_images = $this->Pager($images);
					$gallery['gallery']['images'] = $g_images[$page-1];					
				}
			}
			$gallery['gallery']['counter'] = count($g->gimages);
		}
		$gallery['features'] = $this->Features($this->id_type,0,array(),true);
		return $gallery;
	}
	
	private function GenericImageItem($image, $is_thumb, $is_global, $name, $url_name)
	{
		$size = ($is_thumb)? $image['thumb_size'] : $image['image_size'];
		$img = array();
		$img['xname'] = $name;
		$img['type'] = ($is_thumb)? "gallery_thumb" : "gallery_image";
		$img['id'] = $image['id_image'];
		$img['caption'] = $this->th->StripHtml($image['caption']);
		$img['caption_html'] = $image['captions_html']? $this->th->Htmlise($image['caption'],false) : $image['caption'];
		if ($image['show_date'] && $this->IsDateOk($image['image_date']))
			$img['date'] = $this->FormatDate($image['image_date'],false);
		$img['format'] = $image['format'];
		$img['size'] = $size;
		$img_sizes = $this->conf->Get("img_sizes");
		$img['width'] = $img_sizes[$size];
		$img['height'] = ($is_thumb)? $image['thumb_height'] : $image['image_height'];
		$img['source'] = $image['source'];
		$img['use_image_link'] = (int)$image['use_image_link'];
		if($image['show_author'])
		{
			$img['author'] = $image['author']!=""? $image['author'] : $image['gallery_author'];
		}
		$img['link'] = $image['link'];
		if ($this->ini->Get("licences") && $image['id_licence'] > 0)
			$img['licence'] = $this->Licence($image['id_licence'],$img['author']);
		if ($is_thumb)
		{
			if($this->topic->id>0)
				$this->Urlify($img,"gallery_item",array('id'=>$image['id_image'],'id_gallery'=>$image['id_gallery'],'id_topic'=>$this->topic->id,'id_subtopic'=>(int)$image['id_subtopic']));
			else
				$this->Urlify($img,"galleries",array('id_image'=>$image['id_image'],'id_gallery'=>$image['id_gallery'],'subtype'=>"image"),true);
			if($image['zoom']=="2")
			{
				$image_zoom = array('width'=>$img_sizes[$image['image_size']],'height'=>$image['image_height'],'label'=>$this->tr->Translate("zoom_click"),'label_close'=>$this->tr->Translate("close_window"));
				$this->Urlify($image_zoom,$url_name,array('id'=>$image['id_image'],'format'=>$image['format'],'id_gallery'=>$image['id_gallery'],'image_size'=>$image['image_size']),$is_global);
				$img['image_zoom'] = $image_zoom;
			}
		}
		else 
		{
			if(!$this->global && $this->id_type!=4 )
			{
				if($this->topic->id>0)
					$this->Urlify($img,"gallery_item",array('id'=>$image['id_image'],'id_gallery'=>$image['id_gallery'],'id_topic'=>$this->topic->id,'id_subtopic'=>(int)$image['id_subtopic']));
				else
					$this->Urlify($img,"galleries",array('id_image'=>$image['id_image'],'id_gallery'=>$image['id_gallery'],'subtype'=>"image"),true);			
			}
			$prev_params = array('label'=>$this->tr->Translate("previous"));
			if($image['id_prev']>0)
			{
				$prev_params['id'] = $image['id_prev'];
				if($this->global)
					$this->Urlify($prev_params,"galleries",array('id_image'=>$image['id_prev'],'id_gallery'=>$image['id_gallery'],'subtype'=>"image"),true);
				else
					$this->Urlify($prev_params,"gallery_item",array('id'=>$image['id_prev'],'id_gallery'=>$image['id_gallery'],'id_topic'=>$this->topic->id,'id_subtopic'=>(int)$image['id_subtopic']));
			}
			$img['prev'] = $prev_params;
			$next_params = array('label'=>$this->tr->Translate("next"));
			if($image['id_next']>0)
			{
				$next_params['id'] = $image['id_next'];
				if($this->global)
					$this->Urlify($next_params,"galleries",array('id_image'=>$image['id_next'],'id_gallery'=>$image['id_gallery'],'subtype'=>"image"),true);
				else
					$this->Urlify($next_params,"gallery_item",array('id'=>$image['id_next'],'id_gallery'=>$image['id_gallery'],'id_topic'=>$this->topic->id,'id_subtopic'=>$image['id_subtopic']));
			}
			$img['next'] = $next_params;
		}
		if($image['image_footer']!="")
			$img['image_footer'] = array('xvalue'=>$image['image_footer']);
		if($image['show_orig'])
		{
			$orig = array();
			$orig['info'] = $this->ImageInfo("uploads/images/orig/{$image['id_image']}.{$image['format']}",$image['orig_width'],$image['orig_height']);
			$orig['xname'] = "file";
				$this->Urlify($orig,"galleries",array('id_image'=>$image['id_image'],'format'=>$image['format'],'subtype'=>"image_orig"),true);
			$img['file'] = $orig;
		}
		$src = array();
		$this->Urlify($src,$url_name,array('id'=>$image['id_image'],'format'=>$image['format'],'id_gallery'=>$image['id_gallery'],'image_size'=>$size),$is_global);
		$img['src'] = $src;
		return $img;
	}

	private function Geo($forced_geo_location=0)
	{
		$geo = array();
		$geo['xname'] = "geo";
		$g = new Geo();
		if($forced_geo_location>0)
			$g->geo_location = $forced_geo_location;
		$label = $g->Label();
		$geo['name'] = $this->tr->Translate($label);
		$geo['geo_location'] = $g->geo_location;
		switch($g->geo_location)
		{
			case "1";
				$regions = array();
				$provinces = array();
				foreach($g->Regions() as $region)
				{
					$regions['r_' . $region['id_reg'] ] = array('xname'=>'region','id'=>$region['id_reg'],'name'=>$region['reg']);
				}
				foreach($g->Provinces() as $province)
				{
					$provinces['r_' . $province['id_prov'] ] = array('xname'=>'province','id'=>$province['id_prov'],'name'=>$province['prov'],'short'=>$province['pr'],'id_reg'=>$province['id_reg']);
				}
				$geo['regions'] = $regions;
				$geo['provinces'] = $provinces;
			break;
			case "2";
				$countries = array();
				foreach($g->Countries(1) as $country)
				{
					$countries['c_' . $country['id_country'] ] = array('xname'=>'country','id'=>$country['id_country'],'name'=>$country['name']);
				}
				$geo['countries'] = $countries;
			break;
			case "3";
				$counties = array();
				foreach($g->UkCounties() as $county)
				{
					$counties['c_' . $county['id_county'] ] = array('xname'=>'county','id'=>$county['id_county'],'name'=>$county['name']);
				}
				$geo['counties'] = $counties;
			break;
			case "4";
				$counties = array();
				foreach($g->EireCounties() as $county)
				{
					$counties['c_' . $county['id_county'] ] = array('xname'=>'county','id'=>$county['id_county'],'name'=>$county['name']);
				}
				$geo['counties'] = $counties;
			break;
			case "5";
				$regions = array();
				$provinces = array();
				foreach($g->Regions() as $region)
				{
					$regions['r_' . $region['id_reg'] ] = array('xname'=>'region','id'=>$region['id_reg'],'name'=>$region['reg']);
				}
				$geo['regions'] = $regions;
			break;
			case "6";
				$countries = array();
				foreach($g->Countries(0) as $country)
				{
					$countries['c_' . $country['id_country'] ] = array('xname'=>'country','id'=>$country['id_country'],'name'=>$country['name']);
				}
				$geo['countries'] = $countries;
			break;
			case "7";
				$countries = array();
				foreach($g->Countries(3) as $country)
				{
					$countries['c_' . $country['id_country'] ] = array('xname'=>'country','id'=>$country['id_country'],'name'=>$country['name']);
				}
				$geo['countries'] = $countries;
			break;
			default;
				$countries = array();
				$geo['countries'] = $countries;
			break;
		}
		return $geo;
	}
	
	private function GeoSearch($id_keyword,$id_topic)
	{
		$s_array =  $this->Common(0,$this->id_type);
		$this->current_page_global = true;
		if ($id_topic>0)
			$s_array = array_merge($s_array,$this->TopicCommon());
		$geosearch = array('label'=>$this->tr->Translate("geo_search"));
		$this->Urlify($geosearch,"geosearch",array('id'=>0,'id_topic'=>$this->topic->id),true);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$this->ontology = new Ontology();
		$this->ontology->LoadTree(2);
		if($id_keyword>0)
		{
			$time_weight = $this->conf->Get("time_weight");
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$keyword = $k->KeywordGet($id_keyword);
			if($keyword['keyword']!="")
			{
				$geokeyword = $this->KeywordGeoItem($keyword);
				if($keyword['description']!="")
					$geokeyword['description'] = array('xvalue'=>$keyword['description']);
				$kparents = $this->ontology->KeywordPath($id_keyword);
				$path = array();
				foreach($kparents as $kparent)
				{
					$path['k_'.$kparent['id_keyword']] = $this->KeywordGeoItem($kparent);
				}
				$geokeyword['path'] = $path;
				$articles = array();
				$num_articles = $k->UseArticlesGeo($articles,$keyword['keyword'],$id_topic,$time_weight);
				$articles_paged = $this->Page($articles,$num_articles);
				$this->TopicInit($id_topic);
				$this->Urlify($articles_paged,"geosearch",array('id'=>$id_keyword,'id_topic'=>$this->topic->id),true);
				$geokeyword['articles'] = $articles_paged;
				// $others = $k->UseOthersGeo($id_keyword);
			}
		}
		else 
			$geokeyword = array('id'=>0);
		$geokeyword['children'] = $this->GeoSearchChildren($id_keyword,true);
		$geosearch['geokeyword'] = $geokeyword;
		$s_array['geosearch'] = $geosearch;
		$s_array['features'] = $this->Features($this->id_type,0,array('id_topic'=>$id_topic));
		return $s_array;
	}
	
	private function GeoSearchChildren($id_keyword,$recurse)
	{
		$children = array();
		$kchildren = $this->ontology->KeywordChildren($id_keyword);
		foreach($kchildren as $kchild)
		{
			$keyword = $this->KeywordGeoItem($kchild);
			if($recurse)
				$keyword['children'] = $this->GeoSearchChildren($kchild['id_keyword'],false);
			$children['k_'.$kchild['id_keyword']] = $keyword;
		}
		return $children;
	}

	private function GraphicItem( $item )
	{
		$gi = array();
		$gi['xname'] = "graphic";
		$gi['id_style'] = $item['id_style'];
		$gi['id'] = $item['id_graphic'];
		$gi['caption'] = $item['name'];
		$gi['width'] = $item['width'];
		$gi['height'] = $item['height'];
		$gi['format'] = $item['format'];
		$this->Urlify($gi,"graphic",array('id'=>$item['id_graphic'],'format'=>$item['format']),TRUE);
		return $gi;
	}

	private function Graphics()
	{
		$graphics = array();
		if(!$this->conf->Get("ui")) {
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles();
			if ($this->id_style>0) {
				$tgraphics = $s->GraphicsAll($this->id_style);
				foreach($tgraphics as $tgraphic)
					$graphics['g_' . $tgraphic['id_graphic']] = $this->GraphicItem($tgraphic);
			}
			$sgraphics = $s->GraphicsAll(0);
			foreach($sgraphics as $sgraphic) {
				$graphics['g_' . $sgraphic['id_graphic']] = $this->GraphicItem($sgraphic);
			}
		}
		return $graphics;
	}

	private function GroupItem($group,$recurse,$details=false)
	{
		$g = $this->GroupItemSmall($group);
		$g['breadcrumb'] = $this->GroupPath($group['id_group']);
		$g[$this->groups->gh->type] = $this->GroupItems($group['id_group'], ((!$group['item_menu'])? false:true),$details );
		$children = $this->groups->gh->th->GroupChildren($group['id_group']);
		if ($recurse && count($children)>0)
			$g['groups'] = $this->GroupRecurse($children,false);
		return $g;
	}
	
	private function GroupItemSmall($group,$name="group")
	{
		$g = array();
		$g['xname'] = $name;
		$g['id'] = $group['id_group'];
		$g['name'] = $group['name'];
		if (isset($this->groups->gh->type))
			$g['type'] = $this->groups->gh->type;
		if(isset($group['item_type']))
			$g['type'] = $group['item_type'];
		if(isset($this->groups))
			$g['description'] = $this->groups->gh->th->description[$group['id_group']];
		if($g['type']=="topics")
		{
			$rss = array();
			$this->Urlify($rss,"rss",array('subtype'=>"topic_group",'id'=>$group['id_group']),true);
			$g['rss'] = $rss;
			$this->Urlify($g,"map",array('id'=>$group['id_group'],'subtype'=>$g['type']),true);
		}
		else
			$this->Urlify($g,"galleries",array('id_group'=>$group['id_group']),true);
		return $g;
	}
	
	private function GroupItems($id_group,$item_menu,$with_details)
	{
		$rows = $this->groups->gh->GroupItems($id_group);
		$items = array();
		foreach($rows as $row)
		{
			if ($row['visible'])
			{
				switch ($row['type'])
				{
					case "topic":
						$this->TopicInit($row['id_item']);
						$item = $this->TopicItem($this->topic->row);
						if ($item_menu)
							$item = array_merge($item,$this->Navigation(0));
						$items['t_' . $row['id_item']] = $item;
						unset($item);
					break;
					case "gallery":
						$items['g_' . $row['id_item']] = ($with_details)? $this->GalleryItemDetails($row) : $this->GalleryItem($row);
					break;
				}
			}
		}
		$this->TopicInit(0);
		return $items;
	}
	
	private function GroupPath($id_group)
	{
		$parents = array();
		$this->groups->gh->th->GroupParents($id_group, $parents);
		$parents = array_reverse($parents);
		$groups = array();
		$level = 1;
		foreach($parents as $parent)
		{
			$groups['g_' . $parent['id_group']] = $this->GroupItemSmall($parent);
			$level ++;
		}
		return $groups;
	}

	private function GroupRecurse($groups,$with_item_menu)
	{
		$gs = array();
		foreach($groups as $group)
		{
			if ($group['visible'])
			{
				$group['item_menu'] = $with_item_menu;
				$g = $this->GroupItem($group,true);
				$gs['g_' . $group['id_group']] = $g;
				unset($g);
			}
		}
		return $gs;
	}
	
	private function GroupRss($id_group,$feed_type)
	{
		include_once(SERVER_ROOT."/../classes/topics.php");
		$this->groups = new Topics;
		$this->groups->gh->LoadTree();
		$domain = $this->ini->Get("pub_web");
		if ($id_group==0)
		{
			$group_name = $this->groups->gh->top_name;
			$group_description = $this->groups->gh->top_description;
			$link = $domain;
		}
		else
		{
			$row = $this->groups->gh->GroupGet($id_group);
			$group_name = $row['name'];
			$group_description = $row['description'];
			$this->TopicInit(0);
			$link = $this->irl->PublicUrlGlobal("map",array('id'=>$id_group,'subtype'=>"topics"));
		}
		$rss = array();
		$rss['xname'] = "rss";
		$rss['title'] = $group_name;
		$rss['description'] = $group_description;
		$rss['link'] = $link;
		$this->groups->latest = 15;
		$latest = $this->groups->Latest($id_group);
		$items = array('counter'=>count($latest));
		foreach($latest as  $art)
		{
			$items['a'.$art['id_article']] = $this->RssItem($art,$feed_type);
		}
		$rss['items'] = $items;
		return $rss;
	}

	private function GroupTree($id_group)
	{
		$this->groups->gh->LoadTree();
		$children = $this->groups->gh->th->GroupChildren($id_group);
		$tree = array();
		$this->Urlify($tree,$this->groups->gh->type,array(),true);
		$tree['name'] = $this->groups->gh->top_name;
		$tree['description'] = $this->groups->gh->top_description;
		//$with_item_menu = $this->groups->gh->type=="topics";
		$with_item_menu = ($this->id_type==2 && $this->global);
		$tree['groups'] = $this->GroupRecurse($children,$with_item_menu);
		return $tree;
	}
	
	private function Home($params)
	{
		$home = $this->Common(0,$this->id_type);
		if($this->IsModuleActive("widgets"))
		{
			$la = new Labels(0,true,0);
			$la->module = "widgets";
			$la->tr = $this->tr;
			$la->id_module = 32;
			$widgets_labels = $la->Translate();
			$home['labels'] = array_merge($home['labels'],$widgets_labels);
			$home['publish']['widgets'] = "1";
			include_once(SERVER_ROOT."/../classes/widgets.php");
			switch($this->subtype)
			{
				case "widget_library":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						if ($widgets_category['library_show_widget'] == 1)
							$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",true,true);
						else
							$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
					$widgets = array();
					$user = $this->PeopleUser();
					if($user['id']>0)
					{
						$wi->MyWidgets($widgets,$user['id'],false,true);
						$mw = array();
						foreach($widgets as $widget)
						{
							$mw['w'.$widget['id_widget']] = $this->WidgetItem($widget);
						}
						$home['widgets']['my_widgets'] = $mw;
					}
				break;
				case "widget_category":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					$category_name = '';
					foreach($widgets_categories as $widgets_category)
					{
						if ($params['id'] == $widgets_category['id_category'])
							$category_name = $widgets_category['name'];
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
					
					$wc = array();
					$widgets = array();
					$num_widgets = $wi->CategoryWidgetsApprovedSelected($widgets, $params['id']);
	                $wc['items'] = $this->Page($widgets,$num_widgets,$wi->widgets_per_page);
					$home['widgets']['widget_category'] = $wc;
					$home['widgets']['widget_category']['name'] = $category_name;
					
					$unescaped_query = $this->th->UtfClean($params['id']); 
	                $this->Urlify($home['widgets']['widget_category'],"homepage",array('id'=>$unescaped_query,'subtype'=>"widget_category"),true);
				break;
				case "mywidgets":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
	
					$user = $this->PeopleUser();
					$mw = array();
					$widgets = array();
					if($user['id']>0)
					{
						if($params['token']!="")
						{
	                        include_once(SERVER_ROOT."/../classes/validator.php");
	                        $va = new Validator(true,0,false);
	                        $trm29 = new Translator($this->tr->id_language,29,false,$this->topic->id_style);
							if ($wi->AcknowledgeWidgetSharing($user['id'],$user['email'],$params['token']))
	                        {
	                            $va->FeedbackAdd("notice",$trm29->Translate("widget_sharing_successful"));   
	                        }
	                        else
	                        {
	                            $va->FeedbackAdd("notice",$trm29->Translate("widget_sharing_failed"));   
	                        }
						}
						
						$num_widgets = $wi->MyWidgets($widgets,$user['id'],true);
						$mw['items'] = $this->Page($widgets,$num_widgets,$wi->widgets_per_page);   
					}
					$home['widgets']['my_widgets'] = $mw;
	                $unescaped_query = 1; 
	                $this->Urlify($home['widgets']['my_widgets'],"homepage",array('id'=>$unescaped_query,'subtype'=>"mywidgets"),true);
				break;
				case"widget_search":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
					$home['widgets']['search'] = $this->WidgetSearch($params['qw'],$params['id']);
				break;
				case"widget_content_search":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
					$home['widgets']['search'] = $this->WebFeedItemSearch($params['qw'],$params['id']);
				break;
				case "widget_create":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
					
					
					if ($params['error'] == 1)
					{
						include_once(SERVER_ROOT."/../classes/session.php");
						$session = new Session();
						
						// error occurs
						if ($params['type'] == 0)
						{
							// manual
							$manual_title = $session->Get("title");
							$manual_description = $session->Get("description");
							$content = $session->Get("content");  
							$link_url = $session->Get("linkurl");
	                        $advance_manual_title = '';
	                        $advance_manual_description = '';
	                        $advance_content = '';  
	                        $advance_link_url = 'http://';
							$rss_title = '';
							$rss_description = '';
							$url = 'http://';
						}
						else if ($params['type'] == 1)
						{
							// rss
							$manual_title = '';
							$manual_description = '';
							$content = '';  
							$link_url = 'http://';
	                        $advance_manual_title = '';
	                        $advance_manual_description = '';
	                        $advance_content = '';  
	                        $advance_link_url = 'http://';
							$rss_title = $session->Get("title");
							$rss_description = $session->Get("description");
							$url = $session->Get("url");
						}
	                    else if ($params['type'] == 3)
	                    {
	                        // advance manual
	                        $manual_title = '';
	                        $manual_description = '';
	                        $content = '';  
	                        $link_url = 'http://';
	                        $advance_manual_title = $session->Get("title");
	                        $advance_manual_description = $session->Get("description");
	                        $advance_content = $session->Get("content");  
	                        $advance_link_url = $session->Get("linkurl");
	                        $rss_title = '';
	                        $rss_description = '';
	                        $url = 'http://';
	                    }
					}
					else
					{
						$manual_title = '';
						$manual_description = '';
						$content = '';
						$link_url = 'http://';
				        $advance_manual_title = '';
	                    $advance_manual_description = '';
	                    $advance_content = '';
	                    $advance_link_url = 'http://';
	            		$rss_title = '';
						$rss_description = '';
						$url = 'http://';
					}
	                $home['widgets']['manual_widget']['title'] = $manual_title;
	                $home['widgets']['manual_widget']['description'] = $manual_description; 
	                $home['widgets']['manual_widget']['content'] = $content;
	                $home['widgets']['manual_widget']['link_url'] = $link_url;
	                $home['widgets']['advance_manual_widget']['title'] = $advance_manual_title;
	                $home['widgets']['advance_manual_widget']['description'] = $advance_manual_description; 
	                $home['widgets']['advance_manual_widget']['content'] = $advance_content;
	                $home['widgets']['advance_manual_widget']['link_url'] = $advance_link_url; 
	                $home['widgets']['rss_widget']['title'] = $rss_title;
	                $home['widgets']['rss_widget']['description'] = $rss_description; 
	                $home['widgets']['rss_widget']['url'] = $url; 
				break;
				case "widget_edit":
					$wi = new Widgets();
					$widgets_categories = $wi->PublicCategories();
					$wcategories = array();
					foreach($widgets_categories as $widgets_category)
					{
						$wcategories["wc".$widgets_category['id_category']] = $this->WidgetCategoryItem($widgets_category,"category",false,false);
					}
					$home['widgets']['categories']  = $wcategories;
				
					// Check whether the user has the permission to edit the widget
					if ($params['id_p'] == 0 || $params['id_widget'] == 0)
					{
						$home['widgets']['widget_edit'] = array();
						$home['widgets']['widget_edit']['valid'] = 0;
					}
					else if (!$wi->IsWidgetEditable($params['id_p'], $params['id_widget']))
					{
						$home['widgets']['widget_edit'] = array();
						$home['widgets']['widget_edit']['valid'] = 0;
					}
					else
					{
						$widget = $wi->PublicWidgetGet($params['id_widget']);
	                    include_once(SERVER_ROOT."/../classes/images.php");
	                    $i = new Images();
                        include_once(SERVER_ROOT."/../classes/file.php");
                        $fm = new FileManager;
	                    if ($widget['widget_type'] == 0)
						{
	                        $filename = "widgets/0/" . $widget['id_widget'] .".".$i->convert_format; 
							if($fm->Exists("uploads/$filename"))
							{
								$widget['image_width'] = $i->img_sizes[WIDGET_IMAGE_SIZE];
								$widget['image_src'] = "{$this->pub_web}/images/$filename?" . time();
							}
						}
		                else if ($widget['widget_type'] == 3)
	                    {
	                        $widget['link_url'] = 'http://'; 
	                        foreach($widget['childs'] as $key => $content)
	                        {
	                            unset($widget['childs'][$key]);
	                            $widget['childs']['child'.$key] = $content;
	                            $widget['childs']['child'.$key]['xname'] = 'child';
	                            $widget['childs']['child'.$key]['content'] = stripcslashes(nl2br($content['content']));
								$filename = "widgets/0/" . $content['id_widget'] .".".$i->convert_format; 
	                            if($fm->Exists("uploads/$filename"))
	                            {
                                    $widget['childs']['child'.$key]['image_width'] = $i->img_sizes[WIDGET_IMAGE_SIZE];
                                    $widget['childs']['child'.$key]['image_src'] = "{$this->pub_web}/images/$filename?" . time();
                                }
	                        }
	                    }
	    				
						$home['widgets']['widget_edit'] = $widget;
						$home['widgets']['widget_edit']['valid'] = ($widget['id_widget'] > 0)? 1 : 0;    
					}
				break;
			}
		}
		if($this->subtype=='keyword') {
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology();
			$keyword = $o->KeywordLandingGet($params['id_keyword']);
			if(isset($keyword['id_keyword'])) {
				$k_array = $this->KeywordItem($keyword);
				$k_array['description'] = $keyword['description'];
				$k_array['content'] = array('xvalue'=>$keyword['content']);
				$k_array['youtube'] = $keyword['youtube'];
				$k_array['id_type'] = $keyword['id_type'];
				$this->Urlify($k_array,"keyword",array('k'=>$keyword['keyword']),true);
				include_once(SERVER_ROOT."/../classes/keyword.php");
				$k = new Keyword();
				$articles = array();
				$num_articles = $k->UseArticlesPaged($articles,$keyword['id_keyword'],0,0);
				$articles_paged = $this->Page($articles,$num_articles);
				$k_array['articles'] = $articles_paged;
				$links = $k->UseLinks($keyword['id_keyword'],0,$this->records_per_page);
				$k_links = array();
				foreach($links as $link) {
					$k_links['l_'.$link['id_link']] = $this->LinkItem($link);
				}
				$k_array['links'] = $k_links;
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$events = $ee->Keyword($keyword['id_keyword'], $this->records_per_page, 0, true);
				$k_events = array();
				foreach($events as $event) {
					$k_events['e_'.$event['id_event']] = $this->EventItem($event,"item");
				}
				$k_array['events'] = $k_events;
				$home['keyword'] = $k_array;
			}
		}
		if($this->subtype=='landing') {
			$home['publish']['landing'] = $params['landing'];
		}
		$home['keywords'] = $this->Keywords(1,"topics_group");
		$home['features'] = $this->Features($this->id_type,0,array(),TRUE);
		return $home;
	}
	
	public function Htmlise( $mytext, $wrap=TRUE )
	{
		return $this->th->Htmlise($mytext,$wrap);
	}

	private function ImageInfo($filename,$width,$height)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		if($width>0 && $height>0)
		{
			$info = array('width'=>$width,'height'=>$height);
			$info['kb'] = $fm->ImageFileSize($filename);
		}
		else 
			$info = $fm->ImageInfo($filename);
		return $info;
	}

	private function ImageItem($image,$name="image")
	{
		$img = array();
		$img['xname'] = $name;
		$img['id'] = $image['id_image'];
		$img['caption'] = $image['caption'];
		$img['align'] = $image['align'];
		$img['width'] = $image['width'];
		$img['height'] = $image['height'];
		$img['size'] = $image['size'];
		$img['zoom'] = $image['zoom'];
		if($this->highslide && $image['zoom'])
			$img['zoom'] = "2";
		$img['author'] = $image['author'];
		$img['format'] = $image['format'];
		$img['source'] = $image['source'];
		$this->Urlify($img,"image",array('id'=>$image['id_image'],'size'=>$image['size'],'format'=>$image['format'],'id_article'=>$image['id_article'],'associated'=>$image['associated']));
		return $img;
	}
	
	private function ImageZoom($id_image,$id_article)
	{
		$img = array();
		if (isset($this->item) && $this->item_type=="image")
		{
			$image = $this->item;
			$image['width'] = $image['orig_width'];
			$image['height'] = $image['orig_height'];
			$img = $this->ImageItem($image);
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article($id_article);
			$image = $a->ImageGet($id_image);
			$img['id'] = $image['id_image'];
			$img['id_article'] = $id_article;
			$img['caption'] = $image['caption_art'];
			$img['source'] = $image['source'];
			$img['width'] = $image['width'];
			$img['height'] = $image['height'];
			$img['author'] = $image['author'];
		}
		$img['author_label'] = $this->tr->Translate("author");
		if ($this->ini->Get("licences") && $image['id_licence'] > 0)
			$img['licence'] = $this->Licence($image['id_licence'],$image['author']);
		$this->Urlify($img,"image_orig",array('id'=>$image['id_image'],'format'=>$image['format']));
		if(isset($this->cache_common))
		{
			$common = $this->cache_common;
			$common['publish']['id'] = $id_image;
		}
		else 
			$common = $this->Common($id_image,$this->id_type);
		return array_merge($common,array('image'=>$img));
	}
	
	private function IsDateOk($date_string)
	{
		return ($date_string != "" && $date_string != "0000-00-00" && $date_string != "1970-01-01");
	}
	
	public function IsModuleActive($module_path)
	{
		$active = false;
		foreach($this->modules as $module)
		{
			if($module['path']==$module_path)
				$active= true;			
		}
		return $active;
	}	
	
	public function Item($item)
	{
		if(array_key_exists("id_topic",$item))
			$this->TopicInit($item['id_topic']);
		switch($item['item_type'])
		{
			case "article":
				$it = $this->ArticleItem($item,"item");
			break;
			case "article_full":
				$article = $this->ArticleContent($item['id_article'],false);
				$article['article']['xname'] = "item";
				$it = $article['article'];
				unset($article);
			break;
			case "asso":
				$it = $this->AssoItem($item,"item");
			break;
			case "audio":
				$it = $this->AudioItem($item,"item");
			break;
			case "audio_enc":
				$it = $this->AudioDetails($item);
			break;
			case "book":
				$it = $this->BookItem($item,"item");
			break;
			case "book_big":
				$it = $this->BookItemBig($item,"item");
			break;
			case "book_category":
				$it = $this->BookCategoryItem($item,"item");
			break;
			case "campaign":
				$it = $this->CampaignItem($item,"item");
			break;
			case "event":
				$it = $this->EventItem($item,"item");
			break;
			case "forum":
				$it = $this->ForumItem($item,"item");
			break;
			case "gallery_image":
				if($this->global)
					$it = $this->GenericImageItem($item,true,true,"item","galleries_image");
				else
					$it = $this->TopicImageItem($item,true,"item");
			break;
			case "galleries";
				$it = $this->GroupItemSmall($item,"item");
			break;
			case "galleries_image";
				$it = $this->GenericImageItem($item,$item['is_thumb'],true,"item","galleries_image");
			break;
			case "gallery";
				if($item['with_details'])
					$it = $this->GalleryItemDetails($item,"item");
				else
					$it = $this->GalleryItem($item,"item");
			break;
			case "image":
				$it = $this->TopicImageItem($item,TRUE,"item");
			break;
			case "keyword":
				$it = $this->KeywordItem($item,"item");
			break;
			case "link":
				$it = $this->LinkItem($item,"item");
			break;
			case "list":
				$it = $this->MailingListItem($item,"item");
			break;
			case "map":
				$it = $this->MapItem($item,"item");
			break;
			case "order":
				$it = $this->EcommerceOrderItem($item,"item");
			break;
			case "meeting":
				$it = $this->MeetingItem($item,"item");
			break;
			case "org":
				$it = $this->OrgItem($item,"item");
			break;
			case "person":
				$it = $this->PersonItem($item,"item");
			break;
			case "poll":
				$it = $this->PollItem($item,"item");
			break;
			case "poll_question":
				$it = $this->PollQuestionItem($item,"item");
			break;
			case "poll_questions_group":
				$it = $this->PollQuestionsGroupItem($item,"item");
			break;
			case "product":
				$it = $this->EcommerceProductItem($item,"item");
			break;
			case "quote":
				$it = $this->QuoteItem($item,"item");
			break;
			case "r_event":
				$it = $this->RecurringEventItem($item,"item");
			break;
			case "subtopic":
				$it = $this->SubtopicItem($item);
			break;
			case "template":
				$it = $this->TemplateItem($item,"item");
			break;
			case "text":
				$it = $this->TextItem($item,"item");
			break;
			case "thread";
				$it = $this->ThreadItem($item,"item");
			break;
			case "topic";
				$it = $this->TopicItem($item,"item");
			break;
			case "topics";
				$it = $this->GroupItemSmall($item,"item");
			break;
			case "video":
				$it = $this->VideoItem($item,"item");
			break;
			case "video_enc":
				$it = $this->VideoDetails($item);
			break;
			case "widget":
				$it = $this->WidgetItem($item,"item");
			break;
			case "web_feed_item":
				$it = $this->WebFeedItem($item,"item");
			break;
			case "xml":
				$it = $item;
			break;
			default:
				$it['xname'] = $item['item_type'];
				foreach($item as $item_key=>$item_value)
				{
					if(!is_numeric($item_key))
						$it[$item_key] = $item_value;
				}
			break;
		}
		unset($item);
		return $it;
	}
	
	private function ItemLabel($item_type)
	{
		switch($item_type)
		{
			case "article":
				$label = $this->tr->Translate("articles");
			break;
			case "asso":
				$label = $this->tr->Translate("associations");
			break;
			case "event":
				$label = $this->tr->Translate("events");
			break;
			case "gallery_image":
				$label = $this->tr->Translate("images");
			break;
			case "link":
				$label = $this->tr->Translate("links");
			break;
			case "book":
				$label = $this->tr->Translate("books");
			break;
			case "campaign":
				$label = $this->tr->Translate("campaigns");
			break;
			case "dodc_contractor":
				$label = "Fornitori";
			break;
			case "dodc_contract":
				$label = "Contratti";
				break;
			case "forum":
				$label = $this->tr->Translate("forums");
			break;
			case "org":
				$label = $this->tr->Translate("orgs");
			break;
			case "person":
				$label = $this->tr->Translate("persons");
			break;
			case "poll":
				$label = $this->tr->Translate("polls");
			break;
			case "poll_question":
				$label = $this->tr->Translate("poll_questions");
			break;
			case "quote":
				$label = $this->tr->Translate("quotes");
			break;
			case "thread":
				$label = $this->tr->Translate("threads");
			break;
			case "video":
				$label = $this->tr->Translate("videos");
			break;
			case "widget":
				$label = "widgets";
			break;
			default:
				$label = $this->tr->Translate("records");
		}
		return $label;
	}
	
	public function ItemSet($type,$item)
	{
		$this->item_type = $type;
		$this->item = $item;
	}
	
	public function ItemUnset()
	{
		unset($this->item_type);
		unset($this->item);
	}
	
	public function Javascript($text,$is_html,$is_async=false,$id_div="")
	{
		if($is_async)
		{
			header("Content-type: text/plain; charset=" . $this->conf->Get("encoding"));
		}
		else 
		{
			header("Content-type: text/javascript; charset=" . $this->conf->Get("encoding"));
			if($is_html)
			{
				$text = $this->th->TextReplaceForJavascript($text,true);
				if($id_div!="")
					$text = "\$('#{$id_div}').append('{$text}');\n";
				else 
					$text = "document.write('$text');\n";
			}
		}
		$this->NoCache();
		return $text;
	}
	
	public function JsonOutput($array)
	{
		$this->NoCache();
		header('Content-type: application/json');
		return $this->JsonEncode($array);
	}

	public function JsonEncode($array,$elements_to_skip=array())
	{
		if(count($elements_to_skip)>0)
		{
			foreach($elements_to_skip as $element)
			{
				unset($array[$element]);
			}
		}
		return json_encode($array,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
	}
	
	private function Keyword($keyword,$id_topic)
	{
		$k_array =  $this->Common(0,$this->id_type);
		if ($id_topic>0)
			$k_array = array_merge($k_array,$this->TopicCommon());
		$this->current_page_global = true;
		$this->current_page_params['k'] = $keyword;
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$row = $k->KeywordGetByString($keyword,0,true);
		if($row['id_keyword']>0)
		{
			$articles = array();
			$num_articles = $k->UseArticlesPaged($articles,$row['id_keyword'],0,$id_topic);
			$articles_paged = $this->Page($articles,$num_articles);
			$keyinfo = array('k'=>$keyword,'id'=>$row['id_keyword'],'description'=>$row['description'],'id_type'=>$row['id_type'],'articles'=>$articles_paged,'tot'=>$num_articles);
			$this->Urlify($keyinfo,"keyword",array('k'=>$keyword),true);
			$k_array['keyword'] = $keyinfo;
			$k_array['features'] = $this->Features($this->id_type,0,array('id_topic'=>$id_topic));
		}
		return $k_array;
	}
	
	private function KeywordItem($item,$name="keyword")
	{
		$k = array();
		$k['xname'] = $name;
		$k['id'] = $item['id_keyword'];
		$k['type'] = "keyword";
		$k['name'] = $item['keyword'];
		if(isset($item['occurr']))
		{
			$k['occurr'] = $item['occurr'];
			$this->Urlify($k,"search",array('k'=>urlencode($item['keyword']),'id_topic'=>$this->topic->id,'id_subtopic'=>$item['id_subtopic']),true);
		}
		return $k;
	}
	
	private function KeywordGeoItem($item)
	{
		$k = $this->KeywordItem($item,"geokeyword");
		$k['name'] = ucwords($k['name']);
		$this->Urlify($k,"geosearch",array('id'=>$item['id_keyword'],'id_topic'=>$this->topic->id),true);
		return $k;
	}

	private function KeywordRss($id_keyword,$feed_type)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology();
		$kfeed = $o->KeywordFeedGet($id_keyword);
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$rss = array();
		$rss['xname'] = "rss";
		$rss['title'] = $kfeed['description'];
		$rss['link'] = $this->pub_web;
		$latest = $k->UseArticles($id_keyword,0,15,0);
		$items = array('counter'=>count($latest));
		foreach($latest as $art)
		{
			$items['a'.$art['id_article']] = $this->RssItem($art,$feed_type);
		}
		$rss['items'] = $items;
		return $rss;
	}

	private function Keywords($id_item,$type,$exclude_internal=true)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$okeywords = array();
		$o->GetKeywords($id_item,$o->types[$type], $okeywords, -1, $exclude_internal);
		$keywords = array();
		foreach($okeywords as $okeyword) {
			$keyword = array('xname'=>"keyword",'id'=>$okeyword['id_keyword'],'name'=>$okeyword['keyword'],'id_type'=>$okeyword['id_type'],'context'=>$type);
			$this->Urlify($keyword,"keyword",array('k'=>$okeyword['keyword']),true);
			$keywords['k_' . $okeyword['id_keyword']] = $keyword;
		}
		return $keywords;
	}

	private function Labels()
	{
		include_once(SERVER_ROOT."/../classes/labels.php");
		$la = new Labels($this->id_type,$this->global,$this->id_style);
		$la->module = $this->module;
		$la->tr = $this->tr;
		if($this->id_module>0)
			$la->id_module = $this->id_module;
		return $la->Translate();
	}

	private function LastUpdate()
	{
		$last_update = array();
		if ($this->topic->id > 0)
		{
			$ts = $this->topic->LastUpdate();
			if ($ts>0)
			{
				$last_update = array('label'=>$this->tr->Translate("lastupdate"),'display_date'=>$this->FormatDate($ts),'display_time'=>$this->FormatTime($ts),'ts'=>$ts);
			}
		}
		return $last_update;
	}

	private function Licence($id_licence,$author)
	{
		$licence = array('id'=>$id_licence);
		$licences = $this->tr->Translate("licences");
		$licence['id'] = $id_licence;
		$licence['label'] = $this->tr->Translate("licence");
		$licence['author'] = $author;
		$licence['name'] = $licences[$id_licence];
		if($author!="" && $id_licence>1)
			$licence['copyright'] = "Copyright &copy; $author";
		$licences_desc = $this->tr->Translate("licences_desc");
		$licence['description'] = array('xvalue'=>(sprintf($licences_desc[$id_licence],$this->pub_web."/licences/".$this->tr->lang)));
		return $licence;
	}

	private function LinkItem($item,$name="item")
	{
		$link = array();
		$link['xname'] = $name;
		$link['type'] = "link";
		$link['id'] = $item['id_link'];
		$link['url'] = $item['url'];
		$link['qs'] = $item['url'];
		$link['title'] = $item['title'];
		$link['vote'] = $item['vote'];
		$link['lang'] = $this->tr->languages[$item['id_language']];
		$link['description'] = array('xvalue'=>$item['description']);
		return $link;
	}

	private function MailingList($id_list)
	{
		include_once(SERVER_ROOT."/../modules/list.php");
		$ml = new MailingList($id_list);
		$list = $ml->GetList();
		$group = array();
		if ($list['id_group']>0)
		{
			include_once(SERVER_ROOT."/../modules/lists.php");
			$li = new Lists();
			$group = $li->GroupGet($list['id_group']);
		}
		$lgroup = $this->MailingListGroupItem($group);
		$mlist = $this->MailingListItem($list);
		$trm11 = new Translator($this->id_language,11,false,$this->topic->id_style);
		$mlist['form_info'] = array('xvalue'=>$trm11->Translate("form_info"));
		if($list['archive']!="")
		{
			$mlist['archive'] = array('label'=>$trm11->Translate("archive"),'url'=>$this->th->String2Url($list['archive']));
		}
		if($list['feed']!="")
		{
			$url = $this->th->String2Url($list['feed']);
			$mlist['feed'] = array('label'=>$trm11->Translate("feed"),'url'=>$url);
			$ttl = $this->conf->Get("rss_ttl");
			include_once(SERVER_ROOT."/../classes/rss.php");
			$r = new Rss();
			$rss = $r->Get($url,$ttl);
			if($this->xh->Check($rss,false))
				$mlist['feed']['rss'] =  array('xname'=>"rss",'xxml'=>$rss);
		}
		$lgroup['lists']['list'] = $mlist;
		return array('list'=>$lgroup);
	}
	
	private function LoginFirst($id_topic)
	{
		$login_first = array();
		$login_first = array('label'=>$this->tr->Translate("login_first"),'name'=>$this->tr->Translate("login"));
		$this->Urlify($login_first,"people",array('subtype'=>"login",'jumpback'=>true,'id_topic'=>$id_topic),true);
		$register = array('name'=>$this->tr->Translate("register"));
		$this->Urlify($register,"people",array('subtype'=>"register",'jumpback'=>true,'id_topic'=>$id_topic),true);
		$login_first['register'] = $register;
		return $login_first;
	}
	
	private function MailingListItem($list,$name="list")
	{
		$ml = array();
		$ml['xname'] = $name;
		if ($name!="list")
			$ml['type'] = "mailinglist_list";
		$ml['id'] = $list['id_list'];
		$ml['name'] = $list['name'];
		$ml['email'] = $list['email'];
		$ml['archive'] = $list['archive'];
		if($list['feed']!="")
			$ml['feed'] = $list['feed'];
		$ml['description'] = array('xvalue'=>$list['description']);
		$this->Urlify($ml,"lists",array('id_list'=>$list['id_list'],'id_topic'=>$this->topic->id,'subtype'=>"list"),true);
		return $ml;
	}
	
	private function MailingListGroupItem($group,$name="group")
	{
		$mlg = array();
		$mlg['xname'] = $name;
		if ($name!="group")
			$mlg['type'] = "mailinglist_group";
		$mlg['id'] = $group['id_group'];
		$mlg['name'] = $group['name'];
		$this->Urlify($mlg,"lists",array('id_group'=>$group['id_group'],'id_topic'=>$this->topic->id,'subtype'=>"group"),true);
		return $mlg;
	}
	
	private function MailingListsGroup($id_group)
	{
		include_once(SERVER_ROOT."/../modules/lists.php");
		$li = new Lists();
		$group = $li->GroupGet($id_group);
		$hgroup = $this->MailingListGroupItem($group);
		$lists = $li->GroupLists($group['id_group']);
		if(count($lists)>0)
		{
			$hlists = array();
			foreach($lists as $list)
				$hlists['l'.$list['id_list']] = $this->MailingListItem($list);
			$hgroup['lists'] = $hlists;
		}
		return array('group'=>$hgroup);
	}
	
	private function MailingListsHome($id_topic)
	{
		$hgroups = array();
		if ($id_topic>0)
		{
			include_once(SERVER_ROOT."/../modules/lists.php");
			$li = new Lists();
			$hlists = $li->Topic($id_topic);
			$lists = array();
			foreach($hlists as $hlist)
				$lists['l'.$hlist['id_list']] = $this->MailingListItem($hlist);
			$hgroup = array('lists'=>$lists);
			$hgroups['group'] = $hgroup;
		}
		else
		{
			include_once(SERVER_ROOT."/../modules/lists.php");
			$li = new Lists();
			$groups = $li->GroupsAll();
			foreach($groups as $group)
			{
				$hgroup = $this->MailingListGroupItem($group);
				$lists = $li->GroupLists($group['id_group']);
				if(count($lists)>0)
				{
					$hlists = array();
					foreach($lists as $list)
						$hlists['l'.$list['id_list']] = $this->MailingListItem($list);
					$hgroup['lists'] = $hlists;
				}
				$hgroups['g'.$group['id_group']] = $hgroup;
			}
		}
		return $hgroups;
	}
	
	private function MailingListsWrapper($params)
	{
		if ($this->subtype=="" )
		{
			if ($params['subtype']!="")
				$this->subtype = $params['subtype'];
			else
			{
				$this->subtype = "home";
				if ($params['id_g']>0)
					$this->subtype = "group";
				if ($params['id']>0)
					$this->subtype = "list";
			}
		}
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		switch($this->subtype)
		{
			case "home":
				$ml = $this->MailingListsHome($params['id_topic']);
			break;
			case "group":
				$ml = $this->MailingListsGroup($params['id_g']);
			break;
			case "list":
				$ml = $this->MailingList($params['id']);
			break;
		}
		$this->Urlify($ml,"lists",array('id_topic'=>$params['id_topic']),true);
		$ml['submit'] = $this->pub_web . "/" . $this->ini->Get("lists_path") . "/actions.php";
		$pagetype['mailing_lists'] = $ml;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_list'=>$params['id'],'id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function Map($id_group)
	{
		$map = $this->Common($id_group,$this->id_type);
		$map['features'] = $this->Features($this->id_type,0,array(),TRUE);
		include_once(SERVER_ROOT."/../classes/topics.php");
		include_once(SERVER_ROOT."/../classes/galleries.php");
		if ($id_group>0)
		{
			$this->groups = new Topics;
			$this->groups->gh->LoadTree();
			$group = $this->groups->gh->GroupGet($id_group);
			$group['item_menu'] = true;
			$map['topics'] = array('group'=>$this->GroupItem($group,true));
			if(isset($map['topics']['group']) && isset($map['topics']['group']['topics']) && count($map['topics']['group']['topics'])>0) {
				include_once(SERVER_ROOT."/../modules/lists.php");
				foreach($map['topics']['group']['topics'] as &$topic) {
					$this->TopicInit($topic['id']);
					$lists = array();
					$latest = array();
					// lists
					$li = new Lists();
					$hlists = $li->Topic($topic['id']);
					if(count($hlists)>0) {
						foreach($hlists as $hlist) {
							$lists['l'.$hlist['id_list']] = $this->MailingListItem($hlist);
						}
					}
					$topic['lists'] = $lists;
					// latest
					$articles = $this->topic->Latest($this->topic->records_per_page,0);
					foreach($articles as $art)
					{
						$art_item = $this->ArticleItem($art,'item');
						$latest['a_' . $art['id_article']] = $art_item;
					}
					$topic['latest'] = $latest;
				}
			}
		}
		else
		{
			$this->groups = new Galleries;
			$map['galleries'] = $this->GroupTree(0);
			$this->groups = new Topics;
			$map['topics'] = $this->GroupTree(0);
		}
		$latest = $this->groups->Latest($id_group,$this->ini->Get("map_sort_by"));
		$counter = 0;
		$items = array();
		foreach($latest as $item)
		{
			$items['i_'.$counter] = $this->Item($item);
			$counter ++;
		}
		unset($latest);
		$map['latest'] = $items;
		unset($items);
		return $map;
	}
	
	private function MapItem($item,$name="map")
	{
		$group = array();
		$group['xname'] = $name;
		if ($name!="map")
			$group['type'] = "map";
		$group['id'] = $item['id_group'];
		$group['name'] = $item['name'];
		$group['type'] = $item['type'];
		$group['description'] = $item['description'];
		$this->Urlify($group,"map",array('id'=>$item['id_group'],'type'=>$item['type']));
		return $group;
	}

	private function MeetingsWrapper($params)
	{
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$topic);
		include_once(SERVER_ROOT."/../classes/meetings.php");
		$me = new Meetings();
		$trm23 = new Translator($this->tr->id_language,23,false,$this->topic->id_style);
		$meet = array('label'=>$trm23->Translate("meetings"));
		if($this->subtype=="")
			$this->subtype = "home";
		switch($this->subtype)
		{
			case "home":
				$meetings = array();
				$this->Urlify($meetings,"meeting",array(),true);
				$rows = array();
				$num = $me->MeetingsAll($rows,$this->topic->id,true,false);
				$meetings['items'] = $this->Page($rows,$num);
				$meet['meetings'] = $meetings;
			break;
			case "meeting":
			case "participants":
			case "subscribe":
				$row = $me->MeetingGet($params['id']);
				if($row['status']=="0" || ($row['id_topic']>0 && $row['id_topic']!=$this->topic->id))
					$this->Stop();
				$user = $this->PeopleUser();
				if(!$user['id']>0)
				{
					$meet['login'] = $this->LoginFirst($row['id_topic']);
				}
				else 
				{
					$is_user_invited = $me->IsUserInvited($params['id'],$user['id']);
					if($row['is_private']=="1" && !$is_user_invited)
					{
						include_once(SERVER_ROOT."/../classes/validator.php");
						$va = new Validator(true,0,false);
						$va->FeedbackAdd("error",$trm23->Translate("not_authorized"),array(),false);
					}
					else 
					{
						$meet_item = $this->MeetingItem($row,"meeting");
						if($row['status']=="2")
						{
							$meet_item['over_label'] = $trm23->Translate("meeting_over");
						}
						$meet_item['is_private'] = $row['is_private'];
						$is_user_pending = $me->IsUserPending($params['id'],$user['id']);
						$meet_item['is_user_invited'] = $is_user_invited?"1":"0";
						$slots = array();
						$num_slots = $me->MeetingSlots($slots,$params['id'],false);
						$mslots = array('num'=>$num_slots);
						foreach($slots as $slot)
						{
							$slot_array = array('xname'=>"slot");
							$slot_array['id'] = $slot['id_meeting_slot'];
							$slot_array['start_date'] = $this->FormatDate($slot['start_date_ts']);
							$slot_array['start_time'] = $this->FormatTime($slot['start_date_ts']);
							$slot_array['length'] = $slot['length'];
							$slot_array['title'] = $slot['title'];
							$slot_array['description'] = array('xvalue'=>$slot['description']);
							if($is_user_invited)
							{
								$participants = array();
								$num_participants = $me->MeetingParticipants($participants,$slot['id_meeting_slot'],4,false);
								$slot_array['num_participants'] = $num_participants;
								if(count($participants)>0)
								{
									$spart = array();
									foreach($participants as $participant)
									{
										$spart['p'.$participant['id_p']] = array('xname'=>"participant",'id'=>$participant['id_p'],'name'=>$participant['name']);
									}
									$slot_array['participants'] = $spart;
								}
								$user_slot = $me->ParticipantSlotGet($slot['id_meeting_slot'],$user['id']);
								$suser = array('xname'=>"user",'status'=>$user_slot['status']);
								if($user_slot['comments']!="")
								{
									$suser['comments'] = array('xvalue'=>$user_slot['comments']);
								}
								$slot_array['user'] = $suser;
							}
							$mslots['sl_'.$slot['id_meeting_slot']] = $slot_array;
						}
						$meet_item['slots'] = $mslots;
						if($is_user_invited)
						{
							$part_array = array('label'=>$trm23->Translate("participants"));
							$this->Urlify($part_array,"meet",array('id'=>$params['id'],'id_topic'=>$this->topic->id,'subtype'=>"participants"),true);
							$meet_item['participants'] = $part_array;
							if(!$is_user_pending && $row['status']=="1")
							{
								$part_array = array('label'=>$trm23->Translate("participation"));
								$this->Urlify($part_array,"meet",array('id'=>$params['id'],'id_topic'=>$this->topic->id,'subtype'=>"subscribe"),true);
								$meet_item['subscribe'] = $part_array;		
							}
						}
						elseif($row['is_private']=="0" && !$is_user_pending && $row['status']=="1")
						{
							$part_array = array('label'=>$trm23->Translate("subscribe"));
							$this->Urlify($part_array,"meet",array('id'=>$params['id'],'id_topic'=>$this->topic->id,'subtype'=>"subscribe"),true);
							$meet_item['subscribe'] = $part_array;
						}
						$statuses = $trm23->Translate("participation_status");
						$mstatuses = array();
						foreach($statuses as $key=>$status)
						{
							if($key>1)
								$mstatuses['s_'.$key] = array('xname'=>"status",'id'=>$key,'value'=>$status);
						}
						$meet_item['statuses'] = $mstatuses;
						$meet['meeting'] = $meet_item;				
					}
					$meet['submit'] = $this->pub_web . "/meet/actions.php";
				}
			break;
		}
		$pagetype['meet'] = $meet;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_topic'=>($this->topic->id)));
		return $pagetype;
	}
	
	private function MeetingItem($item,$name="meeting")
	{
		$id_meeting = $item['id_meeting'];
		$meeting = array();
		$meeting['xname'] = $name;
		$meeting['id'] = $item['id_meeting'];
		$meeting['type'] = "meeting";
		$meeting['title'] = $item['title'];
		$meeting['description'] = array('xvalue'=>$item['description']);
		$meeting['status'] = $item['status'];
		$this->Urlify($meeting,"meet",array('id'=>$id_meeting,'id_topic'=>$item['id_topic'],'subtype'=>"meeting"),true);
		return $meeting;
	}
	
	private function MonthSummary($ts)
	{
		$ee = new Events();
		if($this->topic->id>0)
			$ee->id_topic = $this->topic->id;
		$doms = array();
		$dom_rows = $ee->EventsMonth($ts);
		foreach($dom_rows as $dom_row)
		{
			if(isset($doms[$dom_row['dom']]))
				$doms[$dom_row['dom']] = $doms[$dom_row['dom']] + 1;
			else 
				$doms[$dom_row['dom']] = 1;
		}
		$month = array();
		$today_ts_midnight = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$offset = round(($ts - $today_ts_midnight)/SECONDS_PER_DAY);
		$current_dom = $this->dt->DayOfMonth($ts);
		$first_ts = $ts - (($current_dom)*SECONDS_PER_DAY) +SECONDS_PER_DAY;
		$current_wday = $this->dt->Dow($first_ts);
		$monday_ts = $first_ts - (($current_wday)*SECONDS_PER_DAY) + SECONDS_PER_DAY;
		$monday_offset = round(($monday_ts - $today_ts_midnight)/SECONDS_PER_DAY);
		$days = date("t",$first_ts);
		$days_prev = date("t",$first_ts - SECONDS_PER_DAY);
		$weeks = ceil(($days + (($first_ts - $monday_ts)/SECONDS_PER_DAY))/7);
		$month['n'] = $this->dt->MonthNumber($ts);
		$month['name'] = $this->dt->Month($ts);
		$month['year'] = date("Y",$ts);
		$month_ts = mktime(0,0,0,date("m",$ts),1,date("Y",$ts));
		$this->Urlify($month,"events",array('subtype'=>"month",'id_topic'=>$this->topic->id,'month'=>(date("Ym",$ts))),true);
		$month_prev = array();
		$this->Urlify($month_prev,"events",array('subtype'=>"month",'id_topic'=>$this->topic->id,'month'=>(date("Ym",strtotime("-1 month",$month_ts)))),true);
		$month['prev'] = $month_prev;
		$month_next = array();
		$this->Urlify($month_next,"events",array('subtype'=>"month",'id_topic'=>$this->topic->id,'month'=>(date("Ym",strtotime("+1 month",$month_ts)))),true);
		$month['next'] = $month_next;
		if($this->conf->Get('calendar_version')==1) {
			for($j=0;$j<$weeks;$j++)
			{
				$week = array();
				$week['xname'] = "week";
				for($i=0;$i<7;$i++)
				{
					$tsd = $monday_ts + ($i*SECONDS_PER_DAY) + ($j*SECONDS_PER_DAY*7);
					$offset2 = $monday_offset + $i + ($j*7);
					$dom = $this->dt->DayOfMOnth($tsd);
					$day = array();
					$day['xname'] = "day";
					$day['dom'] = $dom;
					if (($j==0 && $dom>7) || ($j==$weeks-1 && $dom<8))
						$day['status'] = "off";
						else
						{
							$day['status'] = ($offset2==$offset)? "current" : "on";
							$day['events'] = isset($doms[$dom])? $doms[$dom] : 0;
						}
						$this->Urlify($day,"events",array('subtype'=>"day",'id_topic'=>$this->topic->id,'offset'=>$offset2),TRUE);
						$week['day_' . $i] = $day;
				}
				$month['w_' . $j] = $week;
			}
		}
		return $month;
	}

	private function MediaWrapper($params,$id_topic)
	{
		$topic = ($params['id_topic']>0)? $this->TopicCommon() : array();
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$topic);
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		include_once(SERVER_ROOT."/../classes/video.php");
		include_once(SERVER_ROOT."/../classes/audio.php");
		$videos = array();
		$this->Urlify($videos,"media",array('subtype'=>'home','id_topic'=>$this->topic->id),true);
		switch ($this->subtype)
		{
			case "audio":
				$au = new Audio();
				$audios = array();
				$this->Urlify($audios,"media",array('subtype'=>'audios','id_topic'=>$this->topic->id),true);
				if($au->IsHash($params['hash']))
				{
					$this->current_page_params['hash'] = $params['hash'];
					$arow = $au->AudioGetByHash($params['hash']);
					$id_audio = (int)$arow['id_audio'];
					if($id_audio>0)
					{
						$pagetype['audio'] = $this->AudioDetails($arow,true);
					}
				}
				else
				{
					$rows = array();
					$num_audios = $au->AudiosAll($rows,$id_topic,true,true);
					$audios['items'] = $this->Page($rows,$num_audios);
				}
				$pagetype['audios'] = $audios;
			break;
			case "video":
				$vi = new Video();
				if($vi->IsHash($params['hash']))
				{
					$this->current_page_params['hash'] = $params['hash'];
					$vrow = $vi->VideoGetByHash($params['hash']);
					$id_video = (int)$vrow['id_video'];
					if($id_video>0)
					{
						$pagetype['video'] = $this->VideoDetails($vrow,true);
					}
				}
			break;
			case "home":
				$vi = new Video();
				$rows = array();
				$num_videos = $vi->VideosAll($rows,$id_topic,true,true);
				$videos['items'] = $this->Page($rows,$num_videos);
			break;
		}
		$pagetype['videos'] = $videos;
		$pagetype['features'] = $this->Features($this->id_type,$this->id_module,array('id_topic'=>$id_topic));
		return $pagetype;
	}
	
	private function Navigation($id_current_subtopic)
	{
		$this->topic->LoadTree(false,$this->live && !$this->preview);
		$children = $this->topic->SubtopicChildren(0);
		$menu = array();
		$menu['depth'] = $this->topic->row['menu_depth'];
		$menu['subtopics'] = $this->NavigationRecurse($children,$id_current_subtopic);
		unset($children);
		$menu_footer = $this->topic->TextGet("menu_footer");
		$menu['menu_footer'] = $this->ExplodeMarkers($menu_footer['is_html']? $menu_footer['content'] : $this->Htmlise($menu_footer['content']));
		return $menu;
	}
	
	private function NavigationRecurse($subtopics, $id_current_subtopic)
	{
		$subs = array();
		foreach($subtopics as $subtopic)
		{
			if ($subtopic['visible']=="1")
			{
				$sub = $this->SubtopicItem($subtopic);
				$sub['xname'] = "subtopic";
				if ($subtopic['id_subtopic']==$id_current_subtopic)
					$sub['selected'] = "true";
				$children = $this->topic->SubtopicChildren($subtopic['id_subtopic']);
				if (count($children)>0)
					$sub['subtopics'] = $this->NavigationRecurse($children,$id_current_subtopic);
				unset($children);
				$subs['s_' . $subtopic['id_subtopic']] = $sub;
			}
		}
		return $subs;
	}

	private function Newsletter($params,$is_html)
	{
		$topic = $this->TopicCommon(false);
		$t = $this->topic;
		$articles = $t->LatestArticles($params['timestamp']);
		$latest = array();
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$attachments = 0;
		foreach($articles as $art)
		{
			$art_item = $this->ArticleItem($art,"item");
			$art_item['xname'] = "article";
			$art_item['halftitle']['xvalue'] = trim($art_item['halftitle']['xvalue']);
			$art_item['headline']['xvalue'] = trim($art_item['headline']['xvalue']);
			$art_item['subhead']['xvalue'] = trim($art_item['subhead']['xvalue']);
			$art_item['headline_ucase'] = array('xvalue'=>trim($this->th->Text2Upper($art['headline'])));
			$latest['a_' . $art['id_article']] = $art_item;
		}
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ee->id_topic = $t->id;
		$ee->id_group = $t->id_group;
		if ($params['seconds'] >0)
			$next_ts = time() + $params['seconds'];
		else
		{
			$next_ts = time() + FrequencySeconds($t->row['id_frequency']);
		}
		$events = $ee->Next(15,0,false,$next_ts);
		$next = array();
		foreach($events as $event)
		{
			$next['e' . $event['id_event']] = $this->EventItem($event);
		}
		if ((count($articles) + count($events))>0)
			$this->newsletter_ready = true;
		$nl = array('xname'=>"newsletter",'ts'=>$params['timestamp'],'last_date'=>$this->FormatDate($params['timestamp']),'tot_articles'=>count($articles),'tot_events'=>count($events),'next_ts'=>$next_ts,'next_date'=>$this->FormatDate($next_ts));
		$nl['format'] = $is_html? "html" : "text";
		$nl['articles'] = $latest;
		$nl['events'] = $next;
		$feature_params = array('id_topic'=>$t->id,'id_topic_group'=>$t->row['id_group']);
		$nl['features'] = $this->Features($this->id_type,0,$feature_params);
		$pagetype = array_merge($this->Common($t->id,$this->id_type),$topic);
		$pagetype['newsletter'] = $nl;
		return $pagetype;
	}
	
	private function NewsletterGlobal()
	{
		$lastnewsletter_ts = $this->ini->GetTimestamp("lastnewsletter");
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		$next_ts = time() + FrequencySeconds(($mail->wday_newsletter == "0")?1:2);
		// TODO Should take into account the next weekday
		$nl = array('xname'=>"newsletter",'last_date'=>$this->FormatDate($lastnewsletter_ts),'next_date'=>$this->FormatDate($next_ts));
		$tot_articles = 0;
		include_once(SERVER_ROOT."/../classes/topics.php");
		$this->groups = new Topics;
		$this->groups->gh->LoadTree();
		$groups = $this->groups->gh->GroupsAll(true);
		foreach($groups as $group)
		{
			$g = array();
			$tot_garticles = 0;
			$g['xname'] = "group";
			$g['id'] = $group['id_group'];
			$g['name'] = $group['name'];
			$g['description'] = $this->groups->gh->th->description[$group['id_group']];
			$this->Urlify($g,"map",array('id'=>$group['id_group'],'type'=>($this->groups->gh->type)),TRUE);

			$rows = $this->groups->gh->GroupItems($group['id_group']);
			$items = array();
			foreach($rows as $row)
			{
				if ($row['visible'])
				{
					$this->TopicInit($row['id_item']);
					$articles = $this->topic->LatestArticles($lastnewsletter_ts);
					$tot_articles += count($articles);
					$tot_garticles += count($articles);
					$latest = array();
					foreach($articles as $art)
					{
						$art_item = $this->ArticleItem($art);
						$art_item['headline_ucase'] = array('xvalue'=>$this->th->Text2Upper($art['headline']));
						$latest['a_' . $art['id_article']] = $art_item;
					}
					$latest['tot_articles'] = count($articles);
					$items['topic'.$row['id_item']] = array_merge($this->TopicItem($this->topic->row),$latest);
				}
			}
			$g['tot_articles'] = $tot_garticles;
			$g['topics'] = $items;
			$nl['group' . $group['id_group']] = $g;
		}
		$this->TopicInit(0);
		$nl['tot_articles'] = $tot_articles;
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$events = $ee->Next(15,0,true,$next_ts);
		$nl['tot_events'] = count($events);
		$next = array();
		foreach($events as $event)
		{
			$next['e' . $event['id_event']] = $this->EventItem($event);
		}
		$nl['events'] = $next;
		$nl['features'] = $this->Features($this->id_type,0,array());
		$pagetype = $this->Common(0,$this->id_type);
		$pagetype['newsletter'] = $nl;
		$this->newsletter_ready = $tot_articles>0 && $tot_garticles>0 && count($events)>0;
		return $pagetype;
	}

	public function NoCache()
	{
		header("Expires: Wed, 18 May 1967 06:55:00 GMT");
		header("Last-Modified: ". gmdate("D, d M Y H:i:s"). " GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Cache-Control: post-check=0,pre-check=0");
		header("Cache-Control: max-age=0");
		//header("Pragma: no-cache");
	}

	private function OrgItem($item,$name="org")
	{
		$org = array();
		$org['xname'] = $name;
		$org['id'] = $item['id_org'];
		$org['type'] = "org";
		$org['name'] = $item['name'];
		$org['email'] = $item['email'];
		$org['geo'] = $item['geo_name'];
		if ($item['comments']!="")
			$org['comments'] = array('xvalue'=>$item['comments']);
		$org['insert_date'] = $this->FormatDate($item['insert_date_ts']);
		return $org;
	}
	
	public function Output($type,$id,$id_topic,$page,$params,$is_global=false)
	{
		if ($is_global)
		{
			$xml = $this->PageTypeGlobal($this->pt->gtypes[$type],$id,$page,$params);
			$id_style = 0;
		}
		else
		{
			$id_type = $this->pt->types[$type];
			if ($id_type==0 && $params['module']!="")
			{
				$type = $params['module'];
			}
			if($this->IsModuleActive("books") && ($type=="books"))
			{
				$force_topic = $this->ini->GetModule("books","id_topic",0);
				if($force_topic>0)
					$id_topic = $force_topic;
			}
			$xml = $this->PageType($id_type,$id_topic,$id,$page,$params);
		}
		if($this->conf->Get("debug") && isset($_GET['xml']))
		{
			header('Content-type: text/xml');
			return $xml;
		}
		else
		{
			$id_style = (int)$this->id_style;
			$xhtml = $this->xh->Transform($type,$xml,$id_style);
			$this->DoctypeHTML5Fix($xhtml);
			return $xhtml;
		}
	}

	private function Page($page_items,$tot_items,$items_per_page=0)
	{
		$page = $GLOBALS['current_page'];
		if ($items_per_page==0)
			$items_per_page = ($this->topic->records_per_page>0)? $this->topic->records_per_page : $this->records_per_page_default;
		$tot_pages = floor(($tot_items-1)/$items_per_page)+1;
		$items = array('page'=>$page,'tot_pages'=>$tot_pages,'tot_items'=>$tot_items,'items_per_page'=>$items_per_page);
		$counter = 1;
		reset($page_items);
		foreach($page_items as $item)
		{
			$current_item = $this->Item($item);
			$current_item['seq'] = $counter;
			$pos = (($page-1) * $items_per_page) + $counter;
			$current_item['pos'] = $pos;
			$current_item['rev_pos'] = $tot_items - $pos + 1;
			$items['i_'.$counter] = $current_item;
			$counter++;
			$type = $item['item_type'];
		}
		$items['label'] = $this->ItemLabel($type);   
		return $items;
	}
	
	private function PageItems($page_items)
	{
		$items = array();
		$counter = 1;
		foreach($page_items as $page_item)
		{
			$current_item = $this->Item($page_item);
			$current_item['seq'] = $counter;
			$type = $page_item['item_type'];
			$items['i_'.$counter] = $current_item;
			$counter++;
		}
		$items['label'] = $this->ItemLabel($type);
		return $items;
	}
	
	private function Pager($items_to_page,$items_per_page=0)
	{
		if(!$items_per_page>0)
			$items_per_page = ($this->records_per_page>0)? $this->records_per_page : $this->records_per_page_default;
		$items_groups = array_chunk($items_to_page,$items_per_page);
		$GLOBALS['current_page'] = 1;
		$tot_items = count($items_to_page);
		$return_groups = array();
		reset($items_groups);
		foreach($items_groups as $page_items)
		{
			$return_groups[] = $this->Page($page_items,$tot_items,$items_per_page);
			$GLOBALS['current_page'] ++;
		}
		return $return_groups;
	}

	public function PageType($id_type,$id_topic,$id,$page,$params=array())
	{
		$this->global = FALSE;
		$this->subtype = "";
		$this->module = "";
		$this->TopicInit($id_topic,true);
		if($this->live && $this->topic->protected=="1")
		{
			$this->TopicAuth();
		}
		$this->id_type = $id_type;
		$this->id_style = (($this->topic->id_style)>0)? ($this->topic->id_style):0;
		$current_topic = $this->topic;
		$this->current_page_params['id_topic'] = $this->topic->id;
        if(array_key_exists('subtopic_type', $params))
        {
            $this->current_page_params['subtopic_type']='form';    
        }
		$this->current_page_params['id'] = $id;
		$this->current_page_global = false;
		if ($this->preview)
		{
			$this->xml_link = "/layout/xml.php?id_type=$id_type&id=$id&id_topic=$id_topic";
			foreach($params as $key=>$value)
			{
				if(is_array($value))
					$this->xml_link .= $this->th->UrlEncodeArray($key,$value);
				elseif ($key!="id_type" && $key!="id" && $key!="id_topic")
					$this->xml_link .= "&$key=$value";
			}
			if ($page>0)
				$this->xml_link .= "&p=$page";
			if($params['id_style']>0)
			{
				$this->id_style = $params['id_style'];
				$this->irl->force_id_style = $params['id_style'];
			}
		}
		if ($params['subtype']!="")
			$this->subtype = $params['subtype'];
		if (!($page>0))
			$page = 1;
		$GLOBALS['current_page'] = $page;
		$additional_params = array();
		switch($id_type)
		{
			case "0":
				$this->module = $params['module'];
				if($params['id_module']>0)
				{
					if($params['module']!="")
					{
						if($this->IsModuleActive($params['module']))
						{
							$this->id_module = $params['id_module'];
							$params['id'] = $id;
							$params['id_topic'] = $id_topic;
							switch($params['module'])
							{
								case "books":
									$pagetype = $this->BooksWrapper($params,$page);
								break;
								case "lists":
									$pagetype = $this->MailingListsWrapper($params);
								break;
								case "dodc":
									$pagetype = $this->DodcWrapper($params);
								break;
								case "orgs":
									$pagetype = $this->AssoWrapper($params,$page);
								break;
								case "meet":
									$pagetype = $this->MeetingsWrapper($params);
								break;
								case "media":
									$pagetype = $this->MediaWrapper($params,$id_topic);
								break;
								case "ecommerce":
									$pagetype = $this->EcommerceWrapper($params,$page);
								break;
								case "ebook":
									$pagetype = $this->EbookWrapper($params);
								break;
							}
						}
						else 
							$this->Stop();
					}
					else 
						UserError("Module path missing",array('id_module'=>$params['id_module']));
				}
			break;
			case "1":
				$pagetype = $this->TopicHome();
			break;
			case "2":
				$subtopic_pages = $this->Subtopic($id,$params);
				$pagetype = $subtopic_pages[$page - 1];
				$additional_params = array('page' => $page-1);
                if($params['id_module']>0)
                {
                    if($params['module']!="")
                    {
                        if($this->IsModuleActive($params['module']))
                        {
                            switch($params['module'])
                            {
                            }
                        }
                    }
                }
			break;
			case "3":
				$pagetype = $this->Article($id);
			break;
			case "4":
				$pagetype = $this->ArticleBox($id);
			break;
			case "5":
				$params['id'] = $id;
				$params['id_topic'] = $id_topic;
				$pagetype = $this->EventsWrapper($params);
			break;
			case "6": // quotes
				$params['id'] = $id;
				$params['id_topic'] = $id_topic;
				$pagetype = $this->QuotesWrapper($params);
			break;
			case "7": // campaign
				$params['id'] = $id;
				$params['id_topic'] = $id_topic;
				$pagetype = $this->CampaignWrapper($params);
			break;
			case "8": // forum
				$params['id'] = $id;
				$params['id_topic'] = $id_topic;
				$pagetype = $this->ForumWrapper($params);
			break;
			case "9": // polls
				$params['id'] = $id;
				$params['id_topic'] = $id_topic;
				$pagetype = $this->PollWrapper($params);
			break;
			case "10":
				$pagetype = $this->GalleryImage($id,$params['id_gallery'],$params['id_subtopic']);
			break;
			case "11": // people
				if($this->profiling)
				{
					$params['id'] = $id;
					$params['id_topic'] = $id_topic;
					$pagetype = $this->PeopleWrapper($params);
				}
			break;
			case "12": // newsletter
				$pagetype = $this->Newsletter($params,$params['is_html']);
			break;
			case "13": // geosearch
				$pagetype = $this->GeoSearch($id,$id_topic);
			break;
			case "14": // print
				$pagetype = $this->Article($id);
			break;
			case "15": // send friend
				$pagetype = $this->FriendSend($id);
			break;
			case "16":
				$pagetype = $this->ImageZoom($id,$params['id_article']);
			break;
			case "17": // error 404
				$pagetype = $this->Error404($params);
			break;
			case "18": // search
				$pagetype = $this->Search($params['q'],$params['k'],$params['id_template'],$params['id_group'],$id_topic,$params['id_subtopic']);
			break;
			case "19":
				$pagetype = $this->RandomItem($params['subtype'],$params);
			break;
			case "22":
				$pagetype = $this->CommentsWrapper($params['id_r'],$params['id_item'],$params);
			break;
		}
		$type = ($this->module!="")? $this->module : array_search($id_type,($this->global)? $this->pt->gtypes: $this->pt->types);
		$this->topic = $current_topic;
		if(!($this->IsStatic()) && $this->topic->id>0)
		{
			$module_subtopic = $this->PageTypeSubtopic($id_type,$id);
			if(count($module_subtopic)>0)
				$pagetype['module_subtopic'] = $module_subtopic;		
		}
		if($id_type!="15" && $id_type!="17" && $id_type!="19" && $type!="ebook")
		{
			$pagetype['page'] = $this->CurrentPage($type,$this->current_page_global,$additional_params);
			if($this->conf->Get("ui")) {
				$this->StructuredData($pagetype);
			}
		}
		$pagetype['feedback'] = $this->FeedBack();
		return $this->xh->Array2Xml($pagetype);
	}
	
	private function PageTypeSubtopic($id_type,$id_item)
	{
		$module_subtopic = array();
		$id_subtopic_type = $this->PageType2SubtopicType($id_type);
		if($id_subtopic_type>0)
		{
			$id_subtopic = $this->topic->HasSubtopicTypeItem($id_subtopic_type,$id_item);
			if($id_subtopic>0)
			{
				$subtopic = $this->topic->SubtopicGet($id_subtopic);
				if(!$this->preview && $subtopic['visible']=="4")
					$this->Stop();
				else $module_subtopic = $this->SubtopicItem($subtopic);
			}
		}
		return $module_subtopic;
	}
	
	private function PageType2SubtopicType($id_type)
	{
		$subtopic_types = array();
		$subtopic_types[0] = "12";
		$subtopic_types[5] = "10";
		$subtopic_types[6] = "14";
		$subtopic_types[7] = "7";
		$subtopic_types[8] = "8";
		$subtopic_types[9] = "17";
		$subtopic_types[18] = "16";
		return (isset($subtopic_types[$id_type]))? $subtopic_types[$id_type] : 0;
	}

	public function PageTypeGlobal($id_type,$id,$page,$params=array())
	{
		$this->global = TRUE;
		$this->TopicInit(0,true);
		$this->id_type = $id_type;
		$this->id_style = 0;
		$this->subtype = "";
		if ($this->preview)
		{
			$this->xml_link = "/layout/xml.php?id_gtype=$id_type&id=$id";
			foreach($params as $key=>$value)
			{
				if(is_array($value))
					$this->xml_link .= $this->th->UrlEncodeArray($key,$value);
				elseif ($key!="id_gtype" && $key!="id")
						$this->xml_link .= "&$key=$value";
			}
			if ($page>0)
				$this->xml_link .= "&p=$page";
			if($params['id_style']>0)
			{
				$this->id_style = $params['id_style'];
				$this->irl->force_id_style = $params['id_style'];
			}
		}
		if ($params['subtype']!="")
			$this->subtype = $params['subtype'];
		if (!($page>0))
			$page = 1;
		$this->current_page_params['id'] = $id;
		$this->current_page_global = true;
		$GLOBALS['current_page'] = $page;
		switch($id_type)
		{
			case "0": //homepage
				$pagetype = $this->Home($params);
			break;
			case "1": // rss
				$id = (int)$id;
				$pagetype = $this->Rss($id,$params['subtype']);
			break;
			case "2": //map group
				$id = (int)$id;
				$pagetype = $this->Map($id);
			break;
			case "3": // feeds
				$pagetype = $this->Feeds();
			break;
			case "4": //gallery group
				$pagetype = $this->GalleryWrapper($params,$page);
			break;
			case "5": //user
				$pagetype = $this->User($params);
			break;
			case "6": //global newsletter
				$pagetype = $this->NewsletterGlobal();
			break;
			case "9": //keyword
				$pagetype = $this->Keyword($params['k'],0);
			break;
		}
		$pagetype['page'] = $this->CurrentPage(array_search($id_type,$this->pt->gtypes),true);
		if($this->conf->Get("ui")) {
			$this->StructuredData($pagetype);
		}
		$pagetype['feedback'] = $this->FeedBack();
		return $this->xh->Array2Xml($pagetype);
	}

	private function ParamValue($type,$type_params,$value)
	{
		$pvalue = $value;
		if($type=="date")
			$pvalue = $this->FormatDate($value);
		if($type=="mchoice")
			$pvalue = str_replace(",",", ",$value);
		if($type=="checkbox")
			$pvalue = ($value=="1")? $this->tr->Translate("yes"):$this->tr->Translate("no");
		if($type=="link")
		{
			$link = $this->th->String2Url($value);
			$pvalue = "<a href=\"$link\">$link</a>";
		}
		if($type=="geo")
		{
			$geo = new Geo();
			$pvalue = $geo->GeoGet($value,0);
		}
		if($type=="textarea" && $type_params=="1")
			$pvalue = $this->Htmlise($value,false);
		return $pvalue;
	}
	
	private function ParseMarkers($textWithMarkers,$marker)
	{
		$markers = array();
		$marker_open = "[[$marker";
		$marker_close = "]]";
		$tot_markers = substr_count($textWithMarkers,$marker_open);
		$pos1 = 0;
		$pos2 = 0;
		for ($i=0;$i<$tot_markers;$i++)
		{
			$pos1 = strpos($textWithMarkers,$marker_open,$pos2);
			$pos2 = strpos($textWithMarkers,$marker_close,$pos1+1);
			$markers[$i] = substr($textWithMarkers,$pos1+strlen($marker_open),$pos2-$pos1-strlen($marker_open));
		}
		return $markers;
	}
	
	private function PaymentItem($item,$name="payment")
	{
		$payment = array();
		$payment['xname'] = $name;
		$payment['id'] = $item['id_payment'];
		$payment['type'] = "payment";
		$payment['date'] = $this->FormatDate($item['pay_date_ts']);
		if($item['pay_date_ts']>0)
			$payment['hdate'] = $this->FormatDate($item['pay_date_ts']);
		$payment['amount'] = (($item['is_in'])? "+":"-") . $item['amount'];
		$payment['id_currency'] = $item['id_currency'];
		$payment['verified'] = (int)$item['verified'];
		$payment['description'] = $item['description'];
		$payment['payment_type'] = $item['payment_type'];
		return $payment;
	}
	
	private function PaymentInfo($payment,$description)
	{
		$cpayment = array();
		$cpayment['id'] = $payment['id_payment'];
		$cpayment['amount'] = $payment['amount'];
		$cpayment['id_currency'] = $payment['id_currency'];
		$cpayment['currency'] = $payment['currency'];
		$cpayment['id_type'] = $payment['id_payment_type'];
		$cpayment['id_payer'] = $payment['id_payer'];
		$cpayment['lang_code'] = ($this->id_language=="1")? "it" : "gb";
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$currency_codes = $p->currency_codes;
		$cpayment['currency_code'] = $currency_codes[$payment['id_currency']];
		$account = $p->AccountGet($payment['id_account']);
		if($account['id_type']=="3" || $account['id_type']=="4")
			$cpayment['token'] = $p->PaymentTokenSet($payment['id_payment']);
		if($account['active'])
		{
			$paccount = array('xname'=>"account");
			$paccount['id'] = $account['id_account'];
			$paccount['id_type'] = $account['id_type'];
			$account_types = $this->tr->Translate("account_types");
			$paccount['type'] = $account_types[$account['id_type']];
			$aparams = $p->AccountParamsGet($payment['id_account']);
			if(is_array($aparams))
			{
				$caparams = array();
				$counter = 1;
				foreach($aparams as $key=>$value)
				{
					$caparams['p'.$counter] = array('xname'=>"param",'name'=>$key,'value'=>$value,'label'=>$this->tr->TranslateTry($key));
					$counter ++;
				}
				$paccount['params'] = $caparams;
			}
			$account_info = $this->tr->Translate("account_info");
			$paccount['label'] = $account_info[$account['id_type']];
			$cpayment['account'] = $paccount;
		}
		$cpayment['description'] = $this->tr->TranslateParams("payment_description",array($description));
		$cpayment['reason'] = $description;
		$cpayment['label'] = $this->tr->TranslateParams("donation_intro",array($payment['amount'],$payment['currency']));
		return $cpayment;
	}
	
	private function PeopleContact($id_p)
	{
		$contact = array('label'=>$this->tr->Translate("person_contact"),'description'=>$this->tr->Translate("person_contact_desc"));
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetDetailsById($id_p);
		$portal = array('name'=>$this->ini->Get("title"),'description'=>$this->ini->Get("description"),'contact'=>$user['contact']);
		$this->Urlify($portal,"homepage",array(),true);
		$contact['portal'] = $portal;
		include_once(SERVER_ROOT."/../classes/campaigns.php");
		$campaigns = Campaigns::SubscribedPerson($id_p);
		if(count($campaigns)>0)
		{
			$pcampaigns = array('label'=>$this->tr->Translate("campaigns"));
			foreach($campaigns as $campaign)
			{
				$pcampaign = $this->CampaignItem($campaign,"item");
				$pcampaign['contact'] = $campaign['contact'];
				$pcampaigns['c_'.$campaign['id_topic_campaign']] = $pcampaign;
			}
			$contact['campaigns'] = $pcampaigns;
		}
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$topics = $tt->SubscribedPerson($id_p);
		$ptopics = array('label'=>$this->tr->Translate("websites"));
		$l2 = new Layout($this->preview,$this->protected,$this->live);
		$l2->TopicInit($this->topic->id,true);
		foreach($topics as $topic)
		{
			$l2->TopicInit($topic['id_topic']);;
			$ptopic = $l2->TopicItem($topic,"item");
			$ptopic['contact'] = $topic['contact'];
			$ptopics['t_'.$topic['id_topic']] = $ptopic;
		}
		$l2->__destruct();
		unset($l2);		
		$contact['topics'] = $ptopics;
		return $contact;
	}

	public function PeopleHistory($id_p,$id_topic=0)
	{
		$history = array();
		if($this->IsModuleActive("orgs"))
		{
			include_once(SERVER_ROOT."/../modules/assos.php");
			$as = new Assos();
			$assos = $as->Mantained($id_p);
			$passos = array();
			foreach($assos as $asso)
				$passos['a_'.$asso['id_ass']] = $this->AssoItem($asso,"item");
			$history['assos'] = $passos;
		}
		include_once(SERVER_ROOT."/../classes/campaigns.php");
		$campaigns = Campaigns::ProposedPerson($id_p,$id_topic);
		if(count($campaigns)>0)
		{
			$pcampaigns = array();
			foreach($campaigns as $campaign)
				$pcampaigns['c_'.$campaign['id_topic_campaign']] = $this->CampaignItem($campaign,"item");
			$history['campaigns'] = $pcampaigns;
		}
		$signatures = Campaigns::SignedPerson($id_p,$id_topic);
		$osignatures = Campaigns::SignedOrg($id_p,$id_topic);
		if(count($signatures)>0 || count($osignatures)>0) 
		{
			$psignatures = array('label'=>$this->tr->Translate("signatures"));
			foreach($signatures as $signature)
				$psignatures['c_'.$signature['id_topic_campaign']] = $this->CampaignItem($signature,"item");
			foreach($osignatures as $osignature)
				$psignatures['c_'.$osignature['id_topic_campaign']] = $this->CampaignItem($signature,"item");
			$history['signatures'] = $psignatures;
		}
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		$polls = $pl->VotedPerson($id_p,$id_topic);
		if(count($polls)>0)
		{
			$ppolls = array('label'=>$this->tr->Translate("polls"));
			foreach($polls as $poll)
			{
				$ppoll = $this->PollItem($poll,"item");
				$ppoll['id_question'] = $poll['id_question'];
				$ppoll['id_person'] = $poll['id_p'];
				$ppolls['p_'.$poll['id_poll']] = $ppoll;
			}
			$history['polls'] = $ppolls;
		}
		include_once(SERVER_ROOT."/../classes/comments.php");
		$co = new Comments("article",0);
		$comments = $co->Submitted($id_p,$id_topic);
		if(count($comments)>0)
		{
			$pcomments = array();
			$pcomments['label'] = $this->tr->Translate("comments");
			foreach($comments as $comment)
				$pcomments['c_'.$comment['id_article']] = $this->ArticleCommentsItem($comment,"item");
			$history['comments'] = $pcomments;
		}
		if($this->IsModuleActive("events"))
		{
			include_once(SERVER_ROOT."/../classes/events.php");
			$ee = new Events();
			$events = $ee->Submitted($id_p,$id_topic);
			$pevents = array();
			foreach($events as $event)
				$pevents['e_'.$event['id_event']] = $this->EventItem($event,"item");
			$history['events'] = $pevents;
		}
		if($this->IsModuleActive("books"))
		{
			$trm16 = new Translator($this->tr->id_language,16,false,$this->topic->id_style);
			include_once(SERVER_ROOT."/../modules/books.php");
			$bb = new Books();
			$reviews = $bb->ReviewsPerson($id_p,$id_topic);
			if(count($reviews)>0)
			{
				$previews = array();
				$previews['label'] = $trm16->Translate("reviews");
				foreach($reviews as $review)
					$previews['r_'.$review['id_review']] = $this->BookReviewItemWithBook($review,"item");
				$history['reviews'] = $previews;
			}
		}
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$payments = array();
		$num_payments = $p->PaymentsPerson($payments,$id_p,false,true);
		if($num_payments>0)
		{
			$ppayments = array('label'=>$this->tr->Translate("payments"));
			$currencies = $p->currencies;
			foreach($payments as $payment)
			{
				$ppayment = $this->PaymentItem($payment);
				$ppayment['currency'] = $currencies[$payment['id_currency']];
				$ppayments['p_'.$payment['id_payment']] = $ppayment;
			}
			$history['payments'] = $ppayments;
		}
		
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs();
		$mailjobs = $mj->RecipientHistory($id_p,$id_topic);
		$pmailjobs = array();
		foreach($mailjobs as $mailjob)
		{
			$mj_item = array('xname'=>"item",'type'=>"mailjob",'id'=>$mailjob['id_mail']);
			$mj_item['hdate'] = $this->FormatDate($mailjob['send_date_ts']);
			$mj_item['title'] = $mailjob['subject'];
			$mj_item['status'] = $mailjob['status'];
			$pmailjobs['mj_'.$mailjob['id_mail']] = $mj_item;
		}
		$history['mailjobs'] = $pmailjobs;
		
		return $history;
	}

	private function PeopleLinks($id_topic)
	{
		$people_params = array();
		$login_params = array('label'=>$this->tr->Translate("login"));
		$logout_params = array('label'=>$this->tr->Translate("logout"));
		$register_params = array('label'=>$this->tr->Translate("register"));
		$data_params = array('label'=>$this->tr->Translate("person_data"));
		$cancel_params = array('label'=>$this->tr->Translate("deactivate"));
		$verify_params = array('label'=>$this->tr->Translate("verify_missing"));
		$this->Urlify($login_params,"people",array('subtype'=>"login",'id_topic'=>$id_topic),true);
		$this->Urlify($logout_params,"people",array('subtype'=>"logout",'id_topic'=>$id_topic),true);
		$this->Urlify($register_params,"people",array('subtype'=>"register",'id_topic'=>$id_topic),true);
		$this->Urlify($data_params,"people",array('subtype'=>"data",'id_topic'=>$id_topic),true);
		$this->Urlify($cancel_params,"people",array('subtype'=>"cancel",'id_topic'=>$id_topic),true);
		$this->Urlify($people_params,"people",array('id_topic'=>$id_topic),true);
		$this->Urlify($verify_params,"people",array('subtype'=>"verify",'id_topic'=>$id_topic),true);
		$people_params['login'] = $login_params;
		$people_params['logout'] = $logout_params;
		$people_params['register'] = $register_params;
		$people_params['data'] = $data_params;
		$people_params['cancel'] = $cancel_params;
		$people_params['verify'] = $verify_params;
		if($this->id_type==11)
		{
			$password_params = array('label'=>$this->tr->Translate("password_change"));
			$this->Urlify($password_params,"people",array('subtype'=>"password",'id_topic'=>$id_topic),true);
			$people_params['password'] = $password_params;
		}
		return $people_params;
	}
	
	private function PeopleRegister()
	{
		$p_array = array();
		$p_array['geo'] = $this->Geo();
		$privacy_text = $this->PrivacyText();
		$p_array['privacy_warning'] = array('xvalue'=>$privacy_text,'subscribe'=>$this->tr->Translate("keep_in_touch"));
		return $p_array;
	}
	
	public function PeopleUser($set_topic=false)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserInfo(false);
		$user['xname'] = "user";
		if($set_topic && $this->topic->id>0 && !isset($user['topic_user']))
		{
			$id_p = $user['id'];
			if($id_p>0)
			{
				$topic_user = $this->topic->PersonGet($id_p);
				$user['topic_user'] = $topic_user['id_p']>0? "1":"0";
				$this->topic->IsVisitorAuthorized($id_p);
				$user['topic_auth'] = $this->topic->authorized? "1":"0";
				$pe->UserTopicSet($this->topic->id,$user['topic_user'],$user['topic_auth']);
			}			
		}
		return $user;
	}

	private function PeopleUserHasHistory($id_p,$id_topic=0)
	{
		$counter = 0;
		if($this->IsModuleActive("orgs"))
		{
			include_once(SERVER_ROOT."/../modules/assos.php");
			$as = new Assos();
			$assos = $as->Mantained($id_p);
			$counter += count($assos);
		}
		include_once(SERVER_ROOT."/../classes/campaigns.php");
		$campaigns = Campaigns::ProposedPerson($id_p,$id_topic);
		$counter += count($campaigns);
		$signatures = Campaigns::SignedPerson($id_p,$id_topic);
		$counter += count($signatures);
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		$polls = $pl->VotedPerson($id_p,$id_topic);
		$counter += count($polls);
		include_once(SERVER_ROOT."/../classes/comments.php");
		$co = new Comments("article",0);
		$comments = $co->Submitted($id_p,$id_topic);
		$counter += count($comments);
		if($this->IsModuleActive("events"))
		{
			include_once(SERVER_ROOT."/../classes/events.php");
			$ee = new Events();
			$events = $ee->Submitted($id_p,$id_topic);
			$counter += count($events);
		}
		if($this->IsModuleActive("books"))
		{
			include_once(SERVER_ROOT."/../modules/books.php");
			$bb = new Books();
			$reviews = $bb->ReviewsPerson($id_p,$id_topic);
			$counter += count($reviews);
		}
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$payments = array();
		$counter += $p->PaymentsPerson($payments,$id_p,false,true);
		return $counter;
	}

	private function PeopleWrapper($params)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		if($this->subtype=="logout")
		{
			$user = $pe->UserInfo(false);
			$id_p = $user['id'];
			$pe->SetUserSession(false,array());
			if($id_p>0)
			{
				include_once(SERVER_ROOT."/../classes/validator.php");
				$va = new Validator(true,0,false);
				$va->FeedbackAdd("notice",$this->tr->Translate("logout_done"),array(),false);
			}
		}
		$topic = ($params['id_topic']>0)? $topic = $this->TopicCommon() : array();
		if ($this->subtype=="" )
			$this->subtype = ($params['subtype']!="")? $params['subtype'] : "home";
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		$people = $this->Common($params['id'],$this->id_type);
		switch($this->subtype)
		{
			case "home":
				$user = $pe->UserInfo(false);
				$id_p = $user['id'];
				if($id_p>0 && $user['auth'])
					$people['user']['history'] = $this->PeopleUserHasHistory($id_p);
			case "login":
			case "logout":
			case "password":
				$pagetype = array();
				$reminder_params = array();
				$this->Urlify($reminder_params,"people",array('subtype'=>"reminder",'id_topic'=>$this->topic->id),true);
				$people['site']['people']['reminder'] = $reminder_params;
				$history_params = array();
				$this->Urlify($history_params,"people",array('subtype'=>"history",'id_topic'=>$this->topic->id),true);
				$people['site']['people']['history'] = $history_params;
				$contact_params = array('label'=>$this->tr->Translate("person_contact"),'description'=>$this->tr->Translate("person_contact_desc"));
				$this->Urlify($contact_params,"people",array('subtype'=>"contact",'id_topic'=>$this->topic->id),true);
				$people['site']['people']['contact'] = $contact_params;
			break;
			case "contact":
				$password_params = array('label'=>$this->tr->Translate("password_change"));
				$this->Urlify($password_params,"people",array('subtype'=>"password",'id_topic'=>$this->topic->id),true);
				$people['site']['people']['password'] = $password_params;
				$user = $pe->UserInfo(false);
				$id_p = $user['id'];
				if($id_p>0)
					$people['user']['contact'] = $this->PeopleContact($id_p);
			break;
			case "data":
				$user = $pe->UserInfo(false);
				$id_p = $user['id'];
				if($id_p>0)
				{
					$people_user = $pe->UserGetDetailsById($id_p);
					$people['user'] = array_merge($people['user'],$this->PeopleItemDetails($people_user));
					$people['geo'] = $this->Geo();
					$user_params = $pe->UserParamsGet($id_p,true);
					if(count($user_params)>0)
					{
						$people_params = array();
						foreach($user_params as $key=>$user_param)
						{
							$people_params['param'.$key] = array('xname'=>'param','name'=>$key,'value'=>$user_param);
						}
						$people['user']['params'] = $people_params;
					}
				}
			break;
			case "history":
				$user = $pe->UserInfo(false);
				$id_p = $user['id'];
				if($id_p>0)
					$people['user']['history'] = $this->PeopleHistory($id_p);
			break;
			case "reminder":
				if($params['email']!="")
					$people['user']['email'] = $params['email'];
			break;
			case "register":
				$pagetype = $this->PeopleRegister();
				$people['site']['people']['reg_type'] = $params['reg_type'];
			break;
			case "verify":
				$verified_array = $pe->VerifyToken($params['token'],$params['id_topic']);
				$verified = $verified_array['status'];
				include_once(SERVER_ROOT."/../classes/validator.php");
				$trm28 = new Translator($this->tr->id_language,28,false,$this->topic->id_style);
				$va = new Validator(true,0,false);
				if($verified>0)
					$va->FeedbackAdd("error",$trm28->Translate("registration_check_failed" . $verified),array(),false);
				else
				{
					$va->FeedbackAdd("notice",$trm28->Translate("registration_ok"),array(),false);
					$people['user']['verified'] = People::PeopleEmailVerificationCompleted;
                    if($verified_array['redirect']!='')
                    {
                        header("Location: {$verified_array['redirect']}");
                        exit();
                    }
				}
			break;
			case "cancel":
				$trm28 = new Translator($this->tr->id_language,28,false,$this->topic->id_style);
				$people['site']['people']['cancel']['confirm'] = $trm28->Translate("deactivate_confirm"); 
			break;
		}
		if($this->subtype=="login")
		{
			$people['user']['login_counter'] = $pe->CounterGet();
		}
		$people['user']['verification_required'] = $this->conf->Get("verification_required");
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_topic'=>$params['id_topic']));
		$pagetype = array_merge($people,$pagetype,$topic);
		return $pagetype;
	}
	
	private function PeopleItemDetails($item)
	{
		$pers = array();
		$pers['salutation'] = $item['salutation'];
		$pers['name3'] = $item['name3'];
		$pers['address'] = $item['address'];
		$pers['address_notes'] = $item['address_notes'];
		$pers['postcode'] = $item['postcode'];
		$pers['town'] = $item['town'];
		$pers['id_geo'] = $item['id_geo'];
		$g = new Geo();
		$pers['geo_label'] = $this->tr->Translate($g->Label());
		$pers['geo'] = $g->GeoGet($item['id_geo'],$g->geo_location);
		$pers['start_date'] = $this->FormatDate($item['start_date_ts']);
		$pers['phone'] = $item['phone'];
		return $pers;
	}
	
	private function PersonItem($item,$name="person")
	{
		$pers = array();
		$pers['xname'] = $name;
		$pers['id'] = $item['id_person'];
		$pers['type'] = "person";
		$pers['name'] = $item['name'];
		$pers['email'] = $item['email'];
		$pers['geo'] = $item['geo_name'];
		if ($item['comments']!="")
			$pers['comments'] = array('xvalue'=>$item['comments']);
		$pers['insert_date'] = $this->FormatDate($item['insert_date_ts']);
		return $pers;
	}
	
	private function PollDetails($row)
	{
		$poll = $this->PollItem($row);
		$poll['id_type'] = $row['id_type'];
		$poll['intro'] = array('xvalue'=>$row['intro']);
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		if($row['active'] && $row['users_type']>0)
		{
			$user = $this->PeopleUser();
			if(($row['users_type']=="1" && $user['auth']!="1") || ($row['users_type']=="2" && !$user['id']>0 && $row['id_type']!=4) )
			{
				$poll['login'] = $this->LoginFirst($row['id_topic']);
			}
			else 
			{
				$questions = array();
				$pquestions = array();
				$num = $pl->Questions($questions,$row['id_poll'],$row['sort_questions_by'],1,0,$row['id_type']=="3");
				switch($row['id_type'])
				{
					case "0":
					case "1":
					case "2":
						foreach($questions as $question)
						{
							$pquestion = array('xname'=>"question");
							$pquestion['id'] = $question['id_question'];
							$pquestion['question'] = $question['question'];
							$pquestion['type'] = $row['id_type'];
							$pquestion['description'] = array('xvalue'=>$question['description']);
							if($row['id_type']=="3")
							{
								$this->Urlify($pquestion,"poll",array('id'=>$row['id_poll'],'id_topic'=>$row['id_topic'],'subtype'=>"question",'id_question'=>$question['id_question']),true);
							}
							$pquestions['q_'.$question['id_question']] = $pquestion;
						}
						$poll['questions'] = $pquestions;
						if($row['id_type']=="2")
						{
							$vote_min = (int)$row['vote_min'];
							$vote_max = (int)$row['vote_max'];
							$votes = array();
							for($i=$vote_min;$i<=$vote_max;$i++)
								$votes['v_' . $i] = array('xname'=>"vote",'value'=>$i);
							$poll['votes'] = $votes;
						}
					break;
					case "3":
						$poll['questions'] = $this->Page($questions,$num);
					break;
					case "4":
						$pgroups = array();
						$num_groups = $pl->QuestionsGroups($pgroups,$row['id_poll']);
						$poll['groups'] = $this->Page($pgroups,$num_groups);
					break;
				}
			}
		}
		$poll['submit'] = $this->pub_web . "/" . $this->ini->Get("poll_path") . "/actions.php";
		if(!$row['is_private'] && !$row['id_type']=="4")
		{
			$chart = array('label'=>$this->tr->Translate("results"));
			$this->Urlify($chart,"poll",array('id'=>$row['id_poll'],'id_topic'=>$row['id_topic'],'subtype'=>"results"),true);
			$poll['results'] = $chart;
		}
		if($row['submit_questions']>0)
		{
			$submit = array('label'=>$this->tr->Translate("propose_new"));
			$this->Urlify($submit,"poll",array('id'=>$row['id_poll'],'id_topic'=>$row['id_topic'],'subtype'=>"question"),true);
			$poll['question'] = $submit;
		}
		return $poll;
	}
	
	private function PollItem($item,$name="poll")
	{
		$id_poll = $item['id_poll'];
		$poll = array();
		$poll['xname'] = $name;
		$poll['id'] = $item['id_poll'];
		$poll['type'] = "poll";
		$poll['id_type'] = $item['id_type'];
		$poll['title'] = $item['title'];
		$poll['active'] = $item['active'];
		$poll['start_date'] = $this->FormatDate($item['start_date_ts']);
		if(!$item['active'] && $item['end_date_ts']>0)
			$poll['end_date'] = $this->tr->TranslateParams("closed_on",array($this->FormatDate($item['end_date_ts'])));
		$poll['description'] = array('xvalue'=>$item['description']);
		if($item['hdate_ts']>0)
			$poll['hdate'] = $this->FormatDate($item['hdate_ts']);
		$this->Urlify($poll,"poll",array('id'=>$id_poll,'id_topic'=>$item['id_topic']),true);
		return $poll;
	}
	
	private function PollQuestionItem($item,$name="question",$with_weight=false)
	{
		$question = array();
		$question['xname'] = $name;
		$question['id'] = $item['id_question'];
		$question['type'] = "poll_question";
		$question['question'] = $item['question'];
		$question['description'] = array('xvalue'=>$item['description']);
		if($with_weight)
		{
			$question['weight'] = $item['weight'];
		}
		$this->Urlify($question,"poll",array('id'=>$item['id_poll'],'id_topic'=>$this->topic->id,'subtype'=>"question",'id_question'=>$item['id_question']),true);
		return $question;
	}
	
	private function PollQuestionsGroupItem($item,$name="questions_group")
	{
		$group = array();
		$group['xname'] = $name;
		$group['id'] = $item['id_questions_group'];
		$group['type'] = "poll_questions_group";
		$group['name'] = $item['name'];
		$group['seq'] = $item['seq'];
		$this->Urlify($group,"poll",array('id'=>$item['id_poll'],'id_topic'=>$this->topic->id,'subtype'=>"questions_group",'id_questions_group'=>$item['id_questions_group']),true);
		return $group;
	}
	
	private function PollSteps($id_poll,$seq=0)
	{
		$steps = array('xname'=>"steps");
		$pgroups = array();
		$pl = new Polls();
		$num_groups = $pl->QuestionsGroups($pgroups,$id_poll);
		$num_steps = $num_groups + 2;
		switch($this->subtype)
		{
			case "info":
				$step = 1;
			break;
			case "register":
				$step = $num_steps;
			break;
			case "questions_group":
				$step = $seq + 1;
			break;
		}
		$steps['num'] = $num_steps;
		$steps['current'] = $step;
		$steps['label'] = $this->tr->TranslateParams("poll_steps",array($step,$num_steps));
		return $steps;
	}
	
	private function PollWrapper($params)
	{
		$pagetype = array();
		$topic = ($params['id_topic']>0)? $topic = $this->TopicCommon() : array();
		if ($this->subtype=="" )
			$this->subtype = ($params['subtype']!="")? $params['subtype'] : ($params['id']>0?"info":"list");
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		switch($this->subtype)
		{
			case "info":
				$poll = $pl->PollGet($params['id']);
				if ($params['thanks']>0)
				{
					$pagetype['poll'] = $this->PollItem($poll);
					$pagetype['poll']['thanks'] = array('xvalue'=>$poll['thanks']);
				}
				else 
				{
					$pagetype['poll'] = $this->PollDetails($poll);
					if($poll['id_type']<3 && $poll['users_type']=="3")
					{
						setcookie("poll_" . $poll['id_poll'],"T", time() + 3600 * 24 * $this->conf->Get("visitors_life"),'/');
					}
					$user = $this->PeopleUser();
					if($user['id']>0)
					{
						$user_params = $pl->PersonParamsSessionGet($poll['id_poll']);
						$user['params'] = $pl->PersonParams($user_params);
					}
					$pagetype['poll']['user'] = $user;
				}
				if($poll['id_type']=="4")
				{
					$pagetype['poll']['steps'] = $this->PollSteps($params['id']);
				}
			break;
			case "result":
				$poll = $pl->PollGet($params['id']);
				$pagetype['poll'] = $this->PollItem($poll);
				if($poll['id_type']=="4")
				{
					$groups = $pl->PersonResultsByGroup($params['id'],$params['id_p']);
					$pagetype['poll']['results']['token'] = $params['token'];
					$pagetype['poll']['results']['questions_groups'] = $groups;
				}
			break;
			case "questions_group":
				$row = $pl->PollGet($params['id']);
				$poll = $this->PollItem($row);
				$pgroups = array();
				$num_groups = $pl->QuestionsGroups($pgroups,$params['id']);
				$poll['num_groups'] = $num_groups;
				$group = $pl->QuestionsGroupGet($params['id_questions_group']);
				$qgroup = $this->PollQuestionsGroupItem($group);
				$qgroup['label'] = $this->tr->Translate("poll_choose");
				$gquestions = array();
				$num_gquestions = $pl->Questions($gquestions,$params['id'],$row['sort_questions_by'],1,$params['id_questions_group']);
				if($num_gquestions>0)
				{
					$qgquestions = array();
					foreach($gquestions as $gquestion)
					{
						$question_item = $this->PollQuestionItem($gquestion,"question",true);
						$vote = $pl->QuestionSessionGet($params['id'],$gquestion['id_question']);
						$weights = $pl->QuestionVotesWeights($params['id'],$gquestion['id_question']);
						if(count($weights)>0)
						{
							$qweights = array();
							foreach($weights as $weight)
							{
								$qweight = $weight['weight']>0? $weight['weight'] : $weight['seq'];
								$qweight_array = array('xname'=>"vote",'id'=>$weight['vote'],'seq'=>$weight['seq'],'name'=>$weight['name'],'weight'=>$qweight);
								if($vote>0 && $vote==$weight['vote'])
									$qweight_array['selected'] = "1";
								$qweights['w'.$weight['vote']] = $qweight_array;
							}
							$question_item['votes'] = $qweights;
						}
						$qgquestions['qgq'.$gquestion['id_question']] = $question_item;
					}
					$qgroup['questions'] = $qgquestions;
				}
				$poll['questions_group'] = $qgroup;
				$votenames = array();
				$num_votenames = $pl->VoteNames($votenames,$params['id']);
				if($num_votenames>0)
				{
					$p_votenames = array();
					foreach($votenames as $votename)
					{
						$p_votenames['v_'.$votename['vote']] = array('xname'=>"votename",'seq'=>$votename['seq'],'vote'=>$votename['vote'],'name'=>$votename['name']);
					}
					$poll['votenames'] = $p_votenames;
				}
				$poll['submit'] = $this->pub_web . "/" . $this->ini->Get("poll_path") . "/actions.php";
				$poll['steps'] = $this->PollSteps($params['id'],$group['seq']);
				$pagetype['poll'] = $poll;
			break;
			case "question":
				$row = $pl->PollGet($params['id']);
				$poll = $this->PollItem($row);
				if($row['id_type']=="3" && $params['id_question']>0)
				{
					$user = $this->PeopleUser();
					if(($row['users_type']=="1" && $user['auth']!="1") || ($row['users_type']=="2" && !$user['id']>0) )
					{
						$poll['login'] = $this->LoginFirst($row['id_topic']);
					}
					else 
					{
						$question = $pl->QuestionGet($params['id_question']);
						$pquestion = array('xname'=>"question");
						$pquestion['id'] = $question['id_question'];
						$pquestion['question'] = $question['question'];
						$pquestion['type'] = $row['id_type'];
						$pquestion['description'] = array('xvalue'=>$question['description']);
						$pquestion['description_long'] = array('xvalue'=>($question['is_html']?$question['description_long']:$this->Htmlise($question['description_long']))  );
						if($question['id_p']>0)
						{
							include_once(SERVER_ROOT."/../classes/people.php");
							$pe = new People();
							$person = $pe->UserGetById($question['id_p']);
							$author = array('xname'=>"author",'name'=>"{$person['name1']} {$person['name2']}",'label'=>$this->tr->Translate("author"));
							$pquestion['author'] = $author;
						}
						if($row['users_type']=="3")
						{
							setcookie("poll_" . $row['id_poll'],"T", time() + 3600 * 24 * $this->conf->Get("visitors_life"),'/');
						}
						$poll['question'] = $pquestion;
						$vote_min = (int)$row['vote_min'];
						$vote_max = (int)$row['vote_max'];
						$votes = array();
						for($i=$vote_min;$i<=$vote_max;$i++)
							$votes['v_' . $i] = array('xname'=>"vote",'value'=>$i);
						$poll['votes'] = $votes;
						include_once(SERVER_ROOT."/../classes/comments.php");
						$this->comments = new Comments("question",$params['id_question']);
						$comments = $this->Comments($row['comments']>0,1);
						$poll['comments'] = $comments;
					}
				}
				else 
				{
					$user = $this->PeopleUser();
					if(($row['submit_questions']=="1" && $user['auth']!="1") || ($row['submit_questions']=="2" && !$user['id']>0) )
					{
						$poll['login'] = $this->LoginFirst($row['id_topic']);
					}
				}
				$poll['submit'] = $this->pub_web . "/" . $this->ini->Get("poll_path") . "/actions.php";
				$pagetype['poll'] = $poll;
			break;
			case "register":
				$poll = $pl->PollGet($params['id']);
				$poll_item = $this->PollItem($poll);
				$poll_item['submit'] = $this->pub_web . "/" . $this->ini->Get("poll_path") . "/actions.php";
				$user = $this->PeopleUser();
				if($user['id']>0)
				{
					$user['params'] = $pl->PersonParams($user['id']);
				}
				$poll_item['user'] = $user;
				if($poll['id_type']=="4")
				{
					$poll_item['steps'] = $this->PollSteps($params['id']);
				}
				$pagetype['poll'] = $poll_item;
			break;
			case "results":
				$row = $pl->PollGet($params['id']);
				$pagetype['poll'] = $this->PollItem($row);
				if(!$row['is_private'] && $row['id_type']!="4")
				{
					$results = array();
					$pl->QuestionsChart( $results, $params['id'], $row['id_type'],(int)$row['vote_threshold'],false );
					$presults = array('label'=>$this->tr->Translate("results"));
					if($row['vote_threshold']>0)
					$presults['threshold'] = $this->tr->TranslateParams("results_threshold",array($row['vote_threshold']));
					$presults['label_votes'] = $this->tr->Translate("votes");
					if($row['id_type']>1)
						$presults['label_avg'] = $this->tr->Translate("average");
					$pos = 1;
					foreach($results as $result)
					{
						$question = array('xname'=>"question");
						$question['id'] = $result['id_question'];
						$question['pos'] = $pos;
						$question['title'] = $result['question'];
						$question['votes'] = number_format($result['votes'],0);
						$question['description'] = array('xvalue'=>$result['description']);
						if($row['id_type']>1)
							$question['avg'] = number_format($result['avg'],2);
						if($row['id_type']=="3")
						{
							$this->Urlify($question,"poll",array('id'=>$row['id_poll'],'id_topic'=>$row['id_topic'],'subtype'=>"question",'id_question'=>$result['id_question']),true);
						}
						$presults['r_'.$result['id_question']] = $question;
						$pos ++;
					}
					$pagetype['poll']['results'] = $presults;
				}
			break;
			case "list":
				$polls = array();
				$this->Urlify($polls,"poll",array('id_topic'=>$params['id_topic']),true);
				$rows = array();
				$num = $pl->ActivePolls($rows,$this->topic->id);
				$polls['items'] = $this->Page($rows,$num);
				$pagetype['polls'] = $polls;
			break;
		}
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_poll'=>$params['id'],'id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function PostBack()
	{
		$postback = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$vars = $session->Get("postvars");
		$counter = 1;
		if(is_array($vars) && count($vars)>0)
		{
			foreach($vars as $key=>$value)
			{
				$pvalue = is_array($value)? implode(",",$value) : $value;
				$postback['p_' . $counter] = array('xname'=>'var','name'=>$key,'value'=>$pvalue);
				$counter ++;
			}
		}
		$session->Delete("postvars");
		return $postback;
	}
	
	private function PostErrors()
	{
		$posterrors = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$messages = $session->Get("posterrors");
		$counter = 1;
		if(is_array($messages) && count($messages)>0)
		{
			foreach($messages as $message)
			{
				$posterrors['p_' . $counter] = array('xname'=>"postvar",'name'=>$message);
				$counter ++;
			}
		}
		$session->Delete("posterrors");
		return $posterrors;
	}
	
	private function PriceFormat($price)
	{
		return "$price $this->currency";
	}
	
	private function PrivacyText()
	{
		$get_generic = false;
		$privacy_text = "";
		if($this->topic->id > 0)
		{
			$tprivacy = $this->th->ContentGetByType($this->th->types['privacy'],$this->topic->id);
			if($tprivacy['id_content']>0 && strlen(strip_tags($tprivacy['content']))>0)
				$privacy_text = $tprivacy['is_html']? $tprivacy['content'] : $this->Htmlise($tprivacy['content'],false);
			else 
				$get_generic = true;
		}
		else 
			$get_generic = true;
		if($get_generic)
		{
			$privacy = $this->th->ContentGetByType($this->th->types['privacy'],0);
			if($privacy['id_content']>0)
				$privacy_text = $privacy['is_html']? $privacy['content'] : $this->Htmlise($privacy['content'],false);
		}
		return $privacy_text;
	}
	
	private function QuoteItem($item,$name="quote",$nl2br=false)
	{
		$quote = array();
		$quote['xname'] = $name;
		$quote['id'] = $item['id_quote'];
		$quote['type'] = "quote";
		$quote['id_topic'] = $item['id_topic'];
		$text = $nl2br? nl2br($item['quote']) : $item['quote'];
		$quote['text'] = array('xvalue'=>$text);
		$author = array();
		$author['xname'] = "author";
		$author['name'] = $item['author'];
		if ($item['notes']!="")
			$author['notes'] = array('xvalue'=>$item['notes']);
		$quote['author'] = $author;
		return $quote;
	}

	private function Quotes()
	{
		$q = array();
		$q['xname'] = "quotes";
		include_once(SERVER_ROOT."/../classes/quotes.php");
		$qu = new Quotes();
		$rows = array();
		$q['num'] = $qu->QuotesApproved( $rows, $this->topic->id, 1 );
		$this->Urlify($q,"quotes",array('id_topic'=>$this->topic->id,'subtype'=>"home"),true);
		$insert = array();
		$insert_params = array('id_topic'=>$this->topic->id,'subtype'=>"insert");
		$this->Urlify($insert,"quotes",$insert_params,true);
		$q['insert'] = $insert;
		$list = array();
		$list_params = array('id_topic'=>$this->topic->id,'subtype'=>"list");
		$this->Urlify($list,"quotes",$list_params,true);
		$q['list'] = $list;
		$random = $qu->RandomQuote($this->topic->id);
		$q['random'] = $this->QuoteItem($random);
		return $q;
	}
	
	private function QuotesList()
	{
		include_once(SERVER_ROOT."/../classes/quotes.php");
		$qu = new Quotes;
		$rows = array();
		$num = $qu->QuotesAll($rows,$this->topic->id,true);
		$q['quotes'] = $this->Page($rows,$num);
		return $q;
	}
	
	private function QuotesWrapper($params)
	{
		if ($this->subtype=="" )
		{
			if ($params['subtype']!="")
				$this->subtype = $params['subtype'];
			else
				$this->subtype = "home";
		}
		$this->current_page_global = true;
		$this->current_page_params['subtype'] = $this->subtype;
		$topic = ($params['id_topic']>0)? $topic = $this->TopicCommon() : array();
		switch($this->subtype)
		{
			case "home":
			case "insert":
				$pagetype['quotes'] = $this->Quotes();
			break;
			case "list":
				$pagetype['quotes'] = array_merge($this->Quotes(),$this->QuotesList());
			break;
		}
		$pagetype['features'] = $this->Features($this->id_type,0,array('id_topic'=>$params['id_topic']));
		$pagetype = array_merge($this->Common($params['id'],$this->id_type),$pagetype,$topic);
		return $pagetype;
	}
	
	private function RandomItem($type,$params)
	{
		$random = array();
		$random['type'] = $type;
		$random['details'] = (int)$params['details'];
		switch ($type)
		{
			case "article":
				$articles = array();
				if($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$articles = $k->UseArticles($params['id_keyword'],$params['id_topic'],0,"6");
				}
				$this->item_type = "article";
				$random['item'] = $this->ArticleItem($articles[0]);
			break;
			case "book":
			case "book_big":
				include_once(SERVER_ROOT."/../modules/books.php");
				if($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$books = $k->UseBooks($params['id_keyword'],0,0,true);
				}
				else 
				{
					$bb = new Books();
					$books = $bb->BooksAll(array('id_publisher'=>$params['id_publisher'],'id_category'=>$params['id_category'],'sort_by'=>"random",'approved'=>"1"),1);
				}
				$this->item_type = $type;
				$b = new Book($books[0]['id_book']);
				$random['item'] = $type=="book_big"? $this->BookItemBig($b->BookGet()) : $this->BookItem($b->BookGet());
			break;
			case "feature":
				include_once(SERVER_ROOT."/../classes/varia.php");
				$v = new Varia();
				$feature = $params['feature'];
				$feature_params = $v->Deserialize($feature['params']);
				$irows = $this->pt->ft->PageFunction($feature['id_function'],$feature_params,array());
				$items = array();
				$seq = 1;
				foreach($irows as $irow)
				{
					$this->TopicInit($irow['id_topic']);
					$irow['item_type'] = $this->pt->ft->item_type;
					$items['i_' . $seq] = $this->Item($irow);
					$seq++;
				}
				$functions = $this->tr->Translate("page_functions");
				$f_array = array('xname'=>"feature",'id'=>$params['id_feature'],'name'=>$feature['name'],'id_user'=>$feature['id_user'],'id_function'=>$feature['id_function'],'function'=>$functions[$feature['id_function']],'params'=>$feature_params,'info'=>($this->pt->ft->info),'type'=>($this->pt->ft->item_type),'items'=>$items,'description'=>$feature['description']);
				$random['item'] = $f_array;
				$this->item_type = "feature";
				if($params['common']) {
					if(isset($this->cache_common)) {
						$common = $this->cache_common;
					} else {
						$common = $this->Common(0,$this->id_type);
					}
					$random = array_merge($common,$random);
				}
				break;
			case "quote":
				include_once(SERVER_ROOT."/../classes/quotes.php");
				$q = new quotes;
				$random['item'] = $this->QuoteItem($q->RandomQuote($params['id_topic']),"quote",$params['nl2br']==1);
			break;
			case "user":
				$id_topic = $params['id_topic'];
				$this->TopicInit($id_topic);
				$random['item'] = $this->PeopleUser($id_topic > 0);
				$random['site']['people'] = $this->PeopleLinks($id_topic);
				$random['labels'] = $this->Labels();
				$random['c_features'] = $this->Features(0,0,array());
			break;
			case "image":
				$subtype = "image";
				include_once(SERVER_ROOT."/../classes/gallery.php");
				if ($params['id_image']>0) // Specific image
				{
					include_once(SERVER_ROOT."/../classes/galleries.php");
					$gg = new Galleries();
					$image = $gg->GalleryImage($params['id_image']);
					$subtype = "image";
					$id_image = $params['id_image'];
					$id_gallery = $image['id_gallery'];
					$g = new Gallery($image['id_gallery']);
				}
				elseif($params['id_gallery']>0)
				{
					$id_gallery = $params['id_gallery'];
					$g = new Gallery($id_gallery);
					if ($params['random']=="1")
						$g->sort_order = 4;
					$images = $g->Images();
					$image = $images[0];
					$id_image = $image['id_image'];
				}
				else
				{
					$id_group = (int)$params['id_group'];
					include_once(SERVER_ROOT."/../classes/galleries.php");
					$gg = new Galleries();
					$galleries = $gg->gh->GroupItemsVisible($id_group);
					$rand_index = rand(0,count($galleries)-1);
					$gallery = $galleries[$rand_index];
					$id_gallery = $gallery['id_gallery'];
					$g = new Gallery($id_gallery);
					if ($params['random']=="1")
						$g->sort_order = 4;
					$images = $g->Images(1);
					$image = $images[0];
					$id_image = $image['id_image'];
				}
				if(!$id_image>0)
					$this->Stop();
				//$width = ($params['w']=="1")? $g->image_size : $g->thumb_size;
				$is_thumb = ($params['w']=="1")? false:true;
				$image['thumb_size'] = $g->thumb_size;
				$image['image_size'] = $g->image_size;
				$random['item'] = $this->GalleryImageItem($image,$is_thumb);
				$random['item']['jump'] = $params['jump'];
				$gallery = array();
				if($this->topic->id>0)
				{
					$id_subtopic = $this->topic->HasSubtopicTypeItem($this->topic->subtopic_types['gallery'],$id_gallery);
					if($id_subtopic>0)
						$this->Urlify($gallery,"gallery_item",array('id'=>$id_image,'id_gallery'=>$id_gallery));
				}
				else 
					$this->Urlify($gallery,"galleries",array('id_image'=>$id_image,'id_gallery'=>$id_gallery,'subtype'=>$subtype),true);
				$random['item']['gallery'] = $gallery;
			break;
			case "today":
				$today = time();
				$random['today'] = $this->DayItem($today);
				$random['history'] = $this->EventsRecurring($today);
			break;
			case "widget_feature":
				if(isset($this->cache_common))
				{
					$common = $this->cache_common;
				}
				else 
					$common = $this->Common(0,$this->id_type);
				$v = new Varia();
				$row = $this->pt->ft->FeatureGet($params['id_feature']);
				$custom_params = isset($row['params'])? $v->Deserialize($row['params']) : array();
				$irows = $this->pt->ft->PageFunction($row['id_function'],$custom_params,array());
				$items = array();
				$seq = 1;
				$functions = $this->tr->Translate("page_functions");
				$l2 = new Layout(false,false,false);
				$l2->global = $this->global;
				$l2->TopicInit($this->topic->id,true);
				foreach($irows as $irow)
				{
					if(array_key_exists("id_topic",$irow))
						$l2->TopicInit($irow['id_topic']);
					$irow['item_type'] = $this->pt->ft->item_type;
					$items['i_' . $seq] = $l2->Item($irow);
					$seq++;
				}
				$random['feature'] = array('xname'=>"feature",'id'=>$row['id_feature'],'feature_name'=>$row['name'],'id_user'=>$row['id_user'],'id_function'=>$row['id_function'],'function'=>$functions[$row['id_function']],'params_in'=>array(),'params'=>$custom_params,'info'=>($this->pt->ft->info),'type'=>($this->pt->ft->item_type),'items'=>$items,'description'=>$row['description']);;
				$random = array_merge($common,$random); 
			break;
		}
		return $random;
	}
	
	private function RecordsPerPage($num)
	{
		if($num>0)
		{
			$GLOBALS['records_per_page'] = $num;
			$this->records_per_page = $num;		
		}
	}
	
	private function RecurringEventItem($item,$name="r_event")
	{
		$event = array();
		$event['xname'] = $name;
		$event['type'] = "r_event";
		$event['id'] = $item['id_event'];
		$event['year'] = $item['r_year'];
		$event['url'] = $item['url'];
		$event['label'] = $this->FormatDate("{$item['r_year']}-{$item['r_month']}-{$item['r_day']}",false);
		$event['description'] = $item['description'];
		return $event;
	}
	
	private function Rss($id_item,$type)
	{
		$feed_type = $this->ini->Get("feed_type");
		$this->feed_dateformat = ($feed_type=="3")? 'Y-m-d\TH:i:s\Z' : "r";
		switch($type)
		{
			case "topic_group":
				$rss2 = $this->GroupRss($id_item,$feed_type);
			break;
			case "topic":
				$this->TopicInit($id_item);
				$rss2 = $this->TopicRss($id_item,$feed_type);
			break;
			case "subtopic":
				$rss2 = $this->SubtopicRss($id_item,$feed_type);
			break;
			case "events":
				$rss2 = $this->EventsRss();
			break;
			case "keyword":
				$rss2 = $this->KeywordRss($id_item,$feed_type);
			break;
			default:
				$rss2 = $this->GroupRss(0,$feed_type);
		}
		$rss = $this->Common($id_item,$this->id_type);
		$rss['rss'] = $rss2;
		$rss['rss']['lastbuild'] = date($this->feed_dateformat);
		$rss['rss']['ttl'] = $this->conf->Get("rss_ttl");
		$rss['rss']['lang'] = $this->tr->languages[$this->id_language];
		$rss['rss']['type'] = $feed_type;
		$rss_url = array();
		$this->Urlify($rss_url,"rss",array('subtype'=>$type,'id'=>$id_item),true);
		$rss['rss']['rss'] = $rss_url['url'];
		return $rss;
	}

	private function RssItem($item,$feed_type)
	{
		$rss = array();
		$this->TopicInit($item['id_topic']);
		$rss['xname'] = "item";
		$rss['id_topic'] = $item['id_topic'];
		if($item['id_topic']>0)
		{
			$topic = array('name'=>$this->topic->name);
			$this->Urlify($topic,"topic_home",array('id_topic'=>$item['id_topic']));
			$rss['topic'] = $topic;
		}
		if ($item['id_article']>0)
		{
			$rss['id'] = $item['id_article'];
			$rss['halftitle'] = array('xvalue'=>$item['halftitle']);
			$rss['headline'] = array('xvalue'=>$item['headline']);
			$rss['subhead'] = array('xvalue'=>$item['subhead']);
			$rss['link'] = $this->irl->PublicUrlTopic("article",array('id'=>$item['id_article'],'jump'=>$this->th->String2Url($item['jump'])),$this->topic);
			$rss['pubdate_ts'] = $item['published_ts'];
			$rss['written_ts'] = $item['written_ts'];
			$rss['pubdate'] =  date($this->feed_dateformat,$item['published_ts']);
			$rss['written'] =  date($this->feed_dateformat,$item['written_ts']);
			if($item['show_author'])
				$rss['author'] = $this->ArticleAuthor($item);
			if ($item['show_date'] && $item['written_ts']>0)
			{
				$rss['display_date'] = $this->FormatDate($item['written_ts']);
			}
			if($feed_type=="4" || $feed_type=="5")
			{
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($item['id_article']);
				$image = $a->Image();
				if($image['id_image']>0)
				{
					$image['associated'] = true;
					$rss['image'] = $this->ImageItem($image);
				}
				if($feed_type=="5")
				{
					$article_content = $this->ArticleContent($item['id_article'],false,false,false);
					if(is_array($article_content['article']['images']) && count($article_content['article']['images'])>0)
						$rss['images'] = $article_content['article']['images']; 
					$rss['content'] = $article_content['article']['content'];
				}
			}
		}
		if ($item['id_event']>0)
		{
			$rss['id'] = $item['id_event'];
			$rss['halftitle'] = array('xvalue'=>$item['type']);
			$rss['headline'] = array('xvalue'=>$item['title']);
			$rss['subhead'] = array('xvalue'=>$item['place'] . " - " . $item['geo_name']);
			$rss['link'] = $this->irl->PublicUrlGlobal("events",array('id'=>$item['id_event'],'subtype'=>"event",'id_topic'=>$item['id_topic']));
			$rss['pubdate_ts'] = $item['start_date_ts'];
			$rss['pubdate'] =  date($this->feed_dateformat,$item['start_date_ts']);
		}
		return $rss;
	}
	
	private function Search($unescaped_query,$unescaped_keyword,$id_template,$id_group,$id_topic,$id_subtopic)
	{
        $s_array =  $this->Common(0,$this->id_type);
		if ($id_topic>0)
			$s_array = array_merge($s_array,$this->TopicCommon());
		$unescaped_query = $this->th->UtfClean($unescaped_query);
		$unescaped_keyword = $this->th->UtfClean($unescaped_keyword);
		$strlen = $this->th->StringLength($unescaped_query);
		$search_words = $this->th->SearchWords($unescaped_query,$this->id_language);
		$min_str_length = $this->conf->Get("min_str_length");
		$this->current_page_global = true;
		$this->current_page_params['q'] = $unescaped_query;
		$this->current_page_params['k'] = $unescaped_keyword;
		$this->current_page_params['id_template'] = $id_template;
		$tot = 0;
		$search  = array('q'=>$unescaped_query,'id_topic'=>(int)$id_topic,'id_group'=>(int)$id_group,'id_template'=>$id_template);
		if ($strlen>=$min_str_length)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();

			$time_weight = $this->conf->Get("time_weight");
			include_once(SERVER_ROOT."/../classes/articles.php");
			$articles = array();
			$num_articles = Articles::SearchPub($articles,$search_words,$id_template,$id_topic,$id_group,$time_weight,$id_subtopic);
			$articles_paged = $this->Page($articles,$num_articles);
			$tot += $num_articles;
			if($num_articles==0 && count($search_words)<=2) {
				$articles_paged['suggestions'] = $this->SearchSuggestions($search_words,$num_articles,5);
			}
			$search['articles'] = $articles_paged;
			if($this->ini->Get("search_events") && $s->IsResourceIndexed(8))
			{
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$ee->id_topic = $id_topic;
				$ee->id_group = (int)$s_array['topic']['id_group'];
				$events = array();
				$num_events = $ee->Search($events,$search_words,array(),$time_weight);
				$tot += $num_events;
				$events_paged = $this->Page($events,$num_events);
				$this->Urlify($events_paged,"events",array('id_topic'=>$id_topic,'id_group'=>$id_group,'subtype'=>"search",'q'=>$unescaped_query),TRUE);
				$search['events'] = $events_paged;
			}
			if($this->ini->GetModule("books","search_books",0) && $s->IsResourceIndexed(13))
			{
				include_once(SERVER_ROOT."/../modules/books.php");
				$bb = new Books();
				$books = array();
				$num_books = $bb->SearchPub($books,$search_words);
				$books_paged = $this->Page($books,$num_books);
				$this->Urlify($books_paged,"books",array('id_topic'=>$id_topic,'subtype'=>"search",'q'=>$unescaped_query),TRUE);
				$search['books'] = $books_paged;
				$tot += $num_books;
			}
			if($s->resources_to_index[1]) //topic
			{
				include_once(SERVER_ROOT."/../classes/topics.php");
				$tt = new Topics();
				$topics = array();
				$num_topics = $tt->SearchPub($topics,$search_words);
				$search['topics'] = $this->Page($topics,$num_topics,$num_topics);
				$tot += $num_topics;
			}
		}
		$search['tot'] = $tot;
		$this->Urlify($search,"search",array('q'=>$unescaped_query,'id_topic'=>$id_topic,'id_group'=>$id_group),true);
		$s_array['search'] = $search;   
		if ($unescaped_keyword!="")
		{
			include_once(SERVER_ROOT."/../classes/db.php");
			$db =& Db::globaldb();
			$keyword = $db->SqlQuote($unescaped_keyword);
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$id_k = $k->KeywordGetId($keyword);
			if ($id_k>0)
			{
				$row = $k->KeywordGet($id_k);
				$articles = array();
				$num_articles = $k->UseArticlesPaged($articles,$id_k,$id_template,$id_topic,$id_subtopic);
				$articles_paged = $this->Page($articles,$num_articles);
				$keyinfo = array('xname'=>"keyword",'name'=>$unescaped_keyword,'id'=>$id_k,'description'=>$row['description'],'id_type'=>$row['id_type']);
				$search  = array('k'=>$unescaped_keyword,'keyword'=>$keyinfo,'articles'=>$articles_paged,'tot'=>$num_articles);
				$this->Urlify($search,"search",array('k'=>$unescaped_keyword,'id_topic'=>$id_topic),true);
				$s_array['search'] = $search;
			}
		}
		$s_array['features'] = $this->Features($this->id_type,0,array('id_topic'=>$id_topic));   
		return $s_array;
	}

	private function SearchSuggestions($search_words,$num_results,$id_res)
	{
		$metaphone_threshold = $this->conf->Get("metaphone_threshold");
		$suggestions = array();
		if($num_results<$metaphone_threshold)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$suggestions = array();
			$counter = 1;
			foreach($search_words as $search_word)
			{
				$word_suggestions = $s->Suggestions($search_word,$id_res);
				if(count($word_suggestions)>0)
				{
					$suggi = array('xname'=>"search_word",'word'=>$search_word);
					foreach($word_suggestions as $suggestion)
					{
						$sugg_array = array('xname'=>"word",'id'=>$suggestion['id_word'],'word'=>$suggestion['word'],'results'=>$suggestion['counter']);
						$this->Urlify($sugg_array,"search",array('q'=>$suggestion['word'],'id_topic'=>$suggestion['id_topic']),true);
						$suggi['s'.$suggestion['id_word']] = $sugg_array;
					}
					$suggestions['w'.$counter] = $suggi;
				}
				$counter++;
			}
		}
		return $suggestions;
	}
	
	public function Stop($message="")
	{
		if($message!="")
			echo $message;
		exit;
	}

	public function StructuredData(&$pagetype) {
		$cdn = $this->conf->Get("cdn");
		$ld = array();
		$ld['@context'] = 'https://schema.org';
		$org = array();
		$org['@type'] = 'Organization';
		$org['address'] = array('@type'=>'PostalAddress','addressLocality'=>'Taranto','postalCode'=>'74100','streetAddress'=>'CP 2009','addressRegion'=>'TA','addressCountry'=>'IT');
		$org['name'] = 'PeaceLink';
		$org['url'] = 'https://www.peacelink.it';
		$org['logo'] = array('@type'=>'ImageObject','url'=>"$cdn/graphics/peacelink_amp.png?format=png",'width'=>'200','height'=>'60');
		$org['email'] = 'mailto:info@peacelink.it';
		$org['image'] = "$cdn/css/i/peacelink.svg";
		$org['description'] = "Telematica per la Pace, associazione di volontariato dell'informazione che dal 1992 offre una alternativa ai grandi gruppi editoriali e televisivi.";
		$org['sameAs'] = array(
			'https://www.facebook.com/retepeacelink',
			'https://twitter.com/peacelink',
			'https://www.youtube.com/c/peacelinkvideo',
			'https://sociale.network/@peacelink'
		);
		if(!$this->global && $this->id_type==$this->pt->types['article']) { //article
			$article = $pagetype['article'];
			$ld['@type'] = 'NewsArticle';
			$ld['mainEntityOfPage'] = array('@type'=>'WebPage','@id'=>$pagetype['page']['url']);
			$ld['headline'] = $article['headline']['xvalue'];
			if(isset($article['images']) && is_array($article['images']) && count($article['images'])>0) {
				$associated_image = array();
				foreach($article['images'] as $image) {
					if(isset($image['xname']) && $image['xname']=='image') {
						if((isset($image['associated']) && $image['associated']=='1') || count($associated_image)==0) {
							$ld['image'] = $image['og_image']['url'];
						}
					}
				}
			} else {
			    $ld['image'] = "$cdn/graphics/peacelink.jpg";
			}
			$ld['datePublished'] = $article['date_iso8601'];
			$ld['dateModified'] = $article['date_iso8601'];
			$ld['author'] = array('@type'=>'Organization','name'=>'PeaceLink');
			if($article['show_author']=='1') {
			    if(isset($article['author']['name']) && $article['author']['name']!='') {
			        $ld['author'] = array('@type'=>'Person','name'=>$article['author']['name']);
			    } else if(isset($article['source']['description']) && $article['source']['description']['xvalue']!='') {
			        $ld['author'] = array('@type'=>'Organization','name'=>$article['source']['description']['xvalue']);
			    }
			}
			$ld['publisher'] = $org;
			if($article['subhead']['xvalue']!='') {
				$ld['description'] = $article['subhead']['xvalue'];
			}
		} else if(!$this->global && $this->id_type==$this->pt->types['events'] && $this->subtype=='event') { // event
			$event = $pagetype['event'];
			$ld['@type'] = 'Event';
			$ld['name'] = $event['title'];
			$ld['startDate'] = $event['start_iso8601'];
			$ld['endDate'] = $event['end_iso8601'];
			if(isset($event['image'])) {
				$ld['image'] = $event['image']['image_zoom']['url'];
			}
			if($event['description_text']!='') {
				$ld['description'] = $event['description_text'];
			} else {
			    $ld['description'] = $event['title'];
			}
			if($event['contact_name']!='') {
			    $ld['organizer'] = array();
			    $ld['organizer']['name'] = $event['contact_name'];
			}
			$location = array();
			if($event['id_geo'] == '105') {
			    $location['@type'] = 'VirtualLocation';
			    if($event['link']!='') {
			        $location['url'] = $event['link'];
			    }
			    $ld['eventAttendanceMode'] = 'OnlineEventAttendanceMode';
			} else {
			    $location['@type'] = 'Place';
			    $location['name'] = $event['place'];
			    $location['address'] = array('@type'=>'PostalAddress','addressLocality'=>$event['place']);
			    if(isset($event['geo_name'])) {
			        $location['address']['addressRegion'] = $event['geo_name'];
			        $location['address']['addressCountry'] = 'IT';
			    }
			    if($event['place_details']['xvalue']!='') {
			        $location['streetAddress'] = $event['place_details']['xvalue'];
			    }
			    $ld['eventAttendanceMode'] = 'OfflineEventAttendanceMode';
			}
			$ld['location'] = $location;
		} else {
			$ld = array_merge($ld,$org);
		}
		$pagetype['structured_data'] = json_encode($ld);
	}

	public function Subtopic($id_subtopic,$params=array())
	{
		$subtopic_pages = array();
		if(isset($this->cache_common))
		{
			$common = $this->cache_common;
			$common['publish']['id'] = $id_subtopic;
			$common['publish']['subtype'] = $this->subtype;
		}
		else 
			$common = $this->Common($id_subtopic,$this->id_type);
		$topic_common = isset($this->cache_topic)? $this->cache_topic : $this->TopicCommon();
		$sub_array = array_merge($common,$topic_common);
		$subtopic = (isset($this->item) && $this->item_type=="subtopic")? $this->item : $this->topic->SubtopicGet($id_subtopic);
		$this->current_page_params['id'] = $id_subtopic;
		$this->current_page_params['id_type'] = $subtopic['id_type'];
		$this->current_page_params['id_item'] = $subtopic['id_item'];
		$this->current_page_params['id_subitem'] = $subtopic['id_subitem'];
		if($params['id_doc']>0)
			$this->current_page_params['id_doc'] = $params['id_doc'];
		$this->current_page_global = !$this->topic->SubtopicPublishable($subtopic['id_type']);
		$sub = $this->SubtopicItem($subtopic,0,true);
		$sub['breadcrumb'] = $this->SubtopicPath($id_subtopic);
		$sub['depth'] = count($sub['breadcrumb']);
		$sub['visible'] = $subtopic['visible'];
		$sub['subtopics'] = $this->SubtopicSubtopics($id_subtopic);
		$sub['header'] = $this->ExplodeMarkers($subtopic['header']);
		$sub_pages = $this->SubtopicContentPaged($id_subtopic,$subtopic['id_type'],$subtopic['id_item'],$subtopic['id_subitem'],$subtopic['sort_by'],$subtopic['records_per_page']);
		$sub['footer'] = $this->ExplodeMarkers($subtopic['footer']);
		$subtopic_keywords = $this->Keywords($id_subtopic,"subtopic");
		$sub['keywords'] = array_merge($this->Keywords($this->topic->id,"topic"),$subtopic_keywords);
		$feature_params = array('id_subtopic'=>$id_subtopic,'id_topic'=>$subtopic['id_topic'],'id_subtopic_type'=>$subtopic['id_type'],'keywords'=>$subtopic_keywords);
		$sub_array['features'] = $this->Features($this->id_type,0,$feature_params);
		$sub_page = $this->CurrentPage("subtopic",$this->current_page_global);
		if (count($sub_pages)>0)
		{
			foreach($sub_pages as $sub_page)
			{
				$sub['content'] = $sub_page;
				$sub_array['subtopic'] = $sub;
				$sub_array['page'] = $sub_page;
				$subtopic_pages[] = $sub_array;
			}
			unset($sub_pages);
		}
		else
		{
			$sub_array['subtopic'] = $sub;
			$sub_array['page'] = $sub_page;
			$subtopic_pages[] = $sub_array;
		}
		return $subtopic_pages;
	}

	private function SubtopicContentPaged($id_subtopic,$id_type,$id_item,$id_subitem,$sort_by,$records_per_page)
	{
		$subtopic_pages = array();
		$sub_content = $this->SubtopicContent($id_subtopic,$id_type,$id_item,$id_subitem,$sort_by,$records_per_page);
		if ($id_type==1 || $id_type==3 || ($id_type==9 && ($id_subitem=="1" || $id_subitem=="3" || $id_subitem=="0")) || $id_type==10 || $id_type==11 || $id_type==15)
		{
			$items_groups = $this->Pager($sub_content,$records_per_page);
			foreach($items_groups as $items)
			{
				$subtopic_pages[] = array('items'=>$items);
			}
		}
		else
		{
			$subtopic_pages[] = $sub_content;
		}
		return $subtopic_pages;
	}

	private function SubtopicContent($id_subtopic,$id_type,$id_item,$id_subitem,$sort_by,$records_per_page=0)
	{
		$content = array();
		switch($id_type)
		{
			case 1:  //folder
				$content = $this->topic->SubtopicArticles($id_subtopic,$sort_by);
			break;
			case 2:  //article
				$articles = $this->topic->SubtopicArticles($id_subtopic,$sort_by);
				if (count($articles)>0)
					$content = $this->ArticleContent($articles[0]['id_article'],false);
			break;
			case 3:  //latest
				$content = $this->topic->Latest($records_per_page,0);
			break;
			case 5:  // form
				$content = $this->Form($id_subtopic,$id_item);
			break;
			case 9:  // gallery
			switch($id_subitem)
				{
					case "0":
					case "1":
						$content = $this->topic->SubtopicGalleryImages($id_item,$id_subtopic);
					break;
					case "2":
						$content = $this->SubtopicGallerySlideshow($id_item);
					break;
					case "3":
						$content = $this->topic->SubtopicGalleryImages($id_item,$id_subtopic,$this->highslide);
					break;
				}
			break;
			case 11:  // links
				$content = $this->topic->Links($id_subtopic);
			break;
			case 13: // insert (link,article)
				$privacy_text = $this->PrivacyText();
				$content['privacy_warning'] = array('xvalue'=>$privacy_text);
			break;
			case 15: // keyword filter
				$subtopic = $this->topic->SubtopicGet($id_subtopic);
				$v = new Varia();
				$params = isset($subtopic['params'])? $v->Deserialize($subtopic['params']) : array();
				if($id_subitem>0)
				{
					$content = $this->topic->KeywordItems($id_item,$id_subitem,(int)$params['keyword_topic'],(int)$params['keyword_year'],$sort_by);	
				}
				elseif($params['filter_keywords']!="")
				{
						$filter_keywords = explode(",",$params['filter_keywords']);
						if(is_array($filter_keywords) && count($filter_keywords)>1 && $id_item=="5")
						{
							$id_keywords = array();
							include_once(SERVER_ROOT."/../classes/keyword.php");
							$k = new Keyword();
							foreach($filter_keywords as $filter_keyword)
							{
								$keyword = $k->KeywordCheck($filter_keyword,1);
								if($keyword['id_keyword']>0)
									$id_keywords[] = $keyword['id_keyword'];
							}
							if(count($id_keywords)>0)
							{
								$content = $this->topic->KeywordItemsMultiple($id_item,$id_keywords,(int)$params['keyword_topic'],(int)$params['keyword_year'],$sort_by);	
							}
						}
				}
			break;
			case 16: // topic search
				$templates = $this->topic->Templates(5);
				if(count($templates)>0)
				{
					$ctemplates = array('label'=>$this->tr->Translate("type"));
					$ctemplates['t_x'] = $this->TemplateItem(array('id_template'=>0,'name'=>$this->tr->Translate("all_option")),"template");
					$ctemplates['t_0'] = $this->TemplateItem(array('id_template'=>-1,'name'=>$this->tr->Translate("articles")),"template");
					foreach($templates as $template)
					{
						$ctemplates['t_'.$template['id_template']] = $this->TemplateItem($template,"template");
					}
					$content['templates'] = $ctemplates;
				}
			break;
			case 4:  //map
			case 6: // url
			case 7: // campaign
			case 8: // forum
			case 10:  //events
			case 12:  //module
                if ($id_item == 29 && $id_subitem == 11)
                {
                    $subtopic = $this->topic->SubtopicGet($id_subtopic);
                    $content = $this->Form($id_subtopic,$subtopic['id_subitem_id']);
                }
			case 14: // quotes
			case 16: // topic search
			case 17: // poll
				// Do nothing, should never get here
			break;
		}
		return $content;
	}
	
	private function SubtopicGallerySlideshow($id_item)
	{
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_item);
		$gallery = array();
		if($g->visible)
		{
			$images = $g->Images();
			$simages = array();
			foreach($images as $image)
			{
				$simages[] = $this->ImageItem($image,"item");
			}
			$item = $g->row;
			$item['slideinfo'] = true;
			$gallery['gallery'] = $this->Gallery($item);
			$gallery['gallery']['images'] = $simages; 
		}
		return $gallery;
	}

	private function SubtopicItem($item,$level=0,$is_subtopic_page=false,$id_doc=0)
	{
		$sub = array();
		$sub['xname'] = "subtopic";
		$sub['id'] = $item['id_subtopic'];
		$sub['name'] = $item['name'];
		if(array_key_exists("description",$item)) 
			$sub['description'] = $item['description']; 
		$sub['id_type'] = $item['id_type'];
		$sub['id_item'] = $item['id_item'];
		$sub['id_subitem'] = $item['id_subitem'];
		$sub['id_subitem_id'] = $item['id_subitem_id'];
		if ($level>0)
			$sub['level'] = $level;
		$is_global = ($id_doc>0)? true : (!$this->topic->SubtopicPublishable($item['id_type']) && $this->topic->row['friendly_url']=="0");
		$this->Urlify($sub,"subtopic",array('id'=>$item['id_subtopic'],'id_type'=>$item['id_type'],'id_item'=>$item['id_item'],'id_subitem'=>$item['id_subitem'],'id_subitem_id'=>$item['id_subitem_id'],'link'=>$item['link'],'id_topic'=>$this->topic->id,'is_subtopic_page'=>$is_subtopic_page,'id_doc'=>$id_doc ),$is_global);
		if (array_key_exists("id_image",$item) && $item['id_image']>0)
		{
			$v = new Varia();
			$params = isset($item['params'])? $v->Deserialize($item['params']) : array();
			$image = array();
			$image['xname'] = "image";
			$image['id'] = $item['id_image'];
			$image['width'] = $params['width'];
			$image['height'] = $params['height'];
			$image['format'] = isset($params['format'])? $params['format'] : 'jpg';
			$image_size = $is_subtopic_page? 2 : $params['image_size'];
			$this->Urlify($image,"image",array('id'=>$item['id_image'],'id_subtopic'=>$item['id_subtopic'],'format'=>$image['format'],'size'=>$image_size ));
			$sub['image'] = $image;
		}
		if(array_key_exists("with_children",$item) && $item['with_children'])
		{
			$sub['subtopics'] = $this->SubtopicSubtopics($item['id_subtopic']);
		}
		return $sub;
	}
	
	private function SubtopicPath($id_subtopic,$page=1)
	{
		$this->topic->LoadTree();
		$parents = array();
		$this->topic->SubtopicParents($id_subtopic, $parents);
		$parents = array_reverse($parents);
		$tot_parents = count($parents);
		$subtopics = array();
		$level = 1;
		foreach($parents as $parent)
		{
			$subtopics['s_' . $parent['id_subtopic']] = $this->SubtopicItem($parent,$level);
			if($level==$tot_parents)
				$subtopics['s_' . $parent['id_subtopic']]['page'] = $page;
			$level ++;
		}
		return $subtopics;
	}

	private function SubtopicRss($id_subtopic,$feed_type)
	{
		$subtopic = $this->topic->SubtopicGet($id_subtopic);
		$this->TopicInit($subtopic['id_topic']);
		$rss = array();
		$rss['xname'] = "rss";
		$rss['title'] = $subtopic['name'];
		$rss['description'] = $subtopic['description'];
		$sub = array();
		$this->Urlify($sub,"subtopic",array('id'=>$id_subtopic,'id_type'=>$subtopic['id_type'],'id_item'=>$subtopic['id_item'],'id_subitem'=>$subtopic['id_subitem'],'id_subitem_id'=>$subtopic['id_subitem_id'],'link'=>$subtopic['link'],'id_topic'=>$this->topic->id ));
		$rss['link'] = $sub['url'];
		$latest = $this->topic->SubtopicLatest($id_subtopic,15);
		$items = array('counter'=>count($latest));
		foreach($latest as $art)
		{
			$items['a'.$art['id_article']] = $this->RssItem($art,$feed_type);
		}
		$rss['items'] = $items;
		return $rss;
	}
	
	private function SubtopicSubtopics($id_subtopic)
	{
		$subtopics = array();
		$sub_subtopics = $this->topic->SubtopicSubtopics($id_subtopic);
		foreach($sub_subtopics as $subtopic)
		{
			$subtopics['s_' . $subtopic['id_subtopic']] = $this->SubtopicItem($subtopic);
		}
		return $subtopics;
	}

	private function TemplateItem($item,$name="item")
	{
		$template = array();
		$template['xname'] = $name;
		$template['type'] = "template";
		$template['id'] = $item['id_template'];
		$template['name'] = $item['name'];
		return $template;
	}

	private function TemplateValueItem($item)
	{
		$tvalue = array();
		$tvalue['xname'] = "tvalue";
		$tvalue['id'] = $item['id_template_param'];
		$tvalue['label'] = $item['label'];
		$tvalue['type'] = $item['type'];
		$tvalue['public'] = $item['public'];
		$tvalue['value'] = $this->ParamValue($item['type'],$item['type_params'],$item['value']);
		return $tvalue;
	}

	public function TextGet($id_content)
	{
		$content = "";
		if($id_content>0)
		{
			$row = $this->th->ContentGet($id_content);
			$content = $row['is_html']? $row['content'] : $this->Htmlise($row['content']);
		}
		return $content;
	}
	
	private function TextItem( $item, $name="text" )
	{
		$txt = array();
		$txt['xname'] = $name;
		if ($name!="text")
			$txt['type'] = "text";
		$txt['id'] = $item['id_content'];
		$txt['id_topic'] = $item['id_topic'];
		$txt['id_type'] = $item['id_type'];
		$txt['description'] = $item['description'];
		$txt['content'] = array('xvalue'=>( $item['is_html']? $item['content'] : $this->Htmlise($item['content'],false)));
		return $txt;
	}
		
	private function ThreadDetails($item,$forum)
	{
		$thread = $this->ThreadItem($item);
		$thread['description_long'] = array('xvalue'=>($item['is_html'])? $item['description_long'] : $this->Htmlise($item['description_long']));
		if($item['source']!="")
			$thread['source'] = array('label'=>$this->tr->Translate("source"),'xvalue'=>$this->Htmlise($item['source'],false));
		if($item['id_p']>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$row = $pe->UserGetById($item['id_p']);
			$user = array('xname'=>"author",'label'=>$this->tr->Translate("author"),'id'=>$row['id_p']);
			$user['name'] = "{$row['name1']} {$row['name2']}";
			$user['email'] = $row['email'];
			$thread['author'] = $user;
		}
		include_once(SERVER_ROOT."/../classes/comments.php");
		$this->comments = new Comments("thread",$item['id_thread']);
		$comments = $this->Comments($forum['comments']>0,$forum['active']=="1");
		$thread['comments'] = $comments;
		return $thread;
	}

	private function ThreadItem($item,$name="thread")
	{
		$id_thread = $item['id_thread'];
		$thread = array();
		$thread['xname'] = $name;
		$thread['id'] = $item['id_thread'];
		$thread['type'] = "thread";
		$thread['title'] = $item['title'];
//		if($item['hdate_ts']>0)
//			$thread['hdate'] = $this->FormatDate($item['hdate_ts']);
		$thread['insert_date'] = $this->FormatDate($item['insert_date_ts']);
		$thread['description'] = array('xvalue'=>$this->Htmlise($item['description'],false));
		$this->Urlify($thread,"forum",array('id'=>$item['id_topic_forum'],'id_thread'=>$id_thread,'id_topic'=>$this->topic->id,'subtype'=>"thread"),true);
		return $thread;
	}
	
	private function TimeItem($ts)
	{
		$time = array();
		$t = getdate($ts);
		$time['ts'] = $ts;
		$time['h'] = $t['hours'];
		$time['m'] = $t['minutes'];
		$time['s'] = $t['seconds'];
		$time['label'] = $this->FormatTime($ts);
		return $time;
	}
	
	private function TopicAuth()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session_var = "topicauth" . (int)$this->topic->id;
		$topic_auth = $session->Get($session_var);
		if($topic_auth!="1")
		{
			if (!isset($_SERVER['PHP_AUTH_USER']))
			{
				header("WWW-Authenticate: Basic realm=\"{$this->topic->name}\"");
				header("HTTP/1.0 401 Unauthorized");
				$this->Stop("Access denied to {$this->topic->name}");
			}
			else 
			{
				$username = $_SERVER['PHP_AUTH_USER'];
				$password = $_SERVER['PHP_AUTH_PW'];
				include_once(SERVER_ROOT."/../classes/adminhelper.php");
				$ah = new AdminHelper();
				$id_user = $ah->UserAuth($username,$password);
				if($id_user>0)
				{
					$topic_user = $this->topic->AmIUser($id_user);
					if($topic_user)
						$session->Set($session_var,"1");
					else 
						$this->Stop("Access denied to {$this->topic->name}");
				}
				else
					$this->Stop("Access denied to {$this->topic->name}");
			}
		}
	}
	
	public function TopicCommon($with_navigation=true)
	{
		$common = array();
		$common['topic'] = $this->TopicInfo();
		$page_header = $this->topic->TextGet("page_header");
		if($page_header['content']!="")
			$common['topic']['page_header'] = $this->ExplodeMarkers($page_header['is_html']? $page_header['content'] : $this->Htmlise($page_header['content'],false));
		$page_footer = $this->topic->TextGet("page_footer");
		if($page_footer['content']!="")
			$common['topic']['page_footer'] = $this->ExplodeMarkers($page_footer['is_html']? $page_footer['content'] : $this->Htmlise($page_footer['content'],false));
		if ($with_navigation)
			$common['menu'] = $this->Navigation(0);
		return $common;
	}
	
	private function TopicEvents( $how_many )
	{
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$ee->id_topic = $this->topic->id;
		$ee->id_group = $this->topic->id_group;
		$hevents = $ee->Next( $how_many, 0, false );
		$subtopics = $this->topic->EventsSubtopics();
		$events = array('how_many'=>"$how_many",'id_subtopic'=>$subtopics[0]['id_subtopic'],'subtopic'=>$subtopics[0]['name']);
		foreach($hevents as $hevent)
			$events['e_' . $hevent['id_event']] = $this->EventItem($hevent);
		return $events;
	}
	
	private function TopicHome()
	{
		$t = $this->topic;
		$topic_common = isset($this->cache_topic)? $this->cache_topic : $this->TopicCommon();
		if($t->row['home_keywords']!="")
		{
			$th_keywords = explode(",",$t->row['home_keywords']);
			if($th_keywords!==false && is_array($th_keywords) && count($th_keywords)>0)
			{
				$counter = 1;
				foreach($th_keywords as $th_keyword)
				{
					$topic_common['topic']['keywords']['thk'.$counter] = array('xname'=>"keyword",'name'=>$th_keyword,'id_type'=>0);
					$counter ++;
				}
			}
		}
		$topic_home = array_merge($this->Common($t->id,$this->id_type),$topic_common);
		$home_header = $t->TextGet("home_header");
		if($home_header['content']!="")
			$topic_home['home_header'] = $this->ExplodeMarkers($home_header['is_html']? $home_header['content'] : $this->Htmlise($home_header['content']));
		$home_footer = $t->TextGet("home_footer");
		if($home_footer['content']!="")
			$topic_home['home_footer'] = $this->ExplodeMarkers($home_footer['is_html']? $home_footer['content'] : $this->Htmlise($home_footer['content']));
		switch($t->row['home_type'])
		{
			case "0":
			if ($t->row['id_article_home']>0)
			{
				$topic_home = array_merge($topic_home,$this->ArticleContent($t->row['id_article_home'],false));
			}
			break;
			case "1":
				$harticles = array();
				$t->HomepageArticles($harticles);
				$articles = array();
				foreach($harticles as $harticle)
				{
					$articles['a_' . $harticle['id_article']] = $this->ArticleItem($harticle,"item");
				}
				$topic_home['items'] = $articles;
			break;
			case "2":
				$last = $t->HomepageLastArticle();
				if($last['id_article']>0)
					$topic_home = array_merge($topic_home,$this->ArticleContent($last['id_article'],false));
			break;
			case "3":
				$latest = $t->Latest($t->row['articles_per_page'],$t->row['home_sort_by']);
				$articles = array();
				foreach($latest as $last)
				{
					$articles['a_' . $last['id_article']] = $this->ArticleItem($last,"item");
				}
				$topic_home['items'] = $articles;
			break;
			case "4":
				$subtopic = $this->topic->SubtopicGet($t->row['id_article_home']);
				$sub_content = $this->SubtopicContentPaged($subtopic['id_subtopic'],$subtopic['id_type'],$subtopic['id_item'],$subtopic['id_subitem'],$subtopic['sort_by'],$subtopic['records_per_page']);
				$sub_item = $this->SubtopicItem($subtopic);
				$sub_item['content'] = ($subtopic['id_type']<4)? $sub_content[0]:$sub_content;
				$topic_home['subtopic'] = $sub_item;
			break;
			case "5":
				$harticles = array();
				$t->HomepageArticles($harticles);
				$articles = array();
				foreach($harticles as $harticle)
				{
					$article_content = $this->ArticleContent($harticle['id_article'],false,false);
					$art_item = $article_content['article'];
					$art_item['xname'] = "item";
					$articles['a_' . $harticle['id_article']] = $art_item;
				}
				$topic_home['items'] = $articles;
			break;
			case "6":
				$latest = $t->Latest($t->row['articles_per_page'],$t->row['home_sort_by']);
				$articles = array();
				foreach($latest as $last)
				{
					$article_content = $this->ArticleContent($last['id_article'],false,false);
					$art_item = $article_content['article'];
					$art_item['xname'] = "item";
					$articles['a_' . $last['id_article']] = $art_item;
				}
				$topic_home['items'] = $articles;
			break;
		}
		$feature_params = array('id_topic'=>$t->id,'id_topic_group'=>$t->row['id_group']);
		$topic_home['features'] = $this->Features($this->id_type,0,$feature_params);
		return $topic_home;
	}
	
	private function TopicImageItem($image, $is_thumb, $name="gallery_image")
	{
		return $this->GenericImageItem($image,$is_thumb,false,$name,"gallery_image");
	}

	private function TopicIncludes()
	{
		$includes = array_merge($this->Common($this->topic->id,$this->id_type),$this->TopicCommon());
		return $this->xh->Array2Xml($includes);
	}
	
	private function TopicInfo()
	{
		$t = $this->topic;
		$topic_info = array();
		$topic_info['id'] = $t->id;
		$topic_info['name'] = $t->name;
		$topic_info['description'] = $t->row['description'];
		$topic_info['lastupdate'] = $this->LastUpdate();
		$topic_info['id_group'] = $t->id_group;
		$topic_info['home_type'] = $t->row['home_type'];
		$topic_info['archived'] = $t->row['archived'];
		$topic_info['show_path'] = $t->row['show_path'];
		$topic_info['show_print'] = $t->row['show_print'];
		$topic_info['comments'] = $t->row['comments'];
		$topic_info['profiling'] = $t->profiling;
		$topic_info['protected'] = $t->protected;
		$topic_info['keywords'] = array_merge($this->Keywords($t->id,"topic"), $this->Keywords($t->id_group,"topics_group"));
		$languages = $this->tr->languages;
		$topic_info['id_language'] = $t->id_language;
		$topic_info['lang'] = $languages[$t->id_language];
		$topic_info['groups'] = $this->TopicPath();
		$topic_params = array('id_topic'=>$t->id);
		$this->Urlify($topic_info,"topic_home",$topic_params);
		$rss = array();
		$this->Urlify($rss,"rss",array('subtype'=>"topic",'id'=>$t->id),true);
		$topic_info['rss'] = $rss;
		$search_params = array('name'=>$this->tr->Translate("search"));
		$this->Urlify($search_params,"search",$topic_params,TRUE);
		$topic_info['search'] = $search_params;
		$geosearch_params = array('name'=>$this->tr->Translate("geo_search"));
		$this->Urlify($geosearch_params,"geosearch",array('id'=>0,'id_topic'=>$t->id),true);
		$topic_info['geosearch'] = $geosearch_params;
		$events_params = array('name'=>$this->tr->Translate("calendar"));
		$this->Urlify($events_params,"events",$topic_params,TRUE);
		$topic_info['events'] = $events_params;
		$quotes_params = array('name'=>$this->tr->Translate("quotes"));
		$this->Urlify($quotes_params,"quotes",$topic_params,TRUE);
		$topic_info['quotes'] = $quotes_params;
		if ($t->row['domain']!="")
		{
			$topic_info['domain'] = $t->row['domain'];
			$topic_info['path'] = $t->path;
			if(($this->conf->Get("track") || $this->conf->Get("track_all")) && !$this->live && !$this->preview )
				$topic_info['tracking'] = "1";
		}
		if ($this->IsModuleActive("lists"))
		{
			$lists = $this->topic->Lists();
			if(count($lists)>0)
			{
				$tlists = array();
				foreach($lists as $list)
				{
					$tlists['l_'.$list['id_list']] = $this->MailingListItem($list);
				}
				$topic_info['lists'] = $tlists;
			}
		}
		if($this->live && $t->profiling)
		{
			$topic_info['user'] = $this->PeopleUser(true);
		}
		return $topic_info;
	}
	
	public function TopicInit($id_topic,$with_translator=false)
	{
		if (!is_object($this->topic) || $id_topic!=$this->topic->id || $with_translator)
		{
			if(isset($this->topic))
			{
				$this->topic->__destruct();
			}
			unset($this->topic);
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->topic = new Topic($id_topic);
			
			$records_per_page = ($this->topic->records_per_page>0)? $this->topic->records_per_page : $this->records_per_page_default;
			if($records_per_page != $this->records_per_page)
				$this->RecordsPerPage($records_per_page);
			
			if($with_translator)
			{
				$id_language = ($id_topic>0)? $this->topic->id_language : $this->ini->Get('id_language');
				$id_style = ($id_topic>0)? $this->topic->id_style : 0;
				$this->TranslatorInit($id_language,$id_style);
			}
			
			$protected = $this->topic->protected=="2";
			if($protected!=$this->protected)
			{
				// Update protected status
				$this->irl = new IRL($protected);
				$this->protected = $protected;
				$this->pt->ft->LayoutMode($this->preview,$protected,$this->live);
				unset($this->xh);
				$this->xh = new XmlHelper($this->preview && !$protected);
			}
		}
		if(!is_object($this->topic->queue))
			$this->topic->queue = new Queue($id_topic);
	}
	
	public function TranslatorInit($id_language,$id_style)
	{
		if(isset($this->tr))
			$this->tr->__destruct();
		unset($this->tr);
		$this->tr = new Translator($id_language,0,false,$id_style);
		$this->id_language = $id_language;
		$this->dt = new DateTimeHelper($this->tr->Translate("month"),$this->tr->Translate("weekdays"),$this->tr->Translate("hours"));
	}

	public function TopicItem($item,$name="topic")
	{
		$this->TopicInit($item['id_topic']);
		$topic = array();
		$topic['xname'] = $name;
		if ($name!="topic")
			$topic['type'] = "topic";
		$topic['id'] = $item['id_topic'];
		$topic['name'] = $item['name'];
		$topic['archived'] = $item['archived'];
		if($item['description']!="")
			$topic['description'] = array('xvalue'=>$item['description']);
		if ($item['show_latest'])
		{
			$rss = array();
			$this->Urlify($rss,"rss",array('subtype'=>"topic",'id'=>$item['id_topic']),true);
			$topic['rss'] = $rss;
		}
		$this->Urlify($topic,"topic_home",array('id_topic'=>$item['id_topic']));
		return $topic;
	}

	private function TopicPath()
	{
		include_once(SERVER_ROOT."/../classes/topics.php");
		$id_group = $this->topic->id_group;
		$this->groups = new Topics();
		$this->groups->gh->LoadTree();
		$parents = array();
		$this->groups->gh->th->GroupParents($id_group, $parents);
		$parents = array_reverse($parents);
		$groups = array();
		$level = 1;
		reset($parents);
		foreach($parents as $parent)
		{
			$groups['g_' . $parent['id_group']] = array('xname'=>"group",'id'=>$parent['id_group'],
			'level'=>$level,'name'=>$parent['name']);
			$level ++;
		}
		return $groups;
	}

	private function TopicRss($id_topic,$feed_type)
	{
		$this->TopicInit($id_topic);
		$rss = array();
		$rss['xname'] = "rss";
		$rss['title'] = $this->topic->name;
		$rss['description'] = $this->topic->description;
		$rss['link'] = $this->topic->url;
		$latest = $this->topic->Latest(15);
		$items = array('counter'=>count($latest));
		foreach($latest as $art)
		{
			$items['a'.$art['id_article']] = $this->RssItem($art,$feed_type);
		}
		$rss['items'] = $items;
		return $rss;
	}
	
	public function Urlify(&$array,$type,$params,$is_global=FALSE)
	{
		if($this->preview)
		{
			$array['qs'] = $this->irl->Preview($type,$params,$is_global);
		}
		if($type=="rss" && $params['subtype']=="topic")
		{
			$this->irl->topic = $this->topic;
		}
		$array['url'] = ($is_global)? $this->irl->PublicUrlGlobal($type,$params) : $this->irl->PublicUrlTopic($type,$params,$this->topic);
		if(isset($params['jump']) && $params['jump']!="")
		{
			$array['jumping'] = "1";
		}
	}
	
	private function User($params)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$user = new User();
		if($params['login']!="")
		{
			$user->UserSet($params['login']);
			$this->subtype = "user";
		}
		if($params['u']>0)
		{
			$user->id = $params['u'];
			$this->subtype = "user";
		}
		elseif ($params['t']>0)
		{
			$user->id = $params['t'];
			$this->subtype = "translator";
		}
		elseif ($params['b']>0)
		{
			$user->id = $params['b'];
			$this->subtype = "bio";
		}
		elseif ($params['c']>0)
		{
			$user->id = $params['c'];
			$this->subtype = "contact";
		}
		$u = array();
		if ($user->id>0)
		{
			$row = $user->UserGet();
			if($row['user_show'])
			{
				$u = $this->Common(0,$this->id_type);
				$u['features'] = $this->Features($this->id_type,0,array(),TRUE);
				$uinfo = array();
				$uinfo['id'] = $user->id;
				$uinfo['name'] = $row['name'];
				if($row['email_visible']=="1")
					$uinfo['email'] = $row['email'];
				$uarray = $this->subtype == 'translator'? array('t'=>$user->id) : array('u'=>$user->id);
				$this->Urlify($uinfo,"user",$uarray,true);
				if($row['photo_visible'])
				{
					$thumb = array();
					$thumb['xname'] = "image";
					$img_sizes = $this->conf->Get("img_sizes");
					$thumb['width'] = $img_sizes[0];
					$this->Urlify($thumb,"user_image",array('id'=>($user->id)),TRUE );
					$uinfo['image'] = $thumb;
				}
				$contact = array('label'=>$this->tr->Translate("contact"));
				$this->Urlify($contact,"user",array('c'=>$user->id),true);
				$uinfo['contact'] = $contact;
				if($row['notes']!="")
				{
					$bio = array('label'=>$this->tr->Translate("biography"));
					$bio['xvalue'] = $this->Htmlise($row['notes'],false);
					$this->Urlify($bio,"user",array('b'=>$user->id),true);
					$uinfo['bio'] = $bio;
				}
				$this->RecordsPerPage($this->conf->Get("records_per_page"));
				$rows = array();
				$num = $user->ArticlesVisible($rows);
				$uinfo['tot_articles'] = $num;
				$tras = array();
				$num_tras = $user->TranslationsVisible($tras);
				if($num_tras>0)
				{
					$translations = array();
					$this->Urlify($translations,"user",array('t'=>$user->id),true);
					$uinfo['translations'] = $translations;
				}
				$uinfo['tot_translations'] = $num_tras;
				if($this->subtype=="user" || $this->subtype=="translator")
				{
					$articles = array();
					$articles['items'] = $this->Page( $this->subtype=="translator"? $tras : $rows , $this->subtype=="translator"? $num_tras : $num );
					$uinfo['articles'] = $articles;
				}
				$u['admin_user'] = $uinfo;
			}
			else 
				$this->Stop();
		}
		else 
			$this->Stop();
		return $u;
	}
	
	private function VideoDetails($item,$with_embed=false)
	{
		$video = $this->VideoItem($item);
		$video['views'] = $item['views'];
		$video['width'] = $this->conf->Get("video_resize_width");
		$video['height'] = $this->conf->Get("video_resize_height");
		$video['auto_start'] = $item['auto_start'];
		$watermark = array();
		$this->Urlify($watermark,"watermark",array(),true);
		$video['watermark'] = $watermark;
		$video['hash'] = $item['hash'];
		$video_image = array();
			$this->Urlify($video_image,"video_image",array('id'=>$item['id_video'],'seq'=>$item['image_seq']),true);
		$video['image'] = $video_image;
		$video_flv = array();
			$this->Urlify($video_flv,"video_enc_link",array('hash'=>$item['hash'],'id'=>$item['id_video']),true);
		$video['flv'] = $video_flv;
		$video_xml = array();
			$this->Urlify($video_xml,"video_xml",array('hash'=>$item['hash']),true);
		$video['xml'] = $video_xml;
		if($item['link']!="")
			$video['link'] = $item['link'];
		if($item['source']!="")
			$video['source'] = array('xvalue'=>$this->Htmlise($item['source'],false));
		if($item['description']!="")
			$video['description'] = array('xvalue'=>$this->Htmlise($item['description'],false));
		if ($this->ini->Get("licences") && $item['id_licence'] > 0)
			$video['licence'] = $this->Licence($item['id_licence'],$item['author']);
		return $video;
	}
	
	private function VideoItem($item,$name="video")
	{
		$vid = array();
		$vid['xname'] = $name;
		if ($name!="video")
			$vid['type'] = "video";
		$vid['id'] = $item['id_video'];
		$vid['title'] = $item['title'];
		$vid['date'] = $this->FormatDate($item['insert_date_ts']);
		if($item['length']>0)
		{
			$vid['length'] = $this->FormatMinutes($item['length']);
			$vid['seconds'] = $item['length'];
		}
		if($item['author']!="")
			$vid['author'] = $item['author'];
		$img_sizes = $this->conf->Get("img_sizes");
		$video_thumb = array('width'=>$img_sizes[0]);
		$this->Urlify($video_thumb,"video_thumb",array('id'=>$item['id_video'],'hash'=>$item['hash']),true);
		$vid['thumb'] = $video_thumb;
		$this->Urlify($vid,"media",array('subtype'=>"video",'hash'=>$item['hash'],'id_topic'=>$this->topic->id),true);
		return $vid;
	}
	
	private function WeekCurrent($ts)
	{
		$week = array();
		$week['n'] = $this->dt->WeekNumber($ts);
		if($this->conf->Get('calendar_version')==1) {
			$current_wday = $this->dt->Dow($ts);
			$monday_ts = $ts - (($current_wday)*SECONDS_PER_DAY) + SECONDS_PER_DAY;
			$monday_offset = round(($monday_ts - time())/SECONDS_PER_DAY);
			for($i=0;$i<7;$i++)
			{
				$tsd = $monday_ts + ($i*SECONDS_PER_DAY);
				$day = $this->DayItem($tsd);
				$day['xname'] = "day";
				$day['offset'] = $monday_offset + $i;
				$week['day_'.$i] = $day;
			}
			$week_prev = array();
			$this->Urlify($week_prev,"events",array('id_topic'=>$this->topic->id,'offset'=>($monday_offset - 7),'subtype'=>"day"),true);
			$week['prev'] = $week_prev;
			$week_next = array();
			$this->Urlify($week_next,"events",array('id_topic'=>$this->topic->id,'offset'=>($monday_offset + 7),'subtype'=>"day"),true);
			$week['next'] = $week_next;
		}
		return $week;
	}

	private function WidgetItem($item,$name="widget")
	{
		$widget = array();
		$widget['xname'] = $name;
		if ($widget!="widget")
			$widget['type'] = "widget";
		$widget['id'] = $item['id_widget'];
		$widget['title'] = $item['title']; 
		if($item['description']!="")
			$widget['description'] = $item['description'];
		$widget['status'] = $item['status']; 
		$widget['status_name'] = $item['status_name']; 
		if($item['comment_visible']=="1")
			$widget['comment'] = $item['comment'];
		if ($item['widget_type'] != "")
			$widget['widget_type'] = $item['widget_type'];
		if ($item['can_edit'] != "")
			$widget['can_edit'] = $item['can_edit'];
		return $widget;
	}

	private function WidgetCategoryItem($item,$name="category",$with_widgets=false,$show_selected_widgets=false)
	{
		$cat = array();
		//$cat['xname'] = "category";
		$cat['xname'] = $name;
		$cat['id'] = $item['id_category'];
		$cat['name'] = $item['name'];
		if($item['description']!="")
			$cat['description'] = $item['description'];
		if ($item['library_show_widget'] != "")
			$cat['library_show_widget'] = $item['library_show_widget'];
		$this->Urlify($cat,"homepage",array('id'=>$item['id_category'],'subtype'=>"widget_category"),true);
		if($with_widgets)
		{
			$wi = new Widgets();
			
			if($show_selected_widgets)
			{
				$widgets = $wi->CategoryWidgetsSelection($item['id_category']);
				
				$num_widgets = count($widgets);
				if($num_widgets>0)
				{
					$cwidgets = array();
					foreach($widgets as $widget)
					{
						$cwidgets['w'.$widget['id_widget']] = $this->WidgetItem($widget);
					}
					$cat['widgets'] = $cwidgets;
				}
			}
			else 
			{
				$widgets = array();
				$num_widgets = $wi->CategoryWidgets($widgets,$item['id_category'],false,true);
				if($num_widgets>0)
				{
					$cat['items'] = $this->Page($widgets,$num_widgets,$wi->widgets_per_page);
				}
			}
			$cat['num_widgets'] = $num_widgets;
		}
		else
		{
			// no display widgets under category
			$wi = new Widgets();
			
			$cat['num_widgets'] = $wi->TotalApprovedWidgetsByCategory($cat['id']);
		}
		return $cat;
	}
	
	private function WidgetSearch($unescaped_query,$id_category)
	{
		$unescaped_query = $this->th->UtfClean($unescaped_query);
		$strlen = $this->th->StringLength($unescaped_query);
		$min_str_length = $this->conf->Get("min_str_length");
		$search = array();
		if ($strlen>=$min_str_length)
		{
			$search  = array('qw'=>$unescaped_query);
			$wi = new Widgets();
			$widgets = array();
            if ($this->th->IsUnicode($unescaped_query))
            {
                $unescaped_query = $this->th->str_split_utf8($unescaped_query);
                $unescaped_query = implode(' ', $unescaped_query);  
            }
			$search_words = $this->th->SearchWords($unescaped_query,$this->id_language);
			$num_widgets = $wi->WidgetSearchPub($widgets,$search_words, $id_category);
			$widgets_paged = $this->Page($widgets,$num_widgets,$wi->widgets_per_page);
			$search['widgets'] = $widgets_paged;
			$this->Urlify($search,"homepage",array('qw'=>$unescaped_query,'subtype'=>"widget_search"),true);
		}
		else
		{
			$search  = array('qw'=>$unescaped_query);
			$widgets_paged = $this->Page(array(),0);
			$search['widgets'] = $widgets_paged;
		}
		return $search;
	}

	private function WebFeedItemSearch($unescaped_query,$id_category)
	{
		include_once(SERVER_ROOT."/../classes/web_feeds.php");   
		$unescaped_query = $this->th->UtfClean($unescaped_query);
		$strlen = $this->th->StringLength($unescaped_query);
		$min_str_length = $this->conf->Get("min_str_length");
		$search = array();
		if ($strlen>=$min_str_length)
		{
			$search  = array('qw'=>$unescaped_query);
			
			$wf = new WebFeeds();
			$widget_items = array();
			$search_words = $this->th->SearchWords($unescaped_query,$this->id_language);
			$num_widget_items = $wf->WebFeedItemSearchPub($widget_items, $search_words,$id_category);
			$widget_items_paged = $this->Page($widget_items, $num_widget_items,$wf->widgets_per_page);
			$search['web_feed_items'] = $widget_items_paged;     
			$this->Urlify($search,"homepage",array('qw'=>$unescaped_query,'subtype'=>"widget_content_search"),true);
		}
		else
		{
			$search  = array('qw'=>$unescaped_query);
			$widget_items_paged = $this->Page(array(),0);
			$search['web_feed_items'] = $widget_items_paged;
		}
		return $search;
	}
	
	private function WebFeedItem($item,$name="web_feed_item")
	{
		$wfi = array();
		$wfi['xname'] = $name;
		if ($wfi!="web_feed_item")
			$wfi['type'] = "web_feed_item";
        if (isset($item['id']))
            $wfi['id'] = $item['id'];
        else
            $wfi['id'] = $item['id_widget'];
		$wfi['title'] = $item['title']; 
		if($item['description']!="")
			$wfi['description'] = $item['description'];
		$wfi['id_widget'] = $item['id_widget'];
		$wfi['widget_title'] = $item['widget_title'];
		$wfi['widget_description'] = $item['widget_description'];
		if ($item['link_url'] != "")
			$wfi['link_url'] = $item['link_url'];
		return $wfi;
	}
	
}
?>
