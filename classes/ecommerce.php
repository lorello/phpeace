<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * Internal eCommerce module
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 * @author Boon Choon Ang
 */
class eCommerce
{
	/**
	 * Payment gateway to use
	 *
	 * @var string
	 */
	public $pgw;
	
	/**
	 * Index of images array for product thumb width
	 *
	 * @var integer
	 */
	public $product_thumb;
	
	/**
	 * Index of images array for product image width
	 *
	 * @var integer
	 */
	public $product_image;
	
	/**
	 * If greater than 0, enforce a specific amount of products per each order
	 *
	 * @var integer
	 */
	public $products_per_order;
	
	/**
	 * Email address to send notifications on each product purchase
	 *
	 * @var string
	 */
	private $notify;
	
	/**
	 * Send confirmation email to buyer once order has been processed
	 *
	 * @var boolean
	 */
    private $send_order_confirm_mail;
	
	/**
	 * Whether to store ecommerce payments in the finance module
	 *
	 * @var boolean
	 */
    public $store_payments;
	
    /**
	 * Currency code to use
	 *
	 * @var string
	 */
	public $currency;
	
	/**
	 * If payments are stored, which cost centre ID to use
	 *
	 * @var integer
	 */
	public $id_payment_type;

	/**
	 * If PayPal is the payment gateway, which PayPal account ID to use
	 *
	 * @var integer
	 */
	public $id_account;
	
	/**
	 * Initialize local variables
	 *
	 */
	function __construct()
	{
		$conf_ecommerce = new Configuration("ecommerce");
		$this->pgw = $conf_ecommerce->Get("pgw");
		$this->product_thumb = $conf_ecommerce->Get("product_thumb");
		$this->product_image = $conf_ecommerce->Get("product_image");
		$this->products_per_order = $conf_ecommerce->Get("products_per_order");
		$this->notify = $conf_ecommerce->Get("notify");
		$this->currency = $conf_ecommerce->Get("currency");
        $this->send_order_confirm_mail = $conf_ecommerce->Get("send_order_confirm_mail");
        $this->store_payments = $conf_ecommerce->Get("store_payments") || ($this->pgw=="paypal");
        $ini = new Ini();
		$this->id_payment_type = $ini->GetModule("ecommerce","id_payment_type",0);
		$this->id_account = $ini->GetModule("ecommerce","id_account",0);
	}
	
	/**
	 * Upade module configuration settings
	 * 
	 * @param integer $id_payment_type	Cost Centre ID
	 * @param integer $id_account		Account ID
	 */
	public function ConfigurationUpdate($id_payment_type,$id_account)
	{
		$ini = new Ini;
		$ini->SetModule("ecommerce","id_payment_type",$id_payment_type);
		$ini->SetModule("ecommerce","id_account",$id_account);
	}
	
	/**
	 * Is portal user authorized for this specific order
	 *
	 * @param integer $id_p
	 * @param integer $id_order
	 * @return boolean
	 */
	public function IsUserAuthorized($id_p,$id_order)
	{
		$order = $this->OrderGet($id_order);
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetById($id_p);
		return $order['id_p']==$id_p && $user['id_p']>0;
	}
	
	/**
	 * Associate a specific order to a portal user
	 *
	 * @param integer $id_order
	 * @param integer $id_p
	 */
	public function OrderAssociate($id_order,$id_p)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "orders" );
			$sqlstr = "UPDATE orders SET id_p='$id_p' WHERE id_order='$id_order' ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}
	
	/**
	 * Create a new order based on a single product
	 *
	 * @param integer 	$id_product		Product ID to add to order
	 * @param integer 	$id_p			Portal user
	 * @param integer 	$quantity		Product quantity
	 * @param string 	$delivery_notes	Delivery notes
	 * @return integer					ID of newly created order
	 */
	public function OrderCreate($id_product,$id_p,$quantity,$delivery_notes)
	{
		$product = $this->ProductGet($id_product);
		if($product['stock']>0)
		{
			$db =& Db::globaldb();
			$today = $db->GetTodayTime();
			$id_order = $this->OrderStore(0,$today,0,$delivery_notes);
			if($id_order>0)
			{
				$this->OrderProductStore($id_order,$id_product,$quantity,$product['price']);
				$this->OrderAssociate($id_order,$id_p);
			}
		}
		return $id_order;
	}
    
    /**
     * Create a new order based on multiple items from shopping cart
     *
     * @param integer $id_p
     * @param string $delivery_notes
     * @param array $shopping_items
     * @return integer
     */
    public function OrderMultiProductCreate($id_p,$delivery_notes,$shopping_items)
    {
        $db =& Db::globaldb();
        $today = $db->GetTodayTime();
        $id_order = $this->OrderStore(0,$today,0,$delivery_notes);
        if($id_order>0)
        {
            foreach($shopping_items as $item)
            {
            	$id_product = (int)$item['id_product'];
            	$quantity = (int)$item['quantity'];
            	if($id_product>0 && $quantity>0)
            	{
	                $product = $this->ProductGet($id_product);
	                $this->OrderProductStore($id_order,$id_product,$quantity,$product['price']);        		
            	}
            }
            $this->OrderAssociate($id_order,$id_p);
        }
        return $id_order;
    }
	
	/**
	 * Delete order
	 *
	 * @param integer $id_order
	 */
	public function OrderDelete( $id_order )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "order_products" );
		$res[] = $db->query( "DELETE FROM order_products WHERE id_order=$id_order ");
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "orders" );
		$res[] = $db->query( "DELETE FROM orders WHERE id_order=$id_order ");
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve order data
	 *
	 * @param integer $id_order
	 * @param boolean $with_total	Calculate total price
	 * @return array
	 */
	public function OrderGet($id_order,$with_total=false)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT o.id_order,UNIX_TIMESTAMP(o.order_date) AS order_date_ts,o.id_status,UNIX_TIMESTAMP(ol.action_date) AS payment_date_ts,
			o.delivery_notes,o.id_p,o.id_transaction,o.admin_notes
			FROM orders o 
            LEFT JOIN order_action_log ol ON o.id_order=ol.id_order
			WHERE o.id_order='$id_order' ";
		$db->query_single($row,$sqlstr);
		if($with_total)
		{
			$total = 0;
			$products = $this->OrderProducts($id_order);
			foreach($products as $product)
			{
				$total += $product['quantity'] * $product['price'];
			}
			$row['total'] = sprintf("%01.2f", $total);
		}
		return $row;
	}
	
	/**
	 * Notify order status change
	 *
	 * @param integer $id_order			Order ID
	 * @param integer $id_status		Current status ID
	 * @param integer $id_status_old	Previous status ID
	 */
	private function OrderStatusChangeNotify($id_order,$id_status,$id_status_old)
	{
		$admin_email = $this->notify;
        
        if ($admin_email != '')
        {
            include_once(SERVER_ROOT."/../classes/translator.php");
            $session = new Session();
            $trm27 = new Translator($session->Get("id_language"),27);
            include_once(SERVER_ROOT."/../classes/mail.php");
            include_once(SERVER_ROOT."/../classes/varia.php");
            $mail = new Mail();
            if($id_status == "0")
            {
                $message = $trm27->Translate("order_new");
                $subject = "[Order #$id_order] New order";
            }
            else 
            {
                $statuses = $trm27->Translate("order_status");
                $message = $trm27->TranslateParams("order_changed",array($statuses[$id_status_old],$statuses[$id_status]));
                $subject = "[Order #$id_order] Status change: {$statuses[$id_status]}";
            }
            $message .= "\n" . $mail->admin_web . "/ecommerce/order.php?id=$id_order\n\n";
            $mail->SendMail($admin_email,"",$subject,$message,array());
        }
	}
	
	/**
	 * Remove a product from an existing order
	 *
	 * @param integer $id_order
	 * @param integer $id_product
	 */
	public function OrderProductDelete( $id_order,$id_product )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "order_products" );
		$res[] = $db->query( "DELETE FROM order_products WHERE id_order=$id_order AND id_product=$id_product ");
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve product information from a specific order
	 *
	 * @param integer $id_order
	 * @param integer $id_product
	 * @return array
	 */
	public function OrderProductGet($id_order,$id_product)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT op.id_product,op.quantity,op.price
			FROM order_products op
			WHERE id_order='$id_order' AND id_product='$id_product' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Store product data in a specific order
	 *
	 * @param integer $id_order
	 * @param integer $id_product
	 * @param integer $quantity
	 * @param decimal $price
	 */
	public function OrderProductStore($id_order,$id_product,$quantity,$price)
	{
		$product = $this->OrderProductGet($id_order,$id_product);
		if($this->products_per_order>0)
		{
			$quantity = $this->products_per_order;
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "order_products" );
		if($product['id_product']>0)
		{
			$sqlstr = "UPDATE order_products SET quantity='$quantity',price='$price'
				WHERE id_order='$id_order' AND id_product='$id_product' ";
		}
		else
		{
			$sqlstr = "INSERT INTO order_products (id_order,id_product,quantity,price)
				 VALUES ('$id_order','$id_product','$quantity','$price')";		
		}
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve all products in a specific order
	 *
	 * @param integer $id_order
	 * @return array
	 */
	public function OrderProducts($id_order)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT op.id_product,op.quantity,p.stock,op.price,p.name,p.description
			FROM order_products op
			INNER JOIN products p ON op.id_product=p.id_product
			WHERE op.id_order='$id_order' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Order has been paid
	 *
	 * @param integer $id_order
	 * @param string $id_transaction
	 * @param integer $id_topic
	 */
	public function OrderPaid($id_order,$id_transaction,$id_topic)
	{
		$id_status = 1;
		$order = $this->OrderGet($id_order);
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables( array("orders", "order_action_log") );
		$sqlstr = "UPDATE orders SET id_status='$id_status',id_transaction='$id_transaction' WHERE id_order='$id_order' ";
		$res[] = $db->query($sqlstr);

        $sqlstr = "REPLACE INTO order_action_log (id_order, action_type, action_date) VALUES ('$id_order', 1, NOW()) ";
        $res[] = $db->query($sqlstr);
        
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/people.php");
		$t = new Topic($id_topic);
		$trm27 = new Translator($t->id_language,27,false,$t->id_style);
		
		if($this->store_payments && $this->id_payment_type>0 && $this->id_account>0)
		{
			include_once(SERVER_ROOT."/../classes/payment.php");
			$p = new Payment();
			$p->PaymentVerify($id_transaction,false);
		}
		if ($this->send_order_confirm_mail)
        {
            $pe = new People();
            $user = $pe->UserGetById($order['id_p']);
            $mail = new Mail();
            $subject = "[" . ($id_topic>0? $t->name : $mail->title) . "] - Order confirmation";
            $order_url = $mail->pub_web . "/ecomm/order.php?id=$id_order&id_topic=$id_topic";
            $message = $trm27->TranslateParams("order_confirmation",array("{$user['name1']} {$user['name2']}",$id_order,$id_transaction,$order_url,$mail->title));
            $mail->SendMail($user['email'],"{$user['name1']} {$user['name2']}",$subject,$message,array());
        }
        $this->OrderStatusChangeNotify($id_order,$id_status,$order['id_status']);
	}

	/**
	 * Store order details
	 *
	 * @param integer 	$id_order
	 * @param datetime 	$order_date
	 * @param integer 	$id_status
	 * @param string 	$delivery_notes
	 * @param datetime 	$payment_date
	 * @return integer
	 */
	public function OrderStore($id_order,$order_date,$id_status,$delivery_notes,$payment_date='')
	{
		$order = $this->OrderGet($id_order);
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables( array("orders", "order_action_log") );
		if($id_order>0)
		{
			$sqlstr = "UPDATE orders SET order_date='$order_date',id_status='$id_status',
				delivery_notes='$delivery_notes'
			  WHERE id_order='$id_order' ";
			$notify = $order['id_status']!=$id_status;
		}
		else 
		{
			$id_order = $db->nextId( "orders", "id_order" );
			$sqlstr = "INSERT INTO orders (id_order,order_date,id_status,delivery_notes)
			 VALUES ($id_order,'$order_date',0,'$delivery_notes')";
			$notify = true;
		}
		$res[] = $db->query($sqlstr);

        if ($id_status > 0)
            $sqlstr = "REPLACE INTO order_action_log (id_order, action_type, action_date) VALUES ('$id_order', 1, '$payment_date') ";
        else
            $sqlstr = "DELETE FROM order_action_log WHERE id_order = '$id_order' ";
        $res[] = $db->query($sqlstr);

		Db::finish( $res, $db);
		if($notify && $this->notify!="")
		{
			$this->OrderStatusChangeNotify($id_order,$id_status,$order['id_status']);
		}
		return $id_order;
	}

	/**
	 * Order list (paginated)
	 *
	 * @param array 	$rows		Orders
	 * @param integer 	$id_status	Filter by order status
	 * @param array 	$params		Search parameters
	 * @param boolean 	$paged		
	 * @return integer				Num of orders
	 */
	public function Orders(&$rows,$id_status=-1,$params=array(),$paged=true)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT o.id_order,UNIX_TIMESTAMP(o.order_date) AS order_date_ts,o.id_status,o.delivery_notes,UNIX_TIMESTAMP(IFNULL(ol.action_date,'')) AS payment_date_ts
			FROM orders o 
            LEFT JOIN order_action_log ol ON ol.id_order = o.id_order
            WHERE 1=1 ";
		if($id_status>-1)
			$sqlstr .= " AND o.id_status=$id_status ";
        if (strtotime($params['start_date']) > 0)
        {
            $start_date = date('Y-m-d', strtotime($params['start_date']));
            $sqlstr .= " AND o.order_date >= '{$start_date}' ";
        }
        if (strtotime($params['end_date']) > 0)
        {
            $end_date = date('Y-m-d', strtotime("+1day",strtotime($params['end_date'])));
            $sqlstr .= " AND o.order_date < '{$end_date}' ";
        }
		$sqlstr .= " ORDER BY o.id_order DESC ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	/**
	 * All orders placed by a specific portal user
	 *
	 * @param integer $id_p
	 * @return array
	 */
	public function OrdersPerson($id_p)
	{
		$rows = array();
		$sqlstr = "SELECT o.id_order,UNIX_TIMESTAMP(o.order_date) AS order_date_ts,o.id_status,
		UNIX_TIMESTAMP(o.order_date) AS hdate_ts,UNIX_TIMESTAMP(ol.action_date) AS payment_date_ts,'order' AS item_type,SUM(op.price*op.quantity) AS total
		 FROM orders o
		 LEFT JOIN order_products op ON o.id_order=op.id_order
         LEFT JOIN order_action_log ol ON o.id_order=ol.id_order
		   WHERE o.id_p=$id_p
		 GROUP BY o.id_order
		 ORDER BY o.id_order DESC";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Process payment
	 *
	 * @param integer $id_p
	 * @param integer $id_order
	 * @param integer $id_topic
	 * @return string			URL to redirect
	 */
	public function PaymentProcess($id_p,$id_order,$id_topic)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$ini = new Ini();
		$pub_web = $ini->Get("pub_web");
		$url = "";
		$t = new Topic($id_topic);
		include_once(SERVER_ROOT."/../classes/translator.php");
		$trm27 = new Translator($t->id_language,27,false,$t->id_style);
		$order_desc = $trm27->Translate("order") . " #" . $id_order;
		$pgw_text_ref = $ini->Get("title") . " - " . $order_desc;
		$order = $this->OrderGet($id_order,true);
		$amount = $order['total'];
		if($order['id_order']>0 && $order['id_status']=="0" && is_numeric($amount) && $amount>0)
        {
			include_once(SERVER_ROOT."/../classes/payment.php");
			$p = new Payment();
        	if($this->store_payments)
			{
				if($this->id_payment_type>0 && $this->id_account>0)
				{
					$id_use = $p->AccountUseGetById($p->account_usage_types['ecommerce'],0,$this->id_account);
					$db =& Db::globaldb();
					$today = $db->getTodayDate();
					$id_payer = $p->PayerAdd($id_p);
					$id_payment = $p->PaymentStore(0,$today,$id_payer,$this->id_account,$amount,$order_desc,"",$this->id_payment_type,1,0,0,0,$id_use,$this->currency);
					$p->SessionSet($p->account_usage_types['ecommerce'],$id_topic,$id_payment);
				}
				else 
				{
					UserError("Ecommerce module not configured for storing payments",array());
				}
			}
			switch($this->pgw)
			{
				case "paypal":
					$error_params = array('id_order'=>$id_order,'id_account'=>$this->id_account,'id_payment'=>$id_payment);
					if($id_payment>0)
					{
						$token = $p->PaymentTokenSet($id_payment);
						$account = $p->AccountGet($this->id_account);
						if($account['active'])
						{
							$aparams = $p->AccountParamsGet($this->id_account);
							if(isset($aparams['email']) && $aparams['email']!="")
							{
								include_once(SERVER_ROOT."/../classes/file.php");
								$fm = new FileManager();
								$data = array();
								$data['no_shipping'] = "1";
								$data['business'] = $aparams['email'];
								$data['item_name'] = $order_desc;
								$data['item_number'] = $id_order;
								$data['currency_code'] = $this->currency;
								$data['lc'] = $trm27->lang;
								if(isset($aparams['page_style']) && $aparams['page_style']!="")
									$data['page_style'] = $aparams['page_style']; 
								$data['return'] = "{$pub_web}/tools/paypal_return.php?id={$token}&status=T&id_topic=$id_topic";
								$data['cancel_return'] = "{$pub_web}/tools/paypal_return.php?id={$token}&status=F&id_topic=$id_topic";
								$data['rm'] = "2";
								$fm->PostData("https://www.paypal.com","cgi-bin/webscr",$data,false,60);
							}
							else
							{
								UserError("Payment failed as account {$account['name']} does not have a valid email",$error_params);
							}
						}
						else
						{
							UserError("Payment failed as account {$account['name']} is not active",$error_params);
						}					
					}
					else
					{
						UserError("Payment failed as payment record has not been created",$error_params);
					}					
					
				break;
			}
        }
		return $url;
	}
	
	/**
	 * Information about payment error
	 *
	 * @param integer $id_error
	 * @return string
	 */
	public function PaymentProcessError($id_error)
	{
		$result = "ERROR";
		
		switch($this->pgw)
		{
		}
		
		return $result;
	}

	
	/**
	 * Delete a product
	 *
	 * @param integer $id_product
	 */
	public function ProductDelete( $id_product )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "order_products" );
		$res[] = $db->query( "DELETE FROM order_products WHERE id_product=$id_product ");
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "products" );
		$res[] = $db->query( "DELETE FROM products WHERE id_product=$id_product ");
		Db::finish( $res, $db);
		$this->ProductImageDelete($id_product);
	}

	/**
	 * Retrieve product data
	 *
	 * @param integer $id_product
	 * @return array
	 */
	public function ProductGet($id_product)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.id_product,p.name,p.description,p.price,p.stock,p.active
			FROM products p
			WHERE id_product='$id_product' ";
		$db->query_single($row,$sqlstr,1);
		return $row;
	}
	
	/**
	 * Delete image associated to a product
	 *
	 * @param integer $id_product
	 */
	public function ProductImageDelete($id_product)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$fm = new FileManager;
		$fm->Delete("uploads/products/orig/{$id_product}.jpg");
		$i->RemoveWrapper("product_image",$id_product);
		$fm->Delete("pub/{$i->pub_path}products/0/{$id_product}.jpg");
		$fm->Delete("pub/{$i->pub_path}products/1/{$id_product}.jpg");
		$fm->PostUpdate();
	}

	/**
	 * Update image associated to a product
	 *
	 * @param integer 	$id_product
	 * @param array		$file		Uploaded file info
	 */
	public function ProductImageUpdate($id_product,$file)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/images.php");
		$fm = new FileManager;
		$this->ProductImageDelete($id_product);
		$origfile = "uploads/products/orig/{$id_product}.jpg";
		$fm->MoveUpload($file['temp'],$origfile);
		$i = new Images();
		$i->ConvertWrapper("product_image",$origfile,$id_product);
		$filename = $id_product . "." . $i->convert_format;
		$fm->Copy("uploads/products/0/$filename","pub/{$i->pub_path}products/0/$filename");
		$fm->Copy("uploads/products/1/$filename","pub/{$i->pub_path}products/1/$filename");
		$fm->PostUpdate();
	}

	/**
	 * Search products (paginated)
	 *
	 * @param array 	$rows	Products
	 * @param array		$params	Search parameters
	 * @param boolean 	$paged	Pagination (optional)
	 * @return integer			Num of products found
	 */
	public function ProductSearch(&$rows, $params, $paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT p.id_product,p.name,p.price,p.stock,p.active 
			FROM products p ";
		$sqlstr .= " WHERE 1=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (p.name LIKE '%{$params['name']}%' OR p.description LIKE '%{$params['name']}%') ";
		if ($params['active']=="on")
			$sqlstr .= " AND p.active=1 ";
        else
            $sqlstr .= " AND p.active=0 ";
		if ($params['stock']>0)
		{
			$sqlstr .= $params['stock']=="1"? " AND p.stock=1 ":" AND p.stock=0 ";
		}
		$sqlstr .= " ORDER BY p.name";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	/**
	 * Store product data
	 *
	 * @param integer $id_product
	 * @param string $name
	 * @param string $description
	 * @param decimal $price
	 * @param boolint $stock
	 * @param boolint $active
	 * @return integer			New product ID
	 */
	public function ProductStore($id_product,$name,$description,$price,$stock,$active)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "products" );
		if($id_product>0)
		{
			$sqlstr = "UPDATE products SET name='$name',description='$description',
				price='$price',stock='$stock',active='$active'
			  WHERE id_product='$id_product' ";
		}
		else 
		{
			$id_product = $db->nextId( "products", "id_product" );
			$sqlstr = "INSERT INTO products (id_product,name,description,price,stock,active)
			 VALUES ($id_product,'$name','$description','$price','$stock','$active')";
		}
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		return $id_product;
	}

	/**
	 * Products list
	 *
	 * @param array 	$rows			Products
	 * @param boolean 	$paged			Paginated (optional)
	 * @param boolean 	$available_only	Active only (optional)
	 * @return integer					Num of products
	 */
	public function Products(&$rows,$paged=true,$available_only=false)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.id_product,p.name,p.price,p.stock,p.active,'product' AS item_type
			FROM products p ";
		if($available_only)
			$sqlstr .= " WHERE p.active=1 ";
		$sqlstr .= " ORDER BY p.name ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	/**
	 * Products that can be added to a specific order
	 *
	 * @param integer $id_order
	 * @param array $rows
	 * @param boolean $paged
	 * @return integer
	 */
	public function ProductsAvailable($id_order,&$rows,$paged=true)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.id_product,p.name,p.price,p.stock
			FROM products p 
			LEFT JOIN order_products op ON p.id_product=op.id_product AND op.id_order=$id_order
			WHERE p.active=1 AND op.id_order IS NULL
			GROUP BY p.id_product
			ORDER BY p.name ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
    
    /**
     * Add product to shopping cart (json call)
     *
     * @param string $input_data	Product data (json encoded)
     * @return string	New shopping cart (json encoded)
     */
    public function AddToShoppingCart($input_data)
    {
        include_once(SERVER_ROOT."/../classes/session.php");
        $session = new Session();
        
        $found = false;                   
        $json_data = $input_data['data'];  
        $array = json_decode($json_data, true);  
        $qty = (int)$array['quantity'];           
        $id_product = (int)$array['id_product'];
        
        if ($session->IsVarSet('shopping_cart'))
        {
            $shopping_cart = $session->Get('shopping_cart');
            if (array_key_exists('item'.$id_product, $shopping_cart) && $qty > 0)
            {
                $shopping_cart['item'.$id_product]['quantity'] += $qty;
                $shopping_cart['item'.$id_product]['sub_total'] = sprintf("%01.2f", $shopping_cart['item'.$id_product]['quantity'] * $shopping_cart['item'.$id_product]['price']);
                $found = true;
            }
        }
        else
        {
            $session->Delete('shopping_cart_order_id');
            $shopping_cart = array();
        }

        if (!$found && $qty > 0)
        {
            $product = $this->ProductGet($id_product);
            $product['quantity'] = $qty;
            $product['sub_total'] = sprintf("%01.2f", $qty * $product['price']);
            $shopping_cart['item'.$id_product] = $product;
        }
        
        $session->Set("shopping_cart", $shopping_cart);
        
        return $this->GetShoppingCartJson($shopping_cart, $id_product);
    }
    
    /**
     * Remove product from shopping cart (json call)
     *
     * @param string	$input_data	Product data (json encoded)
     * @return string				Updated shopping cart (json encoded)
     */
    public function RemoveFromShoppingCart($input_data)
    {
        include_once(SERVER_ROOT."/../classes/session.php");
        $session = new Session();

        $json_data = $input_data['data'];
        $array = json_decode($json_data, true);
        $id_product = (int)$array['id_product'];
        
        $shopping_cart = $session->Get('shopping_cart');
        if($id_product>0)
        {
	        $this->RemoveFromShoppingCartItem($shopping_cart, $id_product);
	        $session->Set("shopping_cart", $shopping_cart);    	
        }
        
        return $this->GetShoppingCartJson($shopping_cart);
    }
    
    /**
     * Remove product from shopping cart
     *
     * @param array $shopping_cart
     * @param integer $id_product
     */
    private function RemoveFromShoppingCartItem(&$shopping_cart, $id_product)
    {
        unset($shopping_cart['item'.$id_product]);
    }
    
    /**
     * Update shopping cart quantities (json call)
     *
     * @param string	$input_data	Products data (json encoded)
     * @return string				Updated shopping cart (json encoded)
     */
    public function UpdateShoppingCart($input_data)
    {
        include_once(SERVER_ROOT."/../classes/session.php");
        $session = new Session();

        $shopping_cart = $session->Get('shopping_cart');
        $json_data = $input_data['data'];
        $array = json_decode($json_data, true);
    
        foreach ($array as $value)
        {
            $id_product = (int)$value['id_product'];
            $qty = (int)$value['quantity'];
            if($id_product>0)
            {
	            if ($qty > 0)
	            {
	                if (array_key_exists('item'.$id_product, $shopping_cart))
	                {
	                    $shopping_cart['item'.$id_product]['quantity'] = $qty;
	                    $shopping_cart['item'.$id_product]['sub_total'] = sprintf("%01.2f", $shopping_cart['item'.$id_product]['quantity'] * $shopping_cart['item'.$id_product]['price']);
	                }
	            }
	            else
	                $this->RemoveFromShoppingCartItem($shopping_cart, $id_product);
            }
        }
        $session->Set("shopping_cart", $shopping_cart);
        
        return $this->GetShoppingCartJson($shopping_cart);
    }
    
    /**
     * Retrieve shopping cart (json encoded)
     *
     * @param array	$shopping_cart	Product items in the current shopping cart
     * @param integer $id_product	Product ID to add
     * @return string				Shopping cart (json encoded)
     */
    private function GetShoppingCartJson($shopping_cart, $id_product=0)
    {
        $total = 0;
        $item_count = 0;
        $shopping_cart_items = array();
        foreach($shopping_cart as $item)
        {
            $total += $item['sub_total'];
            $item_count++;
            if ($id_product == 0 || $item['id_product'] == $id_product)
                $shopping_cart_items[] = $item;
        }
        return json_encode(array('shopping_cart_items'=>$shopping_cart_items, 'item_count'=>$item_count, 'total'=>sprintf("%01.2f", $total)));
    }
}
?>
