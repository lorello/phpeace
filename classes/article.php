<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Article management class
 * Articles are the core content type of PhPeace
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Article
{
	/**
	 * Article ID
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 * Article's headline
	 *
	 * @var string
	 */
	public $headline;
	
	/**
	 * Article topic ID
	 *
	 * @var integer
	 */
	public $id_topic;
	
	/**
	 * Article subtopic ID
	 *
	 * @var integer
	 */
	public $id_subtopic;
	
	/**
	 * ID of admin user who instereted this article
	 *
	 * @var integer
	 */
	public $id_user;
	
	/** 
	 * @var Topic */
	public $topic;
	
	/**
	 * Wether the article is visible online
	 *
	 * @var integer
	 *   1 for true, 0 for false
	 */
	public $visible;
	
	/**
	 * URL to jump directly
	 *
	 * @var string
	 */
	public $jump = "";
	
	/**
	 * Configured sizes of article boxes
	 *
	 * @var array
	 */
	public $box_sizes;
	
	/**
	 * Alignment options
	 *
	 * @var array
	 */
	public $alignment;
	
	/** 
	 * @var History */
	private $h;

	/**
	 * Set article ID
	 * Initialize local variables and classes
	 *
	 * @param integer $id
	 */
	function __construct( $id )
	{
		$this->id = $id;
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration;
		$this->box_sizes = $conf->Get("box_sizes");
		$this->alignment = array('left','center','right','justify');
		unset($conf);
	}

	/**
	 * Unset local classes
	 *
	 */
	function __destruct()
	{
		unset($this->h);
		if(isset($this->topic))
			unset($this->topic);
	}

	/**
	 * Hardcopy current article to a new one
	 *
	 * @param integer $id_topic 	ID of destination topic
	 * @param integer $id_subtopic 	ID of destination subtopic
	 * @param boolint $link_images 	Wether to link source images or hardcopy them too
	 * @param boolint $link_docs 	Wether to link source docs or hardcopy them too
	 * @return integer ID of new article
	 */
	public function ArticleCopy($id_topic,$id_subtopic,$link_images,$link_docs)
	{
		if($this->id > 0 && $id_topic>0)
		{
			$row = $this->ArticleGet();
			$db =& Db::globaldb();
			$written = $db->SqlQuote($row['written']);
			$show_date = $db->SqlQuote($row['show_date']);
			$id_user = $db->SqlQuote($row['id_user']);
			$author = $db->SqlQuote($row['author']);
			$author_notes = $db->SqlQuote($row['author_notes']);
			$source = $db->SqlQuote($row['source']);
			$original = $db->SqlQuote($row['original']);
			$show_author = $db->SqlQuote($row['show_author']);
			$halftitle = $db->SqlQuote($row['halftitle']);
			$headline = $db->SqlQuote($row['headline']);
			$subhead = $db->SqlQuote($row['subhead']);
			$headline_visible = $db->SqlQuote($row['headline_visible']);
			$content = $db->SqlQuote($row['content']);
			$is_html = $db->SqlQuote($row['is_html']);
			$notes = $db->SqlQuote($row['notes']);
			$align = $db->SqlQuote($row['align']);
			$id_language = $db->SqlQuote($row['id_language']);
			$id_visibility = $db->SqlQuote($row['id_visibility']);
			$available = $db->SqlQuote($row['available']);
			$show_source = $db->SqlQuote($row['show_source']);
			$show_latest = $db->SqlQuote($row['show_latest']);
			$id_licence = $db->SqlQuote($row['id_licence']);
			$allow_comments = $db->SqlQuote($row['allow_comments']);
			$jump_to_source = $db->SqlQuote($row['jump_to_source']);
			$id_template = $db->SqlQuote($row['id_template']);
			// keywords
			$keywords = array();
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$o->GetKeywords($this->id, $o->types['article'],$keywords,0,true);
			include_once(SERVER_ROOT."/../classes/texthelper.php");
			$th = new TextHelper();
			$keywords = $db->SqlQuote($th->ArrayMulti2StringIndex($keywords,'keyword',", "));
			// keywords_internal
			$keywords_internal = array();
			$ikeywords = $o->UseInternal($this->id,$o->types['article']);
			foreach($ikeywords as $ikeyword)
				$keywords_internal[] = array('id_keyword'=>$ikeyword['id_keyword']);
			$a = new Article(0);
			$a->ArticleStore($written, $show_date, $id_user, $author, $author_notes, $source, $original, $show_author, $id_topic, $id_subtopic,
				$halftitle, $headline, $subhead, $headline_visible, $content, $is_html, $notes, $align, $id_language,
				0, 0, $id_visibility, $keywords, $available, $show_source, $show_latest, 0,$id_licence,
				$allow_comments,$keywords_internal,$jump_to_source,$id_template,"",0);
			// copy images
			$images = $this->ImageGetAll();
			if(count($images)>0)
			{
				$aimage = $this->Image();
				include_once(SERVER_ROOT."/../classes/image.php");
				foreach($images as $image)
				{
					$im = new Image($image['id_image']);
					if($link_images)
					{
						$id_image = $im->id;
					}
					else 
					{
						$id_image = $im->ImageClone();
						if($image['id_image']==$aimage['id_image'])
						{
							$db->begin();
							$db->lock( "articles" );
							$sqlstr = "UPDATE articles SET id_image='$id_image' WHERE id_article='$a->id' ";
							$res[] = $db->query($sqlstr);
							Db::finish( $res, $db);
						}
						$a->TagReplace("Img",$image['id_image'],$id_image);
					}
					$caption = $db->SqlQuote($image['caption']);
					$align = $db->SqlQuote($image['align']);
					$size = $db->SqlQuote($image['size']);
					$zoom = $db->SqlQuote($image['zoom']);
					$im->ArticleAdd($id_image,$a->id,$caption,$align,$size,$zoom,false,false);
				}
			}
			// copy docs
			$docs = $this->DocGetAll();
			if(count($docs)>0)
			{
				include_once(SERVER_ROOT."/../classes/doc.php");
				foreach($docs as $doc)
				{
					$d = new Doc($doc['id_doc']);
					if($link_docs)
					{
						$id_doc = $doc['id_doc'];
					}
					else 
					{
						$id_doc = $d->DocClone();
					}
					$d->ArticleAdd($id_doc,$a->id,$doc['seq'],$doc['id_subtopic_form']);
				}
			}
			// copy boxes
			$boxes = $this->BoxGetAll();
			if(count($boxes)>0)
			{
				$db->begin();
				$db->lock( "articles_boxes" );
				foreach($boxes as $box)
				{
					$sqlstr = "INSERT INTO articles_boxes (id_box,id_article,popup,width,align,z_width,z_height) 
						VALUES ({$box['id_box']},$a->id,{$box['popup']},{$box['width']},{$box['align']},{$box['z_width']},{$box['z_height']})";
					$res[] = $db->query( $sqlstr);
				}
				Db::finish( $res, $db);
			}
			// copy related
			$relateds = $this->RelatedAll();
			if(count($relateds)>0)
			{
				foreach($relateds as $related)
				{
					$a->Relate($related['id_article'],false);
				}
			}
			if($id_template>0)
			{
				// copy template values
				include_once(SERVER_ROOT."/../classes/template.php");
				$te = new Template("article");
				$values = $te->ResourceValues($id_template,$this->id);
				$db->begin();
				$db->lock( "template_values" );
				foreach($values as $value)
				{
					$esc_value = $db->SqlQuote($value['value']);
					$sqlstr = "INSERT INTO template_values (id_res,id,id_template_param,value) VALUES ({$o->types['article']},$a->id,{$value['id_template_param']},'$esc_value') ";
					$res[] = $db->query( $sqlstr );
				}
				Db::finish( $res, $db);
			}
		}
		return $a->id;
	}
	
	/**
	 * Delete current article
	 * moving it to the Trash topic
	 *
	 */
	public function ArticleDelete()
	{
		$row = $this->ArticleGet();
		$this->ArticleUpdatePropagate($row,0,0,0,array(),false,"update");
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$temp_id_topic = $ini->Get('temp_id_topic');
		include_once(SERVER_ROOT."/../classes/topic.php");
		$temp_t = new Topic($temp_id_topic);
		$temp_id_subtopic = $temp_t->HasSubtopicType(0);
		$sqlstr = "UPDATE articles SET id_topic=$temp_id_topic,id_subtopic=$temp_id_subtopic,
			approved=0,id_template=0
		 	WHERE id_article=$this->id";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles" );
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		$this->UnrelateArticle( $this->id );
		include_once(SERVER_ROOT."/../modules/books.php");
		$bb = new Books();
		$bb->ArticleRemove($this->id);
		include_once(SERVER_ROOT."/../classes/translations.php");
		$tra = new Translations();
		$tra->TranslationDeleteByArticle($this->id);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['article']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['article'],$this->id);
		include_once(SERVER_ROOT."/../classes/irl.php");	
		$irl = new IRL();
		$irl->FriendlyUrlDelete($o->types['article'],$this->id,$row['id_topic']);
		$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['delete']);
	}

	/**
	 * Retrieve current article
	 *
	 * @return array
	 */
	public function ArticleGet()
	{
		$article = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_user,a.id_topic,a.headline,ah.subhead,ac.content,ac.notes,a.author,
			UNIX_TIMESTAMP(a.written) AS written_ts,a.approved,a.source,a.id_image,
			a.id_subtopic,UNIX_TIMESTAMP(a.published) AS published_ts,a.show_date,
			a.halftitle,a.headline_visible,a.is_html,a.align,a.id_visibility,a.show_author,a.original,a.id_language,
			a.available,a.show_source,a.id_article,a.show_latest,a.author_notes,a.id_licence,allow_comments,
			t.id_group AS id_topic_group,t.comments AS topic_allow_comments,t.moderate_comments,a.jump_to_source,a.id_template,
			a.written,IF(a.jump_to_source,a.source,'') AS jump, a.highlight
			FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN articles_content ac ON a.id_article=ac.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic
			WHERE a.id_article=$this->id ";
		$db->query_single($article, $sqlstr);
		$this->headline = $article['headline'];
		$this->id_topic = $article['id_topic'];
		return $article;
	}

	/**
	 * Wether article headings have been changed
	 *
	 * @param array $article Current article
	 * @param array $headings_new New headings
	 * @return boolean
	 */
	private function ArticleHeadingsChanged($article,$headings_new)
	{
		$changed = false;
		$db =& Db::globaldb();
		$headings = array($article['written_ts'],$article['show_date'],$article['id_user'],$db->SqlQuote($article['author']),$article['show_author'],$db->SqlQuote($article['halftitle']),$db->SqlQuote($article['headline']),$db->SqlQuote($article['subhead']),$article['headline_visible'],$article['id_language'],$article['id_visibility'],$article['available'],$db->SqlQuote($article['author_notes']),$article['jump_to_source'],$db->SqlQuote($article['furl']));
		$diff = array_merge(array_diff($headings,$headings_new),array_diff($headings_new,$headings));
		if(count($diff)>0)
			$changed = true;
		return $changed;
	}

	/**
	 * Load article main fields into local variables
	 * Set the visible property
	 *
	 * @return array
	 */
	public function ArticleLoad()
	{
		$article = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT headline,id_topic,id_subtopic,id_user,approved,available,id_language,jump_to_source,source FROM articles WHERE id_article=$this->id ";
		$db->query_single($article, $sqlstr);
		$this->headline = $article['headline'];
		$this->id_topic = $article['id_topic'];
		$this->id_subtopic = $article['id_subtopic'];
		$this->id_user = $article['id_user'];
		if($article['jump_to_source']=="1" && $article['jump_to_source']!="")
			$this->jump = $article['source'];
		if ($article['approved']=="1" && $article['available']=="1")
			$this->visible = 1;
		else
			$this->visible = 0;
		return $article;
	}

	/**
	 * Store article data
	 * Calcocate updates and add them to the publishing queue
	 * Track historical changes
	 * Add tasks to indexing queue
	 *
	 * @param date $written
	 * @param boolint $show_date
	 * @param integer $id_user
	 * @param string $author
	 * @param string $author_notes
	 * @param string $source
	 * @param date $original
	 * @param boolint $show_author
	 * @param integer $id_topic
	 * @param integer $id_subtopic
	 * @param string $halftitle
	 * @param string $headline
	 * @param string $subhead
	 * @param boolint $headline_visible
	 * @param string $content
	 * @param boolint $is_html
	 * @param string $notes
	 * @param integer $align
	 * @param integer $id_language
	 * @param boolint $approved
	 * @param boolint $approved_old
	 * @param integer $id_visibility
	 * @param string $keywords
	 * @param boolint $available
	 * @param boolint $show_source
	 * @param boolint $show_latest
	 * @param boolint $new_latest
	 * @param integer $id_licence
	 * @param boolint $allow_comments
	 * @param array $keywords_internal
	 * @param boolint $jump_to_source
	 * @param integer $id_template
	 */
	public function ArticleStore($written, $show_date, $id_user, $author, $author_notes, $source, $original, $show_author, $id_topic, $id_subtopic,
				$halftitle, $headline, $subhead, $headline_visible, $content, $is_html, $notes, $align, $id_language,
				$approved, $approved_old, $id_visibility, $keywords, $available, $show_source, $show_latest, $new_latest,$id_licence,
				$allow_comments,$keywords_internal,$jump_to_source,$id_template,$furl,$highlight,$from_scheduler=false)
	{
		$action = ($this->id>0)? "update":"insert";
		$new_flag = false;
		if ($id_topic > 0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			include_once(SERVER_ROOT."/../classes/modules.php");
			$t = new Topic($id_topic);
			$topic_admin = $t->AmIAdmin();
			$topics_admin = Modules::AmIAdmin(4);
			if (!$topics_admin && !$topic_admin  && $approved=="1" && !$from_scheduler)
				$approved = 0;
			if($t->row['custom_visibility']=="0")
				$id_visibility = "5";
			if($t->protected != "2")
				$available = "1";
		}
		else 
			$approved = 0;
		if ($action=="update")
		{
			$pub = $this->PublishedGet();
			$new_flag = $new_latest=="1" || (!$pub>0 && $show_latest=="1");
			if($new_latest == "1") {
			    $show_latest = "1";
			}
		}
		else
		{
			$new_flag = $show_latest=="1";
		}
		include_once(SERVER_ROOT."/../classes/formhelper.php");
		$fh = new FormHelper;
		$written_ts = $fh->CheckDateTimestamp($written, TRUE);
		$headings = array($written_ts,$show_date,$id_user,$author,$show_author,$halftitle,$headline,$subhead,$headline_visible,$id_language,$id_visibility,$available,$author_notes,$jump_to_source,$furl);
		$old_article = $this->ArticleGet();
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("articles","articles_subhead","articles_content"));
		if ($action=="insert")
		{
			$this->id = $db->nextId( "articles", "id_article" );
			$sqlstr1 = "INSERT INTO articles (id_article,id_user,halftitle,headline,author,author_notes,written,id_topic,approved,source,id_subtopic,show_date,headline_visible,is_html,align,id_visibility,show_author,original,id_language,available,show_source,show_latest,id_licence,allow_comments,jump_to_source,id_template,highlight)
			VALUES ($this->id,$id_user,'$halftitle','$headline','$author','$author_notes','$written',$id_topic,$approved,'$source',$id_subtopic,$show_date,$headline_visible,$is_html,$align,$id_visibility,$show_author,'$original',$id_language,$available,$show_source,$show_latest,$id_licence,$allow_comments,$jump_to_source,$id_template,$highlight)";
			$sqlstr2 = "INSERT INTO articles_subhead (id_article,subhead) VALUES ($this->id,'$subhead')";
			$sqlstr3 = "INSERT INTO articles_content (id_article,content,notes) VALUES ($this->id,'$content','$notes')";
		}
		else
		{
			$sqlstr1 = "UPDATE articles
			SET id_user=$id_user,halftitle='$halftitle',headline='$headline',
			source='$source',author='$author',written='$written',id_topic=$id_topic,
			approved=$approved,id_subtopic=$id_subtopic,show_date=$show_date,
			headline_visible=$headline_visible,is_html=$is_html,align=$align,id_visibility=$id_visibility,
			show_author=$show_author,original='$original',id_language=$id_language,available=$available,
			show_source=$show_source,show_latest=$show_latest,author_notes='$author_notes',
			id_licence=$id_licence,allow_comments=$allow_comments,jump_to_source=$jump_to_source,id_template=$id_template,highlight=$highlight
			WHERE id_article='$this->id'";
			$sqlstr2 = "UPDATE articles_subhead SET subhead='$subhead' WHERE id_article=$this->id";
			$sqlstr3 = "UPDATE articles_content SET content='$content',notes='$notes' WHERE id_article=$this->id";
		}
		$res1[] = $db->query( $sqlstr1 );
		$res1[] = $db->query( $sqlstr2 );
		$res1[] = $db->query( $sqlstr3 );
		Db::finish( $res1, $db);
		
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		include_once(SERVER_ROOT."/../classes/irl.php");	
		$irl = new IRL();
		$furl_old = $irl->FriendlyUrlGet($o->types['article'],$this->id);
		$irl->FriendlyUrlStore($o->types['article'],$this->id,$furl,$id_topic,$approved);
		$old_article['furl'] = $furl_old['furl'];
		$this->ArticleUpdatePropagate($old_article,$approved,$id_subtopic,$id_topic,$headings,$new_flag,$action);
		
		$o->InsertKeywords($keywords, $this->id, $o->types['article']);
		$o->InsertKeywordsArray($keywords_internal,$this->id,$o->types['article']);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($approved && $available && $t->visible && $id_subtopic>0)
		{
			$subtopic = $t->SubtopicGet($id_subtopic);
			if($subtopic['visible']<4 && $subtopic['id_type']=="2")
			{
				$t->queue->JobInsert($t->queue->types['subtopic'],$id_subtopic,"update");
			}
			if($subtopic['visible']<3)
				$s->IndexQueueAdd($o->types['article'],$this->id,$id_topic,$t->id_group,1);
			else 
				$s->ResourceRemove($o->types['article'],$this->id);
		}
		else 
			$s->ResourceRemove($o->types['article'],$this->id);
		if ($action=="insert")
		{
			$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['create']);
			if ($approved)
				$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['approve']);
		}
		else
		{
			if ($approved!=$approved_old)
			{
				if ($approved)
					$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['approve']);
				else
					$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['reject']);
			}
			else
			{
				$this->h->HistoryAdd($this->h->types['article'],$this->id,$this->h->actions['update']);
			}
		}
	}
	
	/**
	 * Update article content
	 *
	 * @param string $content
	 */
	public function ArticleContentUpdate($content)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->Lock("articles_content");
		$sqlstr = "UPDATE articles_content SET content='$content' WHERE id_article=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Article submission from portal by a public user
	 *
	 * @param string $author
	 * @param string $author_notes
	 * @param string $source
	 * @param integer $id_topic
	 * @param string $halftitle
	 * @param string $headline
	 * @param string $subhead
	 * @param string $content
	 * @param string $notes
	 * @param string $name
	 * @param string $email
	 * @param boolint $notify
	 */
	public function ArticleSubmit($author, $author_notes, $source, $id_topic, $halftitle, $headline, $subhead, $content, $notes, $name, $email,$notify)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$id_subtopic = $t->HasSubtopicType(0);
		$id_language = $t->id_language;
		$db =& Db::globaldb();
		$today = $db->GetTodayDate();
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$id_visibility = $ini->Get("default_visibility");
		$this->ArticleStore($today, 1, 0, $author, $author_notes, $source, "", 1, $id_topic,
			$id_subtopic, $halftitle, $headline, $subhead, 1, $content, 0, $notes, 0, $id_language,
			0, 0, $id_visibility, "", 1, 1, 1,0,0,0,array(),0,0,"",0);
		include_once(SERVER_ROOT."/../classes/contribution.php");
		$co = new Contribution("article");
		$co->Insert($this->id,$name,$email,$notify);
	}

	/**
	 * Insert into update queue all actions related to article deletion
	 *
	 */
	private function ArticleDeletePropagate()
	{
		$this->topic->queue->JobInsert($this->topic->queue->types['article'],$this->id,"delete");
		$boxes = $this->BoxGetAll(true);
		foreach($boxes as $box)
			$this->topic->queue->JobInsert($this->topic->queue->types['article_box'],$box['id_box'],"delete");
		include_once(SERVER_ROOT."/../classes/irl.php");	
		$irl = new IRL();
		$irl->FriendlyUrlDelete(3,$this->id,$this->topic->id);
	}
	
	/**
	 * Propagate article changes to topic queue
	 *
	 * @param boolean $is_new
	 * @param boolean $subtopic_changed
	 */
	private function ArticleTopicPropagate($is_new=FALSE,$subtopic_changed=false)
	{
		$hail = $this->topic->HasArticleInLatest($this->id,$this->topic->row['articles_per_page']);
		$latest_subtopic = $this->topic->HasSubtopicType($this->topic->subtopic_types['latest']);
		if ($latest_subtopic>0 && ($hail || $is_new || $subtopic_changed))
			$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$latest_subtopic,"update");
		if ($hail || $is_new)
			$this->topic->queue->JobInsert($this->topic->queue->types['latest'],$this->topic->id,"update");
		$this->topic->queue->JobInsert($this->topic->queue->types['topic_home'],$this->topic->id,"");
	}

	/**
	 * Evaluate impact of article changes and propagate changes to publishing queue
	 *
	 * @param integer $old_article
	 * @param boolint $approved_new
	 * @param integer $id_subtopic_new
	 * @param integer $id_topic_new
	 * @param array $headings_new
	 * @param boolean $new_flag
	 * @param string $action
	 */
	private function ArticleUpdatePropagate($old_article,$approved_new,$id_subtopic_new,$id_topic_new,$headings_new,$new_flag,$action)
	{
		if ($action=="update")
		{
			if($id_topic_new!=$old_article['id_topic'] && $old_article['id_topic']>0)
			{
				$this->TopicLoad($old_article['id_topic']);
				$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$old_article['id_subtopic'],"update");
				$this->ArticleDeletePropagate();
				$this->ArticleTopicPropagate($new_flag);
			}
			$this->TopicLoad($id_topic_new);
			if ($approved_new=="1")
			{
				$this->topic->queue->JobInsert($this->topic->queue->types['article'],$this->id,"update",$new_flag?"new":"");
				if($id_subtopic_new!=$old_article['id_subtopic'])
					$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$old_article['id_subtopic'],"update");
				$headings_changed = $this->ArticleHeadingsChanged($old_article,$headings_new);
				if($new_flag || $headings_changed || $id_subtopic_new!=$old_article['id_subtopic'] || $old_article['approved']!="1")
					$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$id_subtopic_new,"update");
				$this->ArticleTopicPropagate($new_flag || $id_subtopic_new!=$old_article['id_subtopic']);
			}
			elseif($old_article['approved']=="1")
			{
				$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$old_article['id_subtopic'],"update");
				$this->topic->HomepageArticleRemove( $this->id );
				$this->UnrelateArticle( $this->id );
				$this->ArticleDeletePropagate();
				$this->ArticleTopicPropagate($new_flag);
			}
		}
		else // insert
		{
			$this->TopicLoad($id_topic_new);
			if($approved_new=="1" && $id_subtopic_new>0)
			{
				$this->topic->queue->JobInsert($this->topic->queue->types['article'],$this->id,"update",$new_flag?"new":"");
				$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$id_subtopic_new,"update");
				$this->ArticleTopicPropagate($new_flag);
			}
		}
	}

	/**
	 * Add article update to publishing queue
	 * because of changes in its related content
	 *
	 */
	private function ArticleUpdateSet()
	{
		$this->ArticleLoad();
		if ($this->visible=="1")
		{
			$this->TopicLoad();
			$this->topic->queue->JobInsert($this->topic->queue->types['article'],$this->id,"update");
			$subtopic = $this->topic->SubtopicGet($this->id_subtopic);
			$subtopic_types = $this->topic->subtopic_types;
			if($subtopic['id_type']==$subtopic_types['article'] && $subtopic['visible']!="4")
			{
				$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$this->id_subtopic,"update");
			}
		}
	}
	
	/**
	 * All books associated to this article
	 *
	 * @return array
	 */
	public function Books()
	{
		$rows = array();
		if($this->id > 0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,b.p_year,b.p_month,b.summary,b.catalog,b.price,
			b.cover_format,'book' AS item_type,b.description,b.id_publisher,b.id_category
			FROM books b
			LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
			WHERE b.id_article='$this->id' AND b.approved=1
			ORDER BY b.p_year DESC, b.p_month DESC, b.id_book DESC";
			$db->QueryExe($rows, $sqlstr);
		}
		return $rows;
	}

	/**
	 * De-associate a box from current article
	 *
	 * @param integer $id_box
	 */
	public function BoxDelete( $id_box )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_boxes" );
		$res[] = $db->query( "DELETE FROM articles_boxes WHERE id_box=$id_box AND id_article=" . $this->id );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet();
		if($this->visible)
			$this->topic->queue->JobInsert($this->topic->queue->types['article_box'],$id_box,"delete");
		$this->TagRemove("Box",$id_box);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_box,$o->types['box']);
	}

	/**
	 * Retrieve box content
	 *
	 * @param integer $id_box
	 * @return array
	 */
	public function BoxGet($id_box)
	{
		$box = array();
		$db =& Db::globaldb();
		if($this->id > 0 )
			$sqlstr = "SELECT b.id_box,b.title,b.content,b.is_html,ab.popup,ab.width,ab.align,b.show_title,b.notes,b.id_type,
				ab.z_width,ab.z_height 
				FROM boxes b 
				INNER JOIN articles_boxes ab ON b.id_box=ab.id_box
				WHERE b.id_box='$id_box' AND ab.id_article='{$this->id}' ";
		else 
			$sqlstr = "SELECT b.id_box,b.title,b.content,b.is_html,b.show_title,b.notes,b.id_type 
				FROM boxes b 
				WHERE b.id_box='$id_box' ";
		$db->query_single($box, $sqlstr);
		return $box;
	}

	/**
	 * Retrieve all boxes associated to this article
	 *
	 * @param boolean $popup_only Pop-up boxes only
	 * @return array
	 */
	public function BoxGetAll($popup_only=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT b.id_box,b.title,ab.popup,b.content,b.is_html,ab.width,ab.align,b.show_title,ab.id_article,b.notes,b.id_type,ab.z_width,ab.z_height
		 FROM boxes b
		 INNER JOIN articles_boxes ab ON b.id_box=ab.id_box
		 WHERE ab.id_article='$this->id' ";
		if($popup_only)
		{
			$sqlstr .= " AND ab.popup=1 ";
		}
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Insert a new box and associate it to an article
	 *
	 * @param string 	$title
	 * @param string	$content
	 * @param boolint	$is_html			Is content HTML (avoid parsing)
	 * @param boolint	$popup				Box opens in a new window
	 * @param integer	$width				Box width (when embedded in article content)
	 * @param integer	$align				Text alignment
	 * @param boolint	$show_title
	 * @param integer	$id_article
	 * @param string	$notes
	 * @param integer	$id_type			Box type (for CSS formatting)
	 * @param integer	$keywords
	 * @param array		$keywords_internal
	 * @param integer 	$z_width			Width when zoomed in
	 * @param integer 	$z_height			Height when zoomed in
	 * @return integer						Box ID
	 */
	public function BoxInsert( $title, $content, $is_html, $popup, $width, $align, $show_title, $id_article,$notes,$id_type,$keywords,$keywords_internal,$z_width,$z_height )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "boxes" );
		$id_box = $db->nextId( "boxes", "id_box" );
		$sqlstr = "INSERT INTO boxes (id_box,title,content,is_html,show_title,notes,id_type)
			VALUES ($id_box,'$title','$content',$is_html,$show_title,'$notes',$id_type)";
		$res[] = $db->query( $sqlstr);
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "articles_boxes" );
		$sqlstr = "INSERT INTO articles_boxes (id_box,id_article,popup,width,align,z_width,z_height) VALUES ($id_box,$id_article,$popup,$width,$align,$z_width,$z_height)";
		$res[] = $db->query( $sqlstr);
		Db::finish( $res, $db);
		if ($popup=="1" && $this->id_topic>0)
			$this->BoxUpdateSet($id_box);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_box, $o->types['box']);
		$o->InsertKeywordsArray($keywords_internal,$id_box,$o->types['box']);
		return $id_box;
	}

	/**
	 * Update box content
	 *
	 * @param integer $id_box
	 * @param string 	$title
	 * @param string	$content
	 * @param boolint	$is_html			Is content HTML (avoid parsing)
	 * @param boolint	$popup				Box opens in a new window
	 * @param integer	$width				Box width (when embedded in article content)
	 * @param integer	$align				Text alignment
	 * @param boolint	$show_title
	 * @param string	$notes
	 * @param integer	$id_type			Box type (for CSS formatting)
	 * @param integer	$keywords
	 * @param array		$keywords_internal
	 * @param integer 	$z_width			Width when zoomed in
	 * @param integer 	$z_height			Height when zoomed in
	 */
	public function BoxUpdate($id_box, $title, $content, $is_html, $popup, $width, $align, $show_title,$notes,$id_type, $keywords, $keywords_internal,$z_width,$z_height)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "boxes" );
		$res[] = $db->query( "UPDATE boxes SET title='$title',content='$content',is_html=$is_html,show_title=$show_title,notes='$notes',id_type='$id_type'
			WHERE id_box=$id_box" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "articles_boxes" );
		$res[] = $db->query( "UPDATE articles_boxes SET popup=$popup,width=$width,align=$align,z_width=$z_width,z_height=$z_height WHERE id_box=$id_box AND id_article=" . $this->id );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet();
		if ($popup=="1" && $this->id_topic>0)
			$this->BoxUpdateSet($id_box);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_box, $o->types['box']);
		$o->InsertKeywordsArray($keywords_internal,$id_box,$o->types['box']);
	}

	/**
	 * Add box to publishing uque
	 *
	 * @param integer $id_box
	 */
	private function BoxUpdateSet($id_box)
	{
		$this->TopicLoad();
		$this->topic->queue->JobInsert($this->topic->queue->types['article_box'],$id_box,"update");
	}

	/**
	 * Retrieve all documents attached to current article
	 *
	 * @return array
	 */
	public function DocGetAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT da.id_doc,da.id_article,d.title,d.description,d.filename,d.id_language,d.author,d.source,d.format,d.id_licence,da.seq,da.id_subtopic_form
			FROM docs_articles da
			INNER JOIN docs d ON da.id_doc=d.id_doc
			WHERE da.id_article=$this->id 
			ORDER BY da.seq ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve document sequence and optional form for current article
	 *
	 * @return array
	 */
	public function DocInfo($id_doc)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT seq,id_subtopic_form FROM docs_articles WHERE id_doc='$id_doc' AND id_article='$this->id' ";
		$row = array();
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Reorganise sequence of documents attached to current article
	 *
	 */
	public function DocArticleReshuffle()
	{
		$docs = $this->DocGetAll();
		$counter = 1;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_articles" );
		foreach($docs as $doc)
		{
			$res[] = $db->query( "UPDATE docs_articles set seq=$counter WHERE id_doc='{$doc['id_doc']}' AND id_article=$this->id");
			$counter ++;
		}
		Db::finish( $res, $db);
	}

	/**
	 * Check if image is linked to current article
	 *
	 * @param integer $id_image
	 * @return boolean 	True if article has this image
	 */
	public function HasImage( $id_image )
	{
		$image = array();
		$db =& Db::globaldb();
		$db->query_single($image, "SELECT COUNT(id_image) AS counter FROM images_articles WHERE id_image=$id_image AND id_article=$this->id");
		return $image['counter']>0;
	}

	/**
	 * Check if document is attached to current article
	 *
	 * @param integer $id_doc
	 * @return boolean 	True if article has this attachment
	 */
	public function HasDoc( $id_doc )
	{
		$doc = array();
		$db =& Db::globaldb();
		$db->query_single($doc, "SELECT COUNT(id_doc) AS counter FROM docs_articles WHERE id_doc=$id_doc AND id_article=$this->id");
		return $doc['counter']>0;
	}

	/**
	 * Retrieve associated image for current article
	 *
	 * @return array
	 */
	public function Image()
	{
		$image = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_image,a.image_size AS size,a.image_align AS align,a.id_article,
		a.image_width AS width,a.image_height AS height,i.format,i.caption,i.source,i.author
		FROM articles a
		INNER JOIN images i ON a.id_image=i.id_image
		WHERE a.id_article='$this->id' ";
		$db->query_single($image, $sqlstr);
		return $image;
	}

	/**
	 * Associate image to current article
	 *
	 * @param integer $id_image
	 * @param integer $size			Index of images sizes array
	 * @param integer $align		Image alignment (0=right, 1=left, 2=none)
	 * @param boolean $update		Add article container to publishing queue
	 */
	public function ImageAssociate($id_image,$size,$align,$update=true)
	{
		if($id_image>0)
		{
			include_once(SERVER_ROOT."/../classes/image.php");
			$im = new Image($id_image);
			$sizes = $im->ImageSize($size,$id_image);
		}
		else
		{
			$sizes = array('width'=>0,'height'=>0);
			$size = 0;
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles" );
		$sqlstr = "UPDATE articles SET id_image='$id_image',image_size='$size',image_align='$align',
			image_width='{$sizes['width']}',image_height='{$sizes['height']}' WHERE id_article='$this->id' ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		if($update)
		{
			$this->ArticleLoad();
			if ($this->visible && $this->id_subtopic>0)
			{
				$this->TopicLoad();
				$this->topic->queue->JobInsert($this->topic->queue->types['subtopic'],$this->id_subtopic,"update");
			}
		}
	}
	
	/**
	 * Retrieve image data
	 *
	 * @param integer $id_image
	 * @return array
	 */
	public function ImageGet( $id_image )
	{
		$image = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT i.id_image,i.source,i.author,i.id_licence,i.format,i.caption,i.width,i.height,ia.align,ia.size,ia.zoom,ia.caption AS caption_art
		FROM images i
		LEFT JOIN images_articles ia ON i.id_image=ia.id_image AND ia.id_article='$this->id' 
		WHERE i.id_image='$id_image' ";
		$db->query_single($image, $sqlstr);
		return $image;
	}

	/**
	 * Retrieve all images linked to this article
	 *
	 * @return array
	 */
	public function ImageGetAll()
	{
		$rows = array();
		if ($this->id >0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT ia.id_image,ia.caption,ia.align,ia.size,ia.zoom,i.source,i.format,ia.width,ia.height,i.author,
				i.width AS orig_width,i.height AS orig_height,i.id_licence
				FROM images_articles ia
				INNER JOIN images i ON ia.id_image=i.id_image
				WHERE ia.id_article='$this->id' ORDER BY ia.id_image";
			$db->QueryExe($rows, $sqlstr);
		}
		return $rows;
	}

	/**
	 * Deassociate image from article
	 * Remove image tag from article content
	 *
	 * @param string $id_image
	 */
	public function ImageRemove($id_image)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles" );
		$sqlstr = "UPDATE articles SET id_image=0,image_width=0,image_height=0,image_size=0,image_align=1 WHERE id_article='$this->id' AND id_image='$id_image' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->TagRemove("Img",$id_image);
	}
	
	/**
	 * Retrieve timestamp of last update of current article
	 *
	 * @return timestamp
	 */
	public function LastUpdate()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT UNIX_TIMESTAMP(MAX(time)) AS last_update_ts FROM history WHERE id_type=5 AND id='$this->id' ");
		return $row['last_update_ts'];
	}

	/**
	 * Retrieve timestamp of when current article was published for the first time
	 *
	 * @return timestamp
	 */
	private function PublishedGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT UNIX_TIMESTAMP(published) AS published_ts FROM articles WHERE id_article=$this->id");
		return $row['published_ts'];
	}

	/**
	 * Set timestamp for first time publishing of current article
	 *
	 */
	public function PublishedSet()
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$db->begin();
		$db->lock( "articles" );
		$res[] = $db->query( "UPDATE articles SET published='$today_time' WHERE id_article=$this->id" );
		Db::finish( $res, $db);
	}

	/**
	 * Associate article to current article
	 *
	 * @param integer $id_related
	 * @param boolean $with_content
	 */
	public function Relate( $id_related, $with_content)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_related" );
		$res[] = $db->query( "INSERT INTO articles_related (id_article1,id_article2,with_content) 
			VALUES ('$this->id','$id_related','$with_content')" );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet();
	}

	/**
	 * Remove a tag from current article content
	 *
	 * @param string $type
	 * @param integer $id
	 */
	private function TagRemove($type,$id)
	{
		$marker = "[[" . $type . $id . "]]";
		$article = array();
		$sqlstr = "SELECT content,notes FROM articles_content WHERE id_article=$this->id ";
		$db =& Db::globaldb();
		$db->query_single($article, $sqlstr);
		$content = $db->SqlQuote(str_replace($marker,"",$article['content']));
		$notes = $db->SqlQuote(str_replace($marker,"",$article['notes']));
		$db->begin();
		$db->lock( "articles_content" );
		$sqlstr = "UPDATE articles_content SET content='$content',notes='$notes' WHERE id_article='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Replace ID of a tag in current article content
	 *
	 * @param string 	$type	Tag type
	 * @param integer 	$id		Old ID
	 * @param integer	$id_new	New ID
	 */
	public function TagReplace($type,$id,$id_new)
	{
		$marker = "[[" . $type . $id . "]]";
		$marker_new = "[[" . $type . $id_new . "]]";
		$article = array();
		$sqlstr = "SELECT content,notes FROM articles_content WHERE id_article=$this->id ";
		$db =& Db::globaldb();
		$db->query_single($article, $sqlstr);
		$content = $db->SqlQuote(str_replace($marker,$marker_new,$article['content']));
		$notes = $db->SqlQuote(str_replace($marker,$marker_new,$article['notes']));
		$db->begin();
		$db->lock( "articles_content" );
		$sqlstr = "UPDATE articles_content SET content='$content',notes='$notes' WHERE id_article='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Delink article from current article
	 *
	 * @param integer $id_related	Article to be delinked
	 */
	public function Unrelate( $id_related )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_related" );
		$res[] = $db->query( "DELETE FROM articles_related WHERE id_article1='$this->id' AND id_article2='$id_related'" );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet();
	}

	/**
	 * Remove article from all relationships
	 *
	 * @param integer $id_article
	 */
	private function UnrelateArticle( $id_article )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_related" );
		$res[] = $db->query( "DELETE FROM articles_related WHERE id_article1=$id_article OR id_article2=$id_article" );
		Db::finish( $res, $db);
	}

	/**
	 * Articles that can be associated to the current article, paginated
	 * (excluding those already associated and the article itself)
	 * 
	 * @param integer	$id_topic	Optional topic to filter articles from 
	 * @param array		$rows		Articles available
	 * @return integer				Number of articles
	 */
	public function Related( $id_topic, &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		if ($id_topic>0)
			$cond = "AND t.id_topic=$id_topic ";
		$sqlstr = "SELECT a.id_article,halftitle,headline,subhead,UNIX_TIMESTAMP(written) AS written_ts,
			IF(show_author=1,IF(a.author<>'',a.author,u.name),'-') AS author,t.name AS topic_name
			FROM articles a
			INNER JOIN articles_subhead USING(id_article)
			LEFT JOIN articles_related ar ON a.id_article=ar.id_article2 AND id_article1='$this->id'
			INNER JOIN users u ON a.id_user=u.id_user
			INNER JOIN topics t ON a.id_topic=t.id_topic
			WHERE approved=1 AND a.id_article<>'$this->id' AND id_article2 IS NULL
			$cond ORDER BY a.written DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * All approved articles related to the current one
	 * 
	 * @return array
	 */
	public function RelatedAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ar.id_article2 AS id_article,ar.with_content,headline,subhead,halftitle,UNIX_TIMESTAMP(written) AS written_ts,available,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,show_author,show_date,a.id_user,author,author_notes,
			'article' AS item_type,a.id_topic,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
			FROM articles_related ar
			INNER JOIN articles a ON ar.id_article2=a.id_article
			INNER JOIN articles_subhead ah ON ar.id_article2=ah.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic
			WHERE ar.id_article1='$this->id' AND a.approved=1 
			ORDER BY a.written DESC ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

				
	/**
	 * Get all articles that have been related to the current one
	 * 
	 * @return array
	 */
	public function RelatedInverseAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_article1,headline,topics.name AS topic_name FROM articles_related
			INNER JOIN articles ON articles_related.id_article1=articles.id_article
			INNER JOIN topics ON articles.id_topic=topics.id_topic
			WHERE id_article2='$this->id' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Set the show_latest flag for the current article, to mark that it can be shown 
	 * latest news and feeds
	 */
	public function ShowLatestSet()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles" );
		$res[] = $db->query( "UPDATE articles SET show_latest=1 WHERE id_article='$this->id' " );
		Db::finish( $res, $db);
	}

	/**
	 * Update values of template parameters for current article
	 *
	 * @param array $unescaped_template_params
	 */
	public function TemplateParamsUpdate($unescaped_template_params)
	{
		include_once(SERVER_ROOT."/../classes/template.php");
		$te = new Template("article");
		$te->ResourceUpdate($this->id,$unescaped_template_params);
	}
	
	/**
	 * Retrieve values of template parameters for current article
	 *
	 * @param integer $id_template
	 * @param boolean $public_only
	 * @param boolean $values_only
	 * @return array
	 */
	public function TemplateValues($id_template,$public_only=false,$values_only=true)
	{
		$values = array();
		if($id_template>0)
		{
			include_once(SERVER_ROOT."/../classes/template.php");
			$te = new Template("article");
			$rows = $te->ResourceValues($id_template,$this->id,$public_only);
			if($values_only)
			{
				foreach($rows as $row)
					$values[$row['id_template_param']] = $row['value'];
			}
			else 
				$values = $rows;
		}
		return $values;
	}
	
	/**
	 * Set private topic for queue management
	 *
	 * @param integer $id_topic
	 */
	private function TopicLoad($id_topic=0)
	{
		if($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->topic = new Topic($id_topic);
		}
		elseif ($this->id_topic>0 && !(isset($this->topic)))
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->topic = new Topic($this->id_topic);
		}
	}
	
	/**
	 * Admin user who translated current article
	 *
	 * @return array
	 */
	public function Translator()
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_translation,t.id_language,t.id_article,t.id_language_trad,t.comments,t.id_user,u.name AS user_name,t.translator,
			t.id_rev,t.id_ad,u2.name AS rev_name,u3.name AS ad_name
			FROM translations t
			LEFT JOIN users u ON t.id_user=u.id_user
			LEFT JOIN users u2 ON t.id_rev=u2.id_user
			LEFT JOIN users u3 ON t.id_ad=u3.id_user
			WHERE t.id_article_trad='$this->id' AND t.completed=1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Translations associated to this article
	 *
	 * @param 	array 	$rows	Translations (paginated)
	 * @return 	integer			Num of translations
	 */
	public function Translations(&$rows)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_translation,t.id_language,t.id_article_trad,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language_trad,t.notes
			FROM translations t
			WHERE t.id_article='$this->id' AND t.completed=1 ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Approved translations of current article
	 *
	 * @return array
	 */
	public function TranslationsPub()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT tra.id_translation,tra.id_language,tra.id_article_trad,UNIX_TIMESTAMP(tra.start_date) AS start_date_ts,tra.id_language_trad,
			a.id_article,a.halftitle,a.headline,ah.subhead,UNIX_TIMESTAMP(written) AS written_ts,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,a.id_topic,a.available,a.show_author,a.show_date,
			a.id_user,a.author
			FROM translations tra
			INNER JOIN articles a ON tra.id_article_trad=a.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE a.approved=1 AND a.published>0 AND s.visible<3 AND tra.id_article='$this->id' AND tra.completed=1 
			ORDER BY a.published DESC,a.id_article DESC ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
}
?>
