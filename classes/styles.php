<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Styles
{
	public function BoxesTypeDelete($id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_boxes_types" );
		$res[] = $db->query( "DELETE FROM articles_boxes_types WHERE id_type=$id_type" );
		Db::finish( $res, $db);
	}
	
	public function BoxesTypeGet($id_type)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name,id_style FROM articles_boxes_types WHERE id_type=$id_type";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function BoxesTypeUse($id_type)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT COUNT(id_box) AS counter FROM boxes WHERE id_type=$id_type";
		$db->query_single($row,$sqlstr);
		return (int)$row['counter'];
	}
	
	public function BoxesTypes(&$rows, $id_style=0)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT b.id_type,b.name,s.name AS style_name 
		FROM articles_boxes_types b
		LEFT JOIN styles s USING(id_style) "
		. (($id_style>0)? " WHERE b.id_style='$id_style'":"") .
		" ORDER BY b.id_type ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function BoxesTypeStore($id_type,$id_style,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "articles_boxes_types" );
		if ($id_type>0)
		{
			$sqlstr = "UPDATE articles_boxes_types SET name='$name'	WHERE id_type=$id_type";
		}
		else
		{
			$id_type = $db->nextId( "articles_boxes_types", "id_type" );
			$sqlstr = "INSERT INTO articles_boxes_types (id_type,name,id_style) VALUES ($id_type,'$name','$id_style')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_type;
	}
	
	public function CssDelete($id_style)
	{
		include_once(SERVER_ROOT."/../classes/css.php");
		$types = array("specific","module","ext","custom");
		foreach($types as $type)
		{
			$csm = new CssManager($type);
			$csm->CssDeleteStyle($id_style);
		}
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirDelete("pub/css/$id_style");
		$fm->PostUpdate();
	}

	public function CssInitModule($id_module)
	{
		include_once(SERVER_ROOT."/../classes/css.php");
		$csm = new CssManager("module");
		$csm->id_pagetype = $id_module;
		$css = $csm->CssGetByType(0);
		if(!$css['id_css']>0)
		{
			$css->CssInsert(0,0,"",CSS_EMPTY);
			$styles = $this->StylesAll();
			foreach($styles as $style)
			{
				$css->CssInsert(0,$style['id_style'],"",CSS_EMPTY);
			}
		}
	}
	
	private function CssInitStyle($id_style)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		include_once(SERVER_ROOT."/../classes/css.php");
		$csm = new CssManager();
		$fm->DirCreate("{$csm->path}/{$id_style}");
		include_once(SERVER_ROOT."/../classes/pagetypes.php");			
		$pt = new PageTypes();
		foreach($pt->types as $id)
		{
			$csm->id_pagetype = $id;
			$csm->CssInsert(0,$id_style,"",CSS_EMPTY);
		}
		$csm = new CssManager("ext");
		$csm->StyleAdd($id_style);
		$csm = new CssManager("module");
		$csm->StyleAdd($id_style);
		$fm->PostUpdate();
	}
	
	public function CssReset($id_style)
	{
		include_once(SERVER_ROOT."/../classes/css.php");
		$css_types = array("specific","custom","ext","module");
		if($id_style==0)
			$css_types[] = "global";
		foreach($css_types as $css_type)
		{
			$csm = new CssManager($css_type);
			foreach($csm->CssAllCss($id_style) as $css)
			{
				$csm->id_pagetype = $css['id_type'];
				if(!strlen($css['css'])>0)
					$csm->CssUpdate($css['id_css'],$id_style,CSS_EMPTY);
			}
		}
	}
	
	public function GraphicsAll($id_style,$style_only=true,$exclude_lookup=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_graphic,name,width,height,format,id_style FROM graphics WHERE id_graphic>0 ";
		if($exclude_lookup)
			$sqlstr .= " AND exclude_lookup=0 ";
		if($style_only)
			$sqlstr .= " AND id_style=$id_style";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function InputRight($id_style)
	{
		// TODO Should be done with one direct query only
		$input_right = 0;
		$topics = $this->Topics($id_style);
		if (count($topics)>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			foreach($topics as $topic)
			{
				$t = new Topic($topic['id_topic']);
				if ($t->edit_layout && $t->AmIAdmin())
					$input_right = 1;
			}
		}
		return $input_right;
	}
	
	public function Rebuild($id_style)
	{
		include_once(SERVER_ROOT."/../classes/xsl.php");
		include_once(SERVER_ROOT."/../classes/css.php");
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirClean("xsl/$id_style");
		$fm->DirClean("pub/css/$id_style");
		$xsl_types = array("specific","custom","ext","module");
		if($id_style==0)
			$xsl_types[] = "global";
		foreach($xsl_types as $xsl_type)
		{
			$xslm = new XslManager($xsl_type);
			foreach($xslm->XslAllXsl($id_style) as $xsl)
			{
				$xslm->id_pagetype = $xsl['id_type'];
				$xslm->XslWrite($id_style,$xsl['id_xsl'],$xsl['xsl']);
			}
			$csm = new CssManager($xsl_type);
			foreach($csm->CssAllCss($id_style) as $css)
			{
				$csm->id_pagetype = $css['id_type'];
				$csm->CssWrite($id_style,$css['id_css'],$css['css']);
			}
		}
		$fm->PostUpdate();
	}
	
	public function StyleDelete($id_style,$ui=false)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "styles" );
		$res[] = $db->query( "DELETE FROM styles WHERE id_style=$id_style" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "page_features" );
		$res[] = $db->query( "DELETE FROM page_features WHERE id_style=$id_style" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "styles_labels" );
		$res[] = $db->query( "DELETE FROM styles_labels WHERE id_style=$id_style" );
		Db::finish( $res, $db);
		if(!$ui) {
			$this->XslDelete($id_style);
			$this->CssDelete($id_style);
			include_once(SERVER_ROOT."/../classes/graphic.php");
			$graphics = $this->GraphicsAll($id_style,true,false);
			foreach($graphics as $graphic)
			{
				$g = new Graphic($graphic['id_graphic']);
				$g->GraphicDelete();
			}
		}
	}

	public function StyleGet($id_style)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_style,name,description,id_parent FROM styles WHERE id_style=$id_style";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function StyleInsert( $name,$description,$id_parent,$id_style_copy,$ui=false )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "styles" );
		$id_style = $db->nextId( "styles", "id_style" );
		$sqlstr = "INSERT INTO styles (id_style,name,description,id_parent)
			VALUES ($id_style,'$name','$description','$id_parent')";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		if(!$ui) {
			$this->XslInitStyle($id_style,$id_parent);
			$this->CssInitStyle($id_style);
			if($id_style_copy>0)
			{
				include_once(SERVER_ROOT."/../classes/xsl.php");
				include_once(SERVER_ROOT."/../classes/css.php");
				$types = array("specific","module","ext","custom");
				foreach($types as $type)
				{
					$xslm = new XslManager($type);
					$xsls = $xslm->XslAllXsl($id_style_copy);
					foreach($xsls as $xsl)
					{
						if($xsl['xsl']!="")
						{
							if($xslm->type=="module" || $xslm->type=="specific")
							{
								$xslm->id_pagetype = $xsl['id_type'];
								$row = $xslm->XslGetByType($id_style);
								$id_xsl = $row['id_xsl'];
							}
							else
								$id_xsl = $xsl['id_xsl'];
							$xslm->XslUpdate($id_xsl,$id_style,$xsl['xsl']);
						}
					}
					$csm = new CssManager($type);
					$csss = $csm->CssAllCss($id_style_copy);
					foreach($csss as $css)
					{
						if($css['css']!="")
						{
							if($csm->type=="module" || $csm->type=="specific")
							{
								$csm->id_pagetype = $css['id_type'];
								$row = $csm->CssGetByType($id_style);
								$id_css = $row['id_css'];
							}
							else
								$id_css = $css['id_css'];
							$csm->CssUpdate($id_css,$id_style,$css['css']);
						}
					}
				}
				include_once(SERVER_ROOT."/../classes/features.php");
				$ft = new Features();
				$features = array();
				$ft->PageFeaturesStyle($features,$id_style_copy,-1,false);
				$db->begin();
				$db->lock( "page_features" );
				foreach($features as $feature)
				{
					$id_feature = (int)$feature['id_feature'];
					$id_type = (int)$feature['id_type'];
					$id_module = (int)$feature['id_module'];
					if($id_feature>0)
					{
						$sqlstr = "INSERT INTO page_features (id_feature,id_style,id_type,id_module)
						VALUES ($id_feature,$id_style,$id_type,$id_module)";
						$res[] = $db->query( $sqlstr );
					}
				}
				Db::finish( $res, $db);
				$labels = array();
				$db->QueryExe($labels, "SELECT id_module,labels,id_language FROM styles_labels WHERE id_style='$id_style_copy' ");
				if(count($labels)>0)
				{
				$db->begin();
					$db->lock( "styles_labels" );
				foreach($labels as $label)
							{
							$labels_text = $db->SqlQuote($label['labels']);
							$sqlstr = "INSERT INTO styles_labels (id_style,id_module,labels,id_language)
							VALUES ($id_style,{$label['id_module']},'$labels_text',{$label['id_language']})";
							$res[] = $db->query( $sqlstr );
					}
					Db::finish( $res, $db);
				}
				// TODO custom_labels
			}
		}
		return $id_style;
	}

	public function StyleUpdate( $id_style,$name,$description )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "styles" );
		$sqlstr = "UPDATE styles SET name='$name',description='$description'
			WHERE id_style=$id_style";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function StylesP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_style,name,description FROM styles ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function StylesAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_style,name,id_parent FROM styles ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function StylesUser($id_user,$is_admin)
	{
		$styles = array();
		if($is_admin)
			$styles = $this->StylesAll();
		elseif($id_user>0) 
		{
			$db =& Db::globaldb();
			$styles = array();
			$sqlstr = "SELECT s.id_style,s.name 
				FROM styles s 
				INNER JOIN topics t ON s.id_style=t.id_style AND t.edit_layout=1 
				INNER JOIN topic_users tu ON t.id_topic=tu.id_topic AND tu.is_admin=1 AND tu.id_user=$id_user 
				GROUP BY s.id_style ORDER BY s.name ";
			$db->QueryExe($styles, $sqlstr);
		}
		return $styles;
	}
	
	public function Topics($id_style)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT name,id_topic FROM topics WHERE id_style=$id_style ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function XslDelete($id_style)
	{
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$types = array("specific","module","ext","custom");
		foreach($types as $type)
		{
			$xslm = new XslManager($type);
			$xslm->XslDeleteStyle($id_style);
		}
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirDelete("xsl/$id_style");
		$fm->PostUpdate();
	}

	private function XslInitStyle($id_style,$id_parent)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$xslm = new XslManager();
		$fm->DirCreate(XSL_PATH . "/" . $id_style);
		include_once(SERVER_ROOT."/../classes/pagetypes.php");			
		$pt = new PageTypes();
		foreach($pt->types as $type=>$id)
		{
			$xsl = $xslm->TypeInit($type,$id_parent);
			$xslm->id_pagetype = $id;
			$xslm->XslInsert(0,$id_style,"",$xsl);
		}
		$xslm = new XslManager("ext");
		$xslm->StyleAdd($id_style,$id_parent);
		$xslm = new XslManager("module");
		$xslm->StyleAdd($id_style,$id_parent);
		$fm->PostUpdate();
	}
	
	private function XslUpdate($id_style)
	{
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$xsl_types = array("specific","custom","ext","module");
		if($id_style==0)
			$xsl_types[] = "global";
		include_once(SERVER_ROOT."/../classes/pagetypes.php");
		$pt = new PageTypes();
		foreach($xsl_types as $xsl_type)
		{
			$xslm = new XslManager($xsl_type);
			foreach($xslm->XslAllXsl($id_style) as $xsl)
			{
				$xslm->id_pagetype = $xsl['id_type'];
				$type = "none";
				if($xsl_type=="global")
					$type = array_search($xsl['id_type'],$pt->gtypes);
				if($xsl_type=="specific")
					$type = array_search($xsl['id_type'],$pt->types);
				$xsl_new = $xslm->OutputUpdate($xsl['xsl'],$type);
				$xslm->XslUpdate($xsl['id_xsl'],$id_style,$xsl_new);
			}
		}
	}
	
	public function XslUpdateAll()
	{
		$this->XslUpdate(0);
		$styles = $this->StylesAll();
		foreach($styles as $style)
			$this->XslUpdate($style['id_style']);
	}
	
}
?>
