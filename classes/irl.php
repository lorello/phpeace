<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");
define('FRIENDLY_URL_SESSION_VAR',"furl");

/**
 * Internal Resource Locator
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
*/
class IRL
{
	/**
	 * @var Topic */
	public $topic;

	/**
	 * URL of public portal
	 *
	 * @var string
	 */
	public $pub_web;

	/**
	 * Configured prefixes for topic URLs
	 *
	 * @var array
	 */
	public $paths;

	/**
	 * Enforce a specific style
	 * (used in preview mode)
	 *
	 * @var integer
	 */
	public $force_id_style;

	/**
	 * Redirection types
	 *
	 * @var array
	 */
	public $redirect_types;

	/**
	 * @var Ini */
	private $ini;

	/**
	 * @var TextHelper */
	private $th;

	/**
	 * @var SharedMem */
	private $mem;

	/**
	 * Whether is running in protected mode (restricted access)
	 *
	 * @var boolean
	 */
	private $protected;

	/**
	 * File extension used for image conversion
	 *
	 * @var string
	 */
	private $convert_format;

	/**
	 * JPG compression quality
	 *
	 * @var string
	 */
	private $convert_quality;
	
	/**
	 * CDN
	 *
	 * @var string
	 */
	private $cdn;

	/**
	 * Dynamic page prefix
	 *
	 * @var string
	 */
	private $dpage;

	/**
	 * Active modules
	 *
	 * @var array
	 */
	private $modules;

	/**
	 * Image sizes
	 *
	 * @var string
	 */
	public $img_sizes;

	/**
	 * Whether HighSlide is active
	 *
	 * @var boolean
	 */
	private $highslide;

	/**
	 * Whether Rewrite active
	 *
	 * @var boolean
	 */
	private $rewrite;

	/**
	 * Assets types
	 *
	 * @var array
	 */
	private $assets_types = array('cover','doc_cover','gallery_image','galleries_image','image','image_orig','event_image','event_image_orig','org_image','org_thumb','org_image_orig','product_image','user_image','video_thumb','widget_image');

	/**
	 * Maximum length of a friendly URL
	 *
	 */
	const FriendlyUrlMaxLength = 50;

	/**
	 * Maximum tries in case of friendly URLs collisions
	 *
	 */
	const FriendlyUrlMaxTries = 20;

	/**
	 * Initialize local properties
	 *
	 * @param boolean $protected	Running in protected mode (restricted access)
	 */
	function __construct($protected=false)
	{
		$this->ini = new Ini();
		$conf = new Configuration();
		$this->paths = $conf->Get("paths");
		$this->convert_format = $conf->Get("convert_format");
		$this->convert_quality = $conf->Get("convert_quality");
		$this->cdn = $conf->Get("cdn");
		$this->pub_web = $this->ini->Get("pub_web");
		$this->highslide = $conf->Get("highslide");
		$this->rewrite = $conf->Get("mod_rewrite");
		$this->img_sizes = $conf->Get("img_sizes");
		$this->th = new TextHelper();
		$this->mem = new SharedMem();
		$this->force_id_style = 0;
		$this->protected = $protected;
		$this->dpage = "preview.php";
		$this->modules = array();
		$redirect_types = array();
		$redirect_types[0] = 'Redirect';
		$redirect_types[1] = 'Redirect Permament';
		if($this->rewrite)
			$redirect_types[2] = 'Rewrite';
		$this->redirect_types = $redirect_types;
	}

	/**
	 * Destruct in-memory objects
	 *
	 */
	public function __destruct()
	{
		unset($this->ini);
		unset($this->th);
		unset($this->mem);
		if(isset($this->topic))
			unset($this->topic);
	}

	/**
	 * URL for CDN
	 *
	 * @param string	$path		directory
	 * @param string	$file		filename (original)
	 * @param integer	$size		Index of images widths array
	 * @return string	URL
	 */
	public function CDNURL($path,$file,$size=null)
	{
		$url = "{$this->cdn}/{$this->paths['images']}$path/$file";
		if($size>=0) {
			$url .= "?format={$this->convert_format}";
		} else {
			$format = strtolower(substr($file,strrpos($file,".")+1));
			$url .= "?format=$format";
		}
		if(!is_null($size) && $size>=0) {
			$url .= "&w={$this->img_sizes[$size]}";
		}
		return $url;
	}

	/**
	 * Dynamic URL
	 * (used for live PHP pages, for all protected pages and for internal preview)
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param boolean	$is_global	Whether the page is global or specific
	 * @return string				URL
	 */
	private function DynamicPath($type,$params,$is_global)
	{
		$url = "";
		switch ($type)
		{
			case "article":
				if($params['jump']!="")
					$url = $params['jump'];
				else
					$url .= "{$this->dpage}?id_type=3&id_topic={$params['id_topic']}&id={$params['id']}";
				break;
			case "article_box":
				$url .= "{$this->dpage}?id_type=4&id_topic={$params['id_topic']}&id={$params['id']}";
				break;
			case "audio":
				$subtype = $params['subtype']!=""? $params['subtype'] : "audio";
				$url .= "{$this->dpage}?id_type=0&id_module=9&module=media&hash={$params['hash']}&subtype=$subtype&id={$params['id']}";
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				break;
			case "books":
				$url .= "{$this->dpage}?id_type=0&module=books&id_module=16";
				switch ($params['subtype'])
				{
					case "category":
						$url .= "&id_publisher=" . $params['id_publisher'] . "&id_category=" . $params['id'];
						break;
					case "publisher":
						$url .= "&id_publisher=" . $params['id'];
						break;
					case "book":
						$url .= "&id=" . $params['id'];
						break;
					case "search":
						$url .= "&subtype=search";
						if ($params['q']!="")
							$url .=  "&q=" . $params['q'];
						break;
					case "reviews":
						$url .= "&id=" . $params['id'] . "&subtype=reviews";
						break;
					case "review_insert":
						$url .= "&id=" . $params['id'] . "&subtype=review_insert";
						break;
				}
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				if($params['subtype']=="reviews" && $params['id_review']>0)
					$url .= "#r" . $params['id_review'];
				break;
				break;
			case "campaign":
				$url .= "{$this->dpage}?id_type=7&id={$params['id']}&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				break;
			case "comments":
				$url .= "{$this->dpage}?id_type=22&id_r={$params['id_r']}&id_item={$params['id_item']}&subtype={$params['subtype']}&id_topic={$params['id_topic']}";
				if($params['id']>0)
					$url .= "&id={$params['id']}";
				if($params['id_parent']>0)
					$url .= "&id_parent={$params['id_parent']}";
				break;
			case "ecommerce":
				$url .= "{$this->dpage}?id_type=0&module=ecommerce&id_module=27&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				switch ($params['subtype'])
				{
					case "product":
					case "order":
					case "order_preview":
						$url .= "&id=" . $params['id'];
						break;
				}
				break;
			case "events":
				if($params['id_article']>0)
					$url = $this->Preview("article",array('id'=>$params['id_article'],'id_topic'=>$params['id_topic']),$is_global);
				else
				{
					$url .= "{$this->dpage}?id_type=5&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
					if ($params['offset']!="")
						$url .= "&offset={$params['offset']}";
					if ($params['id']!="")
						$url .= "&id={$params['id']}";
					if ($params['id_topic']>0)
						$url .= "&id_topic={$params['id_topic']}";
					if ($params['q']!="")
						$url .= "&q={$params['q']}";
					if ($params['id_geo']!="")
						$url .= "&id_geo={$params['id_geo']}";
					if ($params['id_etype']!="")
						$url .= "&id_etype={$params['id_etype']}";
					if ($params['period']!="")
						$url .= "&period={$params['period']}";
					if ($params['month']!="")
						$url .= "&m={$params['month']}";
				}
				break;
			case "feeds":
				$url .= "{$this->dpage}?id_gtype=3";
				break;
			case "forum":
				$url .= "{$this->dpage}?id_type=8&id={$params['id']}&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				if($params['id_thread']>0)
					$url .= "&id_thread={$params['id_thread']}";
				if($params['id_comment']>0)
					$url .= "&id_comment={$params['id_comment']}";
				break;
			case "galleries":
				$url .= "{$this->dpage}?id_gtype=4&id_group={$params['id_group']}&id_image={$params['id_image']}&id_gallery={$params['id_gallery']}";
				if($params['page']>0)
					$url .= "&p={$params['page']}";
				if($params['subtype']=="image_orig")
				{
					if($this->protected)
						$url = $this->PublicPath("galleries",array('id_image'=>$params['id_image'],'format'=>$params['format'],'subtype'=>"image_orig"),true);
					else
						$url = "/" . $this->Path("image_orig",array('id'=>$params['id_image'],'format'=>$params['format']));
				}
				if($params['subtype']=="slideshow")
					$url .= "&subtype={$params['subtype']}";
				break;
			case "gallery_item":
				$url .= "{$this->dpage}?id_type=10&id_topic={$params['id_topic']}&id_subtopic={$params['id_subtopic']}&id_gallery={$params['id_gallery']}&id={$params['id']}";
				break;
			case "geosearch":
				$url .= "{$this->dpage}?id_type=13";
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				if ($params['id']>0)
					$url .= "&id={$params['id']}";
				break;
			case "homepage":
				$url .= "{$this->dpage}?id_gtype=0";
				$url .= "&subtype={$params['subtype']}";
				if ($params['id']>0)
					$url .= "&id={$params['id']}";
				if ($params['q']!="")
					$url .= "&q={$params['q']}";
				if ($params['content']!="")
					$url .= "&content={$params['content']}";
				break;
			case "image_zoom":
				$url .= "{$this->dpage}?id_type=16&id_topic={$params['id_topic']}&id={$params['id']}&id_article={$params['id_article']}";
				break;
			case "keyword":
				$url .= "{$this->dpage}?id_gtype=9";
				if($params['k']!="")
					$url .= "&k=" . $params['k'];
				break;
			case "lists":
				$url .= "{$this->dpage}?id_type=0&module=lists&id_module=11";
				switch ($params['subtype'])
				{
					case "group":
						$url .= "&id_g=" . $params['id_group'];
						break;
					case "list":
						$url .= "&id=" . $params['id_list'];
						break;
				}
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				break;
				break;
			case "map":
				$url .= "{$this->dpage}?id_gtype=2";
				if ($params['id']>0)
					$url .= "&id={$params['id']}";
				break;
			case "meet":
				$url .= "{$this->dpage}?id_type=0&id_module=23&module=meet&id={$params['id']}&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				if ($params['id_question']>0)
					$url .=  "&id_question=" . $params['id_question'];
				break;
			case "orgs":
				$id_topic = $this->ini->GetModule("orgs","id_topic",0);
				$url .= "{$this->dpage}?id_type=0&module=orgs&id_module=8&id_topic=$id_topic&subtype={$params['subtype']}";
				switch ($params['subtype'])
				{
					case "org":
					case "keyword":
					case "type":
						$url .= "&id=" . $params['id'];
						break;
					case "search":
					case "search_advanced":
						if($params['from']=="org_search")
						{
							$search_terms = $params['search_terms'];
							$url .= "&from=org_search";
							if(is_array($search_terms))
							{
								$url .= "&name={$search_terms['name']}&address={$search_terms['address']}&id_assotype={$search_terms['id_assotype']}&id_geo={$search_terms['id_geo']}&id_k={$search_terms['id_k']}";
								foreach($search_terms as $key=>$value)
								{
									if (substr($key,0,3)=="kp_" && $value!="")
									{
										$url .= "&$key=$value";
									}
								}
							}
						}
						if($params['subtype']=="search_advanced")
							$url .= "&advanced=1";
						break;
				}
				break;
				break;
			case "people":
				$url .= "{$this->dpage}?id_type=11";
				if ($params['subtype']!="")
					$url .= "&subtype={$params['subtype']}";
				if ($params['jumpback'])
					$url .= "&jumpback=1";
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				break;
			case "poll":
				$url .= "{$this->dpage}?id_type=9&id={$params['id']}&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				if ($params['id_question']>0)
					$url .=  "&id_question=" . $params['id_question'];
				if ($params['id_questions_group']>0)
					$url .=  "&id_questions_group=" . $params['id_questions_group'];
				break;
			case "quotes":
				$url .= "{$this->dpage}?id_type=6&id_topic={$params['id_topic']}&subtype={$params['subtype']}";
				break;
			case "rss":
				$url .= "{$this->dpage}?id_gtype=1";
				if ($params['subtype']!="")
					$url .= "&subtype={$params['subtype']}";
				if ($params['id']>0)
					$url .= "&id={$params['id']}";
				break;
			case "search":
				$url .= "{$this->dpage}?id_type=18";
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				if ($params['q']!="")
					$url .= "&q={$params['q']}";
				if ($params['k']!="")
					$url .= "&k={$params['k']}";
				if ($params['id_group']>0)
					$url .= "&id_group={$params['id_group']}";
				if ($params['id_subtopic']>0)
					$url .= "&id_subtopic={$params['id_subtopic']}";
				break;
			case "subtopic":
				switch($params['id_type'])
				{
					case "5": // form
						$url .= "{$this->dpage}?id_type=2&id_topic={$params['id_topic']}&id={$params['id']}";
						if(isset($params['id_doc']) && $params['id_doc']>0)
							$url .= "&id_doc={$params['id_doc']}";
						break;
					case "6": // link
						$url = $params['link'];
						break;
					case "7": // campaign
						$url .= "{$this->dpage}?id_type=7&id_topic={$params['id_topic']}";
						if($params['id_item']>0)
						{
							$url .= "&id={$params['id_item']}";
							if($params['id_subitem']>0)
							{
								$subtypes = $this->Subitems(7,false);
								foreach($subtypes as $subtype)
								{
									if($subtype[0]==$params['id_subitem'])
										$sbtype = $subtype[1];
								}
								$url .= "&subtype=$sbtype";
							}
						}
						else
							$url .= "&subtype=list";
						break;
					case "8": // forum
						$url .= "{$this->dpage}?id_type=8&id_topic={$params['id_topic']}";
						if($params['id_item']>0)
						{
							$url .= "&id={$params['id_item']}";
							if($params['id_subitem']>0)
							{
								$subtypes = $this->Subitems(8,false);
								$sbtype = '';
								foreach($subtypes as $subtype)
								{
									if($subtype[0]==$params['id_subitem'])
										$sbtype = $subtype[1];
								}
								$url .= "&subtype=$sbtype";
							}
						}
						else
							$url .= "&subtype=list";
						break;
					case "10": // events
						switch($params['id_item']) // subtype
						{
							case 0:
								$subtype = "next";
								break;
							case 1:
								$subtype = "day";
								break;
							case 2:
								$subtype = "month";
								break;
							case 3:
								$subtype = "insert";
								break;
							case 4:
								$subtype = "search";
								break;
						}
						$url .= "{$this->dpage}?id_type=5&id_topic={$params['id_topic']}&subtype=$subtype";
						break;
					case "12": //module
						$url .= "{$this->dpage}?id_type=0&id_module={$params['id_item']}&id_topic={$params['id_topic']}";
						switch($params['id_item']) // id_module
						{
							case "11": //lists
								$url .= "&module=lists";
								if($params['id_subitem']>0)
									$url .= "&id={$params['id_subitem']}";
								break;
							case "16": //books
								$url .= "&module=books";
								break;
							case "8": //orgs
								$id_topic = $this->ini->GetModule("orgs","id_topic",0);
								$url = "{$this->dpage}?id_type=0&id_module={$params['id_item']}&id_topic={$id_topic}&module=orgs";
								if($params['id_subitem']>0)
								{
									$subtypes = $this->SubItems(8);
									foreach($subtypes as $subtype)
									{
										if($subtype[0]==$params['id_subitem'])
											$sbtype = $subtype[1];
									}
									$url .= "&subtype=$sbtype";
								}
								break;
							case "27": //ecommerce
								$url .= "&module=ecommerce";
								if($params['id_subitem']>0)
								{
									$subtypes = $this->SubItems(27);
									foreach($subtypes as $subtype)
									{
										if($subtype[0]==$params['id_subitem'])
											$sbtype = $subtype[1];
									}
									$url .= "&subtype=$sbtype";
								}
								break;
						}
						break;
					case "13":
						$url .= "{$this->dpage}?id_type=2&id_topic={$params['id_topic']}&id={$params['id']}&subtype={$params['id_item']}";
						break;
					case "14": // quotes
						$url .= "{$this->dpage}?id_type=6&id_topic={$params['id_topic']}";
						break;
					case "17": // poll
						$url .= "{$this->dpage}?id_type=9&id_topic={$params['id_topic']}";
						if($params['id_item']>0)
						{
							$url .= "&id={$params['id_item']}";
							if($params['id_subitem']>0)
							{
								$subtypes = $this->Subitems(17,false);
								foreach($subtypes as $subtype)
								{
									if($subtype[0]==$params['id_subitem'])
										$sbtype = $subtype[1];
								}
								$url .= "&subtype=$sbtype";
							}
						}
						else
							$url .= "&subtype=list";
						break;
					default:
						$url .= "{$this->dpage}?id_type=2&id_topic={$params['id_topic']}&id={$params['id']}";
				}
				break;
			case "topic_home":
				$url .= "{$this->dpage}?id_type=1&id_topic={$params['id_topic']}";
				break;
			case "user":
				$url .= "{$this->dpage}?id_gtype=5";
				if($params['login']!="")
					$url .= "&l=" . $params['login'];
				if($params['t']>0)
					$url .= "&t=" . $params['t'];
				if($params['u']>0)
					$url .= "&u=" . $params['u'];
				if($params['b']>0)
					$url .= "&b=" . $params['b'];
				if($params['c']>0)
					$url .= "&c=" . $params['c'];
				break;
			case "media":
				$subtype = $params['subtype']!=""? $params['subtype'] : "home";
				$url .= "{$this->dpage}?id_type=0&id_module=9&module=media&hash={$params['hash']}&subtype=$subtype&id={$params['id']}";
				if ($params['id_topic']>0)
					$url .=  "&id_topic=" . $params['id_topic'];
				break;
			default:
				$url .= "/" . $this->Path($type,$params);
				break;
		}
		if($this->force_id_style>0)
			$url .= "&id_style={$this->force_id_style}";
		return $url;
	}

	/**
	 * Retrieve all friendly URLs for a specific topic
	 *
	 * @param integer $id_topic
	 * @return array
	 */
	public function FriendlyUrlAll($id_topic)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_res,id,furl,url FROM url_friendly WHERE id_topic='$id_topic'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Load all friendly URLs for a specific topic in memory
	 *
	 * @param integer $id_topic	Topic ID
	 * @return	array			List of all topic friendly URLs
	 */
	private function FriendlyUrlsCache($id_topic)
	{
		if($this->mem->IsVarSet(FRIENDLY_URL_SESSION_VAR.$id_topic))
		{
			$topic_furls = $this->mem->Get(FRIENDLY_URL_SESSION_VAR.$id_topic);
		}
		else
		{
			$topic_furls = array();
			$furls = $this->FriendlyUrlAll($id_topic);
			foreach($furls as $furl)
			{
				$id = $this->FriendlyUrlsCacheResourceId($furl['id_res'],$furl['id']);
				$topic_furls[$id] = $furl['furl'];
			}
			$this->mem->Set(FRIENDLY_URL_SESSION_VAR.$id_topic,$topic_furls);
		}
		return $topic_furls;
	}

	/**
	 * Reset friendly URLs cache for a specific topic
	 *
	 * @param integer $id_topic	Topic ID
	 */
	private function FriendlyUrlsCacheExpire($id_topic)
	{
		$this->mem->Delete(FRIENDLY_URL_SESSION_VAR.$id_topic);
	}

	/**
	 * Resource identifier in friendly URLs cache
	 *
	 * @param integer $id_res
	 * @param integer $id
	 * @return string
	 */
	private function FriendlyUrlsCacheResourceId($id_res,$id)
	{
		return "r{$id_res}i{$id}";
	}

	/**
	 * Delete a friendly URL for a specific resource
	 *
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @param integer $id_topic	Topic ID
	 */
	public function FriendlyUrlDelete($id_res,$id,$id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "url_friendly" );
		$res[] = $db->query( "DELETE FROM url_friendly WHERE id_res='$id_res' AND id='$id'" );
		Db::finish( $res, $db);
		$this->FriendlyUrlsCacheExpire($id_topic);
	}

	/**
	 * Delete all friendly URLs for a specific topic
	 *
	 * @param integer $id_topic		Topic ID
	 */
	public function FriendlyUrlDeleteTopic($id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "url_friendly" );
		$res[] = $db->query( "DELETE FROM url_friendly WHERE id_topic='$id_topic' " );
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve a friendly URL for a specific resource
	 *
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @return array
	 */
	public function FriendlyUrlGet($id_res,$id)
	{
		$row = array();
		if($id>0 && $id_res>0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT id_res,id,furl,url,id_topic FROM url_friendly WHERE id_res='$id_res' AND id='$id'";
			$db->query_single($row,$sqlstr);
		}
		return $row;
	}

	/**
	 * Check if a friendly URL already exists
	 *
	 * @param string $furl
	 * @param integer $id_topic	Topic ID
	 * @return boolean
	 */
	public function FriendlyUrlExists($furl,$id_topic)
	{
		$row = array();
		$db =& Db::globaldb();
		$furl = $db->SqlQuote($furl);
		$sqlstr = "SELECT id_res,id FROM url_friendly WHERE furl='$furl' AND id_topic='$id_topic' ";
		$db->query_single($row,$sqlstr);
		return $row['id']>0 || in_array($furl, $this->FriendlyUrlReserved());
	}

	private function FriendlyUrlReserved()
	{
		$reserved = array();
		$reserved[] = "js";
		$reserved[] = "rest";
		return $reserved;
	}

	/**
	 * Store a friendly URL
	 *
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @param string $furl		Friendly URL
	 * @param string $url		URL to redirect
	 * @param boolean $is_res_visible		Whether the resource is visible
	 * @param integer $id_topic	Topic ID
	 */
	public function FriendlyUrlStore($id_res,$id,$furl,$id_topic,$is_res_visible)
	{
		$url = $this->ResourceUrl($id_res,$id,$id_topic);
		if($url!="")
		{
			$row = $this->FriendlyUrlGet($id_res,$id);
			if($row['furl']!="" && ($furl=="" || !$is_res_visible))
			{
				$this->FriendlyUrlDelete($id_res,$id,$id_topic);
			}
			else
			{
				$db =& Db::globaldb();
				if($furl!=$db->SqlQuote($row['furl']))
				{
					$furl = $this->FriendlyUrlCheck($furl,$id_topic,true);
					$furl = $db->SqlQuote($furl);
					$url = $db->SqlQuote($url);
					$db->begin();
					$db->lock( "url_friendly" );
					$sqlstr = "REPLACE INTO url_friendly (id_res,id,furl,url,id_topic)
					VALUES ('$id_res','$id','$furl','$url','$id_topic') ";
					$res[] = $db->query( $sqlstr );
					Db::finish( $res, $db);
				}
				else
				{
					$this->FriendlyUrlResourceUpdate($url,$id_res,$id,$id_topic);
				}
			}
			$this->FriendlyUrlsCacheExpire($id_topic);
		}
	}

	/**
	 * Suggest a friendly URL
	 *
	 * @param integer 	$id_topic		Topic ID
	 * @param integer 	$furl_scheme_id	Friendly URL scheme ID
	 * @param integer 	$id_res			Resource type ID
	 * @param string 	$title			Resource title
	 * @param integer 	$container		Resource container name
	 * @param integer 	$timestamp		Resource creation timestamp
	 * @return string					Friendly URL
	 */
	public function FriendlyUrlSuggest($id_topic,$furl_scheme_id,$id_res,$title,$container,$timestamp)
	{
		$this->FriendlyUrlClean($title);
		$this->FriendlyUrlClean($container);
		$furl = $title;
		if($id_res==5)
		{
			switch($furl_scheme_id)
			{
				case "1":
					$furl = $title;
					break;
				case "2":
					$furl = $container . "/" . $title;
					break;
				case "3":
					$furl = date("Y/m/d",$timestamp) . "/" . $title;
					break;
			}
		}
		return $this->FriendlyUrlCheck($furl,$id_topic,false);
	}

	/**
	 * Clean a Friendly URL from invalid characters
	 *
	 * @param string $string
	 */
	public function FriendlyUrlClean(&$string)
	{
		$string = strtolower($string);
		$string = preg_replace('#\/+#', '\/', $string);
		$string = preg_replace('/[^a-z0-9\s\/-]/', "", $string);
		$string = trim(preg_replace('/[\s-]+/', " ", $string));
		$string = trim(substr($string, 0, self::FriendlyUrlMaxLength));
		$string = preg_replace('/\s/', "-", $string);
		$string = trim($string);
		$string = trim($string,"/");
	}

	/**
	 * Check a suggested friendly URL
	 *
	 * @param string 	$furl		Proposed friendly URL
	 * @param integer 	$id_topic	Topic ID
	 * @param boolean 	$to_clean	Whether proposed URL must be cleaned
	 * @param integer 	$try		Iterative try
	 * @return string				Friendly URL
	 */
	private function FriendlyUrlCheck($furl,$id_topic,$to_clean,$try=1)
	{
		$this->FriendlyUrlClean($furl);
		if($try>1)
		{
			if($try>2)
			{
				$previous_try = $try - 1;
				$furl = substr($furl, 0, strrpos($furl, "-$previous_try")) . "-" . $try;
			}
			else
				$furl .= "-" . $try;
		}
		include_once(SERVER_ROOT."/../classes/validator.php");
		$va = new Validator();
		$va->URLPath($furl);
		if($va->return)
		{
			if($this->FriendlyUrlExists($furl,$id_topic) && $try < self::FriendlyUrlMaxTries)
			{
				$try++;
				$furl = $this->FriendlyUrlCheck($furl,$id_topic,$to_clean,$try);
			}
		}
		return $furl;
	}

	/**
	 * Generate all friendly URLs associated to a topic
	 *
	 * @param integer $id_topic	Topic ID
	 * @return integer		Number of friendly URLs
	 */
	public function FriendlyUrlGenerateTopic($id_topic)
	{
		$counter = 0;
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$friendly_url_scheme = $t->row['friendly_url'];
		if($friendly_url_scheme > 0)
		{
			$subtopics = $t->Subtopics();
			$id_res = 2;
			foreach($subtopics as $subtopic)
			{
				if($subtopic['id_type']>0)
				{
					$furl_row = $this->FriendlyUrlGet($id_res,$subtopic['id_subtopic']);
					if(!isset($furl_row['id']) || $furl_row['furl']=="")
					{
						$furl = $this->FriendlyUrlSuggest($id_topic,$friendly_url_scheme,$id_res,$subtopic['name'],"",0);
						$this->FriendlyUrlStore($id_res,$subtopic['id_subtopic'],$furl,$id_topic,$subtopic['visible']!=4 && $subtopic['id_type']!=$t->subtopic_types['url']);
						$counter++;
					}
				}
			}
			$articles = $t->Articles();
			$id_res = 5;
			include_once(SERVER_ROOT."/../classes/article.php");
			foreach($articles as $article)
			{
				$furl_row = $this->FriendlyUrlGet($id_res,$article['id_article']);
				if(!isset($furl_row['id']) || $furl_row['furl']=="")
				{
					$a = new Article($article['id_article']);
					$art = $a->ArticleGet();
					$subtopic = $t->SubtopicGet($art['id_subtopic']);
					$furl = $this->FriendlyUrlSuggest($id_topic,$friendly_url_scheme,$id_res,$art['headline'],$subtopic['name'],$art['written_ts']);
					$this->FriendlyUrlStore($id_res,$article['id_article'],$furl,$id_topic,$subtopic['visible']!=4);
					$counter++;
				}
			}
			if($counter>0)
			{
				$t->queue->JobInsert($t->queue->types['all'],$id_topic,"");
			}
		}
		$this->FriendlyUrlsCacheExpire($id_topic);
		return $counter;
	}

	/**
	 * Regenerate the friendly URL associated to a resource
	 *
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @param integer $id_topic	Topic ID
	 */
	private function FriendlyUrlRegenerate($id_res,$id,$id_topic)
	{
		$url = $this->ResourceUrl($id_res,$id,$id_topic);
		$this->FriendlyUrlResourceUpdate($url,$id_res,$id,$id_topic);
		$this->FriendlyUrlsCacheExpire($id_topic);
	}

	/**
	 * Store the new friendly URL associated to a resource
	 *
	 * @param string  $url		Resource URL
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @param integer $id_topic	Topic ID
	 */
	private function FriendlyUrlResourceUpdate($url,$id_res,$id,$id_topic)
	{
		$db =& Db::globaldb();
		$url = $db->SqlQuote($url);
		$db->begin();
		$db->lock( "url_friendly" );
		$sqlstr = "UPDATE url_friendly SET url='$url',id_topic='$id_topic' WHERE id_res='$id_res' AND id='$id'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Regenerate all friendly URLs associated to a topic
	 *
	 * @param integer $id_topic	Topic ID
	 */
	public function FriendlyUrlRegenerateTopic($id_topic)
	{
		$resources = $this->FriendlyUrlAll($id_topic);
		foreach($resources as $resource)
		{
			$this->FriendlyUrlRegenerate($resource['id_res'],$resource['id'],$id_topic);
		}
		$this->FriendlyUrlsCacheExpire($id_topic);
	}

	public function GoogleCalendarLink($title,$description,$start_ts,$end_ts,$all_day,$location) {
		$export_google = 'https://www.google.com/calendar/render?action=TEMPLATE';
		$export_google .= '&text=' . urlencode($title);
		if($event['is_html']) {
			$this->th->Html2Text($event['description']);
		}
		$export_google .= '&details=' . urlencode($description);
		$date_format = $all_day? 'Ymd' : 'Ymd\THis';
		$end_ts = $all_day? $start_ts + 24*3600 : $end_ts;
		$export_google .= '&dates=' . date($date_format,$start_ts) . '/' . date($date_format,$end_ts);
		$export_google .= "&location={$location}";
		// time zone
		$export_google .= '&ctz=Europe%2FRome';
		return $export_google;
	}

	/**
	 * Retrieve the real URL for a specific resource
	 *
	 * @param integer $id_res	Resource type ID
	 * @param integer $id		Resource ID
	 * @param integer $id_topic	Topic ID
	 * @return string			URL
	 */
	private function ResourceUrl($id_res,$id,$id_topic)
	{
		$url = "";
		include_once(SERVER_ROOT."/../classes/topic.php");
		$this->topic = new Topic($id_topic);
		switch($id_res)
		{
			case "2":
				$subtopic = $this->topic->SubtopicGet($id);
				$subtopic['id'] = $id;
				$url = $this->topic->SubtopicPublishable($subtopic['id_type'])? $this->PublicUrlTopic("subtopic",$subtopic,$this->topic,false) : $this->PublicUrlGlobal("subtopic",$subtopic,false);
				break;
			case "5";
			$url = $this->PublicUrlTopic("article",array('id'=>$id,'id_topic'=>$id_topic),$this->topic,false);
			break;
		}
		return $url;
	}

	/**
	 * Check if a module is active
	 *
	 * @param string $module_path
	 * @return boolean
	 */
	public function IsModuleActive($module_path)
	{
		$active = false;
		if(count($this->modules)>0)
		{
			reset($this->module);
			foreach($this->modules as $module)
			{
				if($module['path']==$module_path)
					$active= true;
			}
		}
		else
		{
			$active = true;
		}
		return $active;
	}

	/**
	 * Initialize the local array of active modules
	 *
	 * @param array $modules
	 */
	public function ModulesActiveSet($modules)
	{
		$this->modules = $modules;
	}

	/**
	 * The dynamic URL path to local uploaded files
	 * (used in the admin interface)
	 *
	 * @param string 	$type	File type
	 * @param array 	$params	Parameters
	 * @return string			URL Path
	 */
	private function Path($type,$params)
	{
		switch ($type)
		{
			case "article_doc":
			case "org_doc":
				$path = "docs/upload.php?src=docs/" . $params['id'] . "." . $params['format'];
				break;
			case "audio_orig":
				$path = "images/upload.php?src=audios/orig/{$params['id']}.{$params['format']}";
				break;
			case "audio_enc":
			case "audio_enc_link":
				$path = "images/upload.php?src=audios/enc/{$params['id']}.mp3";
				break;
			case "audio_xml":
				$path = "docs/audio_xml.php?hash={$params['hash']}";
				break;
			case "cover":
				$path = "images/upload.php?src=covers/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "doc_cover":
				$path = "images/upload.php?src=docs/covers/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "event_image":
				$path = "images/upload.php?src=events/" . ($params['size']-1) . "/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "event_image_orig":
				$path = "images/upload.php?src=events/orig/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "gallery_image":
			case "galleries_image":
				$path = "images/upload.php?src=images/" . $params['image_size'] . "/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "graphic":
				$path = "images/upload.php?src=graphics/orig/{$params['id']}.{$params['format']}";
				break;
			case "image":
				if($params['size']<0)
					$path = "images/upload.php?src=images/orig/" . $params['id'] . "." . $params['format'];
				else
					$path = "images/upload.php?src=images/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "image_orig":
				$path = "images/upload.php?src=images/orig/{$params['id']}.{$params['format']}";
				break;
			case "org_image":
				$path = "images/upload.php?src=orgs/1/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "org_image_orig":
				$path = "images/upload.php?src=orgs/orig/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "org_thumb":
				$path = "images/upload.php?src=orgs/0/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "product_image":
				$path = "images/upload.php?src=products/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "user_image":
				$path = "images/upload.php?src=users/0/" . $params['id'] . "." . $this->convert_format . "&format={$params['format']}";
				break;
			case "video_orig":
				$path = "images/upload.php?src=videos/orig/{$params['id']}.{$params['format']}";
				break;
			case "video_enc":
			case "video_enc_link":
				$path = "images/upload.php?src=videos/flv/{$params['id']}.flv";
				break;
			case "video_image":
				$seq = (int)$params['seq'];
				$path = "images/upload.php?src=videos/images/{$params['id']}_{$seq}.jpg";
				break;
			case "video_thumb":
				$path = "images/upload.php?src=videos/thumbs/{$params['id']}.{$this->convert_format}";
				break;
			case "video_xml":
				$path = "docs/video_xml.php?hash={$params['hash']}";
				break;
			case "watermark":
				$path = "images/upload.php?src=custom/watermark.png";
				break;
			case "widget_image":
				$path = "images/upload.php?src=widgets/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
		}
		return $path;
	}

	/**
	 * The physical path to local uploaded files
	 *
	 * @param string 	$type	File type
	 * @param array 	$params	Parameters
	 * @return string			Path
	 */
	public function PathAbs($type,$params)
	{
		switch ($type)
		{
			case "article_doc":
			case "org_doc":
				$path = "uploads/docs/" . $params['id'] . "." . $params['format'];
				break;
			case "audio_orig":
				$path = "uploads/audios/orig/{$params['id']}.{$params['format']}";
				break;
			case "audio_enc":
				$path = "uploads/audios/enc/{$params['id']}.mp3";
				break;
			case "cover":
				$path = "uploads/covers/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
			case "doc_cover":
				$path = "uploads/docs/covers/" . $params['id'] . "." . $this->convert_format;
				break;
			case "event_image":
				$path = "uploads/events/" . ($params['size']-1) . "/" . $params['id'] . "." . $this->convert_format;
				break;
			case "event_image_orig":
				$path = "uploads/events/orig/" . $params['id'] . "." . $this->convert_format;
				break;
			case "gallery_image":
			case "galleries_image":
				$path = "uploads/images/" . $params['image_size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
			case "graphic":
				$path = "uploads/graphics/orig/{$params['id']}.{$params['format']}";
				break;
			case "image":
				if($params['size']<0)
					$path = "uploads/images/orig/" . $params['id'] . "." . $params['format'];
				else
					$path = "uploads/images/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
			case "image_orig":
				$path = "uploads/images/orig/{$params['id']}.{$params['format']}";
				break;
			case "org_image":
				$path = "uploads/orgs/1/" . $params['id'] . "." . $this->convert_format;
				break;
			case "org_image_orig":
				$path = "uploads/orgs/orig/" . $params['id'] . "." . $this->convert_format;
				break;
			case "org_thumb":
				$path = "uploads/orgs/0/" . $params['id'] . "." . $this->convert_format;
				break;
			case "product_image":
				$path = "uploads/products/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
			case "user_image":
				$path = "uploads/users/0/" . $params['id'] . "." . $this->convert_format;
				break;
			case "video_orig":
				$path = "uploads/videos/orig/{$params['id']}.{$params['format']}";
				break;
			case "video_enc":
				$path = "uploads/videos/flv/{$params['id']}.flv";
				break;
			case "video_image":
				$seq = (int)$params['seq'];
				$path = "uploads/videos/images/{$params['id']}_{$seq}.jpg";
				break;
			case "video_thumb":
				$path = "uploads/videos/thumbs/{$params['id']}.{$this->convert_format}";
				break;
			case "widget_image":
				$path = "uploads/widgets/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				break;
		}
		return $path;
	}

	/**
	 * Preview URL
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param boolean	$is_global	Whether the page is global or specific
	 * @return string				URL
	 */
	public function Preview($type,$params,$is_global=true)
	{
		$this->dpage = "preview.php";
		return $this->DynamicPath($type,$params,$is_global);
	}

	/**
	 * Protected URL
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param boolean	$is_global	Whether the page is global or specific
	 * @return string				URL
	 */
	public function ProtectedPath($type,$params,$is_global=true)
	{
		$this->dpage = $this->pub_web . "/tools/protected.php";
		return $this->DynamicPath($type,$params,$is_global);
	}

	/**
	 * URL path to public pages
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param boolean	$is_global	Whether the page is global or specific
	 * @param boolean	$abs_path	Whether the path is absolute (from the installation directory) or not (relative to the portal URL)
	 * @param boolean	$furl_lookup	Whether to use friendly URLs
	 * @return string				URL
	 */
	public function PublicPath($type,$params,$is_global,$abs_path=false,$furl_lookup=true)
	{
		$path = "";
		if($abs_path)
			$path .= "pub";
		$path .= "/";
		if(!$is_global)
		{
			if(isset($this->topic))
				$path .= $this->topic->path;
			else
				UserError("Topic not set in IRL",array("type"=>$type));
		}
		if(!$abs_path && ($type=="article" || $type=="subtopic"))
		{
			$topic_furls = $this->FriendlyUrlsCache($this->topic->id);
		}
		$path_prefix = isset($this->paths[$type])? $this->paths[$type] : "";
		switch ($type)
		{
			case "article":
				if($params['jump']!="")
					$path = $params['jump'];
				else
				{
					$furl_id = $this->FriendlyUrlsCacheResourceId(5,$params['id']);
					if($furl_lookup && is_array($topic_furls) && count($topic_furls)>0 && isset($topic_furls[$furl_id]))
						$path .= "/" . $topic_furls[$furl_id];
					else
						$path .= $path_prefix . $params['id'] .".html";
				}
				break;
			case "article_box":
				$path .= $path_prefix . $params['id'] .".html";
				break;
			case "article_doc":
				$path .= $path_prefix . $params['id'] . "." . $params['format'];
				break;
			case "audio_enc":
				$path .= $this->paths['graphic'] . "audios/e/{$params['hash2']}.mp3";
				break;
			case "audio_enc_link":
				$path .= $this->ini->Get('media_path') . "/audio.php?h={$params['hash']}";
				break;
			case "audio_xml":
				$path .= $this->paths['graphic'] . "audios/x/{$params['hash']}.xml";
				break;
			case "books":
				$path .= $this->ini->Get('books_path') . "/";
				switch ($params['subtype'])
				{
					case "category":
						$path .= "index.php?id_publisher=" . $params['id_publisher'] . "&id_category=" . $params['id'];
						break;
					case "book":
						$path .= "index.php?id=" . $params['id'];
						break;
					case "reviews":
						$path .= "index.php?id=" . $params['id'] . "&reviews=all";
						break;
					case "review_insert":
						$path .= "index.php?id=" . $params['id'] . "&reviews=insert";
						break;
					case "publisher":
						$path .= "index.php?id_publisher=" . $params['id'];
						break;
					case "search":
						$path .= "search.php";
						if ($params['q']!="")
							$path .=  "?q=" . $params['q'];
						break;
					default:
						$path .= "index.php";
						break;
				}
				if ($params['id_topic']>0)
					$path .=  ((strpos($path,"?")===false)? "?":"&") . "id_topic=" . $params['id_topic'];
				if($params['subtype']=="reviews" && $params['id_review']>0)
					$path .= "#r" . $params['id_review'];
				break;
			case "campaign":
				$path .= $this->ini->Get('campaign_path') . "/";
				switch ($params['subtype'])
				{
					case "sign_person":
						$path .= "person.php";
						break;
					case "sign_org":
						$path .= "org.php";
						break;
					case "signatures_person":
						$path .= "persons.php";
						break;
					case "signatures_org":
						$path .= "orgs.php";
						break;
					case "insert":
						$path .= "insert.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				$path .= "?id={$params['id']}";
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "comments":
				$path .= "tools/comment.php?id_r={$params['id_r']}&id_item={$params['id_item']}&subtype=" . $params['subtype'];
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				if($params['id']>0)
					$path .= "&id={$params['id']}";
				if($params['id_parent']>0)
					$path .= "&id_parent={$params['id_parent']}";
				break;
			case "cover":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("covers", "{$params['id']}.{$params['format']}", $params['size']);
				} else {
					$path .= $this->paths['graphic'] . "covers/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "doc_cover":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("docs", "{$params['id']}.{$params['format']}", $params['size']);
				} else {
					$path .= $this->paths['graphic'] . "docs/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "dodc":
				$path .= "dodc/";
				switch ($params['subtype'])
				{
					case "contractors":
					case "contractor":
						$path .= "contractors.php?" . ($params['id']>0? "id={$params['id']}":"");
						break;
					case "offices":
					case "office":
						$path .= "offices.php?" . ($params['id']>0? "id={$params['id']}":"");
						break;
					case "search":
						$path .= "search.php";
						if ($params['q']!="")
							$path .=  "?q=" . $params['q'];
							break;
					default:
						$path .= "index.php";
						break;
				}
				break;
			case "ecommerce":
				$path .=  "ecomm/";
				switch ($params['subtype'])
				{
					case "order_preview":
						$path .= "order_preview.php?id=" . $params['id'];
						break;
					case "order":
						$path .= "order.php?id=" . $params['id'];
						break;
					case "orders":
						$path .= "orders.php?";
						break;
					case "product":
						$path .= "product.php?id=" . $params['id'];
						break;
					case "products":
						$path .= "products.php?";
						break;
					default:
						$path .= "index.php?id_topic=" . $params['id_topic'];
						break;
				}
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "error404":
				if($this->topic->id>0)
					$path .= "/";
				$path .=  "404.html";
				break;
			case "event_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("events", "{$params['id']}.jpg", $params['size']);
				} else {
					$path .= $this->paths['graphic'] . "events/1/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "event_image_orig":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("events", "{$params['id']}.jpg");
				} else {
					$path .= $this->paths['graphic'] . "events/orig/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "events":
				$path .= $this->ini->Get('events_path') . "/";
				switch ($params['subtype'])
				{
					case "insert":
						$path .= "insert.php?subtype=" . $params['subtype'];
						break;
					case "event":
						$path .= "event.php?id=" . $params['id'];
						if($params['id_article']>0)
							$path = $this->PublicPath("article",array('id'=>$params['id_article']),false,false);
						break;
					case "day":
						$path .= "index.php?offset=" . $params['offset'];
						break;
					case "date":
						$path .= "day.php?d=" . $params['date'];
						break;
					case "month":
						$path .= "month.php?m=" . $params['month'];
						break;
					case "search":
						$path .= "search.php?subtype=" . $params['subtype'];
						if ($params['q']!="")
							$path .=  "&q=" . $params['q'];
						if ($params['id_topic']>0)
							$path .=  "&id_topic=" . $params['id_topic'];
						if ($params['id_geo']>0)
							$path .=  "&id_geo=" . $params['id_geo'];
						if ($params['id_etype']>0)
							$path .=  "&id_etype=" . $params['id_etype'];
						if ($params['period']>0)
							$path .=  "&period=" . $params['period'];
						if ($params['tab']!='')
							$path .=  "&tab=" . $params['tab'];
							break;
					default:
						$path .= "events.php";
						break;
				}
				if ($params['id_topic']>0 && !$params['id_article']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "feeds":
				$path .= "feeds/index.html";
				break;
			case "forum":
				$path .= $this->ini->Get('forum_path') . "/";
				switch ($params['subtype'])
				{
					case "thread":
						$path .= "thread.php";
						break;
					case "thread_insert":
						$path .= "insert.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				$path .= "?id={$params['id']}";
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				if ($params['id_thread']>0)
					$path .=  "&id_thread=" . $params['id_thread'];
				break;
			case "galleries":
			case "gallery_group":
				$path .= $this->ini->Get('gallery_path');
				switch ($params['subtype'])
				{
					case "image":
						$path .= "/gallery.php?id={$params['id_gallery']}&i={$params['id_image']}";
						break;
					case "image_orig":
						$path .= "/image.php?id={$params['id_image']}&f={$params['format']}";
						break;
					case "gallery":
						$path .= "/gallery.php?id={$params['id_gallery']}";
						if($params['page']>0)
							$path .= "&p={$params['page']}";
						break;
					case "slideshow":
						$path .= "/slideshow.php?id={$params['id_gallery']}";
						break;
					default:
						$path .= "/index.php";
						if ($params['id_group']>0)
							$path .=  "?id=" . $params['id_group'];
						break;
				}
				break;
			case "galleries_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("images", "{$params['id']}.{$params['format']}",$params['image_size']);
				} else {
					$path .=  $this->paths['graphic'] . "galleries/{$params['id_gallery']}/{$params['image_size']}/{$params['id']}.{$this->convert_format}";
				}
				break;
			case "gallery_item":
				$path .= $this->paths["gallery"] . "g" . $params['id_gallery'] . "_i" . $params['id'] . ".html";
				break;
			case "gallery_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("images", "{$params['id']}.{$params['format']}",$params['image_size']);
				} else {
					$path .= $path_prefix . "g" . $params['id_gallery'] . "_i" . $params['id'] . "_s" . $params['image_size'] . "." . $this->convert_format;
				}
				break;
			case "gallery_slideshow":
				$path .=  $this->paths['graphic'] . "galleries/{$params['id_gallery']}/images.xml";
				break;
			case "geosearch":
				$path .= $this->ini->Get('search_path') . "/geo.php?id_topic=" . $params['id_topic'];
				if ($params['id']>0)
					$path .=  "&id=" . $params['id'];
				break;
			case "graphic":
				$path .= $path_prefix . $params['id'] . "." . $params['format'];
				break;
			case "homepage":
				switch ($params['subtype'])
				{
					case "mywidgets":
						$path .= "widgets/mywidgets.php";
						if ($params['id']>0)
							$path .=  "?id=" . $params['id'];
						break;
					case "widget_library":
						$path .= "widgets/library.php";
						break;
					case "widget_category":
						$path .= "widgets/category.php";
						if ($params['id']>0)
							$path .=  "?id=" . $params['id'];
						break;
					case "widget_search":
						$path .= "widgets/search.php";
						if ($params['qw']!="")
							$path .=  "?qw=" . $params['qw'];
						break;
					case "widget_content_search":
						$path .= "widgets/search.php?content=1";
						if ($params['qw']!="")
							$path .=  "&qw=" . $params['qw'];
						break;
					case "advance_widget":
						$path .= "widgets/edit.php";
						if ($params['id']>0)
							$path .=  "?id=" . $params['id'];
						if ($params['id_sub']>0)
							$path .=  "&id_sub=" . $params['id_sub'];
						break;
					case "landing":
						$path .= "{$params['landing']}.html";
						break;
					case "keyword":
						$path .= "{$params['keyword']}.html";
						break;
					default:
						$hometype = $this->ini->GetModule("homepage","hometype",0);
						$path .= $hometype>0? "index.php" : "index.html";
				}
				break;
			case "image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("images", "{$params['id']}.{$params['format']}", $params['size']);
				} else {
					$in = ($params['id_article']>0)? "_a" . $params['id_article'] : "";
					$in .= ($params['id_subtopic']>0)? "_s" . $params['id_subtopic']  . "_" . (int)$params['size'] : "";
					$in .= ($params['id_gallery']>0)? "_g" . $params['id_gallery'] : "";
					$in .= ($params['associated'])? "a":"";
					$path .= $path_prefix . $params['id'] . $in . "." . ($params['size']<0?$params['format']:$this->convert_format);
				}
				break;
			case "image_orig":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("images", "{$params['id']}.{$params['format']}");
				} else {
					$path .= $path_prefix . $params['id'] . "." . $params['format'];
				}
				break;
			case "image_zoom":
				$path .= $path_prefix . $params['id_article'] . "_i" . $params['id'] . ".html";
				break;
			case "keyword":
				if($params['k']!="")
					$path .= "tools/keyword.php?k=" . $params['k'];
				break;
			case "lists":
				$path .= $this->ini->Get('lists_path') . "/index.php?";
				switch ($params['subtype'])
				{
					case "group":
						$path .= "id_g=" . $params['id_group'];
						break;
					case "list":
						$path .= "id=" . $params['id_list'];
						break;
					default:
						$path .= "id=0";
						break;
				}
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "map":
				if($params['id']>0)
					$path .= $this->ini->Get('map_path') . "/group_" . $params['id'] . ".html";
				else
					$path .= $this->ini->Get('map_path') . "/index.html";
				break;
			case "meet":
				$path .= "meet/";
				switch ($params['subtype'])
				{
					case "meeting":
						$path .= "meeting.php";
						break;
					case "participation":
						$path .= "participation.php";
						break;
					case "participants":
						$path .= "participants.php";
						break;
					case "subscribe":
						$path .= "subscribe.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				$path .= "?id={$params['id']}";
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "org_doc":
				$path .= $this->paths['docs'] . "orgs/{$params['id']}.{$params['format']}";
				break;
			case "org_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("orgs", "{$params['id']}.jpg", 1);
				} else {
					$path .= $this->paths['graphic'] . "orgs/1/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "org_thumb":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("orgs", "{$params['id']}.jpg", 0);
				} else {
					$path .= $this->paths['graphic'] . "orgs/0/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "org_image_orig":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("orgs", "{$params['id']}.jpg");
				} else {
					$path .= $this->paths['graphic'] . "orgs/orig/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "orgs":
				$path .= $this->ini->Get('org_path') . "/";
				$id_topic = $this->ini->GetModule("orgs","id_topic",0);
				switch ($params['subtype'])
				{
					case "search":
					case "search_advanced":
						$path .= "search.php?id_topic=" . $id_topic;
						if($params['from']=="org_search")
						{
							$search_terms = $params['search_terms'];
							$path .= "&from=org_search";
							if(is_array($search_terms))
							{
								$path .= "&name={$search_terms['name']}&address={$search_terms['address']}&id_assotype={$search_terms['id_assotype']}&id_geo={$search_terms['id_geo']}&id_k={$search_terms['id_k']}";
								foreach($search_terms as $key => $value)
								{
									if (substr($key,0,3)=="kp_" && $value!="")
									{
										$path .= "&$key=$value";
									}
								}
							}
						}
						if($params['subtype']=="search_advanced" || $search_terms['advanced']=="1")
							$path .= "&advanced=1";
						break;
					case "insert":
						$path .= "insert.php?id_topic=" . $id_topic;
						break;
					case "org":
						$path .= "org.php?id=" . $params['id'] . "&id_topic=$id_topic";
						break;
					case "keyword":
						$path .= "keyword.php?id=" . $params['id'] . "&id_topic=$id_topic";
						break;
					case "type":
						$path .= "type.php?id=" . $params['id'] . "&id_topic=$id_topic";
						break;
					default:
						$path .= "index.php?id_topic=" . $id_topic;
						break;
				}
				break;
			case "people":
				$path .= $this->ini->Get('users_path') . "/";
				switch ($params['subtype'])
				{
					case "contact":
						$path .= "mail.php";
						break;
					case "data":
						$path .= "data.php";
						break;
					case "cancel":
						$path .= "cancel.php";
						break;
					case "history":
						$path .= "history.php";
						break;
					case "login":
						$path .= "login.php";
						if ($params['jumpback'])
							$path .= "?jumpback=1";
						break;
					case "logout":
						$path .= "logout.php";
						break;
					case "password":
						$path .= "password.php";
						break;
					case "register":
						$path .= "register.php";
						if ($params['jumpback'])
							$path .= "?jumpback=1";
						break;
					case "reminder":
						$path .= "reminder.php";
						break;
					case "verify":
						$path .= "verify.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				if ($params['id_topic']>0)
					$path .=  ((strpos($path,"?")===false)? "?":"&") . "id_topic=" . $params['id_topic'];
				break;
			case "poll":
				$path .= $this->ini->Get('poll_path') . "/";
				switch ($params['subtype'])
				{
					case "results":
						$path .= "results.php";
						break;
					case "question":
						$path .= "question.php";
						break;
					case "questions_group":
						$path .= "group.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				$path .= "?id={$params['id']}";
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				if ($params['id_question']>0)
					$path .=  "&id_question=" . $params['id_question'];
				if ($params['id_questions_group']>0)
					$path .=  "&id_questions_group=" . $params['id_questions_group'];
				break;
			case "product_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("products", "{$params['id']}.jpg",$params['size']);
				} else {
					$path .= $this->paths['graphic'] . "products/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "quotes":
				$path .= $this->ini->Get('quotes_path') . "/";
				switch ($params['subtype'])
				{
					case "insert":
						$path .= "insert.php";
						break;
					case "list":
						$path .= "browse.php";
						break;
					default:
						$path .= "index.php";
						break;
				}
				$path .=  "?id_topic=" . $params['id_topic'];
				break;
			case "rss":
				$feed_type = $this->ini->Get("feed_type");
				$extension = ($feed_type=="3")? "atom" : "rss";
				switch($params['subtype'])
				{
					case "topic_group":
						$path .=  "feeds/group_" . $params['id'] . ".$extension";
						break;
					case "topic":
						$path .=  "feeds/{$this->topic->path}.$extension";
						break;
					case "subtopic":
						$path .=  "feeds/subtopic_{$params['id']}.$extension";
						break;
					case "events":
						$path .=  "feeds/events.$extension";
						break;
					case "keyword":
						$path .=  "feeds/k_{$params['id']}.$extension";
						break;
					default:
						$path .=  "feeds/main.$extension";
						break;
				}
				break;
			case "search":
				$path .= $this->ini->Get('search_path') . "/index.php?id_topic=" . $params['id_topic'];
				if ($params['q']!="")
					$path .=  "&q=" . $params['q'];
				if ($params['k']!="")
					$path .=  "&k=" . $params['k'];
				if ($params['id_group']>0)
					$path .=  "&id_group=" . $params['id_group'];
				if ($params['id_subtopic']>0)
					$path .=  "&id_subtopic=" . $params['id_subtopic'];
				break;
			case "subtopic":
				$t = new Topic(0);
				$subtopic_types = $t->subtopic_types;
				$furl_id = $this->FriendlyUrlsCacheResourceId(2,$params['id']);
				if(!$params['is_subtopic_page'] && $furl_lookup && is_array($topic_furls) && count($topic_furls)>0 && isset($topic_furls[$furl_id]))
				{
					if($is_global)
						$path .= $this->topic->path;
					$path .= "/" . $topic_furls[$furl_id];
				}
				else
				{
					switch ($params['id_type'])
					{
						case $subtopic_types['url']:
							$path = ($params['link']!="")? $params['link'] : "#";
							break;
						case $subtopic_types['contact']:
							$path = "/tools/form.php?id={$params['id']}&id_topic={$params['id_topic']}";
							if(isset($params['id_doc']) && $params['id_doc']>0)
								$path .= "&id_doc={$params['id_doc']}";
							break;
						case $subtopic_types['campaign']:
							if($params['id_item']>0 && $params['id_subitem']>0)
							{
								$subtypes = $this->Subitems(7,false);
								foreach($subtypes as $subtype)
								if($subtype[0]==$params['id_subitem'])
									$sbtype = $subtype[1];
								$path = $this->PublicPath("campaign",array('subtype'=>$sbtype,'id'=>$params['id_item'],'id_topic'=>$params['id_topic']),true,false);
							}
							else
							{
								$path = $this->PublicPath("campaign",array('subtype'=>"list",'id_topic'=>$params['id_topic']),true,false);
							}
							break;
						case $subtopic_types['poll']:
							if($params['id_item']>0)
							{
								$sbtype = 1;
								$subtypes = $this->Subitems(17,false);
								foreach($subtypes as $subtype)
								if($subtype[0]==$params['id_subitem'])
									$sbtype = $subtype[1];
								$path = $this->PublicPath("poll",array('subtype'=>$sbtype,'id'=>$params['id_item'],'id_topic'=>$params['id_topic']),true,false);
							}
							else
							{
								$path = $this->PublicPath("poll",array('subtype'=>"list",'id_topic'=>$params['id_topic']),true,false);
							}
							break;
						case $subtopic_types['forum']:
							if($params['id_item']>0)
							{
								$sbtype = 1;
								$subtypes = $this->Subitems(8,false);
								foreach($subtypes as $subtype)
								if($subtype[0]==$params['id_subitem'])
									$sbtype = $subtype[1];
								$path = $this->PublicPath("forum",array('subtype'=>$sbtype,'id'=>$params['id_item'],'id_topic'=>$params['id_topic']),true,false);
							}
							else
							{
								$path = $this->PublicPath("forum",array('subtype'=>"list",'id_topic'=>$params['id_topic']),true,false);
							}
							break;
						case $subtopic_types['calendar']:
							$path = "/" . $this->ini->Get('events_path') . "/";
							switch($params['id_item']) // subtype
							{
								case 0:
									$path .= "events.php";
									break;
								case 1:
									$path .= "index.php";
									break;
								case 2:
									$path .= "month.php";
									break;
								case 3:
									$path .= "insert.php";
									break;
								case 4:
									$path .= "search.php";
									break;
							}
							if($params['id_topic']>0)
								$path .= "?id_topic=" . $params['id_topic'];
							elseif(($this->topic->id)>0)
							$path .= "?id_topic=" . $this->topic->id;
							break;
						case $subtopic_types['module']:
							switch($params['id_item']) // id_module
							{
								case "8": // orgs
									if($params['id_subitem']>0)
									{
										$subtypes = $this->SubItems(8);
										foreach($subtypes as $subtype)
										if($subtype[0]==$params['id_subitem'])
											$sbtype = $subtype[1];
										$path = $this->PublicPath("orgs",array('subtype'=>$sbtype,'id_topic'=>$params['id_topic']),true,false);
									}
									else
										$path = "/" . $this->ini->Get('org_path') . "?id_topic=" . $this->ini->GetModule("orgs","id_topic",0);
									break;
								case "11": // lists
									if($params['id_subitem']>0)
										$path = "/" . $this->ini->Get('lists_path') . "/index.php?id={$params['id_subitem']}&id_topic=" . $params['id_topic'];
									break;
								case "16": // books
									$path = "/" . $this->ini->Get('books_path') . "?id_topic=" . $params['id_topic'];
									break;
								case "23": // meet
									$path = "/meet/";
									if($params['id_subitem']>0)
									{
										$subtypes = $this->SubItems(27);
										foreach($subtypes as $subtype)
										if($subtype[0]==$params['id_subitem'])
											$sbtype = $subtype[1];
										$path = $this->PublicPath("meet",array('subtype'=>$sbtype,'id_topic'=>$params['id_topic']),true,false);
									}
									else
										$path .= "index.php?id_topic=" . $params['id_topic'];
									break;
								case "27": // ecommerce
									$path = "/ecomm/";
									if($params['id_subitem']>0)
									{
										$subtypes = $this->SubItems(27);
										foreach($subtypes as $subtype)
										if($subtype[0]==$params['id_subitem'])
											$sbtype = $subtype[1];
										$path = $this->PublicPath("ecommerce",array('subtype'=>$sbtype,'id_topic'=>$params['id_topic']),true,false);
									}
									else
										$path .= "index.php?id_topic=" . $params['id_topic'];
									break;
							}
							break;
						case $subtopic_types['quotes']:
							$path = "/" . $this->ini->Get('quotes_path') . "?id_topic=" . $params['id_topic'];
							break;
						case $subtopic_types['dynamic']:
							$path = "/tools/subtopic.php?id={$params['id']}&id_topic={$params['id_topic']}";
							break;
						default:
							$path .= $path_prefix . $params['id'] . ((isset($params['page']) && $params['page']>0)? "_{$params['page']}":"") . ".html";
					}
				}
				break;
			case "topic_home":
				$path .= "/index.html";
				break;
			case "topic_xml":
				$path .= "/topic_info.xml";
				break;
			case "user":
				if($params['login']!="")
					$path .= "tools/author.php?l=" . $params['login'];
				if($params['t']>0)
					$path .= "tools/author.php?t=" . $params['t'];
				if($params['u']>0)
					$path .= "tools/author.php?u=" . $params['u'];
				if($params['b']>0)
					$path .= "tools/author.php?b=" . $params['b'];
				if($params['c']>0)
					$path .= "tools/author.php?c=" . $params['c'];
				break;
			case "user_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("users", "{$params['id']}.jpg",0);
				} else {
					$path .= $this->paths['graphic'] . "users/0/" . $params['id'] . "." . $this->convert_format;
				}
				break;
			case "media":
				switch($params['subtype'])
				{
					case "home":
						$path .= $this->ini->Get('media_path') ."/index.php?h=1";
						break;
					case "video":
						$path .= $this->ini->Get('media_path') . "/?v={$params['hash']}";
						break;
					case "audio":
						$path .= $this->ini->Get('media_path') . "/?a={$params['hash']}";
						break;
					case "audios":
						$path .= $this->ini->Get('media_path') ."/audios.php?h=1";
						break;
				}
				if ($params['id_topic']>0)
					$path .=  "&id_topic=" . $params['id_topic'];
				break;
			case "video_enc":
				$path .= $this->paths['graphic'] . "videos/f/{$params['hash2']}.flv";
				break;
			case "video_enc_link":
				$path .= $this->ini->Get('media_path') . "/flv.php?h={$params['hash']}";
				break;
			case "video_image":
				$path .= $this->paths['graphic'] . "videos/i/{$params['hash']}.jpg";
				break;
			case "video_thumb":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("videos", "{$params['hash']}.{$this->convert_format}",0);
				} else {
					$path .= $this->paths['graphic'] . "videos/t/{$params['hash']}.{$this->convert_format}";
				}
				break;
			case "video_xml":
				$path .= $this->paths['graphic'] . "videos/x/{$params['hash']}.xml";
				break;
			case "watermark":
				$path .= $this->paths['graphic'] . "videos/watermark.png";
				break;
			case "widget_image":
				if(!$abs_path && $this->cdn!='') {
					$path = $this->CDNURL("widgets", "{$params['id']}.jpg",$params['size']);
				} else {
					$path .= $this->paths['graphic'] . "widgets/" . $params['size'] . "/" . $params['id'] . "." . $this->convert_format;
				}
				break;
		}
		return $path;
	}

	/**
	 * URL of global pages
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param boolean	$furl_lookup	Whether to lookup friendly URLs
	 * @return string				URL
	 */
	public function PublicUrlGlobal($type,$params,$furl_lookup=true)
	{
		if(($type=="subtopic" && $params['id_type']==6 && $params['link']!="") 
			|| ($type=="article" && $params['jump']!="") 
			|| ($this->cdn!='' && in_array($type, $this->assets_types))) {
			$url = "";
		}
		else {
			$url = $this->pub_web;
		}
		$url .= $this->PublicPath($type,$params,true,false,$furl_lookup);
		return  $url;
	}

	/**
	 * URL of specific pages
	 *
	 * @param string	$type		Page type
	 * @param array		$params		Parameters
	 * @param Topic		$topic		Topic
	 * @param boolean	$furl_lookup	Whether to lookup friendly URLs
	 * @return string				URL
	 */
	public function PublicUrlTopic($type,$params,$topic,$furl_lookup=true)
	{
		$this->topic = $topic;
		if($this->protected && $params['protected'])
		{
			$url = $this->ProtectedPath($type,$params,false);
		}
		else
		{
			$url = $this->topic->domain;
			if(($type=="article" && $params['jump']!="") 
				|| ($type=="subtopic" && $params['id_type']==$this->topic->subtopic_types['url'])
				|| ($this->cdn!='' && in_array($type, $this->assets_types)) ) {
				$url = "";
			}
			$url .= $this->PublicPath($type,$params,false,false,$furl_lookup);
		}
		return  $url;

	}

	/**
	 * Delete a specific redirect
	 *
	 * @param integer $id_redirect
	 */
	public function RedirectDelete($id_redirect)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "redirects" );
		$res[] = $db->query( "DELETE FROM redirects WHERE id_redirect=$id_redirect" );
		Db::finish( $res, $db);
	}

	/**
	 * Check if a redirect is already set from a specific path
	 * @param string $path
	 * @return integer		Redirect ID
	 */
	public function RedirectExists($path)
	{
		$exists = 0;
		$redirects = array();
		$this->Redirects( $redirects, false );
		foreach($redirects as $redirect)
		{
			if($redirect['url_from']=="/$path")
			{
				$exists = (int)$redirects['id_redirect'];
			}
		}
		return $exists;
	}

	/**
	 * Get a specific redirect
	 *
	 * @param integer $id_redirect	Redirect ID
	 * @return array
	 */
	public function RedirectGet($id_redirect)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_redirect,id_type,url_from,url_to FROM redirects WHERE id_redirect=$id_redirect");
		return $row;
	}

	/**
	 * Get a specific redirect based on its path
	 *
	 * @param string $url_from
	 * @return array
	 */
	private function RedirectGetByFrom($url_from)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_redirect FROM redirects WHERE BINARY url_from='$url_from'");
		return $row;
	}

	/**
	 * Get all redirects
	 *
	 * @param array $rows		Redirects
	 * @param boolean $paged	Whether results are paginaged or not
	 * @param boolean $desc		Whether to sort in reverse order
	 * @return integer			Number of redirects
	 */
	public function Redirects( &$rows, $paged=true, $desc=false )
	{
		$sqlstr = "SELECT id_redirect,id_type,url_from,url_to FROM redirects ORDER BY url_from";
		if($desc)
			$sqlstr .= " DESC ";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Store a redirect definition
	 *
	 * @param integer 	$id_redirect	Redirect ID
	 * @param integer 	$id_type		Redirect type ID
	 * @param string 	$path			Path to redirect from
	 * @param string	$url			URL to redirect to
	 */
	public function RedirectStore( $id_redirect, $id_type, $path, $url )
	{
		if(substr($path,0,1)!="/")
			$path = "/$path";
		$red = $this->RedirectGetByFrom($path);
		if (($id_redirect=="0" && $red['id_redirect']>0) || ($id_redirect>0 && $red['id_redirect']>0 && $id_redirect!=$red['id_redirect'] ))
		{
			include_once(SERVER_ROOT."/../classes/adminhelper.php");
			$ah = new AdminHelper;
			$ah->MessageSet("redirect_exists",array($path));
		}
		else
		{
			$db =& Db::globaldb();
			$path = $db->SqlQuote($path);
			$db->begin();
			$db->lock( "redirects" );
			if ($id_redirect>0)
				$sqlstr = "UPDATE redirects SET id_type='$id_type',url_from='$path',url_to='$url'
				WHERE id_redirect='$id_redirect' ";
			else
			{
				$id_redirect = $db->nextId( "redirects", "id_redirect" );
				$sqlstr = "INSERT INTO redirects (id_redirect,id_type,url_from,url_to)
				VALUES ($id_redirect,'$id_type','$path','$url')";
			}
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}

	/**
	 * Sub-pagetypes for modules / interal pagetypes
	 *
	 * @param integer $id_subitem	ID of module / pagetype
	 * @param boolean $is_module	Whether is a module or an internal pagetype
	 * @return array
	 */
	public function SubItems($id_subitem,$is_module=true)
	{
		$subitems = array();
		if($is_module)
		{
			switch($id_subitem)
			{
				case "8": //orgs
					$subitems[] = array('0'=>'1','1'=>"home");
					$subitems[] = array('0'=>'2','1'=>"search");
					$subitems[] = array('0'=>'3','1'=>"insert");
					$subitems[] = array('0'=>'4','1'=>"keyword");
					$subitems[] = array('0'=>'5','1'=>"type");
					$subitems[] = array('0'=>'5','1'=>"search_advanced");
					break;
				case "11": //lists
					include_once(SERVER_ROOT."/../modules/lists.php");
					$li = new Lists();
					$topic_lists = $li->Topic($this->id);
					foreach($topic_lists as $topic_list) {
					    $subitems[] = array('0'=>$topic_list['id_list'],'1'=>$topic_list['email']);
					}
					break;
				case "25": //dodc
					$subitems[] = array('0'=>'1','1'=>"home");
					$subitems[] = array('0'=>'2','1'=>"search");
					$subitems[] = array('0'=>'3','1'=>"contractors");
					$subitems[] = array('0'=>'4','1'=>"offices");
					$subitems[] = array('0'=>'5','1'=>"contracts");
					break;
				case "26": //tourop
					$subitems[] = array('0'=>'1','1'=>"home");
					$subitems[] = array('0'=>'2','1'=>"search");
					$subitems[] = array('0'=>'3','1'=>"insert");
					$subitems[] = array('0'=>'4','1'=>"manage");
					$subitems[] = array('0'=>'5','1'=>"workshops");
					$subitems[] = array('0'=>'6','1'=>"agency");
					$subitems[] = array('0'=>'7','1'=>"competitions",'2'=>true);
					break;
				case "27": //ecommerce
					$subitems[] = array('0'=>'1','1'=>"products");
					$subitems[] = array('0'=>'2','1'=>"orders");
					break;
			}
		}
		else // is pagetype
		{
			switch($id_subitem)
			{
				case "7": //campaigns
					$subitems[] = array('0'=>'1','1'=>"info");
					$subitems[] = array('0'=>'2','1'=>"sign_person");
					$subitems[] = array('0'=>'3','1'=>"sign_org");
					$subitems[] = array('0'=>'4','1'=>"signatures_person");
					$subitems[] = array('0'=>'5','1'=>"signatures_org");
					break;
				case "8": //forums
					$subitems[] = array('0'=>'1','1'=>"info");
					$subitems[] = array('0'=>'2','1'=>"thread_insert");
					break;
				case "9": //galleries
					$subitems[] = array('0'=>'1','1'=>"images_list");
					$subitems[] = array('0'=>'2','1'=>"slideshow");
					if($this->highslide)
						$subitems[] = array('0'=>'3','1'=>"zoomable_list");
					break;
				case "10": //events
					$subitems[] = array('0'=>'0','1'=>"next_events");
					$subitems[] = array('0'=>'1','1'=>"today");
					$subitems[] = array('0'=>'2','1'=>"current_month");
					$subitems[] = array('0'=>'3','1'=>"submit_event");
					$subitems[] = array('0'=>'4','1'=>"search");
					break;
				case "17": //polls
					$subitems[] = array('0'=>'1','1'=>"info");
					$subitems[] = array('0'=>'2','1'=>"results");
					break;
			}
		}
		return $subitems;
	}

	/**
	 * Label for initial page
	 *
	 * @param integer $id_subitem	ID of module / pagetype
	 * @param boolean $is_module	Whether is a module or an internal pagetype
	 * @return string
	 */
	public function SubItemsLabel($id_subitem,$is_module=true)
	{
		$label = "initial_page";
		if($is_module)
		{
			switch($id_subitem)
			{
				case "11": //lists
					$label = "mailing_list";
					break;
				default:
					$label = "initial_page";
			}
		}
		return $label;
	}
}
?>
