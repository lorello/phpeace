<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Forum management
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 *
 */
class Forum
{
	/**
	 * @var integer	Forum ID
	 */
	public $id;

	/**
	 * Initialize local variables
	 * 
	 * @param integer $id_forum
	 */
	function __construct($id_forum)
	{
		$this->id = $id_forum;
	}

	/**
	 * All active forums in a topic (paginated)
	 * 
	 * @param array 	$rows		Forums
	 * @param integer	$id_topic	Topic ID
	 * @return integer				Number of forums
	 */
	public function ActiveForums( &$rows, $id_topic )
	{
		$sqlstr = "SELECT tf.id_topic,tf.id_topic_forum,tf.name,UNIX_TIMESTAMP(tf.start_date) AS start_date_ts,tf.description,
		tf.approve_threads,tf.approve_comments,tf.active,'forum' AS item_type ";
		if ($id_topic>0)
			$sqlstr .= " FROM topic_forums tf   
		    	WHERE tf.active>0 AND tf.id_topic=$id_topic ";
		else 
			$sqlstr .= ",t.name AS topic_name 
			 FROM topic_forums tf   
			 INNER JOIN topics t ON tf.id_topic=t.id_topic 
			 WHERE tf.active>0  ";
		$sqlstr .= " GROUP BY tf.id_topic_forum ORDER BY tf.active ASC, tf.start_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Delete current forum and its related threads 
	 */
	public function ForumDelete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("comments","topic_forum_threads"));
		$res[] = $db->query( "DELETE FROM comments WHERE id_type=10 AND id_item IN (SELECT id_thread FROM topic_forum_threads WHERE id_topic_forum='$this->id') " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_forum_threads" );
		$res[] = $db->query( "DELETE FROM topic_forum_threads WHERE id_topic_forum='$this->id' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_forums" );
		$res[] = $db->query( "DELETE FROM topic_forums WHERE id_topic_forum='$this->id' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['forum']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['forum'],$this->id);
	}
	
	/**
	 * Get current forum
	 * 
	 * @return array
	 */
	public function ForumGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_topic_forum,id_topic,name,UNIX_TIMESTAMP(start_date) AS start_date_ts,thanks,
			approve_threads,approve_comments,comments,uploads,source,active,description,users_type
		    FROM topic_forums WHERE id_topic_forum='$this->id' ");
		return $row;
	}

	/**
	 * Store forum data
	 * 
	 * @param integer 	$id_topic			Associated Topic ID
	 * @param date 		$start_date			Start date
	 * @param string 	$name				Forum title
	 * @param string 	$description		Description
	 * @param string 	$keywords			Associated keywords (separated by comma)
	 * @param string 	$thanks				Message after submission
	 * @param integer 	$users_type			Profiling options
	 * @param boolint 	$approve_threads	Whether threads are moderated
	 * @param integer 	$comments			Profiling comment options
	 * @param boolint 	$approve_comments	Whether comments are moderated
	 * @param boolint 	$uploads			Upload file (unused)
	 * @param boolint 	$source				Whether to request source info
	 * @param integer 	$active				Forum status
	 */
	public function ForumStore($id_topic,$start_date,$name,$description,$keywords,$thanks,$users_type,$approve_threads,$comments,$approve_comments,$uploads,$source,$active)
	{
		$prev = $this->ForumGet();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_forums" );
		if($this->id>0)
		{
			$sqlstr = "UPDATE topic_forums
				SET id_topic=$id_topic,start_date='$start_date',name='$name',description='$description',thanks='$thanks',
				users_type='$users_type',approve_threads='$approve_threads',comments='$comments',
				approve_comments='$approve_comments',uploads='$uploads',source='$source',active='$active'
				WHERE id_topic_forum='$this->id' ";
		}
		else 
		{
			$this->id = $db->nextId("topic_forums","id_topic_forum");
			$sqlstr = "INSERT INTO topic_forums (id_topic_forum,name,id_topic,start_date,thanks,approve_threads,approve_comments,comments,uploads,source,active,description,users_type)
					VALUES ($this->id,'$name',$id_topic,'$start_date','$thanks','$approve_threads','$approve_comments','$comments','$uploads','$source','$active','$description','$users_type')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['forum']);
		$this->QueueAdd($id_topic);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($active>0 && $t->visible)
		{
			$s->IndexQueueAdd($o->types['forum'],$this->id,$id_topic,$t->id_group,1);
		}
		elseif($prev['active']>0) 
		{
			$s->ResourceRemove($o->types['forum'],$this->id);
		}
	}

	/**
	 * Search mailjob recipients
	 * 
	 * @param integer $id_topic
	 * @param array $params
	 */
	public function MailjobSearch($id_topic,$params)
	{
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs(2);
		$sqlstr = "SELECT tft.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.email AS mj_email,p.password AS mj_password 
		 FROM topic_forum_threads tft 
		 INNER JOIN people p ON tft.id_p=p.id_p 
 		 INNER JOIN people_topics pt ON pt.id_p=tft.id_p AND pt.id_topic='$id_topic'
			WHERE tft.id_topic_forum='{$this->id}' AND p.active=1 AND pt.contact=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
		if (strlen($params['name'])>0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%') ";
		if (strlen($params['email'])>0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if ($params['id_geo']>0)
			$sqlstr .= " AND p.id_geo='{$params['id_geo']}' ";
		$sqlstr .= " GROUP BY p.id_p ORDER BY p.name1 ";
		return $mj->RecipientsSet($sqlstr,true,true);
	}
	
	/**
	 * Propagate changes to topic queue
	 *  
	 * @param integer $id_topic
	 */
	private function QueueAdd($id_topic)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT s.id_subtopic FROM topic_forums tf
			INNER JOIN subtopics s ON tf.id_topic=s.id_topic AND s.id_type=8 WHERE tf.id_topic_forum='$this->id' ");
		if($row['id_subtopic']>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$t->queue->JobInsert($t->queue->types['subtopic'],$row['id_subtopic'],"update");
		}
	}

	/**
	 * Delete a specific thread from current forum
	 * 
	 * @param integer $id_thread
	 */
	public function ThreadDelete( $id_thread )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "comments" );
		$res[] = $db->query( "DELETE FROM comments WHERE id_type=10 AND id_item='$id_thread' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_forum_threads" );
		$res[] = $db->query( "DELETE FROM topic_forum_threads WHERE id_thread='$id_thread' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_thread,$o->types['thread']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['thread'],$id_thread);
	}

	/**
	 * Retrieve a specific thread from current forum
	 * 
	 * @param integer $id_thread
	 * @return array
	 */
	public function ThreadGet( $id_thread )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT UNIX_TIMESTAMP(insert_date) AS insert_date_ts,title,description,description_long,
		thread_approved,is_html,source,ip,id_topic_forum,id_thread,id_p
		FROM topic_forum_threads
		WHERE id_thread='$id_thread' ");
		return $row;
	}

	/**
	 * Public insertion of thread
	 * 
	 * @param string 	$title				Thread title
	 * @param string 	$description		Thread description
	 * @param string 	$description_long	Thread more detailed description
	 * @param string 	$source				Source info (optional)
	 * @param integer 	$id_p				Portal user ID
	 * @param integer 	$approved			Whether the thread is approved
	 * @return integer						New thread ID
	 */
	public function ThreadInsertPub($title,$description,$description_long,$source,$id_p,$approved)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$db =& Db::globaldb();
		$insert_date = $db->getTodayTime();
		$db->begin();
		$db->lock( "topic_forum_threads" );
		$id_thread = $db->nextId( "topic_forum_threads", "id_thread" );
		$sqlstr = "INSERT INTO topic_forum_threads (id_thread,title,description,description_long,id_topic_forum,insert_date,thread_approved,source,ip,id_p)
			VALUES ($id_thread,'$title','$description','$description_long','$this->id','$insert_date',$approved,'$source','$ip','$id_p')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_thread;
	}
	
	/**
	 * Store thread data
	 * 
	 * @param integer 	$id_thread			Thread ID
	 * @param date 		$insert_date		Insert data
	 * @param string 	$source				Source info
	 * @param string 	$title				Thread title
	 * @param string 	$description		Thread description
	 * @param string 	$description_long	Thread more detailed description
	 * @param boolint 	$is_html			Whether content is HTML
	 * @param boolint 	$thread_approved	Approved status
	 * @param string 	$keywords			Associated keywords
	 * @return integer						Thread ID
	 */
	public function ThreadStore($id_thread,$insert_date,$source,$title,$description,$description_long,$is_html,$thread_approved,$keywords)
	{
		$prev = $this->ThreadGet($id_thread);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_forum_threads" );
		if($id_thread>0)
			$sqlstr = "UPDATE topic_forum_threads
				SET title='$title',description='$description',description_long='$description_long',
				thread_approved=$thread_approved,is_html=$is_html,source='$source',insert_date='$insert_date'
				WHERE id_thread=$id_thread";
		else 
		{
			$id_thread = $db->nextId( "topic_forum_threads", "id_thread" );
			$sqlstr = "INSERT INTO topic_forum_threads (id_thread,title,description,description_long,id_topic_forum,insert_date,thread_approved,source,is_html)
			VALUES ($id_thread,'$title','$description','$description_long','$this->id','$insert_date',$thread_approved,'$source','$is_html')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_thread, $o->types['thread']);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$forum = $this->ForumGet($this->id);
		$t = new Topic($forum['id_topic']);
		if($thread_approved && $forum['active']=="1" && $t->visible)
		{
			$s->IndexQueueAdd($o->types['thread'],$id_thread,$t->id,$t->id_group,1);
		}
		elseif($prev['thread_approved']) 
		{
			$s->ResourceRemove($o->types['thread'],$id_thread);
		}
		return $id_thread;
	}

	/**
	 * All approved threads for current forum (paginated)
	 * 
	 * @param array 	$rows
	 * @param boolint	$approved
	 * @return integer
	 */
	public function ThreadsApprove( &$rows, $approved )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(tft.insert_date) AS insert_date_ts,tft.id_thread,tft.title,tft.description,'thread' AS item_type,
			tft.id_topic_forum,COUNT(id_comment) AS comments
			FROM topic_forum_threads tft
			LEFT JOIN comments c ON c.id_type='10' AND c.id_item=tft.id_thread
			WHERE tft.id_topic_forum='$this->id' AND tft.thread_approved='$approved'
			GROUP BY tft.id_thread
			ORDER BY tft.insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}
?>
