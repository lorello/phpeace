<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * CSS initial content
 * (cannot be empty, otherwise it will freeze some browsers)
 *
 */
define('CSS_EMPTY',"/* CSS */\n\n");

/**
 * CSS management
 * CSS files are stored directly on filesystem, and their latest version is also stored in DB.
 * If versioning is enabled, their previous version is stored in a dedicated table.
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class CssManager
{
	/**
	 * CSS type
	 * (specific, global, ext, custom, module)
	 *
	 * @var string
	 */
	public $type;
	
	/**
	 * Pagetype ID (for specific and global CSS)
	 * Module ID (for module CSS)
	 *
	 * @var integer
	 */
	public $id_pagetype;
	
	/**
	 * The public path where CSS are stored
	 *
	 * @var string
	 */
	public $path;

	/**
	 * DB table used for CSS version changes
	 *
	 * @var string
	 */
	private $table;
	
	/**
	 * If history is enabled
	 *
	 * @var boolint
	 */
	private $versioning;

	/**
	 * Initialize local variables
	 *
	 * @param string $type CSS type (specific, global, ext, custom, module)
	 */
	function __construct($type="specific")
	{
		$this->table = ($type=="specific")? "css" : "css_" . $type;
		$this->type = $type;
		$this->path = "pub/css";
		$ini = new Ini;
		$this->versioning = $ini->GetModule("layout","gra_versioning",1);
	}
	
	/**
	 * Retrieve all CSS IDs for a specific style
	 *
	 * @param integer $id_style
	 * @return array
	 */
	public function CssAll($id_style=0)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_css" . (($this->type!="module")? ",name":",id_module") . " FROM $this->table WHERE id_style=$id_style";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve all CSS for a specific style, including CSS content
	 *
	 * @param integer $id_style
	 * @return array
	 */
	public function CssAllCss($id_style)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_css,css";
		if ($this->type=="specific")
			$sqlstr .= ",id_type";
		if ($this->type=="module")
			$sqlstr .= ",id_module AS id_type";
		if ($this->type=="global")
			$sqlstr .= ",id_css AS id_type";
		$sqlstr .= " FROM $this->table";
		if ($this->type!="global")
			$sqlstr .= " WHERE id_style=$id_style";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Retrieve all generic CSS 
	 * Used only for ext & custom type
	 *
	 * @param array $rows
	 * @return integer		Num of CSS
	 */
	public function CssAllP( &$rows ) //
	{
		$db =& Db::globaldb();
		$rows = array();
		if ($this->type=="ext")
			$sqlstr = "SELECT id_css,name FROM $this->table WHERE id_style=0 ORDER BY name";
		if ($this->type=="custom")
			$sqlstr = "SELECT id_css,c.name,c.id_style,s.name AS style FROM $this->table c
			 LEFT JOIN styles s USING (id_style)  ORDER BY c.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Append content to CSS
	 * Used only for specific type
	 *
	 * @param integer	$id_type		Pagetype ID
	 * @param string	$unescaped_css	Content to append
	 */
	public function CssAppend($id_type,$unescaped_css)
	{
		$this->id_pagetype = $id_type;
		$row = $this->CssGetByType(0);
		$css = $row['css'] . "\n" . $unescaped_css;
		$this->CssStore($row['id_css'],0,"",$css);
	}
	
	/**
	 * Insert a new CSS in all styles
	 *
	 * @param string $name	CSS name
	 * @param string $css	CSS content
	 * @param boolean $global	Whether is a global pagetype
	 * @return integer		ID of new CSS
	 */
	public function CssCreate($name,$css,$global=false)
	{
		if ($this->type=="module")
		{
			$this->CssInsert(0,0,"",CSS_EMPTY);
			$id_css = 0;
		}
		else
			$id_css = $this->CssInsert(0,0,$name,((strlen($css)>0)? $css:CSS_EMPTY));
		if(!$global)
		{
			$styles = $this->CssStyles();
			foreach($styles as $style)
			{
				$this->CssInsert($id_css,$style['id_style'],$name,CSS_EMPTY);
			}
		}
		return $id_css;
	}
	
	/**
	 * Delete CSS of a specific style
	 *
	 * @param integer $id_css
	 * @param integer $id_style
	 */
	public function CssDelete($id_css,$id_style=0)
	{
		$this->CssRemove($id_css,$id_style);
		if ($this->type=="specific" || $this->type=="ext")
		{
			$styles = $this->CssStyles();
			foreach($styles as $style)
			{
				$this->CssRemove($id_css,$style['id_style']);
			}
		}
	}
	
	/**
	 * Delete all CSS of current module
	 * Used by module only
	 *
	 */
	public function CssDeleteAll()
	{
		$css = $this->CssGetByType(0);
		$this->CssRemove($css['id_css'],0);
		foreach($this->CssStyles() as $style)
		{
			$css = $this->CssGetByType($style['id_style']);
			$this->CssRemove($css['id_css'],$style['id_style']);
		}
	}
	
	/**
	 * Delete all CSS of a specific style
	 *
	 * @param integer $id_style
	 */
	public function CssDeleteStyle($id_style)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$res[] = $db->query( "DELETE FROM $this->table WHERE id_style=$id_style" );
		Db::finish( $res, $db);
	}
	
	/**
	 * CSS filename
	 *
	 * @param integer	$id_style
	 * @param integer	$id_css		CSS ID (used for ext and custom type only)
	 * @return string
	 */
	private function CssFilename($id_style,$id_css)
	{
		$filename = $this->type . "_" . $id_css;
		if ($this->type=="specific" || $this->type=="global")
		{
			include_once(SERVER_ROOT."/../classes/pagetypes.php");			
			$pt = new PageTypes();
			$filename = array_search($this->id_pagetype,($this->type=="specific")? $pt->types: $pt->gtypes);
		}
		if ($this->type=="module")
		{
			include_once(SERVER_ROOT."/../classes/modules.php");
			$mod = Modules::ModuleGet($this->id_pagetype);
			$filename = $mod['path'];
		}
		return $this->path . "/$id_style/" . $filename . ".css";
	}
	
	/**
	 * Retrieve a CSS from DB
	 *
	 * @param integer $id_css
	 * @param integer $id_style
	 * @return array
	 */
	public function CssGet($id_css, $id_style)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_css,css" . (($this->type!="global")? ",id_style":"") .
		(($this->type!="specific" && $this->type!="global")? ",name":"") .
		" FROM $this->table WHERE id_css=$id_css" . 
		(($this->type!="global" && $this->type!="custom")? " AND id_style=$id_style":"") ;
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Retrieve CSS of specific pagetype/module and style
	 * Used for specific and module type only
	 *
	 * @param integer $id_style
	 * @return array
	 */
	public function CssGetByType($id_style)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT c.id_css,c.css,s.name FROM $this->table c LEFT JOIN styles s ON c.id_style=s.id_style
			WHERE " .  (($this->type=="specific")? "c.id_type":"c.id_module" )  . "=$this->id_pagetype AND c.id_style=$id_style ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	/**
	 * Retrieve CSS from filesystem
	 *
	 * @param integer $id_css
	 * @param integer $id_style
	 * @return string
	 */
	public function CssGetLocal($id_css,$id_style)
	{
		$filename = $this->CssFilename($id_style,$id_css);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		return $fm->TextFileRead($filename);
	}

	/**
	 * Insert a new CSS
	 *
	 * @param integer	$id_css
	 * @param integer	$id_style
	 * @param string	$name
	 * @param string	$unescaped_css	CSS content (unescaped)
	 * @return integer					ID of new CSS	
	 */
	public function CssInsert($id_css,$id_style,$name,$unescaped_css)
	{
		$db =& Db::globaldb();
		$css = $db->SqlQuote($unescaped_css);
		$db->begin();
		$db->lock( $this->table );
		$id_css_new = ($id_css>0  || $this->type=="global")? $id_css : $db->nextId( $this->table, "id_css" );
		$sqlstr = "INSERT INTO $this->table ";
		if ($this->type=="specific")
			$sqlstr .= "(id_css,id_type,id_style,css) VALUES ($id_css_new,$this->id_pagetype,$id_style,'$css')";
		elseif($this->type=="global")
			$sqlstr .= "(id_css,css) VALUES ($id_css_new,'$css')";
		elseif($this->type=="module")
			$sqlstr .= "(id_css,id_module,id_style,css) VALUES ($id_css_new,$this->id_pagetype,$id_style,'$css')";
		else
			$sqlstr .= "(id_css,id_style,name,css) VALUES ($id_css_new,$id_style,'$name','$css')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->CssWrite($id_style,$id_css_new,$unescaped_css);
		return $id_css_new;
	}

	/**
	 * Remove a CSS from both filesystem and DB
	 *
	 * @param integer $id_css
	 * @param integer $id_style
	 */
	private function CssRemove($id_css,$id_style)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "DELETE FROM $this->table WHERE id_css=$id_css AND id_style=$id_style";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->Delete($this->CssFilename($id_style,$id_css));
		$fm->PostUpdate();
	}
	
	/**
	 * Store a CSS (both filesystem and DB)
	 *
	 * @param 	integer		$id_css
	 * @param 	integer		$id_style
	 * @param 	string		$name
	 * @param 	string		$unescaped_css	CSS content (unescaped)
	 * @return	integer						ID of new CSS
	 */
	public function CssStore($id_css,$id_style,$name,$unescaped_css)
	{
		if($this->versioning)
		{
			$old_css = $this->CssGetLocal($id_css,$id_style);
			$rows = array();
			$revisions = $this->Revisions($rows,$id_css,$id_style);
			if(!$revisions>0)
				$this->CssHistoryAdd($id_css,$id_style,$old_css,"OLD",true);
		}
		$db =& Db::globaldb();
		$css = $db->SqlQuote($unescaped_css);
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET css='$css' ";
		if ($this->type=="custom")
			$sqlstr .= ",id_style=$id_style";
		$sqlstr .= " WHERE id_css='$id_css'";
		if ($this->type=="ext")
			$sqlstr .= " AND id_style=$id_style";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($this->type=="ext" || $this->type=="custom")
			$this->CssStoreName($id_css,$name);
		$this->CssWrite($id_style,$id_css,$unescaped_css);
		if($this->versioning && trim($old_css) != trim($unescaped_css))
			$this->CssHistoryAdd($id_css,$id_style,$unescaped_css,"");
		return $id_css;
	}
	
	/**
	 * Add an old version of CSS to version history
	 *
	 * @param integer 	$id_css
	 * @param integer 	$id_style
	 * @param string 	$new_css	CSS content
	 * @param string 	$tag		Tag to mark this change
	 * @param boolean	$system		If this action is triggered by system or user
	 */
	private function CssHistoryAdd($id_css,$id_style,$new_css,$tag,$system=false)
	{
		if($system)
			$id_user = 0;
		else 
		{
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			$id_user = (int)$session->Get("current_user_id");
		}
		$db =& Db::globaldb();
		$today_time = $db->getTodayTime();
		$content = $db->SqlQuote($new_css);
		$db->begin();
		$db->lock( "css_history" );
		$revision = $db->nextId( "css_history", "revision" );
		$sqlstr = "INSERT INTO css_history (type,id_css,id_pagetype,id_style,revision,change_time,id_user,content,tag) 
			VALUES ('$this->type','$id_css','$this->id_pagetype','$id_style','$revision','$today_time','$id_user','$content','$tag')  ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Update a CSS name
	 * Used for ext and custom type only
	 *
	 * @param integer $id_css
	 * @param string $name
	 */
	private function CssStoreName($id_css,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET name='$name' WHERE id_css=$id_css";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * All styles
	 *
	 * @return array
	 */
	private function CssStyles()
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles;
		return $s->StylesAll();
	}
	
	/**
	 * Update CSS
	 *
	 * @param integer	$id_css
	 * @param integer	$id_style
	 * @param string	$unescaped_css
	 */
	public function CssUpdate($id_css,$id_style,$unescaped_css)
	{
		$db =& Db::globaldb();
		$css = $db->SqlQuote($unescaped_css);
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET css='$css'  WHERE id_css='$id_css' ";
		if ($this->type=="ext")
			$sqlstr .= " AND id_style='$id_style' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->CssWrite($id_style,$id_css,$unescaped_css);
	}
	
	/**
	 * Write a CSS to filesystem
	 *
	 * @param integer $id_style
	 * @param integer $id_css
	 * @param string $css
	 */
	public function CssWrite($id_style,$id_css,$css)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->WritePage($this->CssFilename($id_style,$id_css),$css );
		$fm->PostUpdate();
	}

	/**
	 * Retrieve a specific revision from CSS history
	 *
	 * @param integer $revision
	 * @return array
	 */
	public function RevisionGet($revision)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(change_time) AS change_time_ts,ch.*
		FROM css_history ch 
		WHERE ch.revision='$revision' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	/**
	 * Retrieve all versions of a specific CSS (paginated)
	 *
	 * @param array $rows
	 * @param integer $id_css
	 * @param integer $id_style
	 * @return integer				Num of versions
	 */
	public function Revisions(&$rows,$id_css,$id_style)
	{
		$id_type = (int)$this->id_pagetype;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = " SELECT UNIX_TIMESTAMP(change_time) AS change_time_ts,ch.*
		FROM css_history ch
		WHERE ch.type='$this->type'  AND ch.id_style='$id_style' ";
		if($this->type=="custom" || $this->type=="ext")
			$sqlstr .= " AND ch.id_css='$id_css' ";
		else
			$sqlstr .= " AND ch.id_pagetype='$id_type' ";
		$sqlstr .= " ORDER BY ch.revision DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	/**
	 * Remove all CSS history
	 *
	 */
	public function RevisionsDelete()
	{
		$db =& Db::globaldb();
		$db->query( "TRUNCATE css_history" );
	}

	/**
	 * Create all empty CSS for a new style
	 *
	 * @param integer $id_style
	 */
	public function StyleAdd($id_style)
	{
		$csss = $this->CssAll();
		foreach($csss as $css)
		{
			$insert = true;
			if ($this->type=="module")
			{
				$this->id_pagetype = $css['id_module'];
				include_once(SERVER_ROOT."/../classes/modules.php");
				$mod = Modules::ModuleGet($this->id_pagetype);
				$insert = $mod['global']!="1";
			}
			$id_css = ($this->type=="module")? 0 : $css['id_css'];
			if($insert)
				$this->CssInsert($id_css,$id_style,$css['name'],CSS_EMPTY);
		}
	}

	/**
	 * Create all CSS for a new pagetype
	 *
	 * @param string $type
	 */
	public function TypeAdd($type)
	{
		include_once(SERVER_ROOT."/../classes/pagetypes.php");
		$pt = new PageTypes();
		$id_type = $pt->types[$type];
		$this->id_pagetype = $id_type;
		$this->CssInsert(0,0,"",CSS_EMPTY);
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles;
		$styles = $s->StylesAll();
		foreach($styles as $style)
		{
			$this->CssInsert(0,$style['id_style'],"",CSS_EMPTY);
		}
	}
	
}
?>
