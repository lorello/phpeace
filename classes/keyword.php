<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class Keyword
{
	private $id;
	private $name;
	private $id_parent;
	private $parents = array();
	private $children = array();
	private $public = false;
	private $path;
	private $default_language;

	function __construct()
	{
		$ini = new Ini;
		$this->path = "uploads/keywords";
		$this->default_language = $ini->Get('id_language');
	}

	public function Features()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_feature,f.name,f.id_function,gf.id_type AS gtype,pf.id_type AS stype,pf.id_style,
			s.name AS style_name,f.params 
			FROM features f 
			LEFT JOIN global_features gf ON f.id_feature=gf.id_feature
			LEFT JOIN page_features pf ON f.id_feature=pf.id_feature
			LEFT JOIN styles s ON pf.id_style=s.id_style
			WHERE (f.id_function=2 OR f.id_function=16 OR f.id_function=20) AND f.active=1
			GROUP BY f.id_feature
			ORDER BY gtype DESC,pf.id_style ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function KeywordCheck($keyword,$id_type=1)
	{
		$id_keyword = $this->KeywordGetId($keyword);
		if(!$id_keyword>0)
		{
			$id_keyword = $this->KeywordInsert( $keyword,"",$id_type,array() );
		}
		return array('id_keyword'=>$id_keyword);
	}
	
	public function KeywordDelete($id_keyword)
	{
		$this->KeywordImageRemove($id_keyword);
		$this->UseDelete($id_keyword);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords" );
		$res[] = $db->query( "DELETE FROM keywords WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "DELETE FROM topic_keywords WHERE id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->StatementsDelete($id_keyword);
	}

	public function KeywordGet($id_keyword)
	{
		$row = array();
		$this->id = $id_keyword;
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_keyword,keyword,description,id_type,id_language FROM keywords WHERE id_keyword=$this->id";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function KeywordGetByString($keyword,$keyword_type=0,$is_escaped=true)
	{
		$db =& Db::globaldb();
		if(!$is_escaped)
			$keyword = $db->SqlQuote($keyword);
		$keyword = strtolower(trim($keyword));
		$sqlstr = "SELECT id_keyword,keyword,description,id_type,id_language FROM keywords WHERE keyword='$keyword'";
		if($keyword_type>0)
			$sqlstr .= "  AND id_type=$keyword_type";
		$row = array();
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function KeywordGetId($keyword,$keyword_type=0,$is_escaped=true)
	{
		$db =& Db::globaldb();
		if(!$is_escaped)
			$keyword = $db->SqlQuote($keyword);
		$keyword = strtolower(trim($keyword));
		$sqlstr = "SELECT id_keyword FROM keywords WHERE keyword='$keyword'";
		if($keyword_type>0)
			$sqlstr .= "  AND id_type=$keyword_type";
		$row = array();
		$db->query_single($row, $sqlstr);
		return (int)$row['id_keyword'];
	}

	public function KeywordInsert( $keyword,$description,$id_type,$file )
	{
		$keyword = strtolower(trim($keyword));
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords" );
		$nextId = $db->nextId( "keywords", "id_keyword" );
		$res[] = $db->query( "INSERT INTO keywords (id_keyword,keyword,description,id_type,id_language)
			VALUES ($nextId,'$keyword','$description',$id_type,$this->default_language)" );
		Db::finish( $res, $db);
		if ($file['ok'])
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new Filemanager;
			$fm->MoveUpload($file['temp'], "$this->path/orig/".$nextId."." . $file['ext']);
			$fm->PostUpdate();
		}
		return $nextId;
	}

	private function KeywordImageRemove($id_keyword)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new Filemanager;
		// TODO instead of this, should store the extension in db...
		$fm->Delete("$this->path/orig/$id_keyword".".gif");
		$fm->Delete("$this->path/orig/$id_keyword".".jpg");
		$fm->Delete("$this->path/orig/$id_keyword".".png");
		$fm->PostUpdate();
	}

	public function KeywordUpdate( $id_keyword,$keyword,$description,$id_type,$file,$id_language,$id_old_type )
	{
		if ($file['ok'])
		{
			$this->KeywordImageRemove($id_keyword);
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new Filemanager;
			$fm->MoveUpload($file['temp'], "$this->path/orig/".$id_keyword."." . $file['ext']);
			$fm->PostUpdate();
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords" );
		$res[] = $db->query( "UPDATE keywords SET keyword='$keyword',description='$description',id_type=$id_type,
			id_language=$id_language WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
		if($id_old_type!=$id_type && $id_old_type=="4")
		{
			$db->begin();
			$db->lock( "topic_keywords" );
			$sqlstr = "DELETE FROM topic_keywords WHERE id_keyword='$id_keyword' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}

	public function KeywordUpdateName( $id_keyword,$keyword )
	{
		$keyword = strtolower(trim($keyword));
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords" );
		$res[] = $db->query( "UPDATE keywords SET keyword='$keyword' WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}

	public function KeywordsInternal($id_keyword)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT tk.id_topic,t.name,t.description,tk.id_res_type,tk.role
			FROM topic_keywords tk
			INNER JOIN topics t ON tk.id_topic=t.id_topic 
			WHERE tk.id_keyword='$id_keyword' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ParamDelete($id_kparam)
	{
		$row = $this->ParamGet($id_kparam);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keyword_params" );
		$res[] = $db->query( "DELETE FROM keyword_params WHERE id_keyword_param=$id_kparam" );
		Db::finish( $res, $db);
		$this->ParamsReshuffle($row['id_keyword'],$row['id_type']);
	}

	public function ParamGet($id_kparam)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_keyword_param,id_type,id_keyword,type,label,params,public,index_include,list_include 
			FROM keyword_params WHERE id_keyword_param=$id_kparam";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function ParamMove($id_keyword,$id_type,$id_kparam,$direction)
	{
		$params = $this->Params($id_keyword,$id_type);
		$counter = 1;
		$id_seq_prev = 0;
		if (!($direction==1 && $id_kparam==$params[0]['id_keyword_param']) && !($direction==0 && $id_kparam==$params[sizeof($params)-1]['id_keyword_param']))
		{
			foreach($params as $param)
			{
				if ($param['id_keyword_param']==$id_kparam && $direction==1)
				{
					$this->ParamUpdateSeq( $id_kparam , ($param['seq']-1) );
					$this->ParamUpdateSeq( $id_seq_prev , $param['seq'] );
				}
				if ($id_seq_prev==$id_kparam && $direction==0)
				{
					$this->ParamUpdateSeq( $param['id_keyword_param'] , ($param['seq']-1) );
					$this->ParamUpdateSeq( $id_seq_prev , $param['seq'] );
				}
				$id_seq_prev = $param['id_keyword_param'];
				$counter ++;
			}
		}
	}
	
	private function ParamNextSeq($id_keyword,$id_type)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT MAX(seq) AS nextseq FROM keyword_params WHERE id_keyword=$id_keyword AND id_type=$id_type";
		$db->query_single($row, $sqlstr);
		return (int)$row['nextseq']>0? (int)$row['nextseq'] + 1 : 1;
	}
	
	private function ParamUpdateSeq($id_keyword_param, $seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keyword_params" );
		$res[] = $db->query( "UPDATE keyword_params SET seq=$seq WHERE id_keyword_param='$id_keyword_param'" );
		Db::finish( $res, $db);
	}
	
	public function ParamStore($id_kparam,$id_type,$id_keyword,$label,$type,$params,$public,$index_include=0,$list_include=0)
	{
		if($id_kparam>0)
		{
			$sqlstr = "UPDATE keyword_params SET id_type=$id_type,id_keyword=$id_keyword,
				label='$label',type='$type',params='$params',public='$public',index_include='$index_include',list_include='$list_include'
				 WHERE id_keyword_param=$id_kparam";
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "keyword_params" );
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		else
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "keyword_params" );
			$id_kparam = $db->nextId( "keyword_params", "id_keyword_param" );
			$seq = $this->ParamNextSeq($id_keyword,$id_type);
			$res[] = $db->query( "INSERT INTO keyword_params (id_keyword_param,id_type,id_keyword,label,type,params,public,index_include,list_include,seq)
				VALUES ($id_kparam,$id_type,$id_keyword,'$label','$type','$params','$public','$index_include','$list_include','$seq')" );
			Db::finish( $res, $db);
		}
		return $id_kparam;
	}
	
	public function ParamValue($type,$name,$value)
	{
		switch($type)
		{
			case "date":
				$pvalue = "";
			break;
			case "checkbox":
				$pvalue = ($value=="1")? $name:"";
			break;
			case "geo":
				include_once(SERVER_ROOT."/../classes/geo.php");
				$geo = new Geo();
				$pvalue = $geo->GeoGet($value,0);
			break;
			default:
				$pvalue = $value;
		}
		return $pvalue;
	}
	
	public function Params($id_keyword,$id_type,$only_public=false,$only_index=false,$only_listed=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword_param,id_keyword,label,type,params,public,index_include,seq
		FROM keyword_params
		WHERE id_keyword=$id_keyword AND id_type=$id_type";
		if($only_public)
			$sqlstr .= " AND public=1 ";
		if($only_index)
			$sqlstr .= " AND index_include=1 ";
		if($only_listed)
			$sqlstr .= " AND list_include=1 ";
		$sqlstr .= " ORDER BY seq,id_keyword_param ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ParamsReshuffleAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DISTINCT id_keyword,id_type FROM keyword_params ORDER BY id_keyword";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
			$this->ParamsReshuffle($row['id_keyword'],$row['id_type']);
		return count($rows);
	}
	
	private function ParamsReshuffle($id_keyword,$id_type)
	{
		$params = $this->Params($id_keyword,$id_type);
		$counter = 1;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keyword_params" );
		for($i=0;$i<count($params);$i++)
		{
			$res[] = $db->query( "UPDATE keyword_params SET seq=$counter WHERE id_keyword_param='" . $params[$i]['id_keyword_param'] . "'");
			$counter ++;
		}
		Db::finish( $res, $db);
	}
	
	public function ParamsTypes($include_wysiwyg=false)
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		return $r->ParamsTypes($include_wysiwyg);
	}
	
	public function Relations()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_statement,k1.keyword,relations.name,k2.keyword AS `range`
		FROM statements
		INNER JOIN relations USING (id_relation)
		INNER JOIN keywords k1 ON statements.id_domain=k1.id_keyword
		INNER JOIN keywords k2 ON statements.id_range=k2.id_keyword
		WHERE id_domain=$this->id OR id_range=$this->id";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	private function StatementsDelete($id_keyword)
	{
		if($id_keyword>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "statements" );
			$sqlstr = "DELETE FROM statements WHERE id_range=$id_keyword OR id_domain=$id_keyword";
			$res[] = $db->query($sqlstr);
			Db::finish( $res, $db);
		}
	}
	
	public function Types()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DISTINCT id_type FROM keywords_use WHERE id_keyword=$this->id ORDER BY keywords_use.id_type";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TypeUse( $id_keyword, $id_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		$cond = "";
		switch ($id_type)
		{
			case 1:
				$sqlstr = "SELECT t.name,t.path,t.description,COUNT(a.id_article) AS counter,t.id_topic
				FROM keywords_use ku
				INNER JOIN topics t ON ku.id=t.id_topic
				LEFT JOIN articles a ON t.id_topic=a.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				GROUP BY t.id_topic ORDER BY counter DESC ";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 2:
				$sqlstr = "SELECT t.name AS topic_name,s.name,t.path,s.id_subtopic,COUNT(a.id_article) AS counter,t.id_topic
				FROM keywords_use ku
				INNER JOIN subtopics s ON ku.id=s.id_subtopic
				INNER JOIN topics t USING(id_topic)
				LEFT JOIN articles a ON a.id_subtopic=s.id_subtopic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				GROUP BY s.id_subtopic ORDER BY counter DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 3:
				$sqlstr = "SELECT g.id_gallery,g.title
				FROM keywords_use ku
				INNER JOIN galleries g ON ku.id=g.id_gallery
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY g.released DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 4:
				$sqlstr = "SELECT COUNT(ku.id) AS counter
				FROM keywords_use ku
				INNER JOIN ass a ON ku.id=a.id_ass
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 5:
				$sqlstr = "SELECT t.name,a.headline,a.id_article,t.path,t.id_topic
				FROM keywords_use ku
				INNER JOIN articles a ON ku.id=a.id_article
				INNER JOIN topics t ON a.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY t.name,a.written DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 6:
				$sqlstr = "SELECT t.name,d.title,da.id_article,t.path,da.id_doc
				FROM keywords_use ku
				INNER JOIN docs_articles da ON ku.id=da.id_doc
				INNER JOIN docs d ON da.id_doc=d.id_doc
				INNER JOIN articles a ON da.id_article=a.id_article
				INNER JOIN topics t ON a.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY t.name, a.written DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 7:
				$sqlstr = "SELECT t.name,a.headline,ia.id_article,t.path,'article' AS type,ia.id_image AS id_image
				FROM keywords_use ku
				INNER JOIN images_articles ia ON ku.id=ia.id_image
				INNER JOIN articles a ON ia.id_article=a.id_article
				INNER JOIN topics t ON a.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type 
				ORDER BY t.name, a.written DESC";
				$db->QueryExe($rows, $sqlstr);
				$sqlstr = "SELECT g.title,g.id_gallery,ku.id AS id_image,'gallery' AS type
				FROM keywords_use ku
				LEFT JOIN images_galleries ig ON ku.id=ig.id_image
				LEFT JOIN galleries g ON ig.id_gallery=g.id_gallery
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type 
				ORDER BY g.released DESC";
				$db->array_query_append($rows, $sqlstr, count($rows));
			break;
			case 8:
				$sqlstr = "SELECT e.id_event,e.title,UNIX_TIMESTAMP(start_date) AS start_date_ts
				FROM keywords_use ku
				INNER JOIN events e ON ku.id=e.id_event
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY e.start_date DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 9:
				$sqlstr = "SELECT f.name,f.id_topic,f.id_topic_forum,t.name AS topic_name
				FROM keywords_use ku
				INNER JOIN topic_forums f ON ku.id=f.id_topic_forum
				INNER JOIN topics t ON f.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY t.id_topic, f.start_date DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 10:
				$sqlstr = "SELECT ft.title,ft.id_thread,f.name,f.id_topic_forum,t.name AS topic_name,t.id_topic
				FROM keywords_use ku
				INNER JOIN topic_forum_threads ft ON ku.id=ft.id_thread
				INNER JOIN topic_forums f ON ft.id_topic_forum=f.id_topic_forum
				INNER JOIN topics t ON f.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY t.id_topic, ft.insert_date DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 11:
				$sqlstr = "SELECT c.name,c.id_topic,c.id_topic_campaign,t.name AS topic_name
				FROM keywords_use ku
				INNER JOIN topic_campaigns c ON ku.id=c.id_topic_campaign
				INNER JOIN topics t ON c.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				ORDER BY t.id_topic, c.start_date DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 12:
				$sqlstr = "SELECT q.quote,q.author,q.notes,q.id_quote
				FROM keywords_use ku
				INNER JOIN quotes q ON ku.id=q.id_quote
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 13:
				$sqlstr = "SELECT b.author,b.title,b.id_book
				FROM keywords_use ku
				INNER JOIN books b ON ku.id=b.id_book
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 14:
				$sqlstr = "SELECT t.name AS topic_name,l.title,l.url,l.id_link,t.id_topic
				FROM keywords_use ku
				INNER JOIN links l ON ku.id=l.id_link
				INNER JOIN links_subtopics ls ON l.id_link=ls.id_link
				INNER JOIN topics t ON ls.id_topic=t.id_topic
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond
				GROUP BY l.id_link ORDER BY l.insert_date DESC";
				$db->QueryExe($rows, $sqlstr);
			break;
			case 15:
				$sqlstr = "SELECT r_day,r_month,r_year,re.description,re.id_event
				FROM keywords_use ku
				INNER JOIN recurring_events re ON ku.id=re.id_event
				WHERE ku.id_keyword=$id_keyword AND ku.id_type=$id_type $cond";
				$db->QueryExe($rows, $sqlstr);
			break;
		}
		return $rows;
	}

	public function UseArticles( $id_keyword,$id_topic,$limit,$sort_by=1,$exclude_keywords=array(),$year=0, $id_subtopic=0 )
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$id_type = $o->types['article'];
		$db =& Db::globaldb();
		$rows = array();
		$wheres = "";
		$sqlstr = "SELECT a.id_article,a.halftitle,a.headline,ah.subhead,UNIX_TIMESTAMP(a.published) AS published_ts,
			UNIX_TIMESTAMP(written) AS written_ts,'article' AS item_type,a.show_author,a.author,a.id_subtopic,a.id_topic,
			a.id_user,a.available,a.show_date,a.original,a.show_source,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
			FROM keywords_use ku
			INNER JOIN articles a ON ku.id=a.id_article
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic ";
		$sqlstr .= ($id_topic>0)? " AND a.id_topic=$id_topic " : " AND t.visible=1 INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1 ";
		for ($i = 0; $i < count($exclude_keywords); $i++)
		{			
			$sqlstr .= " LEFT JOIN keywords_use ku$i ON ku$i.id=a.id_article AND ku$i.id_type='$id_type' AND ku$i.id_keyword={$exclude_keywords[$i]} AND ku$i.id IS NULL ";
		}
		$sqlstr .= " WHERE ku.id_keyword='$id_keyword' " . $wheres;
		if($year>0 && is_numeric($year))
			$sqlstr .= " AND YEAR(a.written)=$year ";
		if($id_subtopic>0)
			$sqlstr .= " AND a.id_subtopic=$id_subtopic ";
		$sqlstr .= " AND ku.id_type='$id_type'
			AND a.approved=1 AND a.published>0 ORDER BY ";
		switch($sort_by)
		{
			case "0":
			case "6":
				$sqlstr .= "a.published DESC,";
			break;
			case "1":
				$sqlstr .= "a.written DESC,";
			break;
			case "2":
				$sqlstr .= "a.id_visibility DESC,a.published DESC,";
			break;
			case "3":
				$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
			break;
			case "4":
				$sqlstr .= "a.original DESC,a.written DESC,";
			break;
			case "5":
				$sqlstr .= "a.headline ASC,";
			break;
		}
			$sqlstr .= "a.id_article DESC ";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		if($sort_by=="6" && count($rows)>1)
			shuffle($rows);
		return $rows;
	}
	
	public function UseArticlesByUser( $id_keyword,$id_user)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline
			FROM keywords_use ku
			INNER JOIN articles a ON ku.id=a.id_article
			WHERE ku.id_keyword='$id_keyword' AND a.approved=1 AND a.id_user=$id_user AND a.published>0 
			ORDER BY a.published DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UseArticlesPaged( &$rows,$id_keyword,$id_template,$id_topic,$id_subtopic=0)
	{
		if(is_null($id_template))
			$id_template = -1;
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.halftitle,a.headline,ah.subhead,
			UNIX_TIMESTAMP(written) AS written_ts,'article' AS item_type,a.show_author,a.author,a.id_subtopic,a.id_topic,
			a.id_user,a.available,a.show_date,a.original,a.show_source
			FROM keywords_use ku
			INNER JOIN articles a ON ku.id=a.id_article
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic ";
		$sqlstr .= ($id_topic>0)? " AND a.id_topic=$id_topic " : " AND t.visible=1 INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1 ";
		$sqlstr .= " WHERE ku.id_keyword='$id_keyword' AND ku.id_type='" . $o->types['article'] . "'
			AND a.approved=1 AND a.published>0 ";
		if($id_template>=0)
			$sqlstr .= " AND a.id_template=$id_template ";
		if($id_subtopic>0)
			$sqlstr .= " AND a.id_subtopic=$id_subtopic ";
		$sqlstr .= " ORDER BY a.written DESC,a.id_article DESC ";
		return $db->QueryExe($rows, $sqlstr,true);
	}
	
	public function UseArticlesGeo( &$rows,$unescaped_words,$id_topic=0,$time_weight=0 )
	{
		$db =& Db::globaldb();
		$words = preg_split('/\W+/', $db->SqlQuote($unescaped_words), -1, PREG_SPLIT_NO_EMPTY);
		$sqljoin = "";
		for ($i = 0; $i < count($words); $i++)
		{
			$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = a.id_article AND si$i.id_res=5 ";
			if($id_topic>0)
				$sqljoin .= " AND si$i.id_topic='$id_topic' ";
			$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
			$wheres[] = "sw$i.word = '$words[$i]'";
			$scores[] = "SUM(si$i.score)";
		}
		$sqlstr = "SELECT a.id_article,a.halftitle,a.headline,ah.subhead,
			UNIX_TIMESTAMP(a.written) AS written_ts,'article' AS item_type,show_author,author,a.id_subtopic,a.id_topic,
			a.id_user,a.available,a.show_date,a.original,a.show_source "; 
		if(count($scores)>0)
			$sqlstr .= ", ((" . implode("+",$scores) . ") ";
		if($time_weight>0)
			$sqlstr .= " - DATEDIFF(CURDATE(),a.written)*$time_weight ";
		$sqlstr .= " ) AS total_score ";
		$sqlstr .= "	FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article $sqljoin ";
			$sqlstr .= " WHERE " . implode(" AND ", $wheres);
			$sqlstr .= " GROUP BY a.id_article ORDER BY  ";
			if(count($scores)>0)
				$sqlstr .= " total_score DESC, ";
			$sqlstr .= " a.written DESC";
		return $db->QueryExe($rows, $sqlstr,true);
	}

	public function UseArticlesMultipleAnd( $keywords,$id_topic,$id_group,$limit,$sort_by,$year)
	{
		$rows = array();
		if(count($keywords)>0)
		{
			$db =& Db::globaldb();
			$sqljoin = "";
			for ($i = 0; $i < count($keywords); $i++)
			{
				$sqljoin .= "INNER JOIN keywords_use ku$i ON ku$i.id = a.id_article AND ku$i.id_type=5 ";
				$wheres[] = "ku$i.id_keyword = '$keywords[$i]'";
			}
			$sqlstr = "SELECT DISTINCT a.id_article,a.halftitle,a.headline,ah.subhead,
				UNIX_TIMESTAMP(written) AS written_ts,'article' AS item_type,a.show_author,a.author,a.id_subtopic,a.id_topic,
				a.id_user,a.available,a.show_date,a.original,a.show_source,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
				FROM articles a
				INNER JOIN articles_subhead ah ON a.id_article=ah.id_article $sqljoin
				INNER JOIN topics t ON a.id_topic=t.id_topic ";
			$sqlstr .= ($id_topic>0)? " AND a.id_topic=$id_topic " : " AND t.visible=1 INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1 ";
			$sqlstr .= " WHERE a.approved=1 AND a.published>0 ";
			if($year>0 && is_numeric($year))
				$sqlstr .= " AND YEAR(a.written)=$year ";
			if($id_group>0)
				$sqlstr .= " AND t.id_group='$id_group' ";
			if(count($wheres)>0)
				$sqlstr .= " AND " . implode(" AND ", $wheres);
			$sqlstr .= " GROUP BY a.id_article ORDER BY ";
			switch($sort_by)
			{
				case "0":
					$sqlstr .= "a.published DESC,";
				break;
				case "1":
					$sqlstr .= "a.written DESC,";
				break;
				case "2":
					$sqlstr .= "a.id_visibility DESC,a.published DESC,";
				break;
				case "3":
					$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
				break;
				case "4":
					$sqlstr .= "a.original DESC,a.written DESC,";
				break;
				case "5":
					$sqlstr .= "a.headline ASC,";
				break;
			}
			$sqlstr .= "a.id_article DESC ";
			if($limit>0)
				$sqlstr .= " LIMIT $limit";
			$db->QueryExe($rows, $sqlstr);
		}
		return $rows;
	}

	public function UseArticlesMultipleOr( $keywords,$id_topic,$id_group,$limit,$sort_by,$id_res,$id,$exclude_id_group=0)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DISTINCT a.id_article,a.halftitle,a.headline,ah.subhead,
			UNIX_TIMESTAMP(written) AS written_ts,'article' AS item_type,a.show_author,a.author,a.id_subtopic,a.id_topic,
			a.id_user,a.available,a.show_date,a.original,a.show_source,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
			FROM keywords_use ku
			INNER JOIN articles a ON ku.id=a.id_article
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic ";
		$sqlstr .= ($id_topic>0)? " AND a.id_topic=$id_topic " : " AND t.visible=1 INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1 ";
		$sqlstr .= " WHERE ku.id_type='$id_res'	AND a.approved=1 AND a.published>0 ";
		if($id>0)
			$sqlstr .= " AND a.id_article<>$id ";
		if($id_group>0)
			$sqlstr .= " AND t.id_group='$id_group' ";
		if($exclude_id_group>0)
		    $sqlstr .= "AND t.id_group<>'$exclude_id_group' ";
		$sqlstr .= " AND (ku.id_keyword='" . implode("' OR ku.id_keyword='",$keywords) . "') ORDER BY ";
		switch($sort_by)
		{
			case "0":
				$sqlstr .= "a.published DESC,";
			break;
			case "1":
				$sqlstr .= "a.written DESC,";
			break;
			case "2":
				$sqlstr .= "a.id_visibility DESC,a.published DESC,";
			break;
			case "3":
				$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
			break;
			case "4":
				$sqlstr .= "a.original DESC,a.written DESC,";
			break;
			case "5":
				$sqlstr .= "a.headline ASC,";
			break;
		}
		$sqlstr .= "a.id_article DESC ";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UseBooks( $id_keyword,$limit,$year=0,$random=false )
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,b.p_year,b.p_month,b.summary,b.catalog,b.price,
		b.cover_format,'book' AS item_type,b.description,b.id_publisher,b.id_category
		FROM keywords_use ku
		INNER JOIN books b ON ku.id=b.id_book
		LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
		WHERE ku.id_keyword=$id_keyword AND ku.id_type=".$o->types['book'] . " AND b.approved=1 ";
		if($year>0 && is_numeric($year))
			$sqlstr .= " AND b.p_year=$year ";
		if(!$random)
			$sqlstr .= " ORDER BY b.p_year DESC, b.p_month DESC, b.id_book DESC ";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$num = $db->QueryExe($rows, $sqlstr);
		if($random && $num>1)
			shuffle($rows);
		return $rows;
	}
	
	public function UseLinks($id_keyword,$id_topic,$limit)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT l.id_link,l.url,l.title,l.description,l.vote,l.id_language,'link' AS item_type
				FROM keywords_use ku
				INNER JOIN links l ON ku.id=l.id_link AND ku.id_type=14 ";
		if($id_topic>0)
			$sqlstr .= " INNER JOIN links_subtopics ls ON l.id_link=ls.id_link AND ls.id_topic=$id_topic ";
		$sqlstr .= "WHERE l.approved=1 AND ku.id_keyword=$id_keyword AND l.error404=0 
				ORDER BY l.title";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UseOthersGeo( $id_keyword )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT DISTINCT id_type,COUNT(id) AS items FROM keywords_use WHERE id_keyword=$id_keyword GROUP BY id_type ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UseTexts($id_keyword,$id_topic,$limit)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT ct.id_content,ct.description,ct.content,ct.id_topic,ct.id_type,'text' AS item_type
				FROM keywords_use ku
				INNER JOIN content_texts ct ON ku.id=ct.id_content AND ku.id_type=21  
				WHERE ku.id_keyword=$id_keyword ";
		if($id_topic>0)
			$sqlstr .= " AND ct.id_topic=$id_topic ";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UseOrg( $id_keyword,$limit )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.*,'asso' AS item_type,{$join['name']} AS geo_name
		FROM keywords_use ku
		INNER JOIN ass a ON ku.id=a.id_ass
		{$join['join']}
		WHERE ku.id_keyword=$id_keyword AND ku.id_type=".$o->types['org'] . " AND a.approved=1
		ORDER BY a.nome ";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UseCount($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_keyword) AS count_keywords FROM keywords_use WHERE id_keyword=$id_keyword";
		$db->query_single($row, $sqlstr);
		return $row['count_keywords'];
	}

	public function UseDelete($id_keyword,$keyword_type=0)
	{
		$sqlstr = "DELETE FROM keywords_use WHERE id_keyword=$id_keyword";
		if($keyword_type>0)
			$sqlstr .= " AND id_type=$keyword_type";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_use" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

}
?>
