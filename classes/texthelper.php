<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
define('STRING_CHUNK',60000);

class TextHelper
{
	public $types;
	
	private $min_str_length;
	private $iconv;
	private $search_query_store;
	
	function __construct()
	{
		$this->types = array('custom'	=> 0,
			'page_header'	=> 1,
			'page_footer'	=> 2,
			'menu_footer'	=> 3,
			'home_header'	=> 4,
			'home_footer'	=> 5,
			'privacy'	=> 6
			);
		$conf = new Configuration();
		$this->min_str_length = $conf->Get("min_str_length");
		$this->search_query_store = $conf->Get("search_query_store");
		$this->iconv = function_exists("iconv");
	}

	public function AdminContentGet($id_text)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT title,description,active FROM admin_text WHERE id_text='$id_text' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function AdminContentUpdate( $id_text, $title, $description, $active )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "admin_text" );
		$sqlstr = "REPLACE INTO admin_text (id_text,title,description,active) VALUES ($id_text,'$title','$description',$active)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function ArrayMulti2StringIndex( $values, $index, $separator=", " )
	{
	    $string = "";
	    if (is_array($values))
	    {
	        $counter = 0;
	        foreach($values as $value)
	        {
	            if ($counter>0)
	                $string .= $separator;
	                $string .= $value[$index];
	                $counter ++;
	        }
	    }
	    return $string;
	}
	
	public function BlockedWordsGet()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		return $fm->TextFileRead("uploads/blockedwords.txt");
	}

	public function BlockedWordsGetArray()
	{
		$blocked_words = array();
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$blocked_words = $fm->TextFileLines("uploads/blockedwords.txt");
		array_walk($blocked_words,array($this,"BlockedWordsTrim"));
		$blocked_words = array_filter($blocked_words);
		return $blocked_words;
	}

	public function BlockedWordsSet($words)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->WritePage("uploads/blockedwords.txt",$words);
		$fm->PostUpdate();
	}

	private function BlockedWordsTrim(&$value)
	{
		$value = trim($value);
	}

	public function ContainsBlockedWord($text)
	{
		$contains = false;
		$blocked_words = $this->BlockedWordsGetArray();
		for($i=0; $i<count($blocked_words); $i++)
		{
			if($this->IsUnicode($blocked_words[$i]))
			{
				if (preg_match('/'.$blocked_words[$i].'/i', $text))
				{
					$contains = true;
				}
			}
			else
			{
				if (preg_match('/\b'.$blocked_words[$i].'\b/i', $text))
				{
					$contains = true;
				}
			}
		}
		return $contains;
	}
	
	public function BlockedWords($text)
	{
		$contains = array();
		$blocked_words = $this->BlockedWordsGetArray();
		for($i=0; $i<count($blocked_words); $i++)
		{
			if($this->IsUnicode($blocked_words[$i]))
			{
				if (preg_match('/'.$blocked_words[$i].'/i', $text))
				{
					$contains[] = $blocked_words[$i];
				}
			}
			else
			{
				if (preg_match('/\b'.$blocked_words[$i].'\b/i', $text))
				{
					$contains[] = $blocked_words[$i];
				}
			}
		}
		return $contains;
	}
	
	public function ContentDelete($id_content,$id_type=0)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "content_texts" );
		$res[] = $db->query( "DELETE FROM content_texts WHERE id_content='$id_content' " );
		Db::finish( $res, $db);
		if ($id_type>0)
			$this->ContentUpdatePropagate($id_content,$id_type,0);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_content,$o->types['text']);
	}
	
	public function ContentGet($id_content)
	{
		$row = array();
		if($id_content>0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT id_content,description,content,id_topic,id_type,is_html,wysiwyg FROM content_texts WHERE id_content='$id_content' ";
			$db->query_single($row, $sqlstr);
		}
		return $row;
	}

	public function ContentGetByType($id_type,$id_topic)
	{
		$row = array();
		if(isset($id_topic))
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT id_content,description,content,id_topic,id_type,is_html,wysiwyg FROM content_texts
			 WHERE id_topic=$id_topic AND id_type=".$id_type;
			$db->query_single($row, $sqlstr);
		}
		return $row;
	}

	public function ContentInsert( $description, $content, $id_topic, $id_type, $keywords,$is_html,$keywords_internal,$wysiwyg )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "content_texts" );
		$id_content = $db->nextId( "content_texts", "id_content" );
		$sqlstr = "INSERT INTO content_texts (id_content,description,content,id_topic,id_type,is_html,wysiwyg)
				VALUES ($id_content,'$description','$content',$id_topic,$id_type,$is_html,$wysiwyg)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->ContentUpdatePropagate($id_content,$id_type,$id_topic);
		if(!$id_type>0)
			$this->ContentKeywords($id_content,$keywords,$keywords_internal);
	}
	
	private function ContentKeywords($id_content,$keywords,$keywords_internal)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_content, $o->types['text']);
		$o->InsertKeywordsArray($keywords_internal,$id_content,$o->types['text']);
	}
	
	public function ContentStore( $description, $content, $id_topic, $id_type, $old_id_type, $keywords,$is_html,$keywords_internal,$wysiwyg)
	{
		$row1 = $this->ContentGetByType($id_type,$id_topic);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "content_texts" );
		if ($row1['id_content']>0)
		{
			$id_content = $row1['id_content'];
			$sqlstr = "UPDATE content_texts SET content='$content',description='$description',is_html='$is_html',wysiwyg='$wysiwyg' 
				WHERE id_content='$id_content' ";
		}
		else
		{
			$id_content = $db->nextId( "content_texts", "id_content" );
			$sqlstr = "INSERT INTO content_texts (id_content,description,content,id_topic,id_type,is_html,wysiwyg)
					VALUES ($id_content,'$description','$content',$id_topic,$id_type,$is_html,$wysiwyg)";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($db->SqlQuote($row1['content'])!=$content || $id_type!=$old_id_type)
			$this->ContentUpdatePropagate($id_content,$id_type,$id_topic);
		if ($id_type!=$old_id_type)
		{
			$row2 = $this->ContentGetByType($old_id_type,$id_topic);
			if ($row2['id_content']>0)
			{
				$this->ContentDelete($row2['id_content'],$old_id_type);
			}
			$this->ContentUpdatePropagate($id_content,$old_id_type,$id_topic);
		}
		if(!$id_type>0)
			$this->ContentKeywords($id_content,$keywords,$keywords_internal);
	}
	
	public function ContentUpdate( $id_content, $description, $content, $id_topic, $id_type, $old_id_type, $keywords,$is_html,$keywords_internal,$wysiwyg )
	{
		$row = $this->ContentGet($id_content);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "content_texts" );
		$sqlstr = "UPDATE content_texts SET content='$content',
			description='$description',id_topic=$id_topic,id_type=$id_type,is_html='$is_html',wysiwyg='$wysiwyg' 
			WHERE id_content=$id_content";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($db->SqlQuote($row['content'])!=$content || $id_type!=$row['id_type'])
			$this->ContentUpdatePropagate($id_content,$id_type,$id_topic);
		if ($id_type!=$old_id_type)
			$this->ContentUpdatePropagate($id_content,$row['id_type'],$id_topic);
		if(!$id_type>0)
			$this->ContentKeywords($id_content,$keywords,$keywords_internal);
	}

	private function ContentUpdatePropagate($id_content,$id_type,$id_topic)
	{
		if ($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			if ($id_type==$this->types['home_header'] || $id_type==$this->types['home_footer'])
			{
				$t->queue->JobInsert($t->queue->types['topic_home'],$t->id,"");
			}
			if ($id_type==$this->types['page_header'] || $id_type==$this->types['page_footer'] || $id_type==$this->types['menu_footer'])
			{
				$t->queue->JobInsert($t->queue->types['all'],$t->id,"");
			}
		}
		if ($id_type==$this->types['custom'])
		{
			include_once(SERVER_ROOT."/../classes/pagetypes.php");
			$pt = new PageTypes();
			$features = $pt->ft->FeatureGetByFunction(12);
			foreach($features as $feature)
			{
				$params = $pt->ft->ParamsDeserialize($feature['params']);
				if($params['id_content']==$id_content)
					$pt->UpdatePropagateWithParams($feature['id_feature'],$params);
			}
		}
	}

	public function Contents( &$rows, $id_topic )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_content,description,content,id_topic,id_type 
		FROM content_texts 
		WHERE id_topic='" . (int)$id_topic . "'
		ORDER BY id_content ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContentsAll($id_topic)
	{
		$db =& Db::globaldb();
		$rows = array();
		if($id_topic>0)
			$sqlstr = "SELECT id_content,description FROM content_texts WHERE id_topic='$id_topic' ORDER BY description "; 
		else 
			$sqlstr = "SELECT ct.id_content,IF(ct.id_topic>0,CONCAT(t.name,': ',ct.description),ct.description) 
				FROM content_texts ct 
				LEFT JOIN topics t ON ct.id_topic=t.id_topic 
				ORDER BY t.name,ct.description "; 
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
    
	public function CSVDump($filename,$headers,$data,$separator=",",$convert_utf_for_excel=false,$http_header=true)
	{
		array_walk($headers,array($this,'CSVCleanText'));
		$output = implode($separator,$headers) . "\r\n";
		foreach($data as $row)
		{
			array_walk($row,array($this,'CSVCleanText'));
			$output .= implode($separator,$row) . "\r\n";
		}
		if($convert_utf_for_excel)
			$output = chr(255) . chr(254) . mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
        if ($http_header)
        {
            header('Content-type: text/csv; charset=utf-8'); 
            header('Content-Type: application/force-download');
            header("Content-Disposition: attachment; filename=\"$filename.csv\"");
            header("Content-Length: " . strlen($output) );
        }
		return $output;
	}
	
	private function CSVCleanText(&$val)
	{
		$val = str_replace("\"", "\"\"",$val);
		$val = preg_replace("/[\n\r]/"," ",$val);
		$val = "\"" . trim($val) . "\"";
	}
	
    public function Html2Text( &$text, $trim=false )
    {
        $search = array(
            "/\r/",                                  // Non-legal carriage return
            "/[\n\t]+/",                             // Newlines and tabs
            '/[ ]{2,}/',                             // Runs of spaces, pre-handling
            '/<script[^>]*>.*?<\/script>/i',         // <script>s -- which strip_tags supposedly has problems with
            '/<style[^>]*>.*?<\/style>/i',           // <style>s -- which strip_tags supposedly has problems with
            //'/<!-- .* -->/',                         // Comments -- which strip_tags might have problem a with
            '/<h[123][^>]*>(.*?)<\/h[123]>/i',      // H1 - H3
            '/<h[456][^>]*>(.*?)<\/h[456]>/i',      // H4 - H6
            '/<p[^>]*>/i',                           // <P>
            '/<br[^>]*>/i',                          // <br>
            '/<b[^>]*>(.*?)<\/b>/i',                // <b>
            '/<strong[^>]*>(.*?)<\/strong>/i',      // <strong>
            '/<i[^>]*>(.*?)<\/i>/i',                 // <i>
            '/<em[^>]*>(.*?)<\/em>/i',               // <em>
            '/(<ul[^>]*>|<\/ul>)/i',                 // <ul> and </ul>
            '/(<ol[^>]*>|<\/ol>)/i',                 // <ol> and </ol>
            '/<li[^>]*>(.*?)<\/li>/i',               // <li> and </li>
            '/<li[^>]*>/i',                          // <li>
            '/<hr[^>]*>/i',                          // <hr>
            '/(<table[^>]*>|<\/table>)/i',           // <table> and </table>
            '/(<tr[^>]*>|<\/tr>)/i',                 // <tr> and </tr>
            '/<td[^>]*>(.*?)<\/td>/i',               // <td> and </td>
            '/<th[^>]*>(.*?)<\/th>/i',              // <th> and </th>
            '/&(nbsp|#160);/i',                      // Non-breaking space
            '/&(quot|rdquo|ldquo|#8220|#8221|#147|#148);/i',
                                                     // Double quotes
            '/&(apos|rsquo|lsquo|#8216|#8217);/i',   // Single quotes
            '/&gt;/i',                               // Greater-than
            '/&lt;/i',                               // Less-than
            '/&(amp|#38);/i',                        // Ampersand
            '/&(copy|#169);/i',                      // Copyright
            '/&(trade|#8482|#153);/i',               // Trademark
            '/&(reg|#174);/i',                       // Registered
            '/&(mdash|#151|#8212);/i',               // mdash
            '/&(ndash|minus|#8211|#8722);/i',        // ndash
            '/&(bull|#149|#8226);/i',                // Bullet
            '/&(pound|#163);/i',                     // Pound sign
            '/&(euro|#8364);/i',                     // Euro sign
            '/[ ]{2,}/'                              // Runs of spaces, post-handling
        );

        /**
         *  List of pattern replacements corresponding to patterns searched.
         *
         *  @var array $replace
         *  @access public
         *  @see $search
         */
        $replace = array(
            '',                                     // Non-legal carriage return
            ' ',                                    // Newlines and tabs
            ' ',                                    // Runs of spaces, pre-handling
            '',                                     // <script>s -- which strip_tags supposedly has problems with
            '',                                     // <style>s -- which strip_tags supposedly has problems with
            //'',                                     // Comments -- which strip_tags might have problem a with
            "strtoupper(\"\n\n\\1\n\n\")",          // H1 - H3
            "ucwords(\"\n\n\\1\n\n\")",             // H4 - H6
            "\n\n\t",                               // <P>
            "\n",                                   // <br>
            'strtoupper("\\1")',                    // <b>
            'strtoupper("\\1")',                    // <strong>
            '_\\1_',                                // <i>
            '_\\1_',                                // <em>
            "\n\n",                                 // <ul> and </ul>
            "\n\n",                                 // <ol> and </ol>
            "\t* \\1\n",                            // <li> and </li>
            "\n\t* ",                               // <li>
    //        '$this->_build_link_list("\\1", "\\2")',
                                                    // <a href="">
            "\n-------------------------\n",        // <hr>
            "\n\n",                                 // <table> and </table>
            "\n",                                   // <tr> and </tr>
            "\t\t\\1\n",                            // <td> and </td>
            "strtoupper(\"\t\t\\1\n\")",            // <th> and </th>
            ' ',                                    // Non-breaking space
            '"',                                    // Double quotes
            "'",                                    // Single quotes
            '>',
            '<',
            '&',
            '(c)',
            '(tm)',
            '(R)',
            '--',
            '-',
            '*',
            'GBP',                                    // GBP sign. £ ?
            'EUR',                                  // Euro sign. € ?
            '',                                     // Unknown/unhandled entities
            ' '                                     // Runs of spaces, post-handling
        );
        $text = trim(stripslashes($text));

        // Run our defined search-and-replace
        $text = preg_replace($search, $replace, $text);
        
        // Strip any other HTML tags
        $text = strip_tags($text);
        
        if($trim) {
        	$text = trim($text);
        }
    }
	
	public function Htmlise( $mytext, $wrap=true, $mailto=true )
	{
		$mytext = " " . $mytext . " \n\r";
		// URLs with www.
		if (strpos($mytext,'www.')!==false)
		{
			$mytext = preg_replace('/\s(www\.)(\S+)/', ' <a href="http://\\1\\2">\\1\\2</a>', $mytext);
		}
		// URLs starting with http://
		if (strpos($mytext,'http')!==false)
		{
			$mytext = preg_replace( '/(?<!<a href=")((http|ftp|mailto)+(s)?:\/\/[^<>\s),;]+)/i', '<a href="\\0">\\0</a>', $mytext ); //"
			$mytext = preg_replace('/(>\shttp)/U', '>http', $mytext);
			$mytext = preg_replace('/(\"\shttp)/U', '\"http', $mytext);
		}
		// Email addresses
		if ($mailto && strpos($mytext,'@')!==false)
		{
			$mytext = preg_replace('/[ \t\r\n<;]([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?))/i',' <a href="mailto:$1">$1</a> ', $mytext);
		}
		// Wrap in a paragraph
		if ($wrap)
		{
			$mytext = preg_replace("/(.*)\n\r/sU","<p>\\1</p>\n",$mytext);
		}
		
		// Newlines to <br>, unless following a heading
		$mytext = preg_replace('/(?<!h[1-6]>)(\r)[^<\/p>]/',"\\1<br/>\n",$mytext);
		// Trim
		return trim($mytext);
	}
	
	public function IsUnicode($string)
	{
		$is_unicode = false;
		if(function_exists("mb_detect_encoding") && mb_detect_encoding($string)=="UTF-8")
		{
			$is_unicode = true;
		}
		return $is_unicode;
	}
	
	public function MakeLowerCase($string)
	{
		if($this->IsUnicode($string))
		{
			$string = mb_strtolower($string,'UTF-8');
		}
		else
		{
			$string = strtolower($string);
		}
		return $string;
	}
    
    public function str_split_utf8($str) 
    { 
        $words = mb_split(' ', $str);   
        $array = array(); 
        foreach($words as $str)
        {
            if ($this->IsUnicode($str))
            {
                $split=1; 
                for ( $i=0; $i < strlen( $str ); ){ 
                    $value = ord($str[$i]); 
                    if($value > 127){ 
                        if($value >= 192 && $value <= 223) 
                            $split=2; 
                        elseif($value >= 224 && $value <= 239) 
                            $split=3; 
                        elseif($value >= 240 && $value <= 247) 
                            $split=4; 
                    }else{ 
                        $split=1; 
                    } 
                        $key = NULL; 
                    for ( $j = 0; $j < $split; $j++, $i++ ) { 
                        $key .= $str[$i]; 
                    } 
                    array_push( $array, $key ); 
                } 
            }
            else
                array_push( $array, $str ); 
        }
        return $array; 
    } 
	
	public function RemoveAccents($text,$id_language)
	{
		if($id_language!="6" && $id_language!="10" && !$this->IsUnicode($text))
		{
			$text = utf8_decode($text);
			$text = strtr($text,utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
			$text = strtolower($text);
			$text = preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde);/','$1',$text);
		}
		return $text;
	}
	
	public function SearchWords($unescaped_words,$id_language)
	{
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($this->search_query_store && strlen($unescaped_words)>$s->min_str_length)
		{
			$s->QueryStore($unescaped_words);		
		}
		$this->min_str_length = $s->min_str_length;
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$unescaped_words = $this->RemoveAccents($unescaped_words,$id_language);
		mb_regex_encoding( "utf-8" ); 
		$words = mb_split(' +', $db->SqlQuote($unescaped_words));
   		$words = array_filter($words,array($this,"CutShortUtf"));
		$stop_words = $s->StopWords($id_language);
   		$words = array_diff($words,$stop_words);
   		$words = array_slice($words,0,SEARCH_WORDS_MAX);
		return $words;
	}
	
	private function CutShortUtf($value)
	{
		$length = $this->StringBytes($value);
		return $length>=($this->min_str_length);
	}
	
	public function String2Url($text_value)
	{
		$ret = $text_value;
		if ($ret!="")
		{
			if (!(strpos($ret,'://')>0))
				$ret = "http://" . $ret;
		}
		return $ret;
	}

	public function StringChunk($string,$length,$separator=" ")
	{
		$chunks = array();
		$shorter = substr($string,0,$length);
		$last_space_pos = strrpos($shorter,$separator);
		$chunks[] = substr($shorter,0,$last_space_pos);
		$leftover = substr($string,$last_space_pos);
		if(strlen($leftover)>$length)
			$chunks = array_merge($chunks,$this->StringChunk($leftover,$length,$separator));
		else 
			$chunks[] = $leftover;
		return $chunks;
	}
	
	public function StringPortion($string, $length) {
		if (strlen($string)>$length) {
			$string = substr($string,0,$length + 4);
			$string = substr($string,0,strrpos($string,' ')) . ' ...';
		}
		return $string;
	}
	
	public function StringCut( &$string, $length, $dots="...", $is_unicode=false, $separator="" )
	{
		if($is_unicode)
		{
			if (mb_strlen($string)>$length)
			{
				// fixing defects where got chinese and english character together
				$string = mb_substr($string,0,$length);
				if ($separator == "")
					$separator = " ";
				$string = mb_substr($string,0,mb_strrpos($string,$separator)) . $dots;
			}
		}
		else 
		{
			if (strlen($string)>$length)
			{
				$string = substr($string,0,$length);
				if ($separator == "")
					$string .= $dots;
				else
					$string = substr($string,0,strrpos($string,$separator)) . $dots;
			}
		}
		return $string;
	}

	public function StringBytes($str)
	{
		// From post by paolo dot mosna at gmail dot com on http://www.php.net/strlen
		// STRINGS ARE EXPECTED TO BE IN ASCII OR UTF-8 FORMAT
	
		// Number of characters in string
		$strlen_var = strlen($str);
		
		// string bytes counter
		$d = 0;
		
		/*
		* Iterate over every character in the string,
		* escaping with a slash or encoding to UTF-8 where necessary
		* See http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
		*/
		for ($c = 0; $c < $strlen_var; ++$c)
		{
			$ord_var_c = ord($str[$d]);
		
			switch (true)
			{
				case (($ord_var_c >= 0x20) && ($ord_var_c <= 0x7F)):
					// characters U-00000000 - U-0000007F (same as ASCII)
					$d++;
				break;
				
				case (($ord_var_c & 0xE0) == 0xC0):
					// characters U-00000080 - U-000007FF, mask 110XXXXX
					$d+=2;
				break;
				
				case (($ord_var_c & 0xF0) == 0xE0):
					// characters U-00000800 - U-0000FFFF, mask 1110XXXX
					$d+=3;
				break;
				
				case (($ord_var_c & 0xF8) == 0xF0):
					// characters U-00010000 - U-001FFFFF, mask 11110XXX
					$d+=4;
				break;
				
				case (($ord_var_c & 0xFC) == 0xF8):
					// characters U-00200000 - U-03FFFFFF, mask 111110XX
					$d+=5;
				break;
				
				case (($ord_var_c & 0xFE) == 0xFC):
					// characters U-04000000 - U-7FFFFFFF, mask 1111110X
					$d+=6;
				break;
				default:
					$d++;   
			}
		}
	 
		return $d;
	}
	
	public function StringLength($string)
	{
		return $this->StringBytes($string);
	}
	
	public function StripNonWords($text)
	{
		return preg_replace('/[\p{Z}\p{C}\p{P}]/u',' ',$text);
	}
	
	public function StripHtml($text)
	{
		$text = strip_tags($text);
		return $text;
	}
	
	public function TextClean( &$text, $is_html=false )
	{
		$text = str_replace("\\n","\n",$text);
		if(!$is_html)
		{
			$text = html_entity_decode($text);
			$text = strip_tags($text);		
		}
		$text = implode("\r\n", preg_split("/\r?\n/",$text));
	}

	public function Text2Upper( &$text )
	{
		if(!$this->IsUnicode($text))
		{
			$search  = array (chr(224),chr(232),chr(233),chr(236),chr(242),chr(249));
			$replace = array ("A'", "E'", "E'", "I'", "O'", "U'");
			$text    = str_replace($search, $replace, $text);
			$text = strtoupper($text);
		}
		return $text;
	}
	
	public function TextReplaceForJavascript($text,$is_html=FALSE)
	{
		$text = str_replace("'","\\'",$text);
		//$text = str_replace("\"","\'",$text); //'
		$text = str_replace("\r","",$text);
		$text = str_replace("\n","",$text);
		$text = trim($text);
		if (!$is_html)
			$text = htmlspecialchars($text);
		return $text;
	}

	public function UtfClean($string)
	{
		if($this->iconv)
			$string = iconv("UTF-8", "UTF-8//IGNORE", $string);
		return $string;
	}
	
	public function UrlEncodeArray($key,$array)
	{
		$encode = "";
		foreach($array as $val)
			$encode .= "&$key%5B%5D=$val";
		return $encode;
	}
	
}
?>
