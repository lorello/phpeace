<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Topic
{
	/** 
	 * @var Queue */
	public $queue;

	public $id;
	public $name;
	public $description;
	public $path;
	public $tree = array();
	public $id_language;
	public $id_group;
	public $id_style;
	public $row;
	public $only_visible;
	public $records_per_page;
	public $profiling;
	public $edit_layout;
	public $url;
	public $domain;
	public $visible;
	public $protected;
	public $wysiwyg;
	public $subtopic_types;
	public $authorized;

	/** 
	 * @var History */
	private $h;
			
	function __construct($id)
	{
		$this->id = $id;
		$this->id_group = 0;
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$this->authorized = false;
		$this->visible = false;
		$this->only_visible = false;
		$this->wysiwyg = "0";
		$this->subtopic_types = array(	'none'		=> 0,
					'folder'		=> 1,
					'article'		=> 2,
					'latest'		=> 3,
					'map'		=> 4,
					'contact'	=> 5,
					'url'		=> 6,
					'campaign'	=> 7,
					'forum'		=> 8,
					'gallery'	=> 9,
					'calendar'	=> 10,
					'links'		=> 11,
					'module'	=> 12,
					'insert'		=> 13,
					'quotes'	=> 14,
					'keyword_filter' => 15,
					'search' 	=> 16,
					'poll' 		=> 17,
					'dynamic'	=> 18 );
		if ($id>0)
		{
			$topic = $this->TopicGet();
			$this->name = $topic['name'];
			$this->description = $topic['description'];
			$this->path = $topic['path'];
			$this->id_language = $topic['id_language'];
			$this->id_group = $topic['id_group'];
			$this->id_style = $topic['id_style'];
			$this->edit_layout = $topic['edit_layout'];
			$this->profiling = $topic['profiling'];
			$this->wysiwyg = $topic['wysiwyg'];
			$this->records_per_page = $topic['articles_per_page'];
			$this->row = $topic;
			if ($topic['visible']=="1" && $topic['group_visible']=="1")
				$this->visible = true;
			$this->protected = $topic['protected'];
			$this->domain = (($topic['domain']!="")? $topic['domain'] : $ini->Get("pub_web"));
			$this->url = $this->domain . "/" . $this->path;
			unset($topic);
		}
		include_once(SERVER_ROOT."/../classes/queue.php");
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
		$this->queue = new Queue($this->id);
		unset($ini);
	}

	public function __destruct()
	{
		if(isset($this->tree))
			unset($this->tree);
		unset($this->queue);
		unset($this->h);
	}

	public function Accounts( &$rows, $paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_account,name,id_topic,id_user,id_type,active,params,shared 
		FROM accounts 
		WHERE id_topic=$this->id OR (id_topic=0 AND shared=1) ORDER BY id_account";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function Admins( $is_admin=1 )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT users.name,email,users.id_user FROM users INNER JOIN topic_users USING(id_user) WHERE id_topic='$this->id' AND users.id_user>0 AND users.active=1 AND topic_users.is_admin=$is_admin ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function AmIAdmin()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$db =& Db::globaldb();
		$admin = array();
		$db->query_single( $admin, "SELECT id_user FROM topic_users WHERE id_user='".$session->Get("current_user_id")."' AND is_admin=1 AND id_topic='$this->id'");
		return $admin['id_user']>0;
	}

	public function AmIUser($id_user=0)
	{
		if(!$id_user>0)
		{
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			$id_user = $session->Get("current_user_id");
		}
		$topic_user = false;
		$db =& Db::globaldb();
		$row = array();
		$db->query_single( $row, "SELECT id_user FROM topic_users WHERE id_user='$id_user' AND id_topic='$this->id'");
		if ($row['id_user']>0) {
		    $topic_user = true;
		}
		// check if memeber of translator's group
		if($this->id_group!=2) {
		    $row = Modules::ModuleGet(21);
		    if($row['active']=="1" && Modules::AmIUser(21)) {
		        $topic_user = true;
		    }
		}
		return $topic_user;
	}

	public function Articles($visible_only=false)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article
			FROM articles a
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic AND s.visible" . ($visible_only? "=1":"<4") . "
			WHERE a.approved=1 AND a.available=1 AND a.id_topic='$this->id' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ArticlesBySource($source)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article
			FROM articles a
			WHERE source='$source' AND a.id_topic='$this->id' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ArticlesApproved( &$rows, $approved=1, $id_template=0 )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,UNIX_TIMESTAMP(a.written) AS written_ts,
			IF(a.show_author=1,IF(a.author<>'',a.author,u.name),'-') AS author,a.id_subtopic,a.id_template,a.id_image,i.format
			FROM articles a
			LEFT JOIN users u ON a.id_user=u.id_user
			LEFT JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			LEFT JOIN images i ON a.id_image=i.id_image
			WHERE a.id_topic='$this->id' AND approved='$approved' ";
		if($this->only_visible)
			$sqlstr .= " AND s.visible<3 ";
		if($id_template>=0)
			$sqlstr .= " AND a.id_template=$id_template ";
		$sqlstr .= " GROUP BY a.id_article ORDER BY a.written DESC, a.id_article DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ArticlesBoxes()
	{
		$sqlstr = "SELECT b.id_box,b.title,b.content,b.is_html,b.show_title,b.id_type
			FROM boxes b
			INNER JOIN articles_boxes ab ON b.id_box=ab.id_box
			INNER JOIN articles a ON ab.id_article=a.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			WHERE ab.popup=1 AND a.approved=1 AND s.visible<4 AND a.id_topic='$this->id'";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ArticlesBoxesTypes()
	{
		$sqlstr = "SELECT id_type,name FROM articles_boxes_types WHERE id_style=0 OR id_style='$this->id_style'";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Boxes(&$rows)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT b.id_box,b.title,ab.popup,b.content,b.is_html,ab.width,ab.align,b.show_title,b.notes,b.id_type
		 FROM boxes b
		 INNER JOIN articles_boxes ab ON b.id_box=ab.id_box
		 INNER JOIN articles a ON ab.id_article=a.id_article
		 WHERE a.id_topic='$this->id' ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Campaigns()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_topic_campaign,name FROM topic_campaigns WHERE id_topic='$this->id' ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function CampaignsP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_topic_campaign,name,UNIX_TIMESTAMP(start_date) AS start_date_ts,money,active
		FROM topic_campaigns WHERE id_topic='$this->id' ORDER BY start_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function CommentsApproved( $approved )
	{
		$rows = array();
		include_once(SERVER_ROOT."/../classes/comments.php");
		$co = new Comments("article",0);
		$num = $co->CommentsApproved( $rows, $approved, $this->id );
		return $num;
	}

	public function Contacts()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT users.name,email,users.id_user
			FROM users INNER JOIN topic_users USING(id_user)
			WHERE id_topic='$this->id' AND users.id_user>0 AND users.active=1 AND topic_users.is_contact=1 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function CountSubtopicItems($id_subtopic, $subtopic_type)
	{
		$count_subtopic_articles = 0;
		$row = array();
		if ($subtopic_type==0)
		{
			$db =& Db::globaldb();
			$db->query_single($row,"SELECT COUNT(id_article) AS counter FROM articles WHERE id_topic=$this->id AND id_subtopic='$id_subtopic'");
			$count_subtopic_articles = $row['counter'];
		}
		if ($subtopic_type<3 && $subtopic_type>0)
		{
			$db =& Db::globaldb();
			$db->query_single($row,"SELECT COUNT(id_article) AS counter FROM articles WHERE id_topic=$this->id AND id_subtopic='$id_subtopic' AND approved=1");
			$count_subtopic_articles = $row['counter'];
		}
		if ($subtopic_type==3)
		{
			include_once(SERVER_ROOT."/../classes/ini.php");
			$ini = new Ini;
			$count_subtopic_articles = $ini->Get('latest');
		}
		if ($subtopic_type==11)
		{
			$db =& Db::globaldb();
			$db->query_single($row,"SELECT COUNT(links_subtopics.id_link) AS counter FROM links_subtopics INNER JOIN links USING(id_link) WHERE id_subtopic='$id_subtopic' AND approved=1 AND error404=0");
			$count_subtopic_articles = $row['counter'];
		}
		return $count_subtopic_articles;
	}

	public function Delete()
	{
		$id_topic = $this->id;
		$db =& Db::globaldb();
		
		$db->query( "DELETE FROM accounts_use WHERE id_account IN (SELECT id_account FROM accounts WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM payments WHERE id_account IN (SELECT id_account FROM accounts WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM accounts WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM images_articles WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM docs_articles WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM friends WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM articles_boxes WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM articles_content WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM articles_subhead WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		
		$db->query( "DELETE FROM articles_related WHERE id_article1 IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM articles_related WHERE id_article2 IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM translations WHERE id_article IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM translations WHERE id_article_trad IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM template_values WHERE id_res=5 AND id IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM contributions WHERE id_resource_type=5 AND id_resource IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM books_home WHERE id_topic='$id_topic' " );

		$comments = array();
		$sqlstr = "SELECT c.id_comment FROM comments c WHERE c.id_type=5 AND id_item IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') ";
		$db->QueryExe($comments, $sqlstr);
		if(count($comments)>0)
		{
			foreach($comments as $comment)
				$db->query( "DELETE FROM comments_content WHERE id_comment='{$comment['id_comment']}' " );
		}

		$db->query( "DELETE FROM comments WHERE id_type=5 AND id_item IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );
				
		$db->query( "DELETE FROM content_texts WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM events WHERE id_topic='$id_topic' AND portal=0 " );

		$db->query( "UPDATE events SET id_topic='0' WHERE id_topic='$id_topic'" );

		$db->query( "DELETE FROM forms WHERE id_topic='$id_topic' " );

		$db->query( "UPDATE galleries SET id_topic='0' WHERE id_topic='$id_topic'" );

		$db->query( "DELETE FROM history WHERE id_type=5 AND id IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM keywords_use WHERE id_type=5 AND id IN (SELECT id_article FROM articles WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM links_subtopics WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM lists WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM people_groups WHERE id_pt_group IN (SELECT id_pt_group FROM people_topics_groups WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM people_topics_groups WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM people_topics WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM poll_votes_names WHERE id_poll IN (SELECT id_poll FROM polls WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM poll_people_votes WHERE id_poll IN (SELECT id_poll FROM polls WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM poll_questions_votes_weights WHERE id_poll IN (SELECT id_poll FROM polls WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM poll_questions_groups WHERE id_poll IN (SELECT id_poll FROM polls WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM publish_log WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM queue WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM search_index WHERE id_res=1 AND id='$id_topic' " );
		$this->RemoveSearchIndex(2, "SELECT id_subtopic AS id_remove FROM subtopics WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(5, "SELECT id_article AS id_remove FROM articles WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(9, "SELECT id_topic_forum AS id_remove FROM topic_forums WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(10, "SELECT DISTINCT tft.id_thread AS id_remove FROM topic_forum_threads tft INNER JOIN topic_forums tf ON tft.id_topic_forum=tf.id_topic_forum WHERE tf.id_topic='$id_topic'");
        $this->RemoveSearchIndex(11, "SELECT id_topic_campaign AS id_remove FROM topic_campaigns WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(12, "SELECT id_quote AS id_remove FROM quotes WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(14, "SELECT DISTINCT id_link AS id_remove FROM links_subtopics WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(19, "SELECT id_poll AS id_remove FROM polls WHERE id_topic='$id_topic'");
        $this->RemoveSearchIndex(20, "SELECT DISTINCT pq.id_question AS id_remove FROM poll_questions pq INNER JOIN polls p ON pq.id_poll=p.id_poll WHERE p.id_topic='$id_topic'");

		$db->query( "DELETE FROM search_index2 WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM search_queue WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM topic_campaigns_persons WHERE id_topic_campaign IN (SELECT id_topic_campaign FROM topic_campaigns WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM topic_campaigns_orgs WHERE id_topic_campaign IN (SELECT id_topic_campaign FROM topic_campaigns WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM topic_campaigns_vips WHERE id_topic_campaign IN (SELECT id_topic_campaign FROM topic_campaigns WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM topic_forum_threads WHERE id_topic_forum IN (SELECT id_topic_forum FROM topic_forums WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM topic_homepage WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM topic_users WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM visits_pages WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM articles WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM quotes WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM links WHERE id_link IN (SELECT DISTINCT id_link FROM links_subtopics WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM poll_questions WHERE id_poll IN (SELECT id_poll FROM polls WHERE id_topic='$id_topic') " );

		$db->query( "DELETE FROM topic_campaigns WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM topic_forums WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM polls WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM template_params WHERE id_template IN (SELECT id_template FROM templates WHERE id_topic='$id_topic' AND id_group>0) " );

		$db->query( "DELETE FROM templates WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM url_friendly WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM keywords_use WHERE id_type=2 AND id IN (SELECT id_subtopic FROM subtopics WHERE id_topic='$id_topic') " );
		$db->query( "DELETE FROM subtopics WHERE id_topic='$id_topic' " );

		$db->query( "DELETE FROM keywords_use WHERE id_type=1 AND id='$id_topic' " );
		
		$db->query( "DELETE FROM topics WHERE id_topic='$id_topic' " );

		$this->queue->JobInsert($this->queue->types['map'],0,"");
		$this->queue->JobInsert($this->queue->types['homepage'],0,"");
		if($this->path!="")
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$fm->DirTopic("pub/{$this->path}","delete",$this->row['domain']!="");
			$fm->Delete("pub/sitemap_{$this->path}.xml.gz");
			$fm->Delete("pub/feeds/{$this->path}.rss");
			$fm->PostUpdate();
		}
		include_once(SERVER_ROOT."/../classes/adminhelper.php");
		$ah = new AdminHelper;
		$ah->MessageSet("topic_path_remove",array($this->path));
	}
    
    private function RemoveSearchIndex($id_res, $sqlstr)
    {
        $db =& Db::globaldb();
        $rows = array();
        $db->QueryExe($rows, $sqlstr);
        if(count($rows)>0)
        {
            foreach($rows as $row)
                $db->query( "DELETE FROM search_index WHERE id_res='$id_res' AND id='{$row['id_remove']}' " );
        }
    }
	
	public function Docs()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT d.id_doc,d.title,d.description,d.filename,da.id_article,d.format
			FROM docs d INNER JOIN docs_articles da USING (id_doc)
			INNER JOIN articles USING(id_article)
			WHERE id_topic='$this->id'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function DonationInsert($id_p,$amount,$id_account,$id_payment_type,$form_name,$id_form,$currency)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$id_use = $p->AccountUseGetById($p->account_usage_types['form'],$id_form,$id_account);
		$db =& Db::globaldb();
		$today = $db->getTodayDate();
		$id_payer = $p->PayerAdd($id_p);
		$id_payment = $p->PaymentStore(0,$today,$id_payer,$id_account,$amount,$form_name,"",$id_payment_type,1,0,0,0,$id_use,$currency);
		$p->SessionSet($p->account_usage_types['form'],$this->id,$id_payment);
		return $id_payment;
	}
	
	public function DonationPending()
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$id_payment = $p->SessionGet($p->account_usage_types['form'],$this->id);
		$payment = array('id_payment'=>$id_payment);
		if($id_payment>0)
		{
			$row = $p->PaymentGet($id_payment);
			$payment['amount'] = $row['amount'];
			$payment['id_currency'] = $row['id_currency'];
			$currencies = $p->currencies;
			$payment['currency'] = $currencies[$row['id_currency']];
			$payment['id_payment_type'] = $row['id_payment_type'];
			$payment['id_payer'] = $row['id_payer'];
			$payment['id_account'] = $row['id_account'];
			if($row['verified'])
				$payment['id_payment'] = 0;
		}
		return $payment;
	}
	
	public function Events( $approved=1 )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT e.id_event,e.title,e.description,UNIX_TIMESTAMP(e.start_date) AS start_date_ts,
			e.place,et.type,e.place_details,e.contact_name,e.email,e.link,e.phone,e.allday,
			e.length,e.id_article,'event' AS item_type,e.id_topic,e.jump_to_article
			FROM events e
			INNER JOIN event_types et ON e.id_event_type=et.id_event_type
			WHERE e.id_topic='$this->id' AND e.approved='$approved' 
			ORDER BY e.start_date DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function EventsSubtopics()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic,name FROM subtopics WHERE id_type=10 AND visible<3 AND id_topic='$this->id'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function FormPostNotify( $id_recipient,$recipient,$id_form,$form_name,$params,$contact,$id_post,$form_weights,$score,$action_email,$hide_empty_fields )
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/geo.php");
		include_once(SERVER_ROOT."/../classes/varia.php");
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($this->id_language,0,false,$this->id_style);
		$mail = new Mail();
		$subject = "[REQ] $this->name - $form_name";
		$extra = array();
		$name_surname = "";
		foreach($params as $param)
		{
			if ($param['use']==1)
			{
				$name = $param['value']; 
			}
			if ($param['use']==2)
			{
				$surname = $param['value']; 
			}
			if ($param['use']==3)
			{
				$name_surname = $param['value']; 
			}
			if ($param['use']==4)
			{
				$extra['email'] = $param['value']; 
			}
			if($param['email_subject'])
			{
				$subject .= " - " . $param['value'];
			}
			if(!$hide_empty_fields || $param['value']!="")
				$msg .= "{$param['name']}: {$param['value']}\t\r\n";
		}
		if($name_surname!="")
		{
			if($name=="")
			{
				$names = explode(" ",$name_surname,2);
				$name = $names[0];
				$surname = $names[1];
			}
			$extra['name'] = $name_surname;
		}
		else 
		{
			$extra['name'] = $name;
			if($surname!="")
				$extra['name'] = $extra['name'] . " " . $surname;
		}
		if($form_weights)
		{
			$msg .= "\r\nScore: $score\r\n";
		}
		$msg .= "\r\n" . $tr->TranslateParams("contact_topic",array($form_name,Varia::Referer(),$this->name)); 
		$msg .= "\r\n$this->url\r\n";
		$ip = Varia::IP();
		$msg .= "\r\nIP: $ip\r\n";
		if($ip!='127.0.0.1') {
		    $geo = new Geo();
		    $country = $geo->IPLocalize($ip);
		    if(isset($country['code'])) {
		        $msg .= "Country: {$country['code']} - {$country['name']}\r\n";
		    }
		}
		$msg .= "Date: " . date("r") . "\r\n";
		if($id_post>0)
		{
			$msg .= "{$mail->admin_web}/topics/form_post.php?id_topic={$this->id}&id_form=$id_form&id=$id_post\n";
		}
		$recipients = $mail->AddressSplitAndValidate($recipient);
		if($id_recipient>0 || count($recipients)>0) 
		{
			if(count($recipients)>0)
			{
				foreach($recipients as $recipient)
				{
					$mail->SendMail($recipient,"",$subject,$msg,$extra,false,array(),false);
				}
			}
			elseif($id_recipient>0) 
			{
				include_once(SERVER_ROOT."/../classes/user.php");
				$u = new User();
				$u->id = $id_recipient;
				$user = $u->UserGet();
				$mail->SendMail($user['email'],$user['name'],$subject,$msg,$extra);
			}
		}
		else 
		{
			$contacts = $this->Contacts();
			foreach($contacts as $contact)
			{
				$mail->SendMail($contact['email'],$contact['name'],$subject,$msg,$extra);
			}
		}
		if($form_weights)
		{
			$recipients2 = $mail->AddressSplitAndValidate($action_email);
			if(count($recipients2)>0)
			{
				foreach($recipients2 as $recipient2)
				{
					$mail->SendMail($recipient2,"",$subject,$msg,$extra,false,array(),false);
				}
			}
		}
	}
	
	public function FormPostFeedback($id_form,$id_p,$email,$form_name,$thanks_email)
	{
		$name = "";
		if($id_p>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$user = $pe->UserGetById($id_p);
			$email = $user['email'];
			$name = "{$user['name1']} {$user['name2']}";
		}
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/varia.php");
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($this->id_language,0,false,$this->id_style);
		$mail = new Mail();
		$extra = array();
		$extra['name'] = $this->name;
		$extra['email'] = $this->row['temail']!=""? $this->row['temail'] : $mail->staff_email;
		$subject = "[REQ] $this->name - $form_name";
		$mail->SendMail($email, $name, $subject, $thanks_email, $extra);
	}
	
	public function FormPostStore($id_form,$id_p,$params,$file,$id_payment,$score)
	{
		include_once(SERVER_ROOT."/../classes/forms.php");
		$fo = new Forms();
		$id_post = $fo->PostStore($id_form,$this->id,$id_p,$params,$id_payment,$score);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		if ($file['ok'])
		{
			if(isset($file['id_gallery']) && $file['id_gallery']>0)
			{
				include_once(SERVER_ROOT."/../classes/image.php");
				$im = new Image(0);
				$image_date = date("Y-m-d");
				$source = "";
				$author = "User";
				if($id_p>0)
				{
					include_once(SERVER_ROOT."/../classes/people.php");
					$pe = new People();
					$user = $pe->UserGetById($id_p);
					$author = "{$user['name1']} {$user['name2']}";
				}
				$caption = isset($file['caption'])? $file['caption'] : "Upload";
				$im->ImageInsert($image_date,$source,$author,0,$caption,"",$file);
				if ($im->id>0)
				{
					$im->GalleryInsert($file['id_gallery'],"");
					$fo->PostUpdate($id_post,$im->id);
				}
			}
			else 
			{
				$fm->DirAction("uploads/forms/$id_form","check");
				$origfile = "uploads/forms/$id_form/{$id_post}.{$file['ext']}";
				$fm->MoveUpload($file['temp'],$origfile);
				$fo->PostUpdate($id_post,$file['ext']);
			}
			$fm->PostUpdate();
		}
		return $id_post;
	}

	public function FormUserCreate( $params,$id_geo,$contact,$id_p,$id_pt_group=0,$id_pt_groups=array() )
	{
		$contact_topic = 0;
		$contact_portal = 0;
		if($contact)
		{
			$contact_portal = 1;
			if($this->id>0 && $this->profiling)
			{
				$contact_topic = 1;
			}
		}
		$name_surname = "";
		foreach($params as $param)
		{
			if ($param['use']==1)
			{
				$name = $param['value']; 
			}
			if ($param['use']==2)
			{
				$surname = $param['value']; 
			}
			if ($param['use']==3)
			{
				$name_surname = $param['value']; 
			}
			if ($param['use']==4)
			{
				$email = $param['value']; 
			}
		}
		if($name_surname!="")
		{
			if($name=="")
			{
				$names = explode(" ",$name_surname,2);
				$name = $names[0];
				$surname = $names[1];
			}
		}
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		if($id_p>0)
		{
			if($contact_portal)
				$pe->PortalAssociate($id_p);
			if($contact_topic)
				$pe->TopicAssociate($id_p,$this->id,$contact_topic);
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/validator.php");
			$va = new Validator(false,0,false);
			$db =& Db::globaldb();
			$name = $db->SqlQuote($name);
			$surname = $db->SqlQuote($surname);
			$email = $db->SqlQuote($email);
			$email_valid = ($va->Email($email))? 1:0;
			$id_p = $pe->UserCreate( $name,$surname,$email,$id_geo,$contact_portal,array(),false,$this->id,$contact_topic,true,$email_valid);
		}
		if($id_pt_group>0)
		{
			$pe->TopicGroupAssociate($id_p,$id_pt_group);
		}
		if(count($id_pt_groups)>0)
		{
			foreach($id_pt_groups as $id_pt_group_additional)
			{
				$pe->TopicGroupAssociate($id_p,$id_pt_group_additional);				
			}
		}
		return $id_p;
	}
	
	public function Forums()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_topic_forum,name FROM topic_forums WHERE id_topic='$this->id' ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ForumsP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_topic_forum,name,UNIX_TIMESTAMP(start_date) AS start_date_ts,active
		FROM topic_forums WHERE id_topic='$this->id' ORDER BY start_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Galleries($id_user)
	{
		$rows = array();
		$db =& Db::globaldb();
		if($id_user>0)
			$sqlstr = "SELECT id_gallery,title FROM galleries WHERE id_user='$id_user' ORDER BY title";
		else
			$sqlstr = "SELECT id_gallery,title FROM galleries WHERE id_topic='$this->id' OR (id_topic_group='$this->id_group' AND id_topic=0) ORDER BY title";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function HasArticleInHomepage($id_article)
	{
		$haih = FALSE;
		switch($this->row['home_type'])
		{
			case "0":
				if ($this->row['id_article_home'] == $id_article)
					$haih = TRUE;
			break;
			case "1":
				$articles = array();
				$this->HomepageArticles($articles);
				foreach($articles as $article)
					if ($article['id_article'] == $id_article)
						$haih = TRUE;
			break;
			case "2":
				$last = $this->HomepageLastArticle();
				if ($last['id_article'] == $id_article)
					$haih = TRUE;
			break;
			case "3":
				$latest = $this->Latest($this->row['articles_per_page']);
				foreach($latest as $article)
					if ($article['id_article'] == $id_article)
						$haih = TRUE;
			break;
			case "4":
			break;
		}
		return $haih;
	}

	public function HasArticleInLatest($id_article, $limit)
	{
		$hail = false;
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article
			FROM articles a
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			WHERE a.id_topic='$this->id' AND a.id_article='$id_article' 
			AND approved=1 AND a.show_latest=1 AND a.published>0 AND s.visible<3
			ORDER BY a.published DESC, a.id_article DESC ";
		if ($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		if ($rows[0]['id_article']>0)
			$hail = true;
		return $hail;
	}

	public function HasFunding()
	{
		$funding = false;
		$db =& Db::globaldb();
		$campaigns = array();
		$db->query_single( $campaigns, "SELECT id_topic_campaign FROM topic_campaigns WHERE money='1' AND id_topic='$this->id'");
		if (count($campaigns)>0)
			$funding = true;
		$forms = array();
		$sqlstr = "SELECT id_subtopic FROM subtopics s 
			INNER JOIN forms f ON s.id_item=f.id_form 
			WHERE s.id_type=5 AND s.id_topic='$this->id' AND f.profiling=2 ";
		$db->QueryExe($forms, $sqlstr);
		if(count($forms)>0)
			$funding = true;
		return $funding;
	}
	
	public function HasSubtopicType( $subtopic_type )
	{
		$has_subtopic = 0;
		$db =& Db::globaldb();
		$type = array();
		$db->query_single( $type, "SELECT id_subtopic FROM subtopics WHERE id_type='$subtopic_type' AND id_topic='$this->id'");
		if (isset($type['id_subtopic']) && $type['id_subtopic']>0)
			$has_subtopic = $type['id_subtopic'];
		return $has_subtopic;
	}

	public function HasSubtopicTypeItem( $subtopic_type, $id_item )
	{
		$has_subtopic = 0;
		$db =& Db::globaldb();
		$type = array();
		$db->query_single( $type, "SELECT id_subtopic FROM subtopics WHERE id_type='$subtopic_type' AND id_topic='$this->id' AND id_item='$id_item' ");
		if (isset($type['id_subtopic']) && $type['id_subtopic']>0)
			$has_subtopic = $type['id_subtopic'];
		return $has_subtopic;
	}

	public function HomepageArticle( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT articles.id_article,headline,subhead,UNIX_TIMESTAMP(written) AS written_ts,id_subtopic
		FROM topics
		INNER JOIN articles ON topics.id_article_home=articles.id_article
		INNER JOIN articles_subhead USING(id_article)
		WHERE topics.id_topic='$this->id' ORDER BY articles.written DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function HomepageArticles( &$rows, $paged=false )
	{
		$sort_by = $this->row['home_sort_by'];
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,ah.subhead,UNIX_TIMESTAMP(a.written) AS written_ts,
		a.show_date,a.id_user,a.author,a.id_topic,th.is_main,a.id_subtopic,a.show_author,a.halftitle,
		a.available,a.author_notes,a.approved,a.id_visibility,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language,a.highlight
		FROM topic_homepage th
		INNER JOIN articles a ON th.id_article=a.id_article
		INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
		WHERE th.id_topic='$this->id' AND a.approved=1 ";
		$sqlstr .= $this->LatestOrderBy($sort_by);
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function HomepageArticleAdd( $id_article )
	{
		$db =& Db::globaldb();
		$db->begin();
		if ($this->row['home_type']==1 || $this->row['home_type']==5)
		{
			$db->lock( "topic_homepage" );
			$sqlstr = "INSERT INTO topic_homepage (id_topic,id_article) VALUES ($this->id,$id_article)";
		}
		else
		{
			$db->lock( "topics" );
			$sqlstr = "UPDATE topics SET id_article_home='$id_article' WHERE id_topic='$this->id'";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->queue->JobInsert($this->queue->types['topic_home'],$this->id,"");
	}

	public function HomepageArticleRemove( $id_article )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_homepage" );
		$res[] = $db->query( "DELETE FROM topic_homepage WHERE id_topic='$this->id' AND id_article='$id_article'" );
		Db::finish( $res, $db);
		$this->queue->JobInsert($this->queue->types['topic_home'],$this->id,"");
	}

	private function HomepageReset()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_homepage" );
		$res[] = $db->query( "DELETE FROM topic_homepage WHERE id_topic='$this->id'" );
		Db::finish( $res, $db);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$res[] = $db->query( "UPDATE topics SET id_article_home=0 WHERE id_topic='$this->id'" );
		Db::finish( $res, $db);
	}

	public function HomepageArticleSwapStatus( $current, $id_article )
	{
		$current = ($current)? 0 : 1;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_homepage" );
		$sqlstr = "UPDATE topic_homepage SET is_main=$current WHERE id_topic='$this->id' AND id_article='$id_article'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->queue->JobInsert($this->queue->types['topic_home'],$this->id,"");
	}

	public function HomepageArticlesAvailable( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,UNIX_TIMESTAMP(a.written) AS written_ts,
				IF(a.show_author=1,IF(a.author<>'',a.author,u.name),'-') AS author,ah.subhead,a.id_subtopic
				FROM articles a
				INNER JOIN articles_subhead ah ON ah.id_article=a.id_article
				INNER JOIN users u ON a.id_user=u.id_user ";
		if ($this->row['home_type']==0)
		{
			$current_article = (int)$this->row['id_article_home'];
			$sqlstr .= "	WHERE a.id_topic='$this->id' AND a.approved=1 AND a.id_article<>$current_article ";
		}
		elseif ($this->row['home_type']==1 || $this->row['home_type']==5)
		{
			$sqlstr .= "	LEFT JOIN topic_homepage th ON th.id_topic=a.id_topic AND th.id_article=a.id_article
					WHERE a.id_topic='$this->id' AND a.approved=1 AND th.id_article IS NULL ";
		}
		else
		{
			$sqlstr .= "	WHERE a.id_topic='$this->id' AND a.approved=1 ";
		}
		$sqlstr .= " ORDER BY a.written DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function HomepageLastArticle()
	{
		$sort_by = $this->row['home_sort_by'];
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT a.id_article,a.halftitle,a.headline,ah.subhead,
			UNIX_TIMESTAMP(written) AS written_ts,a.id_subtopic
			FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			WHERE a.id_topic='$this->id' AND a.approved=1  ";
		$sqlstr .= $this->LatestOrderBy($sort_by) . " LIMIT 1 ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function HomepageSubtopic()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT s.id_subtopic,s.name,s.description,s.id_type,s.id_item FROM topics t
			INNER JOIN subtopics s ON t.id_article_home=s.id_subtopic AND s.id_topic='$this->id'
			WHERE t.id_topic='$this->id'");
		return $row;
	}

	public function ImagesApproved()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT i.id_image,i.format,ia.align,ia.size,ia.caption,ia.zoom,ia.id_article,
				ia.width,ia.height,i.width AS orig_width,i.height AS orig_height
			FROM images i 
			INNER JOIN images_articles ia ON i.id_image=ia.id_image
			INNER JOIN articles a ON ia.id_article=a.id_article AND a.approved=1
			WHERE a.id_topic='$this->id'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ImagesAssociated()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_image,a.image_size AS size,a.image_align AS align,a.id_article,
		a.image_width AS width,a.image_height AS height
		FROM articles a
		WHERE a.id_image>0 AND a.id_topic='$this->id' AND a.approved=1";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function IsVisitorAuthorized($id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_p FROM people_topics WHERE id_topic='$this->id' AND access=1 AND id_p='$id_p' ");
		$this->authorized = $row['id_p']>0;
	}
	
	public function KeywordAdd($id_keyword,$id_res_type,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "INSERT INTO topic_keywords (id_topic,id_keyword,id_res_type,role) VALUES ('$this->id','$id_keyword','$id_res_type','$role')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordDelete($id_keyword)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "DELETE FROM topic_keywords WHERE id_topic='$this->id' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordGet($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk 
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword 
			WHERE k.id_keyword='$id_keyword' AND id_topic='$this->id' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function KeywordUpdate($id_keyword,$id_res_type,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "UPDATE topic_keywords SET id_res_type='$id_res_type',role='$role' 
			WHERE id_topic='$this->id' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordItems($id_type,$id_keyword,$topic_only,$year,$sort_by=1)
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$items = array();
		$id_topic = ($topic_only==0)? $this->id : 0;
		switch($id_type)
		{
			case $r->types['article']:
				$items = $k->UseArticles($id_keyword,$id_topic,0,$sort_by,array(),$year);
			break;
			case $r->types['book']:
				$items = $k->UseBooks($id_keyword,0,$year);
			break;
			case $r->types['event']:
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$ee->id_topic = $id_topic;
				$ee->id_group = $this->id_group;
				$items = $ee->Keyword($id_keyword,0,$year);
			break;
			case $r->types['link']:
				$items = $k->UseLinks($id_keyword,$id_topic,0);
			break;
			case $r->types['org']:
				$items = $k->UseOrg($id_keyword,0);
			break;
		}
		return $items;
	}

	public function KeywordItemsMultiple($id_type,$id_keywords,$topic_only,$year,$sort_by=1)
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$items = array();
		$id_topic = ($topic_only==0)? $this->id : 0;
		switch($id_type)
		{
			case $r->types['article']:
				$items = $k->UseArticlesMultipleAnd($id_keywords,$id_topic,0,0,$sort_by,$year);
			break;
			case $r->types['book']:
			case $r->types['event']:
			case $r->types['link']:
			case $r->types['org']:
				$items = array(); // TODO
			break;
		}
		return $items;
	}

	public function KeywordsInternal($id_res_type)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT tk.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword
			WHERE tk.id_topic='$this->id' ";
		if($id_res_type>0)
			$sqlstr .= " AND (tk.id_res_type=0 OR tk.id_res_type='$id_res_type') ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function KeywordsInternalAvailable(&$rows)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description FROM keywords k 
			WHERE k.id_type=4 AND k.id_keyword NOT IN (SELECT id_keyword FROM topic_keywords WHERE id_topic='$this->id') ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function LastPublish()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT UNIX_TIMESTAMP(publish) AS publish_ts FROM publish_log
			WHERE id_topic='$this->id' ORDER BY publish DESC LIMIT 1");
		return $row['publish_ts'];
	}
	
	public function LastUpdate()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT UNIX_TIMESTAMP(max(publish)) AS ts FROM publish_log WHERE id_topic='$this->id' ");
		return $row['ts'];
	}

	public function Latest($limit,$sort_by=0,$not_only_latest=0,$exclude_keywords=array())
	{
		$rows = array();
		$wheres = "";
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article,a.headline,ah.subhead,a.halftitle,UNIX_TIMESTAMP(a.written) AS written_ts,a.available,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,a.show_author,a.show_date,a.id_user,a.author,a.author_notes,
			'$this->path' as path,'article' AS item_type,a.id_topic,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language,a.highlight
			FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic ";
		for ($i = 0; $i < count($exclude_keywords); $i++)
		{			
			$sqlstr .= " LEFT JOIN keywords_use ku$i ON ku$i.id=a.id_article AND ku$i.id_type='5' AND ku$i.id_keyword={$exclude_keywords[$i]}  ";
			$wheres .= " AND ku$i.id IS NULL ";
		}
		$sqlstr .= " WHERE a.id_topic='$this->id'
			AND approved=1 AND a.published>0 AND s.visible<3 " . $wheres ;
		if($not_only_latest==0)
			$sqlstr .= " AND a.show_latest=1 ";
		$sqlstr .= $this->LatestOrderBy($sort_by);
		if ($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function LatestArticles( $timestamp )
	{
		$timestamp = ($timestamp>0)? $timestamp:0;
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article,a.headline,ah.subhead,a.halftitle,UNIX_TIMESTAMP(a.written) AS written_ts,a.available,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,a.show_author,a.show_date,a.id_user,a.author,a.author_notes,
			'$this->path' as path,'article' AS item_type,a.id_topic,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language,a.highlight
			FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			WHERE a.id_topic='$this->id' AND a.published>=FROM_UNIXTIME($timestamp)
			AND a.approved=1 AND a.show_latest=1 AND s.visible<3 
			ORDER BY a.published DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	private function LatestOrderBy($sort_by)
	{
		$sqlstr = " ORDER BY ";
		switch($sort_by)
		{
			case "0":
				$sqlstr .= "a.published DESC,";
			break;
			case "1":
				$sqlstr .= "a.written DESC,";
			break;
			case "2":
				$sqlstr .= "a.id_visibility DESC,a.published DESC,";
			break;
			case "3":
				$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
			break;
			case "4":
				$sqlstr .= "a.original DESC,a.written DESC,";
			break;
			case "5":
				$sqlstr .= "a.headline ASC,";
			break;
		}
		$sqlstr .= "a.id_article DESC ";
		return $sqlstr;
	}

	public function Links($id_subtopic)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT links.id_link,url,title,links.description,vote,id_language,'link' AS item_type
				FROM links_subtopics
				INNER JOIN links USING(id_link)
				WHERE links.approved=1 AND error404=0 AND links_subtopics.id_subtopic='$id_subtopic'
				ORDER BY title";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function Lists()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_list,email,description,name,feed,archive
				FROM lists
				WHERE public=1 AND id_topic='$this->id'
				ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function LoadTree($force=false,$published_only=false)
	{
		if (!is_array($this->tree) || !count($this->tree)>0 || $force)
		{
			$tree = array();
			$rows = array();
			$db =& Db::globaldb();
			$sqlstr = "SELECT id_subtopic,name,id_parent,visible,id_type,id_item,id_subitem,id_subitem_id,link,seq,sort_by 
				FROM subtopics WHERE id_topic='$this->id' ";
			if($published_only)
				$sqlstr .= " AND is_published=1 ";
			$sqlstr .= " ORDER BY seq";
			$db->QueryExe($rows, $sqlstr);
			$counter = 0;
			foreach($rows as $row)
			{
				$tree[$counter]['id_subtopic']	= $row['id_subtopic'];
				$tree[$counter]['name']		= $row['name'];
				$tree[$counter]['id_parent']	= $row['id_parent'];
				$tree[$counter]['visible']	= $row['visible'];
				$tree[$counter]['id_type']	= $row['id_type'];
				$tree[$counter]['seq']		= $row['seq'];
				$tree[$counter]['link']		= $row['link'];
				$tree[$counter]['id_item']	= $row['id_item'];
				$tree[$counter]['id_subitem']	= $row['id_subitem'];
				$tree[$counter]['id_subitem_id']	= $row['id_subitem_id'];
				$tree[$counter]['sort_by']	= $row['sort_by'];
				$counter ++;
			}
			$this->tree = $tree;
			$tree = null;
			unset($tree);
		}
	}

	public function MailjobSearch($params,$reset)
	{
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs(4);
		$sqlstr = "SELECT p.email AS mj_email,pt.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.password AS mj_password,p.id_geo AS mj_geo
		FROM people_topics pt 
		INNER JOIN people p ON pt.id_p=p.id_p ";
		if ($params['id_pt_group']>0)
		{
			$sqlstr .= " INNER JOIN people_groups pg ON pt.id_p=pg.id_p AND pg.id_pt_group='{$params['id_pt_group']}' ";
		}
		$sqlstr .= " WHERE pt.id_topic={$this->id} AND pt.contact=1 AND p.active=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%')  ";
		if (strlen($params['email']) > 0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%')  ";
		if ($params['id_geo']>0)
			$sqlstr .= " AND p.id_geo='{$params['id_geo']}' ";
		$sqlstr .= " ORDER BY p.name1 ";
		return $mj->RecipientsSet($sqlstr,true,true,$reset);
	}
	
	public function Modules()
	{
		include_once(SERVER_ROOT."/../classes/modules.php");
		$tmodules = array();
		$modules = Modules::AvailableModules();
		foreach($modules as $module)
		{
			if ($module['layout'] && !$module['internal'] && !$module['global'])
				$tmodules[] = array('0'=>$module['id_module'],'1'=>($module['path']));
		}
		return $tmodules;
	}

	public function Move($new_path)
	{
		$move = $this->TopicPathIsOk($new_path);
		if($move)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager;
			$move = $fm->Rename("pub/$this->path","pub/$new_path",false,true);
			$fm->PostUpdate();
		}
		return $move;
	}

	private function ParseTreeForReshuffle($id_root)
	{
		$children = $this->SubtopicChildren($id_root);
		$counter = 1;
		foreach($children as $child)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "subtopics" );
			$res[] = $db->query( "UPDATE subtopics set seq=$counter WHERE id_subtopic='{$child['id_subtopic']}'");
			Db::finish( $res, $db);
			$counter ++;
			$this->ParseTreeForReshuffle($child['id_subtopic']);
		}
	}
	
	public function PasswordHtpasswd()
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		$users = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT u.id_user,name,login,password 
			FROM users u 
			LEFT JOIN topic_users tu ON u.id_user=tu.id_user 
			LEFT JOIN module_users mu ON u.id_user=mu.id_user AND mu.id_module=4 
			WHERE u.active=1 AND (tu.id_topic={$this->id} OR mu.is_admin=1) 
			GROUP BY u.id_user ORDER BY u.name";
		$db->QueryExe($users, $sqlstr);
		$u = new User();
		$htpasswd = "";
		foreach($users as $user)
		{
			$u->id = $user['id_user'];
			$password = $sc->decrypt($user['password']);
			$htpasswd .= $user['login'] . ":" . crypt($password) . "\n";
		}
		return $htpasswd;
	}
	
	public function PasswordUpdate()
	{
		include_once(SERVER_ROOT."/../classes/publishmanager.php");
		$pm = new PublishManager();
		$pm->TopicInit($this->topic->id);
		$pm->TopicHtaccess();
	}

	public function PendingArticles()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_article FROM articles WHERE id_topic='$this->id' AND approved=0";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingCampaignOrgs()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_org) AS counter,c.id_topic,o.id_topic_campaign,c.name
			FROM topic_campaigns_orgs o
			INNER JOIN topic_campaigns c ON o.id_topic_campaign=c.id_topic_campaign AND c.id_topic='$this->id'
			WHERE comment_approved=0 GROUP BY c.id_topic_campaign";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingCampaignPersons()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_person) AS counter,c.id_topic,p.id_topic_campaign,c.name
			FROM topic_campaigns_persons p
			INNER JOIN topic_campaigns c ON p.id_topic_campaign=c.id_topic_campaign AND c.id_topic='$this->id'
			WHERE comment_approved=0 GROUP BY c.id_topic_campaign";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingLinks()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT l.id_link FROM links l LEFT JOIN links_subtopics ls USING(id_link) WHERE ls.id_topic='$this->id' AND l.approved=0";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingThreads()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(t.id_thread) AS counter,t.id_topic_forum,f.name
		FROM topic_forum_threads t
		INNER JOIN topic_forums f USING(id_topic_forum)
		WHERE f.id_topic='$this->id' AND t.thread_approved=0 GROUP BY f.id_topic_forum";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function PendingPollsComments()
	{
		include_once(SERVER_ROOT."/../classes/comments.php");
		$co = new Comments("question",0);
		return $co->CommentsPending(0,$this->id);
	}
	
	public function PendingThreadsComments()
	{
		include_once(SERVER_ROOT."/../classes/comments.php");
		$co = new Comments("thread",0);
		return $co->CommentsPending(0,$this->id);
	}
	
	public function People( &$rows, $paged = true )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$gjoin = $geo->GeoJoin("p.id_geo");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT p.id_p,p.name1,p.name2,CONCAT(p.name1,' ',p.name2) AS full_name,p.email,p.verified,p.town,UNIX_TIMESTAMP(p.start_date) AS start_date_ts,pt.access,
		$gjoin[name] AS geo_name,pt.contact
		FROM people_topics pt 
		INNER JOIN people p ON pt.id_p=p.id_p
		 $gjoin[join] 
		WHERE pt.id_topic='$this->id' AND p.active=1 
		GROUP BY p.id_p
		ORDER BY p.name1";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	public function PeopleGroups(&$rows, $paged=false, $show_generic=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		if($paged)
		{
			$sqlstr = "SELECT ptg.id_pt_group,ptg.name,ptg.id_topic,t.name AS topic_name,COUNT(pt.id_p) AS members
				 FROM people_topics_groups ptg
				 LEFT JOIN topics t ON ptg.id_topic=t.id_topic
				 LEFT JOIN people_groups pg ON ptg.id_pt_group=pg.id_pt_group
				 LEFT JOIN people_topics pt ON pg.id_p=pt.id_p AND pt.id_topic=$this->id 
				 WHERE ptg.id_topic=$this->id ";
			if($show_generic)
				$sqlstr .= " OR ptg.id_topic=0 ";
			$sqlstr .= " GROUP BY ptg.id_pt_group ORDER BY ptg.name";
		}
		else
		{
			$sqlstr = "SELECT id_pt_group,name
				 FROM people_topics_groups
				 WHERE id_topic=$this->id";
			if($show_generic)
				$sqlstr .= " OR id_topic=0 ";
			$sqlstr .= " ORDER BY name";
		}
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function PeopleGroupDelete($id_pt_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_groups" );
		$sqlstr = "DELETE FROM people_groups WHERE id_pt_group='$id_pt_group' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "people_topics_groups" );
		$sqlstr = "DELETE FROM people_topics_groups WHERE id_pt_group='$id_pt_group' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function PeopleGroupGet($id_pt_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_pt_group,name,id_topic FROM people_topics_groups  WHERE id_pt_group=$id_pt_group ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function PeopleGroupStore($id_pt_group,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_topics_groups" );
		if($id_pt_group>0)
			$sqlstr = "UPDATE people_topics_groups SET name='$name' WHERE id_pt_group='$id_pt_group' ";
		else 
		{
			$id_pt_group = $db->nextId( "people_topics_groups", "id_pt_group" );
			$sqlstr = "INSERT INTO people_topics_groups (id_pt_group,id_topic,name) VALUES ($id_pt_group,'$this->id','$name')";
		}
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		return $id_pt_group;
	}

	public function PeopleNum()
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT count(p.id_p) AS counter FROM people_topics pt INNER JOIN people p ON pt.id_p=p.id_p WHERE pt.id_topic=$this->id AND p.active=1 ";
		$db->query_single($row,$sqlstr);
		return $row['counter'];
	}
	
	public function PersonContactDelete($id_p)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_topics" );
		$sqlstr = "DELETE FROM people_topics WHERE id_p='$id_p' AND id_topic='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$pe->GroupsDelete($id_p,$this->id);
	}
	
	public function PersonContactUpdate($id_p,$contact,$access,$admin_notes)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_topics" );
		$sqlstr = "UPDATE people_topics SET contact='$contact',access='$access',admin_notes='$admin_notes' WHERE id_p='$id_p' AND id_topic='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function PersonGet($id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT contact,access,admin_notes,id_p FROM people_topics WHERE id_p=$id_p AND id_topic=$this->id ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function Polls( &$rows, $paged= true, $with_stats=false )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT pl.id_poll,pl.title,UNIX_TIMESTAMP(pl.start_date) AS start_date_ts,pl.is_private,pl.active";
		if($with_stats)
			$sqlstr .= ",COUNT(DISTINCT ppv.id_p) AS votes   ";
		$sqlstr .= " FROM polls pl ";
		if($with_stats)
			$sqlstr .= " LEFT JOIN poll_people_votes ppv ON pl.id_poll=ppv.id_poll ";
		$sqlstr .= " WHERE pl.id_topic='$this->id' ";
		if($with_stats)
			$sqlstr .= " GROUP BY pl.id_poll ";
		$sqlstr .= " ORDER BY pl.start_date DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function Reshuffle()
	{
		$this->LoadTree(true);
		if (count($this->tree)>0)
			$this->ParseTreeForReshuffle(0);
		return sizeof($this->tree);
	}

	public function SubtopicArticle($id_article)
	{
		$id_subtopic = 0;
		$db =& Db::globaldb();
		$row = array();
		$db->query_single( $row, "SELECT s.id_subtopic FROM subtopics s INNER JOIN articles a ON s.id_subtopic=a.id_subtopic WHERE s.id_type='2' AND s.id_topic='$this->id' AND s.visible<4 AND a.id_article='$id_article' AND a.approved=1 ");
		if ($row['id_subtopic']>0)
			$id_subtopic = $row['id_subtopic'];
		return $id_subtopic;
	}
	
	public function SubtopicArticles($id_subtopic,$sort_by=3)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article,headline,subhead,UNIX_TIMESTAMP(written) AS written_ts,show_date,id_user,author,
			id_topic,halftitle,show_author,available,'article' AS item_type,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
			FROM articles a INNER JOIN articles_subhead USING (id_article)
			WHERE id_subtopic='$id_subtopic' AND approved=1 ";
		$sqlstr .= $this->LatestOrderBy($sort_by);
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function SubtopicChildren($id_parent)
	{
		$tree = $this->tree;
		$children = array();
		$child_counter=0;
		for($i=0;$i<count($tree);$i++)
		{
			if ($tree[$i]['id_parent']==$id_parent)
			{
				$children[$child_counter]['id_subtopic']	= $tree[$i]['id_subtopic'];
				$children[$child_counter]['name']		= $tree[$i]['name'];
				$children[$child_counter]['id_parent']	= $tree[$i]['id_parent'];
				$children[$child_counter]['visible']		= $tree[$i]['visible'];
				$children[$child_counter]['id_type']		= $tree[$i]['id_type'];
				$children[$child_counter]['link']		= $tree[$i]['link'];
				$children[$child_counter]['seq']		= $tree[$i]['seq'];
				$children[$child_counter]['id_item']		= $tree[$i]['id_item'];
				$children[$child_counter]['id_subitem']	= $tree[$i]['id_subitem'];
				$children[$child_counter]['id_subitem_id']	= $tree[$i]['id_subitem_id'];
				$children[$child_counter]['sort_by']	= $tree[$i]['sort_by'];
				$child_counter = $child_counter + 1;
			}
		}
		return $children;
	}
	
	private function SubtopicChildrenAll($id_parent)
	{
		$children = $this->SubtopicChildren($id_parent);
		foreach($children as $child)
		{
			$children = array_merge($children,$this->SubtopicChildrenAll($child['id_subtopic']));
		}
		return $children;
	}

	private function SubtopicData($id_subtopic)
	{
		$subtopic_data = array();
		if(is_array($this->tree))
		{
			reset($this->tree);
			foreach($this->tree as $subtopic)
			{
				if ($subtopic['id_subtopic']==$id_subtopic)
				{
					$subtopic_data['id_subtopic']	= $subtopic['id_subtopic'];
					$subtopic_data['name']			= $subtopic['name'];
					$subtopic_data['id_parent']		= $subtopic['id_parent'];
					$subtopic_data['visible']		= $subtopic['visible'];
					$subtopic_data['id_type']		= $subtopic['id_type'];
					$subtopic_data['link']			= $subtopic['link'];
					$subtopic_data['seq']			= $subtopic['seq'];
					$subtopic_data['id_item']		= $subtopic['id_item'];
					$subtopic_data['id_subitem']	= $subtopic['id_subitem'];
					$subtopic_data['id_subitem_id']	= $subtopic['id_subitem_id'];
					$subtopic_data['sort_by']		= $subtopic['sort_by'];
				}
			}		
		}
		return $subtopic_data;
	}

	public function SubtopicDocs($id_subtopic)
	{
		$db =& Db::globaldb();
		$docs = array();
		$sqlstr = "SELECT id_doc FROM docs_articles WHERE id_subtopic_form=$id_subtopic ";
		$db->QueryExe($docs, $sqlstr);
		return $docs;
	}
	
	public function SubtopicDelete( $id_subtopic )
	{
		$row = $this->SubtopicGet($id_subtopic);
		$this->queue->JobInsert($this->queue->types['subtopic'],$id_subtopic,"delete");
		if ($row['id_parent']>0)
			$this->queue->JobInsert($this->queue->types['subtopic'],$row['id_parent'],"update");
		$this->queue->JobInsert($this->queue->types['map'],$this->id,"",$this->id);
		if ($this->SubtopicIsInMenu($id_subtopic,$row['id_parent']))
			$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( "DELETE FROM subtopics WHERE id_subtopic='$id_subtopic'" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_subtopic,$o->types['subtopic']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['subtopic'],$id_subtopic);
		include_once(SERVER_ROOT."/../classes/irl.php");	
		$irl = new IRL();
		$irl->FriendlyUrlDelete($o->types['subtopic'],$id_subtopic,$this->id);
		$this->h->HistoryAdd($this->h->types['subtopic'],$id_subtopic,$this->h->actions['delete']);
	}

	public function SubtopicGallery($id_gallery)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic FROM subtopics
		WHERE id_gallery='$id_gallery' AND id_topic='$this->id'";
		$db->query_single($row,$sqlstr);
		return $row['id_subtopic'];
	}

	public function SubtopicGalleryImages($id_gallery,$id_subtopic,$zoomable=false)
	{
		$simages = array();
		if ($id_gallery>0)
		{
			include_once(SERVER_ROOT."/../classes/gallery.php");
			$g = new Gallery($id_gallery);
			$images = $g->Images();
			foreach($images as $image)
			{
				$image['id_subtopic'] = $id_subtopic;
				if($zoomable)
				{
					$image['zoom'] = "2";
					if($g->show_orig)
						$image['show_orig'] = "1";
				}
				$simages[] = $image;
			}
		}
		return $simages;
	}

	public function SubtopicGet($id_subtopic)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT s1.name as name,s1.description,s1.id_parent,s1.seq,s1.link,s1.header,s1.id_type,
			s1.visible,s1.id_item,s1.id_subitem,s1.id_subitem_id,s1.footer,s1.id_image,s1.sort_by,
			COUNT(id_article) AS tot_articles,s1.link,s1.id_subtopic,s1.params,s1.id_topic,s1.records_per_page,
			s1.has_feed
			FROM subtopics s1
			LEFT JOIN articles ON s1.id_subtopic=articles.id_subtopic AND articles.approved=1
			WHERE s1.id_subtopic='$id_subtopic' GROUP BY s1.id_subtopic";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function SubtopicImageSizeSet($id_subtopic)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$row = $this->SubtopicGet($id_subtopic);
		$params = $v->Deserialize($row['params']);
		if($row['id_image']>0)
		{
			include_once(SERVER_ROOT."/../classes/image.php");
			$im = new Image($row['id_image']);
			$image_size = (int)$params['image_size'];
			$sizes = $im->ImageSize($image_size,$row['id_image']);
			if($sizes['width']>0 && $sizes['height']>0)
			{
				$params['width'] = $sizes['width'];
				$params['height'] = $sizes['height'];
			}
			$params['image_size'] = $image_size;
			$params['format'] = $sizes['format'];
		}
		else 
		{
			unset($params['width']);
			unset($params['height']);
			unset($params['image_size']);
			unset($params['format']);
		}
		$ser_params = $v->Serialize($params);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( "UPDATE subtopics SET params='$ser_params' WHERE id_subtopic='$id_subtopic' " );
		Db::finish( $res, $db);
	}

	public function SubtopicImageUpdate($id_subtopic,$id_image,$update=true)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$sqlstr = "UPDATE subtopics SET id_image='$id_image' WHERE id_subtopic='$id_subtopic'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($update)
		{
			$this->SubtopicImageSizeSet($id_subtopic);
			$this->queue->JobInsert($this->queue->types['subtopic'],$id_subtopic,"update");
			$subtopic = $this->SubtopicGet($id_subtopic);
			if($subtopic['visible']!="4")
				$this->queue->JobInsert($this->queue->types['subtopic'],$subtopic['id_parent'],"update");
		}
	}

	public function SubtopicInsert($name,$description,$id_parent,$header,$footer,$id_type,$visible,$id_item,$id_subitem,$id_subitem_id,$link,$keywords,$params,$sort_by,$records_per_page)
	{
		if($id_type=="3")
			$sort_by = "0";
		$is_published = $this->SubtopicPublishable($id_type)? 0 : 1;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$id_subtopic = $db->nextId( "subtopics", "id_subtopic" );
		$seq = $this->SubtopicNextSeq($id_parent);
		$sqlstr = "INSERT INTO subtopics (id_subtopic,name,description,id_parent,id_topic,seq,header,footer,id_type,visible,id_item,id_subitem,id_subitem_id,link,params,sort_by,is_published,records_per_page)
				VALUES ($id_subtopic,'$name','$description',$id_parent,$this->id,$seq,'$header','$footer',$id_type,$visible,$id_item,$id_subitem,'$id_subitem_id','$link','$params',$sort_by,$is_published,$records_per_page)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_subtopic, $o->types['subtopic']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($visible<3 && $this->visible)
			$s->IndexQueueAdd($o->types['subtopic'],$id_subtopic,$this->id,$this->id_group,1);
		else 
			$s->ResourceRemove($o->types['subtopic'],$id_subtopic);
		if ($visible!="4")
		{
			$this->queue->JobInsert($this->queue->types['subtopic'],$id_subtopic,"update");
			if ($id_parent>0)
				$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
			if($visible=="1")
			{
				$this->queue->JobInsert($this->queue->types['map'],$this->id,"",$this->id);
				if ($this->SubtopicIsInMenu($id_subtopic,$id_parent))
					$this->queue->JobInsert($this->queue->types['all'],$this->id,"");				
			}
		}
		$this->h->HistoryAdd($this->h->types['subtopic'],$id_subtopic,$this->h->actions['create']);
		return $id_subtopic;
	}

	public function SubtopicIsChild($id_candidate,$id_subtopic, $recurse=true)
	{
		$child = 0;
		$children = array();
		$child_counter=0;
		reset($this->tree);
		foreach($this->tree as $subtopic)
		{
			if ($subtopic['id_parent']==$id_subtopic)
			{
				$children[$child_counter] = $subtopic['id_subtopic'];
				$child_counter ++;
			}
		}
		for($i=0;$i<count($children);$i++)
		{
			if ($children[$i]==$id_candidate)
				$child = 1;
			else
			{
				if ($recurse)
					$child = $this->SubtopicIsChild($id_candidate,$children[$i]);
			}
			if ($child==1)
				break;
		}
		return $child;
	}

	public function SubtopicIsInMenu($id_subtopic,$id_parent)
	{
		$is_in_menu = FALSE;
		if ($id_parent>0)
		{
			$this->LoadTree();
			if ($this->row['menu_depth']==2)
			{
				$children = $this->SubtopicChildren(0);
				foreach($children as $child)
				{
					$is_child = $this->SubtopicIsChild($id_subtopic,$child['id_subtopic']);
					if ($is_child>0)
						$is_in_menu = TRUE;
				}
			}
		}
		else
			$is_in_menu = TRUE;
		return $is_in_menu;
	}
	
	public function SubtopickeywordFilterTypes()
	{
		include_once(SERVER_ROOT."/../classes/modules.php");
		$resource_types = array();
		$resource_types[] = 5; //article
		if(Modules::IsActive(8))
			$resource_types[] = 4; //org
		$resource_types[] = 13; //book
		$resource_types[] = 14; //link
		$resource_types[] = 23; //video
		return $resource_types;
	}

	public function SubtopicLatest($id_subtopic,$limit,$recurse=0,$sort_by=0,$not_only_latest=0,$visible_only=true)
	{
		if ($recurse>0)
		{
			$this->LoadTree();
			$children = $this->SubtopicChildrenAll($id_subtopic);
			$sql_cond = " (a.id_subtopic='$id_subtopic' ";
			foreach($children as $child)
			{
				if ($child['visible']<3)
				{
					$sql_cond .= " OR a.id_subtopic={$child['id_subtopic']} ";
				}
			}
			$sql_cond .= ")";
		}
		else
			$sql_cond = "a.id_subtopic='$id_subtopic'";
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_article,headline,subhead,halftitle,UNIX_TIMESTAMP(written) AS written_ts,available,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,show_author,show_date,id_user,author,author_notes,
			'article' AS item_type,a.id_topic,s.name AS subtopic_name,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language,a.highlight
			FROM articles a
			INNER JOIN articles_subhead USING(id_article)
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic AND $sql_cond
			WHERE a.id_topic='$this->id'
			AND a.approved=1 ";
		if($visible_only)
			$sqlstr .= " AND s.visible<3 ";
		if($not_only_latest==0)
			$sqlstr .= " AND a.show_latest=1 ";
		$sqlstr .= $this->LatestOrderBy($sort_by);
		$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function SubtopicMove($id_subtopic,$dir)
	{
		$this->LoadTree();
		$subtopic_data = $this->SubtopicData($id_subtopic);
		$id_parent = $subtopic_data['id_parent'];
		$children = $this->SubtopicChildren($id_parent);
		$counter = 1;
		$id_seq_prev = 0;
		if (!($dir==1 && $id_subtopic==$children[0]['id_subtopic']) && !($dir==0 && $id_subtopic==$children[sizeof($children)-1]['id_subtopic']))
		{
			foreach($children as $child)
			{
				if ($child['id_subtopic']==$id_subtopic && $dir==1)
				{
					$this->SubtopicUpdateSeq( $id_subtopic , ($child['seq']-1) );
					$this->SubtopicUpdateSeq( $id_seq_prev , $child['seq'] );
				}
				if ($id_seq_prev==$id_subtopic && $dir==0)
				{
					$this->SubtopicUpdateSeq( $child['id_subtopic'] , ($child['seq']-1) );
					$this->SubtopicUpdateSeq( $id_seq_prev , $child['seq'] );
				}
				$id_seq_prev = $child['id_subtopic'];
				$counter ++;
			}
		}
		$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
		$this->queue->JobInsert($this->queue->types['map'],$this->id,"",$this->id);
		if ($this->SubtopicIsInMenu($id_subtopic,$id_parent))
			$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
	}

	private function SubtopicNextSeq($id_parent)
	{
		$this->LoadTree();
		$children = $this->SubtopicChildren($id_parent);
		return (count($children)+1);
	}

	public function SubtopicParents($id_subtopic, &$parents)
	{
		$subtopic = $this->SubtopicData($id_subtopic);
		$parents[] = array(	'id_subtopic'	=> $subtopic['id_subtopic'],
					'name'		=> $subtopic['name'],
					'id_parent'	=> $subtopic['id_parent'],
					'link'		=> $subtopic['link'],
					'id_type'	=> $subtopic['id_type']);
		if ($subtopic['id_parent']!=0)
			$parents = $this->SubtopicParents($subtopic['id_parent'], $parents);
		return $parents;
	}

	public function SubtopicSubtopics($id_subtopic)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic,name,description,id_image,id_type,id_item,id_subitem,id_subitem_id,link,params FROM subtopics 
		WHERE id_parent='$id_subtopic' AND visible=1 ORDER BY seq";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Subtopics()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic,id_type,description,header,footer,id_image,name,id_item,id_subitem,id_subitem_id,params,sort_by,records_per_page,has_feed,visible
			FROM subtopics WHERE id_topic='$this->id' AND visible<4
			ORDER BY id_subtopic";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function SubtopicsByType($id_type)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic,name,id_type,description,header,footer,id_image,id_item,id_subitem,id_subitem_id,params,sort_by
			FROM subtopics WHERE id_topic='$this->id' AND id_type='$id_type' 
			ORDER BY id_subtopic";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function SubtopicPublishable($id_subtopic_type)
	{
		$publishable = true;
		switch($id_subtopic_type)
		{
			case $this->subtopic_types['none']:
			case $this->subtopic_types['url']:
			case $this->subtopic_types['contact']:
			case $this->subtopic_types['campaign']:
			case $this->subtopic_types['forum']:
			case $this->subtopic_types['poll']:
			case $this->subtopic_types['calendar']:
			case $this->subtopic_types['module']:
			case $this->subtopic_types['quotes']:
			case $this->subtopic_types['dynamic']:
				$publishable = false;
			break;
			default:
				$publishable = true;
		}
		return $publishable;
	}
	
	public function SubtopicPublishedSet($id_subtopic,$published=true)
	{
		$is_published = $published? 1 : 0;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( "UPDATE subtopics SET is_published='$is_published' WHERE id_subtopic=$id_subtopic" );
		Db::finish( $res, $db);
	}

	public function SubtopicPublishedSetAll()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( "UPDATE subtopics SET is_published='1' WHERE id_topic=$this->id" );
		Db::finish( $res, $db);
	}

	public function SubtopicType($id_subtopic)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_type,visible FROM subtopics WHERE id_subtopic='$id_subtopic'";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function SubtopicUpdate($id_subtopic,$name,$description,$id_parent,$id_parent_old,$header,$footer,$id_type,$visible,$id_item,$id_subitem,$id_subitem_id,$link,$keywords,$params,$sort_by,$records_per_page,$has_feed,$furl)
	{
		if($id_type=="3")
			$sort_by = "0";
		$this->queue->JobInsert($this->queue->types['subtopic'],$id_subtopic,"update");
		$row = $this->SubtopicGet($id_subtopic);
		$is_in_menu = $this->SubtopicIsInMenu($id_subtopic,$id_parent_old);
		$db =& Db::globaldb();
		if (($name!=$db->SqlQuote($row['name']) || $id_parent!=$row['id_parent']) && !$is_in_menu && $visible!="4")
			$this->SubtopicUpdateRecurse($id_subtopic, "update",$id_type);
		if (($records_per_page!=$row['records_per_page'] || $sort_by!=$row['sort_by'] || $has_feed!=$row['has_feed']) && $visible!="4")
			$this->SubtopicUpdateRecurse($id_subtopic, "update",$id_type,false);
		if ($id_parent>0 && $visible=="1" && ($name!=$db->SqlQuote($row['name']) || $description!=$db->SqlQuote($row['description']) || $id_type!=$row['id_type']) || $link!=$db->SqlQuote($row['link']))
			$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
		if ($id_parent!=$row['id_parent'])
		{
			if ($row['id_parent']>0)
				$this->queue->JobInsert($this->queue->types['subtopic'],$row['id_parent'],"update");
			if ($id_parent>0)
				$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
		}
		if ($row['visible']<3 && $visible>2)
		{
			$this->SubtopicUpdateRecurse($id_subtopic,"delete",$id_type);
			$this->SubtopicUpdateReindexItems($id_subtopic,"delete",$id_type);
			if ($id_parent > 0)
				$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
		}
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		include_once(SERVER_ROOT."/../classes/irl.php");	
		$irl = new IRL();
		$furl_old = $irl->FriendlyUrlGet($o->types['subtopic'],$id_subtopic);
		if ($row['visible']>2 && $visible<3)
			$this->SubtopicUpdateReindexItems($id_subtopic,"update",$id_type);
		if ($name!=$db->SqlQuote($row['name']) || $id_parent!=$row['id_parent'] || $visible!=$row['visible'])
			$this->queue->JobInsert($this->queue->types['latest'],$this->id,"");
		if ($is_in_menu && ($name!=$db->SqlQuote($row['name']) || $visible!=$row['visible'] || $id_type!=$row['id_type']  || $link!=$db->SqlQuote($row['link']) || $furl!=$db->SqlQuote($furl_old['furl']) ))
			$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
		if ($id_parent!=$id_parent_old)
		{
			$seq = $this->SubtopicNextSeq($id_parent);
			$cond = ",seq=$seq";
		}
		$sqlstr = "UPDATE subtopics
				SET name='$name',description='$description',header='$header',id_parent=$id_parent,
				id_topic=$this->id,id_type=$id_type,visible=$visible,footer='$footer',sort_by=$sort_by,
				id_item=$id_item,id_subitem=$id_subitem,id_subitem_id='$id_subitem_id',link='$link',params='$params',
				records_per_page=$records_per_page,has_feed='$has_feed' $cond
				WHERE id_subtopic='$id_subtopic'";
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($row['id_image']>0)
			$this->SubtopicImageSizeSet($id_subtopic);
		if($id_parent!=$id_parent_old)
		{
			$is_in_menu2 = $this->SubtopicIsInMenu($id_subtopic,$id_parent);
			if ($is_in_menu || $is_in_menu2)
				$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
		}
		$irl->FriendlyUrlStore($o->types['subtopic'],$id_subtopic,$furl,$this->id,$visible!=4 && $id_type!=$this->subtopic_types['url']);
		if($furl!="" || $furl_old['furl']!="")
		{
			$this->queue->JobInsert($this->queue->types['subtopic'],$id_parent,"update");
		}
		$o->InsertKeywords($keywords, $id_subtopic, $o->types['subtopic']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($visible<3 && $this->visible)
			$s->IndexQueueAdd($o->types['subtopic'],$id_subtopic,$this->id,$this->id_group,1);
		else 
			$s->ResourceRemove($o->types['subtopic'],$id_subtopic);
		if($visible!=$row['visible'] && $this->visible)
		{
			// TODO this is bad, we're reindexing the whole topic content for just a change in subtopic visibility
			$s->Reindex(0,2,$this->id);
		}
		$this->h->HistoryAdd($this->h->types['subtopic'],$id_subtopic,$this->h->actions['update']);
		if ($id_parent!=$id_parent_old)
			$this->Reshuffle();
	}
	
	private function SubtopicUpdateRecurse($id_subtopic, $action, $id_type, $recurse=true)
	{
		$this->queue->JobInsert($this->queue->types['subtopic'], $id_subtopic, $action);
		if($recurse)
		{
			$this->SubtopicUpdateRecurseItems($id_subtopic, $action, $id_type);
			$children = $this->SubtopicChildren($id_subtopic);
			foreach($children as $child)
				$this->SubtopicUpdateRecurse($child['id_subtopic'], $action, $child['id_type']);
		}
	}
	
	private function SubtopicUpdateRecurseItems($id_subtopic, $action, $id_type)
	{
		if ($id_type==1 || $id_type==2)
		{
			$articles = $this->SubtopicArticles($id_subtopic);
			foreach($articles as $article)
				$this->queue->JobInsert($this->queue->types['article'],$article['id_article'], $action);
		}
	}
	
	private function SubtopicUpdateReindexItems($id_subtopic, $action, $id_type)
	{
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if ($id_type==1 || $id_type==2)
		{
			$articles = $this->SubtopicArticles($id_subtopic);
			foreach($articles as $article)
			{
				if($action=="delete")
					$s->ResourceRemove(5,$article['id_article']);
				if($action=="update" && $article['available'])
					$s->IndexQueueAdd(5,$article['id_article'],$this->id,$this->id_group,1);
			}
		}
	}
	
	private function SubtopicUpdateSeq($id_subtopic,$seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$res[] = $db->query( "UPDATE subtopics SET seq=$seq WHERE id_subtopic='$id_subtopic'" );
		Db::finish( $res, $db);
	}

	public function Templates($id_resource)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_template,name,hide_fields,'template' AS item_type,default_subtopic
			FROM templates
			WHERE id_res=$id_resource AND (id_topic='$this->id' OR (id_topic=0 AND id_group='$this->id_group') OR (id_topic=0 AND id_group=0) )
			ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TextGet($type)
	{
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		$th = new TextHelper();
		$text = $this->TextGetRow($th->types[$type]);
		return $text;
	}
	
	public function TextGetRow($id_type)
	{
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		$th = new TextHelper();
		return $th->ContentGetByType($id_type,$this->id);
	}
	
	private function TopicGet()
	{
		$topic = array();
		$db =& Db::globaldb();
		$db->query_single($topic,"SELECT id_topic,t.name AS name,t.description AS description,path,id_style,
			home_type,menu_depth,id_image,show_path,articles_per_page,t.visible,t.id_group AS id_group,
			id_language,tg.name AS group_name,id_article_home,show_latest,domain,share_images,share_docs,show_print,
			edit_layout,tg.visible AS group_visible,t.protected,t.seq,t.awstats,t.id_frequency,t.comments,t.moderate_comments,
			t.profiling,t.campaigns_open,t.custom_visibility,t.home_sort_by,t.wysiwyg,t.temail,t.newsletter_format,
			t.home_keywords,t.associated_image_size,t.friendly_url,t.newsletter_people,t.archived
			FROM topics t
			LEFT JOIN topics_groups tg USING(id_group)
			WHERE t.id_topic='$this->id'");
		return $topic;
	}

	public function TopicImageUpdate($id_image)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$sqlstr = "UPDATE topics SET id_image='$id_image' WHERE id_topic='$this->id'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function TopicInsert($name,$description,$keywords,$path,$id_language,$visible,$id_group,$show_latest,$domain,$share_images,$share_docs,$edit_layout,$protected,$awstats,$comments,$moderate_comments,$profiling,$campaigns_open,$custom_visibility,$temail,$home_keywords,$friendly_url,$archived)
	{
		$path = strtolower($path);
		$seq = $this->TopicNextSeq($id_group);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$this->id = $db->nextId( "topics", "id_topic" );
		if(strlen($path)==0)
			$path = "topic" . $this->id;
		$sqlstr = "INSERT INTO topics (id_topic,name,description,domain,visible,id_group,id_language,show_latest,share_images,share_docs,edit_layout,protected,seq,awstats,comments,moderate_comments,profiling,campaigns_open,custom_visibility,temail,home_keywords,friendly_url,archived)
				VALUES ($this->id,'$name','$description','".strtolower($domain)."',$visible,$id_group,$id_language,$show_latest,$share_images,$share_docs,$edit_layout,$protected,$seq,'$awstats','$comments','$moderate_comments','$profiling','$campaigns_open','$custom_visibility','$temail','$home_keywords','$friendly_url','$archived')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->__construct($this->id);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$path_def = ($this->TopicPathIsOk($path))? $path : $v->Uid();
		$this->TopicUpdatePath($path_def);
		$this->path = $path_def;
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$store = ($ini->Get("topic_store")!="")? $ini->Get("topic_store") : "Deposit";
		$myparams = $v->Serialize(array());
		$this->SubtopicInsert($store,"",0,"","",0,4,0,0,0,"","",$myparams,1,0);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$this->UserInsert( $session->Get('current_user_id'), 1, 1, 0 );
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['topic']);
		$this->h->HistoryAdd($this->h->types['topic'],$this->id,$this->h->actions['create']);
		$this->queue->JobInsert($this->queue->types['all'],$this->id,"update");
		if($domain!="")
			$this->queue->JobInsert($this->queue->types['xdomain'],$this->id,"");
		if($visible)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd($o->types['topic'],$this->id,0,$this->id_group,1);
			$this->queue->JobInsert($this->queue->types['map'],$this->id,"");
		}
		return $this->id;
	}
	
	private function TopicNextSeq($id_group)
	{
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$tt->gh->LoadTree();
		$children = $tt->gh->GroupItems($id_group);
		return (count($children)+1);
	}

	private function TopicPathIsOk($new_path)
	{
		$move = true;
		include_once(SERVER_ROOT."/../classes/adminhelper.php");
		$ah = new AdminHelper;
		if (substr($new_path,0,1)=="_")
		{
			$ah->MessageSet("topic_path_warn");
			$move = false;
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/config.php");
			$conf = new Configuration();
			$blackdirs = $conf->Get("blackdirs");
			if(in_array($new_path,$blackdirs))
			{
				$move = false;
				$ah->MessageSet("topic_path_change_fail",array($new_path));
			}
			else 
			{
				include_once(SERVER_ROOT."/../classes/file.php");
				$fm = new FileManager;
				if ($fm->Exists("pub/$new_path"))
				{
					$move = false;
					$ah->MessageSet("topic_path_change_fail",array($new_path));
				}
				else 
				{
					include_once(SERVER_ROOT."/../classes/irl.php");
					$irl = new IRL();
					$redirect = $irl->RedirectExists($new_path);
					if($redirect>0)
					{
						$move = false;
						$ah->MessageSet("topic_path_warn_redirect",array("/admin/redirect.php?id={$redirect}"));
					}
				}
			}
		}
		if($move)
		{
			if($this->row['domain']!="")
				$ah->MessageSet("topic_path_change_domain",array($this->path,$new_path));
			else
				$ah->MessageSet("topic_path_change",array($this->path,$new_path));
		}
		return $move;
	}
	
	public function TopicUpdate($name,$description,$keywords,$path,$id_language,$visible,$id_group,$show_latest,$domain,$share_images,$share_docs,$edit_layout,$protected,$awstats,$comments,$moderate_comments,$profiling,$campaigns_open,$custom_visibility,$temail,$home_keywords,$friendly_url,$archived)
	{
		$db =& Db::globaldb();
		$path = strtolower($path);
		if(strlen($path)==0)
			$path = "topic" . $this->id;
		if ($this->row['path']!=$path)
		{
			if ($this->Move($path))
			{
				$this->TopicUpdatePath($path);
			}
		}
		if ($db->SqlQuote($this->row['name'])!=$name || $this->row['id_language']!=$id_language || $this->row['path']!=$path || $this->row['domain']!=$domain || $this->row['comments']!=$comments)
			$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
		if ($db->SqlQuote($this->row['name'])!=$name || $this->row['show_latest']!=$show_latest || $this->row['path']!=$path)
			$this->queue->JobInsert($this->queue->types['latest'],$this->id,"");
		if ($this->row['visible']!=$visible || $this->row['id_group']!=$id_group || $this->row['domain']!=$domain || $this->row['show_latest']!=$show_latest || $db->SqlQuote($this->row['name'])!=$name || $db->SqlQuote($this->row['description'])!=$description || $this->row['path']!=$path)
			$this->queue->JobInsert($this->queue->types['map'],$this->id,"");
		if ($this->row['visible']!=$visible || $this->row['show_latest']!=$show_latest || $this->row['friendly_url']!=$friendly_url || $db->SqlQuote($this->row['name'])!=$name || $this->row['path']!=$path || $this->row['protected']!=$protected)
			$this->queue->JobInsert($this->queue->types['homepage'],$this->id,"");
		if($domain!=$this->row['domain'])
		{
			$this->queue->JobInsert($this->queue->types['xdomain'],$this->id,"");
		}
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		if ($this->row['visible']=="0" && $visible=="1")
		{
			$s->Reindex(0,1,$this->id);
		}
		if ($this->row['visible']=="1" && $visible=="0")
		{
			$s->TopicRemove($this->id);
		}
		$db->begin();
		$db->lock( "topics" );
		$sqlstr = "UPDATE topics SET name='$name',description='$description',
			visible=$visible,id_group=$id_group,id_language=$id_language,show_latest=$show_latest,share_images=$share_images,
			share_docs=$share_docs,domain='".strtolower($domain)."',edit_layout=$edit_layout,protected=$protected,
			awstats='$awstats',comments='$comments',moderate_comments='$moderate_comments',profiling='$profiling',
			campaigns_open='$campaigns_open',custom_visibility='$custom_visibility',temail='$temail',
			home_keywords='$home_keywords',friendly_url='$friendly_url',archived='$archived' 
				WHERE id_topic='$this->id'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		if ($this->row['friendly_url']>0 && $friendly_url=="0")
			$irl->FriendlyUrlDeleteTopic($this->id);
		if ($this->row['friendly_url']=="0" && $friendly_url>0)
			$irl->FriendlyUrlGenerateTopic($this->id);
		if ($domain!=$this->row['domain'] || $this->row['path']!=$path)
			$irl->FriendlyUrlRegenerateTopic($this->id);
		$o->InsertKeywords($keywords, $this->id, $o->types['topic']);
		if($visible && $this->visible)
			$s->IndexQueueAdd($o->types['topic'],$this->id,0,$this->id_group,1);
		else 
			$s->ResourceRemove($o->types['topic'],$this->id);
		$this->h->HistoryAdd($this->h->types['topic'],$this->id,$this->h->actions['update']);
	}

	public function TopicUpdateGraphic($id_style,$articles_per_page,$menu_depth,$home_type,$show_path,$show_print,$home_sort_by,$wysiwyg,$associated_image_size)
	{
		$db =& Db::globaldb();
		if ($this->row['id_style']!=$id_style || $this->row['articles_per_page']!=$articles_per_page || $this->row['menu_depth']!=$menu_depth || $this->row['show_print']!=$show_print || $this->row['associated_image_size']!=$associated_image_size)
			$this->queue->JobInsert($this->queue->types['all'],$this->id,"");
		if ($this->row['home_type']!=$home_type || $this->row['show_path']!=$show_path || $this->row['home_sort_by']!=$home_sort_by )
			$this->queue->JobInsert($this->queue->types['topic_home'],$this->id,"");
		if ($this->row['home_type']!=$home_type)
			$this->HomepageReset();
		$db->begin();
		$db->lock( "topics" );
		$sqlstr = "UPDATE topics SET id_style=$id_style,articles_per_page=$articles_per_page,menu_depth=$menu_depth,
			home_type=$home_type,show_path=$show_path,show_print=$show_print,home_sort_by=$home_sort_by,wysiwyg=$wysiwyg,
			associated_image_size=$associated_image_size
				WHERE id_topic='$this->id'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($this->row['associated_image_size']!=$associated_image_size)
		{
			$this->TopicUpdateAssociatedImages($associated_image_size);
		}
	}
	
	private function TopicUpdateAssociatedImages($size)
	{
		include_once(SERVER_ROOT."/../classes/image.php");
		$im = new Image(0);
		$articles = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_article,id_image FROM articles WHERE id_topic='{$this->id}' AND id_image>0";
		$db->QueryExe($articles, $sqlstr);
		if(count($articles)>0)
		{
			$db->begin();
			$db->lock( "articles" );
			foreach($articles as $article)
			{
				$im->id = $article['id_image'];
				$sizes = $im->ImageSize($size,$article['id_image']);
				$sqlstr = "UPDATE articles SET image_size='$size',image_width='{$sizes['width']}',image_height='{$sizes['height']}' WHERE id_article='{$article['id_article']}' ";
				$res[] = $db->query($sqlstr);
			}
			Db::finish( $res, $db);
		}
	}

	private function TopicUpdatePath($path)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$res[] = $db->query( "UPDATE topics SET path='$path' WHERE id_topic='$this->id'" );
		Db::finish( $res, $db);
	}

	public function TopicUsers( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT users.id_user,name,is_admin,is_contact
		FROM users
		INNER JOIN topic_users ON users.id_user=topic_users.id_user WHERE topic_users.id_topic='$this->id' ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function TreeClone($id_topic_to)
	{
		$subtopics = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic
			FROM subtopics WHERE id_topic='$this->id' AND id_type>0 AND id_parent=0
			ORDER BY seq";
		$db->QueryExe($subtopics, $sqlstr);
		foreach($subtopics as $subtopic)
		{
			$this->SubtopicCloneRecurse($subtopic['id_subtopic'],0,$id_topic_to);
		}
	}
	
	private function SubtopicCloneRecurse($id_subtopic,$id_parent,$id_topic_new)
	{
		$subtopic = $this->SubtopicGet($id_subtopic);
		$db =& Db::globaldb();
		$name = $db->SqlQuote($subtopic['name']);
		$description = $db->SqlQuote($subtopic['description']);
		$header = $db->SqlQuote($subtopic['header']);
		$footer = $db->SqlQuote($subtopic['footer']);
		$id_type = $db->SqlQuote($subtopic['id_type']);
		$visible = $db->SqlQuote($subtopic['visible']);
		$id_item = $db->SqlQuote($subtopic['id_item']);
		$id_subitem = $db->SqlQuote($subtopic['id_subitem']);
		$id_subitem_id = $db->SqlQuote($subtopic['id_subitem_id']);
		$link = $db->SqlQuote($subtopic['link']);
		$keywords = "";
		$params = $db->SqlQuote($subtopic['params']);
		$sort_by = $db->SqlQuote($subtopic['sort_by']);
		$records_per_page = $db->SqlQuote($subtopic['records_per_page']);
		$t = new Topic($id_topic_new);
		$id_subtopic_new = $t->SubtopicInsert($name,$description,$id_parent,$header,$footer,$id_type,$visible,$id_item,$id_subitem,$id_subitem_id,$link,$keywords,$params,$sort_by,$records_per_page);
		$children = $this->SubtopicChildren($id_subtopic);
		if(count($children)>0)
		{
			foreach($children as $child)
			{
				$this->SubtopicCloneRecurse($child['id_subtopic'],$id_subtopic_new,$id_topic_new);
			}
		}
	}
	
	public function UserDelete( $id_user )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_users" );
		$res[] = $db->query( "DELETE FROM topic_users WHERE id_topic='$this->id' AND id_user='$id_user'" );
		Db::finish( $res, $db);
	}

	public function UserInsert( $id_user, $is_admin, $is_contact, $pending_notify )
	{
		if ($id_user>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "topic_users" );
			$res[] = $db->query( "INSERT INTO topic_users (id_user,is_admin,is_contact,id_topic,pending_notify) VALUES ($id_user,$is_admin,$is_contact,$this->id,$pending_notify)" );
			Db::finish( $res, $db);
		}
	}

	public function UserNotify($id_user)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT pending_notify FROM topic_users WHERE id_user=$id_user AND id_topic='$this->id'");
		return (int)$row['pending_notify'];
	}

	public function UserUpdate( $id_user, $is_admin, $is_contact, $pending_notify )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_users" );
		$res[] = $db->query( "UPDATE topic_users SET is_admin=$is_admin,is_contact=$is_contact,pending_notify=$pending_notify WHERE id_user='$id_user' AND id_topic='$this->id'" );
		Db::finish( $res, $db);
	}

	public function Users()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT u.id_user,name FROM users u INNER JOIN topic_users t USING(id_user) 
			WHERE u.id_user>0 AND u.active=1 AND t.id_topic='$this->id' ORDER BY u.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UsersDelete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_users" );
		$res[] = $db->query( "DELETE FROM topic_users WHERE id_topic='$this->id'" );
		Db::finish( $res, $db);
	}

}
?>
