<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

class Scheduler
{
	public $actions = array();
	public $executed;
	public $notify;
	
	/** 
	 * @var PublishManager */
	public $pm;

	private $time;
	
	function __construct()
	{
		$this->actions = $this->SchedulableActions();
		include_once(SERVER_ROOT."/../classes/publishmanager.php");
		$this->pm = new PublishManager();
		$this->pm->scheduler = true;
		$ini = new Ini();
		$this->notify = $ini->Get("sched_notify");
		$this->time = 0;
		$this->executed = 0;
	}
	
	function __destruct()
	{
		unset($this->ini);
		if(isset($this->pm))
			$this->pm->__destruct();
		unset($this->pm);
	}
	
	public function HeavyAction($timestamp)
	{
		$messages = array();
		include_once(SERVER_ROOT."/../classes/queue.php");
		$queue = new Queue(0);
		$row = $queue->NextJobAll();
		if($row['id_topic']>0)
		{
			include_once(SERVER_ROOT."/../classes/log.php");
			$log = new Log();
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->pm->force = true;
			$this->pm->TimeStart();
			$this->pm->TopicInit($row['id_topic']);
			$msg = "Topic full publish: {$this->pm->layout->topic->name}";
			$messages[] = $msg;
			if($row['id_user']>0)
			{
				$msg .= " (Requested by: {$row['name']})";
			}
			$log->Write("info",$msg);
			$notify = $this->pm->TopicEverything($row['id_topic']);
			if($notify) 
				$this->pm->NotifyUser($row['id_user'],$row['id_topic']);
			$this->pm->force = false;
			$log->Write("scheduler","Scheduler " . date("G:i",$timestamp),$this->pm->MessageGet(false));
		}
		// galleries
		$grows = $queue->JobsAllByType($queue->types['gallery']);
		foreach($grows as $grow)
		{
			$messages[] = "Publish gallery {$grow['id_item']}";
			$this->pm->JobExecute($grow);
		}
		if($this->pm->layout->IsModuleActive("media"))
		{
			include_once(SERVER_ROOT."/../classes/media.php");
			$conf = new Configuration();
			$encodings_per_cron_job = $conf->Get("encodings_per_cron_job");
			$media_messages = MediaEncoder::EncodeNext($encodings_per_cron_job);
			array_merge($messages,$media_messages);
		}
		$messages[] = "Indexing...";
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$indexed = $s->IndexQueue();
		$messages[] = "Indexed $indexed resources";
		include_once(SERVER_ROOT."/../classes/mail.php");
		$m = new Mail();
		if($m->process_bounces)
		{
			$messages[] = "Processing bounces";
			$m->ProcessBounces();
			$m->ResetBounces();
		}
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs();
		$mj->SendNextLot();
		$messages[] = "Processing mailjobs";
		if($this->pm->layout->IsModuleActive("mastodon")) {
		    $messages[] = "Running Mastodon jobs";
		    include_once(SERVER_ROOT."/../modules/mastodon.php");
		    $ma = new Mastodon();
		    $messages = array_merge($messages,$ma->Cron());
		}
		return $messages;
	}
	
	public function Execute($id_scheduler)
	{
		$start_time = time();
		$actions = $this->SchedulerActions($id_scheduler,true);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$va = new Varia();
		arsort($actions);
		$sc_actions = $this->SchedulableActions();
		foreach($actions as $action)
		{
			$this->SwitchBox($action['id_action'],$va->Deserialize($action['params']));
			$temp_time = time() - $start_time;
			$memory_usage = $this->MemoryUsage();
			$this->pm->MessageSet("schedule_execution",array($sc_actions[$action['id_action']],$temp_time,$memory_usage));
			$this->executed ++;
		}
		$this->time  += time() - $start_time;
		$this->pm->MessageSet("execution_time",array($this->time));
	}
	
	public function SchedulableActions()
	{
		$actions = array();
		$actions[0] = "PhPeace Main Node Exchange";
		$actions[1] = "PhPeace Software Update";
		$actions[2] = "Homepage, Sitemap, Feeds and others";
		$actions[3] = "Global newsletter";
		$actions[4] = "Topic queues";
		$actions[5] = "Topic newsletter";
		$actions[6] = "Clean logs";
		$actions[7] = "Banner update";
		$actions[8] = "Disable inactive users";
		$actions[9] = "XML Sitemaps generation";
		$actions[10] = "Check links";
		$actions[11] = "Feed importer";
		$actions[12] = "Galleries";
		$actions[13] = "Export quotes";
		$actions[14] = "Force Topic full publish";
		$actions[15] = "Notify pending articles";
        $actions[16] = "Site watch";
        $actions[17] = "Remove expired Rss feed items (> 30 days)"; 
        $actions[18] = "N/A"; 
        $actions[19] = "Resources' status changes"; 
        $actions[20] = "Import articles"; 
		return $actions;
	}
	
	public function SchedulableActionParams($id_action)
	{
		$params = $this->SchedulableActionsParams();
		return (isset($params[$id_action]) && is_array($params[$id_action]) && count($params[$id_action]>0))? $params[$id_action] : array();
	}

	private function SchedulableActionsParams()
	{
		$params = array();
		$params[10] = array('wday');
		$params[11] = array('url','id_topic','id_subtopic','id_template','approved','id_keyword');
		$params[14] = array('id_topic');
		$params[18] = array('id_type', 'path', 'email');
		$params[20] = array('url', 'key', 'keyword','limit','id_subtopic');
		return $params;
	}

	public function SchedulableActionStore($id_schedule,$params)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "schedules" );
		$sqlstr = "UPDATE schedules SET params='$params' WHERE id_schedule=$id_schedule";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function ScheduleActiveSwap($id_schedule,$active)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "schedules" );
		$sqlstr = "UPDATE schedules SET active=". (($active)? 0 : 1) . " WHERE id_schedule=$id_schedule";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function ScheduleDelete($id_schedule)
	{
		$row = $this->ScheduleGet($id_schedule);
		if ($row['id_user']>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "schedules" );
			$res[] = $db->query( "DELETE FROM schedules WHERE id_schedule=$id_schedule" );
			Db::finish( $res, $db);
		}
	}
	
	public function ScheduleGet($id_schedule)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT s.id_action,s.id_scheduler,s.params,s.id_user,s.active,s.params 
		FROM schedules s
		WHERE s.id_schedule=$id_schedule";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ScheduleGetByAction($id_action)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT s.id_action,s.id_scheduler,s.params,s.id_user,s.active,s.params 
		FROM schedules s
		WHERE s.id_action=$id_action ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ScheduleStore( $id_schedule,$id_action,$id_scheduler,$active,$id_user )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "schedules" );
		if ($id_schedule>0)
		{
			$sqlstr = "UPDATE schedules SET id_action=$id_action,id_scheduler=$id_scheduler,
			id_user=$id_user,active=$active
			WHERE id_schedule=$id_schedule";
		}
		else
		{
			$id_schedule = $db->nextId( "schedules", "id_schedule" );
			$sqlstr = "INSERT INTO schedules (id_schedule,id_action,id_scheduler,id_user,active) 
				VALUES ($id_schedule,$id_action,$id_scheduler,$id_user,$active)";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_schedule;
	}

	public function SchedulerActions($id_scheduler,$active_only)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_action,params,id_schedule,id_user,active FROM schedules WHERE id_scheduler='$id_scheduler' ";
		if($active_only)
			$sqlstr .= " AND active='1' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function SchedulerSlotAdd($hour,$minute)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_scheduler FROM cron WHERE hour=$hour AND minute=$minute ";
		$db->query_single($row,$sqlstr);
		if(count($row)==0)
		{
			$db->begin();
			$db->lock( "cron" );
			$id_scheduler = $db->nextId( "cron", "id_scheduler" );
			$sqlstr = "INSERT INTO cron (id_scheduler,hour,minute) VALUES ($id_scheduler,'$hour','$minute')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	public function SchedulerSlotGet($id_scheduler)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT CONCAT(hour,':',LPAD(minute,2,'0')) AS time,hour,minute FROM cron WHERE id_scheduler='$id_scheduler' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function SchedulerSlotUpdate($id_scheduler,$hour,$minute)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "cron" );
		$sqlstr = "UPDATE cron SET hour='$hour',minute='$minute' WHERE id_scheduler=$id_scheduler ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);	
	}
	
	public function SchedulerSlots()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_scheduler,CONCAT(hour,':',LPAD(minute,2,'0')) AS time,hour,minute FROM cron ORDER BY hour,minute";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function SchedulerSlotsRange($timestamp,$last_timestamp)
	{
		$exec = array();
		$slots = $this->SchedulerSlots();
		foreach($slots as $slot)
		{
			$cts = mktime($slot['hour'],$slot['minute'],0,(int)date("n",$timestamp),(int)date("j",$timestamp),(int)date("Y",$timestamp));
			if($cts > $last_timestamp && $cts<=$timestamp)
				$exec[] = $slot;
		}
		return $exec;
	}

	private function SwitchBox($id_action,$params)
	{
		$today = getdate();
		$wday = $today['wday'];
		$mday = $today['mday'];
		switch($id_action)
		{
			case "0":
				include_once(SERVER_ROOT."/../classes/phpeace.php");
				$phpeace = new Phpeace;
				$phpeace->ServerUpdate();
			break;
			case "1":
				include_once(SERVER_ROOT."/../classes/phpeace.php");
				$phpeace = new Phpeace;
				$build = $phpeace->build;
				$this->pm->MessageSet($phpeace->VersionCheck());
				if($phpeace->build > $build)
					$this->pm->tr->Reset(false);
			break;
			case "2":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$this->pm->TopicInit(0);
				$this->pm->HomeHighlightCheck();
				$this->pm->Home();
				$this->pm->Error404();
				$this->pm->JavaScriptCustom();
				$this->pm->SiteMap();
				$this->pm->Feeds();
				include_once(SERVER_ROOT."/../classes/topics.php");
				$tt = new Topics();
				foreach($tt->Visible() as $topic)
				{
					$this->pm->TopicInit($topic['id_topic']);
					$this->pm->TopicHome();
				}
				$this->pm->TopicInit(0);
			break;
			case "3":
				include_once(SERVER_ROOT."/../classes/mail.php");
				$mail = new Mail();
				$wday_newsletter = $mail->wday_newsletter;
				if ($wday_newsletter=="0" || ($wday + 1)==$wday_newsletter)
				{
					$mail->NewsletterGlobalSend();
					$this->pm->MessageSet($this->pm->tr->Translate("newsletter_global_sent"));
				}
			break;
			case "4":
				include_once(SERVER_ROOT."/../classes/topics.php");
				$topics = new Topics;
				$qtopics = $topics->Queue();
				foreach ($qtopics as $qtopic)
				{
					$this->pm->step = 0;
					$this->pm->TopicInit($qtopic['id_topic']);
					$this->pm->Topic($qtopic['id_topic']);
				}
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($topics->temp_id_topic);
				$t->queue->JobsDelete();
				$t->queue->ConfirmAll();
				$this->pm->TopicInit(0);
			break;
			case "5":
				include_once(SERVER_ROOT."/../classes/topics.php");
				$topics = new Topics;
				$newstopics = $topics->Newsletter();
				include_once(SERVER_ROOT."/../classes/mail.php");
				$mail = new Mail();
				$mail->force_newsletter = FALSE;
				foreach ($newstopics as $ntopic)
				{
					if ($ntopic['id_topic']>0)
					{
						$return = $mail->NewsletterSend($ntopic['id_topic']);
						$frequency = $this->pm->tr->Translate("newsletter_frequency");
						if ($return['send'])
						{
							if ($return['sent'])
								$this->pm->MessageSet("newsletter_sent_ok",array($frequency[$return['id_frequency']],$return['topic'],$return['user']));
							else
								$this->pm->MessageSet("newsletter_sent_ko",array($frequency[$return['id_frequency']],$return['topic']));
						}
					}
				}
			break;
			case "6":
				if ($wday==1)
				{
					$this->pm->TopicsLogClean();
				}
				if ($mday==1)
				{
					include_once(SERVER_ROOT."/../classes/users.php");
					$uu = new Users;
					$uu->LogClean();
				}
				include_once(SERVER_ROOT."/../classes/tracker.php");
				$tk = new Tracker();
				$tk->ResetIPs();
				include_once(SERVER_ROOT."/../classes/log.php");
				$log = new Log();
				$log->Send();
				include_once(SERVER_ROOT."/../classes/people.php");
				$pe= new People();
				$pe->VerifyPendingClean();
				$pe->UniqueSessionsDeleteOld();
                $pe->LoginLogClean();
                
                $conf = new Configuration();
                $verification_required = $conf->Get("verification_required");
                $unverified_people_ttl = $conf->Get("unverified_people_ttl"); 
                if ($verification_required && $unverified_people_ttl > 0)
                {
                    $pe->RemovedUsersLogClean();
                    $pe->RemoveUnverifiedUsers($unverified_people_ttl);
                }
                
				include_once(SERVER_ROOT."/../classes/services.php");
				$se = new Services();
				$se->ServiceStatusDeleteOld();
				include_once(SERVER_ROOT."/../classes/web_feeds.php");
				$wfi = new WebFeedsImporter();
				$wfi->FeedStatusDeleteOld();
			break;
			case "7":
				include_once(SERVER_ROOT."/../classes/banners.php");
				$b =new Banners();
				$this->pm->MessageSet("banners_published",array($b->BuildAll()));
			break;
			case "8":
				include_once(SERVER_ROOT."/../classes/users.php");
				$uu = new Users();
				$inactive = $uu->InactiveUsers();
				$num_inactive = count($inactive);
				if ($num_inactive > 0) {
					include_once(SERVER_ROOT."/../classes/user.php");
					$u = new User();
					include_once(SERVER_ROOT."/../classes/log.php");
					$log = new Log();
					foreach($inactive as $user) {
						if(isset($user['id_user']) && $user['id_user']>0) {
							$u->Disable($user['id_user']);
						} else {
							$log->Write("error","Empty / null user",$user);
						}
					}
					$log->Write("info","Disabled {$num_inactive} inactive users",$inactive);
				}
			break;
			case "9":
				include_once(SERVER_ROOT."/../classes/sitemap.php");
				$sim = new Sitemap();
				$sim->Generate();
			break;
			case "10":
				if ($params['wday']=="0" || ($wday + 1)==$params['wday'])
				{
					include_once(SERVER_ROOT."/../classes/links.php");
					include_once(SERVER_ROOT."/../classes/topics.php");
					$topics = new Topics;
					$links = new Links;
					$topicslinks = $topics->Links();
					foreach($topicslinks as $topic)
					{
						if ($topic['counter']>0)
						{
							$links->verbose = false;
							$errors = $links->Check($topic['id_topic']);
							$this->pm->MessageSet("links_checked",array($topic['counter'],$topic['name'],$errors));
						}
					}
				}
			break;
			case "11":
				include_once(SERVER_ROOT."/../classes/rssmanager.php");
				$rsm = new RssManager();
				$url = $params['url'];
				$id_topic = (int)$params['id_topic'];
				$id_subtopic = (int)$params['id_subtopic'];
				$id_template = (int)$params['id_template'];
				$approved = (int)$params['approved'];
				$id_keyword = (int)$params['id_keyword'];
				if(strlen($url)>0 && $id_topic>0)
				{
					$counter = $rsm->Import($url,$id_topic,$id_subtopic,$id_template,$approved,$id_keyword);
					if($counter>0)
					{
						$this->pm->step = 0;
						$this->pm->TopicInit($id_topic);
						$this->pm->Topic($id_topic);					
					}
				}
			break;
			case "12":
				include_once(SERVER_ROOT."/../classes/galleries.php");
				include_once(SERVER_ROOT."/../classes/gallery.php");
				$gg = new Galleries();
				foreach($gg->Visible() as $gallery)
				{
					$g = new Gallery($gallery['id_gallery']);
					$g->Publish();
				}
			break;
			case "13":
				include_once(SERVER_ROOT."/../classes/quotes.php");
				$quotes = new Quotes;
				$this->pm->MessageSet("export_quotes",array($quotes->DumpAll()));
			break;
			case "14":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$this->pm->step = 0;
				$this->pm->TopicInit($params['id_topic']);
				$this->pm->force = true;
				$this->pm->TopicEverything($params['id_topic']);
				$this->pm->force = false;
			break;
			case "15":
				include_once(SERVER_ROOT."/../classes/topics.php");
				$tt = new Topics();
				$tt->PendingNotify();
            break;
			case "16":
				$watches = $this->Watches();
				if(count($watches)>0) {
					include_once(SERVER_ROOT."/../classes/mail.php");
					$mail = new Mail();
					$fm = new FileManager();
					foreach($watches as $watch) {
						$html = $fm->Browse($watch['url']);
						if($html!='') {
							$keywords = array();
							if (preg_match_all("/{$watch['keyword']}/i", $html, $keywords) && isset($keywords[0]) && is_array($keywords[0])) {
								$counter = count($keywords[0]);
								$prev_counter = (int)$watch['counter'];
								if($counter != $prev_counter) {
									$subject = "Watch notification for {$watch['keyword']}";
									$message = "Keyword: {$watch['keyword']}\nURL: {$watch['url']}\nCount: $counter (was $prev_counter)\n\n";
									$recipients = $mail->AddressSplitAndValidate($watch['notify']);
									foreach($recipients as $recipient) {
										$mail->SendMail($recipient,"",$subject,$message,array(),false,array(),false);
									}
									$this->WatchUpdateCounter($watch['id_watch'], $counter);
								}
							}
						}
					}
				}
			break;
			case "17":
                include_once(SERVER_ROOT."/../classes/web_feeds.php");
                $wfi = new WebFeedsImporter();
                $wfi->RemoveExpiredFeedItems();
			break;
            case "19":
            	$this->StatusChangeRun();
			break;
            case "20":
       			$id_subtopic = (int)$params['id_subtopic'];
            	if($params['url']!="" && $params['keyword']!="" && $params['key']!="" && $id_subtopic>0)
            	{
	            	include_once(SERVER_ROOT."/../classes/webservice.php");
					$ws = new WebService("articles");
					$ws_params = array();
					$ws_params['keyword'] = $params['keyword'];
					$ws_params['key'] = $params['key'];
					$ws_params['limit'] = $params['limit'];
					$response = $ws->Call($params['url'],"ArticlesByKeyword",$ws_params);
            		include_once(SERVER_ROOT."/../classes/varia.php");
            		$va = new Varia();
            		$articles = $va->Deserialize($response,false);
            		if(is_array($articles) && count($articles)>0)
            		{
            			include_once(SERVER_ROOT."/../classes/topic.php");
            			include_once(SERVER_ROOT."/../classes/file.php");
            			$t = new Topic(0);
            			$subtopic = $t->SubtopicGet($id_subtopic);
            			$id_topic = $subtopic['id_topic'];
            			$t = new Topic($id_topic);
            			include_once(SERVER_ROOT."/../classes/article.php");
            			include_once(SERVER_ROOT."/../classes/ini.php");
            			$ini = new Ini();
            			$fm = new FileManager();
            			$default_visibility = $ini->Get("default_visibility");
						$db = Db::globaldb();
						$counter = 0;
            			foreach($articles as $article)
            			{
            				$art = $article['article'];
            				$article_url = $db->SqlQuote($art['url']);
            				
            				$imported = $t->ArticlesBySource($article_url);
            				if(count($imported)==0)
            				{
	            				$a = new Article(0);
	            				$written = date("Y-m-d",$art['ts']);
	            				$author = $db->SqlQuote($art['author']['name']);
	            				$author_notes = $db->SqlQuote($art['author']['notes']);
	            				$halftitle = $db->SqlQuote($art['halftitle']['xvalue']);
	            				$headline = $db->SqlQuote($art['headline']['xvalue']);
	            				$subhead = $db->SqlQuote($art['subhead']['xvalue']);
	            				$notes = $db->SqlQuote($art['full_content']['notes']['xvalue']);
	            				$keywords = "";
	            				foreach($art['keywords'] as $kw)
	            				{
	            					$keywords .= $kw['name'] . ",";
	            				}
	            				$keywords = $db->SqlQuote(trim($keywords,','));
	            				$a->ArticleStore($written,$art['show_date'],0,$author,$author_notes,$article_url,"",$art['show_author'],$id_topic,$id_subtopic,$halftitle,$headline,$subhead,$art['headline_visible'],"",$art['full_content']['is_html'],$notes,$art['full_content']['align'],$art['id_language'],1,0,$default_visibility,$keywords,1,$art['source']['show'],1,0,$art['full_content']['id_licence'],0,array(),0,0,"",0);
	            				$content = $db->SqlQuote($art['full_content']['content']['xvalue']);
	            				if(isset($art['images']) && is_array($art['images']))
	            				{
	            					include_once(SERVER_ROOT."/../classes/image.php");
	            					$im = new Image(0);
		            				foreach($art['images'] as $image)
		            				{
		            					if(isset($image['xname']) && $image['xname']=="image")
		            					{
			            					$im->id = 0;
			            					$image_filename = "cache/{$image['id']}.{$image['format']}";
			            					if($image['zoom']=="1")
			            					{
			            						$fm->DownLoad($image['image_zoom']['url'],$image_filename);
			            						$image_size = $image['size'];
			            					}
			            					else
			            					{
			            						$fm->DownLoad($image['url'],$image_filename);
			            						$image_size = -1;
			            					}
			            					if($fm->Exists($image_filename))
			            					{
				            					$image_source = $db->SqlQuote($image['source']);
				            					$image_author = $db->SqlQuote($image['author']);
				            					$image_caption = $db->SqlQuote($image['caption']);
				            					$im->ImageInsert("",$image_source,$image_author,0,$image_caption,"",array(),array(),$image_filename);
				            					$content = str_replace("[[Img{$image['id']}]]","[[Img{$im->id}]]",$content);
				            					$fm->Delete($image_filename);
				            					if($im->id > 0)
				            					{
					            					$im->ArticleAdd($im->id,$a->id,$image_caption,$image['align'],$image_size,$image['zoom']);    						
				            					}
			            					}
		            					}
									}
	            				}
	            				if(isset($art['docs']) && is_array($art['docs']))
	            				{
	            					include_once(SERVER_ROOT."/../classes/doc.php");
	            					$d = new Doc(0);
		            				foreach($art['docs'] as $doc)
		            				{
		            					if(isset($doc['xname']) && $doc['xname']=="doc")
		            					{
			            					$doc_filename = "cache/{$doc['id']}.{$doc['file_info']['format']}";
			            					$fm->DownLoad($doc['url'],$doc_filename);
			            					if($fm->Exists($doc_filename))
			            					{
				            					$doc_title = $db->SqlQuote($doc['title']);
				            					$doc_description = $db->SqlQuote($doc['description']['xvalue']);
				            					$doc_source = $db->SqlQuote($doc['source']['xvalue']);
				            					$doc_author = $db->SqlQuote($doc['author']);
				            					$id_doc = $d->DocInsert($doc_title,$doc_description,$doc['id_language'],$doc_source,$doc_author,$doc['id_licence'],"",array(),array(),$doc_filename);
				            					$fm->Delete($doc_filename);
				            					if($id_doc > 0)
				            					{
					            					$d->ArticleAdd($id_doc,$a->id,$doc['seq']);
				            					}
			            					}		            						
		            					}
		            				}
	            				}
	            				if(isset($art['boxes']) && is_array($art['boxes']))
	            				{
	            					foreach($art['boxes'] as $box)
	            					{
	            						if(isset($box['xname']) && $box['xname']=="box")
	            						{
			            					$box_title = $db->SqlQuote($box['title']);
			            					$box_content = $db->SqlQuote($box['full_content']['xvalue']);
			            					$box_notes = $db->SqlQuote($box['notes']['xvalue']);
			            					$box_zoom_width = $box['popup']? $box['z_width'] : "0"; 
			            					$box_zoom_height = $box['popup']? $box['z_height'] : "0"; 
			            					$id_box = $a->BoxInsert($box_title,$box_content,$box['is_html'],$box['popup'],$box['id_width'],$box['align'],$box['show_title'],$a->id,$box_notes,$box['id_type'],"",array(),$box_zoom_width,$box_zoom_height);    						
			            					$content = str_replace("[[Box{$box['id']}]]","[[Box{$id_box}]]",$content);
		            					}
									}
	            				}
	            				// TODO import audio & video
	            				$a->ArticleContentUpdate($content);
	            				$counter ++;
            				}
            			}
            			if($counter>0)
            				$this->pm->MessageSet("imported_articles",array($counter,$params['url']));
            		}
            	}
			break;
		}
	}
	
	public function StatusChanges($id_res,$id)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_res,id,UNIX_TIMESTAMP(change_date) as change_date_ts,change_date,status 
			FROM status_changes
			WHERE id_res='$id_res' AND id='$id'
			ORDER BY change_date ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function StatusChangeDelete($id_res,$id,$change_date)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "status_changes" );
		$sqlstr = "DELETE FROM status_changes WHERE id_res='$id_res' AND id='$id' AND change_date='$change_date' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function StatusChangeGetByDate($id_res,$id,$date)
	{
		$row = array();
		$db = Db::globaldb();
		$date = $db->SqlQuote($date);
		$sqlstr = "SELECT id_res,id,status,change_date 
			FROM status_changes 
			WHERE id_res='$id_res' AND id='$id' AND change_date='$date' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function StatusChangeStore($id_res,$id,$change_date,$status)
	{
		$old_status = $this->StatusChangeGetByDate($id_res,$id,$change_date);
		if($old_status['change_date']==$change_date)
		{
			$sqlstr = "UPDATE status_changes SET change_date='$change_date',status='$status' 
				WHERE id_res='$id_res' AND id='$id' AND change_date='$change_date' ";
		}
		else 
		{
			$sqlstr = "INSERT INTO status_changes (id_res,id,status,change_date) 
				VALUES ($id_res,$id,'$status','$change_date')";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "status_changes" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function StatusChangeRun()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_res,id,status FROM status_changes WHERE DATE(change_date)<=CURRENT_DATE";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
		{
			switch($row['id_res'])
			{
				case "11": // campaigns
					include_once(SERVER_ROOT."/../classes/campaign.php");
					$c = new Campaign($row['id']);
					$c->StatusChange($row['status']);
				break;
				case "19": // polls
					include_once(SERVER_ROOT."/../classes/polls.php");
					$p = new Polls();
					$p->StatusChange($row['id'],$row['status']);
				break;
			}
		}
		$db->begin();
		$db->lock( "status_changes" );
		$res[] = $db->query( "DELETE FROM status_changes WHERE DATE(change_date)<=CURRENT_DATE" );
		Db::finish( $res, $db);
	}
	
	private function MemoryUsage()
	{
		return number_format(memory_get_usage()/1024,0,",",".") . " kB (max " . number_format(memory_get_peak_usage()/1024,0,",",".") . " kB)";
	}

	public function WatchDelete($id_watch)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "site_watch" );
		$res[] = $db->query( "DELETE FROM site_watch WHERE id_watch=$id_watch" );
		Db::finish( $res, $db);
	}
	
	public function WatchGet($id_watch)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_watch,url,keyword,notify,counter
		FROM site_watch
		WHERE id_watch=$id_watch";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function WatchStore( $id_watch,$url,$keyword,$notify )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "site_watch" );
		if ($id_watch>0)
		{
			$sqlstr = "UPDATE site_watch SET url='$url',keyword='$keyword',notify='$notify'
			WHERE id_watch=$id_watch";
		}
		else
		{
			$id_watch = $db->nextId( "site_watch", "id_watch" );
			$sqlstr = "INSERT INTO site_watch (id_watch,url,keyword,notify,counter)
				VALUES ($id_watch,'$url','$keyword','$notify',0)";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_watch;
	}
	
	private function WatchUpdateCounter( $id_watch,$counter )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "site_watch" );
		$sqlstr = "UPDATE site_watch SET counter='$counter' WHERE id_watch=$id_watch";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Watches()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_watch,url,keyword,notify,counter
			FROM site_watch
			ORDER BY url ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
}
?>
