<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class Images
{
	public $img_sizes;
	public $convert_format;
	public $default_img_size;
	public $pub_path;
	
	public $isCDN;
	private $convert_quality;
	private $convert_bin;
	/**
	 * @var Config */
	private $conf;
	/** 
	 * @var FileManager */
	private $fm;

	function __construct($load_ini=true)
	{
		$this->conf = new Configuration();
		$convert_path = $this->conf->Get("convert_path");
		$this->convert_bin = (($convert_path!="")? "$convert_path/" : "") . "convert";
		$this->img_sizes = $this->conf->Get("img_sizes");
		$this->convert_quality = $this->conf->Get("convert_quality");
		$this->convert_format = $this->conf->Get("convert_format");
		$this->default_img_size = $this->conf->Get("default_img_size");
		$cdn = $this->conf->Get("cdn");
		$this->isCDN = $cdn!='';
		$paths = $this->conf->Get("paths");
		$this->pub_path = $paths['graphic'];
		$this->fm = new FileManager();
	}

	public function CDNReset($path,$id,$ext,$orig=false) {
		if($this->isCDN) {
			$cdn = $this->conf->Get("cdn");
			$cdn_token = $this->conf->Get("cdn_token");
			$url = "$cdn/reset.php?id=$id&ext=$ext&path=$path&token=$cdn_token";
			if($orig) {
				$url .= "&orig=1";
			}
			$this->fm->Browse($url);
		}
	}

	public function Convert($size, $origfile, $convfile, $fit=false, $additional_options='')
	{
		$origfile = $this->fm->RealPath($origfile);
		$convfile = $this->fm->RealPath($convfile);
		$options = ($fit)? " -geometry {$size}x{$size} " : " -geometry $size";
		$command = "$this->convert_bin $additional_options $options -quality $this->convert_quality $origfile"."[0]"." +profile \"*\" -colorspace RGB $convfile";
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$v->Exec($command);
		if($v->return_status!="0")
		{
			UserError("ImageMagick failed",array('command'=>$command));
		}
	}

	public function ConvertCovers()
	{
		$images_path = "uploads/covers";
		for($i=0;$i<=2;$i++)
		{
			$this->fm->DirClean("$images_path/$i");
		}
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_book,cover_format FROM books WHERE cover_format<>'' ORDER BY id_book";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
		{
			$origfile = "uploads/covers/orig/{$row['id_book']}.{$row['cover_format']}";
			$this->ConvertWrapper("cover",$origfile,$row['id_book']);
		}
		$this->fm->PostUpdate();
		return count($rows);
	}

	public function ConvertGraphics()
	{
		$images_path = "uploads/graphics";
		$this->fm->DirClean("$images_path/0");
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_graphic,format FROM graphics ORDER BY id_graphic";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
		{
			$origfile = "uploads/graphics/orig/" . $row['id_graphic'] . "." . $row['format'];
			$this->ConvertWrapper("graphic",$origfile,$row['id_graphic']);
		}
		$this->fm->PostUpdate();
		return count($rows);
	}

	public function ConvertImages($all=true,$resize=false)
	{
		$images_path = "uploads/images";
		$sizes = count($this->img_sizes) - 1;
		for($i=0;$i<=$sizes;$i++)
		{
			$this->fm->DirAction("$images_path/$i","check");
			if($all)
				$this->fm->DirAction("$images_path/$i","clean");
		}
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_image,format FROM images ORDER BY id_image";
		$db->QueryExe($rows, $sqlstr);
		$counter_ok = 0;
		$counter_ko = 0;
		foreach($rows as $row)
		{
			$origfile = "$images_path/orig/" . $row['id_image'] . "." . $row['format'];
			$thumbfile = "$images_path/0/" . $row['id_image'] . "." . $this->convert_format;
			if ($this->fm->Exists($origfile))
			{
				if($all || (!$this->fm->Exists($thumbfile)))
				{
					$counter_ok ++;
					$this->ConvertWrapper("image",$origfile,$row['id_image']);
				}
				elseif($resize)
				{
					$counter_ok ++;
					$sizes = count($this->img_sizes);
					for($i=0;$i<$sizes;$i++)
					{
						$convfile = "$images_path/$i/{$row['id_image']}.{$this->convert_format}";
						if(!$this->fm->Exists($convfile))
							$this->Convert($this->img_sizes[$i], $origfile, $convfile);
					}
				}
			}
			else
				$counter_ko ++;
		}
		$this->fm->PostUpdate();
		return array('tot'=>count($rows),'ok'=>$counter_ok,'ko'=>$counter_ko);
	}

	public function ConvertWrapper($type,$filename,$id)
	{
		if(!$this->isCDN || $type=="banner") {
			switch($type)
			{
				case "banner":
					$convfile = "uploads/banners/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[0], $filename, $convfile);
					break;
				case "cover":
					for($i=0;$i<=2;$i++)
					{
					$convfile = "uploads/covers/$i/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$i], $filename, $convfile);
					}
					break;
				case "doc":
					$docs_covers_size = $this->conf->Get("docs_covers_size");
					$convfile = "uploads/docs/covers/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$docs_covers_size], $filename, $convfile,false,"-density 400 -alpha off");
					break;
				case "event_image":
					$convfile = "uploads/events/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[1], $filename, $convfile);
					$convfile = "uploads/events/1/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[2], $filename, $convfile);
					$convfile = "uploads/events/2/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[3], $filename, $convfile);
					break;
				case "graphic":
					$convfile = "uploads/graphics/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[0], $filename, $convfile, true);
					break;
				case "image":
					$sizes = count($this->img_sizes) - 1;
					for($i=0;$i<=$sizes;$i++)
					{
						$convfile = "uploads/images/$i/$id" . "." . $this->convert_format;
						$this->Convert($this->img_sizes[$i], $filename, $convfile);
					}
					break;
				case "org_image":
					$orgs_thumb = $this->conf->Get("orgs_thumb");
					$convfile = "uploads/orgs/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$orgs_thumb], $filename, $convfile);
					$orgs_size = $this->conf->Get("orgs_size");
					$convfile = "uploads/orgs/1/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$orgs_size], $filename, $convfile);
					break;
				case "product_image":
					include_once(SERVER_ROOT."/../classes/ecommerce.php");
					$ec = new eCommerce();
					$product_thumb = $ec->product_thumb;
					$product_image = $ec->product_image;
					$convfile = "uploads/products/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$product_thumb], $filename, $convfile);
					$convfile = "uploads/products/1/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[$product_image], $filename, $convfile);
					break;
				case "user_image":
					$convfile = "uploads/users/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[0], $filename, $convfile);
					break;
				case "video_thumb":
					$convfile = "uploads/videos/thumbs/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[0], $filename, $convfile);
					break;
				case "widget_image":
					include_once(SERVER_ROOT."/../classes/widgets.php");
					$convfile = "uploads/widgets/0/$id" . "." . $this->convert_format;
					$this->Convert($this->img_sizes[WIDGET_IMAGE_SIZE], $filename, $convfile);
					break;
			}
			$this->fm->PostUpdate();
		}
	}
	
	public function Count()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT COUNT(id_image) AS count_images FROM images");
		return $row['count_images'];
	}

	public function Gallery( &$rows, $id_gallery, $id_gallery_exclude=0,$sort=0,$exclude_flash=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ig.id_image,IF(igi.id_image IS NULL,i.caption,igi.caption) AS caption,ig.id_gallery_from,i.format,i.height/i.width AS ratio
		FROM images_galleries ig
		INNER JOIN images i ON ig.id_image=i.id_image 
		LEFT JOIN images_galleries_import igi ON ig.id_image=igi.id_image AND ig.id_gallery=igi.id_gallery ";
		if($id_gallery_exclude>0)
			$sqlstr .= " LEFT JOIN images_galleries ig2 ON ig.id_image=ig2.id_image AND ig2.id_gallery=$id_gallery_exclude ";
		$sqlstr .= " WHERE ig.id_gallery='$id_gallery' ";
		if($id_gallery_exclude>0)
			$sqlstr .= " AND ig2.id_gallery_from IS NULL  ";
		switch($sort)
		{
			case "0":
				$sqlstr .= " ORDER BY ig.id_image";
			break;
			case "1":
			case "4":
				$sqlstr .= " ORDER BY ig.id_image DESC";
			break;
			case "2":
				$sqlstr .= " ORDER BY i.image_date";
			break;
			case "3":
				$sqlstr .= " ORDER BY i.image_date DESC";
			break;
		}
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Graphics( &$rows, $id_style=0 )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_graphic,g.width,g.height,g.name,g.format,s.name AS style_name 
		FROM graphics g
		LEFT JOIN styles s USING(id_style) "
		. (($id_style>0)? " WHERE g.id_style='$id_style'":"") .
		" ORDER BY g.id_graphic DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function ImageMagickVersion()
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$varia = new Varia();
		$return = $varia->Exec($this->convert_bin);
		return $return[0];
	}
	
	public function LogoCreate($title,$description,$output,$install=false)
	{
		$logo = $this->fm->RealPath($output);
		$command = "$this->convert_bin -size 300x67 -background white ";
		$command .= "-fill blue -size 280x -gravity Center -pointsize 40 caption:\"$title\" ";
		$command .= "-fill black -size 260x -pointsize 12 caption:\"$description\" ";
		$command .= "-append $logo";
		include_once(SERVER_ROOT."/../classes/varia.php");
		$varia = new Varia();
		$varia->Exec($command);
		if($install && $this->fm->Exists("uploads/custom/logo.jpg"))
		{
			$this->fm->Copy("uploads/custom/logo.jpg","admin/images/custom.jpg");
			$this->fm->PostUpdate();
		}
	}

	public function Orphans( &$rows, $paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT i.id_image,i.caption,i.format 
			FROM images i
			LEFT JOIN images_articles ia ON ia.id_image=i.id_image
			LEFT JOIN images_galleries ig ON ig.id_image=i.id_image
			LEFT JOIN subtopics s ON s.id_image=i.id_image
			WHERE ia.id_article IS NULL AND ig.id_gallery IS NULL AND s.id_image IS NULL 
			ORDER BY i.id_image DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function OrphansRemove()
	{
		include_once(SERVER_ROOT."/../classes/image.php");
		$images = array();
		$this->Orphans($images,false);
		foreach ($images as $image)
		{
			$im = new Image($image['id_image']);
			$im->Remove($image['id_image'],false);
		}
	}

	public function ReassociateImages()
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$db =& Db::globaldb();
		$convert_format = $this->convert_format;
		// articles associated
		$asimages = array();
		$sqlstr = "SELECT id_article,image_size,id_image FROM articles WHERE id_image>0 ";
		$db->QueryExe($asimages, $sqlstr);
		$db->begin();
		$db->lock( "articles" );
		foreach($asimages as $asimage)
		{
			$size = $asimage['image_size']=="-1"? "orig" : $asimage['image_size'];
			$filename = "uploads/images/{$size}/{$asimage['id_image']}.$convert_format";
			$sizes = $this->fm->ImageSize($filename);
			if($sizes['width']>0 && $sizes['height']>0)
			{
				$sqlstr = "UPDATE articles SET image_width='{$sizes['width']}',image_height='{$sizes['height']}' 
					WHERE id_article='{$asimage['id_article']}' ";
				$res[] = $db->query( $sqlstr );
			}
		}
		Db::finish( $res, $db);
		// images_articles
		$aimages = array();
		$sqlstr = "SELECT id_article,size,id_image FROM images_articles WHERE id_image>0 ";
		$db->QueryExe($aimages, $sqlstr);
		$db->begin();
		$db->lock( "images_articles" );
		foreach($aimages as $aimage)
		{
			$size = $aimage['size']=="-1"? "orig" : $aimage['size'];
			$filename = "uploads/images/{$size}/{$aimage['id_image']}.$convert_format";
			$sizes = $this->fm->ImageSize($filename);
			if($sizes['width']>0 && $sizes['height']>0)
			{
				$sqlstr = "UPDATE images_articles SET width='{$sizes['width']}',height='{$sizes['height']}' 
					WHERE id_article='{$aimage['id_article']}' AND id_image='{$aimage['id_image']}' ";
				$res[] = $db->query( $sqlstr );
			}
		}
		Db::finish( $res, $db);
		// images_galleries
		$gimages = array();
		$sqlstr = "SELECT ig.id_gallery,ig.id_image,g.params FROM images_galleries ig
			 INNER JOIN galleries g ON ig.id_gallery=g.id_gallery 
			 WHERE ig.id_image>0 ";
		$db->QueryExe($gimages, $sqlstr);
		$db->begin();
		$db->lock( "images_galleries" );
		foreach($gimages as $gimage)
		{
			$dparams = $v->Deserialize($gimage['params']);
			$filename1 = "uploads/images/{$dparams['thumb_size']}/{$gimage['id_image']}.$convert_format";
			$filename2 = "uploads/images/{$dparams['image_size']}/{$gimage['id_image']}.$convert_format";
			$sizes1 = $this->fm->ImageSize($filename1);
			$sizes2 = $this->fm->ImageSize($filename2);
			if($sizes['width']>0 && $sizes['height']>0)
			{
				$sqlstr = "UPDATE images_galleries SET thumb_height='{$sizes1['height']}',image_height='{$sizes2['height']}' 
					WHERE id_gallery='{$gimage['id_gallery']}' AND id_image='{$gimage['id_image']}' ";
				$res[] = $db->query( $sqlstr );
			}
		}
		Db::finish( $res, $db);
	}
	
	public function RemoveWrapper($type,$id)
	{
		if ($id>0)
		{
			switch($type)
			{
				case "cover":
					for($i=0;$i<=2;$i++)
					{
						$this->fm->Delete("uploads/covers/$i/$id" . "." . $this->convert_format);
					}
				break;
				case "graphic":
					$this->fm->Delete("uploads/graphics/0/$id" . "." . $this->convert_format);
				break;
				case "event_image":
					$this->fm->Delete("uploads/events/0/{$id}.jpg");
					$this->fm->Delete("uploads/events/1/{$id}.jpg");
				break;
				case "image":
					foreach($this->img_sizes as $key=>$size)
					{
						$this->fm->Delete("uploads/images/$key/$id" . "." . $this->convert_format);
					}
				break;
				case "org_image":
					$this->fm->Delete("uploads/orgs/0/{$id}.jpg");
					$this->fm->Delete("uploads/orgs/1/{$id}.jpg");
				break;
				case "product_image":
					$this->fm->Delete("uploads/products/0/{$id}.jpg");
					$this->fm->Delete("uploads/products/1/{$id}.jpg");
				break;
				case "user_image":
					$this->fm->Delete("uploads/users/0/{$id}.jpg");
				break;
				case "widget_image":
					$this->fm->Delete("uploads/widgets/0/{$id}.jpg");
					$this->fm->Delete("uploads/widgets/1/{$id}.jpg");
				break;
			}
			$this->fm->PostUpdate();
		}
	}
	
	public function Search(&$rows, $current_id_topic, $id_topic, $id_gallery, $text, $author)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT i.id_image,i.caption,i.height/i.width AS ratio,i.format
			FROM images i ";
		if($id_topic>0 || $id_gallery>0)
		{
			if($id_topic>0)
			{
				if($id_gallery>0)
				{
					$sqlstr .= " LEFT JOIN images_galleries ig ON ig.id_image=i.id_image
					INNER JOIN galleries g ON ig.id_gallery=g.id_gallery 
					INNER JOIN topics t ON g.id_topic=t.id_topic
					WHERE (t.share_images=1 OR t.id_topic='$current_id_topic') AND ig.id_gallery='$id_gallery' AND g.id_topic='$id_topic' ";
				}
				else 
				{
					$sqlstr .= " LEFT JOIN images_articles ia ON ia.id_image=i.id_image
					INNER JOIN articles a ON ia.id_article=a.id_article
					INNER JOIN topics t ON a.id_topic=t.id_topic
					WHERE (t.share_images=1 OR t.id_topic='$current_id_topic') AND a.id_topic='$id_topic' ";
				}
			}
			else 
			{
				$sqlstr .= " LEFT JOIN images_galleries ig ON ig.id_image=i.id_image
				INNER JOIN galleries g ON ig.id_gallery=g.id_gallery 
				WHERE g.share_images=1 AND ig.id_gallery='$id_gallery' ";
			}
		}
		else 
		{
			$sqlstr .= " LEFT JOIN images_articles ia ON ia.id_image=i.id_image
			INNER JOIN articles a ON ia.id_article=a.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic
			LEFT JOIN images_galleries ig ON ig.id_image=i.id_image
			INNER JOIN galleries g ON ig.id_gallery=g.id_gallery 
			WHERE ((t.share_images=1 OR t.id_topic='$current_id_topic') OR g.share_images=1) ";
		}
		if(strlen($text)!="")
		{
			if(strpos($sqlstr,"images_articles")!==false)
				$sqlstr .= " AND (i.caption LIKE '%$text%' OR i.source LIKE '%$text%' OR ia.caption LIKE '%$text%') ";
			else
				$sqlstr .= " AND (i.caption LIKE '%$text%' OR i.source LIKE '%$text%') ";
		}
		if(strlen($author)!="")
		{
			$sqlstr .= " AND i.author LIKE '%$author%' ";
		}
		$sqlstr .= " GROUP BY i.id_image ORDER BY i.id_image DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function Topic( &$rows, $id_topic, $exclude_=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ia.id_image,ia.caption,i.width,i.height,ia.zoom,i.format,i.height/i.width AS ratio
		FROM images_articles ia
		INNER JOIN articles a ON ia.id_article=a.id_article
		INNER JOIN images i ON ia.id_image=i.id_image
		WHERE a.id_topic='$id_topic' ";
		$sqlstr .= " GROUP BY ia.id_image ORDER BY ia.id_image DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}
?>
