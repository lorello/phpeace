<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/file.php");

/**
 * Banners management class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Banners
{
	/**
	 * Where banners are stored
	 * The path is relative to PhPeace installation
	 *
	 * @var string
	 */
	private $path;
	
	/**
	 * Where banners are symlinked in the public site
	 *
	 * @var string
	 */
	private $pub_path;

	/**
	 * Whether to log banners clicks
	 *
	 * @var boolean
	 */
	private $log_clicks;

	/** 
	 * @var FileManager */
	private $fm;

	/**
	 * Initialize private variables
	 * Instantiate FileManager for filesystem operations
	 *
	 */
	function __construct()
	{
		$ini = new Ini;
		$this->path = "uploads/banners";
		$this->pub_path = "pub/banners";
		$this->log_clicks = $ini->Get('log_banners');
		$this->fm = new Filemanager;
	}

	/**
	 * All banner clicks
	 *
	 * @param 	array 	$rows	Clicks (paginated)
	 * @return 	integer			Num of total clicks
	 */
	public function AllClicks( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT date_format(click_date,'%d.%m.%Y') AS click_date,
			date_format(click_date,'%H:%i') AS click_time,ip,referer,clicks.id_banner,alt_text
			FROM clicks
			INNER JOIN banners ON clicks.id_banner=banners.id_banner
			ORDER BY click_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Remove a banner
	 *
	 * @param integer $id_banner
	 * @param integer $id_group
	 */
	public function BannerDelete($id_banner,$id_group)
	{
		$this->BannerRemove($id_banner);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners" );
		$res[] = $db->query( "DELETE FROM banners WHERE id_banner='$id_banner' " );
		Db::finish( $res, $db);
		$this->Reset($id_banner,true);
		$this->Reset($id_group,false);
	}

	/**
	 * Retrieve banner details
	 *
	 * @param integer $id_banner
	 * @return array
	 */
	public function BannerGet($id_banner)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT b.link,b.alt_text,UNIX_TIMESTAMP(b.start_date) AS start_date_ts,b.can_expire,b.format,
		UNIX_TIMESTAMP(b.expire_date) AS expire_date_ts,b.filename,b.id_banner,b.popup,bg.width,bg.height
		FROM banners b
		INNER JOIN banners_groups bg ON b.id_group=bg.id_group
		WHERE b.id_banner='$id_banner' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	/**
	 * Add a new banner
	 *
	 * @param string 	$link			The URL to redirect to after clicking the banner
	 * @param string 	$alt_text		Banner caption
	 * @param date 		$start_date		Date the banner will be activated
	 * @param boolint	$can_expire		Whether the banner can automatically expire
	 * @param date 		$expire_date	Expiry date
	 * @param integer 	$id_group		Identifier of the banner group the banner belongs to
	 * @param array 	$file			Uploaded file information
	 * @param boolint 	$popup			Whether the banner's link should open in a new browser window
	 */
	public function BannerInsert($link,$alt_text,$start_date,$can_expire,$expire_date,$id_group,$file,$popup)
	{
		$format = $this->fm->Extension($file['name']);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners" );
		$id_banner = $db->nextId( "banners", "id_banner" );
		$sqlstr = "INSERT INTO banners (id_banner,link,alt_text,start_date,can_expire,expire_date,id_group,filename,popup,format)
			VALUES ($id_banner,'$link','$alt_text','$start_date',$can_expire,'$expire_date',$id_group,'{$file['name']}','$popup','$format')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$filename = $id_banner . "." . $format;
		$this->fm->MoveUpload($file['temp'], "$this->path/orig/" . $filename);
		$this->Convert("$this->path/orig/" . $filename, $id_banner);
		$this->Publish($id_banner,$format);
		$this->Reset($id_group,false);
	}
	
	/**
	 * Update banner's details
	 *
	 * @param integer 	$id_banner 		Banner ID
	 * @param string 	$link 			The URL to redirect to after clicking the banner
	 * @param string 	$alt_text 		Banner caption
	 * @param date 		$start_date 	Date the banner will be activated
	 * @param boolint 	$can_expire 	Whether the banner can automatically expire
	 * @param date 		$expire_date 	Expiry date
	 * @param integer 	$id_group 		Identifier of the banner group the banner belongs to
	 * @param array 	$file 			Uploaded file information
	 * @param boolint 	$popup			Whether the banner's link should open in a new browser window
	 */
	public function BannerUpdate($id_banner,$link,$alt_text,$start_date,$can_expire,$expire_date,$id_group,$file,$popup)
	{
		$banner_old = $this->BannerGet($id_banner);
		if ($file['ok'])
		{
			$this->BannerRemove($id_banner);
			$format = $this->fm->Extension($file['name']);
			$origfile = "$this->path/orig/".$id_banner.".".$format;
			$this->fm->MoveUpload($file['temp'], $origfile);
			$this->Convert($origfile, $id_banner);
			$sqlstr = "UPDATE banners SET link='$link',alt_text='$alt_text',start_date='$start_date',popup='$popup',
				can_expire=$can_expire,expire_date='$expire_date',filename='{$file['name']}',id_group=$id_group,format='$format'
				WHERE id_banner=$id_banner";
			$this->Publish($id_banner,$format);
		}
		else
		{
			$sqlstr = "UPDATE banners SET link='$link',alt_text='$alt_text',start_date='$start_date',popup='$popup',
				can_expire=$can_expire,expire_date='$expire_date',id_group=$id_group
				WHERE id_banner=$id_banner";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners" );
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		$this->Reset($id_group,false);
		if($banner_old['id_group']!=$id_group)
			$this->Reset($banner_old['id_group'],false);
		$this->Reset($id_banner,true);
	}

	/**
	 * Removes banner's files and symlinks from local filesystem
	 *
	 * @param integer $id_banner
	 */
	private function BannerRemove($id_banner)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$db =& Db::globaldb();
		$row = array();
		$db->query_single($row,"SELECT format FROM banners WHERE id_banner=$id_banner");
		$this->fm->Delete("$this->path/orig/$id_banner" . "." . $row['format']);
		$this->fm->Delete("$this->path/orig/$id_banner" . "." . $i->convert_format);
		$this->fm->Delete("$this->pub_path/$id_banner" . "." . $row['format']);
		$this->fm->PostUpdate();
	}

	/**
	 * All banners
	 *
	 * @return array
	 */
	public function BannersAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_banner,alt_text,format FROM banners";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Rebuil all public symlinks and reset memory cache
	 *
	 * @return integer	Number of banners being rebuilt
	 */
	public function BuildAll()
	{
		$this->fm->DirClean($this->pub_path);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_banner,format FROM banners ORDER BY id_banner";
		$num = $db->QueryExe($rows, $sqlstr);
		foreach($rows as $banner)
		{
			$orig = "$this->path/orig/{$banner['id_banner']}.{$banner['format']}";
			$copy = "$this->pub_path/{$banner['id_banner']}.{$banner['format']}";
			$this->fm->Copy($orig,$copy);
		}
		$this->fm->PostUpdate();
		$groups = array();
		$this->Groups($groups,false);
		foreach($groups as $group)
			$this->Reset($group['id_group'],false);
		return $num;
	}

	/**
	 * Stores that a banner has been clicked, and return the link to redirect to
	 *
	 * @param integer $id_banner
	 * @return string	The URL the banner should point to
	 */
	public function Click($id_banner)
	{
		if ($this->log_clicks && $id_banner>0)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$referer = Varia::Referer();
			$ip = Varia::IP();
			$now = date("Y-m-d H:i:s");
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "clicks" );
			$id_click = $db->nextId( "clicks", "id_click" );
			$sqlstr = "INSERT INTO clicks (id_click,referer,ip,id_banner,click_date) VALUES ($id_click,'$referer','$ip',$id_banner,'$now')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		$banner = $this->BannerGet($id_banner);
		return $banner['link'];
	}

	/**
	 * Wrapper for calling the image conversion method
	 *
	 * @param 	string 	$origfile	Relative path to the original file
	 * @param	integer	$id_banner	Banner ID
	 */
	private function Convert($origfile, $id_banner)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$i->ConvertWrapper("banner",$origfile,$id_banner);
	}

	/**
	 * Count all available banners
	 *
	 * @return integer
	 */
	public function Count()
	{
		$db =& Db::globaldb();
		$row = array();
		$db->query_single($row, "SELECT COUNT(id_banner) AS counter FROM banners");
		return (int)$row['counter'];
	}

	/**
	 * All banners in a group
	 *
	 * @param 	array 		$rows  		Banners (paginated)
	 * @param 	integer 	$id_group	Group ID
	 * @return 	integer					Num of total banners
	 */
	public function GroupBanners( &$rows, $id_group )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_banner,link,alt_text,date_format(start_date,'%d.%m.%Y'),
			IF(can_expire,date_format(expire_date,'%d.%m.%Y'),'-'),
			IF((start_date<=CURDATE()) AND ((can_expire=1 AND expire_date>=CURDATE()) OR can_expire=0),'1','0') AS active,format
			FROM banners WHERE id_group=$id_group ORDER BY active DESC, id_banner DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Delete a group
	 *
	 * @param integer $id_group
	 */
	public function GroupDelete($id_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners_groups" );
		$res[] = $db->query( "DELETE FROM banners_groups WHERE id_group='$id_group' " );
		Db::finish( $res, $db);
		$this->Reset($id_group,false);
	}
	
	/**
	 * Retrieve banner group details
	 *
	 * @param integer $id_group
	 * @return array
	 */
	public function GroupGet($id_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_group,name,width,height,description,id_user FROM banners_groups WHERE id_group=$id_group";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	/**
	 * Add a new banner group
	 *
	 * @param string 	$name			The name of the new group
	 * @param integer 	$width			Default width of banners in this group
	 * @param integer 	$height			Default height of banners in this group
	 * @param string 	$description	Group's description
	 * @param integer 	$id_user		Administrator of this banners' group
	 * @return integer					New Group ID
	 */
	public function GroupInsert( $name, $width, $height,$description,$id_user )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners_groups" );
		$id_group = $db->nextId( "banners_groups", "id_group" );
		$res[] = $db->query( "INSERT INTO banners_groups (id_group,name,width,height,description,id_user) VALUES ($id_group,'$name','$width','$height','$description','$id_user') " );
		Db::finish( $res, $db);
		return $id_group;
	}

	/**
	 * Update a banner group
	 *
	 * @param string 	$id_group		Group ID
	 * @param string 	$name			The name of the new group
	 * @param integer 	$width			Default width of banners in this group
	 * @param integer 	$height			Default height of banners in this group
	 * @param string 	$description	Group's description
	 * @param integer 	$id_user		Administrator of this banners' group
	 */
	public function GroupUpdate( $id_group, $name, $width, $height, $description,$id_user )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "banners_groups" );
		$res[] = $db->query( "UPDATE banners_groups SET name='$name',width='$width',height='$height',description='$description',id_user='$id_user' WHERE id_group=$id_group" );
		Db::finish( $res, $db);
		$this->Reset($id_group,false);
	}

	/**
	 * All available banner groups, 
	 * with some additional stats on how many active banners they contain
	 *
	 * @param array 	$rows	Groups of banners (can be paginated)
	 * @param boolean 	$paged	Whether $rows are paginated
	 * @return integer 			Num of total groups
	 */
	public function Groups( &$rows,$paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT bg.id_group,bg.name,bg.width,bg.height,COUNT(b.id_banner) AS counter,bg.description,bg.id_user,
			SUM(IF((b.start_date<=CURDATE()) AND ((b.can_expire=1 AND b.expire_date>=CURDATE()) OR b.can_expire=0),'1','0')) AS active
			FROM banners_groups bg
			LEFT JOIN banners b USING (id_group)
			GROUP BY bg.id_group 
			ORDER BY bg.name,bg.width,bg.height";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	/**
	 * Active banners in a group
	 *
	 * @param integer $id_group	Group ID
	 * @return array	Banners
	 */
	public function GroupActiveBanners($id_group)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT b.id_banner,b.alt_text,b.format,b.popup,bg.width,bg.height,b.link 
		FROM banners b
		INNER JOIN banners_groups bg ON b.id_group=bg.id_group
		WHERE b.id_group=$id_group AND (b.start_date<=CURDATE())
		AND ((b.can_expire=1 AND b.expire_date>=CURDATE()) OR b.can_expire=0)";
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Create symlink for banner
	 *
	 * @param integer 	$id_banner	Banner ID
	 * @param string 	$format		Banner file format
	 */
	private function Publish($id_banner,$format)
	{
		$filename = $id_banner . "." . $format;
		$this->fm->Copy("$this->path/orig/$filename","$this->pub_path/$filename");
		$this->fm->PostUpdate();
	}

	/**
	 * Reset shared memory variable
	 *
	 * @param integer 	$id			ID
	 * @param boolean	$is_banner	Whether is a banners' group
	 */
	private function Reset($id,$is_banner)
	{
		include_once(SERVER_ROOT."/../classes/sharedmem.php");
		$sm = new SharedMem();
		if($sm->IsAPC())
		{
			$mem_var = $this->MemVar($id,$is_banner);
			$sm->Delete($mem_var);
		}
	}
	
	/**
	 * Retrieve banner
	 * If a group ID is passed instead, retrieve a random banner
	 * from all active banners in that group
	 *
	 * @param integer $id_banner	Banner ID
	 * @param integer $id_group		Banner's group ID
	 * @return array				Banner
	 */
	public function MemGet($id_banner,$id_group,$use_cache=false)
	{
		if($use_cache)
		{
			include_once(SERVER_ROOT."/../classes/sharedmem.php");
			$sm = new SharedMem();
			$id = $id_banner>0? $id_banner : $id_group;
			$mem_var = $this->MemVar($id,$id_banner>0);
			if($sm->IsVarSet($mem_var))
			{
				$banner_var = $sm->Get($mem_var);
			}
			else 
			{
				if($id_banner>0)
					$banner_var = $this->BannerGet($id_banner);
				else 
					$banner_var = $this->GroupActiveBanners($id_group);
				$sm->Set($mem_var,$banner_var);
			}
		}
		else 
		{
			if ($id_banner>0)
				$banner_var = $this->BannerGet($id_banner);
			else
				$banner_var = $this->GroupActiveBanners($id_group);
		}
		if(!$id_banner>0)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			srand( Varia::MakeSeed() );
			$rand = rand(0,count($banner_var)-1);
			$banner_var = $banner_var[$rand];
		}
		return $banner_var;
	}
	
	/**
	 * Shared memory variable identifier for banner or banners' group
	 *
	 * @param integer	$id			ID
	 * @param boolean	$is_banner	Whether is a banners' group
	 * @return string				Memory variable name
	 */
	private function MemVar($id,$is_banner)
	{
		return "banner" . ($is_banner?"":"g") . "{$id}";
	}

}
?>
