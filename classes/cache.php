<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/config.php");

/**
 * Cache network resources on local filsystem
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 * @author Giacomo Lacava <g.lacava@gmail.com>
 */
class Cache
{
	/**
	 * Time To Live (in minutes)
	 *
	 * @var integer
	 */
	public $ttl;

	/** 
	 * @var FileManager */
	private $fm;
	
	public $timeout;
	
	/**
	 * Initialize local properties
	 *
	 */
	function __construct()
	{
		$this->fm = new FileManager;
		$conf = new Configuration();
		$this->ttl = $conf->Get("cache_ttl");
		$this->timeout = 0;
	}

	/**
	 * Remove variable from cache
	 *
	 * @param string $type	Type
	 * @param string $id	Identifier
	 */
	public function Delete($type,$id)
	{
		$filename = $this->Filename($type,$id);
		$this->fm->Delete($filename);
	}

	/**
	 * Check if a resource is cached
	 *
	 * @param string $type	Resource type
	 * @param string $id	Resource ID
	 * @return boolean
	 */
	public function Exists($type,$id)
	{
		$exists = FALSE;
		$filename = $this->Filename($type,$id);
		if ($this->fm->Exists($filename) && ((time() - $this->fm->filemtime($filename)) < (($this->ttl)*60)) )
			$exists = TRUE;
		return $exists;
	}
	
	/**
	 * Filename of cached content
	 *
	 * @param string $type
	 * @param string $id
	 * @return string
	 */
	private function Filename($type,$id)
	{
		return $this->TypeFolder($type) . $this->Hash($id);
	}

	/**
	* Return folder for specific type
	*
	* @param string type	Type to be cached
	* @return string
	*/
	private function TypeFolder($type)
	{
		return "cache/$type/";
	}

	/**
	 * Retrieve cached content
	 * If content does not exist, it is updated and stored
	 *
	 * @param string $type
	 * @param string $id
	 * @return string
	 */
	public function Get($type,$id)
	{
		if (!$this->Exists($type,$id))
			$this->Update($type,$id);
		return $this->fm->TextFileRead($this->Filename($type,$id));
	}

	/**
	 * Hash the resource identifier
	 *
	 * @param string $string
	 * @return string
	 */
	private function Hash($string)
	{
		return md5($string);
	}
	
	/**
	 * Update local content
	 * Depending on its type, it is retrieved from network
	 *
	 * @param string 	$type
	 * @param string 	$id
	 * @param boolean	$update_mtime	Update file modified time only
	 */
	public function Update($type,$id,$update_mtime=true)
	{
		$this->fm->DirAction("cache","check");
		$this->fm->DirAction($this->TypeFolder($type),"check");
		$filename = $this->Filename($type,$id);
		
		switch($type)
		{
			case "rss":
			case "feature":
			case "xml":
				include_once(SERVER_ROOT."/../classes/xmlhelper.php");
				$xh = new XmlHelper();
				$filename_temp = $filename . "_temp";
				$http_code = $this->fm->Download($id,$filename_temp);
				$rss_temp = $this->fm->TextFileRead($filename_temp);
				if($http_code<400 && strlen($rss_temp)>0 && $xh->Check($rss_temp,false))
				{
					$this->fm->Rename($filename_temp,$filename,true);
				}
				else
				{
					$this->fm->Delete($filename_temp);
					if($update_mtime)
					{
						$this->fm->Touch($filename);
					}
				}
			break;
			case "json":
			    $this->fm->Download($id,$filename);
		    break;
			case "events":
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$parts = explode('-', $id);
				$start_ts = $parts[0];
				$end_ts = $parts[1];
				$events = $ee->EventsRangeCal($start_ts, $end_ts);
				$this->fm->WritePage($filename, json_encode($events));
			break;
			case "events_api":
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$response = $ee->RequestApi(unserialize($id));
				$this->fm->WritePage($filename, serialize($response));
				break;
			case "aggregated_feed":
				$this->fm->Touch($filename);
			break;
			case "xmlfile":
				include_once(SERVER_ROOT."/../classes/xmlhelper.php");
				$xh = new XmlHelper();
				$file_content = $this->fm->TextFileRead($id);
				if(strlen($file_content)>0 && $xh->Check($file_content,false))
				{
					$this->fm->Rename($file_content,$filename,true);
				}
			break;
			case "webpage":
				$statusCode = $this->fm->Download($id,$filename,false,$this->timeout);
				if($statusCode==0) {
					$this->Delete('webpage',$id);
				}
			break;
			case "widget_feature":
				include_once(SERVER_ROOT."/../classes/layout.php");
				$l = new Layout(false,false,false);
				$l->TopicInit(0,true);
				$output_params = array();
				$output_params['id_feature'] = $id;
				$output_params['subtype'] = "widget_feature";
				$html = $l->Output("random_item",0,0,1,$output_params);
				$this->fm->WritePage($filename, $html);
			break;
		}
	}

	/**
	* Add arbitrary content to the cache
	*
	* @param string $type 		Type of cached file
	* @param string $id			ID of the file (usually returned by a call to Hash)
	* @param string $content	Content to cache
	*
	*/
	public function Set($type,$id,$content)
	{
		if($content!="")
		{
			// $this->fm->DirAction("cache/$type","check");
			$this->fm->WritePage($this->Filename($type,$id),$content);
		}
	}

}
?>
