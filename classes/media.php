<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/history.php");
include_once(SERVER_ROOT."/../classes/irl.php");

// CONSTANTS
define('MEDIA_HASH_LENGTH',12);

class Media
{
	/** 
	 * @var IRL */
	public $irl;
	
	/** 
	 * @var FileManager */
	protected $fm;

	/** 
	 * @var History */
	protected $h;

	protected $ffmpeg_niceness;
	
	private $type;
	protected $id_type;
	protected $id_res_type;
	
	function __construct($type="")
	{
		$this->h = new History();
		if($type!="")
		{
			$this->TypeSet($type);
		}
		$this->irl = new IRL();
		$this->fm = new FileManager();
		$conf = new Configuration();
		$this->ffmpeg_niceness = $conf->Get("ffmpeg_niceness");
	}
	
	public function AdminRight($id_user,$id_media)
	{
		$right = false;
		if($id_media>0)
		{
			$creator = $this->CreatorId($id_media);
			if($id_user == $creator['id_user'] )
				$right = true;
		}
		return $right;
	}

	public function ConfigurationUpdate($media_path)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->SetPath("media_path",$media_path);
	}

	public function CreatorId($id_media)
	{
		return $this->h->CreatorId($this->id_res_type,$id_media);
	}

	protected function TypeSet($type)
	{
		$this->type = $type;
		$types = array('video'=>1,'audio'=>2);
		$this->id_type = $types[$type];
		$this->id_res_type = $this->h->types[$type];
	}
	
	private function Encode($id_media,$format)
	{
		$origfile = $this->fm->RealPath($this->irl->PathAbs("{$this->type}_orig",array('id'=>$id_media,'format'=>$format)));
		$enc_file = $this->irl->PathAbs("{$this->type}_enc",array('id'=>$id_media));
		$enc_file_abs = $this->fm->RealPath($enc_file);
		$command = $this->FfmpegEncodeCommand($origfile,$enc_file_abs);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$feedback = $v->Exec($command);
		if($v->return_status=="0")
		{
			$status = "OK";
			$this->EncodePost($id_media,$enc_file);
		}
		else 
		{
			$this->fm->Delete($enc_file);
			$status = "Encoding error details:\n";
			foreach($feedback as $fbrow)
			{
				$status .= trim($fbrow) . "\n";
			}
			$status .= "\nCommand: $command\n";
		}
		$this->fm->PostUpdate();
		return $status;
	}
	
	public function EncodeMedia($id_media,$id_user,$user_email,$user_name)
	{
		$messages = array();
		$media = $this->MediaGet($id_media);
		$status = $this->Encode($id_media,$media['format']);
		if($id_user>0)
		{
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			$subject = ucfirst($this->type) . " Encoding: " . ($status=="OK"? "OK" : "FAILED");
			$msg = "ENCODING RESULTS\n";
			$msg .= "Title: {$media['title']}\n";
			$msg .= "{$mail->admin_web}/media/{$this->type}.php?id=$id_media\n";
			$msg .= $status=="OK"? "SUCCESSFUL\n" : "FAILED\n$status";
			$mail->SendMail($user_email,$user_name,$subject,$msg,array());
		}
		$messages[] = "Encoding of {$this->type} file #{$id_media}: $status";
		if($status=="OK" && $media['approved'])
		{
			$this->Publish($id_media,$media['hash'],$media['hash2'],$media['title']);
			$this->Index($id_media);
		}
		$this->EncodeQueueDelete($id_media);
		return $messages;
	}
	
	protected function EncodePost($id_media,$enc_file)
	{
		$media_info = $this->MediaInfo($enc_file);
		$db =& Db::globaldb();
		$info = $db->SqlQuote($media_info['info']);
		$this->MediaInfoUpdate($id_media,$info,false);
	}
	
	public function EncodeQueueAdd($id_media)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$current_user = (int)($session->Get('current_user_id'));
		$db =& Db::globaldb();
		$now = $db->getTodayTime();
		$db->begin();
		$db->lock( "media_queue" );
		$res[] = $db->query( "INSERT INTO media_queue (id_media,insert_time,id_user,id_type) 
			VALUES ($id_media,'$now','$current_user','{$this->id_type}') " );
		Db::finish( $res, $db);
	}
	
	protected function EncodeQueueDelete($id_media)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "media_queue" );
		$res[] = $db->query( "DELETE FROM media_queue WHERE id_media='$id_media' AND id_type='{$this->id_type}' " );
		Db::finish( $res, $db);
	}
	
	protected function FfmpegEncodeCommand($input_file,$output_file)
	{
		return "nice -n {$this->ffmpeg_niceness} ffmpeg -i $input_file -y $output_file 2>&1 ";
	}
	
	protected function FfmpegInfoCommand($input_file)
	{
		return "ffmpeg -i $input_file  2>&1 | tail -n 4 | head -n 3 ";
	}
	
	protected function FlashVarsEncode($url)
	{
		$url = str_replace("?","%3F",$url);
		$url = str_replace("=","%3D",$url);
		$url = str_replace("&","%26",$url);
		return $url;
	}
	
	protected function Hash()
	{
  		$hash = "";
		$possible = "123456789abcdefghijkmnpqrstuvwxyzABCDEFGHKLMNPQRSTUVWXYZ"; 
		$i = 0; 
		while ($i < MEDIA_HASH_LENGTH)
		{ 
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($hash, $char))
			{
				$hash .= $char;
				$i++;
			}
		}
		return $hash;
	}
	
	protected function Hash2()
	{
		return md5(uniqid(rand(),true));
	}
	
	public function IsHash($hash)
	{
		return strlen($hash) == MEDIA_HASH_LENGTH;
	}
	
	public function Index($id_media)
	{
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->IndexQueueAdd($this->id_res_type,$id_media,0,0,1);
	}
	
	public function IsInEncodeQueue($id_media)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_media FROM media_queue WHERE id_media='$id_media' AND id_type='{$this->id_type}' ");
		return $row['id_media']>0;
	}

	public function KeywordAdd($id_keyword,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "INSERT INTO topic_keywords (id_topic,id_keyword,id_res_type,role) VALUES ('0','$id_keyword','$this->id_res_type','$role')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordDelete($id_keyword)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "DELETE FROM topic_keywords WHERE id_topic='0' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordGet($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk 
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword 
			WHERE k.id_keyword='$id_keyword' AND id_topic='0' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function KeywordUpdate($id_keyword,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "UPDATE topic_keywords SET id_res_type='$this->id_res_type',role='$role' 
			WHERE id_topic='0' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordsInternal()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT tk.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword
			WHERE tk.id_topic='0' ";
		$sqlstr .= " AND (tk.id_res_type=0 OR tk.id_res_type='$this->id_res_type') ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function KeywordsInternalAvailable(&$rows)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description FROM keywords k 
			WHERE k.id_type=4 AND k.id_keyword NOT IN (SELECT id_keyword FROM topic_keywords WHERE id_topic='0') ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	protected function MediaDelete($id_media,$format)
	{
		$tables = array("media_info","media_queue","media_playlists");
		$db =& Db::globaldb();
		foreach($tables as $table)
		{
			$db->begin();
			$db->lock( $table );
			$res[] = $db->query( "DELETE FROM $table WHERE id_media='$id_media' AND id_type='{$this->id_type}' " );
			Db::finish( $res, $db);
		}
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_media,$this->id_res_type);
		$this->h->HistoryAdd($this->id_res_type,$id_media,$this->h->actions['delete']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($this->id_res_type,$id_media);		
		$origfile = $this->irl->PathAbs("{$this->type}_orig",array('id'=>$id_media,'format'=>$format));
		$this->fm->Delete($origfile);
		$encfile = $this->irl->PathAbs("{$this->type}_enc",array('id'=>$id_media));
		$this->fm->Delete($encfile);
	}

	protected function MediaGet($id_media)
	{
		return array('id'=>$id_media);
	}
	
	protected function MediaInfo($filename)
	{
		$filename = $this->fm->RealPath($filename);
		$command = $this->FfmpegInfoCommand($filename);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$feedback = $v->Exec($command);
		$info = "";
		$length = 0;
		foreach($feedback as $frow)
		{
			$info .= trim($frow) . "\n";
			if(strpos($frow,'uration')>0)
			{
				$match =  array();
				preg_match("/uration: ([0-9]*):([0-9]*):([0-9]*).(.*)/",$frow,$match);
				if(is_numeric($match[2]) && is_numeric($match[3]))
					$length = ((int)$match[2])*60 + (int)$match[3];
			}
		}
		return array('info'=>$info,'length'=>$length);
	}

	protected function MediaInfoUpdate($id_media,$info,$is_orig)
	{
		if($is_orig)
		{
			$info_sqlstr = "UPDATE media_info SET original='$info' WHERE id_media='$id_media' AND id_type='{$this->id_type}' ";
		}
		else 
		{
			$info_sqlstr = "UPDATE media_info SET encoded='$info' WHERE id_media='$id_media' AND id_type='{$this->id_type}'";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "media_info" );
		$res[] = $db->query( $info_sqlstr );
		Db::finish( $res, $db);		
	}
	
	protected function PublishMedia($id_media,$hash,$hash2,$title)
	{
		$origfile = $this->irl->PathAbs("{$this->type}_enc",array('id'=>$id_media));
		$pub_file = $this->irl->PublicPath("{$this->type}_enc",array('hash2'=>$hash2),true,true);
		$this->fm->Copy($origfile,$pub_file);
		$xml = $this->PublishXml($hash,$title);
		$xml_file = $this->irl->PublicPath("{$this->type}_xml",array('hash'=>$hash),true,true);
		$this->fm->WritePage($xml_file,$xml);
	}

	public function PublishXml($hash,$title)
	{
		$tl = $this->Tracklist($hash,$title);
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		$xh->root_element = "playlist";
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xml .= $xh->Array2Xml($tl,false);
		return $xml;
	}

	public function Scan($id_media)
	{
		$enc_file = $this->irl->PathAbs("{$this->type}_enc",array('id'=>$id_media));
		$this->EncodePost($id_media,$enc_file);
	}
	
	protected function Tracklist($hash,$title)
	{
		$tl = array();
		$tl['version'] = "1";
		$tl['xmlns'] = "http://xspf.org/ns/0/";
		$tl['trackList'] = array('xname'=>"trackList");
		$tl['hash'] = $hash;
		$tl['title'] = $title;
		return $tl;
	}

}

abstract class MediaEncoder
{
	public static function EncodeNext($limit)
	{
		$messages = array();
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT mq.id_media,mq.id_user,mq.id_type,u.email,u.name 
			FROM media_queue mq
			LEFT JOIN users u ON mq.id_user=u.id_user
			ORDER BY mq.insert_time ASC
			LIMIT $limit ";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
		{
			switch($row['id_type'])
			{
				case "1":
					include_once(SERVER_ROOT."/../classes/video.php");
					$vi = new Video();
					$messages = $vi->EncodeMedia($row['id_media'],$row['id_user'],$row['email'],$row['name']);
				break;
				case "2":
					include_once(SERVER_ROOT."/../classes/audio.php");
					$au = new Audio();
					$messages = $au->EncodeMedia($row['id_media'],$row['id_user'],$row['email'],$row['name']);
				break;
			}
		}
		return $messages;
	}
	
	public static function EncodeQueue(&$rows,$paged=true)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT mq.id_media,mq.id_user,mq.id_type,UNIX_TIMESTAMP(mq.insert_time) AS insert_time_ts,
			CASE WHEN id_type=1 THEN v.title WHEN id_type=2 THEN a.title END AS title,
			CASE WHEN id_type=1 THEN v.approved WHEN id_type=2 THEN a.approved END AS approved,
			CASE WHEN id_type=1 THEN v.format WHEN id_type=2 THEN a.format END AS format,
			CASE WHEN id_type=1 THEN 'video' WHEN id_type=2 THEN 'audio' END AS media_type,
			u.name as user_name,u.email as email  
			FROM media_queue mq
			LEFT JOIN videos v ON mq.id_media=v.id_video AND mq.id_type=1
			LEFT JOIN audios a ON mq.id_media=a.id_audio AND mq.id_type=2
			LEFT JOIN users u ON mq.id_user=u.id_user
			GROUP BY mq.id_media
			ORDER BY mq.insert_time ASC ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	
}
?>
