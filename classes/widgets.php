<?php
/********************************************************************

   PhPeace - Widgets Management Module

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

 ********************************************************************/

include_once (SERVER_ROOT . "/../classes/db.php");
include_once (SERVER_ROOT . "/../classes/ini.php");
include_once (SERVER_ROOT . "/../classes/history.php");
include_once (SERVER_ROOT . "/../classes/session.php");
include_once (SERVER_ROOT . "/../classes/texthelper.php");

define ( 'WIDGET_TYPE_MANUAL', 0 );
define ( 'WIDGET_TYPE_RSS', 1 );
define ( 'WIDGET_TYPE_CUSTOM', 2 );
define ( 'WIDGET_TYPE_MANUAL_ADVANCE', 3 );
define ( 'WIDGET_TYPE_FEATURE', 4 );

define ( 'WIDGET_STATUS_PENDING', 0 );
define ( 'WIDGET_STATUS_APPROVED', 1 );
define ( 'WIDGET_STATUS_REJECTED', 2 );
define ( 'WIDGET_STATUS_DELETED', 3 );

define ( 'RSS_LAYOUT_LIST_WITH_IMAGE', 0 );
define ( 'RSS_LAYOUT_LIST_ITEM', 1 );
define ( 'RSS_LAYOUT_LIST_IMAGES', 2 );
define ( 'RSS_LAYOUT_LIST_WITH_THUMBNAIL', 3 );

define ( 'WIDGET_IMAGE_SIZE', 3 );

define ( 'WIDGET_LIBARY_REC_PER_CATEGORY', 6 );
define ( 'DEFAULT_RSS_WIDGET_UPDATE_INTERVAL', 60 );

define ( 'WIDGET_CACHE_VARNAME_TABS', 'widgets_tabs' );
define ( 'WIDGET_CACHE_VARNAME_TABS_DEFAULT', 'widgets_tabs_def' );
define ( 'WIDGET_CACHE_VARNAME_WIDGETS_DEFAULT_TAB', 'widgets_def_tab' );

class Widgets {
	public $widgets_group;
	public $widgets_per_page;
	public $widgets_require_authentication;
	public $widgets_init_empty_profiles;
	public $widgets_abuse_reporting;
	public $widgets_hard_delete;
	public $types;
	
	private $id_p_anonymous;
	private $id_res_type;
	private $widgets_cache_default_ttl;
	private $widget_library_display;
	private $dr_site;
	
	/** 
	 * @var History */
	private $h;
	
	/** 
	 * @var Session */
	private $session;
	
	/** 
	 * @var SharedMem */
	private $mem;
	
	function __construct() {
		$this->id_res_type = 24;
		$ini = new Ini ();
		$this->widgets_group = $ini->GetModule ( "homepage", "widgets_group", 0 );
		$this->widgets_per_page = $ini->GetModule ( "homepage", "widgets_per_page", 15 );
		$this->widgets_require_authentication = $ini->GetModule ( "homepage", "widgets_require_authentication", false );
		$this->widgets_init_empty_profiles = $ini->GetModule ( "homepage", "widgets_init_empty_profiles", false );
		$this->widgets_abuse_reporting = $ini->GetModule ( "homepage", "widgets_abuse_reporting", true );
		$this->widgets_hard_delete = $ini->GetModule ( "homepage", "widgets_hard_delete", false );
		$conf = new Configuration ();
		$this->dr_site = $conf->Get("dr_site");
		$this->widgets_cache_default_ttl = $conf->Get ( "widgets_cache_default_ttl" );
		$this->h = new History ();
		$this->session = new Session ();
		$this->mem = new SharedMem ();
		$this->types = array ("Manual", "RSS", "Custom", "Advanced Manual", "Feature" );
		$this->widget_library_display = $conf->Get ( "widget_library_display" );
	}
	
	public function PublicCategories() {
		$data = explode ( ',', $this->widget_library_display );
		$sql = '';
		$excluded_categories = '';
		$sort_order = 0;
		for($i = 0; $i < count ( $data ); $i ++) {
			$name = $data [$i];
			$sort_order = $i + 1;
			if ($sql == '') {
				$sql = "SELECT id_category, name, $sort_order as sort_order, 1 as library_show_widget
						FROM widgets_categories
						WHERE name='$name'
						";
				$excluded_categories = "'" . $name . "'";
			} else {
				$sql .= "UNION
						SELECT id_category, name, $sort_order as sort_order, 1 as library_show_widget
						FROM widgets_categories
						WHERE name='$name'
						";
				$excluded_categories .= ",'" . $name . "'";
			}
		}
		if ($excluded_categories != '') {
			$sort_order ++;
			$sql .= "UNION
					SELECT id_category, name, $sort_order as sort_order, 0 as library_show_widget
					FROM widgets_categories
					WHERE name NOT IN ($excluded_categories)
					ORDER BY sort_order, name
					";
		} else {
			$sql = "SELECT id_category, name, 0 as library_show_widget
					FROM widgets_categories
					ORDER BY name
					";
		}
		
		$rows = array ();
		$db = & Db::globaldb ();
		$db->QueryExe ( $rows, $sql );
		return $rows;
	}
	
	public function Categories() {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_category,name
        	 FROM widgets_categories
                    ORDER BY name
                    ";
		$db->QueryExe ( $rows, $sqlstr );
		return $rows;
	}
	
	public function CategoryDelete($id_category) {
		$widgets = array ();
		$num_widgets = $this->CategoryWidgets ( $widgets, $id_category );
		if ($num_widgets == 0) {
			$db = & Db::globaldb ();
			$db->begin ();
			$db->lock ( "widgets_categories" );
			$res [] = $db->query ( "DELETE FROM widgets_categories WHERE id_category='$id_category' " );
			Db::finish ( $res, $db );
		}
	}
	
	public function CategoryGet($id_category) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_category,name,description
			FROM widgets_categories
			WHERE id_category='$id_category'";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function CategoryGetByName($name) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_category,name,description
            FROM widgets_categories
            WHERE name='$name'";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function CategoryStore($id_category, $name, $description) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_categories" );
		if ($id_category > 0) {
			$sqlstr = "UPDATE widgets_categories SET name='$name',description='$description'
				WHERE id_category='$id_category' ";
		} else {
			$id_category = $db->nextId ( "widgets_categories", "id_category" );
			$sqlstr = "INSERT INTO widgets_categories (id_category,name,description) 
				VALUES ('$id_category','$name','$description')  ";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		return $id_category;
	}
	
	public function CategoryWidgets(&$rows, $id_category, $paged = false, $approved = false) {
		$db = & Db::globaldb ();
		$sqlstr = "SELECT w.id_widget,w.title,w.description,w.status,w.comment,'widget' AS item_type
        	 FROM widgets_category_widgets wcw
        	 INNER JOIN widgets w ON wcw.id_widget=w.id_widget
        	 WHERE wcw.id_category='$id_category' ";
		if ($approved)
			$sqlstr .= " AND w.status=1 ";
		$num = $db->QueryExe ( $rows, $sqlstr, $paged, MYSQL_BOTH, $this->widgets_per_page );
		$this->WidgetStatusAdd ( $rows );
		return $num;
	}
	
	public function TotalApprovedWidgetsByCategory($id_category) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "
            SELECT COUNT(wcw.id_widget) as count_value
        	 FROM widgets_category_widgets wcw
        	 INNER JOIN widgets w 
        	 ON wcw.id_widget=w.id_widget
        	 WHERE wcw.id_category='$id_category' 
        	 AND w.status=1 
        	 AND wcw.is_selected=1 ";
		$db->query_single ( $row, $sqlstr );
		return $row ['count_value'];
	}
	
	public function CategoryWidgetsApprovedSelected(&$rows, $id_category) {
		$rows = array ();
		
		$db = & Db::globaldb ();
		$sqlstr = "SELECT w.id_widget,w.title,w.description,w.status,w.comment,'widget' AS item_type
        	 FROM widgets_category_widgets wcw
        	 INNER JOIN widgets w 
        	 ON wcw.id_widget=w.id_widget
        	 WHERE wcw.id_category='$id_category' 
        	 AND w.status=1 
        	 AND wcw.is_selected=1 ";
		$num = $db->QueryExe ( $rows, $sqlstr, true, MYSQL_BOTH, $this->widgets_per_page );
		return $num;
	}
	
	public function CategoryWidgetsSelection($id_category) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT w.id_widget,w.title,w.description,w.status,w.comment,'widget' AS item_type
        	 FROM widgets_category_widgets wcw
        	 INNER JOIN widgets w ON wcw.id_widget=w.id_widget
        	 WHERE wcw.id_category='$id_category' AND w.status=1 AND wcw.is_selected=1 ";
		$db->QueryExe ( $rows, $sqlstr );
		$this->WidgetStatusAdd ( $rows );
		return $rows;
	}
	
	public function CategoryWidgetsSelectionDelete($id_category) {
		$sqlstr = "UPDATE widgets_category_widgets SET is_selected=0 WHERE id_category='$id_category' ";
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_category_widgets" );
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function CategoryWidgetsSelectionAdd($id_category, $id_widget) {
		$sqlstr = "UPDATE widgets_category_widgets SET is_selected=1 WHERE id_category='$id_category' AND id_widget='$id_widget' ";
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_category_widgets" );
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function IdExtract($id_string, $position) {
		$len_string = strlen ( $id_string ) - $position;
		return ( int ) substr ( $id_string, $position, $len_string );
	}
	
	private function IdTabExtract($tab_string, $default_value = 1) {
		return ($tab_string == 'undefined') ? "$default_value" : $this->IdExtract ( $tab_string, 4 );
	}
	
	private function IdWidgetExtract($widget_string, $default_value = 1) {
		return ($widget_string == 'undefined') ? "$default_value" : $this->IdExtract ( $widget_string, 7 );
	}
	
	public function WidgetCategories($id_widget) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT wcw.id_category,wc.name 
			FROM widgets_category_widgets wcw 
			INNER JOIN widgets_categories wc ON wcw.id_category=wc.id_category
			WHERE wcw.id_widget='$id_widget'";
		$db->QueryExe ( $rows, $sqlstr );
		return $rows;
	}
	
	public function WidgetCategoriesDelete($id_widget) {
		$sqlstr = "DELETE FROM widgets_category_widgets WHERE id_widget='$id_widget' ";
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_category_widgets" );
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function WidgetCategoryAdd($id_widget, $id_category) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_category_widgets" );
		$sqlstr = "INSERT INTO widgets_category_widgets (id_category,id_widget,is_selected) 
				VALUES ('$id_category','$id_widget',1)  ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function MyWidgets(&$rows, $id_p, $paged = true, $limit_rec = false) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_widget, title, description, status, comment, comment_visible, widget_type, 1 as can_edit, insert_date, 'widget' AS item_type
            FROM widgets
            WHERE id_p = '$id_p' AND id_widget_parent = 0 AND status <> 3
            UNION
            SELECT w.id_widget, w.title, w.description, w.status, w.comment, w.comment_visible, w.widget_type, ws.can_edit, ws.insert_date, 'widget' AS item_type 
            FROM widgets w
            INNER JOIN widget_sharings ws
            ON w.id_widget = ws.id_widget
            AND ws.id_p = '$id_p'
            AND ws.status = 1 AND w.status <> " . WIDGET_STATUS_DELETED . " 
            ORDER BY insert_date DESC
            ";
		if ($limit_rec && ! $paged)
			$sqlstr .= ' LIMIT 0, ' . WIDGET_LIBARY_REC_PER_CATEGORY;
		$num = $db->QueryExe ( $rows, $sqlstr, $paged, MYSQL_BOTH, $this->widgets_per_page );
		$this->WidgetStatusAdd ( $rows );
		//print_r($rows);
		return $num;
	}
	
	private function WidgetStatusAdd(&$widgets) {
		$statuses = $this->Statuses ( true );
		foreach ( $widgets as &$widget ) {
			$widget ['status_name'] = $statuses [$widget ['status']];
		}
	}
	
	public function WidgetSearchPub(&$rows, $words, $id_category = 0) {
		$id_p = $this->WidgetUserGet ();
		if ($id_p == 0)
			$id_p = - 1;
		if (count ( $words ) > 0) {
			$db = & Db::globaldb ();
			$sqljoin = "";
			for($i = 0; $i < count ( $words ); $i ++) {
				$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = w.id_widget AND si$i.id_res={$this->id_res_type} ";
				$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
				$wheres [] = "sw$i.word = '$words[$i]'";
				$scores [] = "SUM(si$i.score)";
			}
			if ($id_category > 0)
				$sqljoin_category = "INNER JOIN widgets_category_widgets wcw 
									ON w.id_widget = wcw.id_widget 
									AND wcw.id_category = '$id_category'
									AND wcw.is_selected=1 ";
			else
				$sqljoin_category = "";
			
			$sqlstr = "SELECT CASE WHEN w.id_widget_parent = 0 THEN w.id_widget ELSE w.id_widget_parent END AS id_widget, IFNULL(wp.title,w.title) as title,IFNULL(wp.description,w.description) as description,'widget' AS item_type, 
				(" . implode ( "+", $scores ) . ") AS total_score 
			FROM widgets w 
            LEFT JOIN widgets wp ON wp.id_widget = w.id_widget_parent
            LEFT JOIN widget_sharings ws ON ws.id_widget = w.id_widget AND ws.status = 1 $sqljoin $sqljoin_category 
			 WHERE (w.shareable = 1 OR w.id_p = {$id_p} OR ws.id_p = {$id_p}) AND w.status <> " . WIDGET_STATUS_DELETED . " AND " . implode ( " AND ", $wheres ) . "
			 GROUP BY w.id_widget
			ORDER BY w.title";
			$num = $db->QueryExe ( $rows, $sqlstr, true, MYSQL_BOTH, true, $this->widgets_per_page );
		} else
			$num = 0;
		return $num;
	}
	
	public function WidgetAbusesAll(&$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT wa.id_widget_abuse, w.id_widget, w.title, wa.status, wa.comments, wa.insert_date, 
            case when wa.id_p = 0  then wa.email
                else concat(p.name1,' ',p.name2)
            end as name
            FROM widget_abuses wa
            INNER JOIN widgets w
            ON wa.id_widget = w.id_widget
            LEFT JOIN people p
            ON wa.id_p = p.id_p
            ";
		$sqlstr .= " ORDER BY id_widget_abuse DESC ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetAbuseGet($id_widget_abuse) {
		$row = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "
            SELECT wa.id_widget_abuse, w.id_widget, w.title, p.id_p, wa.status, wa.comments, wa.insert_date, 
            case when wa.id_p = 0  then wa.email
                else concat(p.name1,' ',p.name2)
            end as name
            FROM widget_abuses wa
            INNER JOIN widgets w
            ON wa.id_widget = w.id_widget
            LEFT JOIN people p
            ON wa.id_p = p.id_p
            WHERE wa.id_widget_abuse = $id_widget_abuse";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function WidgetAbuseStore($id_widget_abuse, $id_widget, $id_p, $comments, $status, $email, $to_escape = false) {
		$db = & Db::globaldb ();
		if ($to_escape) {
			$comments = $db->SqlQuote ( $comments );
		}
		$db->begin ();
		$db->lock ( "widget_abuses" );
		if ($id_widget_abuse > 0) {
			$sqlstr = "UPDATE widget_abuses 
                SET id_widget='$id_widget',id_p='$id_p',comments='$comments',
                status='$status'
                WHERE id_widget_abuse='$id_widget_abuse' ";
		} else {
			$id_widget_abuse = $db->nextId ( "widget_abuses", "id_widget_abuse" );
			$sqlstr = "INSERT INTO widget_abuses 
                (id_widget_abuse, id_widget, id_p, comments, email, insert_date, status) 
                VALUES 
                ('$id_widget_abuse', '$id_widget', '$id_p', '$comments', '$email', NOW(), '$status')  ";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		return $id_widget_abuse;
	}
	
	public function WidgetsAll(&$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT id_widget, identifier, title, description, widget_type, status, shareable, id_p, insert_date, ip
            FROM widgets 
            WHERE id_widget_parent = 0
            ";
		$sqlstr .= " ORDER BY id_widget DESC ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsPublished(&$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT w.id_widget, w.title, w.description, w.widget_type, w.status, w.shareable, w.id_p, w.insert_date, w.ip
            FROM widgets w
            INNER JOIN widgets_category_widgets wcw
            ON wcw.id_widget = w.id_widget
            AND wcw.is_selected = 1
            WHERE w.status = 1
			ORDER BY w.title ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsApproved(&$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT id_widget, identifier, title, description, widget_type, status, shareable, id_p, insert_date, ip
            FROM widgets
            WHERE id_widget_parent = 0 AND status = 1
            ";
		$sqlstr .= " ORDER BY title ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsBySearchCriteria($id_widget, $widget_type, $owner, $status, &$rows, $paged = true) {
		$statuses = $this->Statuses ( true );
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlsearch = '';
		if ($id_widget != 0)
			$sqlsearch .= "AND id_widget = '$id_widget' ";
		if ($widget_type != count ( $this->types ))
			$sqlsearch .= "AND widget_type = '$widget_type' ";
		if ($owner != 2) {
			if ($owner == 0)
				$sqlsearch .= "AND id_p = 0 ";
			else
				$sqlsearch .= "AND id_p <> 0 ";
		}
		if ($status < count ( $statuses ))
			$sqlsearch .= "AND status = '$status' ";
		
		$sqlstr = "
            SELECT w.id_widget, w.identifier, w.title, w.description, w.widget_type, w.status, w.shareable, w.id_p, UNIX_TIMESTAMP(w.insert_date) AS insert_date_ts
            FROM widgets w WHERE w.id_widget_parent = 0 ";
		$sqlstr .= $sqlsearch;
		$sqlstr .= " ORDER BY id_widget DESC ";
		
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsByWebFeed($id_web_feed, &$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
	        SELECT w.id_widget, w.title  
	        FROM widgets w
	        INNER JOIN (widget_web_feeds wwf
	        			INNER JOIN web_feeds wf
	        			ON wwf.id_web_feed = wf.id_web_feed
	        			AND wwf.id_web_feed = '$id_web_feed')
	        ON w.id_widget = wwf.id_widget
	        WHERE w.status <> 3
	        ";
		$sqlstr .= " ORDER BY w.title ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsByWidgetType($widget_type, &$rows, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		if ($widget_type == WIDGET_TYPE_RSS)
			$sqlstr = "
	            SELECT w.id_widget, w.title, wp.params, wwf.id_web_feed 
	            FROM widgets w
	            INNER JOIN widget_params wp
	            ON w.id_widget = wp.id_widget
	            LEFT JOIN widget_web_feeds wwf
	            ON w.id_widget = wwf.id_widget
	            WHERE w.status <> 3 AND w.id_widget_parent = 0
	            AND w.widget_type = '$widget_type' 
	            ";
		else
			$sqlstr = "
	            SELECT w.id_widget, w.title, wp.params 
	            FROM widgets w
	            INNER JOIN widget_params wp
	            ON w.id_widget = wp.id_widget
	            WHERE w.status <> 3 AND w.id_widget_parent = 0
	            AND w.widget_type = '$widget_type' 
	            ";
		$sqlstr .= " ORDER BY w.title ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsUsage(&$rows) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT wtp.id_widget,w.title,COUNT(wtp.id_p) AS counter 
            FROM widgets_tabs_position wtp 
            INNER JOIN widgets w ON wtp.id_widget=w.id_widget 
            WHERE wtp.id_p>0 
            GROUP BY w.id_widget ORDER BY counter DESC,title ";
		return $db->QueryExe ( $rows, $sqlstr, true );
	}
	
	public function WidgetGet($id_widget) {
		$row = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "SELECT w.id_widget, w.identifier, w.title, w.description, w.widget_type, w.status, w.shareable, w.id_p, 
                    w.insert_date, w.ip, w.comment, w.comment_visible, 
            UNIX_TIMESTAMP(insert_date) AS insert_date_ts, wwf.id_web_feed
            FROM widgets w
            LEFT JOIN widget_web_feeds wwf 
            ON w.id_widget = wwf.id_widget
            WHERE w.id_widget = $id_widget";
		$db->query_single ( $row, $sqlstr, 1 );
		return $row;
	}
	
	private function IsWidgetShareDuplicate($id_widget, $email_list) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sql = "
        	SELECT COUNT(id_widget_sharing) as 'count'
        	FROM widget_sharings
        	WHERE id_widget = '$id_widget'
        	AND email in ($email_list)
        ";
		$db->query_single ( $rows, $sql );
		
		return ($rows ['count'] > 0);
	}
	
	/*
    	pre-condition: widget cannot be deleted
    	own created widget OR widget shared by somebody but given permission to edit
    */
	public function IsWidgetEditable($id_p, $id_widget) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sql = "
        	SELECT COUNT(id_widget) as 'count'
        	FROM widgets
        	WHERE id_widget IN (
        			SELECT id_widget 
        			FROM 
        			widgets 
        			WHERE id_p = '$id_p' 
        			AND id_widget = '$id_widget'
        			AND status<>3
        			UNION 
        			SELECT w.id_widget 
        			FROM widgets w
        			INNER JOIN widget_sharings ws
        			ON w.id_widget = ws.id_widget
        			AND ws.id_p = '$id_p'
        			AND ws.status = 1
        			AND ws.can_edit = 1
        			WHERE w.id_widget = '$id_widget'
        			AND w.status <> 3
        						)
        ";
		$db->query_single ( $rows, $sql );
		
		return ($rows ['count'] > 0);
	}
	
	public function IsWidgetAvailable($id_p, $id_widget) {
		$sql = "
            SELECT COUNT(*) as 'count'
            FROM widgets_tabs_position
            WHERE id_p = {$id_p} AND id_widget = {$id_widget}
        ";
		
		$rows = array ();
		$db = & Db::globaldb ();
		$db->query_single ( $rows, $sql );
		
		return ($rows ['count'] > 0);
	}
	
	public function IsWidgetAvailableExcludeTabPosition($id_p, $id_widget, $id_tab, $col, $pos) {
		
		$rows = array ();
		$db = & Db::globaldb ();
		$sql = "
            SELECT COUNT(*) as 'count'
            FROM widgets_tabs_position
            WHERE id_p = {$id_p} 
            AND id_widget = {$id_widget}
            AND id_widget NOT IN 
            				(
            					SELECT id_widget
            					FROM widgets_tabs_position
            					WHERE id_tab = {$id_tab} 
            					AND col = {$col} 
            					AND pos = {$pos} 
            				)
        ";
		$db->query_single ( $rows, $sql );
		return ($rows ['count'] > 0);
	}
	
	private function IsWidgetApproved($id_widget) {
		$sql = "
            SELECT COUNT(*) as 'count'
            FROM widgets
            WHERE id_widget = {$id_widget} and status = 1
        ";
		
		$rows = array ();
		$db = & Db::globaldb ();
		$db->query_single ( $rows, $sql );
		
		return ($rows ['count'] > 0);
	}
	
	private function IsWidgetSharedToUser($id_widget, $id_p) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sql = "
            SELECT COUNT(*) as 'count'
            FROM widget_sharings
            WHERE id_widget = '$id_widget' 
            and id_p = '$id_p'
            and status = 1
        ";
		$db->query_single ( $rows, $sql );
		
		return ($rows ['count'] > 0);
	}
	
	public function WidgetStore($id_widget, $title, $description, $widget_type, $status, $shareable, $comment, $comment_visible, $keywords, $id_widget_parent = 0, $is_user_action = false) {
		$insert = ! $id_widget > 0;
		if (! $insert) {
			$widget_old = $this->WidgetGet ( $id_widget );
		}
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets" );
		if ($id_widget > 0) {
			$sqlstr = "UPDATE widgets 
                SET title='$title',description='$description',widget_type='$widget_type',
                status='$status',shareable='$shareable',
                comment='$comment', comment_visible='$comment_visible'
                ";
			if ($is_user_action) {
				$sqlstr .= ", last_update = NOW() ";
			}
			$sqlstr .= "WHERE id_widget='$id_widget' ";
		} else {
			$id_widget = $db->nextId ( "widgets", "id_widget" );
			$sqlstr = "INSERT INTO widgets 
                (id_widget,title,description,widget_type,status,shareable,comment, comment_visible, insert_date, id_widget_parent, last_update) 
                VALUES 
                ('$id_widget','$title','$description','$widget_type','$status','$shareable', '$comment', '$comment_visible', NOW(), $id_widget_parent, NOW()) ";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		include_once (SERVER_ROOT . "/../classes/ontology.php");
		$o = new Ontology ();
		$o->InsertKeywords ( $keywords, $id_widget, $o->types ['widget'] );
		include_once (SERVER_ROOT . "/../classes/search.php");
		$s = new Search ();
		if ($status == "1") {
			$s->IndexQueueAdd ( $o->types ['widget'], $id_widget, 0, 0, 1 );
		} else {
			$s->ResourceRemove ( $o->types ['widget'], $id_widget );
		}
		$action1 = $insert ? "create" : "update";
		$this->h->HistoryAdd ( $o->types ['widget'], $id_widget, $this->h->actions [$action1] );
		if ($status > 0 && ($insert || $status != $widget_old ['status'])) {
			$action2 = $this->HistoryActionByStatus ( $status );
			$this->h->HistoryAdd ( $o->types ['widget'], $id_widget, $this->h->actions [$action2] );
		}
		if ($status == WIDGET_STATUS_DELETED)
			$this->WidgetsDefaultTabsCacheReset ();
		
		return $id_widget;
	}
	
	public function WidgetStatusUpdate($id_widget, $status) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets" );
		$sqlstr = "UPDATE widgets 
            SET status='$status'
            WHERE id_widget='$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function HistoryActionByStatus($status) {
		$action = "";
		switch ($status) {
			case "1" :
				$action = "approve";
				break;
			case "2" :
				$action = "reject";
				break;
			case "3" :
				$action = "delete";
				break;
			case "4" :
				$action = "on_hold";
				break;
		}
		return $action;
	}
	
	public function WidgetWebFeedStore($id_widget, $id_web_feed, $items_display) {
		$row = $this->WidgetWebFeedGet ( $id_widget );
		
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widget_web_feeds" );
		if ($row ['id_widget'] > 0) {
			$sqlstr = "UPDATE widget_web_feeds 
                SET id_web_feed='$id_web_feed', items_display='$items_display' 
                WHERE id_widget='$id_widget' ";
		} else {
			$sqlstr = "INSERT INTO widget_web_feeds 
                (id_widget, id_web_feed, items_display) 
                VALUES 
                ('$id_widget','$id_web_feed','$items_display')";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function WidgetWebFeedGet($id_widget) {
		$row = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "
            SELECT id_widget
            FROM widget_web_feeds  
            WHERE id_widget = $id_widget";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function PublicWidgetGet($id_widget) {
		$row = $this->WidgetGet ( $id_widget );
		$widget_params = $this->WidgetParamGet ( $id_widget );
		
		include_once (SERVER_ROOT . "/../classes/varia.php");
		$v = new Varia ();
		$params = $v->Deserialize ( $widget_params ['params'] );
		
		switch ($row ['widget_type']) {
			case WIDGET_TYPE_RSS :
				//$row['id_web_feed'] = $params['id_web_feed'];
				$row ['items_display'] = $params ['items_display'];
				$row ['layout_type'] = $params ['layout_type'];
				include_once (SERVER_ROOT . "/../classes/web_feeds.php");
				$wf = new WebFeeds ();
				$feed = $wf->WebFeedGet ( $row ['id_web_feed'] );
				$row ['url'] = $feed ['feed_url'];
				break;
			case WIDGET_TYPE_MANUAL :
				$row ['content'] = stripcslashes ( $params ['content'] );
				$row ['link_url'] = $params ['link_url'];
				break;
			case WIDGET_TYPE_MANUAL_ADVANCE :
				$row ['childs'] = $this->PublicWidgetChildGet ( $id_widget, 5 );
				break;
		}
		
		return $row;
	}
	
	public function PublicWidgetChildGet($id_widget, $limit = 0) {
		$rows = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "SELECT w.id_widget, w.title, w.description, wp.params
            FROM widgets w INNER JOIN widget_params wp ON w.id_widget = wp.id_widget
            WHERE w.id_widget_parent = $id_widget
            ORDER BY w.id_widget DESC ";
		
		if ($limit > 0)
			$sqlstr .= "LIMIT $limit";
		$db->QueryExe ( $rows, $sqlstr, false, 1 );
		
		$v = new Varia ();
		
		$childs = array ();
		foreach ( $rows as $value ) {
			$params = $v->Deserialize ( $value ['params'] );
			unset ( $value ['params'] );
			$value ['content'] = $params ['content'];
			$value ['link_url'] = $params ['link_url'];
			$childs [] = $value;
		}
		
		return $childs;
	}
	
	public function PublicWidgetStoreIP($id_widget, $id_p, $ip) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets" );
		$sqlstr = "UPDATE widgets 
            SET id_p='$id_p',ip='$ip' 
                WHERE id_widget='$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function PublicWidgetStore($id_widget, $title, $description, $content, $url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p, $id_widget_parent = 0) {
		$new_widget = ($id_widget < 1);
		include_once (SERVER_ROOT . "/../classes/varia.php");
		$v = new Varia ();
		$ip = $v->IP ();
		
		if ($new_widget) {
			// if call by widget_import.php, widget status = approved
			if ($id_p == 0)
				$id_widget = $this->WidgetStore ( $id_widget, $title, $description, $widget_type, WIDGET_STATUS_APPROVED, 1, '', true, "", $id_widget_parent );
			else
				$id_widget = $this->WidgetStore ( $id_widget, $title, $description, $widget_type, WIDGET_STATUS_PENDING, 1, '', true, "", $id_widget_parent, true );
			$this->PublicWidgetStoreIP ( $id_widget, $id_p, $ip );
		} else {
			$widget = $this->WidgetGet ( $id_widget );
			if ($widget ['status'] == WIDGET_STATUS_REJECTED)
				$widget ['status'] = WIDGET_STATUS_PENDING;
			$id_widget = $this->WidgetStore ( $id_widget, $title, $description, $widget_type, $widget ['status'], 1, '', true, "", $id_widget_parent, true );
		}
		
		switch ($widget_type) {
			case WIDGET_TYPE_RSS :
				include_once (SERVER_ROOT . "/../classes/web_feeds.php");
				$wf = new WebFeeds ();
				$web_feeds = $wf->WebFeedGetByUrl ( $url );
				if ($web_feeds ['id_web_feed'] != 0)
					$id_web_feed = $web_feeds ['id_web_feed'];
				else {
					if ($id_p == 0)
						$id_web_feed = $wf->WebFeedStore ( 0, $title, $url, 20, true );
					else
						$id_web_feed = $wf->WebFeedStore ( 0, $title, $url, DEFAULT_RSS_WIDGET_UPDATE_INTERVAL );
				}
				
				$params = $v->Serialize ( array ('items_display' => $rss_items_display, 'layout_type' => $rss_layout, 'id_web_feed' => $id_web_feed ) );
				break;
			case WIDGET_TYPE_MANUAL :
				$params = $v->Serialize ( array ("link_url" => $url, "content" => $content ) );
				break;
			case WIDGET_TYPE_MANUAL_ADVANCE :
				$params = $v->Serialize ( array ('layout_type' => $rss_layout ) );
				break;
		}
		
		if ($new_widget) {
			$this->WidgetParamStore ( 0, $id_widget, $params );
			if ($widget_type == WIDGET_TYPE_RSS)
				$this->WidgetWebFeedStore ( $id_widget, $id_web_feed, $rss_items_display );
		} else {
			$this->WidgetParamStore ( 1, $id_widget, $params );
			if ($widget_type == WIDGET_TYPE_RSS)
				$this->WidgetWebFeedStore ( $id_widget, $id_web_feed, $rss_items_display );
		
		}
		
		return $id_widget;
	}
	
	public function PublicWidgetDelete($id_widget) {
		$row = $this->PublicWidgetGet ( $id_widget );
		
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets" );
		$sqlstr = "UPDATE widgets 
            SET status=" . WIDGET_STATUS_DELETED . ", last_update = NOW() WHERE id_widget='$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		
		include_once (SERVER_ROOT . "/../classes/varia.php");
		$v = new Varia ();
		
		switch ($row ['widget_type']) {
			case WIDGET_TYPE_RSS :
				include_once (SERVER_ROOT . "/../classes/web_feeds.php");
				$wf = new WebFeeds ();
				$wf->WebFeedDelete ( $row ['id_web_feed'] );
				$params = $v->Serialize ( array ("id_web_feed" => 0, 'items_display' => $row ['items_display'], 'layout_type' => $row ['layout_type'] ) );
				$this->WidgetParamStore ( 1, $id_widget, $params );
				break;
			case WIDGET_TYPE_MANUAL_ADVANCE :
				$this->PublicWidgetChildDeleteAll ( $id_widget );
				break;
		}
		
		$this->WidgetsDefaultTabsCacheReset ();
	}
	
	private function PublicWidgetChildDeleteAll($id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( array ("widgets", "widget_params" ) );
		$sqlstr = "DELETE FROM widget_params 
            WHERE id_widget IN (SELECT id_widget FROM widgets WHERE id_widget_parent = '$id_widget') ";
		$res [] = $db->query ( $sqlstr );
		$sqlstr = "DELETE FROM widgets 
            WHERE id_widget_parent = '$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function PublicWidgetChildDelete($id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( array ("widgets", "widget_params" ) );
		$sqlstr = "DELETE FROM widget_params 
            WHERE id_widget = '$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		$sqlstr = "DELETE FROM widgets 
            WHERE id_widget = '$id_widget' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		
		if (! in_array ( false, $res ))
			$this->WidgetImageDelete ( $id_widget );
	}
	
	public function WidgetDelete($id_widget) {
		$db = & Db::globaldb ();
		$tables = array ('widgets', 'widget_abuses', 'widget_params', 'widget_sharings', 'widget_web_feeds', 'widgets_category_widgets', 'widgets_tabs_position' );
		foreach ( $tables as $table ) {
			$db->begin ();
			$db->lock ( $table );
			$res [] = $db->query ( "DELETE FROM $table WHERE id_widget='$id_widget' " );
			Db::finish ( $res, $db );
		}
	}
	
	public function WidgetSoftDelete($id_widget, $is_user_action = false) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets" );
		$sqlstr = "UPDATE widgets
                    SET status = 3
                ";
		if ($is_user_action) {
			$sqlstr .= ", last_update = NOW() ";
		}
		$sqlstr .= "WHERE id_widget = '$id_widget'";
		
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		$this->h->HistoryAdd ( $this->id_res_type, $id_widget, $this->h->actions ['delete'] );
		$this->WidgetsDefaultTabsCacheReset ();
	}
	
	public function WidgetParamGet($id_widget) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_widget, params
            FROM widget_params 
            WHERE id_widget = $id_widget";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function WidgetParamStore($id_action, $id_widget, $params) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widget_params" );
		if ($id_action > 0) {
			$sqlstr = "UPDATE widget_params 
                SET params='$params'
                WHERE id_widget='$id_widget' ";
		} else {
			$sqlstr = "INSERT INTO widget_params 
                (id_widget,params) 
                VALUES 
                ('$id_widget','$params')";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		return $id_widget;
	}
	
	public function WidgetSharingGet($id_widget) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "
            SELECT id_widget_sharing, id_widget, id_p, insert_date, email, status, can_edit
            FROM widget_sharings 
            WHERE id_widget = $id_widget";
		$db->QueryExe ( $rows, $sqlstr, false, 1 );
		
		return $this->xnameArray ( $rows );
	}
	
	private function xnameArray($rows) {
		$shares = array ();
		$i = 1;
		foreach ( $rows as $value ) {
			$shares ['share' . $i] = $value;
			$shares ['share' . $i] ['xname'] = 'share';
		}
		return $shares;
	}
	
	public function WidgetSharingDelete($id_widget_sharing) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widget_sharings" );
		$sqlstr = "
            DELETE FROM widget_sharings 
            WHERE id_widget_sharing=$id_widget_sharing 
        ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function WidgetSharingDeleteByUserAndWidget($id_p, $id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widget_sharings" );
		$sqlstr = "
            DELETE FROM widget_sharings 
            WHERE id_p = '$id_p'
            AND id_widget = '$id_widget' 
        ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function WidgetSharingStore($id_widget, $email, $can_edit) {
		$db = & Db::globaldb ();
		
		$email = $db->SqlQuote ( $email );
		$id_widget_sharing = $this->WidgetSharingGetShareId ( $id_widget, $email );
		$id_p = 0;
		
		$db->begin ();
		$db->lock ( "widget_sharings" );
		
		if ($id_widget_sharing > 0) {
			$sqlstr = "
                UPDATE widget_sharings 
                SET can_edit = {$can_edit}
                WHERE id_widget_sharing={$id_widget_sharing} 
            ";
		} else {
			$id_widget_sharing = $db->nextId ( "widget_sharings", "id_widget_sharing" );
			$sqlstr = "
                INSERT INTO widget_sharings 
                (id_widget_sharing,id_widget,id_p,insert_date,email,can_edit) 
                VALUES 
                ($id_widget_sharing,$id_widget,$id_p,NOW(),'$email',$can_edit)
            ";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		return $id_widget_sharing;
	}
	
	private function WidgetSharingGetShareId($id_widget, $email) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sql = "
            SELECT IFNULL(MAX(id_widget_sharing), 0) as 'id_widget_sharing'
            FROM widget_sharings
            WHERE id_widget = {$id_widget}
            AND email = '$email'
        ";
		$db->query_single ( $rows, $sql );
		return $rows ['id_widget_sharing'];
	}
	
	public function AcknowledgeWidgetSharing($id_p, $email, $token) {
		$return = false;
		$token = $this->ExtractToken ( $token );
		$id_widget_sharing = ( int ) $token [2];
		if ($token [0] == "ws" && $id_widget_sharing > 0) {
			$db = & Db::globaldb ();
			$token_email = $db->SqlQuote ( $token [1] );
			$db->begin ();
			$db->lock ( "widget_sharings" );
			$sqlstr = "
	            UPDATE widget_sharings 
	            SET status=1, id_p=$id_p, email='{$email}'
	            WHERE id_widget_sharing=$id_widget_sharing AND email='{$token_email}' AND status=0 
	        ";
			$res [] = $db->query ( $sqlstr, true );
			Db::finish ( $res, $db );
			
			if (! in_array ( false, $res ) && $db->affected_rows > 0) {
				$return = true;
			}
		}
		return $return;
	}
	
	private function ExtractToken($token) {
		$ext_token = explode ( ';', $this->Decrypt ( $token ) );
		if (count ( $ext_token ) != 3)
			$ext_token = explode ( ';', $this->Decrypt ( urldecode ( $token ) ) );
		
		return $ext_token;
	}
	
	public function ResendSharingEmail($shares, $id_topic = 0) {
		foreach ( $shares as $share ) {
			$this->SendInvitationEmail ( $share ['email'], $share ['can_edit'], 'ws;' . $share ['email'] . ';' . $share ['id_widget_sharing'], $id_topic );
		}
	}
	
	public function DuplicateEmail(&$shares) {
		$duplicate = false;
		for($i = 0; $i < count ( $shares ); $i ++) {
			for($j = 0; $j < count ( $shares ); $j ++) {
				if ($j != $i && $shares [$i] ['email'] == $shares [$j] ['email']) {
					$shares [$i] ['invalid'] = '1';
					$duplicate = true;
				}
			}
		}
		$shares = $this->xnameArray ( $shares );
		return $duplicate;
	}
	
	private function SendInvitationEmail($email, $can_edit, $token, $id_topic) {
		include_once (SERVER_ROOT . "/../classes/people.php");
		$pe = new People ();
		$user = $pe->UserInfo ( false );
		$inviter = $user ['name1'] . ' ' . $user ['name2'];
		
		include_once (SERVER_ROOT . "/../classes/translator.php");
		$trm3 = new Translator ( 0, 3, false );
		
		include_once (SERVER_ROOT . "/../classes/mail.php");
		$mail = new Mail ();
		$extra = array ();
		$subject = $trm3->TranslateParams ( "widget_share_subject", array ($mail->title ) );
		
		if ($id_topic > 0) {
			include_once (SERVER_ROOT . "/../classes/topic.php");
			$t = new Topic ( $id_topic );
			$extra ['name'] = $t->name;
			if ($t->row ['temail'] != "" && $mail->CheckEmail ( $t->row ['temail'] ))
				$extra ['email'] = $t->row ['temail'];
		}
		
		$link = $this->VerifyEmailLink ( $mail->pub_web, $token, $id_topic );
		$message = $trm3->TranslateParams ( $can_edit ? "widget_share_and_edit_message" : "widget_share_message", array ($inviter, $mail->title, $link ) );
		
		$mail->SendMail ( $email, "", $subject, $message, $extra );
	}
	
	private function VerifyEmailLink($pub_web, $token, $id_topic) {
		$url = "$pub_web/widgets/mywidgets.php?token=" . urlencode ( $this->Encrypt ( $token ) );
		if ($id_topic > 0)
			$url .= "&id_topic=$id_topic";
		return $url;
	}
	
	private function Decrypt($value) {
		include_once (SERVER_ROOT . "/../classes/crypt.php");
		$sc = new Scrypt ();
		return $sc->Decrypt ( $value );
	}
	
	private function Encrypt($value) {
		include_once (SERVER_ROOT . "/../classes/crypt.php");
		$sc = new Scrypt ();
		return $sc->Encrypt ( $value );
	}
	
	public function WidgetsTabsAll(&$rows, $id_p, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT wt.id_p, wt.id_tab, wt.tab_name,COUNT(wtp.id_widget) AS num_widgets
            FROM widgets_tabs wt
            LEFT JOIN widgets_tabs_position wtp ON wt.id_tab=wtp.id_tab AND wtp.id_p='$id_p'
            WHERE wt.id_p = '$id_p' 
            GROUP BY wt.id_tab
            ORDER BY wt.id_tab";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsTabCounter($id_p, $id_tab) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT COUNT(id_widget) AS counter
            FROM widgets_tabs_position 
            WHERE id_p = '$id_p'
            AND id_tab = '$id_tab'";
		$db->query_single ( $row, $sqlstr );
		return ( int ) $row ['counter'];
	}
	
	public function WidgetsTabDelete($id_p, $id_tab) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs" );
		$res [] = $db->query ( "DELETE FROM widgets_tabs WHERE id_p='$id_p' AND id_tab='$id_tab' " );
		Db::finish ( $res, $db );
	}
	
	public function WidgetsTabGet($id_p, $id_tab) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_p, id_tab, tab_name
            FROM widgets_tabs 
            WHERE id_p = '$id_p'
            AND id_tab = '$id_tab'";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function WidgetsTabStore($id_action, $id_p, $id_tab, $tab_name) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs" );
		if ($id_action > 0) {
			$sqlstr = "UPDATE widgets_tabs 
                SET tab_name='$tab_name'
                WHERE id_p='$id_p' AND id_tab='$id_tab' ";
		} else {
			$sqlstr = "INSERT INTO widgets_tabs 
                (id_p,id_tab,tab_name) 
                VALUES 
                ('$id_p','$id_tab','$tab_name')";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		if ($id_p == 0) {
			$this->WidgetsDefaultTabsCacheReset ();
		}
		return $id_tab;
	}
	
	public function WidgetsTabsPositionAll(&$rows, $id_p, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT wtp.id_p, wt.tab_name, wtp.id_tab, wtp.id_widget, wtp.col, wtp.pos, w.title
            FROM widgets_tabs_position wtp
            INNER JOIN widgets w
            ON wtp.id_widget = w.id_widget
            INNER JOIN widgets_tabs wt
            ON wtp.id_p = wt.id_p
            AND wtp.id_tab = wt.id_tab
            WHERE wtp.id_p = '$id_p' 
            ";
		$sqlstr .= " ORDER BY wtp.id_tab, wtp.col, wtp.pos";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsTabPositionDelete($id_p, $id_tab, $col, $pos) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		$sqlstr = "DELETE FROM widgets_tabs_position 
                    WHERE id_p='$id_p' 
                    AND id_tab='$id_tab'
                    AND col = '$col'
                    AND pos = '$pos' ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		if ($id_p == 0) {
			$this->WidgetsDefaultTabsCacheReset ();
		}
	}
	
	public function WidgetsTabPositionDeleteByTab($id_p, $id_tab) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		$sqlstr = "DELETE FROM widgets_tabs_position 
                    WHERE id_p='$id_p' 
                    AND id_tab='$id_tab'
                    ";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	public function WidgetsTabsPositionByTab(&$rows, $id_p, $id_tab, $paged = true) {
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "SELECT wtp.id_p, wtp.id_tab, wtp.id_widget, wtp.col, wtp.pos, wt.tab_name, w.title
            FROM widgets_tabs_position wtp
            INNER JOIN widgets w
            ON wtp.id_widget = w.id_widget
            INNER JOIN widgets_tabs wt
            ON wtp.id_p = wt.id_p
            AND wtp.id_tab = wt.id_tab 
            WHERE wtp.id_p = '$id_p'
            AND wtp.id_tab = '$id_tab'
            ";
		$sqlstr .= " ORDER BY wtp.col, wtp.pos";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}
	
	public function WidgetsTabPositionCountByIdWidget($id_p, $id_widget) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT COUNT(id_widget) as num_widgets
            FROM widgets_tabs_position
            WHERE id_p = '$id_p'
            AND id_widget = '$id_widget'
            ";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function WidgetsTabPositionGet($id_p, $id_tab, $col, $pos) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT wtp.id_p, wtp.id_tab, wtp.id_widget, wtp.col, wtp.pos, wt.tab_name
            FROM widgets_tabs_position wtp
            INNER JOIN widgets_tabs wt
            ON wtp.id_p = wt.id_p
            AND wtp.id_tab = wt.id_tab 
            WHERE wtp.id_p = '$id_p'
            AND wtp.id_tab = '$id_tab'
            AND wtp.col = '$col'
            AND wtp.pos = '$pos'";
		$db->query_single ( $row, $sqlstr );
		return $row;
	}
	
	public function WidgetsTabPositionStore($id_action, $id_p, $id_tab, $id_widget, $col, $pos) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		if ($id_action > 0) {
			$sqlstr = "UPDATE widgets_tabs_position 
                SET id_widget='$id_widget'
                WHERE id_p='$id_p' 
                AND id_tab='$id_tab'
                AND col='$col'
                AND pos='$pos' ";
		} else {
			$sqlstr = "INSERT INTO widgets_tabs_position 
                (id_p,id_tab,id_widget,col,pos) 
                VALUES 
                ('$id_p','$id_tab','$id_widget', '$col', '$pos')";
		}
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
		if ($id_p == 0) {
			$this->WidgetsDefaultTabsCacheReset ();
		}
		return $id_tab;
	}
	
	public function WidgetImageUpdate($id_widget, $file) {
		include_once (SERVER_ROOT . "/../classes/file.php");
		include_once (SERVER_ROOT . "/../classes/images.php");
		$fm = new FileManager ();
		$this->WidgetImageDelete ( $id_widget );
		$origfile = "uploads/widgets/orig/{$id_widget}.jpg";
		$fm->MoveUpload ( $file ['temp'], $origfile );
		$i = new Images ();
		$i->ConvertWrapper ( "widget_image", $origfile, $id_widget );
		$filename = $id_widget . "." . $i->convert_format;
		$fm->Copy ( "uploads/widgets/0/$filename", "pub/{$i->pub_path}widgets/0/$filename" );
		$fm->PostUpdate ();
	}
	
	public function WidgetImageDelete($id_widget) {
		include_once (SERVER_ROOT . "/../classes/images.php");
		$i = new Images ();
		$fm = new FileManager ();
		$fm->Delete ( "uploads/widgets/orig/{$id_widget}.jpg" );
		$fm->Delete ( "pub/{$i->pub_path}widgets/0/{$id_widget}.jpg" );
		$i->RemoveWrapper ( "widget_image", $id_widget );
	}
	
	public function CloneConfiguration($id_p_from, $id_p_to) {
		$rows = $this->WidgetsTabs ( $id_p_to );
		if (count ( $rows ) == 0) {
			// get widgets_tabs
			$rows_widgets_tabs = $this->WidgetsTabs ( $id_p_from );
			if (count ( $rows_widgets_tabs ) > 0) {
				$db = & Db::globaldb ();
				$db->begin ();
				$db->lock ( "widgets_tabs" );
				foreach ( $rows_widgets_tabs as $row_widgets_tabs ) {
					$id_tab = $row_widgets_tabs ['id'];
					$tab_name = $row_widgets_tabs ['name'];
					$sqlstr = "INSERT INTO widgets_tabs
		                        (id_p, id_tab, tab_name)
		                        VALUES 
		                        ('$id_p_to', '$id_tab', '$tab_name')";
					$res [] = $db->query ( $sqlstr );
				}
				Db::finish ( $res, $db );
				
				// get widgets_tabs_position
				$rows_widgets_tabs_position = array ();
				$db = & Db::globaldb ();
				$sqlstr = " SELECT id_tab, id_widget, col, pos
		                    FROM widgets_tabs_position
		                    WHERE id_p = '$id_p_from' 
		                    ORDER BY id_tab,id_widget,col,pos ";
				$db->QueryExe ( $rows_widgets_tabs_position, $sqlstr );
				
				if (count ( $rows_widgets_tabs_position ) > 0) {
					$inserts = array ();
					foreach ( $rows_widgets_tabs_position as $row_widgets_position ) {
						$id_tab = $row_widgets_position ['id_tab'];
						$id_widget = $row_widgets_position ['id_widget'];
						$col = $row_widgets_position ['col'];
						$pos = $row_widgets_position ['pos'];
						$inserts [] = "('$id_p_to', '$id_tab', '$id_widget', '$col', '$pos')";
					}
					$sqlstr = "INSERT INTO widgets_tabs_position
		                        (id_p, id_tab, id_widget, col, pos)
		                        VALUES " . implode ( ",", $inserts );
					$db->begin ();
					$db->lock ( "widgets_tabs_position" );
					$res [] = $db->query ( $sqlstr );
					Db::finish ( $res, $db );
				}
			}
		}
	}
	
	public function Statuses($show_deleted) {
		$statuses = array ("Pending", "Approved", "Rejected", "Deleted", "On hold" );
		if (! $show_deleted) {
			unset ( $statuses [3] );
			unset ( $statuses [4] );
		}
		return $statuses;
	}
	
	private function TabInsert($id_p, $tab_name) {
		$db = & Db::globaldb ();
		$tab_name = $db->SqlQuote ( $tab_name );
		$max_id_tab = $this->TabLastIdGet ( $id_p );
		$id_tab = $max_id_tab + 1;
		$db->begin ();
		$db->lock ( "widgets_tabs" );
		$sqlstr = "INSERT INTO widgets_tabs
                    (id_p, id_tab, tab_name)
                    VALUES 
                    ('$id_p', '$id_tab', '$tab_name')";
		$res [] = $db->query ( $sqlstr );
		
		Db::finish ( $res, $db );
		$this->session->Delete ( WIDGET_CACHE_VARNAME_TABS . $id_p );
	}
	
	private function TabUpdate($id_p, $id_tab, $tab_name) {
		$db = & Db::globaldb ();
		$tab_name = $db->SqlQuote ( $tab_name );
		$db->begin ();
		$db->LockTables ( array ("widgets_tabs" ) );
		$sqlstr = "UPDATE widgets_tabs
                    SET tab_name='$tab_name'
                    WHERE 
                    id_p = '$id_p' AND id_tab = '$id_tab'";
		$res [] = $db->query ( $sqlstr );
		
		Db::finish ( $res, $db );
		$this->session->Delete ( WIDGET_CACHE_VARNAME_TABS . $id_p );
	}
	
	private function TabDelete($id_p, $id_tab) {
		// if a tab is being deleted, the position of the subsquent tab/widgets need to be updated
		$rows = $this->TabBelowPositionGet ( $id_p, $id_tab );
		
		$db = & Db::globaldb ();
		$db->begin ();
		$db->LockTables ( array ("widgets_tabs", "widgets_tabs_position" ) );
		
		$res [] = $db->query ( "DELETE FROM widgets_tabs WHERE id_p = '$id_p' AND id_tab='$id_tab'" );
		$res [] = $db->query ( "DELETE FROM widgets_tabs_position WHERE id_p = '$id_p' AND id_tab='$id_tab'" );
		
		// loop from smallest tab id to largest
		foreach ( $rows as $row ) {
			$id_tab_old = $row ['id_tab'];
			$id_tab_updated = $id_tab_old - 1;
			// update on the widget tabs
			$sqlstr = "UPDATE widgets_tabs
                    SET id_tab = '$id_tab_updated'
                    WHERE id_p = '$id_p' 
                    AND id_tab = '$id_tab_old'";
			$res [] = $db->query ( $sqlstr );
			
			// update on the widget tabs position    
			$sqlstr = "UPDATE widgets_tabs_position
                    SET id_tab = '$id_tab_updated'
                    WHERE id_p = '$id_p'
                    AND id_tab = '$id_tab_old'";
			$res [] = $db->query ( $sqlstr );
		}
		
		Db::finish ( $res, $db );
		$this->session->Delete ( WIDGET_CACHE_VARNAME_TABS . $id_p );
	}
	
	private function TabLastIdGet($id_p) {
		$row = array ();
		$db = & Db::globaldb ();
		$sqlstr = " SELECT MAX(id_tab) as max_id_tab
                    FROM widgets_tabs
                    WHERE id_p = '$id_p'";
		$db->query_single ( $row, $sqlstr );
		return ( int ) $row ['max_id_tab'];
	}
	
	private function TabBelowPositionGet($id_p, $id_tab) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = " SELECT id_tab
                    FROM widgets_tabs
                    WHERE id_p = '$id_p'
                    AND id_tab > '$id_tab'
                    ORDER BY id_tab ";
		$db->QueryExe ( $rows, $sqlstr );
		return $rows;
	}
	
	public function TabsReset($id_p) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->LockTables ( array ("widgets_tabs", "widgets_tabs_position" ) );
		$res [] = $db->query ( "DELETE FROM widgets_tabs WHERE id_p = '$id_p' " );
		$res [] = $db->query ( "DELETE FROM widgets_tabs_position WHERE id_p = '$id_p' " );
		Db::finish ( $res, $db );
		$this->CloneConfiguration ( 0, $id_p );
		$this->session->Delete ( WIDGET_CACHE_VARNAME_TABS . $id_p );
	}
	
	/*
    Get the order of tab per user
    */
	private function WidgetsTabs($id_p) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT id_tab as `id`, tab_name as `name`, '' as `tab_class`
                    FROM widgets_tabs
                    WHERE id_p = '$id_p'
                    ORDER BY id_tab
                    ";
		$db->QueryExe ( $rows, $sqlstr );
		return $rows;
	}
	
	private function WidgetGetById($id_widget) {
		$row = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "SELECT w.id_widget as 'id', w.title, w.title as 'short_title', w.description, w.status, w.widget_type as 'widgetType', wp.params as 'content' 
            FROM widgets w INNER JOIN widget_params wp ON w.id_widget = wp.id_widget 
            WHERE w.id_widget = $id_widget";
		$db->query_single ( $row, $sqlstr, 1 );
		
		$row ['short_title'] = TextHelper::StringCut ( $row ['short_title'], 20, "...", TextHelper::IsUnicode ( $row ['short_title'] ) );
		
		include_once (SERVER_ROOT . "/../classes/varia.php");
		include_once (SERVER_ROOT . "/../classes/images.php");
		include_once (SERVER_ROOT . "/../classes/file.php");
		$v = new Varia ();
		$i = new Images ();
		$fm = new FileManager ();
		
		$params = $v->Deserialize ( $row ['content'] );
		switch ($row ['widgetType']) {
			case WIDGET_TYPE_RSS :
				$row ['widgetType'] = 'rss';
				$row ['contents'] = $this->WidgetsRssContent ( array ($params ['id_web_feed'] ), $params ['items_display'] );
				switch ($params ['layout_type']) {
					case RSS_LAYOUT_LIST_WITH_IMAGE :
						$row ['layoutType'] = 'list_with_image';
						break;
					case RSS_LAYOUT_LIST_ITEM :
						$row ['layoutType'] = 'list_item';
						break;
					case RSS_LAYOUT_LIST_IMAGES :
						$row ['layoutType'] = 'list_images';
						$row ['contents'] ['alt'] = $row ['title'];
						break;
					case RSS_LAYOUT_LIST_WITH_THUMBNAIL :
						$row ['layoutType'] = 'list_with_thumbnail';
						break;
				}
				break;
			case WIDGET_TYPE_MANUAL :
				$row ['widgetType'] = 'manual';
				$row ['content'] = $params ['content'];
				$row ['link'] = $params ['link_url'];
				$filename = "widgets/0/" . $row ['id'] . "." . $i->convert_format;
				$ini = new Ini ();
				$pub_web = $ini->Get ( 'pub_web' );
				
				if ($fm->Exists ( "uploads/$filename" )) {
					$row ['image'] = "$pub_web/images/$filename";
				} else
					$row ['image'] = "";
				break;
			case WIDGET_TYPE_CUSTOM :
				$row ['widgetType'] = 'custom';
				$row ['content'] = $params ['content'];
				$row ['is_html'] = $params ['is_html'];
				break;
			case WIDGET_TYPE_MANUAL_ADVANCE :
				$row ['widgetType'] = 'advance';
				$row ['contents'] = $this->PublicWidgetChildGet ( $id_widget );
				switch ($params ['layout_type']) {
					case RSS_LAYOUT_LIST_WITH_IMAGE :
						$row ['layoutType'] = 'list_with_image';
						break;
					case RSS_LAYOUT_LIST_ITEM :
						$row ['layoutType'] = 'list_item';
						break;
					case RSS_LAYOUT_LIST_IMAGES :
						$row ['layoutType'] = 'list_images';
						break;
					case RSS_LAYOUT_LIST_WITH_THUMBNAIL :
						$row ['layoutType'] = 'list_with_thumbnail';
						break;
				}
				foreach ( $row ['contents'] as $key => $child ) {
					$filename = "widgets/0/" . $child ['id_widget'] . "." . $i->convert_format;
					$ini = new Ini ();
					$pub_web = $ini->Get ( 'pub_web' );
					
					if ($fm->Exists ( "uploads/$filename" )) {
						$row ['contents'] [$key] ['image'] = "$pub_web/images/$filename";
					} else
						$row ['contents'] [$key] ['image'] = "";
				}
				break;
			case WIDGET_TYPE_FEATURE :
				$row ['widgetType'] = 'feature';
				$id_feature = ( int ) $params ['id_feature'];
				$html = "";
				if ($row ['id'] > 0 && $id_feature > 0) {
					include_once (SERVER_ROOT . "/../classes/cache.php");
					$ca = new Cache ();
					$ca->ttl = $params ['update_interval'];
					$html = $ca->Get ( "widget_feature", $id_feature );
					$row ['is_html'] = 1;
				}
				$row ['content'] = $html;
				break;
		}
		return $row;
	}
	
	private function WidgetsDefaultTabsCacheReset() {
		$this->mem->ResetAllPrefix ( WIDGET_CACHE_VARNAME_WIDGETS_DEFAULT_TAB );
	}
	
	public function WidgetsGetByTab($id_p, $id_tab) {
		if (! $id_p > 0 && $this->mem->IsVarSet ( WIDGET_CACHE_VARNAME_WIDGETS_DEFAULT_TAB . $id_tab )) {
			$rows = $this->mem->Get ( WIDGET_CACHE_VARNAME_WIDGETS_DEFAULT_TAB . $id_tab );
		} else {
			$rows = array ();
			$db = & Db::globaldb ();
			$sqlstr = "SELECT wt.id_tab, wt.tab_name,
	                    wtp.col as 'column', wtp.pos,
	                    w.id_widget as 'id', w.title, w.title as 'short_title', w.description, w.status, w.shareable, 
	                    w.widget_type as 'widgetType', wp.params as 'content',
	                    CASE WHEN w.id_p = '$id_p' and w.id_p<>0 then 'self'
	                    	ELSE 'others' END as 'own'
	                    FROM widgets_tabs wt
	                    INNER JOIN widgets_tabs_position wtp ON wt.id_tab = wtp.id_tab AND wtp.id_p = wt.id_p
	                    INNER JOIN widgets w ON wtp.id_widget = w.id_widget
	                    INNER JOIN widget_params wp ON w.id_widget = wp.id_widget
	                    WHERE wt.id_p = '$id_p'
	                    AND wt.id_tab = '$id_tab' AND w.status <> " . WIDGET_STATUS_DELETED . " 
	                    ORDER BY wtp.col, wtp.pos
	                    ";
			$db->QueryExe ( $rows, $sqlstr, false, 1 );
			
			include_once (SERVER_ROOT . "/../classes/varia.php");
			include_once (SERVER_ROOT . "/../classes/images.php");
			include_once (SERVER_ROOT . "/../classes/file.php");
			$v = new Varia ();
			$i = new Images ();
			$fm = new FileManager ();
			
			$count = 0;
			foreach ( $rows as $row ) {
				$params = $v->Deserialize ( $row ['content'] );
				$rows [$count] ['short_title'] = TextHelper::StringCut ( $rows [$count] ['short_title'], 26, "...", TextHelper::IsUnicode ( $rows [$count] ['short_title'] ) );
				
				switch ($row ['widgetType']) {
					case WIDGET_TYPE_RSS :
						$rows [$count] ['widgetType'] = 'rss';
						$rows [$count] ['contents'] = $this->WidgetsRssContent ( array ($params ['id_web_feed'] ), $params ['items_display'] );
						switch ($params ['layout_type']) {
							case RSS_LAYOUT_LIST_WITH_IMAGE :
								$rows [$count] ['layoutType'] = 'list_with_image';
								break;
							case RSS_LAYOUT_LIST_ITEM :
								$rows [$count] ['layoutType'] = 'list_item';
								break;
							case RSS_LAYOUT_LIST_IMAGES :
								$rows [$count] ['layoutType'] = 'list_images';
								$rows [$count] ['contents'] ['alt'] = $rows [$i] ['contents'] ['title'];
								break;
							case RSS_LAYOUT_LIST_WITH_THUMBNAIL :
								$rows [$count] ['layoutType'] = 'list_with_thumbnail';
								break;
						}
						break;
					case WIDGET_TYPE_MANUAL :
						$rows [$count] ['widgetType'] = 'manual';
						$rows [$count] ['content'] = $params ['content'];
						$rows [$count] ['link'] = $params ['link_url'];
						$filename = "widgets/0/" . $rows [$count] ['id'] . "." . $i->convert_format;
						$ini = new Ini ();
						$pub_web = $ini->Get ( 'pub_web' );
						
						if ($fm->Exists ( "uploads/$filename" )) {
							$rows [$count] ['image'] = "$pub_web/images/$filename";
						} else
							$rows [$count] ['image'] = "";
						
						break;
					case WIDGET_TYPE_CUSTOM :
						$rows [$count] ['widgetType'] = 'custom';
						$rows [$count] ['content'] = $params ['content'];
						$rows [$count] ['is_html'] = $params ['is_html'];
						break;
					case WIDGET_TYPE_MANUAL_ADVANCE :
						$rows [$count] ['widgetType'] = 'advance';
						$rows [$count] ['contents'] = $this->PublicWidgetChildGet ( $row ['id'] );
						switch ($params ['layout_type']) {
							case RSS_LAYOUT_LIST_WITH_IMAGE :
								$rows [$count] ['layoutType'] = 'list_with_image';
								break;
							case RSS_LAYOUT_LIST_ITEM :
								$rows [$count] ['layoutType'] = 'list_item';
								break;
							case RSS_LAYOUT_LIST_IMAGES :
								$rows [$count] ['layoutType'] = 'list_images';
								break;
							case RSS_LAYOUT_LIST_WITH_THUMBNAIL :
								$rows [$count] ['layoutType'] = 'list_with_thumbnail';
								break;
						}
						foreach ( $rows [$count] ['contents'] as $key => $child ) {
							$filename = "widgets/0/" . $child ['id_widget'] . "." . $i->convert_format;
							$ini = new Ini ();
							$pub_web = $ini->Get ( 'pub_web' );
							
							if ($fm->Exists ( "uploads/$filename" )) {
								$rows [$count] ['contents'] [$key] ['image'] = "$pub_web/images/$filename";
							} else
								$rows [$count] ['contents'] [$key] ['image'] = "";
						}
						break;
					case WIDGET_TYPE_FEATURE :
						$rows [$count] ['widgetType'] = 'feature';
						$id_feature = ( int ) $params ['id_feature'];
						$html = "";
						if ($row ['id'] > 0 && $id_feature > 0) {
							include_once (SERVER_ROOT . "/../classes/cache.php");
							$ca = new Cache ();
							$ca->ttl = $params ['update_interval'];
							$html = $ca->Get ( "widget_feature", $id_feature );
							$rows [$count] ['is_html'] = 1;
						}
						$rows [$count] ['content'] = $html;
						break;
				}
				$count ++;
			}
			if (! $id_p > 0) {
				$this->mem->Set ( WIDGET_CACHE_VARNAME_WIDGETS_DEFAULT_TAB . $id_tab, $rows, $this->widgets_cache_default_ttl );
			}
		}
		return $rows;
	}
	
	private function WidgetsRssContent($id_widgets, $items_display) {
		$id_widget_list = '';
		foreach ( $id_widgets as $id_widget ) {
			if ($id_widget_list == '')
				$id_widget_list = $id_widget;
			else
				$id_widget_list = $id_widget_list . ',' . $id_widget;
		}
		// make sure query will not break when there is rss widget that half done on setup
		if ($id_widget_list == '')
			$id_widget_list = '0';
		if ($items_display == '')
			$items_display = 5;
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = "SELECT wfi.id_web_feed, wfi.title, wfi.link_url as 'link', wfi.image_url as 'image', wfic.description, wfic.content
                    FROM web_feed_items wfi
                    LEFT JOIN web_feed_items_content wfic
                    ON wfi.id_web_feed_item = wfic.id_web_feed_item
                    WHERE wfi.id_web_feed in ($id_widget_list)
                    AND wfi.is_approved = 1
                    AND wfi.is_filtered = 0
                    AND wfi.is_expired = 0
                    ORDER BY wfi.id_web_feed, IF(wfi.published_date = '0000-00-00 00:00:00', wfi.last_updated, wfi.published_date) desc, wfi.id_web_feed_item LIMIT {$items_display}
                ";
		$db->QueryExe ( $rows, $sqlstr );
		
		foreach ( $rows as &$row ) {
			$row ['title'] = TextHelper::StringCut ( $row ['title'], 100, "...", TextHelper::IsUnicode ( $row ['title'] ) );
			$row ['description'] = TextHelper::StringCut ( $row ['description'], 200, "...", TextHelper::IsUnicode ( $row ['description'] ) );
			$row ['content'] = TextHelper::StringCut ( $row ['content'], 200, "...", TextHelper::IsUnicode ( $row ['content'] ) );
		}
		
		return $rows;
	}
	
	private function WidgetsTabPositionRefresh($id_p, $id_tab, $widgets, $cols, $positions) {
		// this function handle add widget, move widget, delete widget within a tab
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		$sqlstr = "DELETE FROM widgets_tabs_position 
                    WHERE id_p = '$id_p' 
                    AND id_tab = '$id_tab'";
		$res [] = $db->query ( $sqlstr );
		
		for($i = 0; $i < count ( $widgets ); $i ++) {
			$id_widget = $widgets [$i];
			$col = $cols [$i];
			$pos = $positions [$i];
			$sqlstr = "REPLACE INTO widgets_tabs_position 
                        (id_p, id_tab, id_widget, col, pos) 
                        VALUES 
                        ( '$id_p', '$id_tab', '$id_widget', '$col', '$pos')";
			$res [] = $db->query ( $sqlstr );
		}
		Db::finish ( $res, $db );
	}
	
	private function WidgetMove($id_p, $id_tab, $id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		
		$sqlstr = "UPDATE widgets_tabs_position
                    SET pos = pos + 1
                    WHERE id_p = '$id_p'
                    AND id_tab = '$id_tab'
                    AND col = 1";
		$res [] = $db->query ( $sqlstr );
		
		$sqlstr = "INSERT INTO widgets_tabs_position
                    (id_p, id_tab, id_widget, col, pos)
                    VALUES
                    ('$id_p', '$id_tab', '$id_widget', 1, 1)";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function WidgetBelowPostionGet($id_p, $id_tab) {
		$rows = array ();
		$db = & Db::globaldb ();
		$sqlstr = " SELECT id_widget, pos
                    FROM widgets_tabs_position
                    WHERE id_p = '$id_p'
                    AND id_tab = '$id_tab'
                    AND col = 1
                    ORDER BY pos DESC";
		$db->QueryExe ( $rows, $sqlstr );
		return $rows;
	}
	
	private function WidgetAddToTab($id_p, $id_tab, $id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		
		$res [] = $this->WidgetMoveDown ( $db, $id_p, $id_tab, 1, 1 );
		
		$sqlstr = "INSERT INTO widgets_tabs_position
                    (id_p, id_tab, id_widget, col, pos)
                    VALUES
                    ('$id_p', '$id_tab', '$id_widget', 1, 1)";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function WidgetMoveAroundTab($id_p, $id_tab, $id_widget, $col, $pos) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		
		$res [] = $this->WidgetMoveUp ( $db, $id_p, $id_widget );
		$res [] = $this->WidgetMoveDown ( $db, $id_p, $id_tab, $col, $pos );
		
		$sqlstr = "UPDATE widgets_tabs_position
                    SET id_tab = '$id_tab', col = '$col', pos = '$pos'
                    WHERE id_p = '$id_p'
                    AND id_widget = '$id_widget'";
		$res [] = $db->query ( $sqlstr );
		
		Db::finish ( $res, $db );
	}
	
	private function WidgetMoveUp($db, $id_p, $id_widget) {
		$row = array ();
		
		$sqlstr = "SELECT * 
            FROM widgets_tabs_position 
            WHERE id_p = '$id_p' AND id_widget = '$id_widget'";
		$db->query_single ( $row, $sqlstr, 1 );
		
		$sqlstr = "UPDATE widgets_tabs_position
                    SET pos = pos - 1
                    WHERE id_p = '$id_p'
                    AND id_tab = '{$row['id_tab']}'
                    AND pos > '{$row['pos']}'
                    AND col = '{$row['col']}'";
		return $db->query ( $sqlstr );
	}
	
	private function WidgetMoveDown($db, $id_p, $id_tab, $col, $pos) {
		$sqlstr = "UPDATE widgets_tabs_position
                    SET pos = pos + 1
                    WHERE id_p = '$id_p'
                    AND id_tab = '$id_tab'
                    AND pos >= '$pos'
                    AND col = '$col'";
		return $db->query ( $sqlstr );
	}
	
	private function WidgetRemoveFromTab($id_p, $id_widget) {
		$db = & Db::globaldb ();
		$db->begin ();
		$db->lock ( "widgets_tabs_position" );
		
		$res [] = $this->WidgetMoveUp ( $db, $id_p, $id_widget );
		$sqlstr = "DELETE FROM widgets_tabs_position 
                    WHERE id_p = '$id_p' 
                    AND id_widget = '$id_widget'";
		$res [] = $db->query ( $sqlstr );
		Db::finish ( $res, $db );
	}
	
	private function JsonDecode($data, $assoc = false) {
		return json_decode ( $data, $assoc );
	}
	
	private function JsonEncode($array) {
		return json_encode ( $array );
	}
	
	public function OutputJson($params) {
		$id_p = $this->WidgetUserGet ();
		$output_array = array ();
		$type = $params ['type'];
		switch ($type) {
			case 'tabs' :
				if ($id_p > 0) {
					if ($this->session->IsVarSet ( WIDGET_CACHE_VARNAME_TABS . $id_p )) {
						$rows = $this->session->Get ( WIDGET_CACHE_VARNAME_TABS . $id_p );
					} else {
						$rows = $this->WidgetsTabs ( $id_p );
						$this->session->Set ( WIDGET_CACHE_VARNAME_TABS . $id_p, $rows );
					}
					if ($this->widgets_init_empty_profiles && count ( $rows ) == 0) {
						$this->CloneConfiguration ( 0, $id_p );
						$rows = $this->WidgetsTabs ( $id_p );
						$this->session->Set ( WIDGET_CACHE_VARNAME_TABS . $id_p, $rows );
					}
				} else {
					if ($this->mem->IsVarSet ( WIDGET_CACHE_VARNAME_TABS_DEFAULT )) {
						$rows = $this->mem->Get ( WIDGET_CACHE_VARNAME_TABS_DEFAULT );
					} else {
						$rows = $this->WidgetsTabs ( 0 );
						$this->mem->Set ( WIDGET_CACHE_VARNAME_TABS_DEFAULT, $rows, $this->widgets_cache_default_ttl );
					}
				}
				for($i = 0; $i < count ( $rows ); $i ++) {
					$rows [$i] ['id'] = 'tab-' . $rows [$i] ['id'];
				}
				if ($id_p != 0 && !$this->dr_site) {
					$rows [] = array ('id' => 'add-tab', 'name' => 'Add tab', 'tab_class' => '' );
				}
				$rows [] = array ('id' => 'add-widget', 'name' => 'Add Widget', 'tab_class' => '' );
				$output_array = array ('username' => ( string ) $id_p, 'tabs' => $rows );
				$output_array['dr_site'] = $this->dr_site? 'true':'false';
				if ($this->widgets_group > 0 && $id_p == 0) {
					$output_array ['restricted'] = 1;
				}
				break;
			case 'widgets' :
				$id_tab = $this->IdTabExtract ( $params ['tabId'] );
				
				$rows = $this->WidgetsGetByTab ( $id_p, $id_tab );
				
				for($i = 0; $i < count ( $rows ); $i ++) {
					if ($i == 0) {
						$tab_name = $rows [0] ['tab_name'];
						$id_tab = 'tab-' . $rows [0] ['id_tab'];
					}
					$rows [$i] ['id'] = 'widget-' . $rows [$i] ['id'];
					if (array_key_exists ( 'content', $rows [$i] ))
						$rows [$i] ['content'] = stripcslashes ( ($rows [$i] ['is_html'] == "1") ? $rows [$i] ['content'] : nl2br ( $rows [$i] ['content'] ) );
					if (array_key_exists ( 'description', $rows [$i] ))
						$rows [$i] ['description'] = stripcslashes ( $rows [$i] ['description'] );
					if (array_key_exists ( 'title', $rows [$i] ))
						$rows [$i] ['title'] = stripcslashes ( $rows [$i] ['title'] );
					if (array_key_exists ( 'contents', $rows [$i] )) {
						foreach ( $rows [$i] ['contents'] as $key => $row ) {
							if (array_key_exists ( 'title', $row ))
								$rows [$i] ['contents'] [$key] ['title'] = stripcslashes ( $row ['title'] );
							if (array_key_exists ( 'description', $row ))
								$rows [$i] ['contents'] [$key] ['description'] = stripcslashes ( $row ['description'] );
							if (array_key_exists ( 'content', $row ))
								$rows [$i] ['contents'] [$key] ['content'] = stripcslashes ( nl2br ( $row ['content'] ) );
						}
					}
				}
				
				$output_array = array ('name' => $tab_name, 'href' => $id_tab, 'id' => $id_tab, 'widgets' => $rows );
				break;
		}
		$json = $this->JsonEncode ( $output_array );
		return $json;
	}
	
	public function PreviewJson($data) {
		$json_data = $data ['data'];
		$array = $this->JsonDecode ( $json_data, true );
		$id_widget = $this->IdWidgetExtract ( $array ['widgetid'], 0 );
		if ($id_widget > 0) {
			$row = $this->WidgetGetById ( $id_widget );
			$row ['id'] = 'widget-' . $row ['id'];
			if (array_key_exists ( 'content', $row ))
				$row ['content'] = stripcslashes ( array_key_exists ( 'is_html', $row ) ? $row ['content'] : nl2br ( $row ['content'] ) );
			if (array_key_exists ( 'description', $row ))
				$row ['description'] = stripcslashes ( $row ['description'] );
			if (array_key_exists ( 'title', $row ))
				$row ['title'] = stripcslashes ( $row ['title'] );
			if (array_key_exists ( 'contents', $row )) {
				foreach ( $row ['contents'] as $key => $value ) {
					if (array_key_exists ( 'title', $value ))
						$row ['contents'] [$key] ['title'] = stripcslashes ( $value ['title'] );
					if (array_key_exists ( 'description', $value ))
						$row ['contents'] [$key] ['description'] = stripcslashes ( $value ['description'] );
					if (array_key_exists ( 'content', $value ))
						$row ['contents'] [$key] ['content'] = stripcslashes ( nl2br ( $value ['content'] ) );
				}
			}
		} else
			$row = array ();
		return $this->JsonEncode ( $row );
	}
	
	public function StoreJson($action, $data) {
		$id_p = $this->WidgetUserGet ();
		if ($id_p > 0) {
			switch ($action) {
				case "save" :
					$id_tab = $this->IdTabExtract ( $data ['tabId'] );
					$json_data = $data ['data'];
					$array = $this->JsonDecode ( $json_data, true );
					
					$count = 0;
					$widgets = array ();
					$cols = array ();
					$positions = array ();
					foreach ( $array as $key => $values ) {
						$len_col = strlen ( $key ) - 4;
						$col = substr ( $key, 3, $len_col );
						
						$pos = 1;
						foreach ( $values as $value ) {
							if ($value ['id'] != '') {
								$id_widget = $this->IdExtract ( $value ['id'], 7 );
								$widgets [$count] = $id_widget;
								$cols [$count] = ( int ) $col;
								$positions [$count] = ( int ) $pos;
								
								$count ++;
							}
							$pos ++;
						}
					}
					$this->WidgetsTabPositionRefresh ( $id_p, $id_tab, $widgets, $cols, $positions );
					break;
				case "updatetabwidgetplacement" :
					$id_tab = $this->IdTabExtract ( $data ['targetTabId'] );
					$id_widget = $this->IdWidgetExtract ( $data ['widgetId'] );
					if ($id_tab > 0 && $id_widget > 0) {
						$widget = $this->WidgetGet ( $id_widget );
						if ($widget ['id_p'] == 0 || $id_p == $widget ['id_p']) {
							// either admin widget OR own created widget
							if ($widget ['status'] != WIDGET_STATUS_APPROVED) {
								$error = "Widget is not approved";
							} else if ($this->IsWidgetAvailable ( $id_p, $id_widget )) {
								$error = "Widget is already added to your homepage";
							}
						} else if (! $this->IsWidgetSharedToUser ( $id_widget, $id_p )) {
							$error = "Cannot add widget not shared to you";
						}
						
						if (! isset ( $error )) {
							$this->WidgetMove ( $id_p, $id_tab, $id_widget );
						}
					}
					break;
				case "addwidget" :
					$id_tab = $this->IdTabExtract ( $data ['targetTabId'] );
					$id_widget = $this->IdWidgetExtract ( $data ['widgetId'] );
					if ($id_tab > 0 && $id_widget > 0) {
						$widget = $this->WidgetGet ( $id_widget );
						if ($widget ['id_p'] == 0 || $id_p == $widget ['id_p']) {
							// either admin widget OR own created widget
							if ($widget ['status'] != WIDGET_STATUS_APPROVED) {
								$error = "Widget is not approved";
							}
						} else if (! $this->IsWidgetSharedToUser ( $id_widget, $id_p )) {
							$error = "Cannot add widget not shared to you";
						}
						if ($this->IsWidgetAvailable ( $id_p, $id_widget )) {
							$error = "Widget is already added to your homepage";
						}
						
						if (! isset ( $error )) {
							$this->WidgetAddToTab ( $id_p, $id_tab, $id_widget );
						}
					}
					break;
				case "movewidget" :
					$id_tab = $this->IdTabExtract ( $data ['targetTabId'] );
					$id_widget = $this->IdWidgetExtract ( $data ['widgetId'] );
					if ($id_tab > 0 && $id_widget > 0)
						$this->WidgetMoveAroundTab ( $id_p, $id_tab, $id_widget, $data ['col'], $data ['pos'] );
					break;
				case "removewidget" :
					$id_widget = $this->IdWidgetExtract ( $data ['widgetId'] );
					if ($id_widget > 0)
						$this->WidgetRemoveFromTab ( $id_p, $id_widget );
					break;
				case "addtab" :
					$tab_name = $data ['tabName'];
					$this->TabInsert ( $id_p, $tab_name );
					break;
				case "renameTab" :
					$id_tab = $this->IdTabExtract ( $data ['tabId'] );
					$tab_name = $data ['newName'];
					$this->TabUpdate ( $id_p, $id_tab, $tab_name );
					break;
				case "closeTab" :
					$id_tab = $this->IdTabExtract ( $data ['tabId'] );
					$this->TabDelete ( $id_p, $id_tab );
					break;
				case "sharewidgets" :
					$json_data = $data ['data'];
					$array = $this->JsonDecode ( $json_data, true );
					$id_widget = $this->IdWidgetExtract ( $array ['widgetId'] );
					$emails = $array ['Emails'];
					if ($id_widget > 0) {
						foreach ( $emails as $email ) {
							$id_widget_sharing = $this->WidgetSharingStore ( $id_widget, $email ['email'], $email ['can_edit'] );
							$this->SendInvitationEmail ( $email ['email'], $email ['can_edit'], 'ws;' . $email ['email'] . ';' . $id_widget_sharing, 0 );
						}
					}
					break;
			}
		}
		
		//Authentication is not required for abuse report
		if ($action == "reportabuse" && $this->widgets_abuse_reporting) {
			$id_widget = $this->IdWidgetExtract ( $data ['widgetId'] );
			$email = $data ['report_email'];
			$comments = $data ['report_comments'];
			if ($id_widget > 0) {
				$this->WidgetAbuseStore ( 0, $id_widget, $id_p, $comments, 0, $email, true );
			}
		}
		
		$result = array ("status" => isset ( $error ) ? "FAIL" : "SUCCESS", "message" => $error );
		return $this->JsonEncode ( $result );
	}
	
	public function WidgetUserGet() {
		include_once (SERVER_ROOT . "/../classes/people.php");
		$pe = new People ();
		$user = $pe->UserInfo ( false );
		$id_p = $user ['id'];
		
		if (($this->widgets_require_authentication && ! $user ['auth']) || ($pe->verification_required && $user ['verified'] != People::PeopleEmailVerificationCompleted)) {
			$id_p = 0;
		}
		if ($id_p > 0 && $this->widgets_group > 0) {
			if (! isset ( $user ['g' . $this->widgets_group] ))
				$id_p = 0;
		}
		return $id_p;
	}
	
	public function WidgetsSearch(&$rows, $params, $paged = true) {
		$statuses = $this->Statuses ( true );
		$db = & Db::globaldb ();
		$rows = array ();
		$sqlstr = "
            SELECT w.id_widget, w.identifier, w.title, w.description, w.widget_type, w.status, w.shareable, w.id_p, UNIX_TIMESTAMP(w.insert_date) AS insert_date_ts
            FROM widgets w ";
		if ($params ['id_category'] > 0) {
			$sqlstr .= " INNER JOIN widgets_category_widgets wcw ON w.id_widget=wcw.id_widget AND wcw.id_category='{$params['id_category']}' ";
		}
		$sqlstr .= " WHERE w.id_widget_parent = 0 ";
		if (strlen ( $params ['name'] ) > 0)
			$sqlstr .= " AND (w.title LIKE '%{$params['name']}%' OR w.description LIKE '%{$params['name']}%' OR w.comment LIKE '%{$params['name']}%') ";
		if ($params ['type'] != count ( $this->types ))
			$sqlstr .= "AND widget_type = '{$params['type']}' ";
		if ($params ['status'] < count ( $statuses ))
			$sqlstr .= "AND status = '{$params['status']}' ";
		$sqlstr .= " ORDER BY insert_date DESC ";
		return $db->QueryExe ( $rows, $sqlstr, $paged );
	}

}

abstract class WidgetsDashBoard {
	
	public static function TotalWidgetsByTypeGet() {
		$rows = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "
            SELECT widget_type, COUNT(id_widget) as num_widgets
            FROM widgets
            WHERE id_widget_parent = 0
            GROUP BY widget_type";
		$db->QueryExe ( $rows, $sqlstr, false );
		$wi = new Widgets ();
		$widgets = array_combine ( array_keys ( $wi->types ), array_fill ( 0, count ( $wi->types ), 0 ) );
		
		foreach ( $rows as $row ) {
			$widgets [$row ['widget_type']] = ( int ) $row ['num_widgets'];
		}
		return $widgets;
	}
	
	public static function TotalWidgetsByStatusGet() {
		$rows = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "
            SELECT COUNT(id_widget) as num_widgets, status
            FROM widgets 
            WHERE id_widget_parent = 0
            GROUP BY status";
		$db->QueryExe ( $rows, $sqlstr, false );
		$wi = new Widgets ();
		$statuses = $wi->Statuses ( true );
		$widgets = array_combine ( array_keys ( $statuses ), array_fill ( 0, count ( $statuses ), 0 ) );
		foreach ( $rows as $row ) {
			$widgets [$row ['status']] = ( int ) $row ['num_widgets'];
		}
		//unset($widgets[3]);
		return $widgets;
	}
	
	public static function TotalSharedWidgetsGet() {
		$rows = array ();
		$db = & Db::globaldb ();
		
		$sqlstr = "
            SELECT COUNT(id_widget) as num_widgets, shareable
            FROM widgets
            WHERE id_widget_parent = 0
            GROUP BY shareable";
		$db->QueryExe ( $rows, $sqlstr, false );
		
		$widget_share = array ('notshare' => 0, 'share' => 0 );
		foreach ( $rows as $row ) {
			if ($row ['shareable'] == 0) {
				$widget_share ['notshare'] = $row ['num_widgets'];
			} else if ($row ['shareable'] == 1) {
				$widget_share ['share'] = $row ['num_widgets'];
			}
		}
		
		return $widget_share;
	}
	
	public static function TotalWidgetByWebFeedStatusGet() {
		$rows = array ();
		$db = & Db::globaldb ();
		
		// rss
		$sqlstr = "
			SELECT w.id_widget, ifnull(wfs.status,0) as status, max(wfs.pull_date ) as max_pull_date 
            FROM web_feeds wf
            INNER JOIN (widget_web_feeds wwf
                        INNER JOIN widgets w
                        ON wwf.id_widget = w.id_widget)
            ON wf.id_web_feed = wwf.id_web_feed
            LEFT JOIN web_feeds_status_log wfs 
            ON wf.id_web_feed = wfs.id_web_feed
            GROUP BY w.id_widget
            ";
		$db->QueryExe ( $rows, $sqlstr, false );
		
		$success = 0;
		$failed = 0;
		foreach ( $rows as $row ) {
			if ($row ['status'] == 0)
				$failed ++;
			else
				$success ++;
		}
		
		$widget_status = array ('success' => $success, 'failed' => $failed );
		return $widget_status;
	}

}

?>
