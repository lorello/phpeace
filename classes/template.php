<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/resources.php");

class Template
{
	private $id_res;
	private $params_types;
	
	function __construct($resource="")
	{
		$this->id_res = 0;
		$r = new Resources();
		$res_types = $r->types;
		if($resource!="" && isset($res_types[$resource]))
			$this->ResourceSet($res_types[$resource]);
		$this->params_types =  $r->ParamsTypes(true);
	}

	public function HideFields($id_template,$fields)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "templates" );
		$res[] = $db->query( "UPDATE templates SET hide_fields='$fields' WHERE id_template='$id_template' ");
		Db::finish( $res, $db);
	}

	public function HideableFields()
	{
		$fields = array();
		switch($this->id_res)
		{
			case "5":
				$fields = array("author","source","halftitle","subhead","content","notes","boxes","images","docs","related","licence");
			break;
		}
		return $fields;
	}
	
	public function Params($id_template)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT tp.id_template_param,tp.id_template,tp.label,tp.type,tp.type_params,tp.default_value,tp.public,tp.index_include,tp.seq
			FROM template_params tp 
			WHERE tp.id_template=$id_template 
			ORDER BY tp.seq ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ParamGet($id_template_param)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_template_param,id_template,label,type,type_params,default_value,public,index_include,seq 
			FROM template_params WHERE id_template_param='$id_template_param'";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function ParamDelete($id_template_param,$id_template)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "template_params" );
		$res[] = $db->query( "DELETE FROM template_params WHERE id_template_param='$id_template_param' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "template_values" );
		$res[] = $db->query( "DELETE FROM template_values WHERE id_template_param='$id_template_param' " );
		Db::finish( $res, $db);
		$this->ParamReshuffle($id_template);
	}
	
	public function ParamMove($id_template,$id_template_param,$dir)
	{
		$params = $this->Params($id_template);
		$id_param_prev = 0;
		if (!($dir==1 && $id_template_param==$params[0]['id_template_param']) && !($dir==0 && $id_template_param==$params[count($params)-1]['id_template_param']))
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "template_params" );
			foreach ($params as $param)
			{
				if ($param['id_template_param']==$id_template_param && $dir==1)
				{
					$res[] = $db->query( "UPDATE template_params SET seq=" . ($param['seq']-1) . " WHERE id_template_param='$id_template_param' " );
					$res[] = $db->query( "UPDATE template_params SET seq=" . ($param['seq']) . " WHERE id_template_param='{$id_param_prev}' " );
				}
				if ($id_param_prev==$id_template_param && $dir==0)
				{
					$res[] = $db->query( "UPDATE template_params SET seq=" . ($param['seq']-1) . " WHERE id_template_param='{$param['id_template_param']}' " );
					$res[] = $db->query( "UPDATE template_params SET seq=" . ($param['seq']) . " WHERE id_template_param='{$id_param_prev}' " );
				}
				$id_param_prev = $param['id_template_param'];
			}
			Db::finish( $res, $db);
		}
	}
	
	private function ParamNextSeq($id_template)
	{
		$params = $this->Params($id_template);
		return (count($params)+1);
	}

	private function ParamReshuffle($id_template)
	{
		$params = $this->Params($id_template);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "template_params" );
		$counter = 1;
		foreach($params as $param)
		{
			$res[] = $db->query( "UPDATE template_params SET seq=$counter WHERE id_template_param='{$param['id_template_param']}' ");
			$counter ++;
		}
		Db::finish( $res, $db);
	}

	public function ParamStore($id_template_param,$id_template,$label,$id_type,$type_params,$default_value,$public,$index_include)
	{
		$type = $this->params_types[$id_type];
		if (!$id_template_param>0)
			$seq = $this->ParamNextSeq($id_template);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "template_params" );
		if ($id_template_param>0)
		{
			$sqlstr = "UPDATE template_params SET label='$label',type='$type',type_params='$type_params',
				default_value='$default_value',public='$public',index_include='$index_include'
				 WHERE id_template_param='$id_template_param'";
		}
		else
		{
			$id_template_param = $db->nextId( "template_params", "id_template_param" );
			$sqlstr = "INSERT INTO template_params (id_template_param,id_template,label,type,type_params,default_value,public,index_include,seq) 
				VALUES ('$id_template_param','$id_template','$label','$type','$type_params','$default_value','$public','$index_include','$seq')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_template_param;
	}
	
	public function ResourceSet($id_res)
	{
		if($id_res>0)
			$this->id_res = $id_res;
	}
	
	public function Resources($id_template)
	{
		$resources = array();
		switch($this->id_res)
		{
			case "5":
				include_once(SERVER_ROOT."/../classes/articles.php");
				$resources = Articles::Template($id_template);
			break;
		}
		return $resources;
	}
	
	public function ResourceUpdate($id,$unescaped_template_params)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "template_values" );
		foreach($unescaped_template_params as $key=>$unescaped_param)
		{
			$value = $db->SqlQuote($unescaped_param);
			$sqlstr = "REPLACE INTO template_values (id_res,id,id_template_param,value) VALUES ($this->id_res,$id,$key,'$value') ";
			$res[] = $db->query( $sqlstr );
		}
		Db::finish( $res, $db);
	}
	
	public function ResourceValues($id_template,$id,$public_only=false,$index_only=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT tv.value,tp.id_template_param,tp.label,tp.type,tp.type_params,tp.public   
			FROM template_values tv 
			INNER JOIN template_params tp ON tv.id_template_param=tp.id_template_param 
			WHERE tv.id_res='$this->id_res' AND tp.id_template=$id_template AND tv.id=$id ";
		if($public_only)
			$sqlstr .= " AND tp.public=1 ";
		if($index_only)
			$sqlstr .= " AND tp.index_include=1 ";
		$sqlstr .= " ORDER BY tp.seq ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ResourceTypes($all_resource_types)
	{
		foreach($all_resource_types as $key => $res_type)
		{
			if($key!=5)
			{
				unset($all_resource_types[$key]);
			}
		}
		return $all_resource_types;
	}
	
	public function TemplateDelete( $id_template )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("templates","template_params","template_values"));
		$res[] = $db->query( "DELETE FROM template_values WHERE id_template_param IN (SELECT id_template_param FROM template_params WHERE id_template='$id_template') " );
		$res[] = $db->query( "DELETE FROM template_params WHERE id_template='$id_template' " );
		$res[] = $db->query( "DELETE FROM templates WHERE id_template='$id_template' " );
		Db::finish( $res, $db);
	}

	public function TemplateGet($id_template)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_template,id_res,name,id_group,id_topic,hide_fields,default_subtopic FROM templates WHERE id_template='$id_template'";
		$db->query_single($row, $sqlstr);
		$row['hidden_fields'] = explode("|",$row['hide_fields']);
		return $row;
	}
	
	public function TemplateStore( $id_template,$name,$id_group,$id_topic,$hide_fields,$default_subtopic )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "templates" );
		if ($id_template>0)
		{
			$sqlstr = "UPDATE templates SET name='$name',id_group='$id_group',id_topic='$id_topic',hide_fields='$hide_fields',id_res='{$this->id_res}',
				default_subtopic='$default_subtopic'
				 WHERE id_template='$id_template'";
		}
		else
		{
			$id_template = $db->nextId( "templates", "id_template" );
			$sqlstr = "INSERT INTO templates (id_template,id_res,name,id_group,id_topic,hide_fields,default_subtopic) 
				VALUES ($id_template,'{$this->id_res}','$name','$id_group','$id_topic','$hide_fields','$default_subtopic')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_template;
	}
	
	public function Templates( &$rows, $id_topic, $paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT te.id_template,te.name,te.id_group,te.id_topic,te.id_res,count(a.id_article) AS counter
			FROM templates te 
			LEFT JOIN articles a ON te.id_template=a.id_template ";
		if($id_topic>0)
			$sqlstr .= " WHERE te.id_topic=$id_topic ";
		$sqlstr .= " GROUP BY te.id_template ORDER BY te.name ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}


}
?>
