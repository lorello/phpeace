<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/lock.php");
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/varia.php");
include_once(SERVER_ROOT."/../classes/services.php");

/**
 * Cronjob management
 * This class deals with calls originating from external script (admin/gate/cron.php)
 * which is usually exectued every 15 minutes by system cron.
 * PhPeace comes with 3 scheduler slots (0:5, 4:15, 12:30)
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Cron
{

	/**
	 * For how many minutes a cron job is allowed to run
	 * before displaying a warning message in the admin interface
	 *
	 */
	const MinimumTimeAdminWarning = 5;
	
	/**
	 * For how many hours a cron job is allowed to run
	 * before settings its service status to down
	 *
	 */
	const MinimumTimeServiceChange = 24;

	/**
	 * For how many hours a cron job is allowed to run
	 * before removing its lock
	 *
	 */
	const MinimumTimeLockRemove = 48;

	/**
	 * IP allowed to run cron
	 *
	 * @var string
	 */
	public $ip_allow;

	/**
	 * @var Lock */
	public $lock;

	/**
	 * Last cron execution
	 *
	 * @var timestamp
	 */
	private $last_cron_ts;
	
	/**
	 * Timeout for cron execution (in seconds)
	 *
	 * @var integer
	 */
	private $timeout;
	
	/**
	 * Show info during cron executing
	 *
	 * @var boolean
	 */
	private $debug;

	/**
	 * Initialize local properties
	 *
	 */
	function __construct()
	{
		$ini = new Ini();
		$this->ip_allow = trim($ini->Get('scheduler_ip'));
		$this->last_cron_ts = (int)$ini->Get('last_cron_ts');
		$this->lock = new Lock("cron");
		$conf = new Configuration();
		$this->timeout = $conf->Get("cron_timeout");
		$this->debug = false;
	}

	/**
	 * Verify the current IP is allowed to run cron
	 *
	 * @return unknown
	 */
	public function IPCheck()
	{
		$ip = Varia::IP();
		return ($ip==$this->ip_allow  || $ip=="127.0.0.1");
	}
	
	/**
	 * Run scheduler for specific timestamp
	 * Verify lock presence
	 * Retrieve scheduler slot and associated tasks to run
	 *
	 * @param timestamp $timestamp	UNIX timestamp
	 * @param boolean 	$debug		Output information while running
	 */
	public function Run($timestamp,$debug=false)
	{
		$this->debug = $debug;
		$hour = date("G",$timestamp);
		$minute = date("i",$timestamp);
		if($this->lock->LockCheck())
		{
			UserError("Cron at $hour:$minute failed due to existing lock",array(),1024);
			$last_ts = $this->lock->LockLastTime();
			$diff = floor((time() - $last_ts)/(60*60));
			if($diff >= self::MinimumTimeServiceChange )
			{
				$se = new Services();
				$ini = new Ini();
				$admin_web = $ini->Get("admin_web");
				if($diff >= self::MinimumTimeLockRemove )
				{
					$message = "Cron lock has been stuck for 2 days.\nIt has been removed right now.\nPlease check your logs:\n$admin_web/admin/log.php";
					$this->lock->LockRemove();
					$se->ServiceStatusUpdate("scheduler",1,$message);
				}
				else 
				{
					$message = "Cron lock has been stuck for 1 day.\nPlease check your logs:\n$admin_web/admin/log.php\nand remove the cron lock\n$admin_web/admin/schedules.php";
					$se->ServiceStatusUpdate("scheduler",0,$message);
				}
			}
		}
		else
		{
			$this->EchoDebugInfo("Acquiring lock");
			$this->lock->LockSet();
			$this->EchoDebugInfo("Setting timeout = $this->timeout seconds");
			$va = new Varia();
			$va->SetTimeLimit($this->timeout);
			include_once(SERVER_ROOT."/../classes/scheduler.php");
			$sc = new Scheduler();
			include_once(SERVER_ROOT."/../classes/log.php");
			$log = new Log();
			$schedules = $sc->SchedulerSlotsRange($timestamp,$this->last_cron_ts);
			$num_schedules = count($schedules);
			if($num_schedules>0)
			{
				$this->EchoDebugInfo("Found " . count($schedules) . " schedules to run");
				$schedule = $schedules[0]; // Run first schedule only
				$sc->pm->MessageSet("scheduler_cron",array($schedule['time'],"$hour:$minute"));
				$this->EchoDebugInfo("Running scheduler for {$schedule['time']} at $hour:$minute");
				$sc->Execute($schedule['id_scheduler']);
				if($sc->executed>0 && $sc->notify)
				{
					$msg = $sc->pm->MessageGet(false);
					$log->Write("scheduler","Scheduler $hour:$minute",$msg);
				}
				$ini = new Ini();
				$ini->SetTimestamp("last_cron",$num_schedules>1? mktime($schedule['hour'],$schedule['minute'],30) : mktime($hour,$minute,0));
			}
			$this->EchoDebugInfo("Running heavy actions");
			$messages = $sc->HeavyAction($timestamp,$this->debug);
			$this->EchoDebugInfo($messages);
			$this->EchoDebugInfo("removing lock");
			$this->lock->LockRemove();
			$fm = new FileManager();
			$this->EchoDebugInfo("Executing post update script");
			$fm->PostUpdate();
			$this->EchoDebugInfo("Updating Scheduler service status");
			$se = new Services();
			$se->ServiceStatusUpdate("scheduler",1,"Cron executed at $hour:$minute",true);
		}
	}

	/**
	 * Output debugging info
	 * (useful when executing cron from command line)
	 *
	 * @param mixed $info
	 */
	private function EchoDebugInfo($info)
	{
		if($this->debug)
		{
			if(is_array($info))
			{
				foreach($info as $info_msg)
				{
					echo "$info_msg\n";
				}
			}
			else 
			{
				echo "$info\n";
			}
		}
	}
}
?>
