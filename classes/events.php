<?php
use Kigkonsult\Icalcreator\Vcalendar;


/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/geo.php");
include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * Manage generic calendar events functions
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Events
{
	/** 
	 * @var Geo */
	public $geo;

	/**
	 * Topic ID
	 *
	 * @var integer
	 */
	public $id_topic;

	/**
	 * @var Kigkonsult\Icalcreator\Vcalendar */
	private $vcalendar;
	
	/**
	 * Topic group ID
	 *
	 * @var integer
	 */
	public $id_group;
	
	private $img_sizes;
	
	/**
	 * Initialize local properties
	 *
	 */
	function __construct()
	{
		$this->geo = new Geo();
		$this->id_topic = 0;
		$this->id_group = 0;
		$conf = new Configuration();
		$this->img_sizes = $conf->Get("img_sizes");
	}

	/**
	 * Update calendar configuration
	 *
	 * @param string	$path			Calendar directory under the portal
	 * @param string	$title			Calendar name
	 * @param boolint	$events_insert	Whether visitors can insert events from the portal
	 * @param boolint	$search_events	Whether events are shown in search results
	 */
	public function ConfigurationUpdate($path,$title,$events_insert,$search_events)
	{
		$ini = new Ini;
		$ini->Set("events_title",$title);
		$ini->SetPath("events_path",$path);
		$ini->Set("events_insert",$events_insert);
		$ini->Set("search_events",$search_events);
	}

	/**
	 * Total number of events
	 *
	 * @return integer
	 */
	public function Count()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT count(id_event) AS counter FROM events");
		return (int)$row['counter'];
	}

	/**
	 * Retrieve all approved events
	 *
	 * @return array
	 */
	public function EventsAll()
	{
		$sqlstr = "SELECT id_event,UNIX_TIMESTAMP(start_date) AS start_date_ts
			FROM events WHERE approved=1 ORDER BY start_date DESC";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve all events in a specific day
	 *
	 * @param integer	$timestamp	Unix timestamp
	 * @return array
	 */
	public function EventsDay( $timestamp )
	{
		$date = date("Ymd",$timestamp);
		$join = $this->geo->GeoJoin("e.id_geo");
		$sqlstr = "SELECT id_event,e.title,e.description,place,{$join['name']} AS geo_name,type,allday,id_topic,
		UNIX_TIMESTAMP(start_date) AS start_date_ts,e.jump_to_article,e.id_article,e.has_image,e.image_ratio
		FROM events e {$join['join']}
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type
		WHERE e.approved=1 ";
		if($this->id_topic>0)
			$sqlstr .=  " AND (e.id_topic='$this->id_topic' OR e.id_group='$this->id_group') ";
		else 
			$sqlstr .= " AND e.portal=1 ";
		$sqlstr .= " AND DATE_FORMAT(e.start_date,'%Y%m%d')='$date' "; 
		$sqlstr .= " ORDER BY start_date ASC";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve all events with a specific approved status
	 * (in the current topic)
	 *
	 * @param array		$rows		Events
	 * @param boolint 	$approved	Approved status
	 * @return integer				Num of events
	 */
	public function EventsApproved( &$rows, $approved)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts,e.id_event,e.title,e.place,et.type,
		e.approved,IF(e.id_group>0 AND e.id_topic=0,tg.name,t.name) AS name,e.has_image,e.image_ratio
		FROM events e
		INNER JOIN event_types et USING (id_event_type)
		LEFT JOIN topics t ON e.id_topic=t.id_topic
		LEFT JOIN topics_groups tg ON e.id_group=tg.id_group
		WHERE e.approved=$approved ";
		if($this->id_topic>0)
			$sqlstr .= "  AND e.id_topic='$this->id_topic' ";
		$sqlstr .= " ORDER BY start_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Retrieve all events for a specific month
	 *
	 * @param integer	$timestamp	Unix timestamp
	 * @return array
	 */
	public function EventsMonth( $timestamp )
	{
		if($timestamp==0)
			$timestamp = time();
		$year = date("Y",$timestamp);
		$month = date("m",$timestamp);
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts,
			id_event,e.title,place,type,{$join['name']} AS geo_name,e.has_image,e.image_ratio,
			e.id_topic,e.jump_to_article,e.id_article,DAYOFMONTH(start_date) AS dom 
		FROM events e 
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE e.approved=1 AND YEAR(e.start_date) = $year AND MONTH(e.start_date) = $month ";
		if($this->id_topic>0)
			$sqlstr .=  " AND e.id_topic='$this->id_topic' ";
		else
			$sqlstr .= " AND e.portal=1 ";
		$sqlstr .= " ORDER BY e.start_date ASC ";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Retrieve all events for a specific range
	 *
	 * @param integer	$start_ts	Range start Unix timestamp
	 * @param integer	$end_ts		Range end - Unix timestamp
	 * @return array
	 */
	private function EventsRange( $start_ts, $end_ts )
	{
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		$sqlstr = "SELECT DATE_FORMAT(e.start_date,'%Y-%m-%dT%H:%i') as start,DATE_FORMAT(e.start_date,'%Y-%m-%d') as start_day,
		UNIX_TIMESTAMP(start_date) AS start_date_ts,
		id_event,e.title,place,type,{$join['name']} AS geo_name,e.place,e.allday,e.jump_to_article,e.length,e.description,
		e.place_details,e.contact_name,e.email,e.link,e.is_html,e.has_image,e.latitude,e.longitude,e.address,
		e.id_topic,e.jump_to_article,e.id_article,e.image_ratio
		FROM events e
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE e.approved=1 AND UNIX_TIMESTAMP(e.start_date)>=$start_ts
		AND UNIX_TIMESTAMP(e.start_date)<=$end_ts AND e.portal=1
		ORDER BY e.start_date ASC ";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function EventsRangeCal($start_ts,$end_ts) {
		$rows = $this->EventsRange($start_ts, $end_ts);
		return $this->EventsObj($rows);
	}
	
	public function EventsObj($rows,$json=true) {
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		include_once(SERVER_ROOT."/../classes/datetime.php");
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$th = new TextHelper();
		$ini = new Ini();
		$id_language = $ini->Get('id_language');
		$tr = new Translator($id_language,0,false,0);
		$dt = new DateTimeHelper($tr->Translate("month"),$tr->Translate("weekdays"),$tr->Translate("hours"));
		$events = array();
		foreach($rows as $row) {
			$e = $row;
			$e['id'] = $row['id_event'];
			$e['start_date'] = $dt->FormatDate($row['start_date_ts']);
			$e['start_time'] = $dt->FormatTime($row['start_date_ts']);
			$e['prov'] = $row['geo_name'];
			$e['description'] = $row['is_html']? $row['description'] : $th->Htmlise($row['description'],false);
			$e['place_details'] = $th->Htmlise($row['place_details'],false);
			$e['export'] = $irl->pub_web . '/' . $ini->Get("events_path") . "/export.php?format=ical&type=event&id={$row['id_event']}";
			$e['export_escaped'] = urlencode($e['export']);
			$e['export_webcal'] = preg_replace('#^http(s)?://#','webcal://',$e['export']);
			$description = $e['description'];
			if($e['is_html']) {
				$th->Html2Text($description);
			}
			$description = str_replace("\t",'',trim($description));
			if($e['link']!='') {
				$description .= "\n\n$link";
			}
			$end_date_ts = $row['start_date_ts'] + (($row['length']==0?1:$row['length'])*3600);
			$google_location = urlencode("{$row['address']}, {$row['place']}");
			if($row['latitude']!='' && $row['longitude']!='' && $row['latitude']!=0) {
				$google_location .= "&ll={$row['latitude']},{$row['longitude']}";
			}
			$e['export_google'] = $irl->GoogleCalendarLink($e['title'],$description,$e['start_date_ts'],$end_date_ts,$row['allday'],$google_location);
			$e['gmaps'] = "https://www.google.com/maps/search/?api=1&hl={$tr->lang_code}&query=" . $google_location;
			if($row['length']>0) {
				$e['end'] = date("Y-m-d\TH:i",$end_date_ts);
			}
			$e['url'] = $irl->PublicUrlGlobal('events', array('id'=>$row['id_event'],'subtype'=>'event','id_topic'=>0));
			if($row['allday']) {
				$e['allDay'] = true;
			}
			if($row['id_article']>0) {
				include_once(SERVER_ROOT."/../classes/topic.php");
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($row['id_article']);
				$a->ArticleLoad();
				$irl->topic = new Topic($a->id_topic);
				$article_options = 	array('id'=>$row['id_article'],'id_topic'=>($a->id_topic));
				if($a->jump!="") {
					$article_options['jump'] = $th->String2Url($a->jump);
				}
				$e['article_topic'] = $irl->topic->name;
				$e['article_title'] = $a->headline;
				$e['article_url'] = $irl->PublicUrlTopic('article', $article_options, $irl->topic);
			}
			if($row['has_image']) {
				$e['thumb'] = $irl->PublicUrlGlobal("event_image",array('id'=>$row['id_event'],'size'=>1,'format'=>'jpg'),TRUE );
				$e['image'] = $irl->PublicUrlGlobal("event_image",array('id'=>$row['id_event'],'size'=>3,'format'=>'jpg'),TRUE );
				$e['large'] = $irl->PublicUrlGlobal("event_image",array('id'=>$row['id_event'],'size'=>5,'format'=>'jpg'),TRUE );
				$e['orig'] = $irl->PublicUrlGlobal("event_image",array('id'=>$row['id_event'],'size'=>-1,'format'=>'jpg'),TRUE );
			}
			if($json) {
				$events[] = $e;
			} else {
				$e['xname'] = 'event';
				$events['e'.$row['id_event']] = $e;
			}
		}
		return $events;
	}
	
	public function ExportIcsEvent($id_event) {
		include_once(SERVER_ROOT."/../classes/event.php");
		$e = new Event();
		$event = $e->EventGet($id_event);
		$events = [$event];
		$events = $this->EventsObj($events,false);
		$base_url = $this->IcsCalendarInit($id_event,$event['title']);
		$this->IcsEvent($events['e'.$id_event], $base_url);
		return $this->vcalendar->returnCalendar();
	}
	
	public function ExportIcsEvents($events,$id,$name,$desc) {
		$base_url = $this->IcsCalendarInit($id,$name,$desc);
		foreach($events as $event) {
			$this->IcsEvent($event, $base_url);
		}
		return $this->vcalendar->returnCalendar();
	}
	
	private function IcsEvent($event,$base_url) {
		$vevent = $this->vcalendar->newVevent();
		$link = "$base_url/event.php?id={$event['id_event']}";
		if($event['allday']) {
			$vevent->setDtstart(
					date("Ymd",$event['start_date_ts']),
					[ "VALUE" => "DATE" ]
					);
		} else {
			$vevent->setDtstart(
					new DateTime( date("Y-m-d H:i:s",$event['start_date_ts']))
					);
			if($event['length']>0) {
				$vevent->setDtend(
						new DateTime( date("Y-m-d H:i:s",$event['start_date_ts'] + ($event['length']*3600)))
						);
			}
		}
		$vevent->setLocation("{$event['address']} {$event['place']}" );
		if(isset($event['latitude']) && $event['latitude']>0) {
			$vevent->setGeo($event['latitude'],$event['longitude']);
		}
		$vevent->setSummary($event['title']);
		$html = $this->th->Htmlise($event['description'],false) . "<p><a href='$link'>$link</a></p>";
		$vevent->setXprop( "X-ALT-DESC", $html);
		if($event['is_html']) {
			$this->th->Html2Text($event['description']);
		}
		$description = str_replace("\t",'',trim($event['description']));
		$vevent->setDescription($description . "\n\n$link");
		$vevent->setUrl($link);
		if($event['has_image']=='1') {
			$vevent->setXprop('X-WP-IMAGES-URL',$event['orig']);
		}
	}
	
	private function IcsCalendarInit($id='',$name='',$desc='') {
		$calname = "Calendario PeaceLink";
		$caldesc = $desc!=''? $desc : "Gli appuntamenti del mondo pacifista italiano";
		require(SERVER_ROOT."/../others/vendor/autoload.php");
		include_once(SERVER_ROOT."/../classes/ini.php");
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		$this->th = new TextHelper();
		$ini = new Ini();
		$tz = "Europe/Rome";
		$config = [
				Vcalendar::UNIQUE_ID => "peacelink.it"
		];
		$uuid      = "pck-events";
		if($id!='') {
			$uuid .= "-{$id}";
			$calname .= " - {$name}";
			// $config['filename'] = "{$uuid}.ics"; // date( "YmdHis" ).".ics"
		}
		$this->vcalendar = new Vcalendar($config);
		$this->vcalendar->setMethod( Vcalendar::PUBLISH );
		$this->vcalendar->setXprop( Vcalendar::X_WR_CALNAME, $calname);
		$this->vcalendar->setXprop( Vcalendar::X_WR_CALDESC, $caldesc);
		$this->vcalendar->setXprop( Vcalendar::X_WR_TIMEZONE, $tz);
		$url =  $ini->Get("pub_web") . '/' . $ini->Get("events_path");
//		$this->vcalendar->setXprop( "X-FROM-URL", $url);
		$this->vcalendar->setXprop( Vcalendar::X_WR_RELCALID, $uuid);
		return $url;
	}
	
	/**
	 * Retrieve all events associated to a specific keyword
	 *
	 * @param integer $id_keyword	Keyword ID
	 * @param integer $limit		Limit number of results
	 * @param integer $year			Specific year (optional)
	 * @return array				Events
	 */
	public function Keyword($id_keyword,$limit,$year=0,$only_future=true)
	{
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		if($year>0 && is_numeric($year))
			$sqlstr .= " AND YEAR(e.start_date)=$year ";
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts,
			id_event,e.title,place,type,$join[name] AS geo_name,
			e.id_topic,'event' AS item_type,e.jump_to_article,e.id_article
			FROM keywords_use ku
			INNER JOIN events e ON ku.id=e.id_event AND ku.id_type=8 
			INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE ku.id_keyword=$id_keyword AND e.approved=1 ";
		if($only_future) {
			$sqlstr .= " AND e.start_date>=CURDATE()";
		}
		if($this->id_topic>0)
			$sqlstr .=  " AND (e.id_topic='$this->id_topic' OR e.id_group='$this->id_group') ";
		else 
			$sqlstr .= " AND e.portal=1 ";
		$sqlstr .= " ORDER BY e.start_date " . ($only_future? " ASC":" DESC");
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Retrieve all future events
	 *
	 * @param integer	$how_many	Limit how many events to return
	 * @param integer	$event_type	Event type ID
	 * @param boolean	$global		Whether events are from the global calendar or from a specific topic
	 * @param integer	$ts_limit	Date to filter events after (default to 0, i.e. today)
	 * @return array
	 */
	public function Next( $how_many, $event_type, $global, $ts_limit=0 )
	{
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts,
			id_event,e.title,place,type,{$join['name']} AS geo_name,e.address,e.description,
			e.id_topic,e.jump_to_article,e.id_article,e.has_image,e.image_ratio
		FROM events e 
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		if($event_type>0)
			$sqlstr .= " AND e.id_event_type=$event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE e.approved=1 AND e.start_date>=CURDATE()";
		if($global)
			$sqlstr .= " AND e.portal=1 ";
		elseif($this->id_topic>0)
		{
			if($this->id_group>0)
				$sqlstr .=  " AND (e.id_topic='$this->id_topic' OR e.id_group='$this->id_group') ";
			else
				$sqlstr .=  " AND e.id_topic='$this->id_topic' ";
		}
		if ($ts_limit>0)
			$sqlstr .= " AND UNIX_TIMESTAMP(e.start_date)<=$ts_limit ";
		$sqlstr .= " ORDER BY e.start_date ASC LIMIT $how_many";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve all future events
	 *
	 * @param array		$params	Filters
	 * @return array
	 */
	public function NextPub( $params )
	{
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		$sqlstr = "SELECT DATE_FORMAT(e.start_date,'%Y-%m-%dT%H:%i') as start,DATE_FORMAT(e.start_date,'%Y-%m-%d') as start_day,
		UNIX_TIMESTAMP(start_date) AS start_date_ts,{$join['name']} AS geo_name,
		id_event,e.title,place,type,e.place,e.allday,e.jump_to_article,e.length,e.description,
		e.place_details,e.contact_name,e.phone,e.email,e.link,e.is_html,e.has_image,e.latitude,e.longitude,e.address,
		e.id_topic,e.jump_to_article,e.id_article,e.image_ratio
		FROM events e
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE e.approved=1 AND e.start_date>=CURDATE() AND e.portal=1 ";
		if ($params['id_prov'] > 0) {
			$sqlstr .= " AND e.id_geo={$params['id_prov']}";
		}
		if ($params['id_reg'] > 0) {
			$provinces = $this->geo->ProvincesByRegion($params['id_reg']);
			$ids = array();
			foreach($provinces as $prov) {
				$ids[] = $prov['id_prov'];
			}
			if(count($ids)>0) {
				$sqlstr .= " AND e.id_geo IN (" . implode(',',$ids) . ")";
			}
		}
		$sqlstr .= " ORDER BY e.start_date ASC ";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Return the date range of all events in the calendar
	 *
	 * @return array	Containing two values, the minimum day and the maximum day (as Unix timestamps)
	 */
	public function Period()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,	"SELECT UNIX_TIMESTAMP(MIN(start_date))-86400 AS min_ev_ts,UNIX_TIMESTAMP(MAX(start_date))+86400 AS max_ev_ts FROM events");
		return $row;
	}

	/**
	 * The recurring events for a specific date
	 *
	 * @param integer $ts	Unix timestamp of date
	 * @return array
	 */
	public function RecurringEvents( $ts )
	{
		$date = getdate($ts);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_event,r_day,r_month,r_year,description,url
			FROM recurring_events WHERE approved=1 AND r_day=$date[mday] AND r_month=$date[mon] ORDER BY r_year DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Retrieve recurring events (anniversaries)
	 *
	 * @param array 	$rows		Recurring events
	 * @param boolint 	$approved	Approved status
	 * @param integer 	$id_month	Specific month ID (optional)
	 * @return integer				Num of events
	 */
	public function Recurring( &$rows, $approved, $id_month=0 )
	{
		$db =& Db::globaldb();
		if ($id_month>0)
			$cond = " AND r_month=$id_month ";
		$rows = array();
		$sqlstr = "SELECT id_event,description,r_year,r_month,r_day
			FROM recurring_events WHERE approved=$approved $cond ORDER BY r_year DESC, r_month DESC, r_day DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * List of all months including approved recurring events
	 *
	 * @param array $rows	Months
	 * @return integer		Num of months
	 */
	public function RecurringMonths( &$rows )
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT r_month,count(id_event) AS num_events FROM recurring_events WHERE approved=1 GROUP BY r_month";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function RequestApi($arguments) {
		$formats = array('xml','json','ical');
		$params = array();
		$params['format'] = (isset($arguments['format']) && in_array($arguments['format'],$formats))? $arguments['format'] : 'json';
		$params['id_prov'] = "0";
		$params['id_reg'] = "0";
		if(isset($arguments['id']) && $arguments['id']>0) {
			include_once(SERVER_ROOT."/../classes/event.php");
			$e = new Event();
			$event = $e->EventGet($arguments['id']);
			$events = [$event];
		} else {
			if(isset($arguments['prov']) && $arguments['prov']!='') {
				$geo = new Geo();
				$prov = $geo->ProvinceGetByProv($arguments['prov']);
				if(isset($prov['id_prov']) && $prov['id_prov']>0) {
					$params = array_merge($params,$prov);
				}
			} elseif(isset($arguments['reg']) && $arguments['reg']!='') {
				$geo = new Geo();
				$reg = $geo->RegionGetByReg($arguments['reg']);
				if(isset($reg['id_reg']) && $reg['id_reg']>0) {
					$params = array_merge($params,$reg);
				}
			}
			$events = $this->NextPub($params);
		}
		$info = array();
		$info['params'] = $params;
		$info['qs'] = $arguments;
		$info['count'] = count($events);
		$return = array('info'=>$info);
		$return['events'] = $this->EventsObj($events,$params['format']=='json');
		return $return;
	}
	
	/**
	 * Search events
	 *
	 * @param array 	$rows			Events
	 * @param array 	$words			Search words
	 * @param array 	$params			Search params
	 * @param integer 	$time_weight	Weight to decrease ranking of past events (optional, default to 0)
	 * @param boolean 	$only_approved	Whether ony approved events should be returned (optional, default to true)
	 * @return integer					Num of events
	 */
	public function Search( &$rows,$words,$params,$time_weight=0,$only_approved=true )
	{
		$gjoin = $this->geo->GeoJoin("e.id_geo");
		$db =& Db::globaldb();
		$sqljoin = "";
		$wheres = $scores = [];
		for ($i = 0; $i < count($words); $i++)
		{
			$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = e.id_event AND si$i.id_res=8 ";
			if($this->id_topic>0)
				$sqlstr .=  " AND (si$i.id_topic='$this->id_topic' OR si$i.id_group='$this->id_group') ";
			$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
			$wheres[] = "sw$i.word = '$words[$i]'";
			$scores[] = "SUM(si$i.score)";
		}
		if ($params['id_geo'] > 0)
			$cond .= " AND e.id_geo={$params['id_geo']}";
		if ($params['id_etype'] > 0)
			$cond .= " AND e.id_event_type={$params['id_etype']}";
		if($this->id_topic>0)
			$cond .=  " AND (e.id_topic='$this->id_topic' OR e.id_group='$this->id_group') ";
		if ($params['period'] > 0)
		{
			if ($params['period']==1)
				$cond .= " AND e.start_date>=CURDATE() ";
			if ($params['period']==2)
				$cond .= " AND e.start_date<CURDATE() ";
		}
		if ((strlen($params['start_date1']) > 2) && (strlen($params['start_date2']) > 2))
			$cond .= " AND e.start_date BETWEEN '{$params['start_date1']}' AND '{$params['start_date2']}'";
		if (strlen($params['keyword']) > 0)
		{
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$cond .= " AND k.keyword='{$params['keyword']}' ";
			$join = "INNER JOIN keywords_use ku ON ku.id=e.id_event AND ku.id_type=" . $o->types['event'] . " INNER JOIN keywords k ON ku.id_keyword=k.id_keyword";
		}
		$sqlstr = "SELECT UNIX_TIMESTAMP(e.start_date) AS start_date_ts,e.id_event,
			e.title,e.place,e.id_topic,e.jump_to_article,e.id_article,e.has_image,e.image_ratio,
			'event' AS item_type,evt.type,$gjoin[name] AS geo_name ";
		if(!$only_approved)
			$sqlstr .= ",e.approved";
		if(is_array($scores) && count($scores)>0)
		{
			$sqlstr .= ", ((" . implode("+",$scores) . ") ";
			if($time_weight>0)
				$sqlstr .= " - DATEDIFF(CURDATE(),e.start_date)*$time_weight ";
			$sqlstr .= " ) AS total_score ";
		}
		$sqlstr .= " FROM events e
		INNER JOIN event_types evt ON e.id_event_type=evt.id_event_type $gjoin[join] $sqljoin $join 
		WHERE 1=1 ";
		if ($only_approved)
			$sqlstr .= " AND  e.approved=1 ";
		if(is_array($wheres) && count($wheres)>0)
			$sqlstr .= " AND  " . implode(" AND ", $wheres);
		$sqlstr .= " $cond GROUP BY e.id_event
		ORDER BY e.start_date DESC ";
		return $db->QueryExe($rows, $sqlstr,true);
	}

	/**
	 * Events submitted by a specific person
	 * (only approved ones)
	 *
	 * @param integer $id_p		Person ID
	 * @param integer $id_topic	Topic ID
	 * @return array			Events
	 */
	public function Submitted($id_p,$id_topic)
	{
		$cond = ($id_topic>0)? " AND e.id_topic=$id_topic" : "";
		$join = $this->geo->GeoJoin("e.id_geo");
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts,e.id_event,e.title,e.place,et.type,$join[name] AS geo_name,
			e.id_topic,e.jump_to_article,e.id_article,UNIX_TIMESTAMP(e.insert_date) AS hdate_ts
		FROM events e 
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type ";
		$sqlstr .= $join['join'];
		$sqlstr .= " WHERE e.approved=1 $cond AND e.id_p=$id_p ORDER BY e.start_date DESC";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Topics which have a subtopic of type calendar
	 * (i.e. can be shown in the topic assignment box for events)
	 *
	 * @return array
	 */
	public function Topics()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_topic,t.name,t.id_group,tg.name as group_name
		FROM topics t 
		INNER JOIN topics_groups tg ON t.id_group=tg.id_group
		LEFT JOIN subtopics s ON t.id_topic=s.id_topic 
		WHERE s.id_type=10 GROUP BY t.id_topic
		ORDER BY tg.seq,tg.id_group,t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Types of events
	 *
	 * @return array
	 */
	public function Types()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_event_type,type FROM event_types ORDER BY type";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Types of events (paginated)
	 *
	 * @param array $rows	Types
	 * @return integer		Num of types
	 */
	public function TypesP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_event_type,type FROM event_types ORDER BY type";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}

/**
 * REST services for calendar
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class EventsREST
{
	public $arguments;
	
	public function Next() {
		include_once(SERVER_ROOT."/../classes/cache.php");
		$cache = new Cache;
		$cache->ttl = 10;
		$cache_id = serialize($this->arguments);
		$return_ser = $cache->Get('events_api',$cache_id);
		$return = unserialize($return_ser);
		$params = $return['info']['params'];
		switch($params['format']) {
			case 'json':
				header('Content-type: application/json');
				echo json_encode($return);
			break;
			case 'xml':
				header('Content-type: text/xml');
				include_once(SERVER_ROOT."/../classes/xmlhelper.php");
				$xh = new XmlHelper();
				echo $xh->Array2Xml($return);
			break;
			case 'ical':
				$id = $name = $desc = '';
				if(isset($params['id_reg']) && $params['id_reg']>0) {
					$id = 'r' . $params['id_reg'];
					$name = $params['reg'];
					$desc = "Appuntamenti in {$params['reg']}";
				}
				if(isset($params['id_prov']) && $params['id_prov']>0) {
					$id = 'p' . $params['id_prov'];
					$name = $params['prov'];
					$desc = "Appuntamenti nella provincia di {$params['prov']}";
				}
				$ee = new Events();
				echo $ee->ExportIcsEvents($return['events'],$id,$name,$desc);
			break;
		}
	}
}

?>
