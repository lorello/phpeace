<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/resources.php");

/**
 * Manage external contributions
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Contribution
{
	/**
	 * Resource type ID
	 *
	 * @var integer
	 */
	private $id_resource_type;

	/**
	 * Initialize local variables
	 *
	 * @param string $type	Resource type
	 */
	function __construct($type)
	{
		$r = new Resources();
		$this->id_resource_type= $r->types[$type];
	}

	/**
	 * Retrieve external contribution information associated to
	 * current type and specific resource ID
	 *
	 * @param integer $id_resource	Resource ID
	 * @return array
	 */
	public function Get($id_resource)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name,email,notify,UNIX_TIMESTAMP(time) AS time FROM contributions 
		WHERE id_resource_type='{$this->id_resource_type}' AND id_resource='$id_resource'";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	/**
	 * Add info about an external contribution
	 *
	 * @param integer 	$id		Resource ID
	 * @param string	$name	Contributor's name
	 * @param string	$email	Contributor's email
	 * @param boolint	$notify	Notify contributor about publishing
	 */
	public function Insert($id,$name,$email,$notify)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$ip = Varia::IP();
		$db =& Db::globaldb();
		$time = $db->getTodayTime();
		$db->begin();
		$db->lock( "contributions" );
		$sqlstr =  "INSERT INTO contributions (id_resource_type,id_resource,name,email,notify,ip,time)
		 VALUES ($this->id_resource_type,$id,'$name','$email',$notify,'$ip','$time')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Verify if resource has orignated from external contribution
	 * Notify contributor about publishing
	 *
	 * @param integer $id_resource
	 * @param string $url
	 * @param string $title
	 */
	public function Notify($id_resource,$url,$title)
	{
		$row = $this->Get($id_resource);
		if ($row['notify'])
		{
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User;
			$u->id = $session->Get("current_user_id");
			$extra = array();
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			if ($u->id > 0)
			{
				$user = $u->UserGet();
				$extra['name'] = $user['name'];
				$extra['email'] = $user['email'];
			}
			else
			{
				$extra['name'] = "Staff $mail->title";
				$extra['email'] = $mail->staff_email;
				
			}
			$tr = new Translator($user['id_language'],0);
			$subject = $tr->Translate("article_published_notification");
			$message = $tr->TranslateParams("article_published_notification_msg",array($title,$url));
			$mail->SendMail($row['email'], $row['name'], $subject, $message, $extra);
		}
	}
}
?>
