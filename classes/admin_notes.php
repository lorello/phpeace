<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/resources.php");

/**
 * Internal notes for each resource type
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Admin_Notes
{
	/**
	 * Resource type ID
	 *
	 * @var integer
	 */
	public $id_res;
	
	/**
	 * Resource ID
	 *
	 * @var integer
	 */
	private $id;
	
	/**
	 * Initialize local properties
	 *
	 * @param string	$id_res_type	Resource type name
	 * @param integer	$id				Resource ID
	 */
	function __construct($id_res_type,$id)
	{
		$r = new Resources();
		if(array_key_exists($id_res_type,$r->types))
			$this->id_res = $r->types[$id_res_type];
		else
			UserError("Note type $id_res_type unknown",array());
		$this->id = $id;
	}
	
	/**
	 * Delete a note
	 *
	 * @param integer $id_note
	 */
	public function NoteDelete($id_note)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "resource_notes" );
		$res[] = $db->query( "DELETE FROM resource_notes WHERE id_note='$id_note' " );
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve note data
	 *
	 * @param integer $id_note
	 * @return array
	 */
	public function NoteGet($id_note)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_note,id_user,note,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,
		FROM resource_notes
		WHERE id_note='$id_note' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Create a new note for current resource type and ID
	 *
	 * @param string 	$note
	 * @return integer			Newly created ID
	 */
	public function NoteInsert( $note )
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$current_user_id = $session->Get("current_user_id");
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$db->begin();
		$db->lock( "resource_notes" );
		$id_note = $db->nextId( "resource_notes", "id_note" );
		$sqlstr = "INSERT INTO resource_notes (id_note,id_res,id,id_user,insert_date,note)
		 VALUES ('$id_note','$this->id_res','$this->id','$current_user_id','$today_time','$note')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_note;
	}

	/**
	 * Update note
	 *
	 * @param integer	$id_note
	 * @param string	$note
	 */
	public function NoteUpdate($id_note,$note)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "resource_notes" );
		$res[] = $db->query( "UPDATE resource_notes SET note='$note' WHERE id_note='$id_note' " );
		Db::finish( $res, $db);
	}
	
	/**
	 * Retrieve all notes for current resource type and ID
	 *
	 * @param 	array 	$rows	Notes
	 * @param 	boolean $paged	Pagination (optional)
	 * @return	integer			Num of notes
	 */
	public function Notes( &$rows, $paged=true )
	{
		$sqlstr = "SELECT rn.id_note,rn.id_user,u.name,rn.note,UNIX_TIMESTAMP(rn.insert_date) AS insert_date_ts
			FROM resource_notes rn 
			INNER JOIN users u ON rn.id_user=u.id_user 
			WHERE rn.id_res'$this->id_res' AND rn.id='$this->id'
			ORDER BY rn.insert_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

}
?>
