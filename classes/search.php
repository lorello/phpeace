<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");
include_once(SERVER_ROOT."/../classes/sharedmem.php");
define('MAX_STR_LENGTH',"25");
define('SEPARATOR',"\n");
define('SEARCH_WORDS_MAX',5);
define('SESSION_SEARCH_VAR',"search_query"); 

class Search
{
	public $resources_to_index;
	public $min_str_length;
	
	/**
	 * @var TextHelper */
	private $th;
	/** 
	 * @var SharedMem */
	private $mem;
	private $word_cache;
	private $mem_var_base;
	private $scheduler_index;
	private $relevance;
	private $languages;
	private $id_language;
	private $index_pdf;
	private $index_doc;

	function __construct()
	{
		$conf = new Configuration();
		$this->scheduler_index = $conf->Get("scheduler_index");
		$this->relevance = $conf->Get("indexer_weights");
		$this->min_str_length = $conf->Get("min_str_length");
		$this->index_pdf = $conf->Get("index_pdf");
		$this->index_doc = $conf->Get("index_doc");
		$ini = new Ini();
		$this->id_language = $ini->Get("id_language");
		$this->resources_to_index = explode("|",$ini->Get("search_config"));
		array_unshift($this->resources_to_index,0);
		$this->word_cache = array();
		$this->mem = new SharedMem();
		$this->mem_var_base = "searchwords";
		$this->th = new TextHelper();
		$this->languages = array('--','it','en','es','fr','de','ar','ru','pt','fi','ja','sv','ms');
		unset($conf);
		unset($ini);
	}
	
	function __destruct()
	{
		unset($this->th);
		unset($this->mem);
	}

	public function ConfigurationUpdate($resources)
	{
		$current_resources_to_index = $this->resources_to_index;
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->Set("search_config",$resources);
		$s2 = new Search();
		$new_resources_to_index = $s2->resources_to_index;
		foreach($current_resources_to_index as $id_res=>$value)
		{
			$new_value = $new_resources_to_index[$id_res];
			if($value=="0" && $new_value=="1")
				$this->Reindex($id_res,2,0);
			if($value=="1" && $new_value=="0")
				$this->ResourceDeleteAll($id_res);
		}
	}

	private function ContentExplode($content,$id_language)
	{
   		if (!$this->th->IsUnicode($content))
            $words = explode(' ', $content);
        else
            $words = $this->th->str_split_utf8($content);
   		$stop_words = $this->StopWords($id_language);
   		$words = array_diff($words,$stop_words);
		return $words;
	}
	
	private function IndexContent($id_res,$id,$content,$relevance,$id_language,$start_pos=0)
	{
		$last_pos = $start_pos;
		$content = strip_tags($content);
		if($content!="")
		{
			$content = $this->TextClean($content,$id_language);
			$content = $this->th->StripNonWords($content);
			$words = $this->ContentExplode($content,$id_language);
			if(count($words)>0)
				$last_pos = $this->IndexContentArray($id_res,$id,$words,$relevance,$start_pos);
		}
		return $last_pos;
	}
	
	private function IndexContentArray($id_res,$id,$words,$relevance,$start_pos=0)
	{
		$iwords = array();
		$counter = 0;
		foreach($words as $word)
		{
            $strlen = $this->th->StringLength($word);
            if (!is_null($word) && $strlen>=($this->min_str_length) && $strlen<=MAX_STR_LENGTH)
			{
				$iwords[$counter] = $this->WordLookup($word);
				$counter ++;
			}
		}
		$last_pos = $start_pos;
		if(count($iwords)>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "search_index" );
			foreach($iwords as $pos=>$id_word)
			{
				$last_pos = $start_pos + $pos;
				$sqlstr = "INSERT INTO search_index (id_res,id,id_word,word_pos,relevance) 
					VALUES ($id_res,$id,$id_word,$last_pos,$relevance) ON DUPLICATE KEY UPDATE id_word=$id_word, relevance=$relevance";
				$res[] = $db->query( $sqlstr );
			}
			Db::finish( $res, $db);
		}
		return $last_pos + 1;
	}
	
	public function IndexQueue()
	{
		$rows = array();
		$sqlstr = "SELECT id_res,id,id_topic,id_group 
			FROM search_queue 
			ORDER BY priority ASC,insert_time ASC LIMIT $this->scheduler_index";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		$counter = 0;
		foreach($rows as $row)
		{
			$this->ResourceIndex($row['id_res'],$row['id'],$row['id_topic'],$row['id_group']);
			$counter++;
		}
		return $counter;
	}
	
	public function IndexQueueAdd($id_res,$id,$id_topic,$id_group,$priority)
	{
		$resources_to_index = $this->resources_to_index;
		if($resources_to_index[$id_res])
		{
			$db =& Db::globaldb();
			$row = array();
			$db->query_single($row, "SELECT UNIX_TIMESTAMP(insert_time) AS ts FROM search_queue WHERE id_res='$id_res' AND id='$id' AND priority='$priority' ");
			if(!$row['ts']>0)
			{
				$db->begin();
				$insert_time = $db->getTodayTime();
				$db->lock( "search_queue" );
				$sqlstr = "INSERT INTO search_queue (id_res,id,id_topic,id_group,priority,insert_time) VALUES ($id_res,$id,$id_topic,$id_group,$priority,'$insert_time')";
				$res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
			}		
		}
	}
	
	public function IsResourceIndexed($id_resource)
	{
		return ($this->resources_to_index[$id_resource])=="1";
	}
	
	private function IndexQueueDelete($id_res,$id)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "search_queue" );
		$res[] = $db->query( "DELETE FROM search_queue WHERE id_res='$id_res' AND id='$id' " );
		Db::finish( $res, $db);
	}
	
	public function QueryStore($unescaped_query)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$isset = false;
		$searches = $session->Get(SESSION_SEARCH_VAR);
		if(is_array($searches) && in_array($unescaped_query,$searches))
		{
			$isset = true;
		}
		if(!$isset)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$user_agent = Varia::UserAgent();
			$db =& Db::globaldb();
			$query = $db->SqlQuote($unescaped_query);
			$todayTime = $db->getTodayTime();
			$conf = new Configuration();
			$delayed_inserts = $conf->Get("delayed_inserts");
			$db->begin();
			if($delayed_inserts)
			{
				$sqlstr =  "INSERT DELAYED INTO searches (search_time,query,user_agent)
					 VALUES ( '$todayTime', '$query','$user_agent' )";
			}
			else 
			{
				$db->lock( "searches" );
				$sqlstr =  "INSERT INTO searches (search_time,query,user_agent)
					 VALUES ( '$todayTime', '$query','$user_agent' )";
			}
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			if(!is_array($searches))
			{
				$searches = array();
			}
			$searches[] = $unescaped_query;
			$session->Set(SESSION_SEARCH_VAR,$searches);
		}		
	}
	
	public function Queue( &$rows )
	{
		$sqlstr = "SELECT id_res,id,id_topic,priority,UNIX_TIMESTAMP(insert_time) AS insert_time_ts 
		FROM search_queue 
		ORDER BY priority ASC,insert_time ASC";
		$db =& Db::globaldb();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Reindex($id_res,$priority,$id_topic)
	{
		$num = 0;
		if($id_res>0)
		{
			$resources_to_index = $this->resources_to_index;
			if($resources_to_index[$id_res])
			{
				$sqlstrs = $this->ReindexSql($id_res,$id_topic);
				foreach($sqlstrs AS $sql)
				{
					$rows = array();
					$db =& Db::globaldb();
					$db->QueryExe($rows, $sql);
					$db->begin();
					$insert_time = $db->getTodayTime();
					$db->lock( "search_queue" );
					foreach($rows as $row)
					{
						$sqlstr = "INSERT INTO search_queue (id_res,id,id_topic,id_group,priority,insert_time) VALUES ($id_res,'{$row['id_item']}','" . (int)$row['id_topic'] . "','" . (int)$row['id_group'] . "','$priority','$insert_time')";
						$res[] = $db->query( $sqlstr );
					}
					Db::finish( $res, $db);
					$num += count($rows);
				}
			}
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/resources.php");
			$r = new Resources();
			foreach($r->types as $id_res_type)
			{
				if($id_res_type>0)
					$num += $this->Reindex($id_res_type,$priority,$id_topic);
			}
		}
		return $num;
	}
	
	private function ReindexSql($id_res,$id_topic)
	{
		$sql = array();
		switch($id_res)
		{
			case "1":
				$sqlstr = "SELECT t.id_topic AS id_item,'0' AS id_topic,t.id_group
				FROM topics t 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE t.visible=1 ";
				if($id_topic>0)
					$sqlstr .= " AND t.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "2":
				$sqlstr = "SELECT s.id_subtopic AS id_item,s.id_topic,t.id_group
				FROM subtopics s 
				INNER JOIN topics t ON s.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE s.visible<3 ";
				if($id_topic>0)
					$sqlstr .= " AND s.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "3":
				$sqlstr = "SELECT g.id_gallery AS id_item,s.id_topic,t.id_group
				FROM galleries g 
				INNER JOIN galleries_groups gg ON g.id_group=gg.id_group AND gg.visible=1
				LEFT JOIN subtopics s ON g.id_gallery=s.id_item AND s.id_type=9 AND s.visible<3
				LEFT JOIN topics t ON s.id_topic=t.id_topic AND t.visible=1 
				LEFT JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE g.visible=1  ";
				if($id_topic>0)
					$sqlstr .= " AND s.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "4":
				$sqlstr = "SELECT a.id_ass AS id_item,'0' AS id_topic,'0' AS id_group
				FROM ass a 
				WHERE a.approved=1 ";
				$sql[] = $sqlstr;
			break;
			case "5":
				$sqlstr = "SELECT a.id_article AS id_item,a.id_topic,t.id_group
				FROM articles a
				INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic AND s.visible<3
				INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE a.approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND a.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "6":
				/* docs are indexed with articles
				$sqlstr = "SELECT d.id_doc  AS id_item,a.id_topic,t.id_group
				FROM docs_articles d
				INNER JOIN articles a ON d.id_article=a.id_article
				INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic AND s.visible<3
				INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE a.approved=1 AND a.published>0 ";
				if($id_topic>0)
					$sqlstr .= " AND a.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
				*/
			break;
			case "7":
				/* article images are indexed with articles
				$sqlstr = "SELECT i.id_image  AS id_item,a.id_topic,t.id_group
				FROM images_articles i
				INNER JOIN articles a ON i.id_article=a.id_article
				INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic AND s.visible<3
				INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE a.approved=1 AND a.published>0 ";
				if($id_topic>0)
					$sqlstr .= " AND a.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
				*/
				$sqlstr = "SELECT i.id_image  AS id_item,s.id_topic,t.id_group
				FROM images_galleries i
				INNER JOIN galleries g ON i.id_gallery=g.id_gallery AND g.visible=1
				INNER JOIN galleries_groups gg ON g.id_group=gg.id_group AND gg.visible=1
				LEFT JOIN subtopics s ON g.id_gallery=s.id_item AND s.id_type=9 AND s.visible<3
				LEFT JOIN topics t ON s.id_topic=t.id_topic AND t.visible=1 
				LEFT JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1";
				if($id_topic>0)
					$sqlstr .= " WHERE s.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "8":
				$sqlstr = "SELECT e.id_event  AS id_item,e.id_topic,e.id_group
				FROM events e
				WHERE e.approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND e.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "9": 
				$sqlstr = "SELECT f.id_topic_forum AS id_item,f.id_topic,t.id_group
				FROM topic_forums f
				INNER JOIN topics t ON f.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE f.active=1 ";
				if($id_topic>0)
					$sqlstr .= " AND f.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "10": 
				$sqlstr = "SELECT tft.id_thread AS id_item,f.id_topic,t.id_group
				FROM topic_forum_threads tft
				INNER JOIN topic_forums f ON tft.id_topic_forum=f.id_topic_forum AND f.active=1
				INNER JOIN topics t ON f.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE tft.thread_approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND f.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "11": 
				$sqlstr = "SELECT c.id_topic_campaign AS id_item,c.id_topic,t.id_group
				FROM topic_campaigns c
				INNER JOIN topics t ON c.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE c.active=1 ";
				if($id_topic>0)
					$sqlstr .= " AND c.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "12": 
				$sqlstr = "SELECT q.id_quote AS id_item,q.id_topic,t.id_group
				FROM quotes q
				INNER JOIN topics t ON q.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE q.approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND q.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "13": 
				$sqlstr = "SELECT b.id_book AS id_item,'0' AS id_topic,'0' AS id_group
				FROM books b
				WHERE b.approved=1 ";
				$sql[] = $sqlstr;
			break;
			case "14":
				$sqlstr = "SELECT l.id_link AS id_item,ls.id_topic,t.id_group
				FROM links l
				INNER JOIN links_subtopics ls ON l.id_link=ls.id_link
				INNER JOIN subtopics s ON ls.id_subtopic=s.id_subtopic AND s.visible<3
				INNER JOIN topics t ON s.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE l.approved=1 AND l.error404=0 ";
				if($id_topic>0)
					$sqlstr .= " AND ls.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "15":
				$sqlstr = "SELECT e.id_event  AS id_item,'0' AS id_topic,'0' AS id_group
				FROM recurring_events e
				WHERE e.approved=1 ";
				$sql[] = $sqlstr;
			break;
			case "16":
			case "17":
			break;
			case "18": 
				$sqlstr = "SELECT r.id_review AS id_item,r.id_topic,t.id_group
				FROM reviews r
				INNER JOIN topics t ON r.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE r.approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND r.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "19": 
				$sqlstr = "SELECT p.id_poll AS id_item,p.id_topic,t.id_group
				FROM polls p
				INNER JOIN topics t ON p.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE p.is_private=0 ";
				if($id_topic>0)
					$sqlstr .= " AND p.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "20": 
				$sqlstr = "SELECT q.id_question AS id_item,p.id_topic,t.id_group
				FROM poll_questions q
				INNER JOIN polls p ON q.id_poll=p.id_poll AND p.is_private=0
				INNER JOIN topics t ON p.id_topic=t.id_topic AND t.visible=1 
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE q.approved=1 ";
				if($id_topic>0)
					$sqlstr .= " AND p.id_topic='$id_topic' ";
				$sql[] = $sqlstr;
			break;
			case "22": 
			break;
			case "23":
				$sqlstr = "SELECT v.id_video AS id_item,'0' AS id_topic,'0' AS id_group
				FROM videos v 
				WHERE v.approved=1 AND v.encoded=1 ";
				$sql[] = $sqlstr;
			break;
			case "24":
				$sqlstr = "SELECT id_widget AS id_item,'0' AS id_topic,'0' AS id_group
				FROM widgets
				WHERE status=1 ";
				$sql[] = $sqlstr;
			break;
			case "25":
				$sqlstr = "SELECT id_web_feed_item AS id_item,'0' AS id_topic,'0' AS id_group
				FROM web_feed_items
				WHERE is_approved=1 AND is_filtered=0 AND is_expired=0 AND status=1 ";
				$sql[] = $sqlstr;
			break;
		}
		return $sql;
	}
	
	private function ResourceDelete($id_res,$id)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "search_index" );
		$res[] = $db->query( "DELETE FROM search_index WHERE id_res='$id_res' AND id='$id' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "search_index2" );
		$res[] = $db->query( "DELETE FROM search_index2 WHERE id_res='$id_res' AND id='$id' " );
		Db::finish( $res, $db);
	}
	
	private function ResourceDeleteAll($id_res)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "search_index" );
		$res[] = $db->query( "DELETE FROM search_index WHERE id_res='$id_res' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "search_index2" );
		$res[] = $db->query( "DELETE FROM search_index2 WHERE id_res='$id_res' " );
		Db::finish( $res, $db);
	}
	
	public function ResourceRemove($id_res,$id)
	{
		$this->ResourceDelete($id_res,$id);
		$this->IndexQueueDelete($id_res,$id);
	}
	
	private function ResourceIndex($id_res,$id,$id_topic,$id_group)
	{
		$this->ResourceDelete($id_res,$id);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$keywords = array();
		$keywords_related = array();
		$o->GetKeywords($id,$id_res,$keywords);
		$last_pos = 0;
		$notes = "";
		$id_language = $this->id_language;
		$id_template = 0;
		switch($id_res)
		{
			case "1": //topic
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id);
				$id_language = $t->id_language;
				$title = $t->name . "\n" . $t->description;
				$home_header = $t->TextGet("home_header");
				$home_footer = $t->TextGet("home_footer");
				$content = $home_header['content'] . SEPARATOR . $home_footer['content'];
			break;
			case "2": //subtopic
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$id_language = $t->id_language;
				$subtopic = $t->SubtopicGet($id);
				$title = $subtopic['name'] .  SEPARATOR . $subtopic['description'];
				$content = $subtopic['header'] .  SEPARATOR . $subtopic['footer'];
				$o->GetKeywords($id_topic,$o->types['topic'],$keywords);
			break;
			case "3": //gallery
				include_once(SERVER_ROOT."/../classes/gallery.php");
				$g = new Gallery($id);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($g->id_topic);
				$id_language = $t->id_language;
				$title = $g->title .  SEPARATOR . $g->row['description'];
				$content = $g->author .  SEPARATOR . $g->row['note'];
				$o->GetKeywords($g->id_topic,$o->types['topic'],$keywords);
			break;
			case "4": //association
				include_once(SERVER_ROOT."/../modules/assos.php");
				include_once(SERVER_ROOT."/../classes/topic.php");
				$asso = new Asso($id);
				$as = new Assos();
				$t = new Topic($as->id_topic);
				$id_language = $t->id_language;
				$org = $asso->Get(true);
				$title = $org['nome'] . SEPARATOR . $org['nome2'];
				$content = $org['ass_tipo'] . SEPARATOR . $org['referente'];
				$notes = $org['note'];
				$v = new Varia();
				$params = $v->Deserialize($org['kparams']);
				if(is_array($params) && count($params)>0)
				{
					include_once(SERVER_ROOT."/../classes/varia.php");
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$kparams = $k->Params(0,5,true,true);
					foreach($kparams as $kparam)
					{
						$pkey = "kp_" . $kparam['id_keyword_param'];
						if(array_key_exists($pkey,$params))
						{
							$kvalue = $k->ParamValue($kparam['type'],$kparam['label'],$params[$pkey]);
							if($kvalue!="")
								$content .= SEPARATOR . $kvalue;
						}
					}
					$keywords = $asso->Keywords();
					foreach($keywords as $keyword)
					{
						$kparams = $k->Params($keyword['id_keyword'],5,true,true);
						foreach($kparams as $kparam)
						{
							$pkey = "kp_" . $kparam['id_keyword_param'];
							if(is_array($params) && array_key_exists($pkey,$params))
							{
								$kvalue = $k->ParamValue($kparam['type'],$kparam['label'],$params[$pkey]);
								if($kvalue!="")
									$content .= SEPARATOR . $kvalue;
							}
						}
					}
				}
			break;
			case "5": //article
				include_once(SERVER_ROOT."/../classes/article.php");
				include_once(SERVER_ROOT."/../classes/user.php");
				$a = new Article($id);
				$article = $a->ArticleGet();
				$id_language = $article['id_language'];
				$id_template = (int)$article['id_template'];
				$title = $article['halftitle'] . SEPARATOR . $article['headline'] . SEPARATOR . $article['subhead'];
				if($article['show_author'])
				{
					if($article['author']!="")
						$author = $article['author'];
					else 
					{
						$u = new User;
						$u->id = $article['id_user'];
						$user = $u->UserGet();
						$author = $user['name'];
					}
					$title .= SEPARATOR . $author;
				}
				$translator = $a->Translator();
				if(isset($translator['id_translation']) && $translator['id_translation']>0) {
				    if($translator['translator']!='') {
				        $translator_name = $translator['translator'];
				    } else {
				        $u = new User;
				        $u->id = $translator['id_user'];
				        $tuser = $u->UserGet();
				        $translator_name = $tuser['name'];
				    }
				    $title .= SEPARATOR . $translator_name;
				}
				$content = $article['content'];
				$notes = $article['notes'];
				$boxes = $a->BoxGetAll();
				foreach($boxes as $box)
				{
					if($box['show_title'])
						$content .= SEPARATOR . $box['title'];
					$notes .= SEPARATOR . $box['content'] . SEPARATOR . $box['notes'];
					$o->GetKeywords($box['id_box'],$o->types['box'],$keywords_related);
				}
				$docs= $a->DocGetAll();
				foreach($docs as $doc)
				{
					$content .= SEPARATOR . $doc['title'];
					$notes .= SEPARATOR . $doc['author'] . SEPARATOR . $doc['description'];
					$file_content = $this->ResourceFileIndex("uploads/docs/{$doc['id_doc']}.{$doc['format']}",$doc['format']);
					if($file_content!="")
					{
						$notes .= SEPARATOR . $file_content;
					}
					$o->GetKeywords($doc['id_doc'],$o->types['document'],$keywords_related);
				}
				$images = $a->ImageGetAll();
				foreach($images as $image)
				{
					$content .= SEPARATOR . $image['caption'];
					$notes .= SEPARATOR . $image['source'];
					$o->GetKeywords($image['id_image'],$o->types['image'],$keywords_related);
				}
				$o->GetKeywords($article['id_subtopic'],$o->types['subtopic'],$keywords_related);
				$o->GetKeywords($article['id_topic'],$o->types['topic'],$keywords_related);
			break;
			case "7": //image
				include_once(SERVER_ROOT."/../classes/image.php");
				$im = new Image($id);
				$image = $im->Get();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$id_language = $t->id_language;
				$title = "";
				$content = $image['author'] . SEPARATOR . $image['caption'];
				$notes = $image['source'];
			break;
			case "8": //event
				include_once(SERVER_ROOT."/../classes/event.php");
				$e = new Event();
				$event = $e->EventGet($id);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($event['id_topic']);
				$id_language = $t->id_language;
				$title = $event['title'];
				$content = $event['type'] . SEPARATOR . $event['description'];
				$notes = $event['place'] . SEPARATOR . $event['place_details'] . SEPARATOR . $event['address'];
				if($event['id_geo']>0) {
					include_once(SERVER_ROOT."/../classes/geo.php");
					$g = new Geo();
					$geo = $g->GeoGet($event['id_geo'], 0);
					$notes .= SEPARATOR . $geo;
				}
				if($event['id_article']>0)
				{
					$o->GetKeywords($event['id_article'],$o->types['article'],$keywords);
					$title .= SEPARATOR . $event['headline'];
				}
			break;
			case "9": //forum
				include_once(SERVER_ROOT."/../classes/forum.php");
				$f = new Forum($id);
				$forum = $f->ForumGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($forum['id_topic']);
				$id_language = $t->id_language;
				$title = $forum['name'];
				$content = $forum['description'];
				$o->GetKeywords($forum['id_topic'],$o->types['topic'],$keywords);
			break;
			case "10": //forum thread
				include_once(SERVER_ROOT."/../classes/forum.php");
				$f = new Forum(0);
				$thread = $f->ThreadGet($id);
				$f->id = $thread['id_topic_forum'];
				$forum = $f->ForumGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($forum['id_topic']);
				$id_language = $t->id_language;
				$title = $thread['name'];
				$content = $thread['description'] . SEPARATOR . $thread['description_long'];
				$notes = $thread['source'];
				$o->GetKeywords($forum['id_topic_forum'],$o->types['forum'],$keywords);
			break;
			case "11": //campaign
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($id);
				$campaign = $c->CampaignGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($campaign['id_topic']);
				$id_language = $t->id_language;
				$title = $campaign['name'];
				$content = $campaign['promoter'] . SEPARATOR . $campaign['description'] . SEPARATOR . $campaign['description_long'];
				$o->GetKeywords($campaign['id_topic'],$o->types['topic'],$keywords);
			break;
			case "12": //quote
				include_once(SERVER_ROOT."/../classes/quotes.php");
				$q = new Quotes();
				$quote = $q->QuoteGet($id);
				$id_language = $quote['id_language'];
				$title = "";
				$content = $quote['quote'] . SEPARATOR . $quote['author'];
				$notes = $quote['notes'];
			break;
			case "13": //book
				include_once(SERVER_ROOT."/../modules/books.php");
				$b = new Book($id);
				$book = $b->BookGet();
				$title = $book['title'] . SEPARATOR . $book['summary'];
				$content = $book['description'] . SEPARATOR . $book['author'];
				$notes = $book['notes'];
			break;
			case "14": //link
				include_once(SERVER_ROOT."/../classes/link.php");
				$l = new Link($id);
				$link = $l->LinkGet();
				$title = $link['title'];
				$content = $link['description'];
			break;
			case "15": //recurring_event
				include_once(SERVER_ROOT."/../classes/event.php");
				$e = new Event();
				$event = $e->RecurringGet($id);
				$title = "";
				$content = $event['description'];
			break;
			case "18": //review
				include_once(SERVER_ROOT."/../modules/books.php");
				$b = new Book(0);
				$review = $b->ReviewGet($id);
				$title = "";
				$content = $review['review'];
			break;
			case "19": //poll
				include_once(SERVER_ROOT."/../classes/polls.php");
				$pl = new Polls();
				$poll = $pl->PollGet($id);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($poll['id_topic']);
				$id_language = $t->id_language;
				$title = $poll['name'];
				$content = $poll['description'] . SEPARATOR . $poll['intro'];
				$o->GetKeywords($poll['id_topic'],$o->types['topic'],$keywords);
			break;
			case "20": //poll question
				include_once(SERVER_ROOT."/../classes/polls.php");
				$pl = new Polls();
				$question = $pl->QuestionGet($id);
				$poll = $pl->PollGet($question['id_poll']);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($poll['id_topic']);
				$id_language = $t->id_language;
				$title = $question['question'];
				$content = $question['description'] . SEPARATOR . $question['description_long'];
				$o->GetKeywords($question['id_poll'],$o->types['poll'],$keywords);
			break;
			case "23": //video
				include_once(SERVER_ROOT."/../classes/video.php");
				$vi = new Video();
				$video = $vi->VideoGet($id);
				$title = $video['title'] . SEPARATOR . $video['author'];
				$content = $video['source'] . SEPARATOR . $video['description'];
				$id_language = $video['id_language'];
			break;
			case "24":
				include_once(SERVER_ROOT."/../classes/widgets.php");
				$wi = new Widgets();
				$widget = $wi->WidgetGet($id);
				$title = $widget['title'];
				$content = $widget['description'];
                $o->GetKeywords($widget['id_widget'],$o->types['widget'],$keywords);
                /*
				if($widget['widget_type']==WIDGET_TYPE_MANUAL)
				{
					$widget_params = $wi->WidgetParamGet($id);
			        include_once(SERVER_ROOT."/../classes/varia.php"); 
			        $v = new Varia; 
			        $params = $v->Deserialize($widget_params['params']);
					$content .= SEPARATOR . $params['content'];
				}
				if($widget['widget_type']==WIDGET_TYPE_RSS)
				{
					$widget_params = $wi->WidgetParamGet($id);
			        include_once(SERVER_ROOT."/../classes/varia.php"); 
			        $v = new Varia; 
					$params = $v->Deserialize($widget_params['params']);
					$id_web_feed = (int)$params['id_web_feed'];
					if($id_web_feed>0)
					{
						include_once(SERVER_ROOT."/../classes/web_feeds.php"); 
						$wf = new WebFeeds();
						// retrieve all current availabel feed items
						// add their title to $title
						// add their description to $content
					}
				}
                */
			break;
            case "25":
                include_once(SERVER_ROOT."/../classes/web_feeds.php");
                $wf = new WebFeeds();
                $web_feeds = $wf->WebFeedItemGetById($id);
                $title = $web_feeds['title'];
                $content = $web_feeds['description'];
            break;
        }
		if($id_template>0)
		{
			include_once(SERVER_ROOT."/../classes/template.php");
			$te = new Template();
			$te->ResourceSet($id_res);
			$template_fields = $te->ResourceValues($id_template,$id,true,true);
			foreach($template_fields as $field)
			{
				$content .= SEPARATOR . $field['value'];
			}
		}
		$last_pos = $this->IndexContent($id_res,$id,$title,$this->relevance['title'],$id_language,$last_pos);
		$last_pos = $this->IndexContent($id_res,$id,$content,$this->relevance['content'],$id_language,$last_pos);
		$last_pos = $this->IndexContent($id_res,$id,$notes,$this->relevance['notes'],$id_language,$last_pos);
		$res_keywords = array();
		foreach($keywords as $keyword)
		{
			if(!in_array($keyword['keyword'],$res_keywords))
				$res_keywords[] = $this->TextClean($keyword['keyword'],$id_language);
			$neighbours = $o->KeywordNeighbours($keyword['id_keyword'],true);
			foreach($neighbours as $neighbour)
			{
				if(!in_array($neighbour,$res_keywords))
				    $res_keywords[] = $this->TextClean($neighbour,$id_language);
			}
		}
		$res_keywords_related = array();
		foreach($keywords_related as $keyword_related)
		{
		    if(!in_array($keyword_related['keyword'],$res_keywords) && !in_array($keyword_related['keyword'],$res_keywords_related))
			    $res_keywords_related[] = $this->TextClean($keyword_related['keyword'],$id_language);
		}
		$last_pos = $this->IndexContentArray($id_res,$id,$res_keywords,$this->relevance['keyword'],$last_pos);
		$this->IndexContentArray($id_res,$id,$res_keywords_related,$this->relevance['keyword_related'],$last_pos);
		$this->ResourceIndexSummary($id_res,$id,$id_topic,$id_group);
		$this->IndexQueueDelete($id_res,$id);
	}
	
	private function ResourceFileIndex($filename,$format)
	{
		$text = "";
		switch(strtolower($format))
		{
			case "txt":
				include_once(SERVER_ROOT."/../classes/file.php");
				$fm = new FileManager();
				$text = $fm->TextFileRead($filename);
			break;
			case "pdf":
				if($this->index_pdf)
				{
					include_once(SERVER_ROOT."/../classes/file.php");
					include_once(SERVER_ROOT."/../classes/varia.php");
					$fm = new FileManager();
					$v = new Varia();
					$pdffile = $fm->RealPath($filename);
					$textfile = "cache/" . $v->Uid() . ".txt";
					$ret = $v->ExecEscaped("pdftotext",array($pdffile,$fm->RealPath($textfile)));
					if($v->return_status==0)
					{
						$text = $fm->TextFileRead($textfile);
						$fm->Delete($textfile);
					}
				}
			break;
			case "doc":
				if($this->index_doc)
				{
					include_once(SERVER_ROOT."/../classes/file.php");
					include_once(SERVER_ROOT."/../classes/varia.php");
					$fm = new FileManager();
					$v = new Varia();
					$docfile = $fm->RealPath($filename);
					$textfile = "cache/" . $v->Uid() . ".txt";
					$ret = $v->Exec("antiword $docfile > " . $fm->RealPath($textfile));
					if($v->return_status==0)
					{
						$text = $fm->TextFileRead($textfile);
					}
					$fm->Delete($textfile);
				}
			break;
		}
		return $text;
	}
	
	private function ResourceIndexSummary($id_res,$id,$id_topic,$id_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "search_index2" );
		$res[] = $db->query( "DELETE FROM search_index2 WHERE id_res='$id_res' AND id='$id' " );
		Db::finish( $res, $db);
		$rows = array();
		$sqlstr = "	SELECT id_word,SUM(relevance) AS score,COUNT(id_word) AS matches 
			FROM search_index 
			WHERE id_res='$id_res' AND id='$id' GROUP BY id_word";
		$db->QueryExe($rows, $sqlstr);
		if(count($rows)>0)
		{
			$db->begin();
			$db->lock( "search_index2" );
			foreach($rows as $row)
			{
				$sqlstr = "INSERT INTO search_index2 (id_res,id,id_word,score,matches,id_topic,id_group) 
					VALUES ($id_res,$id,'{$row['id_word']}','{$row['score']}','{$row['matches']}',$id_topic,$id_group) ON DUPLICATE KEY UPDATE score='{$row['score']}', matches='{$row['matches']}',id_topic=$id_topic,id_group=$id_group";
				$res[] = $db->query( $sqlstr );
			}
			Db::finish( $res, $db);
		}
	}
	
	public function Stats(&$rows,$id_stat,$params=array())
	{
		$num = 0;
		$headers = array();
		$content = array();
		switch($id_stat)
		{
			case "1": //generic
				$sqlstr = "SELECT si.id_res,COUNT(DISTINCT si.id) AS items FROM search_index2 si GROUP BY si.id_res ORDER BY items DESC";
				$headers = array('resource','items');
				$content = array('{ResourceLookup($row[id_res])}','{LinkTitle("search_stats.php?id=2&id_res=$row[id_res]","$row[items]",0,"",true,"right")}');
			break;
			case "2": //resource
				$sqlstr = "SELECT si.id,COUNT(si.id_word) AS words,si.id_res FROM search_index si WHERE si.id_res='{$params['id_res']}' GROUP BY si.id ORDER BY words DESC";
				$headers = array('id','words');
				$content = array('{LinkTitle("search_stats.php?id=3&id_res=$row[id_res]&id_item=$row[id]",$row[id])}','<div class=\"right\">$row[words]</div>');
			break;
			case "3": //item
				$sqlstr = "SELECT si.id_word,sw.word,si.relevance FROM search_index si INNER JOIN search_words sw ON si.id_word=sw.id_word WHERE  si.id_res='{$params['id_res']}' AND si.id='{$params['id_item']}' ORDER BY relevance DESC, word_pos ASC";
				$headers = array('id_word','word','weight');
				$content = array('$row[id_word]','$row[word]','$row[relevance]');
			break;
			case "4": //search word
				$id_word = $this->WordLookup($params['q']);
				$sqlstr = "SELECT si.id_res,si.id AS id_item,COUNT(si.id) AS counter,SUM(si.relevance) AS score,si.id_word FROM search_index si WHERE si.id_word='$id_word' GROUP BY si.id_res,si.id ORDER BY score DESC,si.id DESC";
				$headers = array('resource','id','counter','score');
				$content = array('{ResourceLookup($row[id_res])}','{LinkTitle("search_stats.php?id=5&id_res=$row[id_res]&id_item=$row[id_item]&id_word=$row[id_word]","$row[id_item]",0,"",true,"right")}','<div class=\"right\">$row[counter]</div>','<div class=\"right\">$row[score]</div>');
			break;
			case "5": //search word details
				$sqlstr = "SELECT si.id_res,si.id AS id_item,sw.word,si.word_pos,si.relevance FROM search_index si INNER JOIN search_words sw ON si.id_word=sw.id_word WHERE si.id_res='{$params['id_res']}' AND si.id='{$params['id_item']}' AND si.id_word='{$params['id_word']}' ORDER BY si.word_pos";
				$headers = array('resource','id','word','position','type','relevance');
				$content = array('{ResourceLookup($row[id_res])}','<div class=\"right\">$row[id_item]</div>','$row[word]','<div class=\"right\">$row[word_pos]</div>','{IndexWeightLookup($row[relevance])}','<div class=\"right\">$row[relevance]</div>');
			break;
		}
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, true);
		return array('num'=>$num,'headers'=>$headers,'content'=>$content);
	}
	
	public function StopWords($id_language)
	{
		$lang = $this->languages[$id_language];
		$var = $this->mem_var_base . "_$lang";
		$stop_words = array();
		if(!$this->mem->IsVarSet($var))
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$stop_words = $fm->TextFileLines("lang/$lang/stopwords.txt");
			array_walk($stop_words,array($this,"StopWordsTrim"));
			$stop_words = array_filter($stop_words);
			$this->mem->Set($var,$stop_words);
		}
		else 
			$stop_words = $this->mem->Get($var);
		return $stop_words;
	}
	
	private function StopWordsTrim(&$value)
	{
		$value = trim($value);
	}
	
	public function Suggestions($word,$id_res)
	{
		$metaphone = metaphone($word);
		$alternative_words = array();
		$sqlstr = "SELECT sw.id_word,sw.word,si.id_topic,COUNT(si.id) AS counter 
			FROM search_words sw 
			INNER JOIN search_index2 si ON si.id_res=$id_res AND si.id_word=sw.id_word 
			WHERE metaphone='$metaphone' AND word!='$word' GROUP BY sw.id_word ORDER BY counter DESC ";
		$db =& Db::globaldb();
		$db->QueryExe($alternative_words, $sqlstr);
		return $alternative_words;
	}
	
	private function TextClean($text,$id_language)
	{
		$text = $this->th->RemoveAccents($text,$id_language);
		$text = $this->th->UtfClean($text);
		return $text;
	}
	
	public function TopSearches( &$rows )
	{
		$sqlstr = "SELECT query,COUNT(*) AS counter FROM searches GROUP BY QUERY ORDER BY counter DESC";
		$db =& Db::globaldb();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function TopicRemove($id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "search_index2" );
		$res[] = $db->query( "DELETE FROM search_index2 WHERE id_topic='$id_topic' " );
		Db::finish( $res, $db);
	}
	
	private function WordLookup($word)
	{
		$word = $this->th->MakeLowerCase($word);
		if(array_key_exists($word,$this->word_cache))
		{
			$id_word = $this->word_cache[$word];
		}
		else 
		{
			$db =& Db::globaldb();
			$sword = $db->SqlQuote($word);
			$row = array();
			$db->query_single($row, "SELECT id_word FROM search_words WHERE word='$sword' ");
			$id_word = (int)$row['id_word'];
			if(!$id_word>0)
			{
				$db->begin();
				$db->lock( "search_words" );
				$id_word = $db->nextId( "search_words", "id_word" );
				$metaphone = $db->SqlQuote(metaphone($word));
				$sqlstr = "INSERT INTO search_words (id_word,word,metaphone) VALUES ($id_word,'$sword','$metaphone')";
				$res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
			}
			$this->word_cache[$word] = $id_word;
		}
		return $id_word;
	}
}
?>
