<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/link.php");

class Links
{
	public $verbose = false;

	public function Check($id_topic)
	{
		$errors = 0;
		$links = $this->Topic($id_topic);
		foreach($links as $link)
		{
			$l = new Link($link['id_link']);
			$l->url = $link['url'];
			$error = $l->Check();
			if ($l->error==1 && $link['error404']==1)
			{
				$errors ++;
				$l->Set404();
				$msg .= "<a href=\"/topics/link.php?id={$link['id_link']}&id_topic=$id_topic&error=1\">$l->url</a>: <b><a href=\"{$l->url}\">$error</a></b><br>\n";
			}
			else 
				$msg .= "$l->url: $error<br>\n";
			if (!($l->error==1) && $link['error404']==1)
				$l->Set404(0);
		}
		if ($errors>0 && !($this->verbose))
			$this->WarnAdmins($id_topic, $errors);
		if ($this->verbose)
			return $msg;
		else
			return $errors;
	}

	public function CountApproved()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT COUNT(id_link) AS count_links FROM links WHERE approved=1");
		return $row['count_links'];
	}

	public function CountTopicApproved($id_topic,$approved)
	{
		$row = array();
		return $this->TopicApprove( $row,$id_topic,$approved,0 );
	}

	public function CountTopicError($id_topic)
	{
		$row = array();
		return $this->TopicApprove( $row,$id_topic,1,1 );
	}

	public function Search( &$rows, $params )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT l.id_link,url,title,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,ls.id_subtopic,ls.id_topic
		FROM links l
		INNER JOIN links_subtopics ls USING(id_link)
		WHERE id_topic=$params[id_topic]";
		if (strlen($params['title']) > 0)
			$sqlstr .= " AND (title LIKE '%{$params['title']}%' OR description LIKE '%{$params['title']}%') ";
		if (strlen($params['url']) > 0)
			$sqlstr .= " AND url LIKE '%{$params['url']}%' ";
		if ($params['id_subtopic'] > 0)
			$sqlstr .= " AND id_subtopic={$params['id_subtopic']} ";
		$sqlstr .= "  ORDER BY title";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Topic( $id_topic )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT links.id_link,url,error404 FROM links INNER JOIN links_subtopics USING(id_link)
			WHERE links_subtopics.id_topic=$id_topic AND approved=1 ORDER BY insert_date DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicApprove( &$rows, $id_topic, $approved, $error404 )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT links.id_link,url,title,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,ls.id_subtopic
		FROM links
		INNER JOIN links_subtopics ls USING(id_link)
		WHERE id_topic=$id_topic AND approved=$approved AND error404=$error404 ORDER BY insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	private function WarnAdmins( $id_topic, $errors)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$admins = $t->Contacts();
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($t->id_language,0);
		include_once(SERVER_ROOT."/../classes/mail.php");
		$m = new Mail();
		$subject = $tr->TranslateParams("links_errors",array($t->name,$errors));
		$body = $tr->TranslateParams("links_errors_desc",array($errors,$t->name,$id_topic));
		foreach($admins as $admin)
			$m->SendMail($admin['email'],$admin['name'],$subject,$body,array());
	}
}
?>
