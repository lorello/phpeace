<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class Geo
{
	public $geo_location;

    var $GEOIP_COUNTRY_CODE_TO_ID = array(
"" => 0, "AP" => 0, "EU" => 0, "AD" => 56, "AE" => 189, "AF" => 54, 
"AG" => 57, "AI" => 0, "AL" => 55, "AM" => 59, "AN" => 0, "AO" => 2, 
"AQ" => 0, "AR" => 58, "AS" => 0, "AT" => 61, "AU" => 60, "AW" => 0, 
"AZ" => 62, "BA" => 72, "BB" => 66, "BD" => 65, "BE" => 68, "BF" => 5, 
"BG" => 75, "BH" => 64, "BI" => 6, "BJ" => 3, "BM" => 0, "BN" => 74, 
"BO" => 71, "BR" => 73, "BS" => 63, "BT" => 70, "BV" => 0, "BW" => 4, 
"BY" => 67, "BZ" => 69, "CA" => 77, "CC" => 0, "CD" => 12, "CF" => 9, 
"CG" => 11, "CH" => 178, "CI" => 23, "CK" => 0, "CL" => 78, "CM" => 7, 
"CN" => 79, "CO" => 80, "CR" => 82, "CU" => 84, "CV" => 8, "CX" => 0, 
"CY" => 85, "CZ" => 86, "DE" => 98, "DJ" => 13, "DK" => 87, "DM" => 88, 
"DO" => 89, "DZ" => 1, "EC" => 91, "EE" => 93, "EG" => 14, "EH" => 0, 
"ER" => 16, "ES" => 174, "ET" => 17, "FI" => 95, "FJ" => 94, "FK" => 0, 
"FM" => 135, "FO" => 0, "FR" => 96, "FX" => 0, "GA" => 18, "GB" => 190,
"GD" => 100, "GE" => 97, "GF" => 0, "GH" => 20, "GI" => 0, "GL" => 0, 
"GM" => 19, "GN" => 22, "GP" => 0, "GQ" => 15, "GR" => 99, "GS" => 0, 
"GT" => 101, "GU" => 0, "GW" => 21, "GY" => 102, "HK" => 0, "HM" => 0, 
"HN" => 104, "HR" => 83, "HT" => 103, "HU" => 105, "ID" => 108, "IE" => 111, 
"IL" => 112, "IN" => 107, "IO" => 0, "IQ" => 110, "IR" => 109, "IS" => 106, 
"IT" => 113, "JM" => 114, "JO" => 116, "JP" => 115, "KE" => 24, "KG" => 122, 
"KH" => 76, "KI" => 118, "KM" => 81, "KN" => 162, "KP" => 119, "KR" => 120, 
"KW" => 121, "KY" => 0, "KZ" => 117, "LA" => 123, "LB" => 125, "LC" => 163, 
"LI" => 126, "LK" => 175, "LR" => 26, "LS" => 25, "LT" => 127, "LU" => 128, 
"LV" => 124, "LY" => 27, "MA" => 33, "MC" => 137, "MD" => 136, "MG" => 28, 
"MH" => 133, "MK" => 129, "ML" => 30, "MM" => 140, "MN" => 138, "MO" => 0, 
"MP" => 0, "MQ" => 0, "MR" => 31, "MS" => 0, "MT" => 132, "MU" => 32, 
"MV" => 131, "MW" => 29, "MX" => 134, "MY" => 130, "MZ" => 34, "NA" => 35,
"NC" => 0, "NE" => 36, "NF" => 0, "NG" => 37, "NI" => 145, "NL" => 143, 
"NO" => 146, "NP" => 142, "NR" => 141, "NU" => 0, "NZ" => 144, "OM" => 147, 
"PA" => 151, "PE" => 154, "PF" => 0, "PG" => 152, "PH" => 155, "PK" => 148, 
"PL" => 156, "PM" => 0, "PN" => 0, "PR" => 158, "PS" => 150, "PT" => 157, 
"PW" => 149, "PY" => 153, "QA" => 159, "RE" => 38, "RO" => 160, "RU" => 161, 
"RW" => 39, "SA" => 168, "SB" => 173, "SC" => 41, "SD" => 45, "SE" => 177, 
"SG" => 170, "SH" => 0, "SI" => 172, "SJ" => 0, "SK" => 171, "SL" => 42, 
"SM" => 166, "SN" => 40, "SO" => 43, "SR" => 176, "ST" => 167, "SV" => 92, 
"SY" => 179, "SZ" => 46, "TC" => 0, "TD" => 10, "TF" => 0, "TG" => 48, 
"TH" => 182, "TJ" => 181, "TK" => 0, "TM" => 186, "TN" => 49, "TO" => 183, 
"TL" => 90, "TR" => 185, "TT" => 184, "TV" => 187, "TW" => 180, "TZ" => 47, 
"UA" => 188, "UG" => 50, "UM" => 0, "US" => 191, "UY" => 192, "UZ" => 193, 
"VA" => 195, "VC" => 164, "VE" => 196, "VG" => 0, "VI" => 0, "VN" => 197,
"VU" => 194, "WF" => 0, "WS" => 165, "YE" => 198, "YT" => 0, "RS" => 169, 
"ZA" => 44, "ZM" => 51, "ME" => 139, "ZW" => 53, "A1" => 0, "A2" => 0, 
"O1" => 0, "AX" => 0, "GG" => 0, "IM" => 0, "JE" => 0, "BL" => 0,
"MF" => 0
);
	
	
	function __construct()
	{
		$ini = new Ini;
		$this->geo_location = $ini->Get('geo_location');
	}

	public function Countries( $id_continent )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_country,name FROM geo_countries ";
		if($id_continent>0)
			$sqlstr .= " WHERE id_continent=$id_continent ";
		$sqlstr .= " ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function CountryGet( $id_country )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name FROM geo_countries WHERE id_country=$id_country";
		$db->query_single($row,$sqlstr);
		return $row['name'];
	}
	
	public function CountryLookup($country)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_country FROM geo_countries WHERE name='$country' ";
		$db->query_single($row,$sqlstr);
		return (int)$row['id_country'];
	}

	public function GeoGet($id_geo,$geo_location)
	{
		if(!$geo_location>0)
			$geo_location = $this->geo_location;
		switch($geo_location)
		{
			case "1";
				$name = $this->ProvinceGet($id_geo);
			break;
			case "2";
			case "6";
			case "7";
				$name = $this->CountryGet($id_geo);
			break;
			case "3";
				$name = $this->UkCounty($id_geo);
			break;
			case "4";
				$name = $this->EireCounty($id_geo);
			break;
			case "5";
				$name = $this->RegionGet($id_geo);
			break;
		}
		return $name;
	}
	
	public function GeoJoin($geo_field,$inner=false)
	{
		$join = array();
		switch ($this->geo_location)
		{
			case "1";
				$join = array('name'=>"pr.pr",'join'=>($inner?"INNER":"LEFT") . " JOIN prov pr ON $geo_field=pr.id_prov");
			break;
			case "2";
			case "6";
			case "7";
				$join = array('name'=>"gc.name",'join'=>"LEFT JOIN geo_countries gc ON $geo_field=gc.id_country");
			break;
			case "3";
				$join = array('name'=>"uc.name",'join'=>"LEFT JOIN uk_counties uc ON $geo_field=uc.id_county");
			break;
			case "4";
				$join = array('name'=>"ec.name",'join'=>"LEFT JOIN eire_counties ec ON $geo_field=ec.id_county");
			break;
			case "5";
				$join = array('name'=>"re.reg",'join'=>"LEFT JOIN reg re ON $geo_field=re.id_reg");
			break;
		}
		return $join;
	}
	
	public function EireCounties()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_county,name FROM eire_counties ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function EireCounty( $id_county )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name FROM eire_counties WHERE id_county=$id_county ";
		$db->query_single($row,$sqlstr);
		return $row['name'];
	}
	
	public function IPLocalize($ip)
	{
        $country = array();
        $code_ids = $this->GEOIP_COUNTRY_CODE_TO_ID;
		include_once(SERVER_ROOT."/../others/vendor/autoload.php");
		$reader = new GeoIp2\Database\Reader(SERVER_ROOT.'/../custom/GeoLite2-Country.mmdb');
       	$record = $reader->country($ip);
       	$country['code'] = $record->country->isoCode;
       	$country['name'] = $record->country->name;
       	$country['id_geo'] = (int)$code_ids[$country['code']]; 
        return $country;
	}
	
	public function Label()
	{
		switch($this->geo_location)
		{
			case "1";
				$label = "province";
			break;
			case "2";
			case "6";
			case "7";
				$label = "country";
			break;
			case "3";
			case "4";
				$label = "county";
			break;
			case "5";
				$label = "region";
			break;
			default;
				$label = "country";
		}
		return $label;
	}

	public function ProvinceGet( $id_province )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT prov FROM prov WHERE id_prov='$id_province' ";
		$db->query_single($row,$sqlstr);
		return $row['prov'];
	}

	public function ProvinceGetAll( $id_province )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT prov,pr FROM prov WHERE id_prov='$id_province' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ProvinceGetByProv( $prov )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_prov,prov FROM prov WHERE pr='$prov' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ProvinceGetByProvince( $prov )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_prov,prov FROM prov WHERE prov='$prov' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function Provinces()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_prov,prov,pr,id_reg FROM prov ORDER BY it DESC,prov ASC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ProvincesByRegion($id_reg)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_prov,prov,pr FROM prov WHERE id_reg='$id_reg' ORDER BY it DESC,prov ASC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function RedirectDefaultSet($id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "homepage_countries" );
		$sqlstr = "REPLACE INTO homepage_countries (id_topic,id_country)	VALUES ('$id_topic',0) ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function RegionByProvince( $id_province )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_reg FROM prov WHERE id_prov='$id_province' ";
		$db->query_single($row,$sqlstr);
		return (int)$row['id_reg'];
	}

	public function RegionGet( $id_region )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT reg FROM reg WHERE id_reg=$id_region";
		$db->query_single($row,$sqlstr);
		return $row['reg'];
	}

	public function RegionGetByREg( $reg )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_reg,reg FROM reg WHERE reg='$reg' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function Regions()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_reg,reg from reg order by sort";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TopicByCountry($id_country,$fallback_default=false)
	{
		$sqlstr = "SELECT id_topic FROM homepage_countries WHERE id_country='$id_country' ";
		if($fallback_default)
			$sqlstr .= " OR id_country=0 ORDER BY id_country DESC LIMIT 1";
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,$sqlstr);
		return $row['id_topic'];
	}

	public function TopicCountries()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT gc.id_country,gc.name,hc.id_topic
			FROM geo_countries gc 
			LEFT JOIN homepage_countries hc ON gc.id_country=hc.id_country
			ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TopicCountriesRules()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT hc.id_topic,t.name,GROUP_CONCAT(c.name) AS countries 
			FROM homepage_countries hc 
			LEFT JOIN geo_countries c ON hc.id_country=c.id_country 
			INNER JOIN topics t ON hc.id_topic=t.id_topic 
			WHERE hc.id_country>0 
			GROUP BY hc.id_topic 
			ORDER BY NULL";		
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TopicCountriesUpdate($id_topic,$group_countries)
	{
		if($id_topic>0 && is_array($group_countries))
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "homepage_countries" );
			$res[] = $db->query( "DELETE FROM homepage_countries WHERE id_topic='$id_topic' AND id_country>0" );
			foreach($group_countries as $id_country)
			{
				$res[] = $db->query( "INSERT INTO homepage_countries (id_topic,id_country) VALUES ($id_topic,'$id_country')" );
			}
			Db::finish( $res, $db);
		}
	}
	
	public function UkCounties()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_county,name FROM uk_counties ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UkCounty( $id_county )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name FROM uk_counties WHERE id_county=$id_county ";
		$db->query_single($row,$sqlstr);
		return $row['name'];
	}

}
?>
