<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");
define('EUROCHANGE',1936.27);

class Payment
{
	public $currencies = array();
	public $currency_codes = array();
	public $currency_symbols = array();
	public $default_currency;
	public $default_currency_desc;
	public $account_usage_types;

	private $paypal_cycle_units;
	
	function __construct()
	{
		$this->currencies[1] = "Lire";
		$this->currencies[2] = "Euro";
		$this->currencies[3] = "GBP";
		$this->currencies[4] = "USD";
		$this->currencies[5] = "RM";
		$this->currency_codes[1] = "___";
		$this->currency_codes[2] = "EUR";
		$this->currency_codes[3] = "GBP";
		$this->currency_codes[4] = "USD";
		$this->currency_codes[5] = "RM";
		$this->currency_symbols[1] = "___";
		$this->currency_symbols[2] = "&euro;";
		$this->currency_symbols[3] = "&pound;";
		$this->currency_symbols[4] = "&#36;";
		$this->currency_symbols[5] = "RM";
		$ini = new Ini();
		$this->default_currency = $ini->Get("default_currency");
		$this->default_currency_desc = $this->currencies[$this->default_currency];
		$this->paypal_cycle_units = array('D','W','M','Y');
		$this->account_usage_types = array('campaign'=>1,'form'=>2,'ecommerce'=>3);
	}

	public function AccountDelete($id_account)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts" );
		$res[] = $db->query( "DELETE FROM accounts WHERE id_account='$id_account' " );
		Db::finish( $res, $db);
	}
	
	public function AccountGet($id_account)
	{
		$row = array();
		if($id_account>0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT id_account,id_topic,id_user,id_type,active,name,params,shared from accounts  WHERE id_account='$id_account' ";
			$db->query_single($row, $sqlstr);
		}
		return $row;
	}

	public function AccountParamStore($id_account,$param,$value)
	{
		$aparams = $this->AccountParamsGet($id_account,false);
		/*
		if($param=="paypal_src")
			$value = $value=="on"? 1 :0;
		*/
		$aparams[$param] = $value;
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ser_params = $v->Serialize($aparams);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts" );
		$sqlstr = "UPDATE accounts SET params='$ser_params' WHERE id_account=$id_account ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function AccountParamsReset($id_account)
	{
		$aparams = array();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ser_params = $v->Serialize($aparams);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts" );
		$sqlstr = "UPDATE accounts SET params='$ser_params' WHERE id_account=$id_account ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function AccountParams($id_account_type)
	{
		$params = array();
		$params[1] = array();
		$params[2] = array('ac_holder','ac_number','sortcode','BIC','IBAN');
		$params[3] = array('email','paypal_currencies','page_style');
		$params[4] = array('email','paypal_currencies','paypal_cycle_length','paypal_cycle_unit','paypal_times','page_style');
		$params[5] = array('ac_number');
		$params[6] = array('ac_holder','ac_number');
		$params[7] = array('ac_holder','ac_number','ABI','CAB');
		return $params[$id_account_type];
	}

	public function AccountParamsGet($id_account,$convert=true)
	{
		$row = $this->AccountGet($id_account);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$aparams = $v->Deserialize($row['params']);
		$current_params = (is_array($aparams))? $aparams : array();
		if($convert && array_key_exists("paypal_cycle_unit",$current_params))
		{
			$current_params['paypal_cycle_unit'] = $this->paypal_cycle_units[$current_params['paypal_cycle_unit']];
		}
		return $current_params;
	}
	
	public function AccountSharedSet($id_account,$shared)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts" );
		$sqlstr = "UPDATE accounts SET shared='$shared' WHERE id_account=$id_account ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function AccountStore($id_topic,$id_account,$id_type,$name,$active,$id_user)
	{
		if($id_account>0)
		{
			$account = $this->AccountGet($id_account);
			if($account['id_type']!=$id_type)
				$this->AccountParamsReset($id_account);
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts" );
		if($id_account>0)
			$sqlstr = "UPDATE accounts SET id_type='$id_type',name='$name',active='$active',id_topic='$id_topic'
				WHERE id_account=$id_account ";
		else 
		{
			$id_account = $db->nextId("accounts","id_account");
			$sqlstr = "INSERT INTO accounts (id_account,id_topic,id_user,id_type,active,name) 
					VALUES ($id_account,'$id_topic','$id_user','$id_type','$active','$name') ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_account;
	}
	
	public function Accounts(&$rows,$paged=true)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT ac.id_account,ac.name,ac.*,t.name AS topic_name 
		FROM accounts ac
		LEFT JOIN topics t ON ac.id_topic=t.id_topic  ORDER BY ac.id_account";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function AccountUseGet($id_use)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_use,id_account,type,id_item
			FROM accounts_use 
			WHERE id_use='$id_use' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function AccountUseGetById($type,$id_item,$id_account)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_use
			FROM accounts_use 
			WHERE type='$type' AND id_item='$id_item' AND id_account='$id_account'";
		$db->query_single($row,$sqlstr);
		return (int)$row['id_use'];
	}

	public function AccountUseDelete($id_use)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts_use" );
		$res[] = $db->query( "DELETE FROM accounts_use WHERE id_use='$id_use' " );
		Db::finish( $res, $db);
	}
	
	public function AccountUseDeleteById($type,$id_item)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts_use" );
		$res[] = $db->query( "DELETE FROM accounts_use WHERE type='$type' AND id_item='$id_item' " );
		Db::finish( $res, $db);
	}
	
	/**
	 * Insert a new use for account
	 *
	 * @param integer $type			Use ID (1=Campaigns,2=Forms)
	 * @param integer $id_item		Campaign/Form ID
	 * @param integer $id_account	Account ID
	 * @return integer				New account use ID
	 */
	public function AccountUseInsert($type,$id_item,$id_account)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts_use" );
		$id_use = $db->nextId( "accounts_use", "id_use" );
		$sqlstr = "INSERT INTO accounts_use (id_use,id_account,type,id_item)
			VALUES ($id_use,'$id_account','$type','$id_item')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_use;
	}
	
	public function AccountUseUpdate($id_use,$type,$id_item,$id_account)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "accounts_use" );
		$sqlstr = "UPDATE accounts_use SET type='$type',id_item='$id_item',id_account='$id_account'
			WHERE id_use='$id_use' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function AccountsUse($id_type,$id_item)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT au.id_account,a.name,a.id_type,au.id_use
			FROM accounts_use au 
			INNER JOIN accounts a ON au.id_account=a.id_account 
			WHERE au.type=$id_type AND au.id_item='$id_item' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function AccountsByType($id_type=0)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_account,a.name 
			FROM accounts a 
			WHERE active=1";
		if($id_type>0)
			$sqlstr .= " AND a.id_type='$id_type'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function AccountsUseAvailable($id_type,$id_item,$id_topic)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_account,a.name,au.id_use 
			FROM accounts a 
			LEFT JOIN accounts_use au ON a.id_account=au.id_account AND au.type=$id_type AND au.id_item='$id_item' 
			WHERE (a.id_topic=$id_topic OR a.shared=1) AND au.id_use IS NULL ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Balance($id_payment_type)
	{
		$balance = array();
		$min_ts = $this->YearMin();
		$min_date = getdate($min_ts);
		$min_year = $min_date['year'];
		$cur_year = date("Y");
		$cur_month = date("m");
		$accounts = array();
		$this->Accounts($accounts,false);
		$period_tot = 0;
		$unverified = 0;
		$incomings = array();
		$outgoings = array();
		for ($year=$min_year;$year<=$cur_year;$year++)
		{
			if($year==$min_year)
				$min_mon = $min_date['mon'];
			else 
				$min_mon = 1;
			$max_mon = ($year<$cur_year)? 12 : $cur_month;
			for ($month=$min_mon;$month<=$max_mon;$month++)
			{
				$month_tot = 0;
				$temp_in = 0;
				$temp_out = 0;
				$temp = array();
				$temp['period'] = "$year.$month";
				$temp['year'] = $year;
				$temp['month'] = $month;
				foreach($accounts as $account)
				{
					$payments = $this->PaymentsPeriod($account['id_account'],$year,$month,$id_payment_type,false);
					foreach($payments as $payment)
					{
						if($payment['verified']==1)
						{
							if ($payment['is_in']==1 && $payment['id_account']==$account['id_account'])
							{
								$incomings['a'.$account['id_account']] += $payment['amount'];
								$temp_in += $payment['amount'];
							}
							if ($payment['is_in']==0 || ($payment['is_in']=="1" && $payment['is_transfer']=="1" && $payment['from_account']==$account['id_account']))
							{
								$outgoings['a'.$account['id_account']] += $payment['amount'];
								$temp_out += $payment['amount'];
							}
						}
						else 
						{
							if ($payment['is_in']==1)
								$unverified += $payment['amount'];
							else
								$unverified -= $payment['amount']; 
						}
					}
					$account_tot = $incomings['a'.$account['id_account']] - $outgoings['a'.$account['id_account']];
					$temp_tot = $temp_in - $temp_out;
					$temp['account'.$account['id_account']] = $account_tot;
					$month_tot = $temp_tot;
					
				}
				$period_tot += $month_tot;
				$temp['total_month'] = $month_tot;
				$temp['total'] = $period_tot;
				$temp['unverified'] = $unverified;
				$balance[] = $temp;
			}
		}
		return array_reverse($balance);
	}

	public function BalanceYears()
	{
		$balance = array();
		$min_ts = $this->YearMin();
		$min_date = getdate($min_ts);
		$min_year = $min_date['year'];
		$cur_year = date("Y");
		$cur_month = date("m");
		$accounts = array();
		$this->Accounts($accounts,false);
		$period_tot = 0;
		$incomings = array();
		$outgoings = array();
		for ($year=$min_year;$year<=$cur_year;$year++)
		{
			if($year==$min_year)
				$min_mon = $min_date['mon'];
			else 
				$min_mon = 1;
			$max_mon = ($year<$cur_year)? 12 : $cur_month;
			$year_tot = 0;
			for ($month=$min_mon;$month<=$max_mon;$month++)
			{
				$temp_in = 0;
				$temp_out = 0;
				$temp = array();
				$temp['period'] = "$year";
				$temp['year'] = $year;
				foreach($accounts as $account)
				{
					$payments = $this->PaymentsPeriod($account['id_account'],$year,$month,0);
					foreach($payments as $payment)
					{
						if ($payment['is_in']==1 && $payment['id_account']==$account['id_account'])
						{
							$incomings['a'.$account['id_account']] += $payment['amount'];
							$temp_in += $payment['amount'];
						}
						if ($payment['is_in']==0 || ($payment['is_in']=="1" && $payment['is_transfer']=="1" && $payment['from_account']==$account['id_account']))
						{
							$outgoings['a'.$account['id_account']] += $payment['amount'];
							$temp_out += $payment['amount'];
						}
					}
					$account_tot = $incomings['a'.$account['id_account']] - $outgoings['a'.$account['id_account']];
					$temp_tot = $temp_in - $temp_out;
					$temp['account'.$account['id_account']] = $account_tot;
					$month_tot = $temp_tot;
				}
				$year_tot += $month_tot;
				$period_tot += $month_tot;
				$temp['total'] = $period_tot;
			}
			$temp['total_year'] += $year_tot;	
			$balance[] = $temp;
		}
		return array_reverse($balance);
	}

	public function Payers()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT py.id_payer,CONCAT(p.name2,' ',p.name1) AS name
			FROM payers py 
			INNER JOIN people p ON py.id_p=p.id_p 
			ORDER BY name ";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}
	
	public function PayerAdd($id_p)
	{
		$db =& Db::globaldb();
		$row = array();
		$db->begin();
		$db->query_single($row, "SELECT id_payer FROM payers WHERE id_p='$id_p' ");
		$id_payer = (int)$row['id_payer'];
		if(!$id_payer>0)
		{
			$db->begin();
			$db->lock( "payers" );
			$id_payer = $db->nextId( "payers", "id_payer" );
			$res[] = $db->query("INSERT INTO payers (id_payer,id_p) VALUES ($id_payer,'$id_p')");
			Db::finish( $res, $db);
		}
		return $id_payer;
	}
	
	public function PayerGetByPayerId($id_payer)
	{
		$db =& Db::globaldb();
		$row = array();
		$db->begin();
		$db->query_single($row, "SELECT id_payer,id_p FROM payers WHERE id_payer='$id_payer' ");
		return $row;
	}
	
	public function PayerCreate($salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetByEmail($email);
		$id_p = (int)$user['id_p'];
		if(!$id_p>0)
		{
			$id_p = $pe->UserCreate($name1,$name2,$email,$id_geo,0,array(),false,0);
			$pe->UserUpdate($id_p,$salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone);
			$pe->Deactivate($id_p);
		}
		$this->PayerAdd($id_p);
		return $id_p;
	}
	
	public function PayerDelete($id_payer)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payers" );
		$res[] = $db->query( "DELETE FROM payers WHERE id_payer=$id_payer" );
		Db::finish( $res, $db);
	}
	
	public function PayerNotify($email,$name,$subject,$message)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");	
		$mail = new Mail();
		$mail->SendMail($email,$name,$subject,$message,array());
	}

	public function PayerSearch(&$rows,$params)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$gjoin = $geo->GeoJoin("p.id_geo");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT py.id_payer,p.id_p,p.name1,p.name2,IF(p.email_valid,p.email,'') AS email,p.verified,p.town,
			$gjoin[name] AS geo_name,p.contact,p.active,
			SUM(IF(pa.is_in,pa.amount,-pa.amount)) AS balance,pa.id_currency";
		$sqlstr .= " FROM payers py 
			INNER JOIN payments pa ON py.id_payer=pa.id_payer 
			INNER JOIN people p ON py.id_p=p.id_p $gjoin[join] ";
		$sqlstr .= " WHERE 1=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%' OR p.name3 LIKE '%{$params['name']}%') ";
		if (strlen($params['email']) > 0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if ($params['id_geo'] > 0)
			$sqlstr .= " AND p.id_geo={$params['id_geo']}";
		if ($params['contact']=="on")
				$sqlstr .= " AND p.contact=1 ";
		$sqlstr .= " GROUP BY py.id_payer ORDER BY p.name2,p.name1";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function PayersAmounts(&$rows, $is_in, $paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT py.id_payer,py.id_p,CONCAT(p.name1,' ',p.name2) AS name,p.name3,SUM(pa.amount) AS amount_tot,pa.id_currency,
		(SELECT SUM(IF(pa2.is_in,pa2.amount,-pa2.amount)) FROM payments pa2 WHERE pa2.id_payer=pa.id_payer) AS balance,
		p.email_valid,IF(p.email_valid,p.email,'') AS email,p.address,p.address_notes,p.town,p.postcode,p.id_geo,p.phone,p.salutation
			FROM payers py 
			INNER JOIN people p ON py.id_p=p.id_p 
			INNER JOIN payments pa ON py.id_payer=pa.id_payer AND pa.is_in='$is_in'
			WHERE is_transfer=0
			GROUP BY py.id_payer
			ORDER BY amount_tot DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function PaymentDelete($id_payment)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payments" );
		$res[] = $db->query( "DELETE FROM payments WHERE id_payment=$id_payment" );
		Db::finish( $res, $db);
	}

	public function PaymentGet( $id_payment )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_payment,amount,id_currency,description,notes,id_payment_type,id_payer,id_account,is_in,is_transfer,
		verified,from_account,id_use,tx_id,UNIX_TIMESTAMP(pay_date) AS pay_date_ts FROM payments WHERE id_payment='$id_payment' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function PaymentsMonth( &$rows, $year, $month, $id_payment_type=0 )
	{
		$min_ts = mktime(0,0,0,$month,1,$year);
		$month_days = date("t",$min_ts);
		$max_ts = mktime(23,59,59,$month,$month_days,$year);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_payment,is_in,SUM(IF(is_transfer OR is_in,amount,0)) AS tot_in,SUM(IF(is_in=0 OR is_transfer=1,amount,0)) AS tot_out,
			(SUM(IF(is_in=1 AND is_transfer=0,amount,0))-SUM(IF(is_in=0 AND is_transfer=0,amount,0))) AS total,pt.payment_type,pt.id_payment_type
			FROM payments pa
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type
			WHERE verified=1 AND UNIX_TIMESTAMP(pay_date)>=$min_ts AND  UNIX_TIMESTAMP(pay_date)<=$max_ts  ";
		if($id_payment_type>0)
			$sqlstr .= " AND pa.id_payment_type=$id_payment_type ";
		$sqlstr .= "GROUP by pa.id_payment_type ORDER BY total ASC ";
		return $db->QueryExe($rows, $sqlstr);
	}
	
	public function PaymentsMonthType( &$rows, $year, $month, $id_payment_type )
	{
		$min_ts = mktime(0,0,0,$month,1,$year);
		$month_days = date("t",$min_ts);
		$max_ts = mktime(23,59,59,$month,$month_days,$year);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(pay_date) AS pay_date_ts,id_payment,id_currency,amount,
			description,pt.payment_type,CONCAT(p.name2,' ',p.name1) AS name,a.name AS account,pa.verified,pa.is_in
			FROM payments pa
			INNER JOIN payers py ON pa.id_payer=py.id_payer 
			INNER JOIN people p ON py.id_p=p.id_p 
			LEFT JOIN accounts a ON pa.id_account=a.id_account 
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type
			WHERE pa.verified=1 AND pa.id_payment_type='$id_payment_type' AND UNIX_TIMESTAMP(pay_date)>=$min_ts AND  UNIX_TIMESTAMP(pay_date)<=$max_ts  ";
		$sqlstr .= " ORDER BY pay_date DESC ";
		return $db->QueryExe($rows, $sqlstr,true);
	}
	
	public function PaymentsPeriod($id_account,$year,$month,$id_payment_type,$verified_only=true)
	{
		$min_ts = mktime(0,0,0,$month,1,$year);
		$month_days = date("t",$min_ts);
		$max_ts = mktime(23,59,59,$month,$month_days,$year);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_payment,is_in,amount,is_transfer,id_account,from_account,verified
			FROM payments 
			WHERE (id_account=$id_account OR (is_transfer=1 AND from_account=$id_account)) ";
		if($verified_only)
			$sqlstr .= " AND verified=1 ";
		$sqlstr .= " AND UNIX_TIMESTAMP(pay_date)>=$min_ts AND  UNIX_TIMESTAMP(pay_date)<=$max_ts  ";
		if($id_payment_type>0)
			$sqlstr .= " AND id_payment_type=$id_payment_type ";
		$sqlstr .= " ORDER BY pay_date ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PaymentCheck($id_account,$tx_id) {
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_payment FROM payments WHERE id_account='$id_account' AND tx_id='$tx_id' ";
		$db->query_single($row,$sqlstr);
		return isset($row['id_payment']);
	}
	
	public function PaymentInsert($pay_date,$id_payer,$id_account,$amount,$description,$notes,$id_payment_type,$is_in,$id_currency,$tx_id)
	{
		$verified = 1;
		$is_transfer = 0;
		$from_account = 0;
		$id_use = 0;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payments" );
		$id_payment = $db->nextId( "payments", "id_payment" );
		$sqlstr = "INSERT INTO payments (id_payment,pay_date,id_payer,id_account,amount,id_currency,description,notes,id_payment_type,is_in,verified,is_transfer,from_account,id_use,tx_id)
			VALUES ($id_payment,'$pay_date','$id_payer','$id_account','$amount','$id_currency','$description','$notes','$id_payment_type','$is_in',$verified,'$is_transfer','$from_account','$id_use','$tx_id')";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		return $id_payment;
	}

	public function PaymentStore($id_payment,$pay_date,$id_payer,$id_account,$amount,$description,$notes,$id_payment_type,$is_in,$verified,$is_transfer,$from_account,$id_use,$currency="")
	{
		$id_currency = $this->default_currency;
		if($currency!="")
		{
			$lookup_currency = array_search($currency,$this->currency_codes);
			if($lookup_currency>0)
				$id_currency = $lookup_currency;
		}
		if($is_transfer)
		{
			$id_payer = 0;
			$id_payment_type = 0;
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payments" );
		if($id_payment>0)
			$sqlstr = "UPDATE payments SET pay_date='$pay_date',id_payer='$id_payer',id_account='$id_account',amount='$amount',
			 description='$description',notes='$notes',id_payment_type='$id_payment_type',
			 is_in='$is_in',verified='$verified',is_transfer='$is_transfer',from_account='$from_account'
			  WHERE id_payment='$id_payment' ";
		else 
		{
			$id_payment = $db->nextId( "payments", "id_payment" );
			$sqlstr = "INSERT INTO payments (id_payment,pay_date,id_payer,id_account,amount,id_currency,description,notes,id_payment_type,is_in,verified,is_transfer,from_account,id_use)
			 VALUES ($id_payment,'$pay_date','$id_payer','$id_account','$amount','$id_currency','$description','$notes','$id_payment_type','$is_in',$verified,'$is_transfer','$from_account','$id_use')";
		}
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		return $id_payment;
	}

	public function PaymentTokenSet($id_payment)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$token = $v->Uid();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payment_tokens" );
		$res[] = $db->query("DELETE FROM payment_tokens WHERE id_payment='$id_payment' ");
		$res[] = $db->query("INSERT INTO payment_tokens (id_payment,token) VALUES ('$id_payment','$token')");
		Db::finish( $res, $db);
		return $token;
	}
	
	public function PaymentTokenGet($token, $delete=true)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_payment FROM payment_tokens WHERE token='$token' ";
		$db->query_single($row,$sqlstr);
		$id_payment = (int)$row['id_payment'];
        if ($delete)
        {
		    $db->begin();
		    $db->lock( "payment_tokens" );
		    $res[] = $db->query("DELETE FROM payment_tokens WHERE token='$token' ");
		    Db::finish( $res, $db);
        }
		return $id_payment;
	}
	
    public function PaymentVerify($id_payment,$send_email=true, $notes = '')
    {
		$row = array();
		if($send_email)
		{
			$row = array();
			$db =& Db::globaldb();
			$sqlstr = "SELECT p.id_p,CONCAT(p.name1,' ',p.name2) AS name,p.email,pa.amount,pa.id_currency,UNIX_TIMESTAMP(pa.pay_date) AS pay_date_ts,pa.description FROM payments pa INNER JOIN payers py ON pa.id_payer=py.id_payer INNER JOIN people p ON py.id_p=p.id_p WHERE pa.id_payment='$id_payment' AND p.email_valid=1 ";
			$db->query_single($row,$sqlstr);
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payments" );
		$sqlstr = "UPDATE payments SET verified='1', notes='" . $notes . "' WHERE id_payment='$id_payment' ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "payment_tokens" );
		$res[] = $db->query("DELETE FROM payment_tokens WHERE id_payment='$id_payment' ");
		Db::finish( $res, $db);
		$payment = $this->PaymentGet($id_payment);
		if($payment['id_use'])
		{
			$use = $this->AccountUseGet($payment['id_use']);
			$this->SessionDelete($use['type'],$use['id_item']);
		}
		return $row;
	}

	public function Payments( &$rows, $is_in, $verified, $sort_by )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(pay_date) AS pay_date_ts,id_payment,id_currency,amount,
			description,pt.payment_type,IF(pa.is_transfer=1,CONCAT('Tr. ',IF(is_in=$is_in,a_from.name,a_to.name)),CONCAT(p.name1,' ',p.name2)) AS payer,
			IF(is_transfer AND is_in<>$is_in,a_from.name,a_to.name) AS account
			FROM payments pa 
			LEFT JOIN payers py ON pa.id_payer=py.id_payer 
			LEFT JOIN people p ON py.id_p=p.id_p 
			LEFT JOIN accounts a_to ON pa.id_account=a_to.id_account 
			LEFT JOIN accounts a_from ON pa.from_account=a_from.id_account
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type 
			WHERE pa.verified='$verified' AND ((pa.is_in='$is_in') OR (pa.is_in<>'$is_in' AND pa.is_transfer=1)) ";
		$sqlstr .= ($sort_by=="1")? " ORDER BY id_payment DESC,pay_date DESC " : " ORDER BY pay_date DESC,id_payment DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
    
    public function PaymentsVerified( &$rows, $verified )
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "SELECT id_payment, description, id_payer
            FROM payments pa
            WHERE pa.verified='$verified'";                                                                                  
        return $db->QueryExe($rows, $sqlstr, false);
    }

	public function PaymentsPerson(&$rows,$id_p,$paged=true,$only_verified=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(pay_date) AS pay_date_ts,id_payment,id_currency,amount,
			description,pt.payment_type,a.name AS account,pa.verified,pa.is_in,
			UNIX_TIMESTAMP(pay_date) AS hdate_ts,pa.tx_id
			FROM payments pa 
			INNER JOIN payers py ON pa.id_payer=py.id_payer 
			INNER JOIN accounts a ON pa.id_account=a.id_account 
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type 
			WHERE py.id_p='$id_p' ";
		if($only_verified)
			$sqlstr .= " AND pa.verified=1 ";
		$sqlstr .= " ORDER BY pay_date DESC";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	public function PaymentsYear(&$rows,$year)
	{
		$min_ts = mktime(0,0,0,1,1,$year);
		$max_ts = mktime(23,59,59,12,31,$year);
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_payment,is_in,SUM(IF(is_transfer OR is_in,amount,0)) AS tot_in,SUM(IF(is_in=0 OR is_transfer=1,amount,0)) AS tot_out,
			(SUM(IF(is_in=1 AND is_transfer=0,amount,0))-SUM(IF(is_in=0 AND is_transfer=0,amount,0))) AS total,pt.payment_type
			FROM payments pa
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type
			WHERE verified=1 AND UNIX_TIMESTAMP(pay_date)>=$min_ts AND  UNIX_TIMESTAMP(pay_date)<=$max_ts  ";
		$sqlstr .= "GROUP by pa.id_payment_type ORDER BY total ASC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Search( $is_in, &$rows, $params )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(pay_date) AS pay_date_ts,id_payment,id_currency,amount,
			description,pt.payment_type,CONCAT(p.name2,' ',p.name1) AS name,a.name AS account,pa.verified
			FROM payments pa 
			INNER JOIN payers py ON pa.id_payer=py.id_payer 
			INNER JOIN people p ON py.id_p=p.id_p 
			LEFT JOIN accounts a ON pa.id_account=a.id_account 
			LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type 
			WHERE pa.is_in='$is_in' ";
		if ($params['id_payer'] > 0)
			$sqlstr .= " AND pa.id_payer='{$params['id_payer']}' ";
		if ($params['id_account'] > 0)
			$sqlstr .= " AND pa.id_account='{$params['id_account']}' ";
		if ($params['id_payment_type'] > 0)
			$sqlstr .= " AND pa.id_payment_type='{$params['id_payment_type']}' ";
		if (strlen($params['desc']) > 0)
			$sqlstr .= " AND (pa.description LIKE '%{$params['desc']}%' OR pa.notes LIKE '%{$params['desc']}%'  )";
		if ($params['sort_by'] > 0)
			$sqlstr .= " ORDER BY pa.id_payment DESC ";
		else
			$sqlstr .= " ORDER BY pay_date DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	private function SessionDelete($type,$id_item)
	{
		$pending_var = $this->SessionVar($type,$id_item);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Delete($pending_var);
	}
	
	public function SessionGet($type,$id_item)
	{
		$pending_var = $this->SessionVar($type,$id_item);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$id_payment = $session->Get($pending_var);
		return (int)$id_payment;
	}
	
	public function SessionSet($type,$id_item,$id_payment)
	{
		$pending_var = $this->SessionVar($type,$id_item);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set($pending_var,$id_payment);
	}
	
	private function SessionVar($type,$id_item)
	{
		return "type" . $type . "_item" . $id_item;
	}
	
	public function Type( $id_payment_type )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT payment_type,id_topic FROM payment_types WHERE id_payment_type=$id_payment_type";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function TypeBalance( $id_payment_type )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT SUM(IF(is_transfer OR is_in,amount,0)) AS tot_in,SUM(IF(is_in=0 OR is_transfer=1,amount,0)) AS tot_out,
			(SUM(IF(is_in=1 AND is_transfer=0,amount,0))-SUM(IF(is_in=0 AND is_transfer=0,amount,0))) AS total
			FROM payments pa 
			WHERE id_payment_type=$id_payment_type AND verified=1  ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function TypeDelete( $id_payment_type )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payment_types" );
		$res[] = $db->query( "DELETE FROM payment_types WHERE id_payment_type='$id_payment_type' " );
		Db::finish( $res, $db);
	}

	public function TypeMonths( $id_payment_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DATE_FORMAT(pay_date,\"%M %Y\") AS dd,DATE_FORMAT(pay_date,\"%Y%m\") AS sort,
			SUM(IF(is_transfer OR is_in,amount,0)) AS tot_in,SUM(IF(is_in=0 OR is_transfer=1,amount,0)) AS tot_out 
			FROM payments 
			WHERE id_payment_type='$id_payment_type' 
			GROUP BY dd ORDER BY sort ASC ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TypeStore( $id_payment_type, $payment_type, $id_topic=0 )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payment_types" );
		if($id_payment_type>0)
			$res[] = $db->query( "UPDATE payment_types SET payment_type='$payment_type',id_topic='$id_topic' WHERE id_payment_type=$id_payment_type" );
		else
		{
			$id_payment_type = $db->nextId( "payment_types", "id_payment_type" );
			$res[] = $db->query( "INSERT INTO payment_types (id_payment_type,payment_type,id_topic) VALUES ($id_payment_type,'$payment_type','$id_topic')" );
		}
		Db::finish( $res, $db);
		return $id_payment_type;
	}
	
	public function TypeTransfer($from,$to)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "payments" );
		$res[] = $db->query( "UPDATE payments SET id_payment_type='$to' WHERE id_payment_type='$from' " );
		Db::finish( $res, $db);
		$this->TypeDelete($from);
	}
	
	public function Types( &$rows, $id_topic=0, $paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT pt.id_payment_type,pt.payment_type,t.name,pt.id_topic 
			FROM payment_types pt
			LEFT JOIN topics t ON pt.id_topic=t.id_topic ";
		if($id_topic>0)
			$sqlstr .= " WHERE pt.id_topic=$id_topic OR pt.id_topic=0 ";
		$sqlstr .= " ORDER BY pt.payment_type";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	public function PaymentsTypesAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_payment_type,payment_type FROM payment_types ORDER BY payment_type";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TypesBalance( &$rows, $exclude )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT pt.id_payment_type,pt.payment_type,t.name,pt.id_topic, 
			(SUM(IF(pa.is_in=1 AND pa.is_transfer=0,pa.amount,0))-SUM(IF(pa.is_in=0 AND pa.is_transfer=0,pa.amount,0))) AS total
			FROM payment_types pt 
			LEFT JOIN payments pa ON pt.id_payment_type=pa.id_payment_type 
			LEFT JOIN topics t ON pt.id_topic=t.id_topic 
			WHERE 1 ";
		if($exclude>0)
			$sqlstr .= " AND pt.id_payment_type<>$exclude ";
		$sqlstr .= " GROUP BY pt.id_payment_type
			 ORDER BY total DESC";
		return $db->QueryExe($rows, $sqlstr,true);
	}

	private function YearMin()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT UNIX_TIMESTAMP(MIN(pay_date)) AS min_date_ts FROM payments WHERE verified=1");
		return $row['min_date_ts'];
	}

}
?>
