<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/crypt.php");
include_once(SERVER_ROOT."/../classes/mail.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/modules.php");
include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/auth.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/resources.php");

/**
 * Maximum timeout (in secods) for script execution
 * 
 * @var integer
 */
define('MANUAL_EXECUTION_TIMEOUT',900);

/**
 * Admin interface management
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class AdminHelper
{
	/**
	 * ID of the user currently logged in
	 *
	 * @var integer
	 */
	public $current_user_id;
	
	/**
	 * HTML content header (dependent on configured content-type)
	 *
	 * @var string
	 */
	public $content_header;
	
	/** 
	 * @var Resources */
	public $r;

	/** 
	 * @var Session */
	public $session;

	/** 
	 * @var Auth */
	public $auth;
	
	/**
	 * Wether a warning is shown in case an article is submitted
	 * with the title all capitals
	 *
	 * @var boolean
	 */
	public $uppercase_warning;

	/**
	 * CDN URL
	 *
	 * @var string
	 */
	public $cdn;
	
	/**
	 * Admin interface content type
	 *
	 * @var string
	 */
	private $content_type;

	/**
	 * Current page URL
	 *
	 * @var string
	 */
	private $current_url;
	
	/**
	 * Authentication mechanism
	 * (either internal or ldap)
	 *
	 * @var string
	 */
	private $user_auth;
	
	/**
	 * Maximum number of failed login attempts
	 *
	 * @var integer
	 */
	private $max_login_attempts;
	
	/**
	 * Minimum delay (in seconds) between login attemps
	 *
	 * @var integer
	 */
	private $login_failure_delay;
    
	/**
	 * Wether to force email verification for admin users
	 *
	 * @var boolean
	 */
	private $force_email_verification;
	
	/**
	 * Variable for timing executions
	 *
	 * @var integer
	 */
	private $time_exec;
	
	/**
	 * Development mode
	 *
	 * @var boolean
	 */
	private $dev;
	
	/**
     * Value for email verification sent
     *
     */
    const UserEmailVerificationSent = -1;
    /**
     * Value for missing email verification
     *
     */
    const UserEmailVerificationMissing = 0;
    /**
     * Value for email verification completed
     *
     */
    const UserEmailVerificationCompleted = 1;
	
	function __construct()
	{
		$this->session = new Session();
		$this->current_url = $_SERVER['REQUEST_URI'];
		$this->current_user_id = $this->session->Get("current_user_id");
		$conf = new Configuration();
		$this->user_auth = $conf->Get("user_auth");
		$this->cdn = $conf->Get("cdn");
		$this->max_login_attempts = $conf->Get("max_login_attempts");
		$this->login_failure_delay = $conf->Get("login_failure_delay");
        $this->force_email_verification = $conf->Get("force_email_verification");
		$this->dev = $conf->Get("dev");
		$this->content_type = $conf->Get("content_type");
		$this->content_header = (($this->content_type!="html4_strict" || $this->content_type!="html4_transitional")? "text/html":"application/xhtml+xml");
		$this->content_header .= "; charset=" . $conf->Get("encoding");
		$this->uppercase_warning = $conf->Get("uppercase_warning");
		$this->r = new Resources;
		$this->auth = new Auth();
	}
	
	/**
	 * The DOCTYPE used by Admin interface
	 *
	 * @return unknown
	 */
	public function DocType()
	{
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$doctypes = XslManager::DocTypes();
		$doctype = $doctypes[$this->content_type];
		return "<!DOCTYPE HTML PUBLIC \"{$doctype['public']}\" \"{$doctype['system']}\">\n";
	}

	/**
	 * Wrapper for authentication check
	 *
	 * @param boolean $with_redirect 	Page to redirect to if authentication is successful
	 */
	public function CheckAuth($with_redirect = true)
	{
		$check = $this->auth->Check();
		if (!$check)
		{
			if($with_redirect)
			{
				if ($_SERVER['REQUEST_METHOD']=="POST")
				{
					$error_desc = "session_expire";
					$this->session->Set("post_dump",serialize($_POST));
					$this->session->Set("post_uri",$_SERVER['REQUEST_URI']);
				}
				else
				{
					$error_desc = "auth_error";
				}
				$this->MessageSet($error_desc);
				$this->session->Set("uri",$_SERVER['REQUEST_URI']);
				header("Location: /gate/index.php");
			}
			else 
			{
				header('HTTP/1.1 403 Forbidden');
			}
			exit;
		}
	}
	
	/**
	 * Check if the current user has the righ to use the module
	 * currently selected
	 *
	 * @return boolean Wether the user is administrator of the module itself
	 */
	public function CheckModule()
	{
		$module_right = false;
		$module_admin = false;
		if ($this->current_user_id>0)
		{
			$this->SetModule();
			include_once(SERVER_ROOT."/../classes/rights.php");
			$ri = new Rights();
			$module_right = $ri->ModuleUser();
			$module_admin = $ri->ModuleAdmin();
		}
		if(!$module_right)
			exit;
		return $module_admin;
	}

	/**
	 * Update configuration of Admin interface
	 *
	 * @param string $pub_web
	 * @param string $admin_web
	 * @param string $title
	 * @param string $description
	 * @param string $staff_email
	 * @param string $init_year
	 * @param string $max_file_size
	 * @param integer $log_banners
	 * @param integer $geo_location
	 * @param integer $sched_notify
	 * @param string $scheduler_ip
	 * @param integer $feed_type
	 * @param integer $default_currency
	 * @param integer $log_level
	 */
	public function ConfigurationUpdate($pub_web,$admin_web,$title,$description,$staff_email,$init_year,$max_file_size,$log_banners,$geo_location,$sched_notify,$scheduler_ip,$feed_type,$default_currency,$log_level)
	{
		$ini = new Ini;
		$admin_web_old = $ini->Get("admin_web");
		$ini->Set("pub_web",$pub_web);
		$ini->Set("admin_web",$admin_web);
		$ini->Set("title",$title);
		$ini->Set("description",$description);
		$ini->Set("scheduler_ip",$scheduler_ip);
		$ini->Set("sched_notify",$sched_notify);
		$ini->Set("staff_email",$staff_email);
		$ini->Set("init_year",$init_year);
		$ini->Set("max_file_size",$max_file_size);
		$ini->Set("log_banners",$log_banners);
		$ini->Set("geo_location",$geo_location);
		$ini->Set("feed_type",$feed_type);
		$ini->Set("default_currency",$default_currency);
		$ini->Set("log_level",$log_level);
	}

	/**
	 * Set the environment for development mode
	 *
	 */
	public function DevSet()
	{
		// Disable WSDL caching
		ini_set("soap.wsdl_cache_enabled", "0");
	}
	
	/**
	 * Wrapper for user authentication for Admin interface
	 *
	 * @param string $username
	 * @param string $password
	 */
	public function Enter( $username , $password )
	{
		$this->Init();
		$id_user = $this->UserAuth($username,$password);
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		if ($id_user > 0)
		{
			$u->id = $id_user;
			$u->LoginSuccessful();
			$user = $u->UserGet();
			$this->auth->Set();
			$this->session->Set("current_user_id",$id_user);
			$this->session->Set("current_user_login",$username);
			$this->session->Set("id_language",$user['id_language']);
			if($user['id_p']>0)
				$this->PubUserSet($user['id_p']);
			include_once(SERVER_ROOT."/../classes/varia.php");
			$ip = Varia::IP();
			$user_agent = Varia::UserAgent();
			$ini = new Ini;
			if ($ip!="127.0.0.1")
			{
				$db =& Db::globaldb();
				$todayTime = $db->getTodayTime();
				$db->begin();
				$db->lock( "users_log" );
				$nextId = $db->nextId( "users_log", "id_user_log" );
				$res[] = $db->query( "INSERT INTO users_log (id_user_log,id_user,login,ip,user_agent)
							VALUES ( $nextId, $id_user, '$todayTime', '$ip','$user_agent')" );
				Db::finish( $res, $db);
				$db->begin();
				$db->lock( "users" );
				$res[] = $db->query( "UPDATE users SET last_conn='$todayTime' WHERE id_user='$id_user'" );
				Db::finish( $res, $db);
			}
			// In development mode, update the DB schema
			if($this->dev)
			{
				include_once(SERVER_ROOT."/../classes/phpeace_updates.php");
				$pu = new PhPeaceUpdates(0);
				$db_new = $pu->UpdateDBAll();
				if($db_new>0)
				{
					$this->MessageSet("update_dev",array($db_new));
				}
			}
		} else {
			if($id_user == -1) {
				$ini = new Ini;
				$staff_email = $ini->Get('staff_email');
				$this->MessageSet('user_inactive',array($login['id_user'],$staff_email));
			} else {
				$this->MessageSet("password_wrong");
				if ($this->user_auth=="internal")
				{
					$failures = $u->LoginFailureSet($username);
					if ($failures == $this->max_login_attempts)
					{
						$this->WarnAdmin("Too many failed login attemps","$username failed to login {$this->max_login_attempts} times ",false,1);
					}
				}
			}
		}
	}
	
	/**
	 * Initializes session variables for Admin interface
	 *
	 */
	public function Init()
	{
		$this->session->Set("auth",0);
		$this->session->Set("current_user_id",0);
		$this->session->Set("current_user_login","");
		$this->session->Set("module_right",0);
		$this->session->Set("module_admin",0);
		$this->session->Set("module_id",0);
	}

	/**
	 * Check if browser is IE6
	 * Based on http://lab.amanwithapencil.com/user_agent/
	 *
	 * @return boolean
	 */
	public function IsIe6()
	{
		$is_ie6 = false;
		$ua = $_SERVER['HTTP_USER_AGENT'];
		if (strpos($ua,'MSIE') !== false && strpos($ua,'Opera') === false)
		{
			if (substr($ua,strpos($ua,'MSIE')+5,1) < 7)
			{
				$is_ie6 = true;
			}
			if (strpos($ua,'Windows NT 5.2') !== false && strpos($ua,'.NET CLR') === false)
			{
				$is_ie6 = false;
			}
		}
		return $is_ie6;
	}
    
    public function UserBrowserType() 
    { 
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $ub = ''; 
        if(preg_match('/MSIE/i',$u_agent)) 
        { 
            $ub = "ie"; 
        } 
        elseif(preg_match('/Firefox/i',$u_agent)) 
        { 
            $ub = "firefox"; 
        } 
        elseif(preg_match('/Safari/i',$u_agent)) 
        { 
            $ub = "safari"; 
        } 
        elseif(preg_match('/Chrome/i',$u_agent)) 
        { 
            $ub = "chrome"; 
        } 
        elseif(preg_match('/Flock/i',$u_agent)) 
        { 
            $ub = "flock"; 
        } 
        elseif(preg_match('/Opera/i',$u_agent)) 
        { 
            $ub = "opera"; 
        } 
        
        return $ub; 
    } 
	
	/**
	 * Wether a module is active
	 *
	 * @param integer $id_module
	 * @return boolean
	 */
	public function IsModuleActive($id_module)
	{
		$row = Modules::ModuleGet( $id_module );
		return $row['active']=="1";
	}
	
	/**
	 * Wether a user exists with a specific username
	 *
	 * @param string $username
	 * @return integer User ID
	 */
	private function IsThereUser($username)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_user FROM users WHERE id_user>0 AND login='$username' AND active=1";
		$row = array();
		$db->query_single( $row, $sqlstr );
		return (int)$row['id_user'];
	}
	
	/**
	 * Retrieve custom JavaScript for Admin interface
	 *
	 * @return string
	 */
	public function JsGet()
	{
		$fm = new FileManager;
		$js = $fm->TextFileRead("uploads/custom/admin.js");
		return trim($js);
	}
	
	/**
	 * Store custom JavaScript for Admin interface
	 *
	 * @param string $js
	 */
	public function JsStore($js)
	{
		$fm = new FileManager;
		$fm->WritePage("uploads/custom/admin.js",$js);
	}
	
	/**
	 * Languages supported by PhPeace Admin interface
	 *
	 * @return array
	 */
	public function Languages()
	{
		$tr = new Translator(0,0);
		$languages = $tr->Translate("languages");
		$languages = array_slice($languages,0,3);
		$languages[0] = "___";
		return $languages;
	}

	/**
	 * Get last messages for Admin user
	 *
	 * @return array All messages
	 */
	public function MessageGet()
	{
		$msg = $this->session->Get("msg");
		$this->session->Delete("msg");
		return $msg;
	}

	/**
	 * Translate and append a message for Admin user
	 *
	 * @param string $msg The message itself
	 * @param array $params Translation params
	 * @return array All messages
	 */
	public function MessageSet($msg,$params=array())
	{
		$tr = new Translator($this->session->Get('id_language'),0);
		$tmsg = $tr->TranslateParams($msg,$params);
		$this->MessageSetTranslated($tmsg);
		return $tmsg;
	}
	
	/**
	 * Append a message for Admin user
	 *
	 * @param string $tmsg
	 */
	public function MessageSetTranslated($tmsg)
	{
		$pmsg = $this->session->Get("msg");
		if (is_array($pmsg) && count($pmsg)>0)
			$smsg = $pmsg;
		else
			$smsg = array();
		$smsg[] = $tmsg;
		$this->session->Set("msg",$smsg);
	}
	
	/**
	 * Variable for timing executions
	 *
	 * @return integer
	 */
	private function MicrotimeFloat()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	/**
	 * Check if the current user if Administrator of a specific module
	 *
	 * @param integer $id_module Module ID
	 * @return boolean
	 */
	public function ModuleAdmin($id_module)
	{
		return Modules::AmIAdmin($id_module);
	}

	/**
	 * Set the current active module
	 * (overriding what has been set previously based on Admin URL)
	 *
	 * @param integer $id_module Module ID
	 */
	public function ModuleForce($id_module)
	{
		$db =& Db::globaldb();
		$module = array();
		$db->query_single($module,"SELECT path,restricted FROM modules WHERE id_module='$id_module'");
		$this->session->Set("module_id",$id_module);
		$this->session->Set("module_path",$module['path']);
		$this->ModuleUserSet($id_module,$module['restricted']);
	}
	
	/**
	 * Check if the current user if user of a specific module
	 *
	 * @param integer $id_module Module ID
	 * @return boolean
	 */
	public function ModuleUser($id_module)
	{
		return Modules::AmIUser($id_module);
	}

	/**
	 * Set session variables for current user
	 *
	 * @param integer $id_module Module ID
	 * @param boolean $is_restricted Wether this module is using permissions or is open to everybody
	 */
	public function ModuleUserSet($id_module,$is_restricted)
	{
		if($is_restricted)
			$this->session->Set("module_right",0);
		else
			$this->session->Set("module_right",1);
		$this->session->Set("module_admin",0);
		$sqlstr = "SELECT id_user,is_admin FROM module_users WHERE id_module='$id_module' AND id_user='$this->current_user_id'";
		$db =& Db::globaldb();
		$user = array();
		$db->query_single($user,$sqlstr);
		if (count($user)>0)
		{
			$this->session->Set("module_right",1);
			if ($user['is_admin']=="1")
				$this->session->Set("module_admin",1);
		}
	}

	/**
	 * Send a password reminder
	 *
	 * @param string $unescaped_email Email address
	 */
	public function PasswordReminder($unescaped_email)
	{
		$mail = new Mail();
		$checkEmail = $mail->CheckEmail( $unescaped_email );
		if ( !$mail->debug_mail )
		{
			if ( $checkEmail )
			{
				include_once(SERVER_ROOT."/../classes/user.php");
				$db =& Db::globaldb();
				$email = $db->SqlQuote($unescaped_email);
				$user_counter = 0;
				$rows = array();
				$sqlstr = "SELECT login,password,name,id_language,verified,id_user FROM users WHERE id_user>0 AND email='$email'";
				$db->QueryExe($rows, $sqlstr);
				foreach($rows as $user)
				{
					$sc = new Scrypt;
					$dpass = $sc->decrypt($user['password']);
					$user_counter ++;
					$tr0 = new Translator($user['id_language'],0);
					$extra = array();
					$extra['name'] = $mail->title;
					$extra['email'] = $mail->staff_email;
					$subject = $tr0->TranslateParams("reserved_access",array($mail->title));
					if($this->force_email_verification && $user['verified']!=self::UserEmailVerificationCompleted)
					{
						$u = new User();
						$u->id = $user['id_user'];
						$link = $u->VerifyEmail(false);
						$message = $tr0->TranslateParams("password_message_and_verification",array($user['name'],$mail->title,$link,$mail->admin_web,$user['login'],$dpass,$mail->staff_email,$mail->title));
					}
					else 
					{
						$message = $tr0->TranslateParams("password_message",array($user['name'],$mail->title,$mail->admin_web,$user['login'],$dpass,$mail->staff_email,$mail->title));					
					}
					if($mail->SendMail($unescaped_email, "", $subject, $message, $extra))
					{
						$this->MessageSet("password_sent",array($unescaped_email));					
					}
					else 
					{
						$this->MessageSet("mail_error",array());
					}
				}
				if ($user_counter == 0)
					$this->MessageSet("password_not_sent",array($unescaped_email));
			}
			else
				$this->MessageSet("email_wrong",array($unescaped_email));
		}
		else
			$this->MessageSet("password_debug");
	}

	/**
	 * Associated Admin account to portal user
	 *
	 * @param integer $id_p ID of portal user
	 */
	public function PubUserSet($id_p)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$pub_user = $pe->UserGetById($id_p);
		$pub_user['auth'] = true;
		$this->session->Set("user",$pe->Row2User($pub_user));
	}

	/**
	 * Set session variables for current module
	 *
	 */
	public function SetModule()
	{
		$current_url = $this->current_url;
		$this->session->Set("module_id",0);
		$this->session->Set("module_path","");
		list(,$extr_path) = explode("/",$current_url);
		if ($extr_path!="")
		{
			$module = Modules::ModuleGetId($extr_path);
			$this->session->Set("module_id",$module['id_module']);
			$this->session->Set("module_path",$extr_path);
			$this->ModuleUserSet($module['id_module'],$module['restricted']);
		}
	}
	
	/**
	 * Set maximum script execution time
	 */
	public function SetTimeLimit()
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$va = new Varia();
		$va->SetTimeLimit(MANUAL_EXECUTION_TIMEOUT);
	}
	
	/**
	 * Set internal variable for timing execution
	 *
	 */
	public function TimeStart()
	{
		$this->time_exec = $this->MicrotimeFloat();
	}
	
	/**
	 * Return execution tim
	 *
	 * @return string
	 */
	public function TimeStat()
	{
		return number_format($this->MicrotimeFloat() - $this->time_exec,3);
	}
	
	/**
	 * Admin user authentication
	 *
	 * @param string $username
	 * @param string $password
	 * @return integer 1 for authenticated, 0 for false
	 */
	public function UserAuth($username, $password)
	{
		$auth = 0;
		switch($this->user_auth)
		{
			case "internal":
				$sc = new Scrypt;
				$cpass = $sc->encrypt($password);
				$db =& Db::globaldb();
				$sqlstr = "SELECT id_user,login_failures,UNIX_TIMESTAMP(last_failure) AS last_failure,active FROM users WHERE id_user>0 AND login='$username' AND password='$cpass'";
				if ($this->force_email_verification)
                    $sqlstr .= " AND verified = " . self::UserEmailVerificationCompleted;
                $login = array();
				$db->query_single( $login, $sqlstr );
				if($login['id_user']>0)
				{
					if($login['active']=='1') {
						if ($login['last_failure']>0 && $login['login_failures']>0)
						{
							if (($db->getTodayTime()-$login['last_failure'] )< $this->login_failure_delay )
								$auth = $login['id_user'];
						}
						else
							$auth = $login['id_user'];
	
					} else {
						$auth = -1;
					}
				}
			break;
			case "ldap":
				if($this->UserAuthLdap($username,$password))
				{
					$db =& Db::globaldb();
					$sqlstr = "SELECT id_user FROM users WHERE login='$username'";
					$login = array();
					$db->query_single( $login, $sqlstr );
					if($login['id_user']>0)
						$auth = $login['id_user'];
				}
			break;
		}
		return $auth;
	}
	
	/**
	 * Admin user authentication via LDAP
	 *
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function UserAuthLdap($username,$password)
	{
		$user_auth = false;
		set_error_handler("ErrorTrash");
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$ldap_config = $conf->Get("ldap");
		$connect=@ldap_connect("ldap://".$ldap_config['server']);
		if ($connect>0 && strlen($password)>0)
		{
			$bind=@ldap_bind($connect, $username . "@" . $ldap_config['domain'], $password);
			if ($bind)
			{
				$user_auth = true;
				@ldap_close($connect);
			}
		}
		set_error_handler("ErrorHandler");
		return $user_auth;		
	}
	
	/**
	 * Send a warning message via email
	 * to all administrators of a specific module
	 *
	 * @param string 	$subject 	Message subject
	 * @param string 	$message 	Message body
	 * @param boolean 	$clean_text	Wether to filter the text
	 * @param integer 	$id_module 	Module ID (default to PhPeace module)
	 */
	public function WarnAdmin($subject,$message,$clean_text=false,$id_module=17)
	{
		$extra = array();
		$user_info = "\n\n--\n";
		if ($this->current_user_id>0)
		{
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User();
			$u->id = $this->current_user_id;
			$row = $u->UserGet();
			$extra['name'] = $row['name'];
			$extra['email'] = $row['email'];
			$user_info .= "User: {$row['name']} (" . $this->session->Get("current_user_login") . ")\n";
		}
		$ini = new Ini;
		$title = $ini->Get("title");
		include_once(SERVER_ROOT."/../classes/varia.php");
		$user_info .= "IP: " . Varia::IP() . "\n";
		$user_info .= "PhPeace - $title\n" . $ini->Get("admin_web") . "\n\n";
		$mail = new Mail();
		$mail->clean_text = $clean_text;
		include_once(SERVER_ROOT."/../classes/users.php");
		$users = new Users;
		$mail_from = $mail->mail_from!=""? $mail->mail_from . " ":"";
		foreach($users->AdminUsers($id_module) as $user)
		{
			$mail->SendMail($user['email'],$user['name'],"[{$mail_from}$title] $subject",$message . $user_info, $extra);
		}
	}

}

/**
 * Web services for admin interface
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class AdminWS
{
	/**
	 * Installation key (used for authorizing requests)
	 *
	 * @var string
	 */
	private $install_key;
	
	function __construct()
	{
		$ini = new Ini();
		$this->install_key = $ini->Get("install_key");   
	}
	
	/**
	 * Update the status of a service
	 *
	 * @param string $key Installation key for authorization
	 * @param string $name Service name
	 * @param integer $status Service status
	 *   1 for up, 0 for down
	 * @param string $notes Details of service status
	 * @param integer $only_if_after Record status only if this number of days have passed since last update
	 * @return string
	 */
	public function ServiceStatusUpdate($key,$name,$status,$notes,$only_if_after)
	{
		include_once(SERVER_ROOT."/../classes/services.php");
		$se = new Services();
		$return = "";
		if($key!="" && $key==$this->install_key)
		{
			$se->ServiceStatusUpdate($name,$status,$notes,false,true,$only_if_after);
		}
		return $return;
	}
	
	/**
	 * Reset all variables in local cache memory
	 *
	 * @param string $key Installation key for authorization
	 */
	public function ResetCacheAll($key)
	{
		if($key!="" && $key==$this->install_key)
		{
			include_once(SERVER_ROOT."/../classes/sharedmem.php");
			$sm = new SharedMem(false);
			$sm->ResetAll();		
		}
	}

	/**
	 * Reset a specific variable in local cache memory
	 *
	 * @param string $varname Variable name
	 * @param string $key Installation key for authorization
	 */
	public function ResetCacheVar($varname,$key)
	{
		if($key!="" && $key==$this->install_key)
		{
			include_once(SERVER_ROOT."/../classes/sharedmem.php");
			$sm = new SharedMem();
			$sm->Delete($varname);
		}
	}

	/**
	 * Reset all variables beginning with a prefix in local cache memory
	 *
	 * @param string $var_prefix Variables' prefix
	 * @param string $key Installation key for authorization
	 */
	public function ResetCacheVarPrefix($var_prefix,$key)
	{
		if($key!="" && $key==$this->install_key)
		{
			include_once(SERVER_ROOT."/../classes/sharedmem.php");
			$sm = new SharedMem();
			$sm->ResetAllPrefix($var_prefix);
		}
	}

}

/**
 * REST services for admin interface
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class AdminREST
{
	/**
	 * Installation key (used for authorizing requests)
	 *
	 * @var string
	 */
	private $install_key;
	
	function __construct()
	{
		$ini = new Ini();
		$this->install_key = $ini->Get("install_key");   
	}

	/**
	 * Authenticate an admin user
	 *
	 * @param string $key 		Installation key for authorization
	 * @param string $username	Service name
	 * @param string $password	Hashed password
	 * @return array
	 */
	public function UserAuth($key="",$username="",$password="")
	{
		$return = array();
		$return['name'] = "";
		$return['auth'] = false;
		if(func_num_args()!=3)
			$return['error'] = "Missing parameters";
		elseif($key!="" && $key==$this->install_key)
		{
			$conf = new Configuration();
			$mobile_salt = $conf->Get("mobile_salt");
			if($mobile_salt!="")
			{
				include_once(SERVER_ROOT."/../classes/user.php");
				include_once(SERVER_ROOT."/../classes/log.php");
				$u = new User();
				$log = new Log();
				$credentials = $u->CredentialsByLogin($username);
				if(isset($credentials['password']) && $credentials['password']!="")
				{
					$sc = new Scrypt;
					$dpass = $sc->decrypt($credentials['password']);
					$hashed_password = hash("sha256",$dpass . $mobile_salt);
					if($password==$hashed_password)
					{
						$id_user = (int)$credentials['id_user'];
						$return['auth'] = $id_user>0;
						if($id_user>0)
						{
							$u->id = $id_user;
							$user = $u->UserGet();
							$return['name'] = $user['name'];
							$ah = new AdminHelper();
							$ah->auth->Set();
							$ah->session->Set("current_user_id",$id_user);
							$ah->session->Set("id_language",$user['id_language']);
						}
					}
					else
					{
						$return['error'] = "Authentication failed";
					}
				}
				else 
					$return['error'] = "Authentication failed";
			}
			else 
				$return['error'] = "This service is closed";
		}
		return $return;
	}

	/**
	 * Return articles for current user
	 *
	 * @return array
	 */
	public function GetArticles()
	{
		$return = array();
		$ah = new AdminHelper();
		$check = $ah->auth->Check();
		$return['auth'] = $check;
		if($check)
		{
			$id_user = $ah->session->Get("current_user_id");
			if($id_user>0)
			{
				$return['id_user'] = $id_user;
				include_once(SERVER_ROOT."/../classes/user.php");
				include_once(SERVER_ROOT."/../classes/topic.php");
				$u = new User();
				$u->id = $id_user;
				$user = $u->UserGet();
				$return['name'] = $user['name'];
				$userTopics = $u->Topics();
				$ini = new Ini();
				foreach($userTopics as $userTopic)
				{
					if($userTopic['id_topic'] != $ini->Get('temp_id_topic'))
					{
						$t = new Topic($userTopic['id_topic']);
						if ($t->AmIAdmin())
						{
							$articles = array();
							$numArticles = $t->ArticlesApproved( $articles, 0 );
							if($numArticles>0)
							{
								$topic = array('id'=>$t->id,'name'=>$t->name,'url'=>$t->url,'articles_pending'=>$numArticles);
								foreach($articles as $article)
								{
									$preview = $ini->Get("admin_web") . "/topics/preview.php?id_type=3&id_topic={$t->id}&id={$article['id_article']}";
									$topic_article = array('id'=>$article['id_article'],'headline'=>$article['headline'],'preview'=>$preview);
									$topic['articles'][] = $topic_article;
								}
								$return['topics'][] = $topic;
							}
						}								
					}
				}
			}
		}
		else
			$return['error'] = "User not authorised";
		return $return;
	}

}
?>
