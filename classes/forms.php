<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Manage dynamic forms' definition and submission 
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Forms
{

	/**
	 * Delete a specific payment account
	 * 
	 * @param integer $id_use
	 */
	public function AccountDelete($id_use)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");	
		$p = new Payment();
		$p->AccountUseDelete($id_use);
	}
	
	/**
	 * Get a specific payment account
	 * 
	 * @param integer $id_use
	 */
	public function AccountGet($id_use)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");	
		$p = new Payment();
		return $p->AccountUseGet($id_use);
	}

	/**
	 * Associate a specific payment account to a specific form
	 * If the association is already in place, it is updated, otherwise it's created
	 * A payment account can be used in multiple forms.
	 *  
	 * @param integer $id_form		Form ID
	 * @param integer $id_use		Use ID (1 campaigns - 2 forms)
	 * @param integer $id_account	Payment Account ID
	 * @return integer				Use ID
	 */
	public function AccountStore($id_form,$id_use,$id_account)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");		
		$p = new Payment();
		if($id_use>0)
			$p->AccountUseUpdate($id_use,$p->account_usage_types['form'],$id_form,$id_account);
		else
			$id_use = $p->AccountUseInsert($p->account_usage_types['form'],$id_form,$id_account);
		return $id_use;
	}

	/**
	 * Get a specific form action
	 * 
	 * @param integer $id_action
	 * @return array
	 */
	public function ActionGet($id_action)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT fa.score_min,fa.score_max,fa.redirect,fa.recipient,fa.id_action
			FROM form_actions fa
			WHERE id_action=$id_action  ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	/**
	 * Remove a specific action from a form
	 * 
	 * @param integer $id_action	Param ID
	 */
	public function ActionDelete($id_action)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "form_actions" );
		$res[] = $db->query( "DELETE FROM form_actions WHERE id_action='$id_action' " );
		Db::finish( $res, $db);
	}

	/**
	 * Store a form action definition
	 * 
	 * @param integer	$id_action			Action ID
	 * @param integer	$id_form			Form ID
	 * @param integer 	$score_min			Minimum score to trigger action
	 * @param integer 	$score_max			Maximum score to trigger action
	 * @param string	$recipient			Eemail recipient
	 * @param string 	$redirect			URL to redirect
	 * @return integer						Action ID
	 */
	public function ActionStore($id_action,$id_form,$score_min,$score_max,$recipient,$redirect)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "form_actions" );
		if($id_action>0)
			$sqlstr = "UPDATE form_actions SET score_min='$score_min',score_max='$score_max',recipient='$recipient',redirect='$redirect' WHERE id_action=$id_action ";
		else 
		{
			$id_action = $db->nextId("form_actions","id_action");
			$sqlstr = "INSERT INTO form_actions (id_action,id_form,score_min,score_max,recipient,redirect) 
					VALUES ($id_action,'$id_form','$score_min','$score_max','$recipient','$redirect') ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_action;
	}
	
	/**
	 * Get all actions associated to a specific form (paginated)
	 * 
	 * @param array $rows		Actions
	 * @param integer $id_form
	 * @return integer			Number of actions
	 */
	public function Actions(&$rows,$id_form)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT fa.score_min,fa.score_max,fa.redirect,fa.recipient,fa.id_action
			FROM form_actions fa
			WHERE id_form=$id_form 
			ORDER BY score_min,score_max ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Delete a specific form
	 * 
	 * @param integer $id_form	Form ID
	 */
	public function FormDelete($id_form)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "forms" );
		$res[] = $db->query( "DELETE FROM forms WHERE id_form='$id_form' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "form_posts" );
		$res[] = $db->query( "DELETE FROM form_posts WHERE id_form='$id_form' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirRemove("uploads/forms/$id_form");
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$p->AccountUseDeleteById($p->account_usage_types['form'],$id_form);
	}
	
	/**
	 * Get a specific form
	 * 
	 * @param integer $id_form
	 * @return array
	 */
	public function FormGet($id_form)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_form,id_topic,id_user,multilanguage,profiling,params,name,id_recipient,recipient,privacy_warning,
			id_payment_type,amount,editable,f.thanks,f.id_pt_group,f.store,f.captcha,f.require_auth,f.thanks_email,f.redirect,f.weights,f.hide_empty_fields
			FROM forms f
			WHERE f.id_form='$id_form' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Check whether a form has a specific parameter type
	 * 
	 * @param integer $id_form	Form ID
	 * @param integer $type		Type
	 * @return boolean
	 */
	public function FormHasParamType($id_form,$type)
	{
		$has_type = false;
		$params = $this->Params($id_form);
		foreach ($params as $param)
		{
			if($param['type']==$type)
			{
				$has_type = true;
			}
		}
		return $has_type;
	}
	
	/**
	 * Store a form definition
	 * 
	 * @param integer	$id_form			Form ID
	 * @param integer 	$id_topic			Associated topic ID
	 * @param boolint 	$multilanguage		Whether form labels are to be translated into multiple languages (requires the use of existing translator labels or custom ones)
	 * @param integer 	$profiling			Form profiling type: 1=Send email, 2 = Send email + profiling, 3 = Send email + profiling + funding 
	 * @param string 	$name				Form's name
	 * @param integer 	$current_user		Administrative user ID
	 * @param integer 	$id_recipient		Email recipient ID (admin user)
	 * @param string	$recipient			Specific email recipient
	 * @param boolint 	$privacy_warning	Whether to show a privacy warning
	 * @param integer 	$id_payment_type	If form is of funding type, the associated cost center ID
	 * @param string 	$amount				Payment amount options
	 * @param boolint 	$editable			Whether the payment amount can be edited by public user
	 * @param string	$thanks				Thanks messages to be shown after form submission
	 * @param integer 	$id_pt_group		People group ID to associate portal users
	 * @param boolint 	$store				Whether to store form posts
	 * @param boolint 	$captcha			Whether to show a captcha on form's submission page
	 * @param boolint 	$require_auth		Profiling option: whether forms require authentication, identification or nothing		
	 * @param string 	$thanks_email		Thanks messages to be sent via email
	 * @param string 	$redirect			URL to redirect after post
	 * @param boolint 	$weights			Whether answers are weighted to trigger actions
	 * @return integer						Form ID
	 */
	public function FormStore($id_form,$id_topic,$multilanguage,$profiling,$name,$current_user,$id_recipient,$recipient,$privacy_warning,$id_payment_type,$amount,$editable,$thanks,$id_pt_group,$store,$captcha,$require_auth,$thanks_email,$redirect,$weights,$hide_empty_fields)
	{
		if($profiling<1)
		{
			$id_pt_group = 0;
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "forms" );
		if($id_form>0)
			$sqlstr = "UPDATE forms SET id_topic=$id_topic,multilanguage=$multilanguage,
				profiling=$profiling,name='$name',id_recipient='$id_recipient',recipient='$recipient',privacy_warning='$privacy_warning',
				id_payment_type='$id_payment_type',amount='$amount',editable='$editable',thanks='$thanks',id_pt_group='$id_pt_group',
				store='$store',captcha='$captcha',require_auth='$require_auth',thanks_email='$thanks_email',redirect='$redirect',
				weights='$weights',hide_empty_fields='$hide_empty_fields' 
				WHERE id_form=$id_form ";
		else 
		{
			$id_form = $db->nextId("forms","id_form");
			$sqlstr = "INSERT INTO forms (id_form,id_topic,id_user,multilanguage,profiling,name,id_recipient,recipient,params,privacy_warning,id_payment_type,amount,editable,thanks,id_pt_group,store,captcha,require_auth,thanks_email,redirect,weights,hide_empty_fields) 
					VALUES ($id_form,$id_topic,$current_user,$multilanguage,$profiling,'$name','$id_recipient','$recipient','','$privacy_warning','$id_payment_type','$amount','$editable','$thanks','$id_pt_group','$store','$captcha','$require_auth','$thanks_email','$redirect','$weights','$hide_empty_fields') ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_form;
	}
	
	/**
	 * Get all forms associated to a specific topic
	 * 
	 * @param array		$rows		Forms
	 * @param integer	$id_topic	Topic ID
	 * @param boolean	$paged		Where results are paged
	 * @return integer
	 */
	public function FormsTopic(&$rows,$id_topic,$paged=true)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_form,f.name,t.name AS topic_name,f.multilanguage,
			f.profiling,f.id_user,f.store,f.id_topic AS form_topic
			FROM forms f
			LEFT JOIN topics t ON f.id_topic=t.id_topic ";
		if($id_topic>0)
			$sqlstr .= " WHERE f.id_topic=0 OR f.id_topic='$id_topic' ";
            
         $sqlstr .=" ORDER BY f.id_form DESC";   
            
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	/**
	 * Get gallery ID associated to a form containing a field of type Image Upload
	 * 
	 * @param integer $id_form
	 * @return integer $id_gallery
	 */
	public function Gallery($id_form)
	{
		$id_gallery = 0;
		$params = $this->Params($id_form);
		foreach($params as $param)
		{
			if($param['type']=="upload_image")
				$id_gallery = (int)$param['type_params'];
		}
		return $id_gallery;
	}
	
	/**
	 * Move a form parameter up / down
	 * 
	 * @param integer $id_form		Form ID
	 * @param integer $id_param		Parameter ID
	 * @param integer $direction	Direction (1 = up, 0 = down)
	 */
	public function MoveParam($id_form,$id_param,$direction)
	{
		$params = $this->Params($id_form);
		$keys = array_keys($params);
		$key1 = "fp_" . $id_param;
		$pos1 = array_search($key1,$keys);
		$pos2 = ($direction==1)? $pos1 - 1 : $pos1 + 1;
		$key2 = $keys[$pos2];
		$out = array();
		foreach($params as $pkey=>$pvalue)
		{
			if($pkey==$key1 || $pkey==$key2)
			{
				if($pkey==$key1)
					$out[$key2] = $params[$key2];
				else
					$out[$key1] = $params[$key1];
			}
			else 
				$out[$pkey] = $pvalue;
		}
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$ser_params = $v->Serialize($out);
		$this->ParamStore($id_form,$ser_params);
	}
	
	/**
	 * Get all parameters (fields) associated to a form
	 * (deserialized in an array, as they're stored as a string)
	 * 
	 * @param integer $id_form
	 * @return array
	 */
	public function Params($id_form)
	{
		$row = $this->FormGet($id_form);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$aparams = $v->Deserialize($row['params']);
		$current_params = (is_array($aparams))? $aparams : array();
		return $current_params;
	}
	
	/**
	 * Ge a specific parameter (field) definition
	 * 
	 * @param integer $id_param	Param ID
	 * @param integer $id_form	Form ID
	 * @return array
	 */
	public function Param($id_param,$id_form)
	{
		$fparam = array();
		$params = $this->Params($id_form);
		foreach($params as $param)
		{
			if($param['id_param']==$id_param)
				$fparam = $param;
		}
		return $fparam;
	}
	
	/**
	 * Remove a specific parameter (field) from a form definition
	 * 
	 * @param integer $id_param	Param ID
	 * @param integer $id_form	Form ID
	 */
	public function ParamDelete($id_param,$id_form)
	{
		$params = $this->Params($id_form);
		unset($params['fp_'.$id_param]);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$ser_params = $v->Serialize($params);
		$this->ParamStore($id_form,$ser_params);
	}

	/**
	 * Update a parameter definition
	 * 
	 * @param integer 	$id_form		Form ID
	 * @param integer 	$id_param		Parameter ID
	 * @param string	$label			Field label
	 * @param string	$value			Default value
	 * @param integer 	$id_type		Field type ID
	 * @param string 	$type_params	Additional parameters
	 * @param boolint 	$mandatory		Whether the field is mandatory
	 * @param boolint 	$email_subject	Whether the field's labels hsould be used in the subject of the notification email
	 * @return integer					Parameter ID
	 */
	public function ParamUpdate($id_form,$id_param,$label,$value,$id_type,$type_params,$mandatory,$email_subject,$type_weights)
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		$types =  $r->ParamsTypes(false,true,true);
		$type = $types[$id_type];
		$fparams = $this->Params($id_form);
		if($id_param==0)
		{
			$id_param = 1;
			foreach($fparams as $fparam)
			{
				if($fparam['id_param']>=$id_param)
					$id_param = $fparam['id_param'] + 1;
			}
		}
		$fparams['fp_' . $id_param] = array('id_param'=>$id_param,'label'=>$label,'value'=>$value,'type'=>$type,'type_params'=>$type_params,'mandatory'=>$mandatory,'email_subject'=>$email_subject,'type_weights'=>$type_weights);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$ser_array = $v->Serialize($fparams);
		$this->ParamStore($id_form,$ser_array);
		return $id_param;
	}
	
	/**
	 * Store a form parameter definition
	 * 
	 * @param integer 	$id_form
	 * @param string	$serialized_params
	 */
	public function ParamStore($id_form,$serialized_params)
	{
		$sqlstr = "UPDATE forms SET params='$serialized_params' WHERE id_form='$id_form' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "forms" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Delete a specific form post
	 * 
	 * @param integer $id_post
	 */
	public function PostDelete($id_post)
	{
		$post = $this->PostGet($id_post);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "form_posts" );
		$res[] = $db->query( "DELETE FROM form_posts WHERE id_post='$id_post' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->Delete("uploads/forms/{$post['id_form']}/{$id_post}.{$post['ext']}");
	}
	
	/**
	 * Get a specific form post
	 * 
	 * @param integer $id_post
	 * @return array
	 */
	public function PostGet($id_post)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_p,id_form,id_topic,post,ip,UNIX_TIMESTAMP(post_time) AS post_time_ts,ext,id_payment,score
			FROM form_posts
			WHERE id_post='$id_post' ";
		$db->query_single($row,$sqlstr);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$post = $v->Deserialize($row['post']);
		$row['post'] = is_array($post)? $post: array();
		return $row;
	}

	/**
	 * Get the action triggered by a specific post score
	 * 
	 * @param integer 	$id_form	Form ID
	 * @param integer 	$score	Post score
	 * @return array				Action
	 */
	public function PostAction($id_form,$score)
	{
		
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "	SELECT id_action,redirect,recipient,score_max - score_min AS diff 
			FROM form_actions 
			WHERE id_form=$id_form AND $score>=score_min AND $score<=score_max 
			ORDER BY diff LIMIT 1 ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Calculate the score of a post for a weighted form
	 * 
	 * @param integer 	$id_form	Form ID
	 * @param array		$params		Submitted parameters' values
	 * @return integer				Post score
	 */
	public function PostScore($id_form,$params)
	{
		$score = 0;
		$fparams = $this->Params($id_form);
    	foreach($fparams as $fparam)
    	{
    		if($fparam['type']=="checkbox" || $fparam['type']=="dropdown" || $fparam['type']=="mchoice" || $fparam['type']=="dropdown_open" || $fparam['type']=="mchoice_open" || $fparam['type']=="radio")
    		{
    			$weights = explode(",",$fparam['type_weights']);
    			if(is_array($weights) && count($weights)>0)
    			{
    				foreach($params as $param)
    				{
    					if($param['name'] == $fparam['label'])
    					{
    						$value = $param['value'];
    						if($fparam['type']=="checkbox")
    						{
    							$score += $value=="ok"? $weights[0] : (int)$weights[1]; 
    						}
    						else 
    						{
    							$values = explode(",",$fparam['type_params']);
                                if(is_array($values))
    							{
    								$value = explode(",",$value);
                                    $param_values = is_array($value) ? $value : array($value);
                                    foreach($param_values as $param_value)
                                    {
                                        if (in_array($param_value, $values))
                                        {
                                            $value_position = array_search($param_value, $values);
                                            if(isset($weights[$value_position]))
                                            {
                                                $score += (int)$weights[$value_position];
                                            }
                                        }
                                    }
    							}
    							else
    							{
    								$no_value_position = count($values);
    								if(isset($weights[$no_value_position]))
    									$score += (int)$weights[$no_value_position];
    							}
    						}
    					}
    				}
    			}
    		}
    	}
    	return $score;
	}
	
	/**
	 * Store a form post
	 * 
	 * @param integer 	$id_form	Form ID
	 * @param integer 	$id_topic	Topic ID
	 * @param integer 	$id_p		People ID
	 * @param array		$params		Submitted parameters' values
	 * @param integer 	$id_payment	Payment transaction ID
	 * @return integer				Form post ID
	 */
	public function PostStore($id_form,$id_topic,$id_p,$params,$id_payment,$score)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$post = $v->Serialize($params);
		$db =& Db::globaldb();
		$today = $db->getTodayTime();
        $miliSecNow = $db->getMilliseconds();
		$db->begin();
		$db->lock( "form_posts" );
		$id_post = $db->nextId("form_posts","id_post");
		$sqlstr = "INSERT INTO form_posts (id_post,id_p,id_form,id_topic,post,ip,post_time,id_payment,score,post_time_milisecond) 
				VALUES ($id_post,'$id_p','$id_form','$id_topic','$post','$ip','$today','$id_payment','$score', '$miliSecNow') ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_post;
	}
	
	/**
	 * Update the extension of the file uploaded in a form submission
	 * 
	 * @param integer $id_post
	 * @param string $ext
	 */
	public function PostUpdate($id_post,$ext)
	{
		$db =& Db::globaldb();
		$ext = $db->SqlQuote($ext);
		$db->begin();
		$db->lock( "form_posts" );
		$sqlstr = "UPDATE form_posts SET ext='$ext' WHERE id_post=$id_post ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Get all posts associated to a specific form
	 * 
	 * @param integer $id_form
	 * @param integer $id_topic
	 * @return array
	 */
	public function PostsAll($id_form,$id_topic)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT fp.id_post,fp.id_form,fp.id_topic,fp.post,fp.ip,
			UNIX_TIMESTAMP(fp.post_time) AS post_time_ts,fp.ext,
			fp.id_p,p.name1,p.name2,p.name3,fp.score
			FROM form_posts fp
			LEFT JOIN people p ON fp.id_p=p.id_p 
			WHERE fp.id_form=$id_form 
			GROUP BY fp.id_post
			ORDER BY fp.post_time ASC ";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}

	/**
	 * Get all posts associated to a specific form (paginated)
	 * 
	 * @param array $rows		Posts
	 * @param integer $id_form
	 * @param integer $id_topic
	 * @return integer			Number of posts
	 */
	public function Posts(&$rows,$id_form,$id_topic)
	{
		$db =& Db::globaldb();
		//$sqlstr = "SELECT fp.id_post,UNIX_TIMESTAMP(fp.post_time) AS post_time_ts,CONCAT(p.name1,' ',p.name2) AS name,t.name AS topic_name,fp.score
		//	FROM form_posts fp
		//	LEFT JOIN people p ON fp.id_p=p.id_p 
		//	LEFT JOIN topics t ON fp.id_topic=t.id_topic
		//	WHERE id_form=$id_form ";
        
        $sqlstr = "SELECT fp.id_post,UNIX_TIMESTAMP(fp.post_time) AS post_time_ts , fp.post_time_milisecond as miliSec,CONCAT(p.name1,' ',p.name2) AS name,t.name AS topic_name,fp.score
            FROM form_posts fp
            LEFT JOIN people p ON fp.id_p=p.id_p 
            LEFT JOIN topics t ON fp.id_topic=t.id_topic
            WHERE id_form=$id_form ";
        
		if($id_topic>0)
			$sqlstr .= " AND fp.id_topic=$id_topic ";
		$sqlstr .= " ORDER BY fp.post_time DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Retrieve minimum and maximum timestamp of articles insertion date
	 *
	 * @return array
	 */
	public function PostsPeriod($id_form)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,	"SELECT UNIX_TIMESTAMP(MIN(post_time))-86400 AS min_post_ts,UNIX_TIMESTAMP(MAX(post_time))+86400 AS max_post_ts FROM form_posts WHERE id_form=$id_form ");
		return $row;
	}

	/**
	 * Get all posts submitted by a specific portal user
	 * 
	 * @param integer $id_p	People ID
	 * @return array		Posts
	 */
	public function PostsPerson( $id_p )
	{
		$sqlstr = "SELECT fp.id_form,fp.id_topic,UNIX_TIMESTAMP(fp.post_time) AS hdate_ts,fp.id_post,
			'post' AS item_type,f.name
		    FROM form_posts fp
		    INNER JOIN forms f ON fp.id_form=f.id_form
		    WHERE fp.id_p=$id_p 
		    ORDER BY fp.post_time DESC";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
    
    public function PostsSummary(&$rows,$id_form,$id_topic,$start_date,$end_date,$by_day)
    {
        $db =& Db::globaldb();
        
        $sqlstr = "SELECT ";
        
        if ($by_day)
            $sqlstr .= "UNIX_TIMESTAMP(DATE(post_time)) AS post_date_ts, DATE(post_time) AS start_date, DATE(post_time) AS end_date, ";
        else
            $sqlstr .= "'$start_date' AS start_date, '$end_date' AS end_date, ";
        
        $sqlstr .= "COUNT(*) AS count
            FROM form_posts 
            WHERE id_form=$id_form ";
        
        if($id_topic>0)
            $sqlstr .= "AND id_topic=$id_topic ";
                                
        if (strtotime($start_date) > 0)
        {
            $start_date = date('Y-m-d', strtotime($start_date));
            $sqlstr .= "AND post_time >= '{$start_date}' ";
        }
        if (strtotime($end_date) > 0)
        {
            $end_date = date('Y-m-d', strtotime("+1day",strtotime($end_date)));
            $sqlstr .= "AND post_time < '{$end_date}' ";
        }
        if ($by_day)
            $sqlstr .= "GROUP BY DATE(post_time) ORDER BY DATE(post_time)";

        return $db->QueryExe($rows, $sqlstr, true);
    }
    
    public function PostsDetail(&$rows,$id_form,$id_topic,$start_date,$end_date,$paged=true)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT fp.id_post,fp.id_form,fp.id_topic,fp.post,fp.ip,
            UNIX_TIMESTAMP(fp.post_time) AS post_time_ts,fp.ext,
            fp.id_p,p.name1,p.name2,p.name3,fp.score
            FROM form_posts fp
            LEFT JOIN people p ON fp.id_p=p.id_p 
            WHERE fp.id_form=$id_form ";
        if($id_topic>0)
            $sqlstr .= " AND fp.id_topic=$id_topic ";
                                
        if (strtotime($start_date) > 0)
        {
            $start_date = date('Y-m-d', strtotime($start_date));
            $sqlstr .= " AND fp.post_time >= '{$start_date}' ";
        }
        if (strtotime($end_date) > 0)
        {
            $end_date = date('Y-m-d', strtotime("+1day",strtotime($end_date)));
            $sqlstr .= " AND fp.post_time < '{$end_date}' ";
        }
        $sqlstr .= " ORDER BY fp.post_time ";
        return $db->QueryExe($rows, $sqlstr, $paged);
    }

	/**
	 * Get all subtopics using a specific form
	 * 
	 * @param integer $id_form
	 * @return array
	 */
	public function Subtopics($id_form)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT s.id_subtopic,s.id_topic,t.name AS topic_name,s.name
			FROM subtopics s
			INNER JOIN topics t ON s.id_topic=t.id_topic 
			WHERE s.id_type=5 AND s.id_item=$id_form";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Parse a feedback message and substitues all parameters' labels with their values
	 * 
	 * @param array 	$form_params	Form parameters definition
	 * @param array 	$params_values	Submitted values
	 * @param string	$message		Feedback message
	 * @param integer	$id_post		Post ID
	 * @return string					Feedback message
	 */
	public function ThankParams($form_params,$params_values,$message,$id_post)
	{
		foreach($form_params as $form_param)
		{
			$id_param = $form_param['id_param'];
			$message = str_replace("[[FP{$id_param}]]", $params_values[$id_param], $message);
		}
		if($id_post>0)
			$message = str_replace("[[ID]]", $id_post, $message);
		return $message;
	}
}
?>
