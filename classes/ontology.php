<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/resources.php");

class Ontology
{
	public $types;

	private $tree = array();

	function __construct()
	{
		$r = new Resources;
		$this->types = $r->types;
	}

	public function Count()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT count(id_keyword) AS count_keywords from keywords ");
		return $row['count_keywords'];
	}

	public function GeoTop()
	{
		$top = array();
		foreach($this->tree as $keyword)
			if ($keyword['id_domain']==0)
				$top[] = $keyword;
		return $top[0]['id_keyword'];
	}

	public function GetKeywords( $id, $id_type, &$keywords, $keyword_type=0, $exclude_internal=true )
	{
		if($id>0)
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT keyword,k.id_keyword,k.id_type FROM keywords k 
				INNER JOIN keywords_use ku USING(id_keyword)
				WHERE ku.id='$id' AND ku.id_type='$id_type' ";
			if($keyword_type>0)
				$sqlstr .= " AND k.id_type='$keyword_type' ";
			if($exclude_internal)
				$sqlstr .= " AND k.id_type<>4 ";
			$db->array_query_append( $keywords, $sqlstr, count($keywords));
		}
		return $keywords;
	}

	public function InsertKeywords($keywords_string, $id_item, $id_type,$keyword_type=1)
	{
		$id_keywords = array();
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$this->UseDelete( $id_item, $id_type);
		$keywords = explode(",",$keywords_string);
		if(count($keywords)>0) {
			foreach($keywords as $keyword)
			{
				$keyword = strtolower(trim($keyword));
				if ($keyword!="")
				{
					$k = new Keyword();
					$row = $k->KeywordCheck($keyword, $keyword_type);
					$this->UseAdd($row['id_keyword'], $id_item, $id_type);
					$id_keywords[] = $row['id_keyword'];
				}
			}
			$this->Propagate($id_keywords,$id_type);
		}
	}
	
	public function KeywordRemove($id, $id_type, $id_keyword) {
		$res = array();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_use" );
		$res[] = $db->query( "DELETE FROM keywords_use WHERE id=$id AND id_type=$id_type AND id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}

	public function InsertKeywordsArray($keywords,$id_item,$id_type)
	{
		if(count($keywords)>0)
		{
			$id_keywords = array();
			foreach($keywords as $keyword)
			{
				$this->UseAdd($keyword['id_keyword'], $id_item, $id_type);
				$id_keywords[] = $keyword['id_keyword'];
			}
			$this->Propagate($id_keywords,$id_type);
		}
	}
	
	private function Propagate($id_keywords,$id_type)
	{
		$types = $this->types;
		if($id_type==$types['org'] || $id_type==$types['article'] || $id_type==$types['event'] || $id_type==$types['book'] || $id_type==$types['link'])
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$db =& Db::globaldb();
			$sqlstr = "SELECT DISTINCT id_topic FROM subtopics WHERE id_type=15 AND id_item=$id_type ";
			foreach($id_keywords as $id_keyword)
				$sqlstr .= " AND id_subitem=$id_keyword ";
			$topics = array();
			$db->QueryExe($topics, $sqlstr);
			foreach($topics as $topic)
			{
				$t = new Topic($topic['id_topic']);
				foreach($id_keywords as $id_keyword)
				{
					$subtopics = array();
					$sqlstr = "SELECT id_subtopic FROM subtopics WHERE id_topic={$topic['id_topic']} AND id_type=15 AND id_item=$id_type AND id_subitem=$id_keyword ";
					$db->QueryExe($subtopics, $sqlstr);
					foreach($subtopics as $subtopic)
						$t->queue->JobInsert($t->queue->types['subtopic'],$subtopic['id_subtopic'],"update");
				}
			}
		}
		// Propagate event keywords to articles
		if($id_type==$types['event']) {
			include_once(SERVER_ROOT."/../classes/pagetypes.php");
			include_once(SERVER_ROOT."/../classes/article.php");
			include_once(SERVER_ROOT."/../classes/topic.php");
			$pt = new PageTypes();
			$features = $pt->FeaturesByFunction(24);
			foreach($features as $feature) {
				if($feature['condition_id']>0 && $feature['id_type']=="3") {
					$a = new Article($feature['condition_id']);
					$a->ArticleLoad();
					$t = new Topic($a->id_topic);
					$t->queue->JobInsert($t->queue->types['article'],$feature['condition_id'],"update");
				}
			}
		}
	}

	public function KeywordsByType( $id_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword,keyword FROM keywords WHERE id_type=$id_type ORDER BY keyword";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function KeywordChildren($id_keyword)
	{
		$children = array();
		$child_counter = 0;
		for($i=0;$i<count($this->tree);$i++)
		{
			if ($this->tree[$i]['id_domain']==$id_keyword)
			{
				$children[$child_counter]['id_keyword'] = $this->tree[$i]['id_keyword'];
				$children[$child_counter]['keyword'] = $this->tree[$i]['keyword'];
				$children[$child_counter]['id_domain'] = $this->tree[$i]['id_domain'];
				$child_counter = $child_counter + 1;
			}
		}
		return $children;
	}
	
	private function KeywordData($id_keyword)
	{
		$keyword_data = array();
		foreach($this->tree as $keyword)
		{
			if ($keyword['id_keyword']==$id_keyword)
			{
				$keyword_data['id_keyword']		= $keyword['id_keyword'];
				$keyword_data['keyword']		= $keyword['keyword'];
				$keyword_data['id_domain']		= $keyword['id_domain'];
			}
		}
		return $keyword_data;
	}

	public function KeywordFeedDelete($id_keyword)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_feeds" );
		$res[] = $db->query( "DELETE FROM keywords_feeds WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}

	public function KeywordFeedGet($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT kf.id_keyword,kf.description,k.keyword,kf.public FROM keywords_feeds kf 
			INNER JOIN keywords k ON kf.id_keyword=k.id_keyword
			WHERE kf.id_keyword=$id_keyword";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function KeywordFeedInsert($id_keyword,$description,$public)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_feeds" );
		$res[] = $db->query( "INSERT INTO keywords_feeds (id_keyword,description,public) VALUES ($id_keyword,'$description','$public')" );
		Db::finish( $res, $db);
	}
	
	public function KeywordFeedUpdate($id_keyword,$description,$public)
	{	
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_feeds" );
		$res[] = $db->query( "UPDATE keywords_feeds SET description='$description',public='$public' WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}
	
	public function KeywordLandingDelete($id_keyword)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_landing" );
		$res[] = $db->query( "DELETE FROM keywords_landing WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}
	
	public function KeywordLandingGet($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT kl.id_keyword,kl.content,k.keyword,kl.youtube,k.description,k.id_type 
			FROM keywords_landing kl
			INNER JOIN keywords k ON kl.id_keyword=k.id_keyword
			WHERE kl.id_keyword=$id_keyword";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function KeywordLandingInsert($id_keyword,$content,$youtube)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_landing" );
		$res[] = $db->query( "INSERT INTO keywords_landing (id_keyword,content,youtube) VALUES ($id_keyword,'$content','$youtube')" );
		Db::finish( $res, $db);
	}
	
	public function KeywordLandingUpdate($id_keyword,$content,$youtube)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_landing" );
		$res[] = $db->query( "UPDATE keywords_landing SET content='$content',youtube='$youtube' WHERE id_keyword=$id_keyword" );
		Db::finish( $res, $db);
	}
	
	public function KeywordNeighbours($id_keyword,$strict=false)
	{
		$neighbours = array();
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT s.id_domain,k.keyword 
			FROM statements s
			INNER JOIN keywords k ON s.id_domain=k.id_keyword 
			WHERE s.id_range='$id_keyword' AND (s.id_relation=4)";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
			$neighbours[$row['id_domain']] = $row['keyword'];
		$rows2 = array();
		$sqlstr2 = "SELECT s.id_range,k.keyword 
			FROM statements s
			INNER JOIN keywords k ON s.id_range=k.id_keyword 
			WHERE s.id_domain='$id_keyword' AND ";
		$sqlstr2 .= $strict? "(s.id_relation=4)":"(s.id_relation=4 OR s.id_relation=3)";
		$db->QueryExe($rows2, $sqlstr2);
		foreach($rows2 as $row2)
			$neighbours[$row2['id_range']] = $row2['keyword'];
		return $neighbours;
	}
	
	private function KeywordParents($id_keyword, &$parents)
	{
		$keyword = $this->KeywordData($id_keyword);
		$parents[] = array(	'id_keyword'	=> $keyword['id_keyword'],
					'keyword'	=> $keyword['keyword'],
					'id_domain'	=> $keyword['id_domain']);
		if ($keyword['id_domain']>0)
			$parents = $this->KeywordParents($keyword['id_domain'], $parents);
		return $parents;
	}
	
	public function KeywordPath($id_keyword)
	{
		$parents = array();
		$this->KeywordParents($id_keyword,$parents);
		return array_reverse($parents);
	}

	public function Keywords()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword,keyword FROM keywords ORDER BY keyword";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function KeywordsFeeds( &$rows, $paged=true, $only_public=false )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT kf.id_keyword,k.keyword,kf.description,k.id_type,kf.public
		 FROM keywords_feeds kf
		 INNER JOIN keywords k ON kf.id_keyword=k.id_keyword ";
		if($only_public)
			$sqlstr .= " WHERE kf.public=1 ";
		$sqlstr .= " ORDER BY k.keyword";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function KeywordsLanding( &$rows, $paged=true )
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT kl.id_keyword,k.keyword,kl.content,k.description,k.id_type,kl.youtube
		 FROM keywords_landing kl
		 INNER JOIN keywords k ON kl.id_keyword=k.id_keyword 
		 ORDER BY k.keyword";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function LoadTree($id_type)
	{
		$tree = array();
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword,keyword,id_domain FROM keywords LEFT JOIN statements ON keywords.id_keyword=statements.id_range AND statements.id_relation=3 WHERE id_type=$id_type ORDER BY keyword";
		$db->QueryExe($rows, $sqlstr);
		$counter = 0;
		foreach($rows as $keyword)
		{
			$tree[$counter]['id_keyword'] = $keyword['id_keyword'];
			$tree[$counter]['keyword'] = $keyword['keyword'];
			$tree[$counter]['id_domain'] = (int)$keyword['id_domain'];
			$counter ++;
		}
		$this->tree = $tree;
	}

	public function Merge($id_keyword_from,$id_keyword_to)
	{
		$return = false;
		if($id_keyword_from>0 && $id_keyword_to>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "keywords_use" );
			$res[] = $db->query( "UPDATE keywords_use SET id_keyword='$id_keyword_to' WHERE id_keyword='$id_keyword_from' " );
			Db::finish( $res, $db);
			$db->begin();
			$db->lock( "statements" );
			$res[] = $db->query( "DELETE FROM statements WHERE id_domain='$id_keyword_from' OR id_range='$id_keyword_from'  " );
			Db::finish( $res, $db);
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$k->KeywordDelete($id_keyword_from);
			$return = true;
		}
		return $return;
	}
	
	public function Orphans( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = " SELECT k.id_keyword,keyword
			FROM keywords k
			LEFT JOIN keywords_use USING(id_keyword)
			LEFT JOIN statements s1 ON s1.id_domain=k.id_keyword
			LEFT JOIN statements s2 ON s2.id_range=k.id_keyword
			WHERE keywords_use.id IS NULL AND s1.id_domain IS NULL AND s2.id_range IS NULL ORDER BY keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function NotUsed( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT k.id_keyword,keyword FROM keywords k 
			WHERE k.id_keyword NOT IN (SELECT DISTINCT id_keyword FROM keywords_use) ORDER BY k.id_keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function NotUsedRemove()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("keywords","keywords_use"));
		$res[] = $db->query( "DELETE FROM keywords WHERE id_keyword NOT IN (SELECT DISTINCT id_keyword FROM keywords_use) " );
		Db::finish( $res, $db);
	}
	
	public function Parents($id_keyword, &$geo_path)
	{
		$geo_data = $this->geoData($id_keyword);
		$geo_path[] = array($geo_data['id_keyword'], $geo_data['keyword'], $geo_data['id_domain']);
		if ($geo_data['id_domain']>0)
			$geo_path = $this->Parents($geo_data['id_domain'], $geo_path);
		return $geo_path;
	}

	public function RelationGet($id_relation)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name FROM relations WHERE id_relation=$id_relation";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function RelationInsert( $name  )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "relations" );
		$nextId = $db->nextId( "relations", "id_relation" );
		$res[] = $db->query( "INSERT INTO relations (id_relation,name) VALUES ($nextId,'$name')" );
		Db::finish( $res, $db);
	}

	public function RelationUpdate( $id_relation, $name )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "relations" );
		$res[] = $db->query( "UPDATE relations SET name='$name' WHERE id_relation=$id_relation" );
		Db::finish( $res, $db);
	}

	public function Relations( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT relations.id_relation,name,COUNT(statements.id_relation) as counter
		FROM relations LEFT JOIN statements USING (id_relation) GROUP BY relations.id_relation ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function RelationsAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_relation,name FROM relations ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function StatementDelete($id_statement)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "statements" );
		$res[] = $db->query( "DELETE FROM statements WHERE id_statement=$id_statement" );
		Db::finish( $res, $db);
	}

	public function StatementGet($id_statement)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_domain,id_relation,id_range FROM statements WHERE id_statement=$id_statement";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function StatementInsert( $id_domain,$id_relation,$id_range )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "statements" );
		$nextId = $db->nextId( "statements", "id_statement" );
		$res[] = $db->query( "INSERT INTO statements (id_statement,id_domain,id_relation,id_range)
			VALUES ($nextId,$id_domain,$id_relation,$id_range)" );
		Db::finish( $res, $db);
	}

	public function StatementUpdate( $id_statement,$id_domain,$id_relation,$id_range )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "statements" );
		$res[] = $db->query( "UPDATE statements SET id_domain=$id_domain,id_relation=$id_relation,id_range=$id_range 
			WHERE id_statement=$id_statement" );
		Db::finish( $res, $db);
	}

	public function Statements( &$rows, $id_relation )
	{
		$db =& Db::globaldb();
		$rows = array();
		if ($id_relation>0)
			$cond = " WHERE s.id_relation=$id_relation";
		$sqlstr = "SELECT id_statement,k1.keyword AS keyword_domain,r.name,
			k2.keyword AS keyword_range,k1.id_keyword AS id_keyword_domain,k2.id_keyword AS id_keyword_range
		FROM statements s
		INNER JOIN relations r ON s.id_relation=r.id_relation
		INNER JOIN keywords k1 ON s.id_domain=k1.id_keyword
		INNER JOIN keywords k2 ON s.id_range=k2.id_keyword
		$cond ORDER BY s.id_statement DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function StatementsBrokenRemove()
	{
		$tot = 0;
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("statements","keywords"));
		$res[] = $db->query( "DELETE FROM statements WHERE id_domain NOT IN (SELECT id_keyword FROM keywords) ",true );
		$tot += $db->affected_rows;
		$res[] = $db->query( "DELETE FROM statements WHERE id_range NOT IN (SELECT id_keyword FROM keywords) ",true );
		$tot += $db->affected_rows;
		Db::finish( $res, $db);
		return $tot;
	}
	
	public function Search( &$rows, $params )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword,keyword,description,id_type FROM keywords k 
			WHERE 1=1 ";
		if ($params['id_type']>0)
			$sqlstr .= " AND k.id_type={$params['id_type']} ";
		if (strlen($params['q']) > 0)
			$sqlstr .= " AND (keyword LIKE '%{$params['q']}%' OR description LIKE '%{$params['q']}%') ";
		$sqlstr .= "  ORDER BY keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function TagCloud($id_topic,$id_subtopic=0,$limit=0,$resource_type=5,$keyword_type=1)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,COUNT(ku.id_keyword) AS occurr";
		if($id_subtopic>0)
			$sqlstr .= ",$id_subtopic AS id_subtopic ";
		$sqlstr .= "FROM keywords k 
			INNER JOIN keywords_use ku ON k.id_keyword=ku.id_keyword AND ku.id_type={$resource_type} AND k.id_type={$keyword_type} ";
		if($id_topic>0)
		{
			$sqlstr .= " INNER JOIN articles a ON ku.id=a.id_article WHERE a.approved=1 AND a.id_topic={$id_topic} ";
			if($id_subtopic)
				$sqlstr .= " AND a.id_subtopic={$id_subtopic}"; 
		}
		$sqlstr .= " GROUP BY k.id_keyword ORDER BY occurr DESC";
		if($limit>0)
			$sqlstr .= " LIMIT $limit";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function Top( &$rows, $id_type, $id_relation )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT keywords.id_keyword,keyword,description FROM keywords
		LEFT JOIN statements ON keywords.id_keyword=statements.id_range
		AND statements.id_relation=$id_relation WHERE id_type=$id_type AND id_domain IS NULL ORDER BY keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function Tree($id_keyword)
	{
		$tree = array();
		$children = $this->KeywordChildren($id_keyword);
		if(count($children)>0)
			$tree = $this->TreeRecurse($children);
		return $tree;
	}
	
	private function TreeRecurse($children)
	{
		$subtree = array();
		foreach($children as $child)
		{
			$children = $this->KeywordChildren($child['id_keyword']);
			if(count($children)>0)
				$child['sub'] = $this->TreeRecurse($children);
			$subtree[] = $child;
		}
		return $subtree;
	}

	public function KeywordsByTypePaged( &$rows, $id_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description,k.id_type
		 FROM keywords k ";
		if ($id_type>0)
			$sqlstr .= "WHERE k.id_type=$id_type ";
		$sqlstr .= " ORDER BY k.keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function GeoData($id_keyword)
	{
		$geo_data = array();
		foreach($this->tree as $keyword)
			if ($keyword['id_keyword']==$id_keyword)
			{
				$geo_data['id_keyword'] = $keyword['id_keyword'];
				$geo_data['keyword'] = $keyword['keyword'];
				$geo_data['id_domain'] = $keyword['id_domain'];
			}
		return $geo_data;
	}

	public function UseAdd($id_keyword, $id_item, $id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_use" );
		$res[] = $db->query( "INSERT INTO keywords_use (id_keyword, id, id_type) VALUES ($id_keyword, $id_item, $id_type)" );
		Db::finish( $res, $db);
	}

	public function UseDelete($id_item, $id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "keywords_use" );
		$res[] = $db->query( "DELETE FROM keywords_use WHERE id=$id_item AND id_type=$id_type" );
		Db::finish( $res, $db);
	}

	public function UseInternal( $id_item, $id_res_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ku.id_keyword 
			FROM keywords_use ku 
			INNER JOIN keywords k ON ku.id_keyword=k.id_keyword AND k.id_type=4 
			WHERE ku.id='$id_item' AND ku.id_type='$id_res_type'
			GROUP BY ku.id_keyword 
			ORDER BY ku.id_keyword ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

}
?>
