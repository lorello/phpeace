<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/config.php");

class Validator
{
	/**
	 * All tests passed
	 *
	 * @var  bool
	 */
	public $return;
	/** 
	 * @var Translator */
	public $tr;
	
	public $feedback;

	/** 
	 * @var Session */
	private $session;
	
	private $is_admin;
	private $id_module;
	private $formspam_log;
	private $recaptcha;
	private $recaptcha_private_key;
	private $recaptcha_version;
	private $stopforumspam;
	private $stopforumspam_key;
	private $id_p;
	
	function __construct($feedback=false,$id_module=0,$is_admin=true)
	{
		$conf = new Configuration();
		$this->formspam_log = $conf->Get("formspam_log");
		$this->recaptcha_private_key = $conf->Get("recaptcha_private_key");
		$this->recaptcha_version = $conf->Get("recaptcha_version");
		$this->recaptcha = $conf->Get("recaptcha");
		$this->stopforumspam = $conf->Get("stopforumspam");
		$this->stopforumspam_key = $conf->Get("stopforumspam_key");
		$this->session = new Session();
		$this->return = true;
		$this->id_p = 0;
		$this->feedback = $feedback;
		$this->is_admin = $is_admin;
		$this->id_module = $id_module;
		if ($feedback)
		{
			if($is_admin)
				$id_language = $this->session->Get("id_language");
			else
			{
				$user = $this->session->Get("user");
				if(isset($user['id_language'])) 
				{
					$id_language = $user['id_language'];
				}
				if(isset($user['id'])) {
					$this->id_p = (int)$user['id'];
				}
			}
			$this->tr = new Translator($id_language,$id_module,$is_admin);
		}
	}
	
	public function DateInFuture($timestamp)
	{
		if($timestamp>0)
		{
			if($timestamp < (time() - 86400))
			{
				$this->return = false;
				$this->MessageSet("error","error_date_past",array());
			}
		}
		else
		{
			$this->return = false;
			$this->MessageSet("error","error_date_invalid",array());
		}
	}
	
	public function Email($email,$varname="email",$set_msg=true)
	{
		$checkemail = false;
		if (preg_match('/([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i', $email ) && ctype_print($email) && $email!="")
		{
			$checkemail = true;
		}
		else
		{
			if ($set_msg)
            {
                $this->return = false;
                $this->PostError($varname);
                $this->MessageSet("error","error_email",array($email));
            }
		}
		return $checkemail;
	}		
	
	public function EmailAdvanced($email,$varname="email",$set_msg=true,$check_dns=false)
	{
		$isValid = true;
		$atIndex = strrpos($email, "@");
		if (is_bool($atIndex) && !$atIndex)
		{
			$isValid = false;
		}
		else
		{
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			if ($localLen < 1 || $localLen > 64)
			{
				// local part length exceeded
				$isValid = false;
			}
			else if ($domainLen < 1 || $domainLen > 255)
			{
				// domain part length exceeded
				$isValid = false;
			}
			else if ($local[0] == '.' || $local[$localLen-1] == '.')
			{
				// local part starts or ends with '.'
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $local))
			{
				// local part has two consecutive dots
				$isValid = false;
			}
			else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
			{
				// character not valid in domain part
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $domain))
			{
				// domain part has two consecutive dots
				$isValid = false;
			}
			else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',str_replace("\\\\","",$local)))
			{
				// character not valid in local part unless local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/',str_replace("\\\\","",$local)))
				{
					$isValid = false;
				}
			}
			if ($check_dns && $isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
			{
				// domain not found in DNS
				$isValid = false;
			}
		}
		if ($set_msg && !$isValid)
        {
            $this->return = false;
            $this->PostError($varname);
            $this->MessageSet("error","error_email",array($email));
        }
		return $isValid;
	}
	
	public function RealAccountNumber($acct_no,$varname="acct_no",$set_msg=true,$prefix_zero=true)
	{
		$check = false; 
        
        if ($prefix_zero)
            $match_str = '/^08[0-9]{7,7}/';
        else
            $match_str = '/^8[0-9]{7,7}/';
           
		if (preg_match($match_str, $acct_no ) && ctype_print($acct_no) && $acct_no!="")
		{                          
			$check = true;
		}
		else
		{
			if ($set_msg)
            {
                $this->return = false;
                $this->PostError($varname);
            }
		}
		return $check;
	}		
	
	public function FeedbackAdd($type,$message,$params=array(),$to_translate=true)
	{
		$fmsg = $this->session->Get("feedback");
		if (is_array($fmsg) && count($fmsg)>0)
			$smsg = $fmsg;
		else
			$smsg = array();
			
		$temp = array('type'=>$type,'text'=>$message,'params'=>$params,'tra'=>$to_translate,'id_module'=>$this->id_module);
		
		if(!in_array($temp, $smsg))
			$smsg[] = $temp;
			
		$this->session->Set("feedback",$smsg);
	}
	
	public function FirstTime($session_var)
	{
		$isset = $this->session->IsVarSet($session_var);
		if($isset)
			$this->return = false;
		return !$isset;
	}
	
	public function GreaterThenZero($myvar,$varname="")
	{
		if(!$myvar>0)
		{
			$this->return = false;
			if($varname!="")
			{
				$this->PostError($varname);
				$this->MessageSet("error","error_var_empty",array($this->tr->Translate($varname)));
			}
		}
	}
	
	public function IsTrue($var)
	{
		if(!$var)
			$this->return = false;
		return $var;
	}
	
	public function MessageSet($type,$msg,$params=array())
	{
		if($this->feedback)
		{
			if($this->is_admin)
			{
				include_once(SERVER_ROOT."/../classes/adminhelper.php");
				$ah = new AdminHelper();
				if(isset($this->tr))
				{
					$msg = $this->tr->TranslateParams($msg,$params);
					$ah->MessageSetTranslated($msg);
				}
				else
					$ah->MessageSet($msg,$params);
			}
			else
			{
				$this->FeedbackAdd($type,$msg,$params);
			}
		}
	}
	
	public function Mobile($number,$varname="mobile", $showmessage = false)
	{
		$check = false;
        
        //start with 0, and at least 8 digits after 0
		if (preg_match('/^0[0-9]{8,}/',$number ) && ctype_print($number))
		{
			$check = true;
		}
		else
		{
			$this->return = false;
			$this->PostError($varname);
            
            if ($showmessage)
			    $this->MessageSet("error","error_mobile",array($number));
		}
		return $check;
	}		

	public function NoForumSpam($email="")
	{
		if($this->stopforumspam && $this->return)
		{
	        include_once(SERVER_ROOT."/../classes/varia.php");
	        $va = new Varia();
			$ip = $va->IP();
			$url = "https://api.stopforumspam.org/api?ip=" . urlencode($ip);
			if($email!="")
				$url .= "&email=" . urlencode($email);
			$url .= "&f=json";
	        include_once(SERVER_ROOT."/../classes/file.php");
	        $fm = new FileManager();
	        $response = json_decode($fm->Browse($url));
	        if(is_object($response) && isset($response->success) && $response->success == "1")
			{
				if($response->ip->appears == "1")
				{
					$this->return = false;
					$this->FeedbackAdd("error","Your IP $ip is blacklisted by stopforumspam.com",array(),false);
				}
				elseif($email!="" && $response->email->appears == "1")
				{
					$this->return = false;
					$this->FeedbackAdd("error","Your email $email is blacklisted by stopforumspam.com",array(),false);
				}
			}
		}
	}
	
	public function NotEmpty($myvar,$varname="",$translate=true)
	{
		if($myvar=="")
		{
			$this->return = false;
			if($varname!="")
			{
				$this->PostError($varname);
				$label = ($translate)? $this->tr->Translate($varname) : $varname;
				$this->MessageSet("error","error_var_empty",array($label));
			}
		}
	}
	
	public function NoSpam($letlinkgo="")
	{
		$headers = "/(content-type|bcc:|cc:|mime-version:|content-transfer-encoding:|document.cookie|onclick|onload)/i";
		foreach ($_POST as $key=>$value)
		{
			if(!is_array($value))
			{
				if (preg_match($headers,$value))
				{
					$this->return = false;
					$this->FeedbackAdd("error","SPAM FOUND in $key: <strong>" . htmlspecialchars($value) . "</strong>",array(),false);
					if($this->formspam_log)
						$this->SpamLog(array('key'=>$key,'value'=>$value));
				}
			}
		}
	}
	
	public function Password($password)
	{
		if(strlen($password)<6 || strlen($password)>100)
		{
			$this->return = false;
			$this->MessageSet("error","error_password");
		}
	}
	
	public function PasswordsEqual($string1,$string2)
	{
		if($string1!=$string2)
		{
			$this->return = false;
			$this->MessageSet("error","error_passwords_equal");
		}
	}
	
	public function PhPeaceKeyCheck($key)
	{
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$phpeace = new PhPeace();
		if(!$phpeace->KeyCheck($key))
		{
			$this->return = false;
			$this->MessageSet("error","error_key",array($key));
		}
	}
	
	private function PostError($varname)
	{
		if($this->feedback)
		{
			$pmsg = $this->session->Get("posterrors");
			if (is_array($pmsg) && count($pmsg)>0)
				$smsg = $pmsg;
			else
				$smsg = array();
			$smsg[] = $varname;
			$this->session->Set("posterrors",$smsg);
		}
	}
	
	public function PostStore()
	{
		$this->session->Set("postvars",$_POST);
	}
	
	public function Captcha($post)
	{
		if($this->recaptcha && $this->id_p==0) {
	        if($this->return) {
	            $this->return = false;
	            if(isset($post["g-recaptcha-response"]) && $this->ValidRecaptcha($post["g-recaptcha-response"])) {
	                $this->return = true;
	            } else {
	                $this->MessageSet("error","error_captcha");
	            }
	        } elseif(isset($post["g-recaptcha-response"]) && !$this->ValidRecaptcha($post["g-recaptcha-response"])) {
	            $this->MessageSet("error","error_captcha");
	        }
	    }
	}
	
    private function ValidRecaptcha($response)
    {
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
				'secret' => $this->recaptcha_private_key,
				'response' => $response
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success = json_decode($verify);
		return $captcha_success->success;
    }
	
	public function URL($url,$varname="URL")
	{
		$checkurl = false;
		// SCHEME
		$urlregex = '^(https?|ftp)\:\/\/';
		
		// USER AND PASS (optional)
		//$urlregex .= '([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?';
		
		// HOSTNAME OR IP
		$urlregex .= '[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)+';
		
		// PORT (optional)
		$urlregex .= '(\:[0-9]{2,5})?';
		// PATH (optional)
		$urlregex .= '(\/([a-z0-9+\$_-]\.?)+)*\/?';
		// GET Query (optional)
		//$urlregex .= '(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?';
		// ANCHOR (optional)
		//$urlregex .= '(#[a-z_.-][a-z0-9+\$_.-]*)?\$';
		
		// check
		if (preg_match("/$urlregex/i", $url))
		{
			$checkurl = true;
		}
		else
		{
			$this->return = false;
			$this->PostError($varname);
			$this->MessageSet("error","error_url",array($url));
		}
		return $checkurl;
	}
	
	/**
	 * Validate a URL path (temporary mock-up)
	 *
	 * @param string $path
	 * @return boolean
	 */
	public function URLPath($path)
	{
		$valid = preg_match('/[0-9a-z-]/',$path) && !preg_match('/[!.@~A-Z\s]/',$path);
		$this->return = $valid;
		return $valid;
	}
	
	private function SpamLog($suspect=array())
	{
		UserError("Suspicious variable",$suspect,512);
	}

	public function WellFormedXml($xmlstring)
	{
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		if(!$xh->Check($xmlstring,false))
		{
			$this->return = false;
			//$this->MessageSet("error","error_xml",array($email));
		}
	}
	
}
?>
