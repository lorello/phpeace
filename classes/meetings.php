<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");


class Meetings
{
	function __construct()
	{
	}
	
	public function AmIAdmin($id_meeting,$id_user)
	{
		$admin = false;
		if($id_meeting>0)
		{
			$row = $this->MeetingGet($id_meeting);
			if ($id_user>0 && $row['id_user']>0 && $id_user==$row['id_user'])
			{
				$admin = true;
			}
		}
		return $admin;
	}
	
	public function IsUserInvited($id_meeting,$id_p)
	{
		$invited = false;
		$slots = $this->ParticipantSlots($id_meeting,$id_p);
		if(count($slots)>0)
		{
			foreach($slots as $slot)
			{
				if($slot['status']>0)
				{
					$invited = true;
				}
			}
		}
		return $invited;
	}
	
	public function IsUserPending($id_meeting,$id_p)
	{
		$pending = false;
		$slots = $this->ParticipantSlots($id_meeting,$id_p);
		if(count($slots)>0)
		{
			foreach($slots as $slot)
			{
				if($slot['status']=="0")
				{
					$pending = true;
				}
			}
		}
		return $pending;
	}
	
	public function MeetingDelete($id_meeting)
	{
		$slots = array();
		$this->MeetingSlots($slots,$id_meeting,false);
		foreach($slots as $slot)
		{
			$this->MeetingSlotDelete($slot['id_meeting_slot']);
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "meetings" );
		$res[] = $db->query( "DELETE FROM meetings WHERE id_meeting='$id_meeting' " );
		Db::finish( $res, $db);
	}
	
	public function MeetingSlotDelete($id_meeting_slot)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "meeting_slots" );
		$res[] = $db->query( "DELETE FROM meeting_slots WHERE id_meeting_slot='$id_meeting_slot' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "meeting_participants" );
		$res[] = $db->query( "DELETE FROM meeting_participants WHERE id_meeting_slot='$id_meeting_slot' " );
		Db::finish( $res, $db);
	}
	
	public function MeetingGet($id_meeting)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT m.id_meeting,m.title,m.description,m.status,m.is_private,m.id_topic,m.id_user
			FROM meetings m 
			WHERE m.id_meeting=$id_meeting";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function MeetingImport($id_meeting,$people,$id_pt_group)
	{
		if($id_meeting>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			include_once(SERVER_ROOT."/../classes/validator.php");
			$pe = new People();
			$va = new Validator();
			$db =& Db::globaldb();
			$today_time = $db->GetTodayTime();
			if($id_pt_group>0)
			{
				$slots = array();
				$this->MeetingSlots($slots,$id_meeting,false);
				$users = array();
				$sqlstr = "SELECT id_p FROM people_groups WHERE id_pt_group='$id_pt_group'";
				$db->QueryExe($users, $sqlstr);
				if(count($slots)>0 && count($users)>0)
				{
					$db->begin();
					$db->lock( "meeting_participants" );
					foreach($users as $user)
					{
						foreach($slots as $slot)
						{
							$sqlstr = "INSERT INTO meeting_participants (id_meeting_slot,id_p,join_date,status,comments) 
								VALUES ('{$slot['id_meeting_slot']}','{$user['id_p']}','$today_time',1,'')  ";
							$res[] = $db->query( $sqlstr );
						}
					}
					Db::finish( $res, $db);
				}
			}
			elseif($people!="")
			{
				$row = $this->MeetingGet($id_meeting);
				$id_topic = (int)$row['id_topic'];
				$lines = explode("\r\n",$people);
				if(count($lines)>0)
				{
					reset($lines);
					foreach($lines as $line)
					{
						$vals = explode("|",$line);
						if(count($vals)==3)
						{
							$name1 = $db->SqlQuote(trim($vals[0]));
							$name2 = $db->SqlQuote(trim($vals[1]));
							$email = $db->SqlQuote(trim($vals[2]));
							if($va->Email($email))
							{
								$slots = array();
								$this->MeetingSlots($slots,$id_meeting,false);
								if(count($slots)>0)
								{
									$id_p = $pe->UserCreate($name1,$name2,$email,0,0,array(),false,$id_topic,1,false);
									$db->begin();
									$db->lock( "meeting_participants" );
									foreach($slots as $slot)
									{
										$sqlstr = "INSERT INTO meeting_participants (id_meeting_slot,id_p,join_date,status,comments) 
											VALUES ('{$slot['id_meeting_slot']}','$id_p','$today_time',1,'')  ";
										$res[] = $db->query( $sqlstr );
									}
									Db::finish( $res, $db);
								}
							}
						}
					}
				}
			}
		}
	}
	
	public function ParticipantSlotGet($id_meeting_slot,$id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(mp.join_date) AS join_date_ts,mp.status,mp.comments
			FROM meeting_participants mp 
			WHERE mp.id_meeting_slot='$id_meeting_slot' AND mp.id_p='$id_p' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function ParticipantSlots($id_meeting,$id_p)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ms.id_meeting_slot,UNIX_TIMESTAMP(mp.join_date) AS join_date_ts,mp.status,mp.comments,ms.title,UNIX_TIMESTAMP(ms.start_date) AS start_date_ts
			FROM meeting_participants mp 
			INNER JOIN meeting_slots ms ON mp.id_meeting_slot=ms.id_meeting_slot
			WHERE ms.id_meeting='$id_meeting' AND mp.id_p='$id_p'
			ORDER BY ms.start_date ";
		$db->QueryExe($rows, $sqlstr,false);
		return $rows;
	}
	
	public function ParticipantUpdate($id_meeting_slot,$id_p,$status,$comments,$id_meeting=0)
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$sqlstr = "UPDATE meeting_participants SET join_date='$today_time',status='$status',comments='$comments'
			WHERE id_meeting_slot='$id_meeting_slot' AND id_p='$id_p' ";
		$notify = false;
		if($id_meeting_slot>0)
		{
			$slot = $this->ParticipantSlotGet($id_meeting_slot,$id_p);
			if(count($slot)==0)
			{
				$sqlstr = "INSERT INTO meeting_participants (id_meeting_slot,id_p,join_date,status,comments) 
					VALUES ('$id_meeting_slot','$id_p','$today_time','0','$comments') ";
				$notify = true;
			}
		}
		$db->begin();
		$db->lock( "meeting_participants" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($id_meeting>0 && $notify)
		{
			$this->NotifyAdmin($id_meeting,$id_meeting_slot,$id_p,$status);
		}
	}
	
	public function MeetingParticipants(&$rows,$id_meeting_slot,$status,$paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(mp.join_date) AS join_date_ts,mp.id_p,mp.status,CONCAT(p.name1,' ',p.name2) AS name,mp.comments
			FROM meeting_participants mp 
			INNER JOIN people p ON mp.id_p=p.id_p
			WHERE mp.id_meeting_slot='$id_meeting_slot' ";
		if($status>-1)
			$sqlstr .= " AND mp.status=$status ";
		$sqlstr .= " GROUP BY mp.id_p ORDER BY mp.join_date DESC,p.name2,p.name1 ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	public function MeetingSlotGet($id_meeting_slot)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT ms.id_meeting_slot,ms.id_meeting,UNIX_TIMESTAMP(ms.start_date) AS start_date_ts,
			ms.length,ms.title,ms.description
			FROM meeting_slots ms 
			WHERE ms.id_meeting_slot='$id_meeting_slot' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function MeetingSlots(&$rows,$id_meeting,$paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ms.id_meeting_slot,UNIX_TIMESTAMP(ms.start_date) AS start_date_ts,ms.length,ms.title,ms.description,COUNT(mp.id_p) AS num_participants
			FROM meeting_slots ms 
			LEFT JOIN meeting_participants mp ON ms.id_meeting_slot=mp.id_meeting_slot AND mp.status=4
			WHERE ms.id_meeting='$id_meeting'
			GROUP BY ms.id_meeting_slot
			ORDER BY ms.start_date ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	public function MeetingSlotStore($id_meeting_slot,$id_meeting,$start_date,$length,$title,$description)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "meeting_slots" );
		if($id_meeting_slot>0)
		{
			$sqlstr = "UPDATE meeting_slots SET start_date='$start_date',length='$length',title='$title',description='$description'
				WHERE id_meeting_slot='$id_meeting_slot' ";
		}
		else 
		{
			$id_meeting_slot = $db->nextId("meeting_slots","id_meeting_slot");
			$sqlstr = "INSERT INTO meeting_slots (id_meeting_slot,id_meeting,start_date,length,title,description) 
				VALUES ('$id_meeting_slot','$id_meeting','$start_date','$length','$title','$description')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_meeting_slot;
	}

	public function MeetingStore($id_meeting,$title,$description,$status,$id_topic,$is_private,$id_user)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "meetings" );
		if($id_meeting>0)
		{
			$sqlstr = "UPDATE meetings SET title='$title',description='$description',status='$status',
				id_topic='$id_topic',is_private='$is_private',id_user='$id_user'
				WHERE id_meeting='$id_meeting' ";
		}
		else 
		{
			$id_meeting = $db->nextId("meetings","id_meeting");
			$sqlstr = "INSERT INTO meetings (id_meeting,title,description,status,id_topic,is_private,id_user) 
				VALUES ('$id_meeting','$title','$description','$status','$id_topic','$is_private','$id_user')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_meeting;
	}

	public function MeetingsAll(&$rows,$id_topic=0,$public_only=false,$paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT m.id_meeting,m.title,m.description,m.status,m.is_private,m.id_topic,m.id_user,'meeting' AS item_type
			FROM meetings m 
			WHERE id_meeting>0 ";
		if($id_topic>0)
			$sqlstr .= " AND m.id_topic=$id_topic ";
		if($public_only)
			$sqlstr .= " AND m.status>0 AND m.is_private=0";
		$sqlstr .= " ORDER BY m.id_meeting DESC ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	private function NotifyAdmin($id_meeting,$id_meeting_slot,$id_p,$status)
	{
		$meeting = $this->MeetingGet($id_meeting);
		$slot = $this->MeetingSlotGet($id_meeting_slot);
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/people.php");
		include_once(SERVER_ROOT."/../classes/user.php");
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$mail = new Mail();
		$pe = new People();
		$t = new Topic($meeting['id_topic']);
		$person = $pe->UserGetById($id_p);
		$name = $person['name1'] . " " . $person['name2'];
		$extra = array('name'=>$name,'email'=>$person['email']);
		$u = new User();
		$u->id = $meeting['id_user'];
		$user = $u->UserGet();
		$trm23 = new Translator($t->id_language,23);
		$subject = "[REQ] {$meeting['title']} - " . $trm23->Translate("subscription");
		$admin_url = $mail->admin_web . "/meet/meeting_participant_slot.php?id_p=$id_p&id_meeting_slot=$id_meeting_slot";
		$statuses = $trm23->Translate("participation");
		$message = $trm23->TranslateParams("request_notification",array($name,$meeting['title'],$slot['title'],$statuses[$status],$admin_url));
		$mail->SendMail($user['email'],$user['name'],$subject,$message,$extra);
	}

	public function MeetingParticipantsSearch(&$rows,$id_meeting,$params)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(mp.join_date) AS join_date_ts,mp.id_p,mp.status,CONCAT(p.name1,' ',p.name2) AS name,mp.comments
			FROM meeting_participants mp 
			INNER JOIN people p ON mp.id_p=p.id_p
			INNER JOIN meeting_slots ms ON mp.id_meeting_slot=ms.id_meeting_slot
			WHERE ms.id_meeting='$id_meeting' ";
		if($params['status']>-1)
			$sqlstr .= " AND mp.status={$params['status']} ";
		if (strlen($params['email']) > 0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%' OR p.name3 LIKE '%{$params['name']}%') ";
		$sqlstr .= " GROUP BY mp.id_p 
			ORDER BY p.name1,p.name2 ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
}
?>
