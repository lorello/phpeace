<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Generic methods for campaigns
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
abstract class Campaigns
{
	/**
	 * All active campaigns in a topic (paginated)
	 *
	 * @param array $rows
	 * @param integer $id_topic
	 * @return integer	Num of campaigns
	 */
	public static function ActiveCampaigns( &$rows, $id_topic )
	{
		$sqlstr = "SELECT tc.id_topic,tc.id_topic_campaign,tc.name,UNIX_TIMESTAMP(tc.start_date) AS start_date_ts,tc.thanks,tc.money,
		tc.approve_comments,tc.active,'campaign' AS item_type ";
		if ($id_topic>0)
			$sqlstr .= " FROM topic_campaigns tc  
		    	WHERE tc.active>0 AND tc.id_topic=$id_topic ";
		else 
			$sqlstr .= ",t.name AS topic_name 
			 FROM topic_campaigns tc  
			 INNER JOIN topics t ON tc.id_topic=t.id_topic 
			 WHERE tc.active>0  ";
		$sqlstr .= " GROUP BY tc.id_topic_campaign ORDER BY tc.active ASC, tc.start_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * All campaigns proposed by a person
	 *
	 * @param integer $id_p
	 * @param integer $id_topic	(optional)
	 * @return array
	 */
	public static function ProposedPerson( $id_p,$id_topic=0 )
	{
		$cond = ($id_topic>0)? " AND tc.id_topic=$id_topic" : "";
		$sqlstr = "SELECT tc.id_topic,tc.id_topic_campaign,tc.name,UNIX_TIMESTAMP(tc.start_date) AS hdate_ts,tc.active,'campaign' AS item_type
		    FROM topic_campaigns tc  
		    WHERE tc.active>0 AND tc.id_p=$id_p $cond 
		    ORDER BY tc.start_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * All campaigns signed by a person
	 *
	 * @param integer $id_p
	 * @param integer $id_topic	(optional)
	 * @return array
	 */
	public static function SignedPerson( $id_p,$id_topic=0 )
	{
		$cond = ($id_topic>0)? " AND tc.id_topic=$id_topic" : "";
		$sqlstr = "SELECT tc.id_topic,tc.id_topic_campaign,tc.name,UNIX_TIMESTAMP(tc.start_date) AS start_date_ts,tc.thanks,tc.money,
		tc.approve_comments,tc.active,'campaign' AS item_type,UNIX_TIMESTAMP(tcp.insert_date) AS hdate_ts,tcp.id_person
		    FROM topic_campaigns tc  
		    INNER JOIN topic_campaigns_persons tcp ON tc.id_topic_campaign=tcp.id_topic_campaign
		    WHERE tc.active>0 $cond AND tcp.id_p=$id_p
		    GROUP BY tc.id_topic_campaign ORDER BY tcp.insert_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * All campaigns signed by an organisation
	 *
	 * @param integer $id_p
	 * @param integer $id_topic	(optional)
	 * @return array
	 */
	public static function SignedOrg( $id_p,$id_topic=0 )
	{
	    $cond = ($id_topic>0)? " AND tc.id_topic=$id_topic" : "";
	    $sqlstr = "SELECT tc.id_topic,tc.id_topic_campaign,tc.name,UNIX_TIMESTAMP(tc.start_date) AS start_date_ts,tc.thanks,tc.money,
		tc.approve_comments,tc.active,'campaign' AS item_type,UNIX_TIMESTAMP(tco.insert_date) AS hdate_ts,tco.id_org
		    FROM topic_campaigns tc
		    INNER JOIN topic_campaigns_orgs tco ON tc.id_topic_campaign=tco.id_topic_campaign
		    WHERE tc.active>0 $cond AND tco.id_p=$id_p
		    GROUP BY tc.id_topic_campaign ORDER BY tco.insert_date DESC";
	    $db =& Db::globaldb();
	    $rows = array();
	    $db->QueryExe($rows, $sqlstr);
	    return $rows;
	}
	
	/**
	 * All campaigns a person has subscribed to
	 *
	 * @param integer $id_p
	 * @return array
	 */
	public static function SubscribedPerson( $id_p )
	{
		$sqlstr = "SELECT tc.id_topic,tc.id_topic_campaign,tc.name,UNIX_TIMESTAMP(tc.start_date) AS start_date_ts,tcp.contact,
		tc.approve_comments,tc.active,'campaign' AS item_type,UNIX_TIMESTAMP(tcp.insert_date) AS hdate_ts
		    FROM topic_campaigns tc  
		    INNER JOIN topic_campaigns_persons tcp ON tc.id_topic_campaign=tcp.id_topic_campaign AND tcp.id_p=$id_p
		    GROUP BY tc.id_topic_campaign ORDER BY tcp.insert_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
}
?>
