<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Quotes
{
	public function ConfigurationUpdate($path)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->SetPath("quotes_path",$path);
	}

	public function DumpAll()
	{
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		$quotes = array();
		$counter = $this->QuotesAll($quotes);
		$dump_xml = "<?xml version=\"1.0\" encoding=\"" . $xh->encoding . "\"?>\n";
		$dump_xml .= "<quotes date=\"".date("Y-m-d")."\" num=\"$counter\">\n";
		foreach($quotes as $quote)
		{
			$dump_xml .= "<quote id=\"$quote[id_quote]\" lang=\"$quote[lang]\">";
			$dump_xml .= "<text>" . htmlspecialchars($quote['quote']) . "</text>";
			$dump_xml .= "<author><name>".htmlspecialchars($quote['author'])."</name><notes>".htmlspecialchars($quote['notes'])."</notes></author>";
			$dump_xml .= "</quote>";
		}
		$dump_xml .= "</quotes>\n";
		if ($xh->Check($dump_xml))
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager;
			$filename = "pub/quotes.xml";
			$fm->WritePage( $filename, $dump_xml );
			$fm->PostUpdate();
		}
		else
			$counter = 0;
		return $counter;
	}

	public function QuoteDelete($id_quote)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "quotes" );
		$res[] = $db->query( "DELETE FROM quotes WHERE id_quote='$id_quote' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_quote,$o->types['quote']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['quote'],$id_quote);
	}

	public function QuoteGet($id_quote)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT quote,author,approved,notes,id_language,id_topic FROM quotes WHERE id_quote=$id_quote";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function QuoteInsert( $quote,$author,$notes,$approved,$keywords, $id_language, $id_topic )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "quotes" );
		$id_quote = $db->nextId( "quotes", "id_quote" );
		$sqlstr = "INSERT INTO quotes (id_quote,quote,author,notes,approved, id_language,id_topic)
		 VALUES ($id_quote,'$quote','$author','$notes',$approved, $id_language,$id_topic)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_quote, $o->types['quote']);
		include_once(SERVER_ROOT."/../classes/search.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($approved)
			$s->IndexQueueAdd($o->types['quote'],$id_quote,$id_topic,$t->id_group,1);
		return $id_quote;
	}

	public function QuoteUpdate($id_quote,$quote,$author,$notes,$approved,$keywords,$id_language, $id_topic)
	{
		$prev = $this->QuoteGet($id_quote);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "quotes" );
		$sqlstr = "UPDATE quotes SET quote='$quote',author='$author',notes='$notes',
			id_language=$id_language,approved=$approved,id_topic=$id_topic WHERE id_quote=$id_quote";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_quote, $o->types['quote']);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($approved)
			$s->IndexQueueAdd($o->types['quote'],$id_quote,$id_topic,$t->id_group,1);
		elseif($prev['approved'])
			$s->ResourceRemove($o->types['quote'],$id_quote);
	}

	public function QuotesAll(&$rows, $id_topic=0,$paged=false)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_quote,quote,author,notes,q.id_language,'quote' AS item_type,id_topic 
			FROM quotes q 
			WHERE q.approved=1 " . (($id_topic>0)? " AND q.id_topic='$id_topic' ":"") . " ORDER BY id_quote ASC";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function QuotesApproved( &$rows, $id_topic, $approved )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT q.id_quote,q.quote,q.author,q.notes,t.name 
		FROM quotes q 
		LEFT JOIN topics t ON q.id_topic=t.id_topic
		WHERE q.approved=$approved " . (($id_topic>0)? " AND q.id_topic='$id_topic' ":"") . " ORDER BY q.id_quote DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function RandomQuote($id_topic,$id_language=0)
	{
		$cond = "";
		if ($id_topic>0)
			$cond .= "AND id_topic='$id_topic'";
		if ($id_language>0)
			$cond .= "AND id_language='$id_language'";
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_quote,quote,author,notes,id_topic FROM quotes WHERE approved=1 $cond ORDER BY id_quote";
		$db->QueryExe($rows, $sqlstr);
		$random_quote = array();
		$tot_quotes = count($rows);
		if ($tot_quotes>0)
		{
			$random_index = rand(0, $tot_quotes-1);
			$random_quote['id_quote'] = $rows[$random_index]['id_quote'];
			$random_quote['quote'] = $rows[$random_index]['quote'];
			$random_quote['author'] = $rows[$random_index]['author'];
			$random_quote['notes'] = $rows[$random_index]['notes'];
			$random_quote['id_topic'] = $rows[$random_index]['id_topic'];
		}
		return $random_quote;
	}

	public function Search( &$rows, $params )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "	SELECT id_quote,quote,author,approved,notes FROM quotes WHERE 1=1";
		if (strlen($params['quote']) > 0)
			$sqlstr .= " AND quote LIKE '%{$params['quote']}%'";
		if (strlen($params['author']) > 0)
			$sqlstr .= " AND author LIKE '%{$params['author']}%'";
		$sqlstr .= " ORDER BY id_quote DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}
?>
