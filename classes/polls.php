<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
define('TIME_BETWEEN_VOTES',1200);

class Polls
{
	private $counter;
	
	public function ActivePolls( &$rows, $id_topic, $paged= true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_poll,title,description,UNIX_TIMESTAMP(start_date) AS start_date_ts,'poll' AS item_type,active,id_topic
		FROM polls 
		WHERE active>0 AND is_private=0 ";
		if($id_topic>0)
			$sqlstr .= " AND id_topic='$id_topic' ";
		$sqlstr .= " ORDER BY active ASC, start_date DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function MailjobSearch($id_poll,$id_topic,$params)
	{
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs(3);
		$sqlstr = "SELECT ppv.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.email AS mj_email,p.password AS mj_password 
		 FROM poll_people_votes ppv 
		 INNER JOIN people p ON ppv.id_p=p.id_p 
		 INNER JOIN people_topics pt ON pt.id_p=ppv.id_p AND pt.id_topic='$id_topic'
		 WHERE ppv.id_poll='{$id_poll}' AND p.active=1 AND pt.contact=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
		if (strlen($params['name'])>0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%') ";
		if (strlen($params['email'])>0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if ($params['id_geo']>0)
			$sqlstr .= " AND p.id_geo='{$params['id_geo']}' ";
		$sqlstr .= " GROUP BY p.id_p ORDER BY p.name1 ";
		return $mj->RecipientsSet($sqlstr,true,true);
	}

	public function PersonParamsSessionGet($id_poll)
	{
		$session_var = $this->SessionVar($id_poll,true,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		return $session->Get( $session_var );
	}
	
	public function PersonParamsSessionSet($id_poll,$user_params)
	{
		$session_var = $this->SessionVar($id_poll,true,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set( $session_var, $user_params );
	}
	
	public function PersonParamsSessionStore($id_poll,$id_p)
	{
		$session_var = $this->SessionVar($id_poll,true,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$user_params = $session->Get( $session_var);
		$this->PersonParamsStore($id_p,$user_params);
	}
	
	public function PersonParamsStore($id_p,$user_params)
	{
		if(is_array($user_params) && count($user_params)>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$pe->UserParamsUpdate($id_p,$user_params);		
		}
	}
	
	private function PersonResults($id_poll,$id_p,$id_questions_group)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "	SELECT pq.id_question,pq.question,
			pq.weight,pvn.name AS 'vote_name',IF(pqvw.weight IS NULL,ppv.vote,pqvw.weight) AS 'vote_weight',
			(SELECT MAX(weight) FROM poll_questions_votes_weights WHERE id_poll=$id_poll AND id_question=pq.id_question) AS max_weight
			FROM poll_people_votes ppv 
			LEFT JOIN poll_questions_votes_weights pqvw ON pqvw.vote=ppv.vote AND pqvw.id_question=ppv.id_question 
			INNER JOIN poll_questions pq ON ppv.id_question=pq.id_question 
			INNER JOIN poll_votes_names pvn ON ppv.vote=pvn.vote 
			WHERE ppv.id_poll=$id_poll AND ppv.id_p=$id_p AND pq.id_questions_group=$id_questions_group
			GROUP BY ppv.id_question 
			ORDER BY pq.seq ";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}
	
	public function PersonResultsByGroup($id_poll,$id_p)
	{
		$qgroups = array();
		$this->QuestionsGroups($qgroups,$id_poll);
		$groups = array();
		foreach($qgroups as $qgroup)
		{
			$group = array('xname'=>"questions_group");
			$group['id'] = $qgroup['id_questions_group'];
			$group['name'] = $qgroup['name'];
			$group['seq'] = $qgroup['seq'];
			$group['num_questions'] = $qgroup['questions'];
			$total = 0;
			$total_max = 0;
			$presults = $this->PersonResults($id_poll,$id_p,$qgroup['id_questions_group']);
			$questions = array();
			foreach($presults as $presult)
			{
				$res_array = array('xname'=>"question");
				$res_array['id'] = $presult['id_question'];
				$res_array['question'] = $presult['question'];
				$res_array['weight'] = $presult['weight'];
				$res_array['vote'] = $presult['vote_name'];
				$res_array['vote_weight'] = $presult['vote_weight'];
				$res_array['max_weight'] = $presult['max_weight'];
				$qtotal = 0;
				$qtotal_max = 0;
				if($presult['vote_weight']>=0)
				{
					$qtotal = $presult['weight'] * $presult['vote_weight'];
					$res_array['total'] = $qtotal;
					$qtotal_max = $presult['weight'] * $presult['max_weight'];
					$res_array['total_max'] = $qtotal_max;
					$total += $qtotal;
					$total_max += $qtotal_max;
				}
				$questions['q'.$presult['id_question']] = $res_array;
			}
			$group['questions'] = $questions;
			$group['total'] = $total;
			$group['total_max'] = $total_max;
			$group['total_norm'] = $total_max>0? round($total/$total_max,2) : 0;
			$group['total_perc'] = $total_max>0? round($total/$total_max,2)*100 : 0;
			$groups['g_'.$qgroup['id_questions_group']] = $group;
		}
		return $groups;
	}

	public function PersonTokenGet($token)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_poll,id_p FROM poll_tokens WHERE token='$token' ");
		return $row;
	}
	
	public function PersonTokenGetById($id_poll,$id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_poll,id_p,token FROM poll_tokens WHERE id_poll='$id_poll' AND id_p='$id_p' ");
		return $row;
	}
	
	public function PersonTokenNotify($id_poll,$id_p)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetById($id_p);
		$token = $this->PersonTokenSet($id_p,$id_poll);
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/translator.php");
		$m = new Mail();
		$ini = new Ini();
		$poll = $this->PollGet($id_poll);
		$t = new Topic($poll['id_topic']);
		$tr = new Translator($t->id_language,0,false,$t->id_style);
		$extra = array();
		$extra['name'] = $poll['promoter']!=""? $poll['promoter'] : $t->name;
		if($poll['promoter_email']!="" && $m->CheckEmail($poll['promoter_email']))
		{
			$extra['email'] = $poll['promoter_email'];
		}
		elseif($t->row['temail']!="")
		{
			$extra['email'] = $t->row['temail'];
		}
		$url = $m->pub_web . "/" . $ini->Get('poll_path') . "/result.php?t=$token";
		$subject = $poll['title'];
		$message = $tr->TranslateParams("poll_notify",array($poll['title'],$url));
		$m->SendMail($user['email'],"{$user['name1']} {$user['name2']}",$subject,$message,$extra);
	}
	
	public function PersonTokenValid($token)
	{
		return strlen($token)==32;
	}
	
	public function PersonTokenSet($id_p,$id_poll)
	{
		$token = "";
		$row = $this->PersonTokenGetById($id_poll,$id_p);
		if($row['id_p']>0 && $this->PersonTokenValid($row['token']))
		{
			$token = $row['token'];
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$token = $v->Uid();
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "poll_tokens" );
			$insert = "INSERT INTO poll_tokens (id_poll,id_p,token) 
					VALUES ($id_poll,'$id_p','$token') ";
			$res[] = $db->query( $insert );
			Db::finish( $res, $db);
		}
		return $token;
	}
	
	public function PollDelete($id_poll)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_votes_names" );
		$res[] = $db->query( "DELETE FROM poll_votes_names WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->LockTables(array("comments","poll_questions"));
		$res[] = $db->query( "DELETE FROM comments WHERE id_type=20 AND id_item IN (SELECT id_question FROM poll_questions WHERE id_poll='$id_poll') " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_questions" );
		$res[] = $db->query( "DELETE FROM poll_questions WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_people_votes" );
		$res[] = $db->query( "DELETE FROM poll_people_votes WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_tokens" );
		$res[] = $db->query( "DELETE FROM poll_tokens WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_questions_votes_weights" );
		$res[] = $db->query( "DELETE FROM poll_questions_votes_weights WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_questions_groups" );
		$res[] = $db->query( "DELETE FROM poll_questions_groups WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "polls" );
		$res[] = $db->query( "DELETE FROM polls WHERE id_poll='$id_poll' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_poll,$o->types['poll']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['poll'],$id_poll);
	}

	public function PollGet($id_poll)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_poll,UNIX_TIMESTAMP(start_date) AS start_date_ts,
			title,description,id_type,users_type,id_topic,vote_threshold,vote_min,vote_max,intro,thanks,is_private,comments,
			moderate_comments,submit_questions,sort_questions_by,promoter,promoter_email,notify_answers,active
		    FROM polls WHERE id_poll='$id_poll' ");
		return $row;
	}
	
	public function PollStore($id_poll,$id_topic,$start_date,$id_type,$title,$description,$intro,$thanks,$users_type,$keywords,$is_private,$comments,$moderate_comments,$vote_min,$vote_max,$vote_threshold,$submit_questions,$sort_questions_by,$promoter,$promoter_email,$notify_answers,$active)
	{
		$prev = $this->PollGet($id_poll);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "polls" );
		if($id_poll>0)
		{
			$sqlstr = "UPDATE polls SET id_poll='$id_poll',id_topic='$id_topic',start_date='$start_date',active='$active',
				id_type='$id_type',title='$title',description='$description',intro='$intro',thanks='$thanks',
				users_type='$users_type',is_private='$is_private',comments='$comments',moderate_comments='$moderate_comments',
				vote_min='$vote_min',vote_max='$vote_max',vote_threshold='$vote_threshold',submit_questions='$submit_questions',
				sort_questions_by='$sort_questions_by',promoter='$promoter',promoter_email='$promoter_email',notify_answers='$notify_answers'
				WHERE id_poll='$id_poll' ";
		}
		else 
		{
			$id_poll = $db->nextId("polls","id_poll");
			$sqlstr = "INSERT INTO polls (id_poll,id_topic,start_date,active,id_type,title,description,intro,thanks,users_type,is_private,comments,moderate_comments,submit_questions,sort_questions_by,promoter,promoter_email,notify_answers) 
				VALUES ('$id_poll','$id_topic','$start_date','$active','$id_type','$title','$description','$intro','$thanks','$users_type','$is_private','$comments','$moderate_comments','$submit_questions','$sort_questions_by','$promoter','$promoter_email','$notify_answers')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_poll, $o->types['poll']);
		$this->QueueAdd($id_topic,$id_poll);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($is_private!="0" && $t->visible)
		{
			$s->IndexQueueAdd($o->types['poll'],$id_poll,$id_topic,$t->id_group,1);
		}
		elseif($prev['is_private']!="0") 
		{
			$s->ResourceRemove($o->types['poll'],$id_poll);
		}
	}
	
	public function PromoterNotify($id_poll,$id_p,$comments="")
	{
		$poll = $this->PollGet($id_poll);
		if($id_p>0 && $poll['notify_answers'] && $poll['promoter_email']!="")
		{
			include_once(SERVER_ROOT."/../classes/mail.php");
			$m = new Mail();
			if($m->CheckEmail($poll['promoter_email']))
			{
				include_once(SERVER_ROOT."/../classes/people.php");
				$pe = new People();
				$user = $pe->UserGetById($id_p);
				$url = $m->admin_web . "/topics/poll_question_answer.php?id_p=$id_p&id_poll=$id_poll";
				$subject = "[{$poll['title']}] - New submission";
				$message = "Name: {$user['name1']} {$user['name2']}\n";
				if($comments!="")
					$message .= "Comments: $comments\n";
				$message .= "\nLink: $url\n";
				$m->SendMail($poll['promoter_email'],$poll['promoter'],$subject,$message,array());
			}
		}
	}

	private function QueueAdd($id_topic,$id_poll)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT s.id_subtopic FROM polls p
			INNER JOIN subtopics s ON p.id_topic=s.id_topic AND s.id_type=17 WHERE p.id_poll='$id_poll' ");
		if($row['id_subtopic']>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$t->queue->JobInsert($t->queue->types['subtopic'],$row['id_subtopic'],"update");
		}
	}

	public function QuestionDelete($id_question)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions" );
		$res[] = $db->query( "DELETE FROM poll_questions WHERE id_question='$id_question' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_questions_votes_weights" );
		$res[] = $db->query( "DELETE FROM poll_questions_votes_weights WHERE id_question='$id_question' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_people_votes" );
		$res[] = $db->query( "DELETE FROM poll_people_votes WHERE id_question='$id_question' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove(20,$id_question);
	}

	public function QuestionGet($id_question)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_question,id_poll,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,question,
			description,seq,id_p,ip,approved,description_long,is_html,id_questions_group,weight
		    FROM poll_questions WHERE id_question='$id_question' ");
		return $row;
	}
	
	private function QuestionGetNextBySeq($id_poll,$seq,$up,$id_questions_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_question,id_poll,approved,seq 
			FROM poll_questions 
			WHERE id_poll='$id_poll' ";
		if($id_questions_group>0)
			$sqlstr .= " AND id_questions_group='$id_questions_group' ";
		$sqlstr .= " AND seq" . ($up? "<":">" ) . "'$seq' LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function QuestionInsertPub($id_poll,$question,$description,$description_long,$id_p)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions" );
		$id_question = $db->nextId("poll_questions","id_question");
		$seq = $db->nextId("poll_questions","seq");
		$insert_date = $db->getTodayTime();
		$sqlstr = "INSERT INTO poll_questions (id_question,id_poll,insert_date,question,description,description_long,is_html,seq,approved,id_p,ip) 
			VALUES ('$id_question','$id_poll','$insert_date','$question','$description','$description_long','0','$seq','0','$id_p','$ip')  ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_question;
	}
	
	public function QuestionMove($id_question,$up=true,$id_question_group=0)
	{
		$question = $this->QuestionGet($id_question);
		$question_swap = $this->QuestionGetNextBySeq($question['id_poll'],$question['seq'],$up,$id_question_group );
		if($question_swap['seq']>0)
		{
			$this->QuestionSeqUpdate($id_question,$question_swap['seq']);
			$this->QuestionSeqUpdate($question_swap['id_question'],$question['seq']);			
		}
	}
	
	private function QuestionSeqUpdate($id_question,$seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions" );
		$sqlstr = "UPDATE poll_questions SET seq='$seq' WHERE id_question='$id_question' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function QuestionSessionGet($id_poll,$id_question)
	{
		$session_var = $this->SessionVar($id_poll,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$questions = $session->Get($session_var);
		return (is_array($questions) && isset($questions[$id_question]))? $questions[$id_question] : 0;
	}
	
	public function QuestionSessionSet($id_poll,$id_question,$vote)
	{
		$session_var = $this->SessionVar($id_poll,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$questions = ($session->IsVarSet($session_var) && is_array($session->Get($session_var)))? $session->Get($session_var) : array();
		$questions[$id_question] = $vote;
		$session->Set( $session_var, $questions );
	}
	
	public function QuestionSessionStore($id_poll,$id_p)
	{
		$session_var = $this->SessionVar($id_poll,true);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$questions = $session->Get($session_var);
		if(is_array($questions))
		{
			foreach($questions as $id_question=>$vote)
			{
				$this->Vote($id_poll,$id_question,$id_p,$vote);
			}
		}
		$session->Delete($session_var);
		$session->Delete($this->SessionVar($id_poll,true,true));
	}
	
	public function QuestionStore($id_question,$id_poll,$question,$description,$insert_date,$approved,$description_long,$is_html,$id_questions_group,$weight)
	{
		$prev = $this->QuestionGet($id_question);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions" );
		if($id_question>0)
			$sqlstr = "UPDATE poll_questions SET question='$question',description='$description',insert_date='$insert_date',
				approved='$approved',description_long='$description_long',is_html='$is_html',
				id_questions_group='$id_questions_group',weight='$weight'
				WHERE id_question='$id_question' ";
		else 
		{
			$id_question = $db->nextId("poll_questions","id_question");
			$seq = $db->nextId("poll_questions","seq");
			$insert_date = $db->getTodayTime();
			$sqlstr = "INSERT INTO poll_questions (id_question,id_poll,insert_date,question,description,seq,approved,description_long,is_html,id_questions_group,weight) 
				VALUES ('$id_question','$id_poll','$insert_date','$question','$description','$seq','$approved','$description_long','$is_html','$id_questions_group','$weight')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$poll = $this->PollGet($id_poll);
		$t = new Topic($poll['id_topic']);
		if($approved && $poll['is_private']!="0" && $t->visible)
		{
			$s->IndexQueueAdd(20,$id_question,$t->id,$t->id_group,1);
		}
		elseif($prev['approved']) 
		{
			$s->ResourceRemove(20,$id_question);
		}
		return $id_question;
	}
	
	public function QuestionVoteWeightStore($id_poll,$id_question,$vote,$weight)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions_votes_weights" );
		$sqlstr = "REPLACE INTO poll_questions_votes_weights (id_question,vote,id_poll,weight) 
			VALUES ($id_question,$vote,$id_poll,$weight) ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function QuestionVotesWeights($id_poll,$id_question)
	{
		$rows = array();
		$sqlstr = "SELECT pvn.vote,pvn.name,pvn.seq,pqvw.weight 
			FROM poll_votes_names pvn 
			LEFT JOIN poll_questions_votes_weights pqvw ON pqvw.id_poll=pvn.id_poll AND pqvw.vote=pvn.vote AND pqvw.id_question='$id_question'
			WHERE pvn.id_poll='$id_poll' 
			GROUP BY pvn.vote
			ORDER BY pvn.seq ";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}
	
	public function Questions(&$rows,$id_poll,$sort_questions_by,$approved=true,$id_questions_group=0,$paged=false)
	{
		$sqlstr = "SELECT pq.id_question,UNIX_TIMESTAMP(pq.insert_date) AS insert_date_ts,pq.question,pq.description,pq.seq,
			COUNT(id_comment) AS comments,'poll_question' AS item_type,pq.id_poll,pq.approved,pq.id_questions_group,pq.weight,pqg.name AS group_name
			FROM poll_questions pq
			LEFT JOIN comments c ON c.id_type='20' AND c.id_item=pq.id_question
			LEFT JOIN poll_questions_groups pqg ON pq.id_questions_group=pqg.id_questions_group
			WHERE pq.id_poll='$id_poll' ";
		if($approved>-1)
			$sqlstr .= " AND pq.approved='$approved' ";
		if($id_questions_group>0)
			$sqlstr .= " AND pq.id_questions_group='$id_questions_group' ";
		$sqlstr .= " GROUP BY pq.id_question ORDER BY approved ";
		switch($sort_questions_by)
		{
			case "1":
				$sqlstr .= ",pq.seq ";
			break;
			case "2":
				$sqlstr .= ",pq.insert_date DESC ";
			break;
			case "3":
				$sqlstr .= ",pq.insert_date ";
			break;
		}
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, $paged);
		$this->counter = 1 + ($paged? ($GLOBALS['current_page'] - 1)*$db->records_per_page : 0);
		array_walk($rows,array($this,'Counter'));
		return $num;
	}

	public function QuestionsChart(&$rows,$id_poll,$poll_type,$vote_threshold, $paged=true)
	{
		switch($poll_type)
		{
			case "0":
			case "1":
				$sqlstr = "SELECT ppv.id_question,pq.question,COUNT(ppv.id_p) AS votes,pq.description
					FROM poll_people_votes ppv 
					INNER JOIN poll_questions pq ON ppv.id_question=pq.id_question
					WHERE ppv.id_poll='$id_poll' 
					GROUP BY ppv.id_question
					ORDER BY votes DESC ";
			break;
			case "2":
			case "3":
				$sqlstr = "SELECT ppv.id_question,pq.question,COUNT(ppv.vote) AS votes,AVG(ppv.vote) AS avg,pq.description
					FROM poll_people_votes ppv 
					INNER JOIN poll_questions pq ON ppv.id_question=pq.id_question
					WHERE ppv.id_poll='$id_poll'
					GROUP BY ppv.id_question HAVING votes>=$vote_threshold
					ORDER BY avg DESC ";
			break;
			case "4":
			break;
		}
		$db =& Db::globaldb();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function QuestionsGroupGet($id_questions_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_questions_group,id_poll,name,seq
		    FROM poll_questions_groups WHERE id_questions_group='$id_questions_group' ");
		return $row;
	}
	
	public function QuestionsGroupGetnext($id_poll,$seq,$is_next)
	{
		$sqlstr = "SELECT id_questions_group
			    FROM poll_questions_groups 
			    WHERE id_poll=$id_poll ";
		if($seq>0)
		{
			if($is_next)
				$sqlstr .= " AND seq>$seq ORDER BY seq ASC ";
			else
				$sqlstr .= " AND seq<$seq ORDER BY seq DESC ";
		}
		else 
		{
			$sqlstr .= " AND seq>0 ORDER BY seq ASC ";
		}
		$sqlstr .= " LIMIT 1  ";
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		return $row['id_questions_group'];
	}

	private function QuestionsGroupGetNextBySeq($id_poll,$seq,$up)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_questions_group,id_poll,seq FROM poll_questions_groups
			WHERE id_poll='$id_poll' AND seq" . ($up? "<":">" ) . "'$seq' LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	private function QuestionsGroupSeqUpdate($id_questions_group,$seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions_groups" );
		$sqlstr = "UPDATE poll_questions_groups SET seq='$seq' WHERE id_questions_group='$id_questions_group' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function QuestionsGroupMove($id_questions_group,$up=true)
	{
		$group = $this->QuestionsGroupGet($id_questions_group);
		$group_swap = $this->QuestionsGroupGetNextBySeq($group['id_poll'],$group['seq'],$up );
		if($group_swap['seq']>0)
		{
			$this->QuestionsGroupSeqUpdate($id_questions_group,$group_swap['seq']);
			$this->QuestionsGroupSeqUpdate($group_swap['id_questions_group'],$group['seq']);			
		}
	}
	
	public function QuestionsGroups(&$rows,$id_poll,$paged=false)
	{
		$sqlstr = "SELECT pqg.id_questions_group,pqg.name,pqg.id_poll,pqg.seq,COUNT(pq.id_question) AS questions,'poll_questions_group' AS item_type
			FROM poll_questions_groups pqg
			LEFT JOIN poll_questions pq ON pqg.id_questions_group=pq.id_questions_group
			WHERE pqg.id_poll='$id_poll' 
			GROUP BY pqg.id_questions_group ";
		$sqlstr .= " ORDER BY seq ";
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, $paged);
		$this->counter = 1 + ($paged? ($GLOBALS['current_page'] - 1)*$db->records_per_page : 0);
		array_walk($rows,array($this,'Counter'));
		return $num;
	}

	public function QuestionsGroupStore($id_questions_group,$id_poll,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_questions_groups" );
		if($id_questions_group>0)
			$sqlstr = "UPDATE poll_questions_groups SET name='$name'
				WHERE id_questions_group='$id_questions_group' ";
		else 
		{
			$id_questions_group = $db->nextId("poll_questions_groups","id_questions_group");
			$seq = $db->NextId("poll_questions_groups","seq");
			$sqlstr = "INSERT INTO poll_questions_groups (id_questions_group,id_poll,name,seq) 
				VALUES ('$id_questions_group','$id_poll','$name','$seq')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_questions_group;
	}
	
	public function SessionVar($id_poll,$questions=false,$person_params=false)
	{
		return $questions? ($person_params? "pollparams_" . $id_poll : "pollquestions_" . $id_poll) : "poll_" . $id_poll;
	}
	
	public function StatusChange($id_poll,$status)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "polls" );
		$sqlstr = "UPDATE polls SET active='$status' WHERE id_poll='$id_poll' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Vote($id_poll,$id_question,$id_p,$vote)
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$db->begin();
		$db->lock( "poll_people_votes" );
		$sqlstr = "INSERT INTO poll_people_votes (id_poll,id_question,vote,id_p,insert_date,ip)
				VALUES ('$id_poll','$id_question','$vote','$id_p','$today_time','$ip')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set( $this->SessionVar($id_poll), 1 );
	}
	
	public function VoteCheck($id_p,$id_poll,$poll_type,$id_question=0)
	{
		$check = false;
		$cond = " id_poll='$id_poll' ";
		if($id_p>0)
			$cond .= " AND id_p='$id_p'";
		else 
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$ip = $v->IP();
			$cond .= " AND ip='$ip' AND TIMESTAMPDIFF(SECOND,insert_date,now())<'".TIME_BETWEEN_VOTES . "' " ;
		}
		$sqlstr = "";
		if($poll_type=="0" || $poll_type=="1" || $poll_type=="4")
		{
			$sqlstr = "SELECT COUNT(vote) AS counter FROM poll_people_votes WHERE $cond ";
		}
		elseif(($poll_type=="2" || $poll_type=="3") && $id_question>0)
		{
			$sqlstr = "SELECT COUNT(vote) AS counter FROM poll_people_votes WHERE $cond AND id_question='$id_question' ";
		}

		if($sqlstr!="")
		{
			$row = array();
			$db =& Db::globaldb();
			$db->begin();
			$db->query_single($row, $sqlstr);
			if ($row['counter']>0)
				$check = true;
		}
		return $check;
	}
	
	public function VoteDelete($id_p,$id_poll,$id_question=0)
	{
		$sqlstr = "DELETE FROM poll_people_votes WHERE id_poll='$id_poll' AND id_p=$id_p ";
		if($id_question>0)
		{
			$sqlstr .= " AND id_question='$id_question' ";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_people_votes" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_tokens" );
		$res[] = $db->query( "DELETE FROM poll_tokens WHERE id_poll='$id_poll' AND id_p=$id_p " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Delete( $this->SessionVar($id_poll) );
	}
	
	public function VotePerson( $id_p,$id_question,$poll_type )
	{
		$row = array();
		$db =& Db::globaldb();
		if($poll_type=="4")
		{
			$sqlstr = "SELECT UNIX_TIMESTAMP(ppv.insert_date) AS insert_date_ts,ppv.vote,CONCAT(p.name1,' ',p.name2) AS name,ppv.id_p,ppv.ip 
				FROM poll_people_votes ppv 
				INNER JOIN people p ON ppv.id_p=p.id_p
				WHERE ppv.id_p='$id_p' 
				GROUP BY ppv.id_p ";
		}
		else 
		{
			$sqlstr = "SELECT UNIX_TIMESTAMP(ppv.insert_date) AS insert_date_ts,ppv.vote,CONCAT(p.name1,' ',p.name2) AS name,ppv.id_p,pq.question,ppv.ip 
				FROM poll_people_votes ppv 
				INNER JOIN poll_questions pq ON ppv.id_question=pq.id_question
				LEFT JOIN people p ON ppv.id_p=p.id_p
				WHERE ppv.id_question='$id_question' AND ppv.id_p='$id_p'  ";
		}
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function VotedPerson( $id_p,$id_topic=0 )
	{
		$cond = ($id_topic>0)? " AND pl.id_topic=$id_topic" : "";
		$sqlstr = "SELECT pl.id_topic,pl.id_poll,pl.title,'poll' AS item_type,ppv.id_question,
			UNIX_TIMESTAMP(ppv.insert_date) AS hdate_ts,ppv.id_p
		    FROM polls pl
		    LEFT JOIN poll_people_votes ppv ON pl.id_poll=ppv.id_poll AND ppv.id_p=$id_p
		    WHERE 1=1 $cond  AND ppv.id_p=$id_p
		    GROUP BY pl.id_poll ORDER BY ppv.insert_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Votes(&$rows,$id_poll,$id_question,$id_poll_type,$paged=true)
	{
		$sqlstr = "SELECT UNIX_TIMESTAMP(ppv.insert_date) AS insert_date_ts,ppv.vote,CONCAT(p.name1,' ',p.name2) AS name,
			ppv.id_p,pq.question,ppv.id_question,p.email
			FROM poll_people_votes ppv
			INNER JOIN poll_questions pq ON ppv.id_question=pq.id_question
			LEFT JOIN people p ON ppv.id_p=p.id_p
			WHERE ppv.id_poll='$id_poll' ";
		if($id_question>0 && $id_poll_type!="4")
			$sqlstr .= " AND ppv.id_question='$id_question' ";
		if($id_poll_type=="4")
			$sqlstr .= " GROUP BY ppv.id_p ";
		$sqlstr .= " ORDER BY ppv.insert_date DESC ";
		$db =& Db::globaldb();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function VoteNameDelete($id_poll,$vote)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_votes_names" );
		$res[] = $db->query( "DELETE FROM poll_votes_names WHERE id_poll='$id_poll' AND vote='$vote' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "poll_questions_votes_weights" );
		$res[] = $db->query( "DELETE FROM poll_questions_votes_weights WHERE id_poll='$id_poll' AND vote='$vote' " );
		Db::finish( $res, $db);
	}

	public function VoteNameGet($id_poll,$vote)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_poll,vote,name,seq
		    FROM poll_votes_names WHERE id_poll='$id_poll' AND vote='$vote' ");
		return $row;
	}
	
	private function VoteNameGetNextBySeq($id_poll,$seq,$up)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT vote,id_poll,seq FROM poll_votes_names
			WHERE id_poll='$id_poll' AND seq" . ($up? "<":">" ) . "'$seq' LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	private function VoteNameSeqUpdate($id_poll,$vote,$seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_votes_names" );
		$sqlstr = "UPDATE poll_votes_names SET seq='$seq' WHERE id_poll='$id_poll' AND vote='$vote' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function VoteNameMove($id_poll,$vote,$up=true)
	{
		$votename = $this->VoteNameGet($id_poll,$vote);
		$votename_swap = $this->VoteNameGetNextBySeq($id_poll,$votename['seq'],$up );
		if($votename_swap['seq']>0)
		{
			$this->VoteNameSeqUpdate($id_poll,$vote,$votename_swap['seq']);
			$this->VoteNameSeqUpdate($id_poll,$votename_swap['vote'],$votename['seq']);			
		}
	}
	
	public function VoteNameStore($id_poll,$vote,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "poll_votes_names" );
		if($vote>0)
			$sqlstr = "UPDATE poll_votes_names SET name='$name'
				WHERE id_poll=$id_poll AND vote=$vote ";
		else 
		{
			$vote = $db->NextId("poll_votes_names","vote","id_poll=$id_poll");
			$seq = $db->NextId("poll_votes_names","seq","id_poll=$id_poll");
			$sqlstr = "INSERT INTO poll_votes_names (id_poll,vote,name,seq) 
				VALUES ('$id_poll','$vote','$name','$seq')  ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $vote;
	}
	
	public function VoteNames(&$rows,$id_poll,$paged=false)
	{
		$sqlstr = "SELECT vote,name,seq
			FROM poll_votes_names 
			WHERE id_poll='$id_poll' 
			ORDER BY seq ";
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, $paged);
		$this->counter = 1 + ($paged? ($GLOBALS['current_page'] - 1)*$db->records_per_page : 0);
		array_walk($rows,array($this,'Counter'));
		return $num;
	}

	private function Counter(&$value)
	{
		$value['pos'] = $this->counter;
		$this->counter++;
	}

	public function PersonParams($user_params)
	{
		$uparams = array();
		if(is_array($user_params) && count($user_params)>0)
		{
			$counter = 1;
			foreach($user_params as $key=>$value)
			{
				$uparam = array('xname'=>"param",'name'=>$key);
				if (strpos($value,'|')!==false)
				{
					$uparam_array = explode('|',$value);
					if(is_array($uparam_array) && count($uparam_array)>0)
					{
						$upvalues = array();
						$upcounter = 1;
						foreach($uparam_array as $upvalue)
						{
							$upvalues['up'.$upcounter] = array('xname'=>"value",'xvalue'=>$upvalue);
							$upcounter ++;
						}
						$uparam['values'] = $upvalues;
					}
					else
					{
						$uparam['value'] = $value;
					}
				}
				else 
				{
					$uparam['value'] = $value;
				}
				$uparams['p'.$counter] = $uparam;
				$counter ++;
			}
		}
		return $uparams;
	}
	
	public function XmlDump($id_poll)
	{
		$poll = $this->PollGet($id_poll);
		$xml = array('xname'=>"poll");
		$xml['id'] = $id_poll;
		$xml['name'] = $poll['title'];
		$persons = array();
		$num_persons = $this->Votes( $persons, $id_poll, 0, $poll['id_type']);
		$xml['num_answers'] = $num_persons;
		foreach($persons as $person)
		{
			$vote = array('xname'=>"person");
			$vote['id'] = $person['id_p'];
			$vote['name'] = $person['name'];
			$vote['email'] = $person['email'];
			$vote['date_ts'] = $person['insert_date_ts'];
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$user_params = $pe->UserParamsGet($person['id_p'],true);
			$vote['user_params'] = $this->PersonParams($user_params);
			$groups = $this->PersonResultsByGroup($id_poll,$person['id_p']);
			$vote['votes'] = $groups;
			$xml['persons'.$person['id_p']] = $vote;
		}
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		return $xh->Array2Xml($xml);
	}
}
?>
