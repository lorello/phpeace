<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class History
{
	public $actions = array(
			'create'	=> 0,
			'reject'	=> 1,
			'update'	=> 2,
			'approve'	=> 3,
			'delete'	=> 4,
			'on_hold'	=> 5
			);

	public $types;

	private $diff;

	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources;
		$this->types = $r->types;
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$this->diff = $conf->Get("update_diff");
	}

	public function CreatorDateTime($id_type,$id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(time) AS time_ts
		FROM history h
		WHERE id_type=$id_type AND id=$id AND action=" . $this->actions['create'] . " LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row['time_ts'];
	}

	public function CreatorId($id_type,$id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_user
		FROM history h
		WHERE id_type=$id_type AND id=$id AND action=" . $this->actions['create'] . " LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function HistoryAdd( $id_type,$id,$id_action,$only_real_user=false )
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$current_user = (int)($session->Get('current_user_id'));
		if($only_real_user && $current_user>0)
		{
			$this->HistoryAddUser($id_type,$id,$id_action,$current_user);	
		}
		else 
		{
			$this->HistoryAddUser($id_type,$id,$id_action,$current_user);
		}
	}
	
	private function HistoryAddUser( $id_type,$id,$id_action,$id_user )
	{
		$db =& Db::globaldb();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$ip = Varia::IP();
		$time = $db->getTodayTime();
		if($id_action==$this->actions['create'])
		{
			$db->begin();
			$db->lock( "history" );
			$id_history = $db->nextId( "history", "id_history" );
			$sqlstr =  "INSERT INTO history (id_history, id_type, id, id_user, time, action, ip)
			VALUES ($id_history,$id_type,$id,$id_user,'$time',$id_action, '$ip') ON DUPLICATE KEY UPDATE id_type=$id_type, id=$id, id_user=$id_user, time='$time', action=$id_action, ip='$ip'";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		else 
		{
			$row = $this->HistoryLast($id_type,$id);
			$db->begin();
			$db->lock( "history" );
			if ($id_action!=$row['action'] || $id_user!=$row['id_user'] || (time() > ($row['ts_time'] + ($this->diff))) )
			{
				$id_history = $db->nextId( "history", "id_history" );
				$sqlstr =  "INSERT INTO history (id_history, id_type, id, id_user, time, action, ip)
				VALUES ($id_history,$id_type,$id,$id_user,'$time',$id_action, '$ip') ON DUPLICATE KEY UPDATE id_type=$id_type, id=$id, id_user=$id_user, time='$time', action=$id_action, ip='$ip'";
			}
			else
			{
				$sqlstr = "UPDATE history SET time='$time' WHERE id_history=".$row['id_history'];
			}
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);		
		}
 	}
	
	public function HistoryAll( &$rows, $id_type, $id )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT h.id_history,h.id_user,u.name,UNIX_TIMESTAMP(h.time) AS ts_time,h.action,h.ip,h.id
		FROM history h
		LEFT JOIN users u ON h.id_user=u.id_user
		WHERE id_type='$id_type' AND id='$id' ORDER BY id_history DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function HistoryAllByResourceType( &$rows, $id_type)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT h.id_history,h.id_user,u.name,UNIX_TIMESTAMP(h.time) AS ts_time,h.action,h.ip,h.id
		FROM history h
		LEFT JOIN users u ON h.id_user=u.id_user
		WHERE id_type='$id_type' 
		ORDER BY id_history DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function HistoryDelete($id_type,$id)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "history" );
		$sqlstr = "DELETE FROM history WHERE id_type='$id_type' AND id='$id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function HistoryGet($id_type,$id) // should be array
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT h.id_history,h.id_user,u.name,UNIX_TIMESTAMP(h.time) AS ts_time,h.action
		FROM history h
		INNER JOIN users u ON h.id_user=u.id_user
		WHERE id_type='$id_type' AND id='$id' ORDER BY id_history DESC";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	private function HistoryLast($id_type,$id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT h.id_history,h.id_user,UNIX_TIMESTAMP(h.time) AS ts_time,h.action
		FROM history h
		WHERE id_type='$id_type' AND id='$id' ORDER BY id_history DESC LIMIT 1";
		$db->query_single($row, $sqlstr);
		return $row;
	}

}
?>
