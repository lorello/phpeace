<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/resources.php");

/**
 * Comments management
 * Generic for each resource type
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Comments
{
	/**
	 * Resource type ID
	 *
	 * @var integer
	 */
	public $id_type;
	
	/**
	 * Resource ID
	 *
	 * @var integer
	 */
	public $id_item;
	
	/**
	 * Comments tree
	 *
	 * @var array
	 */
	private $tree = array();
	
	/**
	 * Initialize local properties
	 *
	 * @param string	$type		Resource type name
	 * @param integer	$id_item	Resource ID
	 */
	function __construct($type,$id_item)
	{
		$r = new Resources();
		if(array_key_exists($type,$r->types))
			$this->id_type = $r->types[$type];
		else
			UserError("Comment type $type unknown",array());
		$this->id_item = $id_item;
	}
	
	/**
	 * Delete comment
	 *
	 * @param integer $id_comment
	 */
	public function CommentDelete($id_comment)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "comments" );
		$res[] = $db->query( "DELETE FROM comments WHERE id_comment='$id_comment' " );
		Db::finish( $res, $db);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "comments_content" );
		$res[] = $db->query( "DELETE FROM comments_content WHERE id_comment='$id_comment' " );
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve comment data
	 *
	 * @param integer $id_comment
	 * @return array
	 */
	public function CommentGet($id_comment)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT c.id_comment,title,id_parent,id_item,id_type,UNIX_TIMESTAMP(insert_date) AS date,
		c.ip, approved,c.id_p,params,cc.text,CONCAT(p.name1,' ',p.name2) AS author
		FROM comments c
		INNER JOIN comments_content cc USING(id_comment) 
		INNER JOIN people p ON c.id_p=p.id_p 
		WHERE c.id_comment='$id_comment' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Insert new comment
	 *
	 * @param integer	$id_parent		Parent comment ID
	 * @param string	$title
	 * @param string	$comment
	 * @param datetime	$insert_date
	 * @param string	$ip				IP address
	 * @param boolint	$approved
	 * @param integer	$id_p
	 * @param integer	$id_topic
	 * @param array		$params
	 */
	public function CommentInsert( $id_parent,$title,$comment,$insert_date,$ip,$approved,$id_p,$id_topic,$params )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "comments" );
		$id_comment = $db->nextId( "comments", "id_comment" );
		$sqlstr1 = "INSERT INTO comments (id_comment,id_parent,id_item,id_type,insert_date,
		title,ip,approved,id_p,params)
		 VALUES ('$id_comment','$id_parent','$this->id_item','$this->id_type','$insert_date','$title','$ip','$approved','$id_p','$params')";
		$res1[] = $db->query( $sqlstr1 );
		Db::finish( $res1, $db);
		$db->lock( "comments_content" );
		$sqlstr2 = "INSERT INTO comments_content (id_comment,text) VALUES ('$id_comment','$comment')";
		$res2[] = $db->query( $sqlstr2 );
		Db::finish( $res2, $db);
		if($approved=="1")
			$this->Propagate($id_topic);
	}

	/**
	 * Insert new comment from portal
	 *
	 * @param integer	$id_parent	Parent comment ID
	 * @param string	$title
	 * @param string 	$comment
	 * @param integer	$id_p
	 * @param integer	$id_topic
	 * @param boolint	$approved
	 * @param array		$params
	 */
	public function CommentInsertPublic( $id_parent,$title,$comment,$id_p,$id_topic,$approved,$params )
	{
		$db =& Db::globaldb();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$this->CommentInsert($id_parent,$title,$comment,$db->getTodayTime(),Varia::IP(),$approved,$id_p,$id_topic,$params);
	}

	/**
	 * Update comment details
	 *
	 * @param integer	$id_comment
	 * @param string	$title
	 * @param string	$content
	 * @param boolint	$approved
	 * @param integer	$id_topic
	 */
	public function CommentUpdate($id_comment,$title,$content,$approved,$id_topic)
	{
		$row = $this->CommentGet($id_comment);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "comments" );
		$res[] = $db->query( "UPDATE comments SET title='$title',approved='$approved'
		 WHERE id_comment='$id_comment' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "comments_content" );
		$res[] = $db->query( "UPDATE comments_content SET text='$content' WHERE id_comment=$id_comment" );
		Db::finish( $res, $db);
		if($approved != $row['approved'])
			$this->Propagate($id_topic);
	}
	
	/**
	 * Approved comments
	 *
	 * @param array		$rows
	 * @param boolint	$approved
	 * @param integer	$id_topic
	 * @param boolean	$paged		Paginated
	 * @return integer				Num of comments
	 */
	public function CommentsApproved( &$rows, $approved, $id_topic, $paged=true )
	{
		switch($this->id_type)
		{
			case "5":
				$sqlstr = "SELECT c.id_comment,c.title,UNIX_TIMESTAMP(c.insert_date) AS insert_date_ts,
				c.approved,c.params,c.id_p,CONCAT(p.name1,' ',p.name2) AS name,a.headline,c.id_item,c.id_parent 
				FROM comments c 
				INNER JOIN articles a ON c.id_item=a.id_article  AND a.id_topic=$id_topic ";
				if($this->id_item>0)
					$sqlstr.= " AND a.id_article='$this->id_item' ";
				$sqlstr .= " INNER JOIN people p ON c.id_p=p.id_p 
				WHERE c.approved='$approved' AND c.id_type='$this->id_type'
				GROUP BY c.id_comment ORDER BY insert_date DESC";
			break;
			case "10":
				$sqlstr = "SELECT c.id_comment,c.title,UNIX_TIMESTAMP(c.insert_date) AS insert_date_ts,
				c.approved,c.params,c.id_p,CONCAT(p.name1,' ',p.name2) AS name,tft.title AS thread,c.id_item,c.id_parent,tft.id_topic_forum 
				FROM comments c 
				INNER JOIN topic_forum_threads tft ON c.id_item=tft.id_thread ";
				if($this->id_item>0)
					$sqlstr.= " AND tft.id_thread='$this->id_item' ";
				$sqlstr .= " INNER JOIN topic_forums tf ON tft.id_topic_forum=tf.id_topic_forum AND tf.id_topic='$id_topic' ";
				$sqlstr .= " INNER JOIN people p ON c.id_p=p.id_p 
				WHERE c.approved='$approved' AND c.id_type='$this->id_type'
				GROUP BY c.id_comment ORDER BY c.insert_date DESC";
			break;
			case "20":
				$sqlstr = "SELECT c.id_comment,c.title,UNIX_TIMESTAMP(c.insert_date) AS insert_date_ts,
				c.approved,c.params,c.id_p,CONCAT(p.name1,' ',p.name2) AS name,pq.question,c.id_item,c.id_parent,pq.id_poll 
				FROM comments c 
				INNER JOIN poll_questions pq ON c.id_item=pq.id_question ";
				if($this->id_item>0)
					$sqlstr.= " AND pq.id_question='$this->id_item' ";
				$sqlstr .= " INNER JOIN polls po ON pq.id_poll=po.id_poll AND po.id_topic='$id_topic' ";
				$sqlstr .= " INNER JOIN people p ON c.id_p=p.id_p 
				WHERE c.approved='$approved' AND c.id_type='$this->id_type'
				GROUP BY c.id_comment ORDER BY c.insert_date DESC";
			break;
		}
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Number of comments
	 *
	 * @param boolint $approved
	 * @return integer
	 */
	public function CommentsCount($approved)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_comment) AS counter FROM comments 
			 WHERE id_type='$this->id_type' AND approved='$approved' AND id_item='$this->id_item' ";
		$db->query_single($row, $sqlstr);
		return (int)$row['counter'];
	}
	
	/**
	 * Number of comments pending approval
	 *
	 * @param boolint	$approved
	 * @param integer	$id_topic
	 * @param integer	$id_context
	 * @return integer
	 */
	public function CommentsPending($approved,$id_topic=0,$id_context=0)
	{
		$row = array();
		$db =& Db::globaldb();
		$join = "";
		if($id_topic>0)
		{
			switch($this->id_type)
			{
				case "5":
					$join = " INNER JOIN articles a ON c.id_item=a.id_article AND a.id_topic='$id_topic' ";
				break;
				case "10":
					$join = " INNER JOIN topic_forum_threads tft ON c.id_item=tft.id_thread ";
					if($id_context>0)
						$join .= " AND tft.id_topic_forum='$id_context' ";
					$join .= " INNER JOIN topic_forums tf ON tft.id_topic_forum=tf.id_topic_forum AND tf.id_topic='$id_topic' ";
				break;
				case "20":
					$join = " INNER JOIN poll_questions pq ON c.id_item=pq.id_question ";
					if($id_context>0)
						$join .= " AND pq.id_poll='$id_context' ";
					$join .= " INNER JOIN polls po ON pq.id_poll=po.id_poll AND po.id_topic='$id_topic' ";
				break;
			}
		}
		$sqlstr = "SELECT COUNT(c.id_comment) AS counter 
			FROM comments c  $join
			 WHERE c.id_type='$this->id_type' AND c.approved='$approved' ";
		if ($this->id_item>0)
			$sqlstr .= " AND c.id_item='$this->id_item' ";
		$db->query_single($row, $sqlstr);
		return (int)$row['counter'];
	}
	
	/**
	 * Comments below a comment
	 *
	 * @param integer $id_parent	Parent comment ID
	 * @return array
	 */
	public function Children($id_parent)
	{
		$tree = $this->tree;
		$children = array();
		$child_counter=0;
		for($i=0;$i<count($tree);$i++)
		{
			if ($tree[$i]['id_parent']==$id_parent)
			{
				$children[$child_counter]['id_comment']	= $tree[$i]['id_comment'];
				$children[$child_counter]['title']		= $tree[$i]['title'];
				$children[$child_counter]['id_parent']	= $tree[$i]['id_parent'];
				$children[$child_counter]['date']		= $tree[$i]['date'];
				$children[$child_counter]['author']		= $tree[$i]['author'];
				$child_counter = $child_counter + 1;
			}
		}
		return $children;
	}

	/**
	 * Load all comments in the local tree variable
	 *
	 * @return integer	Num of comments
	 */
	public function LoadTree()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT c.id_comment,c.title,c.id_parent,UNIX_TIMESTAMP(c.insert_date) AS insert_date_ts,
			CONCAT(p.name1,' ',p.name2) AS name
		FROM comments c
		INNER JOIN people p ON c.id_p=p.id_p 
		WHERE approved=1 AND c.id_type='$this->id_type' AND c.id_item='$this->id_item' ORDER BY c.insert_date ASC";
		$db->QueryExe($rows, $sqlstr);
		$counter = 0;
		$tree = array();
		foreach($rows as $row)
		{
			$tree[$counter]['id_comment']	= $row['id_comment'];
			$tree[$counter]['title']		= $row['title'];
			$tree[$counter]['id_parent']	= $row['id_parent'];
			$tree[$counter]['date']	= $row['insert_date_ts'];
			$tree[$counter]['author']	= $row['name'];
			$counter ++;
		}
		$this->tree = $tree;
		return count($this->tree);
	}

	/**
	 * Propagate comment updates to publishing queue
	 *
	 * @param integer $id_topic
	 */
	private function Propagate($id_topic)
	{
		include_once(SERVER_ROOT."/../classes/publishmanager.php");
		$pm = new PublishManager();
		if($this->id_type=="5")
		{
			$pm->TopicInit($id_topic);
			$pm->Article($this->id_item);
		}
		
	}
	
	/**
	 * All comments submitted by a person for current resource type
	 *
	 * @param integer $id_p
	 * @param integer $id_topic
	 * @return array
	 */
	public function Submitted($id_p,$id_topic=0)
	{
		switch($this->id_type)
		{
			case "5":
				$cond = ($id_topic>0)? " AND a.id_topic=$id_topic" : "";
				$sqlstr = "SELECT a.id_article,a.id_topic,a.headline,COUNT(co.id_comment) AS comments,
				ah.subhead,a.halftitle,UNIX_TIMESTAMP(a.written) AS written_ts,a.available,
				UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,a.show_author,a.show_date,a.id_user,a.author,a.author_notes,
				'article' AS item_type,UNIX_TIMESTAMP(MAX(co.insert_date)) AS hdate_ts
				FROM comments co
				INNER JOIN articles a ON co.id_item=a.id_article AND co.id_type=$this->id_type $cond
				INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
				WHERE co.approved=1 AND a.approved=1 AND co.id_p=$id_p
				GROUP BY a.id_article
				ORDER BY co.insert_date DESC ";
			break;
			case "10":
				$sqlstr = "SELECT tft.id_thread,tf.id_topic,tft.title,COUNT(co.id_comment) AS comments,
				'thread' AS item_type,UNIX_TIMESTAMP(MAX(co.insert_date)) AS hdate_ts
				FROM comments co
				INNER JOIN topic_forum_threads tft ON co.id_item=tft.id_thread AND co.id_type=$this->id_type 
				INNER JOIN topic_forums tf ON tft.id_topic_forum=tf.id_topic_forum
				WHERE co.approved=1 AND tft.thread_approved=1 AND co.id_p=$id_p ";
				if($id_topic>0)
					$sqlstr .= " AND tf.id_topic='$id_topic' ";
				$sqlstr .= " GROUP BY tft.id_thread
				ORDER BY co.insert_date DESC ";
			break;
			case "20":
				$sqlstr = "SELECT pq.id_question,p.id_topic,pq.question,COUNT(co.id_comment) AS comments,
				'question' AS item_type,UNIX_TIMESTAMP(MAX(co.insert_date)) AS hdate_ts
				FROM comments co
				INNER JOIN poll_questions pq ON co.id_item=pq.id_question AND co.id_type=$this->id_type 
				INNER JOIN polls p ON pq.id_poll=p.id_poll
				WHERE co.approved=1 AND pq.approved=1 AND co.id_p=$id_p ";
				if($id_topic>0)
					$sqlstr .= " AND p.id_topic='$id_topic' ";
				$sqlstr .= " GROUP BY pq.id_question
				ORDER BY co.insert_date DESC ";
			break;
		}
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
}
?>
