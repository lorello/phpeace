<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

class Labels
{
	public $module;
	public $id_module;
	public $custom_keys = array();
	/**
	 * @var Translator */
	public $tr;

	private $id_pagetype;
	private $id_style;
	private $is_global;
	
	function __construct($id_pagetype,$is_global,$id_style)
	{
		$this->id_pagetype = $id_pagetype;
		$this->is_global = $is_global;
		$this->id_style = $id_style;
		$this->id_module = 0;
		$this->module = "";
	}

	private function PageLabels()
	{
		$words = $this->PageLabelsCommon();
		return array_merge($words, ($this->is_global)? $this->PageLabelsGlobal():$this->PageLabelsSpecific());
	}
	
	private function PageLabelsCommon()
	{
		$words = array();
		$words[] = 'articles';
		$words[] = 'attachments';
		$words[] = 'authenticated';
		$words[] = 'books';
		$words[] = 'error';
		$words[] = 'events';
		$words[] = 'format';
		$words[] = 'friend';
		$words[] = 'homepage';
		$words[] = 'images';
		$words[] = 'next';
		$words[] = 'notes';
		$words[] = 'of';
		$words[] = 'page';
		$words[] = 'preview';
		$words[] = 'previous';
		$words[] = 'print';
		$words[] = 'seealso';
		$words[] = 'share';
		$words[] = 'source';
		$words[] = 'verified';
		$words[] = 'verified_no';
		$words[] = 'verified_pending';
		return $words;
	}
	
	private function PageLabelsModule()
	{
		$words = array();
		switch($this->module)
		{
			case "books":
				$words[] = 'author';
				$words[] = 'books';
				$words[] = 'categories';
				$words[] = 'category';
				$words[] = 'email';
				$words[] = 'isbn';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'pages';
				$words[] = 'price';
				$words[] = 'publish_date';
				$words[] = 'publisher';
				$words[] = 'register_before_review';
				$words[] = 'review';
				$words[] = 'review_insert';
				$words[] = 'review_to_be_approved';
				$words[] = 'reviews';
				$words[] = 'reviews_all';
				$words[] = 'reviews_not_allowed';
				$words[] = 'reviews_read';
				$words[] = 'search';
				$words[] = 'see_book_article';
				$words[] = 'submit';
				$words[] = 'text';
				$words[] = 'vote';
			break;
			case "lists":
				$words[] = 'email';
				$words[] = 'mailing_lists';
				$words[] = 'mandatory_fields';
				$words[] = 'subscribe';
				$words[] = 'unsubscribe';
			break;
			case "meet":
				$words[] = 'comments';
				$words[] = 'participation';
				$words[] = 'submit';
			break;
			case "dodc":
			break;
			case "ecommerce":
				$words[] = 'address';
				$words[] = 'address_notes';
				$words[] = 'buy';
				$words[] = 'delivery';
				$words[] = 'email';
				$words[] = 'name';
				$words[] = 'order';
				$words[] = 'pay';
				$words[] = 'phone';
				$words[] = 'postcode';
				$words[] = 'price';
				$words[] = 'stock_in';
				$words[] = 'stock_out';
				$words[] = 'total';
				$words[] = 'town';
			break;
			case "orgs":
				$words[] = 'additional_fields';
				$words[] = 'address';
				$words[] = 'activities';
				$words[] = 'all_option';
				$words[] = 'asso_data';
				$words[] = 'asso_keywords';
				$words[] = 'asso_type';
				$words[] = 'author';
				$words[] = 'choose_option';
				$words[] = 'contact_details';
				$words[] = 'contact_main';
				$words[] = 'email';
				$words[] = 'email_one';
				$words[] = 'fax';
				$words[] = 'in_charge_of';
				$words[] = 'insert_desc';
				$words[] = 'insert_desc_keywords';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'name2';
				$words[] = 'org_insert';
				$words[] = 'org_search';
				$words[] = 'orgs';
				$words[] = 'phone';
				$words[] = 'postcode';
				$words[] = 'reset';
				$words[] = 'search';
				$words[] = 'source';
				$words[] = 'subactivities';
				$words[] = 'submit';
				$words[] = 'submit_by';
				$words[] = 'text';
				$words[] = 'town';
				$words[] = 'type';
				$words[] = 'types';
				$words[] = 'website';
			break;
			case "media":
				$words[] = 'audios';
				$words[] = 'author';
				$words[] = 'date';
				$words[] = 'length';
				$words[] = 'source';
				$words[] = 'views';
				$words[] = 'videos';
			break;
			case "widgets":
				$words[] = 'description';
				$words[] = 'feed_url_example';
				$words[] = 'my_widgets';
				$words[] = 'no_widgets';
				$words[] = 'search';
				$words[] = 'title';
				$words[] = 'widget';
				$words[] = 'widget_content';
				$words[] = 'widget_content_desc';
				$words[] = 'widget_create';
				$words[] = 'widget_create_advanced';
				$words[] = 'widget_create_advanced_welcome';
				$words[] = 'widget_create_manual';
				$words[] = 'widget_create_manual_welcome';
				$words[] = 'widget_create_rss';
				$words[] = 'widget_create_rss_welcome';
				$words[] = 'widget_create_welcome';
				$words[] = 'widget_description';
				$words[] = 'widget_image';
				$words[] = 'widget_image_desc';
				$words[] = 'widget_library';
				$words[] = 'widget_library_welcome';
				$words[] = 'widget_link_desc';
				$words[] = 'widget_reset';
				$words[] = 'widget_reset_info';
				$words[] = 'within';
				$words[] = 'would_create';
			break;
		}
		return $words;
	}
	
	private function PageLabelsSpecific()
	{
		$words = array();
		switch($this->id_pagetype)
		{
			case "0":
				$words[] = 'homepage';
			break;
			case "1":
				$words[] = 'comments';
				$words[] = 'comments_num';
			break;
			case "2":
				$words[] = 'article_insert_public';
				$words[] = 'author';
				$words[] = 'author_notes';
				$words[] = 'cancel';
				$words[] = 'choose_option';
				$words[] = 'close_window';
				$words[] = 'comments';
				$words[] = 'comments_num';
				$words[] = 'content_notes';
				$words[] = 'description';
				$words[] = 'download_orig';
				$words[] = 'email';
				$words[] = 'halftitle';
				$words[] = 'language';
				$words[] = 'link';
				$words[] = 'mandatory_fields';
				$words[] = 'missing_content';
				$words[] = 'missing_email';
				$words[] = 'missing_link';
				$words[] = 'missing_name';
				$words[] = 'missing_title';
				$words[] = 'missing_your_email';
				$words[] = 'name';
				$words[] = 'other';
				$words[] = 'source';
				$words[] = 'submit';
				$words[] = 'submitted_by';
				$words[] = 'subhead_instr';
				$words[] = 'surname';
				$words[] = 'text';
				$words[] = 'title';
			break;
			case "3":
				$words[] = 'author';
				$words[] = 'comment';
				$words[] = 'comments';
				$words[] = 'comments_not_allowed';
				$words[] = 'comments_num';
				$words[] = 'email';
				$words[] = 'if_registered_login';
				$words[] = 'name';
				$words[] = 'register_before_comment';
				$words[] = 'submit';
				$words[] = 'title';
			break;
			case "4":
				$words[] = 'close_window';
			break;
			case "5":
				$words[] = 'address';
				$words[] = 'address_notes';
				$words[] = 'all_day_long';
				$words[] = 'all_option';
				$words[] = 'anniversaries';
				$words[] = 'back_day';
				$words[] = 'back_today';
				$words[] = 'calendar';
				$words[] = 'click_here';
				$words[] = 'choose_option';
				$words[] = 'contact_main';
				$words[] = 'current_month_events';
				$words[] = 'day';
				$words[] = 'date';
				$words[] = 'date_end';
				$words[] = 'description';
				$words[] = 'email';
				$words[] = 'email_one';
				$words[] = 'error_captcha';
				$words[] = 'event_submit';
				$words[] = 'event_submit_click';
				$words[] = 'event_type';
				$words[] = 'events';
				$words[] = 'events_of_day';
				$words[] = 'hour';
				$words[] = 'hours';
				$words[] = 'insert_event_desc';
				$words[] = 'insert_thanks';
				$words[] = 'future';
				$words[] = 'length';
				$words[] = 'link_one';
				$words[] = 'mandatory_fields';
				$words[] = 'more_info';
				$words[] = 'next_events';
				$words[] = 'past';
				$words[] = 'phone';
				$words[] = 'reset';
				$words[] = 'search';
				$words[] = 'see_event_article';
				$words[] = 'submit';
				$words[] = 'text';
				$words[] = 'time';
				$words[] = 'time_format';
				$words[] = 'title';
				$words[] = 'town';
				$words[] = 'week';
				$words[] = 'what';
				$words[] = 'when';
				$words[] = 'when_desc';
				$words[] = 'where';
				$words[] = 'who';
				
			break;
			case "6":
				$words[] = 'author';
				$words[] = 'author_notes';
				$words[] = 'cancel';
				$words[] = 'insert_thanks';
				$words[] = 'missing_author';
				$words[] = 'missing_quote';
				$words[] = 'quote';
				$words[] = 'quotes';
				$words[] = 'quotes_script';
				$words[] = 'quotes_submit';
				$words[] = 'quotes_total';
				$words[] = 'submit';
			break;
			case "7":
				$words[] = 'address';
				$words[] = 'as_org';
				$words[] = 'as_person';
				$words[] = 'campaign';
				$words[] = 'campaign_over';
				$words[] = 'campaigns';
				$words[] = 'comments';
				$words[] = 'contact_main';
				$words[] = 'description';
				$words[] = 'email';
				$words[] = 'insert_desc';
				$words[] = 'job';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'persons';
				$words[] = 'phone';
				$words[] = 'promoter';
				$words[] = 'org';
				$words[] = 'orgs';
				$words[] = 'sign';
				$words[] = 'signature_org';
				$words[] = 'signature_person';
				$words[] = 'signatures_person';
				$words[] = 'signatures_org';
				$words[] = 'signatures_since';
				$words[] = 'submit';
				$words[] = 'surname';
				$words[] = 'title';
				$words[] = 'town';
				$words[] = 'website';
			break;
			case "8":
				$words[] = 'author';
				$words[] = 'comments';
				$words[] = 'comments_num';
				$words[] = 'description';
				$words[] = 'email';
				$words[] = 'forum';
				$words[] = 'forum_over';
				$words[] = 'forums';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'pubdate';
				$words[] = 'source';
				$words[] = 'submit';
				$words[] = 'submitted_by';
				$words[] = 'text';
				$words[] = 'thread_approve';
				$words[] = 'thread_insert';
				$words[] = 'title';
			break;
			case "9":
				$words[] = 'choose_option';
				$words[] = 'comments';
				$words[] = 'comments_num';
				$words[] = 'description';
				$words[] = 'email';
				$words[] = 'name';
				$words[] = 'poll';
				$words[] = 'poll_over';
				$words[] = 'poll_questions';
				$words[] = 'polls';
				$words[] = 'submit';
				$words[] = 'register';
				$words[] = 'reset';
				$words[] = 'text';
				$words[] = 'title';
				$words[] = 'vote';
			break;
			case "10":
				$words[] = 'author';
				$words[] = 'date';
				$words[] = 'download_orig';
			break;
			case "11":
				$words[] = 'already_auth';
				$words[] = 'associations';
				$words[] = 'address';
				$words[] = 'address_notes';
				$words[] = 'campaigns';
				$words[] = 'computer_private';
				$words[] = 'computer_public';
				$words[] = 'email';
				$words[] = 'email';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'no';
				$words[] = 'password';
				$words[] = 'password_new';
				$words[] = 'password_old';
				$words[] = 'password_remind';
				$words[] = 'password_verify';
				$words[] = 'person_data';
				$words[] = 'person_history';
				$words[] = 'person_history_desc';
				$words[] = 'phone';
				$words[] = 'postcode';
				$words[] = 'privacy';
				$words[] = 'reminder';
				$words[] = 'security';
				$words[] = 'submit';
				$words[] = 'surname';
				$words[] = 'town';
				$words[] = 'user';
				$words[] = 'verify';
				$words[] = 'verify_missing';
				$words[] = 'verify_pending';
				$words[] = 'verify_submit';
				$words[] = 'yes';
			break;
			case "12":
				$words[] = 'newsletter_articles';
				$words[] = 'calendar';
				$words[] = 'newsletter_events';
			break;
			case "15":
				$words[] = 'email';
				$words[] = 'friend_sender';
				$words[] = 'sender';
				$words[] = 'to_email';
				$words[] = 'send_to_friend';
				$words[] = 'message';
				$words[] = 'missing_email';
			break;
			case "16":
				$words[] = 'close_window';
			break;
			case "17":
				$words[] = 'error404_msg';
				$words[] = 'go_back_to';
			break;
			case "18":
				$words[] = 'articles_containing';
				$words[] = 'books_containing';
				$words[] = 'events_containing';
				$words[] = 'results';
			break;
			case "19":
				$words[] = 'verified';
			break;
			case "22":
				$words[] = 'author';
				$words[] = 'comment';
				$words[] = 'comments';
				$words[] = 'comments_not_allowed';
				$words[] = 'comments_num';
				$words[] = 'email';
				$words[] = 'mandatory_fields';
				$words[] = 'name';
				$words[] = 'reply';
				$words[] = 'submit';
				$words[] = 'text';
				$words[] = 'title';
			break;
		}
		return $words;
	}
	
	private function PageLabelsGlobal()
	{
		$words = array();
		switch($this->id_pagetype)
		{
			case "0": // homepage
				$words[] = 'homepage';
			break;
			case "2": // map
				$words[] = 'calendar';
				$words[] = 'map';
				$words[] = 'search_engine';
			break;
			case "3": // feeds
				$words[] = 'next_events';
			break;
			case "4": // galleries
				$words[] = 'author';
				$words[] = 'date';
				$words[] = 'download_orig';
			break;
			case "5": // admin user
				$words[] = 'translations';
				$words[] = 'articles';
				$words[] = 'name';
				$words[] = 'email';
				$words[] = 'comments';
				$words[] = 'mandatory_fields';
				$words[] = 'send';
				$words[] = 'submit';
			break;
			case "6": // global newsletter
				$words[] = 'calendar';
				$words[] = 'newsletter_articles';
				$words[] = 'newsletter_events';
			break;
		}
		return $words;
	}
	
	public function Translate()
	{
		$words = $this->PageLabels();
		$labels = array();
		$counter = 1;
		foreach($words as $word)
		{
			$labels['l_' . $counter] = array('xname'=>"label",'word'=>$word,'tr'=>($this->tr->Translate($word)));
			$counter ++;
		}
		unset($words);
		if($this->id_module>0)
		{
			$reset = false;
			$mod_words = $this->PageLabelsModule();
			$trm = new Translator($this->tr->id_language,$this->id_module,false,$this->id_style);
			foreach($mod_words as $word)
			{
				$tra_word = "";
				if($trm->WordExists($word))
				{
					$tra_word = $trm->Translate($word);
				}
				else
				{
					if($this->tr->WordExists($word))
					{
						$tra_word = $this->tr->Translate($word);
					}
					else
					{
						$reset = true;
					}
				}
				$labels['l_' . $counter] = array('xname'=>"label",'word'=>$word,'tr'=>$tra_word);
				$counter ++;
			}
			if($reset)
			{
				$trm->Reset();
				$this->tr->Reset();
			}
			unset($mod_words);
		}
		$c0words = array();
		$this->CustomAddedGet($c0words,0,0,$this->id_style>0);
		foreach($c0words as $c0word)
		{
			$word = "c_" . $c0word['id_label'];
			$labels['l_' . $counter] = array('xname'=>"label",'word'=>$word,'tr'=>($this->tr->TranslateTry($word)));
			$counter ++;
		}
		unset($c0words);
		$cwords = array();
		if($this->id_module>0)
		{
			$this->CustomAddedGet($cwords,$this->id_module,$this->id_style,$this->id_style>0);
			foreach($cwords as $cword)
			{
				$word = "c_" . $cword['id_label'];
				$labels['l_' . $counter] = array('xname'=>"label",'word'=>$word,'tr'=>($trm->TranslateTry($word)));
				$counter ++;
			}
		}
		unset($cwords);
		return $labels;
	}
	
	public function CustomAddedDelete($id_module,$label)
	{
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$phpeace = new PhPeace();
		$languages = $phpeace->Languages();
		foreach($languages as $key=>$language)
		{
			if($key>0)
				$this->CustomDelete($id_module,$label,$key);
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "labels_custom");
		$id_label = substr($label,2);
		$sqlstr = "DELETE FROM labels_custom WHERE id_label=$id_label ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function CustomDelete($id_module,$label,$id_language)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_style,labels FROM styles_labels WHERE id_style='$this->id_style' AND id_module='$id_module' AND id_language='$id_language' ");
		$v = new Varia();
		$labels2 = $v->Deserialize($row['labels']);
		unset($labels2[$label]);
		$sqlstr = "UPDATE styles_labels SET labels='" . $v->Serialize($labels2) . "' WHERE id_style='$this->id_style' AND id_module='$id_module'  AND id_language='$id_language' ";
		$db->begin();
		$db->lock( "styles_labels" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->CustomReset($id_module,$id_language);
	}
	
	public function CustomAdd($id_module,$labels_custom)
	{
		$id_label = $this->CustomNewId($id_module);
		$label = "c_" . $id_label;
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		foreach($labels_custom as $label_custom)
		{
			$id_language = $label_custom['id_language'];
			$tlabel = $label_custom['label'];
			$row = array();
			$db =& Db::globaldb();
			$db->query_single( $row, "SELECT id_style,labels FROM styles_labels WHERE id_style='$this->id_style' AND id_module='$id_module'AND id_language='$id_language'  ");
			if(count($row)>0)
			{
				$labels2 = $v->Deserialize($row['labels']);
				if(!is_array($labels2))
					$labels2 = array();
				$labels2[$label] = $tlabel;
				$sqlstr = "UPDATE styles_labels SET labels='" . $v->Serialize($labels2) . "' WHERE id_style='$this->id_style' AND id_module='$id_module' AND id_language='$id_language' ";
			}
			else
			{
				$labels2 = array();
				$labels2[$label] = $tlabel;
				$sqlstr = "INSERT INTO styles_labels (id_style,id_module,id_language,labels) 
					VALUES ('$this->id_style','$id_module','$id_language','" . $v->Serialize($labels2) . "')";
			}
			$db->begin();
			$db->lock( "styles_labels" );
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$this->CustomReset($id_module,$id_language);
		}
		return $label;
	}

	private function CustomNewId($id_module)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "labels_custom");
		$id_new = $db->nextId( "labels_custom", "id_label" );
		$sqlstr = "INSERT INTO labels_custom (id_label,id_style,id_module) VALUES ('$id_new','$this->id_style','$id_module')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_new;
	}
	
	private function CustomReset($id_module,$id_language)
	{
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($id_language,$id_module,false,$this->id_style);
		$tr->Reset();
	}
	
	private function CustomAddedGet(&$rows,$id_module,$id_style,$get_all=false)
	{
		$sqlstr = "SELECT id_label FROM labels_custom WHERE id_module='$id_module' ";
		if(!$get_all)
			$sqlstr .= " AND id_style='$id_style' ";
		$sqlstr .= "ORDER BY id_style";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr);
	}
	
	private function CustomGet($id_module,$id_style,$id_language)
	{
		$labels = array();
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT labels FROM styles_labels WHERE id_style='$id_style' AND id_module='$id_module' AND id_language='$id_language' ORDER BY id_style ");
		if ($row['labels']!="")
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$labels2 = $v->Deserialize($row['labels']);
		}
		if (is_array($labels2))
		{
			$labels = $labels2;
		}
		return $labels;
	}

	public function CustomStore($id_module,$label,$label_custom,$id_language)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_style,labels FROM styles_labels WHERE id_style='$this->id_style' AND id_module='$id_module'AND id_language='$id_language'  ");
		if(count($row)>0)
		{
			$labels2 = $v->Deserialize($row['labels']);
			if(!is_array($labels2))
				$labels2 = array();
			$labels2[$label] = $label_custom;
			$sqlstr = "UPDATE styles_labels SET labels='" . $v->Serialize($labels2) . "' WHERE id_style='$this->id_style' AND id_module='$id_module' AND id_language='$id_language' ";
		}
		else
		{
			$labels2 = array();
			$labels2[$label] = $label_custom;
			$sqlstr = "INSERT INTO styles_labels (id_style,id_module,id_language,labels) 
				VALUES ('$this->id_style','$id_module','$id_language','" . $v->Serialize($labels2) . "')";
		}
		$db->begin();
		$db->lock( "styles_labels" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->CustomReset($id_module,$id_language);
	}
	
	public function CustomCustom($id_language,$id_module)
	{
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr0 = new Translator($id_language,$id_module,false);
		$words = $tr0->ParseLangFile();
		if($this->id_style>0)
		{
			$c0words = $this->CustomGet($id_module,0,$id_language);
			foreach($c0words as $c0key=>$c0word)
			{
				$words[$c0key] = $c0word;
			}
		}
		$cwords = $this->CustomGet($id_module,$this->id_style,$id_language);
		foreach($cwords as $ckey=>$cword)
		{
			$this->custom_keys[$ckey] = $words[$ckey];
			$words[$ckey] = $cword;
		}
		foreach($words as $key=>$word)
			if(is_array($word))
				unset($words[$key]);
		return $words;
	}
}
?>
