<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Topics
{
	/** 
	 * @var GroupHelper */
	public $gh;

	public $latest;
	public $temp_id_topic;

	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/grouphelper.php");
		$this->gh = new GroupHelper("topics");
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$this->gh->top_name = $ini->Get('title');
		$this->gh->top_description = $ini->Get('description');
		$this->latest = $ini->Get('latest');
		$this->temp_id_topic = $ini->Get('temp_id_topic');
	}

	public function __destruct()
	{
		unset($this->gh);
	}

	public function AllTopics($show_temp=false,$only_visible=false,$sharing_images_only=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,t.id_group,tg.name AS group_name
		 	FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group 
			WHERE t.id_topic>0 ";
		if (!$show_temp && $this->temp_id_topic >0)
			$sqlstr .= " AND id_topic<>$this->temp_id_topic ";
		if($only_visible)
			$sqlstr .= " AND t.visible=1 ";
		if($sharing_images_only)
			$sqlstr .= " AND t.share_images=1 ";
		$sqlstr .= " ORDER BY tg.seq,t.seq,t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function AllTopicsP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,description,u.name AS contact,t.id_group FROM topics t
		LEFT JOIN topic_users tu ON t.id_topic=tu.id_topic AND tu.is_contact=1
		LEFT JOIN users u ON tu.id_user=u.id_user AND u.active=1
		GROUP BY t.id_topic ORDER BY t.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function AmIAdmin($id_group)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$topic_admin = false;
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT COUNT(tu.id_user) AS counter 
			FROM topic_users tu 
			INNER JOIN topics t ON tu.id_topic=t.id_topic AND t.id_group=$id_group 
			WHERE id_user='".$session->Get("current_user_id")."' AND is_admin=1";
		$db->query_single( $row, $sqlstr);
		if((int)$row['counter']>0)
			$topic_admin = true;
		return $topic_admin;
		
	}

	public function Boxes( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT COUNT(ab.id_box) AS counter, t.name,t.id_topic
			FROM articles_boxes ab
			LEFT JOIN articles a ON ab.id_article=a.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic 
			WHERE t.id_topic<>$this->temp_id_topic
			GROUP BY t.id_topic ORDER BY t.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ConfigurationUpdate($id_language,$temp_id_topic,$latest,$wday_newsletter,$topic_store,$default_visibility,$campaign_path,$forum_path,$licences,$id_licence,$poll_path,$map_path,$search_path,$map_sort_by)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$topic_store_old = $ini->Get("topic_store");
		$ini->Set("id_language",$id_language);
		$ini->Set("temp_id_topic",$temp_id_topic);
		$ini->Set("latest",$latest);
		$ini->Set("topic_store",$topic_store);
		$ini->Set("default_visibility",$default_visibility);
		$ini->Set("licences",$licences);
		$ini->Set("id_licence",$id_licence);
		$ini->Set("map_sort_by",$map_sort_by);
		$ini->SetModule("topics","wday_newsletter",$wday_newsletter);
		$ini->SetPath("campaign_path",$campaign_path);
		$ini->SetPath("poll_path",$poll_path);
		$ini->SetPath("forum_path",$forum_path);
		$ini->SetPath("map_path",$map_path);
		$ini->SetPath("search_path",$search_path);
		if ($topic_store!=$topic_store_old)
			$this->StoreUpdate($topic_store);
	}
	
	public function Domains()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_topic,name,path,domain FROM topics WHERE domain!='' AND topics.id_topic<>$this->temp_id_topic ORDER BY name";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}

	public function Docs( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT COUNT(id_doc) AS counter, topics.name,topics.id_topic
			FROM docs_articles
			LEFT JOIN articles ON docs_articles.id_article=articles.id_article
			INNER JOIN topics ON articles.id_topic=topics.id_topic AND share_docs=1 
			WHERE topics.id_topic<>$this->temp_id_topic
			GROUP BY name ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Images( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT COUNT(ia.id_image) AS counter, t.name,t.id_topic
		FROM images_articles ia
		LEFT JOIN articles a ON ia.id_article=a.id_article
		INNER JOIN topics t ON a.id_topic=t.id_topic AND t.share_images=1
		GROUP BY t.id_topic ORDER BY t.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Jobs( &$rows )
	{
		$sqlstr = "SELECT queue.id_topic,topics.name,COUNT(id_job) AS counter FROM queue INNER JOIN topics USING(id_topic) GROUP BY queue.id_topic";
		$rows = array();
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, true);
		return $num;
	}

	public function Latest($id_group,$sort_by=0,$not_only_latest=0,$topic_limit=0,$exclude_keywords=array(),$exclude_id_group=0)
	{
		$sql_cond = "";
		if ($id_group>0)
		{
			$topics = $this->gh->GroupItems($id_group);
			$children = array();
			$this->gh->th->GroupChildrenAll($id_group, $children);
			foreach($children as $child)
				$topics = array_merge($this->gh->GroupItems($child['id_group']),$topics);
			if (count($topics)>0)
			{
				$counter = 0;
				$topic_counter = array();
				foreach($topics as $topic)
				{
					if($topic['visible']=="1" && $topic['show_latest']=="1")
					{
						if ($counter>0)
							$sql_cond .= " OR ";
						$sql_cond .= "a.id_topic=$topic[id_item]";
						$counter ++;
						$topic_counter[$topic['id_item']] = 0;
					}
				}
				if ($counter>0)
					$sql_cond = " AND ($sql_cond) ";
				else
					$sql_cond = " AND 1=0 ";
			}
		} elseif ($exclude_id_group>0) {
		    $sql_cond = " AND t.id_group <> {$exclude_id_group} ";
		}
		$limit = $topic_limit>0? $topic_limit * $counter * 10 : $this->latest;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,ah.subhead,UNIX_TIMESTAMP(written) AS written_ts,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_topic,a.id_subtopic,a.available,a.show_author,a.show_date,
			a.id_user,a.author,a.author_notes,a.halftitle,'article' AS item_type,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language,a.highlight
			FROM articles a
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic 
            INNER JOIN topics t ON a.id_topic=t.id_topic ";
		$wheres = '';
		for ($i = 0; $i < count($exclude_keywords); $i++)
		{			
			$sqlstr .= " LEFT JOIN keywords_use ku$i ON ku$i.id=a.id_article AND ku$i.id_type='5' AND ku$i.id_keyword={$exclude_keywords[$i]}  ";
			$wheres .= " AND ku$i.id IS NULL ";
		}
		$sqlstr .= " WHERE a.approved=1 " . $wheres;
		if($not_only_latest==0)
			$sqlstr .= " AND a.show_latest=1 ";
		$sqlstr .= " AND a.published>0 AND s.visible<3 AND t.visible=1 $sql_cond";
		$sqlstr .= $this->LatestOrderBy($sort_by);
		$sqlstr .= " LIMIT $limit ";
		$db->QueryExe($rows, $sqlstr);
		if($topic_limit>0)
		{
			$reduced_rows = array();
			$topic_counter = array();
			foreach($rows as $row)
			{
				if(count($reduced_rows) < $this->latest)
				{
					$current_topic = $row['id_topic'];
					if($topic_counter[$current_topic]<$topic_limit)
					{
						$reduced_rows[] = $row;
						$topic_counter[$current_topic]++;
					}
				}
			}
			$rows = $reduced_rows;
		}
		return $rows;
	}
	
	private function LatestOrderBy($sort_by) // copy from topic.php
	{
		$sqlstr = " ORDER BY ";
		switch($sort_by)
		{
			case "0":
				$sqlstr .= "a.published DESC,";
			break;
			case "1":
				$sqlstr .= "a.written DESC,";
			break;
			case "2":
				$sqlstr .= "a.id_visibility DESC,a.published DESC,";
			break;
			case "3":
				$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
			break;
			case "4":
				$sqlstr .= "a.original DESC,a.written DESC,";
			break;
			case "5":
				$sqlstr .= "a.headline ASC,";
			break;
		}
		$sqlstr .= "a.id_article DESC ";
		return $sqlstr;
	}

	public function LatestNews($sitemap_keyword_id,$days=2)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_topic,
			IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language
			FROM keywords_use ku
			INNER JOIN articles a ON ku.id=a.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
            INNER JOIN topics t ON a.id_topic=t.id_topic 
			WHERE ku.id_keyword='$sitemap_keyword_id' AND a.approved=1 AND a.show_latest=1 AND a.published>0 AND s.visible<3 AND t.visible=1
			AND a.published >= ( CURDATE() - INTERVAL $days DAY ) 
			ORDER BY a.published DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function Links()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,COUNT(id_link) AS counter FROM topics t
			LEFT JOIN links_subtopics USING(id_topic) WHERE t.visible=1 GROUP BY t.id_topic";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Newsletter()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_topic FROM topics WHERE newsletter=1 AND visible=1";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function NoDomains()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE t.visible=1 AND t.domain='' AND t.show_latest=1 ORDER BY t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingNotify()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT u.name,u.email,tu.id_topic,COUNT(a.id_article) AS counter,t.name AS topic_name 
			FROM topic_users tu 
			INNER JOIN users u ON tu.id_user=u.id_user 
			INNER JOIN topics t ON tu.id_topic=t.id_topic 
			LEFT JOIN articles a ON tu.id_topic=a.id_topic AND a.approved=0 
			WHERE tu.id_topic<>$this->temp_id_topic AND tu.is_admin=1 AND tu.pending_notify=1 
			GROUP BY tu.id_user,tu.id_topic HAVING counter>0 
			ORDER BY t.id_topic";
		$db->QueryExe($rows, $sqlstr);
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator(0,0,true);
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		foreach($rows as $row)
		{
			$subject = "[NOTIFY] {$row['topic_name']}";
			$extra = array();
			$url_articles = $mail->admin_web . "/topics/articles.php?id={$row['id_topic']}&appr=0&id_template=-1";
			$url_settings = $mail->admin_web . "/gate/user_topic.php?id={$row['id_topic']}";
			$message = $tr->TranslateParams("pending_notify_msg",array($row['name'],$row['counter'],$row['topic_name'],$url_articles,$url_settings));
			$mail->SendMail($row['email'],$row['name'],$subject,$message,$extra);
		}
	}
	
	public function PeopleGroups(&$rows,$paged=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ptg.id_pt_group,IF(ISNULL(t.name),ptg.name,CONCAT(t.name,': ',ptg.name)),ptg.name,t.name AS topic_name,COUNT(id_p) AS members
			FROM people_topics_groups ptg
			LEFT JOIN people_groups pg ON ptg.id_pt_group=pg.id_pt_group
			LEFT JOIN topics t ON ptg.id_topic=t.id_topic
			GROUP BY ptg.id_pt_group
			ORDER BY t.name, ptg.name";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function ProtectedUpdate($id_user)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$topics = $this->User($id_user);
		foreach($topics as $topic)
		{
			if ($topic['protected']=="1")
			{
				$t = new Topic($topic['id_topic']);
				$t->PasswordUpdate();
			}
		}
	}

	public function Queue()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DISTINCT id_topic FROM queue WHERE id_topic<>'$this->temp_id_topic' ORDER BY id_topic";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function QueueMapUpdate()
	{
		include_once(SERVER_ROOT."/../classes/queue.php");
		$queue = new Queue(0);
		$queue->JobInsert($queue->types['map'],0,"update");
	}
	
	public function SearchPub(&$rows,$words)
	{
		if(count($words)>0)
		{
			$db =& Db::globaldb();
			$sqljoin = "";
			$wheres = array();
			for ($i = 0; $i < count($words); $i++)
			{
				$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = t.id_topic AND si$i.id_res=1 ";
				$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
				$wheres[] = "sw$i.word = '$words[$i]'";
				$scores[] = "SUM(si$i.score)";
			}
			$sqlstr = "SELECT t.id_topic,t.name,t.id_group,tg.name AS group_name,'topic' AS item_type,
				t.description,t.show_latest,t.path,t.domain,
				(" . implode("+",$scores) . ") AS total_score 
		 	FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group $sqljoin
			 WHERE t.visible=1 AND " . implode(" AND ", $wheres) . "
			 GROUP BY t.id_topic
			ORDER BY total_score DESC,tg.seq,t.seq";
			$num = $db->QueryExe($rows, $sqlstr,true);
		}
		else 
			$num = 0;
		return $num;
	}

	public function StoreUpdate($name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "subtopics" );
		$sqlstr = "UPDATE subtopics SET name='$name' WHERE id_type=0";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function SubscribedPerson($id_p)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,t.id_group,t.description,pt.contact,t.path
		FROM people_topics pt
		INNER JOIN topics t ON pt.id_topic=t.id_topic
		WHERE pt.id_p=$id_p  
		GROUP BY pt.id_topic
		ORDER BY t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TopicArchive($id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$res[] = $db->query( "UPDATE topics SET archived='1' WHERE id_topic='$id_topic'" );
		Db::finish( $res, $db);
	}

	public function TopicUpdateGroup($id_topic, $id_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$res[] = $db->query( "UPDATE topics SET id_group='$id_group' WHERE id_topic='$id_topic'" );
		Db::finish( $res, $db);
	}

	public function TopicsUser( $id_user )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,tu.id_user,tg.name AS group_name 
		FROM topics t 
		INNER JOIN topics_groups tg ON t.id_group=tg.id_group
		LEFT JOIN topic_users tu ON t.id_topic=tu.id_topic AND tu.id_user=$id_user
		ORDER BY tg.id_group,t.seq,t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicsUsers( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,count(tu.id_user) AS counter,tg.name AS group_name
		FROM topics t
		INNER JOIN topics_groups tg ON t.id_group=tg.id_group
		LEFT JOIN topic_users tu ON t.id_topic=tu.id_topic 
		GROUP BY t.id_topic,name 
		ORDER BY tg.id_group,t.seq,t.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function User($id_user)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User;
		$u->id = $id_user;
		if ($u->ModuleUserAdmin(21, 4)) {
			$sqlstr = "SELECT t.id_topic,t.name,t.protected,t.id_group,tg.name AS group_name
			FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group
			ORDER BY tg.seq,t.seq,t.name";
		} else {
			$sqlstr = "SELECT t.id_topic,t.name,t.protected,t.id_group,tg.name AS group_name
			 FROM topics t
			 INNER JOIN topics_groups tg ON t.id_group=tg.id_group
			 INNER JOIN topic_users tu ON t.id_topic=tu.id_topic AND tu.id_user=$id_user 
			ORDER BY tg.seq,t.seq,t.name";
		}
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Visible()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE t.visible=1 ORDER BY t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

}
?>
