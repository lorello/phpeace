<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/** Manage content features and their functions
 * 
 * To create a new feature function:
 * - Define its parameters in PageFunctionsParams
 * - Implement its functionality in PageFunction
 * - Define its behavior in the feature XSL template in common.xsl  
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Features
{

	/** 
	 * @var Topic */
	public $topic;
	
	/**
	 * Item type of last executed function
	 * @var string
	 */
	public $item_type;
	
	/**
	 * Information on last executed function
	 * @var string
	 */
	public $info;
	
	/**
	 * Function params
	 * @var array
	 */
	public $params = array();
	
	/**
	 * Current Layout preview mode
	 * @var boolean
	 */
	private $preview;
	
	/**
	 * Current Layout protected mode
	 * @var boolean
	 */
	private $protected;

	/**
	 * Current Layout live mode
	 * @var boolean
	 */
	private $live;
	
	/**
	 * Default value of records per page
	 * @var integer
	 */
	private $records_per_page;

	/**
	 * Initialize local properties 
	 */
	function __construct()
	{
		$this->params = $this->PageFunctionsParams();
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$this->records_per_page = $conf->Get("records_per_page");
	}

	/**
	 * Set the activation status for a specific feature
	 * 
	 * @param integer $id_feature
	 * @param boolean $active
	 */
	public function FeatureActiveSwap($id_feature,$active)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "features" );
		$sqlstr = "UPDATE features SET active=". (($active)? 0 : 1) . " WHERE id_feature=$id_feature";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Delete a specific feature
	 * 
	 * @param integer $id_feature
	 */
	public function FeatureDelete($id_feature)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "features" );
		$res[] = $db->query( "DELETE FROM features WHERE id_feature=$id_feature" );
		Db::finish( $res, $db);
	}
	
	/**
	 * Get feature data
	 * 
	 * @param integer $id_feature
	 * @return array
	 */
	public function FeatureGet($id_feature)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT id_feature,name,description,params,id_user,id_function,active,public,condition_var,condition_type,condition_value,condition_id
		 FROM features WHERE id_feature=$id_feature";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Get all features using a specific function
	 * 
	 * @param integer $id_function
	 * @return array
	 */
	public function FeatureGetByFunction($id_function)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_feature,f.params 
			FROM features f 
			WHERE f.id_function=$id_function" ;
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Get all active features associated to a specifig pagetype, i.e. all features defined for the
	 * pagetype in the common style OR in the specified style
	 * 
	 * @param integer $id_type		ID of pagetype
	 * @param integer $id_module	ID of module (when pagetype = 0)
	 * @param integer $id_style		ID of style
	 * @return array
	 */
	public function FeatureGetByType($id_type,$id_module,$id_style)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_feature,f.name,f.id_function,f.id_user,f.params,f.description ,f.condition_var,f.condition_type,f.condition_value,f.condition_id
			FROM features f 
			INNER JOIN page_features pf ON f.id_feature=pf.id_feature
			WHERE f.active=1 AND pf.id_type=$id_type AND pf.id_module=$id_module AND (pf.id_style=$id_style OR pf.id_style=0)" ;
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Get all active features associated to a specific pagetype and a specific style only
	 * (i.e. inherited features from common style are excluded)
	 * 
	 * @param integer $id_type		ID of pagetype
	 * @param integer $id_module	ID of module (when pagetype = 0)
	 * @param integer $id_style		ID of style
	 * @return array
	 */
	public function FeatureGetByTypeOnly($id_type,$id_module,$id_style)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_feature,f.name,f.id_function,f.params 
			FROM features f 
			INNER JOIN page_features pf ON f.id_feature=pf.id_feature
			WHERE f.active=1 AND pf.id_type=$id_type AND pf.id_module=$id_module AND pf.id_style=$id_style" ;
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Get the pagetype and module IDs for a specific feature,
	 * to find out where it is used
	 * 
	 * @param integer $id_feature
	 * @return array
	 */
	public function FeatureGetTypeAndModule($id_feature)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT id_type,id_module FROM page_features WHERE id_feature=$id_feature";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Get all styles using a specific feature
	 * 
	 * @param integer $id_feature
	 * @return array
	 */
	public function FeatureGetStyles($id_feature)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_style,id_type FROM page_features WHERE id_feature=$id_feature";
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Store a feature
	 * 
	 * @param integer	$id_feature			Feature ID (for update only)
	 * @param string	$name				Feature name
	 * @param string	$description		Feature description
	 * @param integer	$id_function		ID of function used in this feature
	 * @param boolean	$active				Whether the feature is active or not
	 * @param boolean	$public				Whether the feature is publicly accessible or not
	 * @param string	$condition_var		Querystring variable to trigger feature activation
	 * @param integer	$condition_type		Comparison function of querystring variable (1 = isset, 2 = less than, 3 = equals, 4 = greater than, 5 = isnotset)
	 * @param string	$condition_value	Querystring variable value to trigger feature activation
	 * @param integer	$condition_id		Current resource ID to trigger feature activation
	 * @param integer	$id_user			Administrative user ID creating this feature
	 * @return integer						Feature ID
	 */
	public function FeatureStore( $id_feature,$name,$description,$id_function,$active,$public,$condition_var,$condition_type,$condition_value,$condition_id,$id_user )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "features" );
		if ($id_feature>0)
		{
			$sqlstr = "UPDATE features SET name='$name',description='$description',id_function=$id_function,active=$active,public=$public,condition_var='$condition_var',condition_type='$condition_type',condition_value='$condition_value',condition_id='$condition_id'
			WHERE id_feature=$id_feature";
		}
		else
		{
			$id_feature = $db->nextId( "features", "id_feature" );
			$sqlstr = "INSERT INTO features (id_feature,name,description,id_function,id_user,active,public,condition_var,condition_type,condition_value,condition_id) 
				VALUES ($id_feature,'$name','$description',$id_function,'$id_user',$active,$public,'$condition_var',$condition_type,'$condition_value','$condition_id')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_feature;
	}
	
	/**
	 * Get all active features associated to a specifig global pagetype
	 * @return array
	 */
	public function GlobalFeatureGetByType($id_type)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.id_feature,f.name,f.id_function,f.id_user,f.params,f.description 
			FROM features f 
			INNER JOIN global_features gf ON f.id_feature=gf.id_feature
			WHERE f.active=1 AND gf.id_type=$id_type ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Get the global pagetype for a specific feature,
	 * to find out where it is used
	 * 
	 * @param integer $id_feature
	 * @return array
	 */
	public function GlobalFeatureGetType($id_feature)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT id_type
		 FROM global_features WHERE id_feature=$id_feature";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Get all features associated to a global pagetype, paginated
	 * 
	 * @param array	$rows
	 * @param integer $id_type
	 * @return integer
	 */
	public function GlobalPageFeatures(&$rows,$id_type)
	{
		$sqlstr = "SELECT gf.id_feature,f.name,f.description,f.active,f.id_user,f.id_function,f.public
		FROM global_features gf
		INNER JOIN features f ON gf.id_feature=f.id_feature
		WHERE gf.id_type=$id_type ORDER BY f.name";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Get all features associated to global pagetypes
	 * 
	 * @param array		$rows
	 * @param boolean	$paged	Whether results are paginated or not
	 * @return integer
	 */
	public function GlobalPageFeaturesAll(&$rows,$paged=true)
	{
		$sqlstr = "SELECT gf.id_feature,f.name,f.description,gf.id_type,f.id_function,f.active,f.id_user
		FROM global_features gf
		INNER JOIN features f ON gf.id_feature=f.id_feature
		ORDER BY gf.id_type,f.name";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	/**
	 * Set current Layout mode
	 * 
	 * @param boolean $preview
	 * @param boolean $protected
	 * @param boolean $live
	 */
	public function LayoutMode($preview,$protected,$live)
	{
		$this->preview = $preview;
		$this->protected = $protected;
		$this->live = $live;
	}

	/**
	 * Get all features associated to a specific pagetype and style (paginated)
	 * 
	 * @param array		$rows
	 * @param integer $id_type		ID of pagetype
	 * @param integer $id_style		ID of style
	 * @param integer $id_module	ID of module (when pagetype = 0)
	 * @return integer				Number of features
	 */
	public function PageFeatures(&$rows,$id_type,$id_style,$id_module=0)
	{
		$sqlstr = "SELECT pf.id_feature,f.name,f.description,f.active,f.id_user,f.public
		FROM page_features pf
		INNER JOIN features f ON pf.id_feature=f.id_feature
		WHERE pf.id_type=$id_type AND pf.id_style=$id_style ";
		if($id_module>0)
			$sqlstr .= " AND pf.id_module=$id_module ";
		$sqlstr .= " ORDER BY f.name";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Get all features associated to a specific style and an optional pagetype
	 * 
	 * @param array		$rows
	 * @param integer 	$id_style	Style ID
	 * @param integer	$id_type	Optional pagetype ID
	 * @param boolean	$paged		Whether results are paginated or not
	 * @return integer				Number of features
	 */
	public function PageFeaturesStyle(&$rows,$id_style,$id_type,$paged=true)
	{
		$sqlstr = "SELECT pf.id_feature,f.name,f.description,pf.id_type,f.id_function,f.active,f.id_user,pf.id_module
		FROM page_features pf
		INNER JOIN features f ON pf.id_feature=f.id_feature
		WHERE pf.id_style=$id_style " . (($id_type>=0)? " AND pf.id_type=$id_type":"") . " 
		ORDER BY pf.id_module,pf.id_type,f.name";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Execute a specific feature function
	 * Set local variables (info, item_type) and return an array of items
	 *  
	 * @param integer	$id_function	Function ID
	 * @param array		$params			Function parameters
	 * @param array		$params_in		Current pagetype parameters, optionally used by the function 
	 * @return array 					Results
	 */
	public function PageFunction($id_function,$params,$params_in)
	{
		$this->item_type = NULL;
		$this->info = NULL;
		if ($params['id_topic']!="id_topic" && $params['id_topic']>0)
		{
			$this->TopicInit($params['id_topic']);
		}
		$limit = ($params['limit']>0)? $params['limit'] : $this->records_per_page;
		$rows = array();
		switch($id_function)
		{
			case "0":
			break;
			case "1":
				$this->item_type = ($params['with_content'])? "article_full" : "article";
				$id_subtopic = (int)$params['id_subtopic'];
				$sort_by = (int)$params['sort_by'];
				$show_latest = (int)$params['show_latest'];
				$exclude_id_group = (int)$params['exclude_id_group'];
				$irl = new IRL();
				$info = array();
				if($id_subtopic>0)
				{
					if ($this->topic->id>0)
					{
						$recurse = (int)$params['recurse'];
						$rows = $this->topic->SubtopicLatest($id_subtopic,$limit,$recurse,$sort_by,$show_latest,false);
					}
					$info = array('subtopic_name'=>$rows[0]['subtopic_name'],'subtopic_id'=>$id_subtopic);
					$info['url'] = $irl->PublicUrlTopic("subtopic",array('id'=>$id_subtopic),$this->topic);
				}
				else
				{
					$exclude = array();
					$exclude_keywords = array();
					if($params['exclude_keywords']!="")
					{
						$exclude = explode(",",$params['exclude_keywords']);
						if(is_array($exclude) && count($exclude)>0)
						{
							foreach($exclude as $kw)
							{
								$kw = (int)$kw;
								if($kw>0)
									$exclude_keywords[] = $kw;
							}
						}
					}
					$topic_limit = (int)$params['topic_limit'];
					if (($params['id_topic']=="id_topic" || $params['id_topic']>0) && isset($this->topic) && $this->topic->id>0)
					{
						$rows = $this->topic->Latest($limit,$sort_by,$show_latest,$exclude_keywords);
						$info = array('topic_name'=>$this->topic->name);
						$info['url'] = $irl->PublicUrlTopic("topic_home",array(),$this->topic);
					}
					elseif ($params['id_topic_group']>0)
					{
						include_once(SERVER_ROOT."/../classes/topics.php");
						$topics = new Topics;
						$topics->gh->LoadTree();
						$group = $topics->gh->GroupGet($params['id_topic_group']);
						$info = array('group_name'=>$group['name'],'id_group'=>$params['id_topic_group']);
						$info['url'] = $irl->PublicUrlGlobal("map",array('id'=>$params['id_topic_group']));
						$topics->latest = $limit;
						$rows = $topics->Latest($params['id_topic_group'],$sort_by,$show_latest,$topic_limit,$exclude_keywords);
					}
					else 
					{
						include_once(SERVER_ROOT."/../classes/topics.php");
						$topics = new Topics;
						$topics->latest = $limit;
						$rows = $topics->Latest(0,$sort_by,$show_latest,$topic_limit,$exclude_keywords,$exclude_id_group);
					}
				}
				if (count($info)>0)
					$this->info = $info;
			break;
			case "2":
				$this->item_type = ($params['with_content'])? "article_full" : "article";
				$exclude_keywords = array();
				$info = array();
				if ($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$row = $k->KeywordGet($params['id_keyword']);
					$sort_by = (int)$params['sort_by'];
					$id_topic = ($params['all_topics'])? 0 : (int)($this->topic->id);
					$exclude = array();
					if($params['exclude_keywords']!="")
					{
						$exclude = explode(",",$params['exclude_keywords']);
						if(is_array($exclude) && count($exclude)>0)
						{
							foreach($exclude as $kw)
							{
								$kw = (int)$kw;
								if($kw>0)
									$exclude_keywords[] = $kw;
							}
						}
					}
					$year = (int)$params['year']; 
					$id_subtopic = (int)$params['id_subtopic']; 
					$rows = $k->UseArticles($params['id_keyword'],$id_topic,$limit,$sort_by,$exclude_keywords,$year,$id_subtopic);
					$irl = new IRL();
					$info = array('keyword'=>$row['keyword'],'description'=>$row['description'],'url'=>$irl->PublicUrlGlobal("keyword",array('k'=>$row['keyword'])));
				}
				$this->info = $info;
			break;
			case "3":
				$this->item_type = "topic";
				$id_group = (int)$params['id_group'];
				if($id_group>0)
				{
					include_once(SERVER_ROOT."/../classes/topics.php");
					$topics = new Topics;
					$rows = $topics->gh->GroupItemsVisible($id_group);
					if (count($rows)>0)
					{
						$group = $topics->gh->GroupGet($id_group);
						$this->info = array('group_name'=>$group['name'],'id_group'=>$id_group);
					}
				}
				else 
				{
					UserError("Group not set in feature of type Topic List",array(),512);
				}
			break;
			case "4":
				$this->item_type = "event";
				$id_topic = ($params['id_topic']>0)? $params['id_topic']:0;
				$this->TopicInit($id_topic);
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$ee->id_topic = $id_topic;
				$ee->id_group = ($params['id_group']>0)? $params['id_group']:0;
				$rows = $ee->Next( $limit, $params['event_type'], $id_topic==0 );
				$irl = new IRL();
				$this->info = array('url'=>$irl->PublicUrlGlobal("events",array()));
			break;
			case "5":
				$this->item_type = "xml";
				$id_xml = $params['id_xml'];
				include_once(SERVER_ROOT."/../classes/xmlhelper.php");
				$xh = new XmlHelper();
				$row = $xh->XmlFragmentGet($id_xml);
				$rows[] = array('xname'=>"xml",'xxml'=>$row['xml']);
			break;
			case "6":
				$this->item_type = ($params['with_content'])? "article_full" : "article";
				$id_article = $params['id_article'];
				if ($id_article>0)
				{
					include_once(SERVER_ROOT."/../classes/article.php");
					$a = new Article($id_article);
					$article = $a->ArticleGet();
					if (count($article)>0)
						$rows[] = $article;
				}
			break;
			case "7":
				$this->item_type = "subtopic";
				include_once(SERVER_ROOT."/../classes/topic.php");
				$this->TopicInit($params['id_topic']);
				$subtopic = $this->topic->SubtopicGet($params['id_subtopic']);
				if($params['with_children']=="1")
					$subtopic['with_children'] = true;
				$rows[] = $subtopic;
			break;
			case "8":
				$this->item_type = "galleries_image";
				include_once(SERVER_ROOT."/../classes/gallery.php");
				$g = new Gallery($params['id_gallery']);
				$g->sort_order = (int)$params['gallery_sort'];
				$images = $g->Images($limit);
				$irl = new IRL();
				if(isset($this->topic) && $this->topic->id>0)
				{
					$id_subtopic = $this->topic->HasSubtopicTypeItem($this->topic->subtopic_types['gallery'],$g->id);
					if($id_subtopic>0)
						$url = $irl->PublicUrlTopic("subtopic",array('id'=>$id_subtopic),$this->topic);
					else
					 	$url = $irl->PublicUrlGlobal("galleries",array('id_gallery'=>$params['id_gallery']));
				}
				else
				 	$url = $irl->PublicUrlGlobal("galleries",array('id_gallery'=>$params['id_gallery']));
				$this->info = array('title'=>$g->title,'id_topic'=>$g->id_topic,'url'=>$url);
				foreach($images as $image)
				{
					$image['is_thumb'] = ($params['size']!="1");
					if($params['size']=="2")
						$image['zoom'] = "2";
					if($g->id_topic>0)
						$image['id_topic'] = (int)$g->id_topic;
					elseif(isset($this->topic) && $g->id_topic_group==$this->topic->id_group)
						$image['id_topic'] = $this->topic->id;
					if(isset($this->topic) && $id_subtopic>0)
						$image['id_subtopic'] = $id_subtopic;
					$image['use_image_link'] = $params['use_images_link']? "1":"0";
					$rows[] = $image;
				}
			break;
			case "9":
				$this->item_type = "book_category";
				include_once(SERVER_ROOT."/../modules/books.php");
				$p = new Publisher;
				$pub = $p->PublisherGet($params['id_publisher']);
				$categories = $p->CategoriesAll($params['id_publisher']);
				$rows = $categories;
				$this->info = array('name'=>$pub['name']);
			break;
			case "10":
				include_once(SERVER_ROOT."/../modules/books.php");
				$bb = new Books();
				if ($params['select']=="0")
					$params['sort_by'] = "random";
				$rows2 = $bb->BooksAll(array('id_publisher'=>$params['id_publisher'],'id_category'=>$params['id_category'],'approved'=>"1"),$limit);
				if ($params['item_type']=="1")
				{
					$this->item_type = "book_big";
					foreach($rows2 as $row2)
					{
						$b = new Book($row2['id_book']);
						$rows['b_' . $row2['id_book']] = $b->BookGet();
					}
				}
				else
				{
					$this->item_type = "book";
					$rows = $rows2;
				}
			break;
			case "11":
				$this->item_type = "xml";
				if($params['url']!="")
				{
					include_once(SERVER_ROOT."/../classes/rss.php");
					$r = new Rss();
					$limit = (int)$params['limit']>0? (int)$params['limit']:15;
					$rss = $r->Get($params['url'],$params['ttl'],$limit);
					$rows[] = array('xname'=>"rss",'xxml'=>$rss);
				}
			break;
			case "12":
				$this->item_type = "text";
				$id_content = $params['id_content'];
				include_once(SERVER_ROOT."/../classes/texthelper.php");
				$th = new TextHelper();
				$rows[] = $th->ContentGet($id_content);
			break;
			case "13":
				$this->item_type = "topics";
				include_once(SERVER_ROOT."/../classes/topics.php");
				$tt = new Topics;
				$rows = $tt->gh->GroupsAll(true,true);
			break;
			case "14":
				$this->item_type = "galleries";
				include_once(SERVER_ROOT."/../classes/galleries.php");
				$gg = new Galleries();
				$rows = $gg->gh->GroupsAll(true,true);
			break;
			case "15":
				$this->item_type = "topic_full";
				include_once(SERVER_ROOT."/../classes/layout.php");
				$l = new Layout($this->preview,$this->protected,$this->live);
				$l->TopicInit($params['id_topic'],true);
				$rows[] = $l->TopicCommon($params['with_menu']=="1");
			break;
			case "16":
				$this->item_type = "book";
				if ($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$row = $k->KeywordGet($params['id_keyword']);
					$rows = $k->UseBooks($params['id_keyword'],$limit,$params['year']);
				}
				$this->info = array('keyword'=>$row['keyword'],'description'=>$row['description']);
			break;
			case "17":
				$this->item_type = "book";
				if ($params['id_book']>0)
				{
					include_once(SERVER_ROOT."/../modules/books.php");
					$b = new Book($params['id_book']);
					$book = $b->BookGet();
					$rows[] = $book;
				}
			break;
			case "18":
				$this->item_type = "gallery";
				if ($params['id_gallery']>0)
				{
					include_once(SERVER_ROOT."/../classes/gallery.php");
					$g = new Gallery($params['id_gallery']);
					$this->info = array('title'=>$g->title,'id_topic'=>$g->id_topic);
					$row = $g->row;
					$row['id_item'] = $row['id_gallery'];
					$row['name'] = $row['title'];
					$row['with_details'] = true;
					if($this->topic->id>0)
					{
						$id_subtopic = $this->topic->HasSubtopicTypeItem($this->topic->subtopic_types['gallery'],$g->id);
						if($id_subtopic>0)
							$row['id_subtopic'] = $id_subtopic;
					}
					$rows[] = $row;
				}
			break;
			case "19":
				$this->item_type = "article";
				$keywords = $params_in['keywords'];
				if(is_array($keywords) && count($keywords)>0)
				{
					$res_keywords = array();
					include_once(SERVER_ROOT."/../classes/ontology.php");
					$o = new Ontology;
					foreach($keywords as $keyword)
					{
						if(!in_array($keyword['id'],$res_keywords))
							$res_keywords[] = $keyword['id'];
						if($params['infer']=="1")
						{
							$neighbours = $o->KeywordNeighbours($keyword['id']);
							foreach($neighbours as $id_neighbour=>$neighbour)
							{
								if(!in_array($id_neighbour,$res_keywords))
									$res_keywords[] = $id_neighbour;
							}						
						}
					}
					if(count($res_keywords)>0)
					{
						include_once(SERVER_ROOT."/../classes/keyword.php");
						$k = new Keyword();
						$sort_by = (int)$params['sort_by'];
						$id_topic = $id_group = 0;
						if($params['id_topic']>0)
							$id_topic = $params['id_topic'];
						if($params['id_group']>0)
							$id_group = $params['id_group'];
						$exclude_id_group = (int)$params['exclude_id_group'];
						$rows = $k->UseArticlesMultipleOr($res_keywords,$id_topic,$id_group,$limit,$sort_by,$o->types['article'],$params_in['id_article'],$exclude_id_group);
					}
				}
				$this->info = array();
			break;
			case "20":
				$this->item_type = "text";
				if ($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$row = $k->KeywordGet($params['id_keyword']);
					$rows = $k->UseTexts($params['id_keyword'],$id_topic,$limit);
				}
				$this->info = array('keyword'=>$row['keyword'],'description'=>$row['description']);
			break;
			case "21":
				$this->item_type = "list";
				if ($params['id_group']>0)
				{
					include_once(SERVER_ROOT."/../modules/lists.php");
					$li = new Lists();
					$group = $li->GroupGet($params['id_group']);
					$rows = $li->GroupLists($params['id_group']);
					$this->info = array('group'=>$group['name'],'id_group'=>$params['id_group']);
				}
			break;
			case "22":
				$this->item_type = "video_enc";
				if ($params['id_video']!="")
				{
					include_once(SERVER_ROOT."/../classes/video.php");
					$vi = new Video();
					if(is_numeric($params['id_video']))
						$video = $vi->VideoGet($params['id_video'],true);
					elseif($vi->IsHash($params['id_video']))
						$video = $vi->VideoGetByHash($params['id_video']);
					if($video['id_video']>0)
					{
						$rows[0] = $video;
						$this->info = array('title'=>$video['title']);
					}
				}
			break;
			case "23":
				$this->item_type = "gallery";
				if ($params['id_gallery']>0)
				{
					include_once(SERVER_ROOT."/../classes/gallery.php");
					$g = new Gallery($params['id_gallery']);
					$this->info = array('title'=>$g->title,'id_topic'=>$g->id_topic);
					$row = $g->row;
					$row['id_item'] = $row['id_gallery'];
					$row['name'] = $row['title'];
					$row['slideinfo'] = true;
					if(strlen($params['audio'])>0)
						$row['audio'] = $params['audio'];
					if($params['show_captions'])
						$row['show_captions'] = true;
					if($params['shuffle'])
						$row['shuffle'] = true;
					$row['with_details'] = true;
					if($this->topic->id>0)
					{
						$id_subtopic = $this->topic->HasSubtopicTypeItem($this->topic->subtopic_types['gallery'],$g->id);
						if($id_subtopic>0)
							$row['id_subtopic'] = $id_subtopic;
					}
					$rows[] = $row;
				}
			break;
			case "24":
				$this->item_type = "event";
				$id_keyword = (int)$params['id_keyword'];
				if($id_keyword>0)
				{
					$id_topic = (int)$params['id_topic'];
					$this->TopicInit($id_topic);
					include_once(SERVER_ROOT."/../classes/events.php");
					$ee = new Events();
					$ee->id_topic = $id_topic;
					$ee->id_group = 0;
					if($params['id_group']>0)
					{
						$ee->id_topic = 0;
						$ee->id_group = $params['id_group'];
					}
					$rows = $ee->Keyword($id_keyword,$limit,(int)$params['year']);
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$krow = $k->KeywordGet($id_keyword);
					$this->info = array('keyword'=>$krow['keyword'],'description'=>$krow['description']);
				}
			break;
			case "25":
				$this->item_type = "xml";
				if($params['id_group']>0)
				{
					include_once(SERVER_ROOT."/../classes/rssmanager.php");
					$rsm = new RssManager();
					$xml = $rsm->GetAggregation($params['id_group']);
					$rows[] = array('xname'=>"xml",'xxml'=>$xml);
				}
			break;
			case "26":
				$this->item_type = "link";
				if ($params['id_keyword']>0)
				{
					include_once(SERVER_ROOT."/../classes/keyword.php");
					$k = new Keyword();
					$row = $k->KeywordGet($params['id_keyword']);
					$rows = $k->UseLinks($params['id_keyword'],$params['id_topic'],$limit);
				}
				$this->info = array('keyword'=>$row['keyword'],'description'=>$row['description']);
			break;
			case "27":
				$this->item_type = ($params['with_content'])? "article_full" : "article";
				if ($params['id_keywords']>0)
				{
					$sort_by = (int)$params['sort_by'];
					$id_topic = (int)($this->topic->id);
					$id_group = (int)$params['id_group'];
					$keywords = array();
					$keywords_info = array();
					$id_keywords = array();
					if($params['id_keywords']!="")
					{
						include_once(SERVER_ROOT."/../classes/keyword.php");
						$k = new Keyword();
						$keywords = explode(",",$params['id_keywords']);
						if(is_array($keywords) && count($keywords)>0)
						{
							foreach($keywords as $kw)
							{
								$kw = (int)$kw;
								if($kw>0)
								{
									$id_keywords[] = $kw;
									$row = $k->KeywordGet($kw);
									$keywords_info['k_'.$kw] = array('xname'=>"keyword",'id'=>$kw,'name'=>$row['keyword'],'description'=>$row['description']);
								}
							}
						}
					}
					$year = (int)$params['year']; 
					if(count($id_keywords>0))
						$rows = $k->UseArticlesMultipleAnd($id_keywords,$id_topic,$id_group,$limit,$sort_by,$year);
				}
				$this->info = $keywords_info;
			break;
			case "28":
				$this->item_type = "r_event";
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$ts = time();
				$rows = $ee->RecurringEvents($ts);
				$this->info = array('day'=>date("d",$ts),'month'=>date("m",$ts));
			break;
			case "29":
				$this->item_type = "xml";
				if($params['url']!="" || $params['local_file']!="")
				{
					include_once(SERVER_ROOT."/../classes/cache.php");
					$cache = new Cache();
					if(isset($params['ttl']) && $params['ttl']>0)
						$cache->ttl = $params['ttl'];
					if($params['url']!="")
					{
						$url = $params['url'];
						if($params['url_push_down_params']!="")
						{
							$url_push_down_params = explode(",",$params['url_push_down_params']);
							if(count($url_push_down_params)>0)
							{
								$url .=  ((strpos($url,"?")===false)? "?":"&");
								foreach($url_push_down_params as $upd_param)
								{
									$upd_name = $upd_param;
									if(strpos($upd_param,"|")!==false)
									{
										$upd_param2 = explode("|",$upd_param);
										$upd_param = $upd_param2[0];
										$upd_name = $upd_param2[1];  
									}
									$url .= "$upd_name=" . $_GET[$upd_param] . "&";
								}
							}
						}
						$xml = $cache->Get("xml",$url);
					}
					elseif(strpos($params['local_file'],"..")===false)
					{
						$xml = $cache->Get("xmlfile","import/" . $params['local_file']);
					}
					$rows[] = array('xname'=>"xml",'xxml'=>$xml);
					if($xml!="" && $params['iterate_xpath']!="" && ($params['iterate_url']!="" || $params['iterate_local_file']!=""))
					{
						$xmldoc = new SimpleXMLElement($xml);
						$nodes = $xmldoc->xpath($params['iterate_xpath']);
						if(count($nodes)>0)
						{
							$xnodes = array('xname'=>"iteration",'xpath'=>$params['iterate_xpath']);
							$counter = 1;
							foreach($nodes as $node)
							{
								if($params['iterate_url']!="")
								{
									$node_xml = $cache->Get("xml",$params['iterate_url'] . $node);
								}
								elseif(strpos($params['iterate_local_file'],"..")===false)
								{
									$node_xml = $cache->Get("xmlfile","import/" . $params['iterate_local_file'] . $node);
								}
								$xnodes['node'.$counter] = array('xname'=>"xml",'value'=>$node,'xxml'=>$node_xml);
								$counter ++;
							}
						}
						$rows[] = $xnodes;
					}
				}
			break;
			case "30":
				$this->item_type = "keyword";
				$id_subtopic = (int)$params['id_subtopic'];
				include_once(SERVER_ROOT."/../classes/ontology.php");
				$o = new Ontology();
				$rows = $o->TagCloud($params['id_topic'],$id_subtopic,$limit);
				$this->info = array();
			break;
            case "31":
                $this->item_type = "xml";
                if ($params['page_id'] != "" && $params['token'] != "") {
                    $endpoint = $params['endpoint'] != "" ? $params['endpoint'] : 'posts';
                    $url = "https://graph.facebook.com/{$params['page_id']}/{$endpoint}?access_token={$params['token']}";
                    include_once (SERVER_ROOT . "/../classes/cache.php");
                    $cache = new Cache();
                    if (isset($params['ttl']) && (int) $params['ttl'] > 0) {
                        $cache->ttl = $params['ttl'];
                    }
                    $json = $cache->Get("json", $url);
                    if (strlen($json) > 0) {
                        $json_array = json_decode($json, true);
                        switch ($endpoint) {
                            case "posts":
                                $this->item_type = "post";
                                if (isset($json_array['data']) && is_array($json_array['data'])) {
                                    foreach ($json_array['data'] as $post) {
                                        $post['xname'] = 'item';
                                        $post['ts'] = strtotime($post['created_time']);
                                        $rows[] = $post;
                                    }
                                }
                                break;
                        }
                    }
                }
                $this->info = array();
                break;
		}
		return $rows;
	}
	
	/**
	 * Parameters of feature functions
	 * 
	 * @return array 
	 */
	private function PageFunctionsParams()
	{
		$params = array();
		$params[0] = array();
		$params[1] = array('limit','id_subtopic','recurse','id_topic','id_topic_group','show_latest','with_content','sort_by','topic_limit','exclude_keywords','exclude_id_group');
		$params[2] = array('limit','id_keyword','all_topics','with_content','sort_by','exclude_keywords','year','id_subtopic');
		$params[3] = array('id_group');
		$params[4] = array('limit','id_group','id_topic','event_type');
		$params[5] = array('id_xml');
		$params[6] = array('id_article','with_content');
		$params[7] = array('id_topic','id_subtopic','with_children');
		$params[8] = array('limit','id_gallery','size','gallery_sort','use_images_link'); 
		$params[9] = array('id_publisher');
		$params[10] = array('limit','id_publisher','id_category','select','item_type'); // select 0: random, 1: date desc
		$params[11] = array('url','ttl','limit');
		$params[12] = array('id_content');
		$params[13] = array();
		$params[14] = array();
		$params[15] = array('id_topic','with_menu');
		$params[16] = array('limit','id_keyword','year');
		$params[17] = array('id_book');
		$params[18] = array('id_gallery');
		$params[19] = array('id_topic','id_group','limit','sort_by','infer','exclude_id_group');
		$params[20] = array('limit','id_keyword');
		$params[21] = array('id_group');
		$params[22] = array('id_video');
		$params[23] = array('id_gallery','shuffle','show_captions','audio');
		$params[24] = array('limit','id_keyword','id_topic','id_topic_group','year');
		$params[25] = array('id_group');
		$params[26] = array('limit','id_keyword','id_topic');
		$params[27] = array('limit','id_keywords','id_topic','id_topic_group','with_content','sort_by','year');
		$params[28] = array();
		$params[29] = array('url','url_push_down_params','local_file','ttl','iterate_xpath','iterate_url','iterate_local_file');
		$params[30] = array('id_topic','id_subtopic','limit');
		$params[31] = array('page_id','token','endpoint','ttl');
		return $params;

	/**
	 * Store the serialized function parameters for a specific feature
	 * 
	 * @param integer	$id_feature	Feature ID
	 * @param string	$params		Serialized array of function parameters
	 */
	}
	
	/**
	 * Store the function paramenters settings for a specific feature
	 * 
	 * @param integer 	$id_feature	Feature ID
	 * @param string 	$params		Serialized function parameters
	 */
	public function PageFunctionsParamsStore($id_feature,$params)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "features" );
		$sqlstr = "UPDATE features SET params='$params' WHERE id_feature='$id_feature' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Deserialize function parameters
	 * 
	 * @param integer $params
	 * @return array
	 */
	public function ParamsDeserialize($params)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		return $v->Deserialize($params);
	}
	
	/**
	 * Set local topic instance
	 * 
	 * @param integer $id_topic
	 */
	public function TopicInit($id_topic)
	{
		if (isset($this->topic) && $id_topic!=$this->topic->id)
		{
			if(isset($this->topic))
			{
				$this->topic->__destruct();
			}
			$this->topic = null;
			unset($this->topic);
		}
		include_once(SERVER_ROOT."/../classes/topic.php");
		$this->topic = new Topic($id_topic);
	}
}
?>
