<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Generic function for documents management
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
abstract class Docs
{
	/**
	 * All documents
	 *
	 * @return array
	 */
	public static function Count()
	{
		$db =& Db::globaldb();
		$row = array();
		$db->query_single($row, "SELECT count(id_doc) AS count_docs FROM docs");
		return $row['count_docs'];
	}

	/**
	 * Generate all covers
	 *
	 */
	public static function CoversUpdate()
	{
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, "SELECT id_doc,format FROM docs WHERE format!='' ");
		include_once(SERVER_ROOT."/../classes/doc.php");
		foreach($rows as $row)
		{
			if($row['id_doc']>0)
			{
				$d = new Doc($row['id_doc']);
				$d->DocCover($row['id_doc'], $row['format']);
			}
		}
	}
	
	/**
	 * Retrieve all documents not associated to any article
	 *
	 * @param array $rows
	 * @param boolean $paged	Paginate results
	 * @return integer			Num of documents
	 */
	public static function Orphans( &$rows, $paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT d.id_doc,d.title,d.description
				FROM docs d 
				LEFT JOIN docs_articles da ON d.id_doc=da.id_doc 
				LEFT JOIN docs_orgs do ON d.id_doc=do.id_doc 
				WHERE da.id_article IS NULL OR do.id_org=NULL";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Delete all documents not associated to any article
	 *
	 */
	public static function OrphansRemove()
	{
		include_once(SERVER_ROOT."/../classes/doc.php");
		$docs = array();
		self::Orphans($docs,false);
		foreach ($docs as $doc)
		{
			$d = new Doc($doc['id_doc']);
			$d->DocRemove($doc['id_doc'],false);
		}
	}

	/**
	 * All documents associated to articles of a specific topic (paginated)
	 *
	 * @param array $rows
	 * @param integer $id_topic
	 * @return integer				Num of documents
	 */
	public static function Topic( &$rows, $id_topic)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT da.id_doc,d.title,d.description,d.filename,d.format,COUNT(dt.token) AS tokens 
		FROM docs_articles da
		INNER JOIN docs d ON da.id_doc=d.id_doc
		INNER JOIN articles a ON da.id_article=a.id_article
		LEFT JOIN docs_tokens dt ON dt.id_doc=d.id_doc
		WHERE a.id_topic=$id_topic
		GROUP BY da.id_doc ORDER BY da.id_doc DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}
?>
