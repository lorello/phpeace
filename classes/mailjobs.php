<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/mail.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/config.php");

class Mailjobs
{
	/** 
	 * @var Mail */
	private $mail;

	/** 
	 * @var Session */
	private $session;

	public $attachment_max_size;

	private $mailjob_limit;
	
	private $recipient_varname;
	public $profiling_varname;
	private $path;
	public $bounces_threshold;
	
	function __construct($id_module=0)
	{
		$this->mail = new Mail();
		$this->session = new Session();
		$conf = new Configuration();
		$this->mailjob_limit = $conf->Get("mailjob_limit");
		$this->attachment_max_size = $conf->Get("attachment_max_size");
		$this->bounces_threshold = $conf->Get("bounces_threshold");
		$this->path = "uploads/mailjobs";
		if($id_module>0)
		{
			$this->recipient_varname = "mj_recipients_" . $id_module;
			$this->profiling_varname = "mj_profiling_" . $id_module;
		}
	}
	
	public function Filename($id_mail,$extension)
	{
		return $this->path . "/" . $id_mail . "." . $extension;
	}
	
	private function LotUpdate($id_mail,$lot,$track)
	{
		if($track)
		{
			$sqlstr = "UPDATE mailjob_recipients SET status=1 WHERE id_mail='$id_mail' AND lot='$lot' AND status=0";
		}
		else
		{
			$sqlstr = "DELETE FROM mailjob_recipients WHERE id_mail='$id_mail' AND lot='$lot' ";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "mailjob_recipients" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$lot = $this->NextLot($id_mail);
		if(count($lot)==0)
			$this->MailjobCompleted($id_mail);
	}

	public function MailJobCheck( $id_module, $id_item )
	{
		$mjs = array();
		$check = false;
		$db =& Db::globaldb();
		$db->query_single( $mjs, "SELECT id_mail FROM sendmail WHERE id_module='$id_module' AND id_item='$id_item' AND sent=0 ");
		if ($mjs['id_mail']>0)
			$check = true;
		return $check;
	}
	
	private function MailjobCompleted($id_mail)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "sendmail" );
		$res[] = $db->query( "UPDATE sendmail SET sent=1 WHERE id_mail=$id_mail " );
		Db::finish( $res, $db);
		$this->NotifyEnd($id_mail);
	}

	public function MailJobDelete( $id_mail )
	{
		$row = $this->MailJobGet($id_mail);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "mailjob_recipients" );
		$res[] = $db->query( "DELETE FROM mailjob_recipients WHERE id_mail='$id_mail' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "sendmail" );
		$res[] = $db->query( "DELETE FROM sendmail WHERE id_mail='$id_mail' " );
		Db::finish( $res, $db);
		if($row['format']!="")
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$fm->Delete($this->Filename($id_mail,$row['format']));
			$fm->PostUpdate();
		}
	}

	public function MailJobGet( $id_mail )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_mail,UNIX_TIMESTAMP(send_date) AS send_date_ts,id_module,id_item,subject,body,id_user,sent,how_many,is_html,
			from_name,from_email,footer,send_password,format,id_topic,track,track_redirect,id_article,auto_footer,alt_text,embed_images
			FROM sendmail WHERE id_mail='$id_mail' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function MailJobUpdate( $id_mail,$subject,$body,$alt_text,$footer )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "sendmail" );
		$sqlstr = "UPDATE sendmail SET subject='$subject',body='$body',alt_text='$alt_text',footer='$footer'
		  WHERE id_mail='$id_mail' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function MailJobsAll( &$rows,$id_topic=0)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT s.id_mail,UNIX_TIMESTAMP(send_date) AS send_date_ts,subject,sent,from_name,how_many,id_module,id_item,track
			FROM sendmail s ";
		if($id_topic>0)
			$sqlstr .= " WHERE s.id_topic=$id_topic ";
		$sqlstr .= " GROUP BY s.id_mail
			ORDER BY s.send_date DESC, s.id_mail DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function MessageFooter($id_topic,$send_password,$is_html=false,$with_direct_link=false)
	{
		$ini = new Ini();
		$title = $this->mail->title;
		$url = $this->mail->pub_web;
		$id_language = $ini->Get("id_language");
		$staff_email = $this->mail->staff_email;
		if($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$title = $t->name;
			$url = $t->url;
			$id_language = $t->id_language;
			if($t->row['temail']!="" && $this->mail->CheckEmail($t->row['temail']))
				$staff_email = $t->row['temail'];
		}
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($id_language,0,false);
		if($this->ProfilingGet())
		{
			$users_path = $ini->Get("users_path");
			$user_profile = $this->UserProfile($id_topic,"$users_path/mail.php");
			if($send_password)
				$footer = $tr->TranslateParams("mailjob_footer_password",array($title,$url,$user_profile,$staff_email));
			elseif($with_direct_link) 
			{
				$footer = $tr->TranslateParams("mailjob_footer_unsubscribe",array($title,$url,"[[UNSUBSCRIBE]]",$staff_email));
			}
			else 
			{
				$user_reminder = $this->UserProfile($id_topic,"$users_path/reminder.php") . (($id_topic>0)?"&":"?") . "email=[[EMAIL]]";
				$footer = $tr->TranslateParams("mailjob_footer",array($title,$url,$user_profile,$user_reminder,$staff_email));
			}
		}
		else 
			$footer = $tr->TranslateParams("mailjob_footer_generic",array($title,$url,$staff_email));
		if(!$is_html)
		{
			$footer = "\n\n--------------\n$footer\n\n";
		}
		return $footer;
	}
	
	private function NextLot($id_mail=0)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT min(mr.lot) AS lot,mr.id_mail 
			FROM mailjob_recipients mr 
			INNER JOIN sendmail s ON mr.id_mail=s.id_mail 
			WHERE s.send_date<=NOW() AND mr.status=0";
		if($id_mail>0)
			$sqlstr .= " AND mr.id_mail=$id_mail ";
		$sqlstr .= " GROUP BY mr.id_mail LIMIT 1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	private function NotifyEnd($id_mail)
	{
		$row = $this->MailJobGet($id_mail);
		if($row['id_user']>0)
		{
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User();
			$u->id = $row['id_user'];
			$user = $u->UserGet();
			include_once(SERVER_ROOT."/../classes/translator.php");
			$tr = new Translator($user['id_language'],0);
			$message = $tr->TranslateParams("mailjob_notify",array($row['subject'],$row['how_many']));
			$this->mail->SendMail($user['email'],$user['name'],"Mail confirmation",$message,array());
		}
	}

	public function ProfilingGet()
	{
		return $this->session->Get($this->profiling_varname);
	}

	public function RecipientDelete($delete)
	{
		$recipients = $this->RecipientsGet();
		unset($recipients[$delete]);
		$this->session->Set($this->recipient_varname,$recipients);
	}

	public function RecipientHistory($id_p,$id_topic)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT mj.id_mail,mj.subject,UNIX_TIMESTAMP(mj.send_date) AS send_date_ts,mr.status 
			FROM mailjob_recipients mr 
			INNER JOIN sendmail mj ON mr.id_mail=mj.id_mail
			WHERE mr.id_p=$id_p ";
		if($id_topic>0)
			$sqlstr .= " AND mj.id_topic=$id_topic";
		$sqlstr .= " ORDER BY mj.send_date DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function RecipientsToSend($id_mail)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_p) as sent
			FROM sendmail s
			LEFT JOIN mailjob_recipients mr ON s.id_mail=mr.id_mail
			WHERE s.id_mail='$id_mail' AND mr.status=0
			GROUP BY s.id_mail  ";
		$db->query_single($row, $sqlstr);
		return (int)$row['sent'];
	}
	
	public function RecipientRead($identifier)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT mr.id_mail,mj.track,mj.track_redirect,mr.id_p 
			FROM mailjob_recipients mr 
			INNER JOIN sendmail mj ON mr.id_mail=mj.id_mail 
			WHERE mr.identifier='$identifier' ";
		$db->query_single($row, $sqlstr);
		if($row['track'])
		{
			$db->begin();
			$db->lock( "mailjob_recipients" );
			$res[] = $db->query( "UPDATE mailjob_recipients SET status=2 WHERE identifier='$identifier'  " );
			Db::finish( $res, $db);
		}
		if($row['id_p']>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$pe->SetUserCookieAndSession($row['id_p']);
		}
		return $row;
	}
	
	public function RecipientUnsubscribe($identifier)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT mr.id_mail,mj.track,mj.track_redirect,mr.id_p,mr.email,mj.id_topic,mr.status
			FROM mailjob_recipients mr 
			INNER JOIN sendmail mj ON mr.id_mail=mj.id_mail WHERE mr.identifier='$identifier' ";
		$db->query_single($row, $sqlstr);
		$status = (int)$row['status'];
		if($status!=3) {
		    $db->begin();
		    $db->lock( "mailjob_recipients" );
		    $res[] = $db->query( "UPDATE mailjob_recipients SET status=3 WHERE identifier='$identifier'  " );
		    Db::finish( $res, $db);
		}
		$id_mail = (int)$row['id_mail'];
		$id_p = (int)$row['id_p'];
		$row['return'] = false;
		if($id_mail>0 && $id_p>0)
		{
			$job = $this->MailJobGet($id_mail);
			$row['return'] = $status!=3;
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			switch($job['id_module'])
			{
				case "4":
					$pe->TopicAssociate($id_p,$job['id_item'],0);
				break;
				case "28":
					$pe->ContactUpdatePortal($id_p, 0);
				break;
			}
		}
		return $row;
	}

	public function RecipientsSet($sqlstr,$profiling,$email_already_checked=false,$reset=true)
	{
		$rows = array();
		$recipients = $reset? array() : $this->RecipientsGet();
		if(strlen(($sqlstr))>0)
		{
			$db =& Db::globaldb();
			$db->QueryExe($rows,$sqlstr);
			foreach($rows as $row)
			{
				if($email_already_checked || ($row['mj_id']>0 && $this->mail->CheckEmail($row['mj_email'])))
				{
					$recipient = array();
					$recipient['id'] = $row['mj_id'];
					$recipient['name'] = $row['mj_name'];
					$recipient['email'] = $row['mj_email'];
					if($profiling)
						$recipient['password'] = $row['mj_password'];
					$recipient['id_geo'] = isset($row['mj_geo'])? $row['mj_geo'] : 0;
					$recipients[ $row['mj_id'] ] = $recipient;
				}
			}
		}
		$this->session->Set($this->recipient_varname,$recipients);
		$this->session->Set($this->profiling_varname,$profiling);
		return sizeof($recipients);
	}
	
	public function RecipientsDelete()
	{
		$this->session->Delete($this->profiling_varname);
		return $this->session->Delete($this->recipient_varname);
	}

	public function RecipientsGet()
	{
		$recipients = array();
		$session_recipients = $this->session->Get($this->recipient_varname);
		return (is_array($session_recipients))? $session_recipients : $recipients;
	}
	
	private function RecipientsLot( $id_mail, $lot )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT name,email,id_p,password,identifier,id_geo 
			FROM mailjob_recipients 
			WHERE id_mail=$id_mail AND lot=$lot AND status=0";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function RecipientsStatus( &$rows, $id_mail, $status, $id_topic=0 )
	{
		switch($status)
		{
			case "1":
				$sqlstr = "SELECT name,email,id_p 
					FROM mailjob_recipients 
					WHERE id_mail=$id_mail AND status>0";
			break;
			case "2":
			case "3":
				$sqlstr = "SELECT mr.name,mr.email,mr.id_p
					FROM mailjob_recipients mr
					WHERE mr.id_mail=$id_mail AND mr.status=$status  
					GROUP BY mr.id_p ";
			break;
		}
		$db =& Db::globaldb();
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	private function SendActualMailjob($id_mail,$recipients,$id_article,$is_html,$format,$subject,$body,$alt_text,$auto_footer,$footer,$from_name,$from_email,$embed_images)
	{
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		$th = new TextHelper();
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$attachments = array();
		if($id_article>0)
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article($id_article);
			include_once(SERVER_ROOT."/../classes/irl.php");
			$irl = new IRL();
			if($embed_images)
			{
				$images = $a->ImageGetAll();
				foreach($images as $image)
				{
					$image['id'] = $image['id_image'];
					$filename = $irl->PathAbs("image",$image);
					$attachment = array();
					$attachment['id'] = "i" . $image['id_image'];
					$attachment['ext'] = $image['format'];
					$attachment['type'] = $fm->ContentType($image['format']);
					$attachment['enc'] = $fm->Base64Encode($filename);
					$attachment['filename'] = $fm->RealPath($filename);
					$attachments[] = $attachment;
				}
			}
			$docs = $a->DocGetAll();
			foreach($docs as $doc)
			{
				$doc['id'] = $doc['id_doc'];
				$filename = $irl->PathAbs("article_doc",$doc);
				$attachment = array();
				$attachment['id'] = "d" . $doc['id_doc'];
				$attachment['ext'] = $doc['format'];
				$attachment['type'] = $fm->ContentType($doc['format']);
				$attachment['enc'] = $fm->Base64Encode($filename);
				$attachment['filename'] = $fm->RealPath($filename);
				$attachments[] = $attachment;
			}
		}
		if($format!="")
		{
			$filename = $this->Filename($id_mail,$format);
			$attachment = array();
			$attachment['id'] = $id_mail;
			$attachment['ext'] = $format;
			$attachment['type'] = $fm->ContentType($format);
			$attachment['enc'] = $fm->Base64Encode($filename);
			$attachment['filename'] = $fm->RealPath($filename);
			$attachments[] = $attachment;
		}
		foreach($recipients as $recipient)
		{
			$recipient_message = $this->TagsReplace($body,$recipient);
			$recipient_alt_text = $this->TagsReplace($alt_text,$recipient);
			if($auto_footer)
			{
				$recipient_footer = $this->TagsReplace($footer,$recipient);
				if($is_html)
				{
					$recipient_alt_text .= "\n\n" . str_replace("\\n","\n",$recipient_footer);
					$recipient_footer = str_replace("\\n","<br>",$recipient_footer);
					$recipient_footer = $th->Htmlise($recipient_footer,false);
					$recipient_message = "<html><body>$recipient_message<p style=\"font-size:0.9em;font-style:italic;margin:10px 0;border-top:1px dashed #444444;padding-top:5px;\">$recipient_footer</p></body></html>";
				}
				else 
				{
					$recipient_footer = "\n\n" . str_replace("\\n","\n",$recipient_footer);
					$recipient_message = $recipient_message . $recipient_footer;
				}				
			}
			elseif($is_html)
			{
				$recipient_message = "<html><body>$recipient_message</body></html>";
			}
			$extra = array();
			$extra['name'] = $from_name;
			$extra['email'] = $from_email;
			$extra['force'] = true;
			$this->mail->SendMail($recipient['email'],$recipient['name'],$subject,$recipient_message,$extra,$is_html || $id_article>0,$attachments,true,$recipient_alt_text);
		}
	}
	
	public function SendNextLot()
	{
		$lot = $this->NextLot();
		if($lot['id_mail']>0)
		{
			$job = $this->MailJobGet($lot['id_mail']);
			$recipients = $this->RecipientsLot($lot['id_mail'],$lot['lot']);
			$this->SendActualMailjob($lot['id_mail'],$recipients,$job['id_article'],$job['is_html'],$job['format'],$job['subject'],$job['body'],$job['alt_text'],$job['auto_footer'],$job['footer'],$job['from_name'],$job['from_email'],$job['embed_images']);
			$this->LotUpdate($lot['id_mail'],$lot['lot'],$job['track']=="1");
		}
	}
	
	private function TagsReplace($content,$recipient)
	{
		$content = str_replace("[[NAME]]",$recipient['name'],$content);
		$content = str_replace("[[EMAIL]]",$recipient['email'],$content);
		$content = str_replace("[[PASSWORD]]",$recipient['password'],$content);
		$content = str_replace("[[ID_GEO]]",$recipient['id_geo'],$content);
		$content = str_replace("[[TRACK]]",$this->mail->pub_web . "/tools/mj.php?mjt=" . $recipient['identifier'],$content);
		$content = str_replace("[[UNSUBSCRIBE]]",$this->mail->pub_web . "/tools/unsub.php?mjt=" . $recipient['identifier'],$content);
		return $content;
	}
	
	public function Test($id_topic,$is_html,$id_article,$from_name,$from_email,$subject,$body,$alt_text,$auto_footer,$send_password,$embed_images)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$user = $u->UserGet();
		$recipients = array();
		$recipients[] = array('name'=>$user['name'],'email'=>$user['email'],'password'=>"xxxxxxxx",'identifier'=>"RECIPIENT_ID");
		$footer = $auto_footer>0? $this->MessageFooter($id_topic,$send_password,$is_html,$auto_footer=2) : "";
		if($id_article>0)
		{
			$is_html = 1;
			$body = $this->MailjobArticleContent($id_article,$body,$embed_images=="1");
		}
		$this->SendActualMailjob(0,$recipients,$id_article,$is_html,"",$subject,$body,$alt_text,$auto_footer,$footer,$from_name,$from_email,$embed_images);
	}

	private function MailjobArticleContent($id_article,$body,$embed_images)
	{
		// Encode article
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$article = $a->ArticleGet();
		$body = $article['content'];
		$images = $a->ImageGetAll();
		if(!$embed_images)
		{
			include_once(SERVER_ROOT."/../classes/irl.php");
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($article['id_topic']);
			$irl = new IRL();
		}
		foreach($images as $image)
		{
			if($embed_images)
			{
				$image_html = "<img src=\"cid:atti{$image['id_image']}.{$image['format']}\" width=\"{$image['width']}\" height=\"{$image['height']}\" alt=\"{$image['caption']}\"";
			}
			else
			{
				$params = array('id_article'=>$id_article,'id'=>$image['id_image'],'size'=>$image['size'],'format'=>$image['format']);
				$image_url = $irl->PublicUrlTopic("image",$params,$t);
				$image_html = "<img src=\"{$image_url}\" width=\"{$image['width']}\" height=\"{$image['height']}\" alt=\"{$image['caption']}\"";
			}
			if($image['align']=="0")
				$image_html .= " align=\"right\"";
			if($image['align']=="1")
				$image_html .= " align=\"left\"";
			$image_html .= ">";
			$body = str_replace("[[Img{$image['id_image']}]]",$image_html,$body);
		}
		return $body;
	}
	
	public function Store($time,$from_name,$from_email,$id_user,$subject,$body,$id_module,$id_item,$recipients,$unescaped_footer,$send_password,$is_html=0,$file=array(),$track=false,$track_redirect='',$id_topic=0,$id_article=0,$auto_footer=1,$alt_text="",$embed_images=1)
	{
		$how_many = sizeof($recipients);
		if($how_many>0)
		{
			$db =& Db::globaldb();
			if($id_article>0)
			{
				$is_html = 1;
				$unescaped_body = $this->MailjobArticleContent($id_article,$body,$embed_images=="1");
				$body = $db->SqlQuote($unescaped_body);
			}
			$format = $file['ok']? $file['ext'] : "";
			$footer = $db->SqlQuote($unescaped_footer);
			$db->begin();
			$db->lock( "sendmail" );
			$id_mail = $db->nextId( "sendmail", "id_mail" );
			$insert = "INSERT INTO sendmail (id_mail,send_date,id_user,sent,subject,body,how_many,from_name,from_email,id_module,id_item,footer,send_password,is_html,format,track,track_redirect,id_topic,id_article,auto_footer,alt_text,embed_images)
				VALUES ($id_mail,'$time','$id_user',0,'$subject','$body','$how_many','$from_name','$from_email','$id_module','$id_item','$footer','$send_password','$is_html','$format','$track','$track_redirect','$id_topic','$id_article','$auto_footer','$alt_text','$embed_images')";
			$res[] = $db->query( $insert );
			Db::finish( $res, $db);
			if ($file['ok'])
			{
				include_once(SERVER_ROOT."/../classes/file.php");
				$fm = new FileManager;
				$fm->MoveUpload($file['temp'], $this->Filename($id_mail,$file['ext']));
				$fm->PostUpdate();
			}
			$db->begin();
			$db->lock( "mailjob_recipients" );
			$lot = 1;
			$lot_counter = 0;
			if($send_password)
			{
				include_once(SERVER_ROOT."/../classes/crypt.php");
				$sc = new Scrypt;
			}
			include_once(SERVER_ROOT."/../classes/db.php");
			$v = new Varia();
			foreach($recipients as $recipient)
			{
				$id = $recipient['id'];
				$name = $db->SqlQuote($recipient['name']);
				$email = $db->SqlQuote($recipient['email']);
				$id_geo = $recipient['id_geo'];
				$identifier = $v->Uid();
				$password = ($send_password)? $db->SqlQuote($sc->decrypt($recipient['password'])) : "";
				$insert = "INSERT INTO mailjob_recipients (id_mail,id_p,lot,name,email,password,status,identifier,id_geo) 
					VALUES ($id_mail,'$id','$lot','$name','$email','$password',0,'$identifier','$id_geo') ";
				$res[] = $db->query( $insert );
				$lot_counter ++;
				if($lot_counter==$this->mailjob_limit)
				{
					$lot ++;
					$lot_counter = 0;
				}
			}
			Db::finish( $res, $db);
		}
		$this->RecipientsDelete();
	}

	private function UserProfile($id_topic,$page)
	{
		$user_path = $this->mail->pub_web . "/$page";
		if($id_topic>0)
			$user_path .= "?id_topic=$id_topic";
		return $user_path;
	}
	
}
?>
