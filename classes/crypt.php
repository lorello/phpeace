<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * Bi-directional encryption
 * Used for admin passwords
 *
 * @package PhPeace
 */
class Scrypt
{
	/**
	 * Encryption key
	 *
	 * @var string
	 */
	private $key;

	/**
	 * Initialize local variables
	 *
	 */
	function __construct()
	{
		$ini = new Ini;
		$password = $ini->Get('crypt_password');
		$this->GenerateKey($password);
	}

	/**
	 * Generate encryption key based on local installation's key
	 *
	 * @param string $password
	 * @param integer $level
	 */
	private function GenerateKey($password,$level=30)
	{
		for ($i=0; $i<$level; $i++) $mymd[$i]=md5(substr($password,($i%strlen($password)),1));
		for ($a=0; $a<32; $a++) for ($i=0; $i<$level; $i++) $key.=substr($mymd[$i],$a,1);
		$this->key=$key;
	}

	/**
	 * Encrypt text
	 *
	 * @param string $text
	 * @return string
	 */
	public function Encrypt($text)
	{
		$key = $this->key;
		for ($i=0; $i<strlen($key); $i=$i+2) $d[$i/2]=hexdec(substr($key,$i,2));
		for ($i=0, $ntext=""; substr($text, $i, 1) !=""; $i++) $ntext.=chr((ord(substr($text, $i, 1))+$d[($i%strlen($key))])%255);
		$ntext=base64_encode($ntext);
		return ($ntext);
	}

	/**
	 * Decrypt text
	 *
	 * @param string $text
	 * @return string
	 */
	public function Decrypt($text)
	{
		$key = $this->key;
		$key=str_replace("3", "j", str_replace("2", "i", str_replace("1", "h", str_replace("0", "g", $key))));
		$key=str_replace("7", "n", str_replace("6", "m", str_replace("5", "l", str_replace("4", "k", $key))));
		$key=str_replace("b", "4", str_replace("a", "5", str_replace("9", "6", str_replace("8", "7", $key))));
		$key=str_replace("f", "0", str_replace("e", "1", str_replace("d", "2", str_replace("c", "3", $key))));
		$key=str_replace("j", "c", str_replace("i", "d", str_replace("h", "e", str_replace("g", "f", $key))));
		$key=str_replace("n", "8", str_replace("m", "9", str_replace("l", "a", str_replace("k", "b", $key))));
		$text=base64_decode($text);
		for ($i=0; $i<strlen($key); $i=$i+2) $d[$i/2]=hexdec(substr($key,$i,2));
		for ($i=0, $ntext=""; substr($text, $i, 1) !=""; $i++) $ntext.=chr((ord(substr($text, $i, 1))+$d[($i%strlen($key))])%255);
		return ($ntext);
	}

}

?>
