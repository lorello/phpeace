<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/history.php");

class Image
{
	public $id;

	public $path;
	/** 
	 * @var History */
	public $h;

	private $img_sizes;
	
	private $isCDN;

	function __construct( $id )
	{
		$this->id = $id;
		$this->path = "uploads/images";
		$this->h = new History();
		$conf = new Configuration();
		$this->img_sizes = $conf->Get("img_sizes");
		$this->isCDN = $conf->Get("cdn") != '';
	}
	
	public function AdminRight()
	{
		$right = false;
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$creator = $this->CreatorId();
		if($session->Get("current_user_id") == $creator['id_user'] )
			$right = true;
		else
		{
			include_once(SERVER_ROOT."/../classes/modules.php");
			if (Modules::AmIAdmin(4) || Modules::AmIAdmin(14) || Modules::AmIAdmin(7))
				$right = true;
		}
		return $right;
	}

	public function Articles($exclude_id_article=0)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,t.name AS topic_name,a.id_topic 
			FROM articles a
			INNER JOIN images_articles ia ON a.id_article=ia.id_article
			LEFT JOIN topics t ON a.id_topic=t.id_topic 
			WHERE a.id_topic<>'" . $ini->Get('temp_id_topic') . "' AND ia.id_image='$this->id'";
		if($exclude_id_article>0)
			$sqlstr .= " AND ia.id_article<>'$exclude_id_article' ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ArticleAdd($id_image,$id_article,$caption,$align,$size,$zoom,$is_flash=false,$associate=true)
	{
		$sizes = $this->ImageSize($size,$id_image);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images_articles" );
		$res[] = $db->query( "INSERT INTO images_articles (id_image,id_article,caption,align,size,zoom,width,height)
					VALUES ($id_image,$id_article,'$caption',$align,'$size','$zoom','{$sizes['width']}','{$sizes['height']}')" );
		Db::finish( $res, $db);
		if($associate)
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article($id_article);
			$a->ArticleLoad();
			$images = $a->ImageGetAll();
			if(count($images)==1)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($a->id_topic);
				$a->ImageAssociate($id_image,$t->row['associated_image_size'],1,true);
			}
		}
	}

	public function ArticleDelete($id_image,$id_article)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$a->ArticleLoad();
		if($a->id_topic > 0)
		{
			include_once(SERVER_ROOT."/../classes/irl.php");
			$irl = new IRL();
			include_once(SERVER_ROOT."/../classes/topic.php");
			$irl->topic = new Topic($a->id_topic);
			$image = $a->ImageGet($id_image);
			$filename = $irl->PublicPath("image",array('id'=>$id_image,'size'=>$image['size'],'id_article'=>$id_article,'format'=>$image['format']),FALSE,TRUE);
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$fm->Delete($filename);
			if($image['zoom'])
			{
				$filename2 = $irl->PublicPath("image_orig",array('id'=>$id_image,'format'=>$image['format']),FALSE,TRUE);
				$fm->Delete($filename2);
				$zoom_html = $irl->PublicPath("image_zoom",array('id'=>$id_image,'id_article'=>$id_article),FALSE,TRUE);
			}
			$fm->PostUpdate();
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images_articles" );
		$res[] = $db->query( "DELETE FROM images_articles WHERE id_image=$id_image AND id_article=$id_article" );
		Db::finish( $res, $db);
		$a->ImageRemove($id_image);
		$this->ArticleUpdateSet($id_article);
	}

	public function ArticleRemove($id_image,$id_article)
	{
		if ($id_article > 0)
			$this->ArticleDelete($id_image,$id_article);
		$this->Remove($id_image);
	}

	public function ArticleUpdate($id_image,$id_article,$caption,$align,$size,$zoom)
	{
		$sizes = $this->ImageSize($size,$id_image);
		$db =& Db::globaldb();
		$db->lock( "images_articles" );
		$res[] = $db->query( "UPDATE images_articles SET caption='$caption',align='$align',size='$size',zoom='$zoom',
				width='{$sizes['width']}',height='{$sizes['height']}' WHERE id_image='$id_image' AND id_article='$id_article' " );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet($id_article);
	}

	private function ArticleUpdateSet($id_article)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$a->ArticleLoad();
		if ($a->visible && ($a->id_topic)>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($a->id_topic);
			$t->queue->JobInsert($t->queue->types['article'],$id_article,"update");
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd($t->queue->types['article'],$id_article,$t->id,$t->id_group,1);
		}
	}

	public function ArticleUpdateSuptopic($id_image,$id_article)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$a->ArticleLoad();
		if ($a->id_topic>0)
		{
			$t = new Topic($a->id_topic);
			if($a->visible)
			{
				$id_subtopic= $t->SubtopicArticle($id_article);
				if($id_subtopic>0)
					$t->queue->JobInsert($t->queue->types['subtopic'],$id_subtopic,"update");
			}
		}
		$articles = $this->Articles($id_article);
		foreach($articles as $article)
		{
			$a = new Article($article['id_article']);
			$a->ArticleLoad();
			if($a->visible)
			{
				$t = new Topic($article['id_topic']);
				$t->queue->JobInsert($t->queue->types['article'],$article['id_article'],"update");
				$id_subtopic= $t->SubtopicArticle($article['id_article']);
				if($id_subtopic>0)
					$t->queue->JobInsert($t->queue->types['subtopic'],$id_subtopic,"update");
			}
		}
		$subtopics = $this->SubtopicsImage($id_image);
		if(count($subtopics)>0)
		{
			foreach($subtopics as $subtopic)
			{
				$t = new Topic($subtopic['id_topic']);
				if($t->SubtopicPublishable($subtopic['id_type']) && $subtopic['visible']!="4")
				{
					$t->queue->JobInsert($t->queue->types['subtopic'],$subtopic['id_subtopic'],"update");
					$t->queue->JobInsert($t->queue->types['subtopic'],$subtopic['id_parent'],"update");
				}
				$t->SubtopicImageSizeSet($subtopic['id_subtopic']);
			}
		}
	}
	
	private function SubtopicsImage($id_image)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_subtopic,name,id_parent,id_type,visible,id_topic FROM subtopics WHERE id_image='$id_image'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Convert($origfile, $id_image)
	{
		$i = new Images();
		$i->ConvertWrapper("image",$origfile,$id_image);
		$i->CDNReset('images',$id_image,$i->convert_format);
	}

	public function CreatorId()
	{
		return $this->h->CreatorId($this->h->types['image'],$this->id);
	}

	public function Galleries($id_gallery)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_gallery,g.title 
			FROM galleries g
			INNER JOIN images_galleries_import igi ON g.id_gallery=igi.id_gallery
			WHERE igi.id_image='$this->id' AND g.id_gallery<>'$id_gallery'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Gallery()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_gallery FROM images_galleries WHERE id_image=$this->id");
		return $row;
	}

	public function GalleryDelete($id_image,$id_gallery)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images_galleries" );
		$res[] = $db->query( "DELETE FROM images_galleries WHERE id_image=$id_image AND id_gallery=$id_gallery" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "images_galleries_import" );
		$res[] = $db->query( "DELETE FROM images_galleries_import WHERE id_image=$id_image AND id_gallery=$id_gallery" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$g->RemoveImage($id_image);
		$this->TopicQueueUpdate($id_gallery);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove(7,$id_image);
	}
	
	public function GalleryImport($id_gallery,$link,$caption,$id_gallery_from)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_image,caption FROM images_galleries_import WHERE id_image='$this->id' AND id_gallery='$id_gallery' ");
		if($row['id_image']>0)
		{
			$this->GalleryUpdate($id_gallery,$link);
			$sqlstr =  "UPDATE images_galleries_import SET caption='$caption' WHERE id_image='$this->id' AND id_gallery='$id_gallery' ";
		}
		else 
		{
			$this->GalleryInsert($id_gallery,$link,$id_gallery_from);
			$sqlstr = "INSERT INTO images_galleries_import (id_image,id_gallery,caption) VALUES ('$this->id','$id_gallery','$caption' )" ;
		}
		$db->begin();
		$db->lock( "images_galleries_import" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function GalleryInsert( $id_gallery, $link, $id_gallery_from=0,$prevnext=true )
	{
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$sizes1 = $this->ImageSize($g->thumb_size,$this->id);
		$sizes2 = $this->ImageSize($g->image_size,$this->id);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images_galleries" );
		$res[] = $db->query( "INSERT INTO images_galleries (id_image,id_gallery,link,thumb_height,image_height,id_gallery_from)
			VALUES ('$this->id','$id_gallery','$link','{$sizes1['height']}','{$sizes2['height']}','$id_gallery_from' )" );
		Db::finish( $res, $db);
		if($g->visible)
		{
			$g->PublishImage($this->id);
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd(7,$this->id,0,0,1);
		}
		$this->TopicQueueUpdate($id_gallery);
		if($prevnext)
			$g->PrevNext();
	}

	public function GalleryRemove($id_image,$id_gallery)
	{
		$this->GalleryDelete($id_image,$id_gallery);
		$this->Remove($id_image);
		$this->TopicQueueUpdate($id_gallery);
	}

	public function GalleryUpdate($id_gallery,$link)
	{
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$sizes1 = $this->ImageSize($g->thumb_size,$this->id);
		$sizes2 = $this->ImageSize($g->image_size,$this->id);
		$sqlstr = "UPDATE images_galleries
			 SET link='$link',thumb_height='{$sizes1['height']}',image_height='{$sizes2['height']}'
			  WHERE id_image='$this->id' AND id_gallery='$id_gallery' ";
		$db =& Db::globaldb();
		$db->lock( "images_galleries" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->TopicQueueUpdate($id_gallery);
		if($g->visible)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd(7,$this->id,0,0,1);
		}
	}

	public function Get()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_image,source,author,id_licence,caption,format,width,height,
			image_date FROM images WHERE id_image='$this->id' ");
		return $row;
	}

	public function ImageClone()
	{
		$image = $this->Get();
		$db =& Db::globaldb();
		$source = $db->SqlQuote($image['source']);
		$author = $db->SqlQuote($image['author']);
		$id_licence = $image['id_licence'];
		$format = $db->SqlQuote($image['format']);
		$caption = $db->SqlQuote($image['caption']);
		$image_date = $db->SqlQuote($image['image_date']);
		$width = $image['width'];
		$height = $image['height'];
		$db->begin();
		$db->lock( "images" );
		$id_image = $db->nextId( "images", "id_image" );
		$res[] = $db->query( "INSERT INTO images (id_image,source,author,id_licence,format,caption,image_date,width,height) 
			VALUES ($id_image,'$source','$author','$id_licence','$format','$caption','$image_date','$width','$height')" );
		Db::finish( $res, $db);
		$i = new Images();
		$fm = new FileManager();
		$filename_orig = "$this->path/orig/{$this->id}.{$image['format']}";
		$filename_copy = "$this->path/orig/{$id_image}.{$image['format']}";
		$fm->HardCopy($filename_orig,$filename_copy);
		$sizes = count($this->img_sizes) - 1;
		for($j=0;$j<=$sizes;$j++)
		{
			$filename_orig = "$this->path/$j/{$this->id}.{$i->convert_format}";
			$filename_copy = "$this->path/$j/{$id_image}.{$i->convert_format}";
			$fm->HardCopy($filename_orig,$filename_copy);
		}
		$fm->PostUpdate();
		return $id_image;
	}
	
	public function ImageInsert( $image_date, $source, $author, $id_licence, $caption, $keywords, $file, $keywords_internal=array(), $image_filename="")
	{
		$fm = new FileManager();
		$file_ext = ($image_filename!="")? $fm->Extension($image_filename) : $file['ext'];
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images" );
		$this->id = $db->nextId( "images", "id_image" );
		$res[] = $db->query( "INSERT INTO images (id_image,source,author,id_licence,format,caption,image_date) 
			VALUES ($this->id,'$source','$author','$id_licence','$file_ext','$caption','$image_date')" );
		Db::finish( $res, $db);
		$filename = "$this->path/orig/" . $this->id . "." . $file_ext;
		if($image_filename!="")
			$fm->Rename($image_filename,$filename);
		else
			$fm->MoveUpload($file['temp'], $filename);
		$this->ImageSizeSet();
		$this->Convert($filename, $this->id);
		$this->Keywords($this->id,$keywords,$keywords_internal);
		$this->h->HistoryAdd($this->h->types['image'],$this->id,$this->h->actions['create']);
	}

	public function ImageSize($size,$id_image,$format="default")
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($this->isCDN) {
			$img = $this->Get();
			$format = $img['format'];
			$filename = "$this->path/orig/{$id_image}.{$format}";
			$image_size = $fm->ImageSize($filename);
			$width = $size=='orig'? $image_size['width'] : $this->img_sizes[$size];
			$image_size['height'] = floor($image_size['height'] * ($width) / $image_size['width']);
			$image_size['width'] = $width;
		} else {
			if($format=="default")
			{
				$i = new Images();
				$format = $i->convert_format;
			}
			if($size<0)
			{
				$img = $this->Get();
				$format = $img['format'];
			}
			$filename = "$this->path/" . (($size<0)? "orig":$size)  .  "/" . $id_image . "." . $format;
			$image_size = $fm->ImageSize($filename);
			if(is_numeric($size) && $size>=0 && $image_size['mime']=="application/x-shockwave-flash" && $image_size['width']>0 && $image_size['height']>0)
			{
				$img_sizes = $this->img_sizes;
				$image_size['height'] = floor($image_size['height'] * ($this->img_sizes[$size]) / $image_size['width']);
				$image_size['width'] = $this->img_sizes[$size];
			}
		}
		$image_size['format'] = $format;
		return $image_size;
	}
	
	public function ImageSizeSet()
	{
		$img = $this->Get();
		$sizes = $this->ImageSize("orig",$this->id,$img['format']);
		if($sizes['width']>0 && $sizes['height']>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "images" );
			$sqlstr = "UPDATE images SET width='{$sizes['width']}',height='{$sizes['height']}' WHERE id_image='$this->id' ";
			$res[] = $db->query($sqlstr);
			Db::finish( $res, $db);
		}
	}
	
	public function ImageUpdate($id_image,$image_date,$source,$author,$id_licence,$caption,$keywords,$file,$keywords_internal=array())
	{
		if ($file['ok'])
		{
			$this->RemoveOriginal($id_image);
			$sqlstr = "UPDATE images SET caption='$caption',source='$source',author='$author',id_licence='$id_licence',
			caption='$caption',image_date='$image_date',format='{$file['ext']}' WHERE id_image='$id_image' ";
		}
		else 
		{
			$sqlstr = "UPDATE images SET caption='$caption',source='$source',author='$author',id_licence='$id_licence',
			caption='$caption',image_date='$image_date' WHERE id_image='$id_image' ";
		}
		$db =& Db::globaldb();
		$db->lock( "images" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($file['ok'])
		{
			$fm = new FileManager;
			$filename = "$this->path/orig/" . $this->id . "." . $file['ext'];
			$fm->MoveUpload($file['temp'], $filename);
			$this->ImageSizeSet();
			$this->Convert($filename, $this->id);
		}
		$this->Keywords($id_image,$keywords,$keywords_internal);
		$this->h->HistoryAdd($this->h->types['image'],$id_image,$this->h->actions['update']);
	}

	public function IsInGalleries()
	{
		$isingalleries = false;
		$image = $this->Gallery();
		if ($image['id_gallery']>0)
			$isingalleries = true;
		return $isingalleries;
	}

	public function Keywords($id_image,$keywords,$keywords_internal)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_image, $o->types['image']);
		$o->InsertKeywordsArray($keywords_internal,$id_image,$o->types['image']);
	}

	private function RemoveOriginal($id_image)
	{
		$image = array();
		$db =& Db::globaldb();
		$db->query_single($image, "SELECT format FROM images WHERE id_image='$id_image' ");
		$filename = "$this->path/orig/{$id_image}.{$image['format']}";
		$fm = new FileManager();
		$fm->Delete($filename);
		$i = new Images();
		$i->CDNReset('images', $id_image, $image['format'], true);
	}
	
	public function Remove($id_image,$track=true)
	{
		$this->RemoveOriginal($id_image);
		$i = new Images();
		$i->RemoveWrapper("image",$id_image);
		$i->CDNReset('images', $id_image, $i->convert_format);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images" );
		$res[] = $db->query( "DELETE FROM images WHERE id_image='$id_image' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_image,$o->types['image']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['image'],$id_image);
		if($track)
			$this->h->HistoryAdd($this->h->types['image'],$id_image,$this->h->actions['delete']);
	}

	public function Subtopics()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT s.id_subtopic,s.name,t.name AS topic_name,s.id_topic 
			FROM subtopics s
			LEFT JOIN topics t ON s.id_topic=t.id_topic 
			WHERE s.id_topic<>'" . $ini->Get('temp_id_topic') . "' AND s.id_image='$this->id'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	private function TopicQueueUpdate($id_gallery)
	{
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$topics = $g->Topics();
		if (count($topics)>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			foreach ($topics as $topic)
			{
				$t = new Topic($topic['id_topic']);
				$t->queue->JobInsert($t->queue->types['subtopic'],$topic['id_subtopic'],"update");
			}
		}
		if($g->visible)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t0 = new Topic(0);
			$t0->queue->JobInsert($t0->queue->types['gallery'],$id_gallery,"update_xml");
		}
	}

}
?>
