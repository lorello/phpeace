<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/irl.php");
include_once(SERVER_ROOT."/../classes/file.php");
define('SITEMAP_FILE',"sitemap.xml");
define('SITEMAP_INDEX',"sitemap_index.xml");

class Sitemap
{
	/** 
	 * @var IRL */
	private $irl;
	
	/**
	 * @var Ini */
	private $ini;
		
	/** 
	 * @var Topic */
	private $topic;
	
	private $current_year;
	private $last_mod;
	
	private $sitemap_keyword_id;
	
	private $priorities = array();
	private $frequencies = array();
	
	function __construct()
	{
		$this->irl = new IRL();
		$this->ini = new Ini();
		$today = getdate();
		$this->current_year = $today['year'];
		$this->priorities['homepage'] = "0.9000";
		$this->priorities['topic_home'] = "0.8000";
		$this->priorities['subtopic'] = "0.6000";
		$this->priorities['article'] = "0.5000";
		$this->frequencies['homepage'] = "always";
		$this->frequencies['topic_home'] = "daily";
		$this->frequencies['subtopic'] = "weekly";
		$this->frequencies['article'] = "weekly";
		$conf = new Configuration();
		$this->sitemap_keyword_id = $conf->Get("sitemap_keyword_id");
	}
	
	private function ArticlesYear($year)
	{
		$this->last_mod = 0;
		$urls = array();
		$db =& Db::globaldb();
		$articles = array();
		$sqlstr = "SELECT a.id_article,a.id_topic,UNIX_TIMESTAMP(a.published) AS last_ts
			FROM articles a 
			INNER JOIN subtopics s ON a.id_topic=s.id_topic AND s.visible<3
			INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE a.approved=1 AND a.jump_to_source=0 AND YEAR(a.published)=$year 
			GROUP BY a.id_article
			ORDER BY a.published DESC";
		$db->QueryExe($articles, $sqlstr);
		include_once(SERVER_ROOT."/../classes/topic.php");
		foreach($articles as $article)
		{
			$this->TopicInit($article['id_topic']);
			$url = $this->Url("article",array('id'=>$article['id_article'],'id_topic'=>$this->topic->id ));
			$articlemap = array();
			$articlemap['url'] = $url;
			$articlemap['lastmod_ts'] = $article['last_ts'];
			if($article['last_ts']>($this->last_mod))
				$this->last_mod = $article['last_ts'];
			$urls[] = $articlemap;
		}
		return $urls;
		}
	
	private function FixRobotsTxt()
	{
		$fm = new FileManager();
		$robots = $fm->TextFileRead("pub/robots.txt");
		if(strpos($robots,"Sitemap:")===false)
		{
			$robots = "Sitemap: " . $this->irl->pub_web . "/" . SITEMAP_INDEX . "\n" . $robots;
			$fm->WritePage("pub/robots.txt", $robots);
		}
	}

	private function FixRobotsTxtTopic()
	{
		$fm = new FileManager();
		$robots = $fm->TextFileRead("pub/{$this->topic->path}/robots.txt");
		if(strpos($robots,"Sitemap:")===false)
		{
			$robots = "Sitemap: " . $this->topic->domain . "/" . SITEMAP_FILE . "\n" . $robots;
			$fm->WritePage("pub/{$this->topic->path}/robots.txt", $robots);
		}
	}

	public function Generate()
	{
		//global
		$this->FixRobotsTxt();
		$index = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$index .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		$index .= "<sitemap>\n";
		$urls_global = array();
		$now = time();
		$home = array();
		$home['url'] = $this->irl->pub_web;
		$home['lastmod_ts'] = $now;
		$urls_global[] = $home;
		// topics groups
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$groups = $tt->gh->GroupsAll(true);
		foreach($groups as $group) {
			$urls_global[] = array('url'=>($this->irl->pub_web) . '/' . $this->ini->Get("map_path") . "/group_{$group['id_group']}.html",'lastmod_ts'=>$now);
		}
		// calendar
		$urls_global[] = array('url'=>($this->irl->pub_web) . '/' . $this->ini->Get("events_path"),'lastmod_ts'=>$now);
		// $urls_global[] = array('url'=>());
		$url_global = $this->WriteMap($urls_global,$this->priorities['homepage'],"always","global");
		$index .= "<loc>{$url_global}</loc>\n";
		$index .= "<lastmod>" . $this->DateISO8601($now) . "</lastmod>\n";
		$index .= "</sitemap>\n";
		// news
		if($this->sitemap_keyword_id > 0) {
			$urls_news = array();
			$news = $tt->LatestNews($this->sitemap_keyword_id);
			foreach($news as $new) {
				$newsmap = array();
				$this->TopicInit($new['id_topic']);
				$newsmap['url'] = $this->Url("article",array('id'=>$new['id_article'],'id_topic'=>$new['id_topic']));
				$newsmap['title'] = $new['headline'];
				$newsmap['published_ts'] = $new['published_ts'];
				$urls_news[] = $newsmap;
			}
			$url_news = $this->WriteMapNews($urls_news);
			$index .= "<sitemap>\n";
			$index .= "<loc>{$url_news}</loc>\n";
			$index .= "<lastmod>" . $this->DateISO8601($now) . "</lastmod>\n";
			$index .= "</sitemap>\n";
		}
		// events
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$urls_events = array();
		$events = $ee->EventsAll();
		foreach($events as $event)
		{
			$url = $this->Url("events",array('id'=>$event['id_event'],'subtype'=>'event'),true);
			$event_ts = $event['start_date_ts'] > $now ? $now : $event['start_date_ts'];
			$urls_events[] = array('type'=>'article','url'=>$url,'lastmod_ts'=>$event_ts);
		}
		$events_map = $this->WriteMap($urls_events,$this->priorities['article'],"always","events");
		$index .= "<sitemap>\n";
		$index .= "<loc>{$events_map}</loc>\n";
		$index .= "<lastmod>" . $this->DateISO8601($now) . "</lastmod>\n";
		$index .= "</sitemap>\n";
		// topics
		$topics = $tt->NoDomains();
		$fm = new FileManager();
		foreach($topics as $topic)
		{
			$urls_topic = array();
			$this->TopicInit($topic['id_topic']);
			$lastmod_ts = $fm->FileMTime("pub/{$this->topic->path}/index.html");
			$topic_home = array();
			$topic_home['type'] = "topic_home";
			$topic_home['url'] = $this->topic->url;
			$urls_topic[] = $topic_home;
			$subtopics = $this->topic->Subtopics();
			foreach($subtopics as $subtopic)
			{
				if($subtopic['visible']=="1" && $subtopic['id_type']!=$this->topic->subtopic_types['url'])
				{
					$url = $this->Url("subtopic",array('id'=>$subtopic['id_subtopic'],'id_type'=>$subtopic['id_type'],'id_item'=>$subtopic['id_item'],'id_subitem'=>$subtopic['id_subitem'],'id_subitem_id'=>$subtopic['id_subitem_id'],'id_topic'=>$this->topic->id ));
					$subtopicmap = array();
					$subtopicmap['type'] = "subtopic";
					$subtopicmap['url'] = $url;
					$urls_topic[] = $subtopicmap;
				}
			}
			$articles = $this->topic->Articles(true); 
			foreach($articles as $article)
			{
				$url = $this->Url("article",array('id'=>$article['id_article'],'id_topic'=>$this->topic->id ));
				$articlemap = array();
				$articlemap['type'] = "article";
				$articlemap['url'] = $url;
				$urls_topic[] = $articlemap;
			}
			$index .= "<sitemap>\n";
			$topic_map = $this->WriteMapTopic($urls_topic,$lastmod_ts);
			$index .= "<loc>{$topic_map}</loc>\n";
			$index .= "<lastmod>" . $this->DateISO8601($now) . "</lastmod>\n";
			$index .= "</sitemap>\n";
		}
		$index .= "</sitemapindex>\n";
		$fm->WritePage("pub/" . SITEMAP_INDEX,$index);
		$fm->PostUpdate();
		return $this->irl->pub_web . "/" . SITEMAP_INDEX;
	}
	
	private function Filename($type,$counter=0)
	{
		return "sitemap_{$type}.xml.gz";
	}
	
	private function Subtopics()
	{
		$this->last_mod = 0;
		$urls = array();
		$db =& Db::globaldb();
		$subtopics = array();
		$sqlstr = "SELECT DISTINCT a.id_topic,a.id_subtopic,UNIX_TIMESTAMP(MAX(a.published)) AS last_ts,s.id_type,s.id_item,s.id_subitem,s.id_subitem_id,s.link 
			FROM articles a 
			INNER JOIN subtopics s ON a.id_topic=s.id_topic AND s.visible=1
			INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE a.approved=1 AND s.id_type!=11 
			GROUP BY a.id_subtopic
			ORDER BY tg.seq,t.seq,s.seq";
		$db->QueryExe($subtopics, $sqlstr);
		include_once(SERVER_ROOT."/../classes/topic.php");
		foreach($subtopics as $subtopic)
		{
			$this->TopicInit($subtopic['id_topic']);
			$url = $this->Url("subtopic",array('id'=>$subtopic['id_subtopic'],'id_type'=>$subtopic['id_type'],'id_item'=>$subtopic['id_item'],'id_subitem'=>$subtopic['id_subitem'],'id_subitem_id'=>$subtopic['id_subitem_id'],'link'=>$subtopic['link'],'id_topic'=>$this->topic->id ));
			$subtopicmap = array();
			$subtopicmap['url'] = $url;
			$subtopicmap['lastmod_ts'] = $subtopic['last_ts'];
			if($subtopic['last_ts']>($this->last_mod))
				$this->last_mod = $subtopic['last_ts'];
			$urls[] = $subtopicmap;
		}
		return $urls;
	}
	
	private function Topics()
	{
		$this->last_mod = 0;
		$urls = array();
		$db =& Db::globaldb();
		$topics = array();
		$sqlstr = "SELECT DISTINCT a.id_topic,UNIX_TIMESTAMP(MAX(a.published)) AS last_ts 
			FROM articles a 
			INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE a.approved=1
			GROUP BY a.id_topic
			ORDER BY tg.seq,t.seq";
		$db->QueryExe($topics, $sqlstr);
		include_once(SERVER_ROOT."/../classes/topic.php");
		foreach($topics as $topic)
		{
			$this->TopicInit($topic['id_topic']);
			$url = $this->Url("topic_home",array('id_topic'=>$this->topic->id));
			$topicmap = array();
			$topicmap['url'] = $url;
			$topicmap['lastmod_ts'] = $topic['last_ts'];
			if($topic['last_ts']>($this->last_mod))
				$this->last_mod = $topic['last_ts'];
			$urls[] = $topicmap;
		}
		return $urls;
	}
	
	private function DateISO8601($timestamp)
	{
		return date('Y-m-d\TH:i:s',$timestamp) . substr_replace(date('O',$timestamp),':',3,0);
	}
	
	private function TopicInit($id_topic)
	{
		if (!is_object($this->topic) || $id_topic!=$this->topic->id)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->topic = new Topic($id_topic);
			$this->irl->topic = $this->topic;
		}
	}
	
	private function Url($type,$params=array(),$global=false)
	{
		return $global? $this->irl->PublicUrlGlobal($type, $params) : $this->irl->PublicUrlTopic($type,$params,$this->topic);
	}
	
	private function WriteMap($urls,$priority,$changefreq,$type)
	{
		$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$sitemap .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		foreach($urls as $url)
		{
			$sitemap .= "<url>\n";
			$sitemap .= "<loc>" . htmlspecialchars($url['url'],ENT_QUOTES) . "</loc>\n";
			$sitemap .= "<lastmod>" . $this->DateISO8601($url['lastmod_ts']) . "</lastmod>\n";
			$sitemap .= "<priority>{$priority}</priority>\n";
			$sitemap .= "<changefreq>{$changefreq}</changefreq>\n";
			$sitemap .= "</url>\n";
		}
		$sitemap .= "</urlset>\n";
		$filename = $this->Filename($type);
		$fm = new FileManager();
		$fm->WriteZip("pub/$filename",$sitemap);
		return $this->irl->pub_web . "/" . $filename;
	}

	private function WriteMapNews($urls)
	{
		$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$sitemap .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:news=\"http://www.google.com/schemas/sitemap-news/0.9\">\n";
		$website = htmlspecialchars($this->ini->Get("title"),ENT_QUOTES);
		$tr = new Translator(0,0);
		$lang = $tr->languages[$tr->id_language];
		foreach($urls as $url)
		{
			$sitemap .= "<url>\n";
			$sitemap .= "<loc>" . htmlspecialchars($url['url'],ENT_QUOTES) . "</loc>\n";
			$sitemap .= "<news:news>\n";
			$sitemap .= "<news:publication>$website</news:publication>\n";
			$sitemap .= "<news:language>$lang</news:language>\n";
			$sitemap .= "<news:title>" . htmlspecialchars($url['title'],ENT_QUOTES) . "</news:title>\n";
			$sitemap .= "<news:publication_date>" . $this->DateISO8601($url['published_ts']) . "</news:publication_date>\n";
			$sitemap .= "</news:news>\n";
			$sitemap .= "</url>\n";
		}
		$sitemap .= "</urlset>\n";
		$filename = $this->Filename('googlenews');
		$fm = new FileManager();
		$fm->WriteZip("pub/$filename",$sitemap);
		return $this->irl->pub_web . "/" . $filename;
	}
	
	private function WriteMapTopic($urls,$lastmod_ts)
	{
		$filename = "sitemap_{$this->topic->path}.xml.gz";
		$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$sitemap .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		foreach($urls as $url)
		{
			$priority = $this->priorities[$url['type']];
			$changefreq = $this->frequencies[$url['type']];
			$sitemap .= "<url>\n";
			$sitemap .= "<loc>" . htmlspecialchars($url['url'],ENT_QUOTES) . "</loc>\n";
			$sitemap .= "<lastmod>" . $this->DateISO8601($lastmod_ts) . "</lastmod>\n";
			$sitemap .= "<priority>{$priority}</priority>\n";
			$sitemap .= "<changefreq>{$changefreq}</changefreq>\n";
			$sitemap .= "</url>\n";
		}
		$sitemap .= "</urlset>\n";
		$fm = new FileManager();
		$fm->WriteZip("pub/$filename",$sitemap);
		if($this->topic->row['domain']!="")
		{
			$this->FixRobotsTxtTopic();
			$fm->WritePage("pub/{$this->topic->path}/sitemap.xml", $sitemap);
		}
		return $this->irl->pub_web . "/" . $filename;
	}
}
?>
