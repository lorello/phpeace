<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Calendar event management
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Event
{
	/** 
	 * @var History */
	private $h;
	
	/**
	 * @var TextHelper */
	private $th;
	
	public $errors;

	/**
	 * Initialize local properties
	 *
	 */
	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
		$this->errors = array();
	}

	/**
	 * The creator of a specific event
	 *
	 * @param integer $id	ID of event
	 * @return integer		ID of admin user
	 */
	public function CreatorId($id)
	{
		return $this->h->CreatorId($this->h->types['event'],$id);
	}
	
	/**
	 * When the event has been created
	 *
	 * @param integer $id	ID of event
	 * @return integer		Unix timestamp of creation
	 */
	public function CreatorTime($id)
	{
		return $this->h->CreatorDateTime($this->h->types['event'],$id);
	}

	/**
	 * Retrieve a specific event
	 *
	 * @param integer $id	ID of event
	 * @return array
	 */
	public function EventGet($id)
	{
		$row = array();
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		$gjoin = $ee->geo->GeoJoin("e.id_geo");
		$db =& Db::globaldb();
		$sqlstr = "SELECT e.title,e.approved,UNIX_TIMESTAMP(start_date) AS start_date_ts,
		length,place,place_details,id_geo,e.id_event_type,contact_name,email,link,phone,description,
		allday,e.id_article,e.id_topic,e.id_group,e.has_image,e.is_html,e.latitude,e.longitude,e.id_geo,
		UNIX_TIMESTAMP(end_date) AS end_date_ts,e.portal,et.type,e.facebook_id,e.address,
		$gjoin[name] AS geo_name,e.id_event,e.jump_to_article,e.id_p,e.image_ratio
		FROM events e
		INNER JOIN event_types et ON e.id_event_type=et.id_event_type $gjoin[join]
		WHERE id_event=$id";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function EventGetByFacebookId($facebook_id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_event,title,approved FROM events WHERE facebook_id='$facebook_id'";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	/**
	 * Retrieve the Unix timestamp of a specific event
	 *
	 * @param integer $id	ID of event
	 * @return integer
	 */
	public function EventGetTs($id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) AS start_date_ts FROM events WHERE id_event=$id";
		$db->query_single($row, $sqlstr);
		return $row['start_date_ts'];
	}

	/**
	 * Delete specific event
	 *
	 * @param integer $id_event
	 * @param integer $id_topic	ID of topic for propagating calendar updates
	 */
	public function EventDelete($id_event, $id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "events" );
		$res[] = $db->query( "DELETE FROM events WHERE id_event=$id_event" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_event,$o->types['event']);
		$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['delete']);
		if ($id_topic>0)
			$this->EventQueueJobSet($id_topic);
	}

	/**
	 * Insert an event in the calendar
	 *
	 * @param datetime 	$start_date			Start date and time
	 * @param integer 	$length				Length (in hours)
	 * @param boolint 	$allday				Whether the event lasts all day long
	 * @param integer 	$id_event_type		ID of event type
	 * @param string 	$title				Title
	 * @param string 	$description		Description
	 * @param string 	$place				Location
	 * @param string 	$address			Address
	 * @param integer 	$id_geo				ID of regional location
	 * @param string 	$place_details		Location details
	 * @param string 	$contact_name		Contact info
	 * @param string 	$email				Contact email
	 * @param string 	$phone				Contact phone
	 * @param string 	$link				URL for additional info
	 * @param integer 	$id_article			ID of related article
	 * @param integer 	$id_topic			ID of event topic
	 * @param boolint 	$portal				Whether is shown in the portal
	 * @param boolint 	$approved			Approved
	 * @param string 	$keywords			Keywords
	 * @param boolint	$jump_to_article	Jump straight to article
	 * @param integer 	$id_p				ID of visitor submitting the event
	 * @param integer 	$id_group			ID of event topic group
	 * @return integer						ID of new event
	 */
	public function EventInsert( $start_date, $length, $allday, $id_event_type, $title, $description, $place, $address, $id_geo,
			$place_details, $contact_name, $email, $phone, $link, $id_article, $id_topic, $portal, $approved, $keywords,$jump_to_article,$id_p,$id_group,$facebook_id='',$latitude=NULL,$longitude=NULL )
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$ip = Varia::IP();
		$db->begin();
		$db->lock( "events" );
		$id_event = $db->nextId( "events", "id_event" );
		$sqlstr =  "INSERT INTO events (id_event, start_date,length,place,address,place_details,id_geo,id_event_type,
		contact_name,email,link,phone,title,description,approved,allday,id_article,id_topic,portal,
		jump_to_article,id_p,insert_date,ip,id_group,is_html,facebook_id,latitude,longitude)
		 VALUES ($id_event,'$start_date','$length','$place','$address','$place_details','$id_geo','$id_event_type',
		 '$contact_name','$email','$link','$phone','$title','$description',$approved,$allday,
		'$id_article','$id_topic',$portal,$jump_to_article,$id_p,'$today_time','$ip','$id_group','1','$facebook_id','$latitude','$longitude')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_event, $o->types['event']);
		$this->Geocode($id_event);
		$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['create']);
		if ($approved=="1")
		{
			$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['approve']);
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd($o->types['event'],$id_event,0,0,1);
			if($id_topic>0)
				$this->EventQueueJobSet($id_topic);
		}
		return $id_event;
	}

	/**
	 * Update event data
	 *
	 * @param integer	$id_event			ID of event
	 * @param datetime 	$start_date			Start date and time
	 * @param integer 	$length				Length (in hours)
	 * @param boolint 	$allday				Whether the event lasts all day long
	 * @param integer 	$id_event_type		ID of event type
	 * @param string 	$title				Title
	 * @param string 	$description		Description
	 * @param string 	$place				Location
	 * @param string 	$address			Address
	 * @param integer 	$id_geo				ID of regional location
	 * @param string 	$place_details		Location details
	 * @param string 	$contact_name		Contact info
	 * @param string 	$email				Contact email
	 * @param string 	$phone				Contact phone
	 * @param string 	$link				URL for additional info
	 * @param integer 	$id_article			ID of related article
	 * @param integer 	$id_topic			ID of event topic
	 * @param boolint 	$portal				Whether is shown in the portal
	 * @param boolint 	$approved			Approved
	 * @param string 	$keywords			Keywords
	 * @param integer 	$approved_old		Previous approval status
	 * @param boolint	$jump_to_article	Jump straight to article
	 * @param integer 	$id_group			ID of event topic group
	 */
	public function EventUpdate($id_event, $start_date, $length, $allday, $id_event_type, $title, $description, $place, $address, $id_geo,
			$place_details, $contact_name, $email, $phone, $link, $id_article, $id_topic, $portal, $approved, $keywords, $approved_old,$jump_to_article,$id_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "events" );
		$sqlstr = "UPDATE events SET start_date='$start_date',length='$length',place='$place',
		place_details='$place_details',id_geo=$id_geo,id_event_type=$id_event_type,address='$address',
		contact_name='$contact_name',email='$email',link='$link',phone='$phone',title='$title',
		description='$description',approved=$approved,allday=$allday,id_article=$id_article,id_topic=$id_topic,
		portal=$portal,jump_to_article=$jump_to_article,id_group=$id_group,is_html=1
		 WHERE id_event='$id_event' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_event, $o->types['event']);
		$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['update']);
		if ($approved!=$approved_old)
		{
			if ($approved)
			{
				$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['approve']);
			}
			else
			{
				$this->h->HistoryAdd($this->h->types['event'],$id_event,$this->h->actions['reject']);
			}
		}
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($approved)
		{
			$s->IndexQueueAdd($o->types['event'],$id_event,0,0,1);
		}
		elseif($approved_old)
			$s->ResourceRemove($o->types['event'],$id_event);
		if ($approved=="1" && $id_topic>0)
			$this->EventQueueJobSet($id_topic);
	}

	public function ImageUpdate($id_event,$file)
	{
		$this->ImageDelete($id_event,false);
		$filename = "uploads/events/orig/{$id_event}.jpg";
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->Delete($filename);
		$fm->MoveUpload($file['temp'],$filename);
		$this->ImageStore($id_event);
		$fm->PostUpdate();
	}
	
	/**
	 * Propagate the update to subtopic containing the calendar
	 *
	 * @param integer $id_topic
	 */
	private function EventQueueJobSet($id_topic)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$id_subtopic = $t->HasSubtopicType($t->subtopic_types['calendar']);
		if ($id_subtopic>0)
		{
			$t->queue->JobInsert($t->queue->types['subtopic'],$id_subtopic,"update");
			$t->queue->JobInsert($t->queue->types['topic_home'],0,"update");
		}
	}
	
	public function FacebookImport($link) {
		include_once(SERVER_ROOT."/../classes/file.php");
		$conf = new Configuration();
		$id_event = 0;
		$track_error = false;
		$this->errors = array();
		if($conf->Get("fb_scraper")!='') {
			$fm = new FileManager();
			$url = str_ireplace('FBEVENTURL', $link, $conf->Get("fb_scraper"));
			$response = $fm->Browse($url);
			if($response!='') {
				$json = json_decode($response);
				if(json_last_error() === JSON_ERROR_NONE) {
					$db =& Db::globaldb();
					if(!(isset($json->facebook_id) && $json->facebook_id!='')) {
						$this->errors[] = "Facebook ID missing";
						$track_error = true;
					}
					
					if(isset($json->title) && $json->title!='') {
						$title = $db->SqlQuote($json->title);
					} else {
						$track_error = true;
						$this->errors[] = "Title missing";
					}
					$start_ts = $end_ts = 0;
					$length = 2;
					if(strpos($json->eventTime,' to ')!==false) {
						$time = explode(' to ', $json->eventTime);
						if($time[0]!='' && $time[1]!='') {
							$start_ts = strtotime($time[0]);
							$end_ts = strtotime($time[1]);
							if($start_ts>0 && $end_ts>0 && $end_ts>$start_ts) {
								floor(($end_ts - $start_ts)/3600);
							}
						}
					} else {
						$start_ts = strtotime($json->eventTime);
					}
					if($start_ts>0) {
						$start_date = $db->getTodayTime($start_ts);
					} else {
						$this->errors[] = "Timestamp missing";
					}
					
					if(count($this->errors)==0) {
						$address = (isset($json->venue) && $json->venue!='')? $db->SqlQuote(str_replace(', Italy', '', $json->venue)) : '';
						$description = '';
						if((isset($json->description) && $json->description!='')) {
							// strip 4-bytes Unicode
							$description = preg_replace('/[\xF0-\xF7].../s', '', $json->description);
							$description = $db->SqlQuote($description);
						}
						$event_type = $json->id_type;
						$fb_link = "https://www.facebook.com/events/{$json->facebook_id}/";
						$lat = (isset($json->venueLatitude) && $json->venueLatitude!='')? $db->SqlQuote($json->venueLatitude) : NULL;
						$long = (isset($json->venueLongitude) && $json->venueLongitude!='')? $db->SqlQuote($json->venueLongitude) : NULL;
						
						$id_geo = $place = "";
						if(isset($json->venueTown) && $json->venueTown!='') {
							$place = $db->SqlQuote($json->venueTown);
						}
						if(isset($json->venueProvince) && $json->venueProvince!='') {
							include_once(SERVER_ROOT."/../classes/geo.php");
							$geo = new Geo();
							$province = $db->SqlQuote($json->venueProvince);
							$prov = $geo->ProvinceGetByProvince($province);
							if(isset($prov['id_prov'])) {
								$id_geo = $prov['id_prov'];
							}
						}
						
						// store event
						$id_event = $this->EventInsert("$start_date", "$length", '0', "$event_type", "$title", "$description", "$place", "$address", "$id_geo", "", "", "", "", "$fb_link", 0, 0, 1, 0, "", 0, 0, 0,"{$json->facebook_id}",$lat,$long);
						
						// download image
						if(isset($json->coverImage) && $id_event>0) {
							$filename = "uploads/events/orig/{$id_event}.jpg";
							$fm->DownLoad($json->coverImage, $filename);
							if($fm->Exists($filename)) {
								$this->ImageStore($id_event);
							}
						}
					}
					
				} else {
					$this->errors[] = "Invalid json";
					$track_error = true;
				}
			} else {
				$this->errors[] = "Empty response (is the event public?)";
				$track_error = true;
			}
		} else {
			$this->errors[] = 'Scraper Missing';
		}
		if(count($this->errors)>0 && $track_error) {
			UserError("Facebook import error: " . implode(',',$this->errors) , array('link'=>$link));
		}
		return $id_event;
	}
	
	public function Geocode($id) {
		$event = $this->EventGet($id);
		$update = false;
		// virtual
		if($event['id_geo']=='105') {
			$latitude = 0;
			$longitude = 0;
			$update = true;
		} else {
			include_once(SERVER_ROOT."/../classes/file.php");
			$conf = new Configuration();
			$google_api_key = $conf->Get("google_api_key");
			$fm = new FileManager();
			$address = urlencode("{$event['address']} {$event['place']} {$event['place_details']}");
			$res = $fm->Browse("https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$google_api_key}");
			$data = json_decode($res,true);
			if(json_last_error() === JSON_ERROR_NONE) {
				if(isset($data['results']) && count($data['results'])>0 && isset($data['results'][0]['geometry']['location'])) {
					$latitude = $data['results'][0]['geometry']['location']['lat'];
					$longitude = $data['results'][0]['geometry']['location']['lng'];
					$update = true;
				}
			}	
		}
		if($update) {
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "events" );
			$sqlstr = "UPDATE events SET latitude='$latitude',longitude='$longitude' WHERE id_event='$id' ";
			$db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}

	public function IcsImport($file) {
		require(SERVER_ROOT."/../others/vendor/autoload.php");
		$ical = new ICal();
		$ical->initFile($file['temp']);
		if($ical->hasEvents()) {
			foreach($ical->events() as $event) {
				print_r($event);
				$summary = $event->summary;
				$start_ts = strtotime($event->dtstart);
				$end_ts = strtotime($event->dtend);
				$description = $event->description;
				$location = $event->location;
				$uid = $event->uid;
				$isFacebook = strpos($uid,'@facebook.com') !== false;
				if($isFacebook) {
					$facebook_id = str_replace('@facebook.com', '', $uid);
				}
			}
		}
		exit;
	}

	public function ImageDelete($id_event,$update_record=true)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$fm = new FileManager;
		$fm->Delete("uploads/events/orig/{$id_event}.jpg");
		$i->RemoveWrapper("event_image",$id_event);
		$i->CDNReset('events', $id_event, $i->convert_format, true);
		$fm->Delete("pub/{$i->pub_path}events/0/{$id_event}.jpg");
		$fm->Delete("pub/{$i->pub_path}events/1/{$id_event}.jpg");
		$fm->Delete("pub/{$i->pub_path}events/2/{$id_event}.jpg");
		$fm->Delete("pub/{$i->pub_path}events/orig/{$id_event}.jpg");
		if($update_record) {
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "events" );
			$sqlstr = "UPDATE events SET has_image='0',image_ratio='0' WHERE id_event='$id_event' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		$fm->PostUpdate();
	}
	
	private function ImageStore($id_event) {
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$origfile = "uploads/events/orig/{$id_event}.jpg";
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$i->ConvertWrapper("event_image",$origfile,$id_event);
		if(!$i->isCDN) {
			$filename = $id_event . "." . $i->convert_format;
			$fm->Copy("uploads/events/0/$filename","pub/{$i->pub_path}events/0/$filename");
			$fm->Copy("uploads/events/1/$filename","pub/{$i->pub_path}events/1/$filename");
			$fm->Copy("uploads/events/2/$filename","pub/{$i->pub_path}events/2/$filename");
		} else {
			$i->CDNReset('events', $id_event, $i->convert_format);
		}
		$sizes = $fm->ImageSize($origfile);
		$image_ratio = ($sizes['width']>0 && $sizes['height']>0)? $sizes['width']/$sizes['height'] : 0;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "events" );
		$sqlstr = "UPDATE events SET has_image='1',image_ratio='$image_ratio' WHERE id_event='$id_event' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$fm->PostUpdate();
	}
	
	/**
	 * Delete a recurring event
	 *
	 * @param integer $id
	 */
	public function RecurringDelete($id)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "recurring_events" );
		$res[] = $db->query( "DELETE FROM recurring_events WHERE id_event=$id" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id,$o->types['r_event']);
	}

	/**
	 * Retrieve a recurring event
	 *
	 * @param integer $id
	 * @return array
	 */
	public function RecurringGet($id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_event,description,r_year,r_month,r_day,url,approved FROM recurring_events WHERE id_event=$id";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Insert a new recurring event
	 *
	 * @param integer 	$r_day			Day of recurring event
	 * @param integer 	$r_month		Month of recurring event
	 * @param integer 	$r_year			Year of original event
	 * @param string 	$description	Description
	 * @param string 	$url			URL for additional info
	 * @param boolint 	$approved		Approved
	 * @param string	$keywords		keywords
	 * @return integer					ID of new recurring event
	 */
	public function RecurringInsert( $r_day, $r_month, $r_year, $description, $url, $approved, $keywords )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "recurring_events" );
		$id_event = $db->nextId( "recurring_events", "id_event" );
		$sqlstr = "INSERT INTO recurring_events (id_event, r_day,r_month,r_year,url,description,approved)
			 VALUES ($id_event, $r_day,$r_month,$r_year,'$url','$description',$approved)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_event, $o->types['r_event']);
		return $id_event;
	}

	/**
	 * Update recurring event
	 *
	 * @param integer 	$id_event		ID of recurring event
	 * @param integer 	$r_day			Day of recurring event
	 * @param integer 	$r_month		Month of recurring event
	 * @param integer 	$r_year			Year of original event
	 * @param string 	$description	Description
	 * @param string 	$url			URL for additional info
	 * @param boolint 	$approved		Approved
	 * @param string	$keywords		keywords
	 */
	public function RecurringUpdate($id_event, $r_day, $r_month, $r_year, $description, $url, $approved, $keywords)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "recurring_events" );
		$sqlstr = "UPDATE recurring_events SET r_day=$r_day,r_month=$r_month,r_year=$r_year,
			url='$url',description='$description',approved=$approved
		 	WHERE id_event=$id_event";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_event, $o->types['r_event']);
	}

	/**
	 * Retrieve event type
	 *
	 * @param integer $id	ID of event type
	 * @return array
	 */
	public function TypeGet($id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_event_type,type FROM event_types WHERE id_event_type=$id";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Insert new event type
	 *
	 * @param string $type
	 */
	public function TypeInsert( $type )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "event_types" );
		$id_event_type = $db->nextId( "event_types", "id_event_type" );
		$sqlstr = "INSERT INTO event_types (id_event_type, type) VALUES ($id_event_type, '$type')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Update event type
	 *
	 * @param integer $id_event_type
	 * @param string $type
	 */
	public function TypeUpdate($id_event_type, $type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "event_types" );
		$sqlstr = "UPDATE event_types SET type='$type' WHERE id_event_type=$id_event_type";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		// TODO should update all calendars in all topics
	}
	
}
?>
