<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/datetime.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");
include_once(SERVER_ROOT."/../classes/session.php");

class HtmlHelper
{
	/** 
	 * @var GroupHelper */
	public $groups;
	
	/** 
	 * @var Topic */
	public $topic;
	
	/** 
	 * @var Ini */
	public $ini;
	
	/**
	 * @var Translator */
	public $tr;

	/**
	 * @var DateTimeHelper */
	private $dt;
	
	/**
	 * @var TextHelper */
	public $th;
	
	/** 
	 * @var Images */
	protected $i;
	
	public $mandatory;
	public $records_per_page;
	public $img_arrow_up_off;
	public $img_arrow_down_off;
	public $img_arrow_up_on;
	public $img_arrow_down_on;

	private $init_year;
	private $img_folder;
	private $encoding;
	private $highslide;

	function __construct($set_images=true)
	{
		$conf = new Configuration();
		$this->records_per_page = $conf->Get("records_per_page");
		$this->encoding = $conf->Get("encoding");
		$this->highslide = $conf->Get("highslide");
		$this->ini = new Ini;
		$this->init_year = $this->ini->Get('init_year');
		$session = new Session();
		$this->tr = new Translator($session->Get("id_language"),0);
		$this->dt = new DateTimeHelper($this->tr->Translate("month"),$this->tr->Translate("weekdays"),$this->tr->Translate("hours"));
		$this->th = new TextHelper();
		$this->i = new Images();
		$this->mandatory = FALSE;
		if($set_images)
		{
			$this->img_folder = "<img src=\"/images/bullet1.gif\" width=\"22\" height=\"18\" />";
			$this->img_arrow_up_off = "<img src=\"/images/arrow1b.gif\" border=\"0\" width=\"7\" />";
			$this->img_arrow_down_off = "<img src=\"/images/arrow2b.gif\" border=\"0\" width=\"7\" />";
			$this->img_arrow_up_on = "<img src=\"/images/arrow1.gif\" border=\"0\" alt=\"" . $this->tr->Translate("move_up") . "\"  title=\"" . $this->tr->Translate("move_up") . "\"width=\"7\" />";
			$this->img_arrow_down_on = "<img src=\"/images/arrow2.gif\" border=\"0\" alt=\"" . $this->tr->Translate("move_down") . "\" title=\"" . $this->tr->Translate("move_down") . "\" width=\"7\" />";
		}
	}

	public function Array2String( $values, $separator=", " )
	{
		return implode($separator,$values);
	}

	public function Bool2UpDown($bool,$center=true)
	{
		return $this->Wrap(strtoupper((($bool=="1")? "UP":"DOWN")),"<div class=\"center\">","</div>",$center);
	}

	public function ColumnsMaker( $items, $columns_num )
	{
		if (count($items)>0)
		{
			$width = floor(100/$columns_num);
			$columns = "<table>\n";
			$counter = 0;
			foreach($items as $item)
			{
				if ($counter==0)
					$columns .= "<tr>";
				$columns .= "<td class=\"column_cell\" width=\"$width%\" valign=\"top\">$item</td>";
				$counter ++;
				if ($counter==$columns_num)
				{
					$columns .= "</tr>\n";
					$counter = 0;
				}
			}
			if ($counter>0)
			{
				for ($i=1;$i<=($columns_num-$counter);$i++)
					$columns .= "<td>&nbsp;</td>";
				$columns .= "</tr>\n";
			}
			$columns .= "</table>\n";
		}
		return $columns;
	}
	
	public function FileSize($filename)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fileinfo = $fm->FileInfo("uploads/$filename");
		if ($fileinfo['exists'])
		{
			$file_size = " (" . $fileinfo['kb'] . " Kb - " . $this->tr->Translate("format") . " <b>" . strtoupper($fileinfo['format']) . "</b>)";
		}
		else
			$file_size .= " (ERROR: file not found)";
		return $file_size;
	}

	public function FormatDate($ts,$condition=true)
	{
		return $condition? $this->dt->FormatDate($ts) : "-";
	}

	public function FormatDateTime($ts)
	{
		return $this->dt->FormatDate($ts) . " " . $this->dt->FormatTime($ts);
	}

	public function FormatDateTimeSeconds($ts)
	{
		return $this->dt->FormatDate($ts) . " " . $this->dt->FormatTimeSeconds($ts);
	}
    
    public function FormatDateTimeMilliseconds($ts , $ms)
    {
        return $this->dt->FormatDate($ts) . " " . $this->dt->FormatTimeSeconds($ts).".".$ms;   
    }

	public function FormatMonth($ts)
	{
		return $this->dt->FormatMonth($ts);
	}

	public function FormatTime($ts)
	{
		return $this->dt->FormatTime($ts);
	}

	public function geo_label($geo_location)
	{
		switch($geo_location)
		{
			case "1";
				$label = "province";
			break;
			case "5";
				$label = "region";
			break;
			case "2";
			case "6";
			case "7";
				$label = "country";
			break;
			case "3";
			case "4";
				$label = "county";
			break;
			default:
				$label = "region";
		}
		return $label;
	}
	
	public function Graphic($filename,$width,$height,$is_flash=false,$id=0)
	{
		if ($height>0)
			$height_attr = " height=\"$height\"";
		if ($width>0)
			$width_attr = " width=\"$width\"";
		$graphic = "<img src=\"$filename\" $width_attr $height_attr border=\"0\">";
		return $graphic;
	}

	public function GroupsParentsTree($id_group,$id_parent)
	{
		$top = $this->groups->gh->top_name;
		$desc = $this->groups->gh->top_description;
		$stree = "<ul class=\"tree\"><li>\n";
		if ($id_parent>0)
			$stree .= "<a href=\"#\"  onClick=\"set_group('0','$top');\">$top</a>";
		else
			$stree .= "<b>$top</b>";
		$stree .= "<div>$desc</div>";
		$stree .= $this->GroupsParentsTreeRecurse(0,$id_group,$id_parent,$id_group);
		$stree .= "</li></ul>\n";
		return $stree;
	}

	private function GroupsParentsTreeRecurse($id_root,$id_group,$id_parent,$id_first_group)  // per scegliere il genitore del gruppo
	{
		$img = $this->img_folder;
		$children = $this->groups->gh->th->GroupChildren($id_root);
		$stree = "<ul class=\"tree\">\n";
		foreach($children as $child)
		{
			$group_name = $this->th->TextReplaceForJavascript($child['name']);
			$stree .= "<li>$img";
			if ($child['id_group']==$id_group)
				$stree .= "<u>{$child['name']}</u>";
			else
			{
				if ($child['id_group']==$id_parent)
					$stree .= "<b>{$child['name']}</b>\n";
				elseif ($this->groups->gh->th->GroupIsChild($child['id_group'],$id_group) && $id_first_group>0)
					$stree .= "<span class=\"off\">$child[name]</span>";
				else
					$stree .= "<a href=\"#\" onClick=\"set_group('".$child['id_group']."','$group_name');\">{$child['name']}</a>";
			}
			$stree .= "<div>".$this->groups->gh->th->description[$child['id_group']] . "</div>";
			$stree .= $this->GroupsParentsTreeRecurse($child['id_group'],$id_group,$id_parent, $id_first_group);
			$stree .= "</li>\n";
		}
		$stree .= "</ul>\n";
		return $stree;
	}

	public function GroupsTree($move_items=false)   // per mostrare l'albero e gli items
	{
		$this->groups->gh->UserRightSet();
		$ttree = "<ul class=\"tree\"><li><b>{$this->groups->gh->top_name}</b><div class=\"notes\">{$this->groups->gh->top_description}</div>\n";
		$ttree .= $this->GroupsTreeRecurse(0,$move_items);
		$ttree .= "</li></ul>\n";
		return $ttree;
	}

	public function GroupsTree2( $id_group )
	{
		$ttree = "<ul class=\"tree\"><li><i>{$this->groups->gh->top_name}</i><br>{$this->groups->gh->top_description}\n";
		$ttree .= $this->GroupsTree2Recurse(0, $id_group);
		$ttree .= "</li></ul>\n";
		return $ttree;
	}

	public function GroupsTree3()
	{
		$this->groups->gh->UserRightSet();
		$ttree = "<ul class=\"tree\"><li><b>{$this->groups->gh->top_name}</b>\n";
		$ttree .= $this->GroupsTree3Recurse(0);
		$ttree .= "</li></ul>\n";
		return $ttree;
	}

	private function GroupsTreeRecurse($id_root,$move_items=false) // per mostrare l'albero e gli items
	{
		$children = $this->groups->gh->th->GroupChildren($id_root);
		$tot_children = count($children);
		if (count($children)>0)
		{
			$ttree = "<ul class=\"tree\">\n";
			foreach($children as $child)
			{
				$child_name = (($child['name'])!="")? $child['name'] : "---";
				$ttree .= "<li>";
				if ($this->groups->gh->input_right)
				{
					if ($child['seq']!=1)
						$ttree .= "<a href=\"actions.php?from2=group&id=".$child['id_group']."&action3=up\">$this->img_arrow_up_on</a>";
					else
						$ttree .= $this->img_arrow_up_off;
					if ($child['seq']!=$tot_children)
						$ttree .= "<a href=\"actions.php?from2=group&id=".$child['id_group']."&action3=down\">$this->img_arrow_down_on</a>";
					else
						$ttree .= $this->img_arrow_down_off;
				}
				$ttree .= "$this->img_folder&nbsp;";
				$ttree .= ($child['visible']==0)? "(inv.) " : "";
				$ttree .= $this->Wrap("<b>$child_name</b>","<a href=\"group.php?id=".$child['id_group']."\">","</a>",$this->groups->gh->input_right);
				$ttree .= "<div class=\"notes\">".$this->groups->gh->th->description[$child['id_group']]."</div>\n";
				$group_items = $this->groups->gh->GroupItems($child['id_group']);
				$tot_items = count($group_items);
				if (count($group_items)>0)
				{
					$ttree .= "<ul class=\"tree\">";
					foreach($group_items as $item)
					{
						$ttree .= "<li>";
						if($move_items)
						{
							if ($item['seq']!=1)
								$ttree .= "<a href=\"actions.php?from2=group_item&id_item=".$item['id_item']."&id_group={$child['id_group']}&action3=up\">$this->img_arrow_up_on</a>";
							else
								$ttree .= $this->img_arrow_up_off;
							if ($item['seq']!=$tot_items)
								$ttree .= "<a href=\"actions.php?from2=group_item&id_item=".$item['id_item']."&id_group={$child['id_group']}&action3=down\">$this->img_arrow_down_on</a>";
							else
								$ttree .= $this->img_arrow_down_off;
							$ttree .= "&nbsp;";								
						}
						if ($item['visible']==0)
							$ttree .= "(inv.) ";
						$ttree .= "<a href=\"ops.php?id={$item['id_item']}\">{$item['name']}</a><div class=\"notes\">{$item['description']}</div></li>\n";
					}
					$ttree .= "</ul>";
				}
				$ttree .= "</li>\n";
				$ttree .= $this->GroupsTreeRecurse($child['id_group'],$move_items);
			}
			$ttree .= "</ul>\n";
		}
		return $ttree;
	}

	private function GroupsTree2Recurse($id_root,$id_first_group) // per collocare la galleria
	{
		$children = $this->groups->gh->th->GroupChildren($id_root);
		if (count($children)>0)
		{
			$stree = "<ul class=\"tree\">\n";
			foreach($children as $child)
			{
				$group_name = $this->th->TextReplaceForJavascript($child['name']);
				$group_description = $this->groups->gh->th->description[$child['id_group']];
				$stree .= "<li>";
				$stree .= ($child['visible']==0)? "(inv.) " : "";
				if ($child['id_group']==$id_first_group)
					$stree .= "<b>{$child['name']}</b>";
				else
					$stree .= "<a href=\"#\" onClick=\"set_group('".$child['id_group']."','$group_name');\">{$child['name']}</a>\n";
				$stree .= "<div>$group_description</div></li>\n";
				$stree .= $this->GroupsTree2Recurse($child['id_group'],$id_first_group);
			}
			$stree .= "</ul>\n";
		}
		return $stree;
	}

	private function GroupsTree3Recurse($id_root)
	{
		$children = $this->groups->gh->th->GroupChildren($id_root);
		if (count($children)>0)
		{
			$stree = "<ul class=\"tree\">\n";
			foreach($children as $child)
			{
				$stree .= "<li>";
				$stree .= ($child['visible']==0)? "(inv.) " : "";
				$stree .= "<a href=\"/admin/group_publish.php?id={$child['id_group']}\">{$child['name']}</a>\n";
				$group_items = $this->groups->gh->GroupItems($child['id_group']);
				if (count($group_items)>0)
				{
					$stree .= "<ul class=\"tree\">";
					foreach($group_items as $item)
					{
						$stree .= "<li>";
						if ($item['visible']==0)
							$stree .= "(inv.) ";
						$stree .= "{$item['name']}</li>";
					}
					$stree .= "</ul>";
				}
				$stree .= "</li>\n";
				$stree .= $this->GroupsTree3Recurse($child['id_group']);
			}
			$stree .= "</ul>\n";
		}
		return $stree;
	}

	public function input_array($input_descriz,$input_name,$input_value,$values,$input_right,$event="",$size=0)
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		if ($input_right==1)
		{
			$input .= "<select name=\"$input_name\"";
			if ($event!="")
				$input .= " onChange=\"$event\"";
            if ($size != "")
                $input .= " size=\"$size\"";  
			$input .= ">\n";
			foreach ($values as $key => $value)
			{
				if ($value!="___")
				{
					$input .= "<option value=\"$key\"";
					if ($key==$input_value)
						$input .= " selected ";
					$input .= ">$value</option>\n";
				}
			}
			$input .= "</select>\n";
		}
		else
		{
			foreach ($values as $key => $value)
			{
				if ($key==$input_value)
				{
					$input .= "<span class=\"inactive\">$value</span>";
					$input .= "<input type=\"hidden\" name=\"$input_name\" value=\"$key\">";
				}
			}
		}
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_checkbox($input_descriz,$input_name,$input_value,$help,$input_right)
	{
		return $this->input_checkbox2($input_descriz,"",$input_name,$input_value,"",$input_right);
	}

	public function input_checkbox_warn($input_descriz,$input_name,$input_value,$warn,$input_right)
	{
		return $this->input_checkbox2($input_descriz,"",$input_name,$input_value,$warn,$input_right);
	}

	private function input_checkbox2($input_descriz,$input_descriz2,$input_name,$input_value,$warn,$input_right)
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		if ($input_right==1)
		{
			$input .= "<input class=\"input-checkbox\" type=\"checkbox\" id=\"$input_name-field\" name=\"$input_name\" ";
			if ($input_value==1)
				$input .= " checked";
			if ($warn!="")
				$input .= " onClick=\"alertCheckboxClick(this,'" .  $this->th->TextReplaceForJavascript($warn) . "');\"";
			$input .= " >";
		}
		else
			$input .= "<span class=\"inactive\">" . (($input_value==1)? $this->tr->Translate("yes") : $this->tr->Translate("no") ) . "</span>" . $this->input_hidden($input_name,$input_value);
		if ($input_descriz2!="")
			$input .= " ($input_descriz2)";
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_checkbox3($input_descriz,$input_name,$input_value,$checked,$input_right)
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		if ($input_right==1)
		{
			$input .= "<input class=\"input-checkbox\" type=\"checkbox\" name=\"$input_name\"  value=\"$input_value\" ";
			if ($checked)
				$input .= " checked";
			$input .= " >";
		}
		else
			$input .= "<span class=\"inactive\">" . ($checked? $this->tr->Translate("yes") : $this->tr->Translate("no") ) . "</span>" . $this->input_hidden($input_name,$input_value);
		$input .= "</td></tr>\n";
		return $input;
	}
	
	public function input_condition($input_descriz,$condition_var,$condition_type,$condition_value,$input_right)
	{
		$input_name = "condition";
		$types = array("","Is set","<","=",">","Is not set");
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->Wrap($this->tr->TranslateTry($input_descriz),"<b>","</b>",$this->mandatory) . "</td><td>\n";
		if ($input_right==1)
		{
			$input .= "<input id=\"{$input_name}_var-field\" type=\"text\" name=\"{$input_name}_var\" value=\"" . htmlspecialchars($condition_var) . "\" > ";
			$input .= "<select name=\"{$input_name}_type\">\n";
			foreach ($types as $key => $type)
			{
				$input .= "<option value=\"$key\"";
				if ($key==$condition_type)
					$input .= " selected ";
				$input .= ">$type</option>\n";
			}
			$input .= "</select>\n";
			$input .= "<input id=\"{$input_name}_value-field\" type=\"text\" name=\"{$input_name}_value\" value=\"" . htmlspecialchars($condition_value) . "\" > ";
		}
		elseif($condition_var!="")
		{
			$input .= "<span class=\"inactive\">$condition_var " . $types[$condition_type] . $condition_value;
		}
		$input .= "</td></tr>\n";
		return $input;		
	}
	
	public function input_internal_keywords($id_resource,$internal_keywords,$res_type,$input_super_right,$input_right,$show_use_info=false,$input_super_super_right=false)
	{
		$input = "";
		$o = new Ontology();
		if(count($internal_keywords)>0)
		{
			$current_keywords_internal = ($id_resource>0)? $o->UseInternal($id_resource,$o->types[$res_type]) : array();
			$id_keywords_internal = array();
			foreach($current_keywords_internal as $current_keyword_internal)
				$id_keywords_internal[] = $current_keyword_internal['id_keyword'];
			$visible_internal_keywords = 0;
			foreach($internal_keywords as $tikeyword)
			{
				if($input_super_super_right===false) {
					$tik_right = $tikeyword['role']? $input_super_right : $input_right;
				} else {
					switch($tikeyword['role']) {
						case 0:
							$tik_right = $input_right;
							break;
						case 1:
							$tik_right = $input_super_right;
							break;
						case 2:
							$tik_right = $input_super_super_right;
							break;
					}
				}
				$idesc = $tikeyword['description']!=""? $tikeyword['description'] : $tikeyword['keyword'];
				if($tik_right)
				{
					$input .= $this->input_checkbox3($idesc,"ikeywords[]",$tikeyword['id_keyword'],in_array($tikeyword['id_keyword'],$id_keywords_internal),$tik_right);
					$visible_internal_keywords ++;
				}
				elseif(in_array($tikeyword['id_keyword'],$id_keywords_internal)) 
				{
					$input .= $this->input_hidden("ikeywords[]",$tikeyword['id_keyword']);
				}
			}
			if($visible_internal_keywords>0)
			{
				$input = $this->input_separator("internal_use") . ($show_use_info? $this->input_note("internal_use_info"):"") . $input;
			}
		}
		return $input;
	}
	
	public function InputDateEvent($input_descriz,$input_name,$input_value,$input_right,$event,$show_year=1,$allow_blank_date=false,$show_time=false)
	{
		$month = $this->tr->Translate("month");
		$today = getdate();
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->Wrap($this->tr->TranslateTry($input_descriz),"<b>","</b>",$this->mandatory) . "</td><td>\n";

        if ($input_value>0)
            $input_date = getdate($input_value);
        elseif($allow_blank_date)
            $input_date = array('mday'=>'', 'mon'=>'', 'year'=>'');
        else
            $input_date = getdate();

		if ($input_right==1)
		{
			$input .= "<select name=\"".$input_name."_d\" $event>\n";
            if ($allow_blank_date)
            {
                $input .= "<option value=''></option>\n";
            }
			for ($i=1;$i<=31;$i++)
			{
				$input .= "<option value=$i ";
				if ($i==$input_date['mday'])
					$input .= " selected";
				$input .= ">$i</option>\n";
			}
			$input .= "</select>\n";
			$input .= "<select name=\"".$input_name."_m\" $event>\n";
            if ($allow_blank_date)
            {
                $input .= "<option value=''></option>\n";
            }
			for ($i=1;$i<=12;$i++)
			{
				$input .= "<option value=$i ";
				if ($i==$input_date['mon'])
					$input .= " selected";
				$input .= ">".$month[$i-1]."</option>\n";
			}
			$input .= "</select>\n";
			if ($show_year==1)
			{
				$input .= "<select name=\"".$input_name."_y\">\n";
                if ($allow_blank_date)
                {
	                $input .= "<option value=''></option>\n";
                }
				for ($i=$this->init_year;$i<=($today['year'] + 1);$i++)
				{
					$input .= "<option value=$i ";
					if ($i==$input_date['year'])
						$input .= " selected";
					$input .= ">$i</option>\n";
				}
				$input .= "</select>\n";
			}
            if ($show_time)
            {
                $input .= "<select name=\"".$input_name."_th\">\n";
                for ($i=0;$i<=23;$i++)
                {
                    $input .= "<option value=$i ";
                    if ($i==$input_date['hours'])
                        $input .= " selected";
                    $input .= ">".sprintf('%02d',$i)."</option>\n";
                }
                $input .= "</select>\n";
                $input .= "<select name=\"".$input_name."_tm\">\n";
                for ($i=0;$i<=59;$i++)
                {
                    $input .= "<option value=$i ";
                    if ($i==$input_date['minutes'])
                        $input .= " selected";
                    $input .= ">".sprintf('%02d',$i)."</option>\n";
                }
                $input .= "</select>\n";
            }
		}
		else
		{
			$month_index = $input_date['mon'];
            if ($show_time)
            {
                $input .= "<span class=\"inactive\">" . $input_date['mday']." ".$month[$month_index-1]." ".$input_date['year']." ".sprintf("%02d",$input_date['hours']).":".sprintf("%02d",$input_date['minutes']).":00" . "</span>";
                $input .= "<input type=\"hidden\" name=\"".$input_name."_th\" value=\"$input_date[hours]\">";
                $input .= "<input type=\"hidden\" name=\"".$input_name."_tm\" value=\"$input_date[minutes]\">";
            }
            else
            {
                $input .= "<span class=\"inactive\">" . $input_date['mday']." ".$month[$month_index-1]." ".$input_date['year'] . "</span>";
            }
			$input .= "<input type=\"hidden\" name=\"".$input_name."_d\" value=\"$input_date[mday]\">";
			$input .= "<input type=\"hidden\" name=\"".$input_name."_m\" value=\"$input_date[mon]\">";
			$input .= "<input type=\"hidden\" name=\"".$input_name."_y\" value=\"$input_date[year]\">";
		}
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_date($input_descriz,$input_name,$input_value,$input_right,$allow_blank_date=false,$show_time=false)
	{
		return $this->InputDateEvent($input_descriz,$input_name,$input_value,$input_right,"",1,$allow_blank_date,$show_time);
	}

	public function input_date_text($input_descriz,$input_name,$input_value,$input_size,$help,$input_right)
	{
		$date_text = "";
		if ($input_value!="" && strpos($input_value,"-")>0)
		{
			$values = explode("-",$input_value);
			if (checkdate($values[1],$values[2],$values[0]))
				$date_text = $values[2] . "-" . $values[1] . "-" . $values[0];
		}
		return $this->input_text($input_descriz,$input_name,$date_text,$input_size,$help,$input_right);
	}

	public function input_day($input_descriz,$input_name,$input_value,$input_right)
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td><td>\n";
		if ($input_right==1)
		{
			$input .= "<select name=\"$input_name\">\n";
			for ($i=1;$i<=31;$i++)
			{
				$input .= "<option value=$i ";
				if ($i==$input_value)
					$input .= " selected";
				$input .= ">$i</option>\n";
			}
			$input .= "</select>\n";
		}
		else
			$input .= "<span class=\"inactive\">" . $input_value . "</span>";
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_form_close()
	{
		return "</form>";
	}
	
	public function input_form($method,$action,$multipart=false,$js_function="",$id="form1")
	{
		$form = "<form id=\"$id\" name=\"$id\" action=\"$action\" method=\"$method\"";
		if ($js_function!="")
			$form .= " onsubmit=\"return $js_function;\"";
		if ($multipart)
			$form .= " enctype=\"multipart/form-data\"";
		$form .= " accept-charset=\"$this->encoding\" >\n";
		return  $form;
	}
	
	public function input_form_open()
	{
		return $this->input_form("post","actions.php");
	}
	
	public function input_geo($id_geo,$input_right,$geo_location=0,$name="id_geo")
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		if(!$geo_location>0)
		{
			$geo_location = $geo->geo_location;
		}
		switch($geo_location)
		{
			case "1";
				$reg = $geo->Provinces();
			break;
			case "2";
				$reg = $geo->Countries(1);
			break;
			case "3";
				$reg = $geo->UKCounties();
			break;
			case "4";
				$reg = $geo->EireCounties();
			break;
			case "5";
				$reg = $geo->Regions();
			break;
			case "6";
				$reg = $geo->Countries(0);
			break;
			case "7";
				$reg = $geo->Countries(3);
			break;
			default:
				$reg = array();
		}
		return $this->input_row($this->geo_label($geo_location),$name,$id_geo,$reg,"--",0,$input_right);
	}

	public function input_hidden($input_name,$input_value)
	{
		return "<input type=\"hidden\" name=\"$input_name\" value=\"$input_value\">\n";
	}

	public function input_keyword_param($id_kparam,$label,$type,$value,$input_right,$public,$params=array())
	{
		$input = "";
		$name = "param_" . $id_kparam;
		switch($type)
		{
			case "text":
			case "link":
				$input = $this->input_text($label,$name,$value,"30",0,$input_right);
			break;
			case "date":
				$name = "paramd_" . $id_kparam;
				$input = $this->input_date($label,$name,$value,$input_right);
			break;
			case "html":
				$input = $this->input_wysiwyg($label,$name,$value,1,15,$input_right,true);
			break;
			case "textarea":
				$input = $this->input_textarea($label,$name,$value,40,10,"",$input_right);
			break;
			case "checkbox":
				$input = $this->input_checkbox($label,$name,$value,0,$input_right);
			break;
			case "geo":
				$input = $this->input_geo($value,$input_right,0,$name);
			break;
			case "dropdown":
				$pvalues = explode(",",$params);
				$combo_values = array(''=>$this->tr->Translate("choose_option"));
				foreach($pvalues as $pvalue)
				{
					$pvalue = trim($pvalue);
					$combo_values[$pvalue] = $pvalue;
				}
				$input = $this->input_array($label,$name,$value,$combo_values,$input_right);
			break;
			case "mchoice":
				$pvalues = explode(",",$params);
				$counter = 1;
				foreach($pvalues as $pvalue)
				{
					$combo_values["p".$counter] = trim($pvalue);
					$counter ++;
				}
				$input = $this->input_list_array($label,$name,$value,$combo_values,$input_right,true,false,true);
			break;
		}
		if(!$public)
			$input = str_replace("input-label","input-label non-public",$input);
		return $input;
	}
	
	public function input_keywords($id, $id_type, $super_keywords, $input_right, $descriz="[[KwInput]]")
	{
		$keywords = array();
		if ($id>0)
		{
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$o->GetKeywords($id, $id_type, $keywords,0,true);
		}
		$input = "<tr id=\"finput-keywords\"><td class=\"input-label\">" . str_replace("[[KwInput]]",$this->tr->Translate("keywords_input"),$descriz) . "</td>\n<td>";
		$input .= "<table border=\"0\"><tr><td valign=top>";
		if (count($super_keywords)>0)
			$input .= "<i>" . $this->tr->Translate("keywords_already") . ": " . $this->th->ArrayMulti2stringIndex($super_keywords,'keyword') ."</i><br>";
		if ($input_right==1)
		{
		    $input .= "<input type=\"text\" name=\"keywords\" value=\"".$this->th->ArrayMulti2stringIndex($keywords,'keyword')."\" size=\"50\">";
			$input .= "</td><td>" . $this->tr->Translate("keywords_instr");
		}
		else
		    $input .= "<span class=\"inactive\">" . $this->th->ArrayMulti2stringIndex($keywords,'keyword') . "</span>";
		$input .= "</td></tr></table></td></tr>\n";
		return $input;
	}

	public function input_article($input_descriz,$input_name,$input_link,$id_article,$headline,$id_topic,$input_right)
	{
		$input_value_descriz = $this->th->StringCut($headline,30);
		if ($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$input_value_descriz = "$t->name: " . $input_value_descriz;
		}
		return $this->input_link($input_descriz,$input_name,$input_link,$id_article,$input_value_descriz,$input_right);
	}

	public function input_code($id,$label,$value,$size=70,$rows=0)
	{
		$input = "<div class=\"code-info\"><label for=\"$id\">" . $this->tr->TranslateTry($label) . ":</label>";
		if($rows>1)
			$input .= "<br><textarea name=\"$id\" class=\"code-box\" rows=\"$rows\" cols=\"$size\" readonly=\"readonly\" onClick=\"this.select();\" >" . htmlspecialchars($value) . "</textarea></div>";
		else 
			$input .= "<input name=\"$id\" type=\"text\" class=\"code-box\" size=\"$size\" value=\"" . htmlspecialchars($value) . "\" readonly=\"readonly\" onClick=\"this.select();\" ></div>";
		return $input;
	}
	
	public function input_subtopic($input_descriz,$input_name,$input_link,$id_subtopic,$id_topic,$input_right,$form_id='form1')
	{
		$hhf = new HHFunctions();
		$input_value_descriz = $hhf->PathToSubtopic($id_topic,$id_subtopic,20);
		return $this->input_link($input_descriz,$input_name,$input_link,$id_subtopic,$input_value_descriz,$input_right,$form_id);
	}

	public function input_link($input_descriz,$input_name,$input_link,$input_value,$input_value_descriz,$input_right,$form_id='form1')
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n";
		$input .= "<td><input type=hidden name=\"$input_name\" value=\"$input_value\">\n";
		if ($input_right==1)
		{
			$input_descriz = str_replace(" ","",$input_descriz);
			$input_value_descriz = htmlspecialchars($input_value_descriz);
			$input .= "<input type=\"text\" size=\"50\" onFocus=\"this.blur()\" name=\"descriz_".$input_name."\" value=\"$input_value_descriz\" class=\"input-link\">\n";
			$input .= " <a href=\"#\" onClick=\"window.open($input_link,'$input_descriz','status=no,menu=no,scrollbars=yes,width=500,height=400');\">" . $this->tr->Translate("change") . "</a></td></tr>\n";
			$input .= "<script type=\"text/javascript\">\ndocument.forms['".$form_id."'].descriz_$input_name.disabled=true;\n</script>\n";
		}
		else
			$input .= "<span class=\"inactive\">$input_value_descriz</span></td></tr>\n";
		return $input;
	}

	public function input_list($input_descriz,$input_name,$input_value,$values,$input_right)
	{
		$my_values = explode("|",$input_value);
		return $this->input_list_array($input_descriz,$input_name,$my_values,$values,$input_right,false,true,true);
	}

	public function input_list_array($input_descriz,$input_name,$input_values,$values,$input_right,$set_value=false,$show_counter=true,$shift_array=false)
	{
		$input .= "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		foreach ($values as $key => $value)
		{
			$value_index = $shift_array? $key-1 : $key;
			if ($value!="___")
			{
				if ($input_right==1)
				{
					$name = ($set_value)? $input_name . "[]" : $input_name . "_" . $key;
					$input .= "<input type=\"checkbox\" name=\"$name\"";
					if($set_value)
					{
						$input .= " value=\"$value\" ";
						$ivalues = explode(",",$input_values);
						if(in_array($value,$ivalues))
							$input .= " checked ";
					}
					elseif ($input_values[$value_index]==1)
						$input .= " checked ";

					$input .= "> $value<br>\n";
				}
				else
					$input .= "- " . $this->Wrap($value,"<b>","</b>\n",$input_values[$value_index]==1) . "<br/>\n";			
			}
		}
		if($show_counter)
			$input .= "<input type=\"hidden\" name=\"{$input_name}_counter\" value=\"" . count($values) . "\">\n";
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_list_labels($input_descriz,$input_name,$input_value,$values,$input_right)
	{
		$my_values = explode("|",$input_value);
		$input .= "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		foreach ($values as $value)
		{
			if ($value!="___")
			{
				if ($input_right==1)
				{
					$input .= "<input type=\"checkbox\" name=\"" . $input_name  . "_" . $value . "\"";
					if(in_array($value,$my_values))
						$input .= " checked ";
					$input .= ">" . $this->tr->TranslateTry($value) . "<br>\n";
				}
				else
					$input .= "- " . $this->Wrap($value,"<b>","</b>\n",in_array($value,$my_values)) . "<br/>\n";			
			}
		}
		$input .= "</td></tr>\n";
		return $input;
	}
	
	public function input_money($input_descriz,$input_name,$input_value,$input_size,$help,$input_right,$id_currency=0)
	{
		$currency = "";
		if($id_currency>0)
		{
			include_once(SERVER_ROOT."/../classes/payment.php");
			$p = new Payment();
			$currencies = $p->currencies;
			$currency = $currencies[$id_currency];		
		}
		return $this->input_text($input_descriz,$input_name,str_replace(",","",$input_value),$input_size,$help,$input_right,$currency);
	}

	public function input_month($input_descriz,$input_name,$input_value,$input_right,$allow_empty=false)
	{
		$month = $this->tr->Translate("month");
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td><td>\n";
		if ($input_right==1)
		{
			$input .= "<select name=\"$input_name\">\n";
			$start_value = ($allow_empty)? 0 :1;
			for ($i=$start_value;$i<=12;$i++)
			{
				$input .= "<option value=$i ";
				if ($i==$input_value)
					$input .= " selected";
				$input .= ">";
				$input .= ($i>0)? $month[$i-1] : "-";
				$input .= "</option>\n";
			}
			$input .= "</select>\n";
		}
		else
			$input .= "<span class=\"inactive\">" . $month[$input_value-1] . "</span>";
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_note($description,$is_warning=FALSE,$link="")
	{
		$input = "<tr><td colspan=\"2\" class=\"input-note\">";
		$desc = $this->tr->TranslateTry($description);
		$desc = $this->Wrap($desc,"<a href=\"$link\">","</a>",$link!="");
		$input .= $this->Wrap($desc,"<span class=\"warning\">","</span>",$is_warning);
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_param($param,$param2,$id_topic,$input_right)
	{
		$id_param = "param_" . $param;
		switch ($param)
		{
			case "email":
				$input = $this->input_text($param,$id_param,$param2,40,0,$input_right);
			break;
			case "url":
				$input = $this->input_text("link",$id_param,$param2,40,0,$input_right);
			break;
			case "audio":
			case "local_file":
			case "iterate_local_file":
			case "iterate_url":
			case "iterate_xpath":
			case "url_push_down_params":
				$param_name = ucwords(str_replace("_"," ",$param));
				$input = $this->input_text($param_name,$id_param,$param2,40,0,$input_right);
			break;
			case "id_content":
				$input = $this->input_row("content",$id_param,$param2,$this->th->ContentsAll($id_topic),"",0,$input_right);
			break;
			case "event_type":
				include_once(SERVER_ROOT."/../classes/events.php");
				$ee = new Events();
				$input = $this->input_row($param,$id_param,$param2,$ee->Types(),"all",0,$input_right);
			break;
			case "id_book":
				include_once(SERVER_ROOT."/../modules/books.php");
				$bb = new Books();
				$input = $this->input_row($param,$id_param,$param2,$bb->BooksAll(array('sort_by'=>"name")),"",0,$input_right);
			break;
			case "id_gallery":
				include_once(SERVER_ROOT."/../classes/galleries.php");
				$gg = new Galleries();
				$input = $this->input_galleries($param2,$gg->AllGalleries(true),"",$input_right,$id_param);
			break;
			case "wday":
				$combo_values[0] = $this->tr->Translate("every_day");
				$wk = $this->tr->Translate("weekdays");
				for($i=0;$i<7;$i++)
					$combo_values[] = $wk[$i];
				$input = $this->input_array("day",$id_param,$param2,$combo_values,$input_right);
			break;
			case "select":
				$input = $this->input_array("sort_by",$id_param,$param2,array("Random","Date desc"),$input_right);
			break;
			case "gallery_options":
				$input = $this->input_array($param."_label",$id_param,$param2,$this->tr->Translate($param),$input_right);
			break;
			case "gallery_sort":
				$input = $this->input_array("sort_by",$id_param,$param2,$this->tr->Translate($param),$input_right);
			break;
			case "paypal_currencies":
				include_once(SERVER_ROOT."/../classes/payment.php");
				$p = new Payment();
				$currencies = $p->currencies;
				$currency = $currencies[$this->ini->Get("default_currency")];
				$input = $this->input_text("paypal_currencies",$id_param,$param2,15,0,$input_right," <br><span class=\"notes\">(" . $this->tr->TranslateParams("paypal_currencies_desc",array($currency)) . ")</span>");
			break;
			case "paypal_cycle_unit":
				$input = $this->input_array("paypal_cycle_unit",$id_param,$param2,$this->tr->Translate("paypal_cycle_units"),$input_right);
			break;
			case "size":
				$combo_values = array("thumb","image");
				if($this->highslide)
					$combo_values[] = "zoomable thumbs";
				$input = $this->input_array($param,$id_param,$param2,$combo_values,$input_right);
			break;
			case "sort_by":
				$input = $this->input_array($param,$id_param,$param2,$this->tr->Translate("sort_by_options"),$input_right);
			break;
			case "show_latest":
				$combo_values[0] = $this->tr->Translate("show_latest_only");
				$combo_values[1] = $this->tr->Translate("any_latest");
				$input = $this->input_array($param,$id_param,$param2,$combo_values,$input_right);
			break;
			case "recurse":
			case "with_content":
			case "with_children":
			case "with_menu":
			case "infer":
			case "all_topics":
			case "show_captions":
			case "shuffle":
			case "use_images_link":
			case "approved":
			// case "paypal_src":
				$input = $this->input_checkbox($param,$id_param,$param2,0,$input_right);
			break;
			case "exclude_keywords":
				$input = $this->input_text($param,$id_param,$param2,30,0,$input_right);
			break;
			case "ttl":
				$input = $this->input_text($param,$id_param,$param2,3,0,$input_right,$this->tr->Translate("minutes"));
			break;
			default:
				$input = $this->input_text($param,$id_param,$param2,12,0,$input_right);
			break;
		}
		return $input;
	}
	
	public function input_galleries($id_gallery,$galleries,$first_option,$input_right,$input_name="id_gallery",$input_descriz="gallery")
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n<td>";
		if ($input_right)
		{
			$input .= "<select name=\"$input_name\">\n";
			if ($first_option != "")
				$input .= "<option value=\"0\">" . $this->tr->TranslateTry($first_option) . "</option>\n";
			$gallery_group = "";
			$counter = 0;
			$tot = count($galleries);
			foreach($galleries as $row)
			{
				if($row['group_name']!=$gallery_group)
				{
					if($counter>0)
						$input .= "</optgroup>\n";
					$input .= "<optgroup label=\"{$row['group_name']}\">\n";
					$gallery_group = $row['group_name'];
				}
				$input .= "<option value=\"{$row['id_gallery']}\"";
				if ($row['id_gallery']==$id_gallery)
					$input .= " selected";
				$input .= ">" . $row['name'] . "</option>\n";
				$counter ++;
				if($counter==$tot)
				{
					$input .= "</optgroup>\n";
				}
			}
			$input .= "</select>\n";
		}
		else
		{
			$input .= "<span class=\"inactive\">";
			if($id_gallery>0)
			{
				foreach($galleries as $row)
				{
					if ($row['id_gallery']==$id_gallery)
						$input .= $row['name'];
				}
			}
			elseif ($first_option != "")
				$input .= $this->tr->TranslateTry($first_option);
			$input .= "</span>" . $this->input_hidden($input_name, $id_gallery );
		}
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_topics($id_topic,$id_group,$topics,$first_option,$input_right,$select_group=false,$input_name="id_topic",$input_descriz="topic")
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->Translate($input_descriz) . "</td>\n<td>";
		if ($input_right)
		{
			$input .= "<select name=\"$input_name\">\n";
			if ($first_option != "")
				$input .= "<option value=\"\">" . $this->tr->TranslateTry($first_option) . "</option>\n";
			$topic_group = "";
			$counter = 0;
			$tot = count($topics);
			foreach($topics as $row)
			{
				if($row['group_name']!=$topic_group)
				{
					if($counter>0)
						$input .= "</optgroup>\n";
					$input .= "<optgroup label=\"{$row['group_name']}\">\n";
					$topic_group = $row['group_name'];
					if($select_group)
					{
						$input .= "<option value=\"g_{$row['id_group']}\"";
						if ($row['id_group']==$id_group)
							$input .= " selected";
						$input .= " class=\"all\">" . $this->tr->TranslateParams("all_topics_in",array($row['group_name'])) . "</option>\n";
					}
				}
				$input .= "<option value=\"{$row['id_topic']}\"";
				if ($row['id_topic']==$id_topic)
					$input .= " selected";
				$input .= ">" . $row['name'] . "</option>\n";
				$counter ++;
				if($counter==$tot)
				{
					$input .= "</optgroup>\n";
				}
			}
			$input .= "</select>\n";
		}
		else
		{
			$input .= "<span class=\"inactive\">";
			if($id_group>0)
			{
				foreach ($topics as $row)
				{
					if ($row['id_group']==$id_group)
						$group_name = $row['group_name'];
				}
				$input .= $this->tr->TranslateParams("all_topics_in",array($group_name));
			}
			elseif($id_topic>0)
			{
				foreach ($topics as $row)
				{
					if ($row['id_topic']==$id_topic)
						$input .= $row['name'];
				}
			}
			elseif ($first_option != "")
				$input .= $this->tr->TranslateTry($first_option);
			$input .= "</span>" . $this->input_hidden($input_name, (($id_group>0)?"g_$id_group":"$id_topic") );
		}
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_row($input_descriz,$input_name,$input_value,$rows,$first_option,$first_option_value,$input_right,$event="")
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->Wrap($this->tr->TranslateTry($input_descriz),"<b>","</b>",$this->mandatory) . "</td>\n<td>";
		if ($input_right==1)
		{
			$input .= "<select name=\"$input_name\"";
			if ($event!="")
				$input .= " onChange=\"$event\"";
			$input .= ">\n";
			if ($first_option != "")
				$input .= "<option value=\"$first_option_value\">" . $this->tr->TranslateTry($first_option) . "</option>\n";
			foreach ($rows as $row)
			{
			    $key = array_keys($row);
				$input .= "<option value=\"{$row[$key[0]]}\"";
				if ($row[$key[0]]==$input_value)
					$input .= " selected";
				$input .= ">{$row[$key[1]]}</option>\n";
			}
			$input .= "</select>\n";
		}
		else
		{
			$input .= "<span class=\"inactive\">";
			if ($input_value==0)
				$input .= $this->tr->TranslateTry($first_option);
			foreach ($rows as $row)
			{
			    $key = array_keys($row);
			    if ($row[$key[0]]==$input_value)
					$input .= $row[$key[1]];
			}
			$input .= "</span>" . $this->input_hidden($input_name,$input_value);
		}
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_user($input_descriz,$users,$id_user,$input_right,$input_name="id_user",$show_empty=false)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$current_user = false;
		$set_user = false;
		foreach($users as $user)
		{
			if($user['id_user']==$u->id)
				$current_user = true;
			if($user['id_user']==$id_user)
				$set_user = true;
		}
		if(!$current_user)
		{
			$row = $u->UserGet();
			$users[] = array($u->id,$row['name']);
		}
		if(!$set_user)
		{
			if($id_user>0 && $id_user!=$u->id)
			{
				$u->id = $id_user;
				$row = $u->UserGet();
				$users[] = array($u->id,$row['name']);
			}
		}
		if($show_empty)
			array_unshift($users,array(0,"-"));
		return $this->input_row($input_descriz,$input_name,$id_user,$users,"",0,$input_right);
	}
	
	public function input_separator($description)
	{
		return "<tr><td class=\"input-separator\" colspan=\"2\">" . strtoupper($this->tr->TranslateTry($description)) . "</td></tr>\n";
	}

	public function input_submit($input_descriz,$action,$input_right,$in_table=TRUE)
	{
		if ($input_right==1)
		{
			$input = "";
			if ($in_table)
				$input .= "<tr><td>&nbsp;</td>\n<td>";
			if ($action!="")
				$input .= "<input class=\"input-submit\" type=\"button\" value=\"" . $this->tr->TranslateTry($input_descriz) . "\" onclick=\"$action\">";
			else
				$input .= "<input class=\"input-submit\" type=\"submit\" value=\"" . $this->tr->TranslateTry($input_descriz) . "\">";
			if ($in_table)
				$input .= "</td></tr>\n";
		}
		return $input;
	}
	
	public function input_actions($actions,$input_right,$wrap=true)
	{
		$input = "";
		if ($input_right==1)
		{
			if ($wrap)
				$input .= "<tr><td>&nbsp;</td>\n<td>";
			foreach($actions as $action)
			{
				if ($action['right'])
				{
					$label = $this->tr->TranslateTry($action['label']);
					if($action['type']=="button")
						$input .= "<input class=\"input-submit\" type=\"button\" onclick=\"{$action['function']}\" value=\"$label\">";
					else
						$input .= "<input class=\"input-submit\" type=\"submit\" name=\"action_{$action['action']}\" value=\"$label\">";
				}
			}
			if ($wrap)
				$input .= "</td></tr>\n";
		}
		return $input;
	}
	
	public function input_table($input_descriz,$url,$input_right,$table)
	{
		$input = "<tr><td align=\"right\" valign=\"top\">" . $this->tr->TranslateTry($input_descriz);
		if ($input_right==1)
			$input .= "<br/>(<a href=\"$url\">aggiungi</a>)";
		$input .= "</td><td valign=top>$table</td></tr>\n";
		return $input;
	}
	
	public function input_table_open($id="")
	{
		$table = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" ";
		if($id!="")
			$table .= " id=\"$id\" name=\"$id\" ";
		$table .= ">\n";
		return $table;
	}

	public function input_table_close()
	{
		return "</table>\n";
	}

	private function input_text_html_replace($content)
	{
		$content = str_replace("</textarea>","</_textarea>",$content);
		$content = str_replace("&amp;","&_amp;",$content);
		$content = str_replace("&#","&_#",$content);
		$content = str_replace("&lt","&_lt",$content);
		$content = str_replace("&gt","&_gt",$content);
		return $content;
	} 
	
	public function input_text($input_descriz,$input_name,$input_value,$input_size,$help,$input_right,$input_descriz2 = "",$disable_autocomplete=false,$max_length=0,$prefix="")
	{
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->Wrap($this->tr->TranslateTry($input_descriz),"<b>","</b>",$this->mandatory) . "</td>\n<td>";
		if($prefix!="")
			$input .= $prefix;
		if (substr($input_name,0,8)=="password")
			$input_type = "password";
		else
			$input_type="text";
		if ($input_right==1)
		{
			$input .= "<input id=\"$input_name-field\" type=\"$input_type\" name=\"$input_name\" value=\"" . htmlspecialchars($input_value) . "\" size=\"$input_size\" ";
			if($disable_autocomplete)
				$input .= " autocomplete=\"off\" ";
			if ($max_length > 0)
				$input .= " maxlength=\"{$max_length}\"";
			$input .= "> ";
		}
		else
		{
			if (substr($input_name,0,8)=="password")
				$input .= "**** ";
			else
				$input .= "<span class=\"inactive\">" . $input_value . "</span>" . $this->input_hidden($input_name,strip_tags($input_value));
		}
		$input .= " " . $input_descriz2;
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_textarea($input_descriz,$input_name,$input_value,$input_cols,$input_rows,$event,$input_right,$codepress="",$escape=true,$html_replace=true)
	{
		if($html_replace)
			$input_value = $this->input_text_html_replace($input_value);
		$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">";
		if ($input_right==1)
		{
			$input .=  $this->Wrap($this->tr->TranslateTry($input_descriz),"<strong>","</strong>",$this->mandatory) . "</td>";
			$input .= "<td><textarea name=\"$input_name\" cols=\"$input_cols\" rows=\"$input_rows\" ";
			$input .= " class=\"mceNoEditor\" id=\"$input_name-field\" ";
			$input .= " $event >$input_value</textarea>";
		}
		else
		{
			$input .= str_replace("TYPE='TEXT'","TYPE='HIDDEN'",$this->tr->TranslateTry($input_descriz))."</td>\n";
			$input .= "<td valign=\"top\" class=\"inactive\">" . nl2br( $escape? htmlspecialchars($input_value) : $input_value );
		}
			$input .= "</td></tr>\n";
		return $input;
	}
	
	public function input_wysiwyg($input_descriz,$input_name,$input_value,$is_html,$input_rows,$input_right,$wysiwyg,$align=0)
	{
		if($wysiwyg)
		{
			if(!$is_html)
			{
				$input_value = $this->th->Htmlise($input_value);
				if($align>0)
				{
					switch($align)
					{
						case "1":
							$align_class = "center";
						break;
						case "2":
							$align_class = "right";
						break;
						case "3":
							$align_class = "justify";
						break;
					}
					$input_value = "<div class=\"text-{$align_class}\">" . $input_value . "</div>";
				}
			}
			$input = "<tr id=\"finput-$input_name\"><td class=\"input-label\">";
			if ($input_right==1)
			{
				$input .=  $this->Wrap($this->tr->TranslateTry($input_descriz),"<strong>","</strong>",$this->mandatory) . "</td>";
				$input .= "<td><textarea name=\"$input_name\" cols=\"90\" rows=\"$input_rows\">" . htmlentities($input_value,ENT_COMPAT,$this->encoding) . "</textarea>";
			}
			else
			{
				$input .= $this->tr->TranslateTry($input_descriz) . "</td>\n";
				$input .= "<td valign=\"top\" class=\"inactive\">" . nl2br( htmlspecialchars($input_value) );
			}
			$input .= $this->input_hidden("is_html","on");
			$input .= "</td></tr>\n";
		}
		else 
		{
			$input = $this->input_textarea($input_descriz,$input_name,$input_value,90,$input_rows,"",$input_right);
			$input .= $this->input_checkbox("no_html","is_html",$is_html,0,$input_right);
		}
		return $input;
	}

	public function input_time($input_descriz,$input_name,$input_value,$input_right)
	{
		$input_time = ($input_value>0)? getdate($input_value) : getdate();
		$hours = sprintf("%02s",$input_time['hours']);
		$minutes = sprintf("%02s",$input_time['minutes']);
		$input = "<tr id=\"finput-$input_name\"><td align=\"right\">" . $this->Wrap($this->tr->TranslateTry($input_descriz),"<b>","</b>",$this->mandatory) . "</td><td valign=middle>\n";
		if ($input_right==1)
		{
			$input .= "<input name=\"".$input_name."_h\" size=\"2\" value=\"$hours\">&nbsp;:&nbsp;";
			$input .= "<input name=\"".$input_name."_i\" size=\"2\" value=\"$minutes\"> (" . strtolower($this->tr->Translate("hour") . ":" . $this->tr->Translate("minutes")) . ")\n";
		}
		else
			$input .= "<span class=\"inactive\">$hours:$minutes</span>";
		$input .= "</td></tr>\n";
		return $input;
	}

	public function input_upload($input_descriz,$input_name,$input_size,$input_right)
	{
		$input = "";
		if ($input_right==1)
		{
			$input .= "<tr id=\"finput-$input_name\"><td class=\"input-label\">" . $this->tr->TranslateTry($input_descriz) . "</td>\n";
			$input .= "<td><input type=\"file\" name=\"".$input_name."[]\" size=\"$input_size\"> ";
			$input .= "</td></tr>\n";
		}
		return $input;
	}

	public function JavascriptWarning($level="normal")
	{
		$warning = "<script type=\"text/javascript\"></script>\n";
		$warning .= "<noscript>";
		$warning .= "<ul class=\"msg\"><li>" . $this->tr->Translate($level=="normal"?"javascript_disabled_warning":"javascript_disabled_warning2") . "</li></ul>";
		$warning .= "</noscript>";
		return $warning;
	}

	public function KeywordUse($id_keyword,$id_type)
	{
		$links = array();
		$links = $this->UseLink($id_keyword,$id_type);
		$ontotypes = $this->tr->Translate("onto_types");
		$name = $ontotypes[$id_type];
		if (count($links)>0)
		{
			$show_use = "<li>$name (" . count($links) . ")\n<ul>";
			foreach($links as $link)
				$show_use .= "<li>$link</li>\n";
			$show_use .= "</ul></li>";
		}
		return $show_use;
	}

	public function ListItem($label,$value,$check=false,$test_value="")
	{
		$return = "<li>$label: ";
		if(is_array($value))
		{
			if(count($value)>0)
			{
				$return .= "<ul>";
				foreach($value as $value2)
				{
					$return .= "<li>$value2</li>";
				}
				$return .= "</ul>";
			}
			else
				$return .= "-";
		}
		else 
		{
			$return .= "<em>$value</em>";
			if($check && $test_value!=$value)
				$return .= " <span class=\"warning\">[" . $this->tr->TranslateParams("should_be",array($test_value)) . "]</span>";
		}
		$return .= "</li>\n";
		return $return;
	}
	
	public function MessageShow($messages=array())
	{
		if(count($messages)>0)
			$msg_list = $messages;
		else 
		{
			include_once(SERVER_ROOT."/../classes/adminhelper.php");
			$ah = new AdminHelper;
			$msg_list = $ah->MessageGet();
		}
		$out = "";
		if (is_array($msg_list) && count($msg_list)>0)
		{
			$out .= "<ul class=\"msg\">";
			foreach($msg_list as $msg)
			{
				$out .= "<li>" . nl2br($msg) . "</li>\n";
			}
			$out .= "</ul>";
		}
		return $out;
	}

	private function Pagination($current_page,$tot_pages,$domain)
	{
		$pages_separator = " ";
		$max_pages = 10;
		$page_diff = 3;
		$uri = $_SERVER['REQUEST_URI'];
		$page_url = preg_replace("/[?|&]p=[0-9]*/","",$uri);
		$url_concat = (strpos($page_url,"?")>0)? "&" : "?";
	
		$pagination = $this->tr->Translate("page") . " ";
		for($i=1;$i<=$tot_pages;$i++)
		{
 			if ($tot_pages>$max_pages && (($i>$page_diff && $i<=($tot_pages-$page_diff)) && ($i<($current_page-$page_diff) || $i>($current_page+$page_diff))  ))
			{
				if (($i==$current_page+$page_diff+1) || ($i==$current_page-$page_diff-1))
					$pagination .= "..." . $pages_separator;
			}
			else
			{
				$pagination .= ($current_page!=$i)? "<a href=\"$domain".$page_url.$url_concat."p=".($i)."\">$i</a>":"<b>$i</b>";
				$pagination .= $pages_separator;
			}
		}
		
		return $pagination;
	}
		
	private function PaginationBottom($current_page,$tot_pages,$domain)
	{
		$previous = $this->tr->Translate("previous");
		$next = $this->tr->Translate("next");
		$uri = $_SERVER['REQUEST_URI'];
		$page_url = preg_replace("/[?|&]p=[0-9]*/","",$uri);
		$url_concat = (strpos($page_url,"?")>0)? "&" : "?";
		$pagination = $this->Wrap($previous,"<a href=\"$domain".$page_url.$url_concat."p=".($current_page-1)."\">","</a>",$current_page>1);
		$pagination .= " / " . $this->Wrap($next,"<a href=\"$domain".$page_url.$url_concat."p=".($current_page+1)."\">","</a>",$current_page<$tot_pages);
		return $pagination;
	}
	
	private function PageInfo($num,$current_page,$pages)
	{
		return $this->tr->TranslateParams("num_results",array($num));
	}
		
	private function PageInfoBottom($current_page,$pages)
	{
		$page_info = $this->tr->TranslateParams("paging",array($current_page,$pages));
		return $page_info;
	}
	
	public function RecordsPerPage($num)
	{
		$GLOBALS['records_per_page'] = $num;
		$this->records_per_page = $num;		
	}
	
	public function ResourceLink($id_res,$id_item)
	{
		switch($id_res)
		{
			case "1":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_item);
				$link .= "<a href=\"/topics/topic.php?id=$id_item\">" . $t->name . "</a>";
			break;
			case "2":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic(0);
				$subtopic = $t->SubtopicGet($id_item);
				$t = new Topic($subtopic['id_topic']);
				$link .= "$t->name - <a href=\"/topics/subtopic.php?id=$id_item&id_topic={$t->id}\">" . $subtopic['name'] . "</a>";
			break;
			case "3":
				include_once(SERVER_ROOT."/../classes/gallery.php");
				$g = new Gallery($id_item);
				$link .= "<a href=\"/galleries/gallery.php?id=$id_item\">" . $g->title . "</a>";
			break;
			case "4":
				include_once(SERVER_ROOT."/../modules/assos.php");
				$asso = new Asso($id_item);
				$org = $asso->Get(true);
				$link .= "<a href=\"/orgs/org.php?id=$id_item\">" . $org['nome'] . "</a>";
			break;
			case "5":
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($id_item);
				$article = $a->ArticleGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($article['id_topic']);
				$link .= "$t->name - <a href=\"/articles/article.php?w=topics&id=$id_item\">" . $article['headline'] . "</a>";
			break;
			case "7":
				include_once(SERVER_ROOT."/../classes/image.php");
				$im = new Image($id_item);
				$gallery = $im->Gallery();
				$g = new Gallery($gallery['id_gallery']);
				$image = $g->ImageGet($id_item);
				$this->th->StringCut($image['caption'],15);
				$link .= "<a href=\"/galleries/image.php?id=$id_item&id_gallery={$gallery['id_gallery']}\">" . $image['caption'] . "</a>";
			break;
			case "8":
				include_once(SERVER_ROOT."/../classes/event.php");
				$e = new Event();
				$event = $e->EventGet($id_item);
				$link .= "<a href=\"/events/event.php?id=$id_item\">" . $event['title'] . "</a>";
			break;
			case "9":
				include_once(SERVER_ROOT."/../classes/forum.php");
				$f = new Forum($id_item);
				$forum = $f->ForumGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($forum['id_topic']);
				$link .= "$t->name - <a href=\"/topics/forum.php?id=$id_item&id_topic={$t->id}\">" . $forum['name'] . "</a>";
			break;
			case "10":
				include_once(SERVER_ROOT."/../classes/forum.php");
				$f = new Forum(0);
				$thread = $f->ThreadGet($id_item);
				$f->id = $thread['id_topic_forum'];
				$forum = $f->ForumGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($forum['id_topic']);
				$link .= "$t->name - <a href=\"/topics/forum_thread.php?id_forum={$f->id}&id=$id_item&id_topic={$t->id}\">" . $thread['title'] . "</a>";
			break;
			case "11":
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($id_item);
				$campaign = $c->CampaignGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($campaign['id_topic']);
				$link .= "$t->name - <a href=\"/topics/campaign.php?id=$id_item&id_topic={$t->id}\">" . $campaign['name'] . "</a>";
			break;
			case "12":
				include_once(SERVER_ROOT."/../classes/quotes.php");
				$q = new Quotes();
				$quote = $q->QuoteGet($id_item);
				$link .= "<a href=\"/quotes/quote.php?id=$id_item\">" . $this->th->StringCut($quote['quote'],15) . "</a>";
			break;
			case "13":
				include_once(SERVER_ROOT."/../modules/books.php");
				$b = new Book($id_item);
				$book = $b->BookGet();
				$link .= "<a href=\"/books/book.php?id=$id_item\">" . $book['title'] . "</a>";
			break;
			case "14":
				include_once(SERVER_ROOT."/../classes/link.php");
				$l = new Link($id_item);
				$link = $l->LinkGet();
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($link['id_topic']);
				$link .= "$t->name - <a href=\"/links/link.php?id=$id_item&id_topic={$t->id}\">" . $link['title'] . "</a>";
			break;
			case "15":
				include_once(SERVER_ROOT."/../classes/event.php");
				$e = new Event();
				$event = $e->RecurringGet($id_item);
				$link .= "<a href=\"/events/recurring_event.php?id=$id_item\">" . $event['description'] . "</a>";
			break;
			case "18":
				include_once(SERVER_ROOT."/../modules/books.php");
				$b = new Book(0);
				$review = $b->ReviewGet($id_item);
				$link .= "<a href=\"/books/review.php?id=$id_item&id_book={$review['id_book']}\">" . $this->th->StringCut($review['review'],20) . "</a>";
			break;
			case "19":
				include_once(SERVER_ROOT."/../classes/polls.php");
				$pl = new Polls();
				$poll = $pl->PollGet($id_item);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($poll['id_topic']);
				$link .= "$t->name - <a href=\"/topics/poll.php?id=$id_item&id_topic={$t->id}\">" . $poll['title'] . "</a>";
			break;
			case "20":
				include_once(SERVER_ROOT."/../classes/polls.php");
				$pl = new Polls();
				$question = $pl->QuestionGet($id_item);
				$poll = $pl->PollGet($question['id_poll']);
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($forum['id_topic']);
				$link .= "$t->name - <a href=\"/topics/poll_question.php?id_poll={$question['id_poll']}&id=$id_item\">" . $question['question'] . "</a>";
			break;
		}
		$link = "<p>$link</p>\n";
		return $link;
	}
	
	public function ServiceStatus($service_name)
	{
	    include_once(SERVER_ROOT."/../classes/services.php");
	    $se = new Services();
	    $service_status = $se->ServiceStatusGet($service_name);
	    if($service_status['last_update_ts']>0)
	    {
	        $status = "<h3>$service_name STATUS</h3>";
	        $status .= "<ul><li>" . $this->FormatDateTime($service_status['last_update_ts']) . ": <strong>" . $this->Bool2UpDown($service_status['status'],false) . "</strong>";
	        if($service_status['notes']!="")
	            $status .= "<div class=\"notes\">" . nl2br($service_status['notes']) . "</div>";
	        $status .= "</li></ul>";    
	    }
		return $status;
	}
	
	public function ShowTable( $rows, $titles, $content, $num, $domain="")
	{
		$hhf = new HHFunctions;
		$records_per_page = $this->records_per_page;
		$show_table = "<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\">\n";
		$num_columns = count($titles);

		if ($num>$records_per_page)
		{
			$page = $GLOBALS['current_page'];
			$pages = floor(($num-1)/$records_per_page) + 1;
			$counter = (($page-1)*$records_per_page) + 1;
			$show_table .= "<tr><td colspan=" . ($num_columns+1) . " class=\"pager\">";
			$show_table .= $this->PageInfo($num,$page,$pages) . " - " . $this->Pagination($page,$pages,$domain);
			$show_table .= "</td></tr>\n";
		}
		else
			$show_table .= "<tr><td colspan=\"$num_columns\" align=\"right\">" . $this->tr->TranslateParams("num_results",array($num)) . "</td></tr>";
		if (count($rows)>0)
		{
			$show_table .= "<tr>";
			if ($num>$records_per_page)
				$show_table .= "<th>&nbsp;</th>";
			foreach ($titles as $title)
				$show_table .= "<th>" . strtolower($this->tr->TranslateTry($title)) . "</th>\n";
			$show_table .= "</tr>";
			$i = 1;
			foreach($rows as $row)
			{
				if (($i%2)==1)
					$show_table .= "<tr class=\"row0\">";
				else
					$show_table .= "<tr class=\"row1\">";
				if ($num>$records_per_page)
				{
					$show_table .= "<td valign=\"top\" class=\"counter\">$counter.</td>";
					$counter = $counter + 1;
				}
				foreach ($content as $con)
				{
					$str = "";
					if ((strpos($con,"{"))===0)
					{					
						$function = preg_replace('[{|}]','',$con);
						// ??
						$function = preg_replace('/(\\\$row\[[0-9]\])/',"\\1",$function);
						// add single quotes for array element
						$function = preg_replace('/\$row\[([^\]]*)\]/',"{\$row['$1']}",$function);
						// remove curly brackets
						$function = preg_replace('/([\(|\*|\s?,])\{\$row/',"$1\$row",$function);
						$function = preg_replace('/\]\}([\s?,|\)])/',"]$1",$function);
						// echo("$function<br>");
						eval("\$str = \$hhf->$function;");
						$show_table .= "<td valign=\"top\">" . $str . "</td>";
					}
					else
					{
						eval ("\$str = \"$con\";");
						$show_table .= "<td valign=\"top\">$str</td>";
					}
				}
				$show_table .= "</tr>\n";
				$i++;
			}
			if ($num>$records_per_page)
			{
				$show_table .= "<tr><td colspan=\"" . ($num_columns+1) . "\" class=\"counter\">";
				$show_table .= $this->PageInfoBottom($page,$pages) . " - ";
				$show_table .= $this->PaginationBottom($page,$pages,$domain);
				$show_table .= "</td></tr>\n";
			}
		}
		$show_table .= "</table>\n";
		return $show_table;
	}

	public function ShowTitle($titles)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$t_modules = $this->tr->Translate("modules_names");
		$module_name = $t_modules[($session->Get('module_id'))];
		$path = "<div class=\"breadcrumb\">";
		$uri = pathinfo($_SERVER['REQUEST_URI']);
		if ($uri=="index.php")
			$path .= $module_name;
		else
			$path .= "<a href=\"/" . $session->Get("module_path") . "/index.php\">$module_name</a>";
		if (count($titles)>0)
		{
			foreach($titles as $title)
			{
				$this->th->TextClean($title[0],false);
				$this->th->StringCut($title[0],30);
				$path .= " / ";
				$tr_title = $this->tr->TranslateTry($title[0]);
				$path .= $this->Wrap(ucfirst($tr_title),"<a href=\"$title[1]\">","</a>",$title[1]!="");
			}
		}
		$path .= "</div>\n";
		$path .= $this->MessageShow();
		return $path;
	}

	public function Toolbar($tools)
	{
		if (count($tools)>0)
		{
			$tb = "<ul id=\"toolbar\">";
			foreach($tools as $tool)
			{
				$tb .= "<li><a href=\"$tool[1]\"";
				if($tool[0]=="preview")
					$tb .= " target=\"_blank\"";
				$tb .= ">" . $this->tr->TranslateTry($tool[0]) . "</a></li>\n";
			}
			$tb .= "</ul>\n";
		}
		return $tb;
	}

	public function show_audio($id_audio,$download)
	{
		$video = "<tr><td class=\"input-label\">audio</td><td><div id=\"audio_$id_audio\">MP3</div>";
		$video .= "/images/upload.php?src=audios/enc/{$id_audio}.mp3";
		$video .= "</td></tr>\n";
		return $video;
	}

	public function show_banner($id_banner,$width,$height,$extension)
	{

		$show_graphic = "<tr><td class=\"input-label\">banner</td><td>";
		$filename = "/images/upload.php?src=banners/orig/{$id_banner}.{$extension}";
		$show_graphic .= $this->Graphic($filename,$width,$height,false,$id_banner);
		$show_graphic .= "</td></tr>\n";
		return $show_graphic;
	}

	public function show_graphic($filename,$width,$height,$ext)
	{
		$show_graphic = "<tr><td class=\"input-label\">" . $this->tr->Translate("image") . "<br><br><font size=1>({$width}x{$height})</font></td><td>";
		$filename = "/images/upload.php?src=graphics/orig/$filename";
		$show_graphic .= $this->Graphic($filename,$width,$height,false);
		$show_graphic .= "</td></tr>\n";
		return $show_graphic;
	}
	
	public function show_video($id_video,$width,$height,$image_seq,$logo="")
	{
		$video = "<tr><td class=\"input-label\">video</td><td><div id=\"video_$id_video\">FLV</div>";
		$video .= "/images/upload.php?src=videos/flv/{$id_video}.flv";
		$video .= "</td></tr>\n";
		return $video;
	}

	public function ShowCommentsTree($obj,$id_root,$url)
	{
		$children = $obj->Children($id_root);
		if(count($children)>0)
		{
			$tree = "<ul class=\"tree\">\n";
			foreach($children as $child)
			{
				$tree .= "<li>";
				$tree .= "<a href=\"$url&id={$child['id_comment']}\">{$child['title']}</a>";
				$tree .= "<div class=\"subitems\">" . $this->FormatDateTime($child['date']) . " - {$child['author']}</div>";
				$tree .= $this->ShowCommentsTree($obj,$child['id_comment'],$url);
				$tree .= "</li>";
			}
			$tree .= "</ul>\n";
		}
		return $tree;
	}
	
	public function ShowOntoTree($items,$link,$with_checkboxes=false,$current_values=array())
	{
		$out = "<ul>\n";
		foreach($items as $item)
		{
			$out .= "<li>\n" . $item['approved'];
			if($with_checkboxes)
			{
				$out .= "<input type=\"checkbox\" name=\"keyword[]\" value=\"{$item['id_keyword']}\"";
				if (in_array($item['id_keyword'],$current_values))
					$out .= " checked ";
				$out .= ">";
			}
			$out .= "<a href=\"$link{$item['id_keyword']}\">{$item['keyword']}</a>";
			if(is_array($item['sub']))
			{
				$out .= $this->ShowOntoTree($item['sub'],$link,$with_checkboxes,$current_values);
			}
			$out .= "</li>\n";
		}
		$out .= "</ul>\n";
		return $out;
	}
	
	public function ShowSubtopicsTree($id_root)
	{
		$this->topic->LoadTree();
		$children = $this->topic->SubtopicChildren($id_root);
		$subtypes = $this->tr->Translate("subtopic_types");
		$t_modules = $this->tr->Translate("modules_names");
		$stree = '';
		if (count($children)>0)
		{
			$stree .= "<ul class=\"tree subtopics\">\n";
			foreach($children as $child)
			{
				$stree .= "<li style=\"list-style-image: url(" . $this->SubtopicIcon($child['id_type'],$child['id_item'],false,$child['visible'],true) . ");\">";
				if ($child['id_type']>0)
				{
					if ($child['seq']>2 || ($child['seq']>1 && $id_root>0))
						$stree .= "<a href=\"actions.php?from2=subtopic&id_topic={$this->topic->id}&id={$child['id_subtopic']}&action3=up\">{$this->img_arrow_up_on}</a>";
					else
						$stree .= $this->img_arrow_up_off;
					if ($child['seq'] < count($children))
						$stree .= "<a href=\"actions.php?from2=subtopic&id_topic={$this->topic->id}&id={$child['id_subtopic']}&action3=down\">{$this->img_arrow_down_on}</a>";
					else
						$stree .= $this->img_arrow_down_off;
					$stree .= "&nbsp;";
					$stree .= ($child['visible']==2)? "(nolink) " : "";
					$stree .= ($child['visible']==3)? "(inv) " : "";
					$stree .= ($child['visible']==4)? "(temp) " : "";
					$stree .= "<a href=\"subtopic.php?id={$child['id_subtopic']}&id_topic={$this->topic->id}\">".$this->StringNull2Dashes($child['name'])."</a>";
				}
				else
					$stree .= $child['name'];
				$type = $child['id_type']=="12"? htmlspecialchars($this->tr->Translate("module") . " " . $t_modules[$child['id_item']]) : $subtypes[$child['id_type']];
				$stree .= $this->SubtopicItems($child['id_subtopic'],$child['id_type'],$type,$child['sort_by'],$child['visible']);
				$stree .= "</li>\n";
				$stree .= $this->ShowSubtopicsTree($child['id_subtopic']);
			}
			$stree .= "</ul>\n";
		}
		return $stree;
	}

	public function ShowSubtopicsTreeForHomepage($id_root, $id_subtopic)
	{
		$this->topic->LoadTree();
		$children = $this->topic->SubtopicChildren($id_root);
		$tree = "<dl>\n";
		foreach($children as $child)
		{
			if ($child['visible']<3 && 
			($child['id_type']==$this->topic->subtopic_types['article'] || $child['id_type']==$this->topic->subtopic_types['folder'] || $child['id_type']==$this->topic->subtopic_types['latest'] || $child['id_type']==$this->topic->subtopic_types['map']))
			{
				$tree .= "<dt><dd>";
				$tree .= $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']);
				if ($child['id_subtopic']==$id_subtopic)
					$tree .= "<b>" . $child['name'] . "</b>";
				else
					$tree .= "<a href=\"actions.php?from2=homepage&id_topic=".$this->topic->id."&id=".$child['id_subtopic']."&action3=add\">" . $child[name] . "</a>";
				$tree .= $this->ShowSubtopicsTreeForHomepage($child['id_subtopic'], $id_subtopic);
				$tree .= "</dd></dt>\n";
			}
		}
		$tree .= "</dl>\n";
		return $tree;
	}

	public function ShowSubtopicsTreeForArticle($id_root, $id_subtopic)
	{
		$children = $this->topic->SubtopicChildren($id_root);
		$tree = "<dl>\n";
		$hhf = new HHFunctions();
		$hhf->topic = $this->topic;
		foreach($children as $child)
		{
			if ($child['visible']!="4" || !($this->topic->only_visible))
			{
				$tree .= "<dt><dd>";
				if ($child['id_subtopic']==$id_subtopic)
					$tree .= $this->SubtopicIcon( $child['id_type'], $child['id_item'], true, $child['visible']) . "<b>" . $child['name'] . "</b>";
				else
				{
					$path = $hhf->PathToSubtopic($this->topic->id,$child['id_subtopic'],20);
					$tree .= $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']);
					$tree .= $this->Wrap($child['name'],"<a href=\"#\" onClick=\"set_subtopic('".$child['id_subtopic']."','". $this->th->TextReplaceForJavascript($path) ."');\">","</a>",$child['id_type']<3);
				}
				$tree .= $this->ShowSubtopicsTreeForArticle($child['id_subtopic'], $id_subtopic);
				$tree .= "</dd></dt>\n";
			}
		}
		$tree .= "</dl>\n";
		return $tree;
	}

	public function ShowSubtopicsTreeForLink($id_root, $id_subtopic)
	{
		$children = $this->topic->SubtopicChildren($id_root);
		$tree = "<dl>\n";
		$hhf = new HHFunctions();
		$hhf->topic = $this->topic;
		foreach($children as $child)
		{
			if ($child['id_type']==1 || $child['id_type']==2 || $child['id_type']==11)
			{
				$tree .= "<dt><dd>";
				if ($child['id_type']==11)
				{
					if ($child['id_subtopic']==$id_subtopic)
						$tree .= $this->SubtopicIcon( $child['id_type'],$child['id_item'], true, $child['visible']) . "<b>" . $child['name'] . "</b>";
					else
					{
						$path = $hhf->PathToSubtopic($this->topic->id,$child['id_subtopic'],20);
						$tree .= $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']) .
						"<a href=\"#\" onClick=\"set_subtopic('".$child['id_subtopic']."','". $this->th->TextReplaceForJavascript($path) ."');\">" . $child['name'] . "</a>";
					}
				}
				else
					$tree .= $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']) . $child['name'];
				$tree .= $this->ShowSubtopicsTreeForLink($child['id_subtopic'], $id_subtopic);
				$tree .= "</dd></dt>\n";
			}
		}
		$tree .= "</dl>\n";
		return $tree;
	}

	public function ShowSubtopicsTreeForSubtopic($id_root, $id_subtopic, $id_parent, $id_first_subtopic)
	{
		$children = $this->topic->SubtopicChildren($id_root);
		$hhf = new HHFunctions();
		$hhf->topic = $this->topic;
		$tree = "<dl class=\"tree\">\n";
		foreach($children as $child)
		{
			if ($child['id_type']>0 && $child['id_type']!="6")
			{
				$tree .= "<dt><dd>" . $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']);
				if ($child['id_subtopic']==$id_subtopic)
					$tree .= "<u>" . $child['name'] . "</u>";
				else
				{
					if ($child['id_subtopic']==$id_parent)
						$tree .= "<b>" . $child['name'] . "</b>";
					elseif ($id_first_subtopic>0 && $this->topic->SubtopicIsChild($child['id_subtopic'],$id_subtopic))
						$tree .= "<font color=\"#888888\">" . $child['name'] . "</font>";
					else
					{
						$path = $hhf->PathToSubtopic($this->topic->id,$child['id_subtopic'],20);
						$tree .= "<a href=\"#\" onClick=\"set_subtopic('".$child['id_subtopic']."','". $this->th->TextReplaceForJavascript($path) ."');\">" . $child['name'] . "</a>\n";
					}
				}
			}
			else
			{
				$tree .= "<dt><dd>" . $this->SubtopicIcon( $child['id_type'], $child['id_item'], false, $child['visible']);
				$tree .=  "<font color=\"#888888\">" . $child['name'] . "</font>";
			}
			$tree .= $this->ShowSubtopicsTreeForSubtopic($child['id_subtopic'], $id_subtopic, $id_parent, $id_first_subtopic);
			$tree .= "</dd></dt>\n";
		}
		$tree .= "</dl>\n";
		return $tree;
	}

	public function ShowSubtopicsTreeForXml($id_root)
	{
		$this->topic->LoadTree();
		$children = $this->topic->SubtopicChildren($id_root);
		$stree = "<ul class=\"tree\">\n";
		foreach($children as $child)
		{
			$stree .= "<li style=\"list-style-image: url(" . $this->SubtopicIcon($child['id_type'],$child['id_item'],false,$child['visible'],true) . ");\">";
			if ($child['id_type']>0)
			{
				$stree .= "&nbsp;";
				$stree .= ($child['visible']==2)? "(nolink) " : "";
				$stree .= ($child['visible']==3)? "(inv) " : "";
				$stree .= ($child['visible']==4)? "(temp) " : "";
				$stree .= "<a href=\"xml.php?id_type=2&id={$child['id_subtopic']}&id_topic={$this->topic->id}\">".$this->StringNull2Dashes($child['name'])."</a> [XML]";
			}
			else
				$stree .= $child['name'];
			$stree .= "</li>\n";
			$stree .= $this->ShowSubtopicsTreeForXml($child['id_subtopic']);
		}
		$stree .= "</ul>\n";
		return $stree;
	}

	public function String2Url($text_value)
	{
		$ret = $text_value;
		if ($ret!="")
		{
			if ((strpos($ret,'://')===false))
				$ret = "http://" . $ret;
		}
		return "<a href=\"$ret\" target=\"_blank\">$ret</a>";
	}

	public function String2Mailto($text_value)
	{
		return "<a href=\"mailto:$text_value\">$text_value</a>";
	}

	public function StringNull2Dashes( $string )
	{
		return ($string!="")? $string : "---";
	}

	public function SubtopicIcon( $id_type, $id_item, $open, $visible="1", $url_only=false )
	{
		$icon = "/images/bullet" . $id_type . (($visible=="1")? "" : "i" ) . (($open)? "_s" : "" ) . (($id_type==12)? "_m$id_item":"") .".gif";
		if(!$url_only)
			$icon = "<img src=\"$icon\" width=\"22\" height=\"18\" />";
		return $icon;
	}

	private function SubtopicItems($id_subtopic, $subtopic_type, $type, $sort_by, $visible)
	{
		$subvisible = $this->tr->Translate("subtopic_visibility");
		$items = $this->topic->CountSubtopicItems($id_subtopic, $subtopic_type);
		$countSubtopic = "<div class=\"subitems\">";
		if($subtopic_type>0)
			$countSubtopic .= $this->tr->Translate("type") . ": $type";
		if(($subtopic_type>0 && $subtopic_type<=3) || $subtopic_type==11)
			$countSubtopic .= " - " . $this->tr->Translate("content") . ": ";
		if($subtopic_type==0)
			$countSubtopic .= "<a href=\"articles_search2.php?id_topic={$this->topic->id}&id_subtopic=$id_subtopic&approved=-1\">$items art.</a>";
		if ($subtopic_type<3 && $subtopic_type>0)
		{
			if ($items>0)
				$countSubtopic .= "<a href=\"articles_search2.php?id_topic={$this->topic->id}&id_subtopic=$id_subtopic&sort_by=$sort_by&id_template=-1\">$items art.</a>";
			else
				$countSubtopic .= "$items art.";
		}
		if ($subtopic_type==3)
			$countSubtopic .= "$items art.";
		if ($subtopic_type==11)
		{
			if ($items>0)
				$countSubtopic .= "<a href=\"links_search2.php?id_topic={$this->topic->id}&id_subtopic=$id_subtopic\">$items links</a>";
			else
				$countSubtopic .= "$items links";
		}
		if($visible!="1")
			$countSubtopic .= " - " .$subvisible[$visible];
		$countSubtopic .= "</div>\n";
		return $countSubtopic;
	}
	
	public function Stop()
	{
		include_once(SERVER_ROOT."/include/footer.php");
		exit;
	}
	
	public function Tabs($tabs)
	{
		$path = "<div><ul class=\"tabs\">";
		if (count($tabs)>0)
			foreach($tabs as $tab)
			{
				$tr_tab = $this->tr->TranslateTry($tab[0]);
				$path .= "<li";
				if($tab[2])
					$path .= " class=\"selected\" ";
				$path .= ">" . $this->Wrap($tr_tab,"<a href=\"$tab[1]\">","</a>",$tab[1]!="") . "</li>";
			}
		$path .= "</ul></div>\n";
		return $path;
	}
	
	public function TopicLoad($id_topic)
	{
		if (!isset($this->topic) || !count($this->topic->tree)>0 || $id_topic!=$this->topic->id)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$this->topic = new Topic($id_topic);
			$this->topic->LoadTree();
		}
	}
	
	private function UseLink($id_keyword,$id_type)
	{
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$rows = $k->TypeUse($id_keyword,$id_type);
		$use = array();
		switch ($id_type)
		{
			case 1:
				foreach($rows as $row)
				{
					$use[] = "<a href=\"/topics/topic.php?id={$row['id_topic']}\">{$row['name']}</a> ({$row['counter']} articoli)<div>{$row['description']}</div>";
				}
			break;
			case 2:
				foreach($rows as $row)
				{
					$use[] = "{$row['topic_name']}: <a href=\"/topics/subtopic.php?id={$row['id_subtopic']}&id_topic={$row['id_topic']}\">{$row['name']}</a> ({$row['counter']} articoli)";
				}
			break;
			case 3:
				foreach($rows as $row)
				{
					$use[] = "<a href=\"/galleries/gallery.php?id={$row['id_gallery']}\">{$row['title']}</a>";
				}
			break;
			case 4:
				foreach($rows as $row)
				{
					$use[] = "<a href=\"/orgs/search.php?id_k=$id_keyword\">{$row['counter']} associazioni</a>";
				}
			break;
			case 5:
				foreach($rows as $row)
				{
					$use[] = "{$row['name']}: <a href=\"/articles/article.php?id={$row['id_article']}&w=topics\">" . $this->th->StringCut($row['headline'],50) . "</a>";
				}
			break;
			case 6:
				foreach($rows as $row)
				{
					$use[] = "{$row['name']}: <a href=\"/articles/doc.php?id={$row['id_doc']}\">{$row['title']}</a>";
				}
			break;
			case 7:
				foreach($rows as $row)
				{
					if ($row['type'] == "article")
					{
						$use[] = "{$row['name']}: <a href=\"/articles/image.php?id={$row['id_image']}&id_article={$row['id_article']}&w=topics\">{$row['headline']}</a>";
					}
					if ($row['type'] == "gallery")
					{
						include_once(SERVER_ROOT."/../classes/gallery.php");
						if ($row['id_gallery'] > 0)
						{
							$g = new Gallery($row['id_gallery']);
							$pos = $g->ImagePosition($row['id_image']);
							$use[] = "Galleria {$row['title']}: <a href=\"/galleries/image.php?id={$row['id_image']}&id_gallery={$row['id_gallery']}\">Immagine n. $pos</a>";
						}
						else
						{
							$use[] = "Orfana: <a href=\"/articles/image.php?id={$row['id_image']}\">Immagine id {$row['id_image']}</a>";
						}
					}
				}
			break;
			case 8:
				foreach($rows as $row)
				{
					$use[] = $this->FormatDate($row['start_date_ts']) . " <a href=\"/events/event.php?id={$row['id_event']}\">{$row['title']}</a>";
				}
			break;
			case 9:
				foreach($rows as $row)
				{
					$use[] = "{$row['topic_name']}: <a href=\"/topics/forum.php?id={$row['id_topic_forum']}&id_topic={$row['id_topic']}\">{$row['name']}</a>";
				}
			break;
			case 10:
				foreach($rows as $row)
				{
					$use[] = "{$row['topic_name']} - {$row['name']}: <a href=\"/topics/forum_thread.php?id_forum={$row['id_topic_forum']}&id_topic={$row['id_topic']}&id={$row['id_thread']}\">" . $this->th->StringCut($row['title'],50) . "</a>";
				}
			break;
			case 11:
				foreach($rows as $row)
				{
					$use[] = "{$row['topic_name']}: <a href=\"/topics/campaign.php?id={$row['id_topic_campaign']}&id_topic={$row['id_topic']}\">{$row['name']}</a>";
				}
			break;
			case 12:
				foreach($rows as $row)
				{
					$use[] = "<a href=\"/quotes/quote.php?id={$row['id_quote']}\">" . $this->th->StringCut($row['quote'],50) . "</a>";
				}
			break;
			case 13:
				foreach($rows as $row)
				{
					$use[] = "{$row['author']}: <a href=\"/books/book.php?id={$row['id_book']}\">{$row['title']}</a>";
				}
			break;
			case 14:
				foreach($rows as $row)
				{
					$use[] = "{$row['topic_name']}: <a href=\"/topics/link.php?id={$row['id_link']}&id_topic={$row['id_topic']}\">{$row['title']}</a>";
				}
			break;
			case 15:
				foreach($rows as $row)
				{
					$use[] = "{$row['r_day']}.{$row['r_month']}.{$row['r_year']}: <a href=\"/events/recurring_event.php?id={$row['id_event']}\">" . $this->th->StringCut($row['description'],50) . "</a>";
				}
			break;
		}
		return $use;
	}

	public function Wrap($string,$open,$close,$condition=true)
	{
		$string = $this->tr->TranslateTry($string);
		if ($condition)
			$wrap = $open . $string . $close;
		else
			$wrap = $string;
		return $wrap;
	}

	public function WrapTag($string,$tag,$condition=true)
	{
		return $this->Wrap($string,"<$tag>","</$tag>",$condition);

	}

}

class HHFunctions extends HtmlHelper
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function AccountType($id_type)
	{
		$account_types = $this->tr->Translate("account_types");
		return $account_types[$id_type];
	}

	public function ArtStatus($num)
	{
		if ($num<0)
			$status = $this->tr->Translate("at_work");
		else
			$status = $this->Bool2YN($num);
		return $status;
	}

	public function Bool2YN($bool,$url="")
	{
		return "<div class=\"center\">" . (($url!="")?"<a href=\"$url\">":"") . strtoupper((($bool=="1")? $this->tr->Translate("yes") : $this->tr->Translate("no"))) . (($url!="")?"</a>":"") . "</div>";
	}

	public function BoxType($id_type)
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles();		
		$box = $s->BoxesTypeGet($id_type);
		return $box['name'];
	}

	public function ContactEmail($email,$consent)
	{
		return ($consent)? "<a href=\"mailto:$email\">$email</a>" : $email;
	}
	
	public function DeSer($serialized_array,$key)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$deparams = $v->Deserialize($serialized_array);
		return $deparams[$key];
	}

	public function Escape($text)
	{
		return htmlspecialchars($text);
	}
	
	public function FormatLength($seconds,$align=true)
	{
		$length = "?";
		if($seconds>0)
		{
			$work_time = getdate($seconds);
			$length = sprintf("%02d:%02d",$work_time['minutes'],$work_time['seconds']);
		}
		return $align? "<div class=\"right\">$length</div>" : $length;
	}
	
	public function FormatMoney( $amount, $id_currency, $wrap=true)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$currencies = $p->currency_symbols;
		return $this->Wrap($currencies[$id_currency] . " " . number_format($amount),"<div class=\"right\">","</div>",$wrap);
	}

	public function FormatNumber($number,$decimals,$unit="")
	{
		return "<div class=\"right\">" . number_format($number,$decimals) . ($unit!=""? " $unit":"") ."</div>";
	}
	
	public function FormatSize($bytes)
	{
		return $bytes>0? $this->FormatNumber(floor($bytes/1024),0,"Kb") : "";
	}
	
	public function FormType($id_type)
	{
		$form_types = $this->tr->Translate("form_types");
		return $form_types[$id_type];
	}

	public function GeoLookup($id_geo,$id_geo_group=0)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		return $geo->GeoGet($id_geo,$id_geo_group);
	}
	
	public function GeoOption($id_geo_group)
	{
		$options = $this->tr->Translate("geo_options");
		return $options[$id_geo_group];
	}
	
	public function HistoryAction( $id_action )
	{
		include_once(SERVER_ROOT."/../classes/history.php");
		$h = new History;
		return array_search($id_action,$h->actions);
	}

	public function Htmlize($text,$is_html)
	{
		return $is_html? $this->th->Htmlise($text,false) : $text;
	}
	
	public function IndexWeightLookup($relevance)
	{
		$conf = new Configuration();
		return array_search($relevance,$conf->Get("indexer_weights"));
	}

	public function KeywordsGet($id, $id_type)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$keywords = array();
		$o->GetKeywords($id,$id_type,$keywords,0,true);
		return $this->th->ArrayMulti2StringIndex($keywords,'keyword');
	}
	
	public function KeywordsAdd($type,$id,&$keywords)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->GetKeywords($id,$o->types[$type],$keywords,0,true);
	}

	public function KeywordType($id_type)
	{
		$types = $this->tr->Translate("keyword_types");
		return $types[$id_type];
	}
	
	public function LanguageLookup($id_language)
	{
		$languages = $this->tr->Translate("languages");
		return $languages[$id_language];
	}
	
	public function LinkImage($url,$id_image,$size,$format,$type="images")
	{
		return "<a href=\"$url\"><img src=\"/images/upload.php?src={$type}/$size/{$id_image}.{$this->i->convert_format}&format=$format\" border=\"0\" alt=\"Img $id_image\"></a>";
	}
	
	public function LinkTitle($url,$title,$max_length=0,$description="",$condition=true,$css_class="")
	{
		$this->th->TextClean($title,false);
		if($max_length>0)
			$this->th->StringCut($title,$max_length);
		if(strlen(trim($title))==0)
			$title = "-----";
		if($condition)
		{
			if($css_class!="")
				$lt = "<div class=\"$css_class\"><a href=\"$url\">$title</a></div>";
			else 
				$lt = "<a href=\"$url\">$title</a>";
		}
		else
			$lt =  $title;
		if($description!="")
			$lt .= "<div>$description</div>";
		return $lt;
	}
	
	public function MailjobInfo($id_module,$id_item)
	{
		$t_modules = $this->tr->Translate("modules_names");
		$module = $t_modules[$id_module];
		if($id_module==8)
		{
			$mj_types = array('___','Indirizzi email delle risorse');
			$info = $module . " - " . $mj_types[$id_item];
		}
		if($id_module==4)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_item);
			$info = $t->name;
		}
		if($id_module==5)
		{
			include_once(SERVER_ROOT."/../classes/campaign.php");
			$c = new Campaign($id_item);
			$row = $c->CampaignGet();
			$info = $this->tr->Translate("campaigns") . ": " . $row['name'];
		}
		if($id_module==2)
		{
			include_once(SERVER_ROOT."/../classes/forum.php");
			$f = new Forum($id_item);
			$row = $f->ForumGet();
			$info = $this->tr->Translate("forum") . ": " . $row['name'];
		}
		if($id_module==3)
		{
			include_once(SERVER_ROOT."/../classes/polls.php");
			$pl = new Polls();
			$row = $pl->PollGet($id_item);
			$info = $this->tr->Translate("poll") . ": " . $row['title'];
		}
		if($id_module==28)
		{
			$info = $this->tr->Translate("visitors");
		}
		return $info;
	}

	public function ModuleLink($module,$id_module,$id_topic)
	{
		$t_modules = $this->tr->Translate("modules_names");
		$module_name = $t_modules[$id_module];
		switch($module)
		{
			case "books":
				$link = "$module_name<ul>";
				include_once(SERVER_ROOT."/../modules/books.php");
				$bb = new Books();
				foreach($bb->subtypes as $subtype)
					$link .= "<li><a href=\"xml.php?id_type=0&id_topic=$id_topic&module=$module&subtype=" . $subtype . "\">$subtype</a> [XML]</li>\n";
				$link .= "</ul>";
			break;
			default:
				$link = "<a href=\"xml.php?id_type=0&id_topic=$id_topic&module=$module&id_module=$id_module\">$module_name</a> [XML]\n";
			break;
		}
		return $link;
	}
	
	public function Money( $amount, $decimals=0 )
	{
		return "<div class=\"right\">" . number_format($amount,$decimals) . "</div>";
	}

	public function MoneyCurrency( $amount, $id_currency, $is_in=2 )
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$currencies = $p->currencies;
		if($is_in==2)
			return "<div class=\"right\"><nobr>$amount {$currencies[$id_currency]}</nobr></div>";
		else
			return "<div class=\"right\"><nobr>" .  ($is_in=="1"? "+":"-")  .  " $amount {$currencies[$id_currency]}</nobr></div>";
	}

	public function Nl2br($string)
	{
		return nl2br($string);
	}

	public function Notags($string)
	{
		return strip_tags($string);
	}

	public function Null2String($value,$string)
	{
		if ($value!="")
			$return_value = $value;
		else
			$return_value = $string;
		return $return_value;
	}
	
	public function OrderStatus($id_status) 
	{ 
		$trm27 = new Translator($this->tr->id_language,27); 
		$statuses = $trm27->Translate("order_status"); 
		return $statuses[$id_status]; 
	} 	

	public function PageFunction($id_function)
	{
		$functions = $this->tr->Translate("page_functions");
		return $functions[$id_function];
	}

	public function PageType($id_type,$id_module)
	{
		if($id_module>0 && $id_type==0)
		{
			$t_modules = $this->tr->Translate("modules_names");
			$pagetype = $t_modules[$id_module];			
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/pagetypes.php");
			$pt = new PageTypes();
			$pagetype = array_search($id_type,$pt->types); 
		}
		return $pagetype;
	}

	public function PageTypeGlobal($id_type)
	{
		include_once(SERVER_ROOT."/../classes/pagetypes.php");
		$pt = new PageTypes();
		return array_search($id_type,$pt->gtypes);
	}

	public function PageTypeLink($name,$id_type,$id_topic)
	{
		if ($id_type==0 || $id_type==2 || $id_type==3 || $id_type==14 || $id_type==15)
			$link = "<a href=\"xml_choose.php?id_topic=$id_topic&id_type=$id_type\">$name</a>\n";
		else
		{
			include_once(SERVER_ROOT."/../classes/db.php");
			$pt = new PageTypes();
			if (is_array($pt->subtypes[$id_type]))
			{
				$link = "$name<ul>";
				foreach($pt->subtypes[$id_type] as $subtype)
					$link .= "<li><a href=\"xml.php?id_topic=$id_topic&id_type=$id_type&subtype=" . $subtype . "\">$subtype</a> [XML]</li>\n";
				$link .= "</ul>";
			}
			else
				$link = "<a href=\"xml.php?id_topic=$id_topic&id_type=$id_type\">$name</a> [XML]\n";
		}
		return $link;
	}
	
	public function PathToGallery($id_group, $id_gallery)
	{
		if ($id_gallery>0)
		{
			$this->groups = new Galleries();
			$path = $this->PathToGroup($id_group);
		}
		return $path;
	}

	public function PathToGroup($id_group)
	{
		$this->groups->gh->LoadTree();
		$parents = array();
		$this->groups->gh->th->GroupParents($id_group, $parents);
		$parents = array_reverse($parents);
		$counter = 0;
		foreach($parents as $parent)
		{
			if ($counter > 0)
				$path .= " /<br>";
			$path .= "<nobr>" . $parent['name'] . "</nobr>";
			$counter ++;
		}
		return $path;
	}

	public function PathToSubtopic($id_topic, $id_subtopic, $max_length=0, $show_topic=0)
	{
		$path = "";
		if ($id_subtopic>0)
		{
			$this->TopicLoad($id_topic);
			if($show_topic==1) {
				$path = "<b>{$this->topic->name}</b><br>";
			}
			$parents = array();
			$this->topic->SubtopicParents($id_subtopic, $parents);
			$parents = array_reverse($parents);
			$counter = 0;
			foreach($parents as $parent)
			{
				if ($counter > 0)
					$path .= " / <br>";
				if ($max_length>0)
					$this->th->StringCut($parent['name'],$max_length);
				$path .= "<nobr>".$parent['name']."</nobr>";
				$counter ++;
			}
		}
		if ($max_length>0)
			$path = strip_tags($path);
		return $path;
	}

	public function PathToTopic($id_group, $id_topic)
	{
		if ($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topics.php");
			$this->groups = new Topics();
			$path = $this->PathToGroup($id_group);
		}
		return $path;
	}

	public function PaymentOrigin($id_use)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$use = $p->AccountUseGet($id_use);
		switch($use['type'])
		{
			case "1":
				include_once(SERVER_ROOT."/../classes/campaign.php");
				$c = new Campaign($use['id_item']);
				$row = $c->CampaignGet();
				$id_topic = $row['id_topic'];
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$url = "$t->name: <a href=\"/topics/campaign.php?id={$use['id_item']}&id_topic={$row['id_topic']}\">Campaign #{$use['id_item']}</a>";
			break;
			case "2":
				$account = $p->AccountGet($use['id_account']);
				if($account['id_topic']>0)
				{
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($account['id_topic']);
					include_once(SERVER_ROOT."/../classes/forms.php");
					$fo = new Forms();
					$form = $fo->FormGet($use['id_item']);
					$url = "$t->name: <a href=\"/topics/form.php?id={$use['id_item']}&id_topic={$account['id_topic']}\">Form \"{$form['name']}\"</a>";
				}
				else 
					$url = "<a href=\"/topics/form.php?id={$use['id_item']}\">Form #{$use['id_item']}</a>";
			break;
			default:
				$url = $this->tr->Translate("manual");
			break;
		}
		return $url;
	}
	
	public function PollQuestionMove($seq,$tot,$id_question,$id_questions_group=0)
	{
		$link = "";
		$link = ($seq>1)? "<a href=\"actions.php?from2=poll_question_move&id_question=$id_question&id_questions_group=$id_questions_group&move=up\" title=\"" . $this->tr->Translate("move_up") . "\">$this->img_arrow_up_on</a>" : $this->img_arrow_up_off;
		$link .= " - ";
		$link .= ($seq<$tot)? "<a href=\"actions.php?from2=poll_question_move&id_question=$id_question&id_questions_group=$id_questions_group&move=down\" title=\"" . $this->tr->Translate("move_down") . "\">$this->img_arrow_down_on</a>" : $this->img_arrow_down_off;
		return $link;
	}
	
	public function PollQuestionsGroupMove($seq,$tot,$id_questions_group)
	{
		$link = "";
		$link = ($seq>1)? "<a href=\"actions.php?from2=poll_questions_group_move&id_questions_group=$id_questions_group&move=up\" title=\"" . $this->tr->Translate("move_up") . "\">$this->img_arrow_up_on</a>" : $this->img_arrow_up_off;
		$link .= " - ";
		$link .= ($seq<$tot)? "<a href=\"actions.php?from2=poll_questions_group_move&id_questions_group=$id_questions_group&move=down\" title=\"" . $this->tr->Translate("move_down") . "\">$this->img_arrow_down_on</a>" : $this->img_arrow_down_off;
		return $link;
	}

	public function PollVoteNameMove($seq,$tot,$id_poll,$vote)
	{
		$link = "";
		$link = ($seq>1)? "<a href=\"actions.php?from2=poll_votename_move&id_poll=$id_poll&vote=$vote&move=up\" title=\"" . $this->tr->Translate("move_up") . "\">$this->img_arrow_up_on</a>" : $this->img_arrow_up_off;
		$link .= " - ";
		$link .= ($seq<$tot)? "<a href=\"actions.php?from2=poll_votename_move&id_poll=$id_poll&vote=$vote&move=down\" title=\"" . $this->tr->Translate("move_down") . "\">$this->img_arrow_down_on</a>" : $this->img_arrow_down_off;
		return $link;
	}

	public function PriorityLookup($id_priority)
	{
		$priorities = $this->tr->Translate("priorities");
		return $priorities[$id_priority];
	}
	
	public function QueueType($id_type)
	{
		include_once(SERVER_ROOT."/../classes/queue.php");
		$queue = new Queue(0);
		return $queue->Type($id_type);
	}

	public function RedirectLookup($id_type)
	{
		$irl = new IRL();
		return $irl->redirect_types[$id_type];
	}
	
	public function ResourceLookup($id_res)
	{
		$resources = $this->tr->Translate("resources");
		return $resources[$id_res];
	}
	
	public function RssGroupTypeLookup($group_type)
	{
		$rsm = new RssManager();
		$group_types = $rsm->group_types;
		return $group_types[$group_type];
	}

	public function SendMessageToUser($receiver_id, $receiver_name)
	{
		$descriz = $this->tr->Translate("write_message");
		if ($receiver_name!="")
			$descriz .= " a $receiver_name";
		$send_message = "<a href=\"/mail/message.php?id=0&tou=$receiver_id\" title=\"$descriz\">";
		$send_message .= "<img src=\"/images/mail.gif\" border=0 width=22 height=12 hspace=\"5\" alt=\"$descriz\"></a>\n";
		return $send_message;
	}
	
	public function StatusLookup($id_status)
	{
		$status = $this->tr->Translate("status_options");
		return $status[$id_status];
	}
	
	public function StringCut($string,$length=30)
	{
		return $this->th->StringCut($string,$length);
	}

	public function SumInt($num1,$num2)
	{
		return "<div align=\"right\">" . ($num1 + $num2) . "</div>";
	}
	
	public function TagsShow($string)
	{
		return nl2br(htmlspecialchars($string));
	}

	public function TemplateLookup($id_template)
	{
		if($id_template>0)
		{
			include_once(SERVER_ROOT."/../classes/template.php");
			$te = new Template();
			$template = $te->TemplateGet($id_template);
			$template_name = $template['name'];
		}
		else 
			$template_name = $this->tr->Translate("article");
		return $template_name;
	}

	public function TextType($id_type)
	{
		$texts_types = $this->tr->Translate("texts_types");
		return $texts_types[$id_type];
	}

	public function ThumbImage($id_image,$type,$format,$check=true)
	{
		$thumb_size = $this->i->img_sizes[0];
		$filename = "/images/upload.php?src=$type/0/{$id_image}.{$this->i->convert_format}&format=$format";
		$width = ($type=="graphics")? "0" : $thumb_size;
		return $check? $this->Graphic($filename,$width,0,FALSE) : '';
	}

	public function ThumbVideo($id_video)
	{
 		$filename = "/images/upload.php?src=videos/thumbs/{$id_video}.{$this->i->convert_format}";
 		$width = $this->i->img_sizes[0];
		return "<a href=\"video.php?id=$id_video\">" . $this->Graphic($filename,$width,0,FALSE) . "</a>";
	}
	
	public function TopicGroup($id_topic,$id_group,$otherwise_all=true)
	{
		$name ="";
		if($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$name= $t->name;
		}
		elseif($id_group>0) 
		{
			include_once(SERVER_ROOT."/../classes/topics.php");
			$tt = new Topics();
			$group = $tt->gh->GroupGet($id_group);
			$name = $this->tr->TranslateParams("all_topics_in",array($group['name']));
		}
		else
		{
			$name = $this->tr->Translate($otherwise_all? "all_option" : "none_option");
		}
		return $name;
	}

	public function TranslationInfo($id_translation,$id_article,$notes,$insert)
	{
		$desc = "apri";
		if ($notes!="")
			$desc = $this->th->StringCut($notes,30);
		if ($id_article>0)
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article($id_article);
			$art = $a->ArticleLoad();
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($art['id_topic']);
			$desc = $t->name . ": " . $this->th->StringCut($art['headline'],30);
		}
		return "<a href=\"" . ($insert==1?"/articles/article.php?w=tra&id_translation=$id_translation&id=0":"translation.php?id=$id_translation") . "\">$desc</a>";
	}
	
	public function UserLookup($id_user,$system=true)
	{
		if ($id_user>0)
		{
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User();
			$u->id = $id_user;
			$row = $u->UserGet();
			$username = $row['name'];
		}
		else
			$username = $system? $this->tr->Translate("system") : "-";
		return $username;
	}
    
    public function WidgetsStatusLookup($id_status)
    {
    	$wi = new Widgets();
   		$statuses = $wi->Statuses(true);
   		return $statuses[$id_status];
    }
    
    public function WidgetsTypeLookup($widget_type)
    {
    	$wi = new Widgets();
    	return $wi->types[$widget_type];
    }
    
    public function WebFeedItemsStatusLookup($status)
    {
        if ($status == 0)
            return 'No';
        else 
            return 'Yes';
    }

    public function WebFeedItemsApprovedLookup($approved)
    {
        if ($approved == 0)
            return 'Filtered';
        else
            return 'Approved';
    }
    
    public function WidgetAbuseStatusLookup($status)
    {
        if ($status == 0)
            return 'Pending';
        else 
            return 'Processed';
    }
    
    public function WebFeedLogStatusLookup($status)
    {
        if ($status == 0)
            return 'Failed';
        else 
            return 'Success';
    }
    
    public function WidgetOwnerLookup($owner)
    {
        if ($owner == 0)
            return 'Admin';
        else
            return 'Public';    
    }
}
?>
