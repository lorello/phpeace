<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/validator.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");

/**
 * @var string	The name of the session variable used to store the URL to jumpback after login
 */
define('JUMPBACK_VARNAME',"jumpback");

/** Form helper methods
 * 
 * Provide functionality to parse input from admin and portal forms
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class FormHelper
{
	/** 
	 * @var Validator */
	public $va;

	/**
	 * Whether magic_quotes option is enabled in php.ini
	 * @var boolean	
	 */
	private $magic_quotes;
	
	/**
	 * Whether this class is instantiated from the Admin interface or the portal
	 * @var boolean
	 */
	private $is_admin;

	/**
	 * @var TextHelper */
	public $th;

	/**
	 * Initialize local properties
	 * 
	 * @param boolean	$feedback	Whether to provide feedback or not
	 * @param integer	$id_module	Module ID
	 * @param boolean	$is_admin	Whether this class is instantiated from the Admin interface or the portal
	 */
	function __construct($feedback=false,$id_module=0,$is_admin=true)
	{
		$this->magic_quotes = false;
		$this->va = new Validator($feedback,$id_module,$is_admin);
		$this->is_admin = $is_admin;
		$this->th = new TextHelper();
	}

	/**
	 * Get the action chosen in the form submission
	 *  
	 * @param array $vars
	 * @return string
	 */
	public function ActionGet($vars)
	{
		$action = "";
		$keys = array_keys($vars);
		reset($keys);
		foreach($keys as $key)
		{
			if (substr($key,0,7)=="action_")
				$action = substr($key,7);
		}
		return $action;
	}
	
	/**
	 * Convert a checkbox submission into a boolean
	 * (1=true, 0=false) 
	 * 
	 * @param mixed $checkbox_value
	 * @return boolin
	 */
	public function Checkbox2Bool($checkbox_value)
	{
		$return_value = 0;
		if ($checkbox_value=="on" || $checkbox_value=="1")
			$return_value = 1;
		return $return_value;
	}

	/**
	 * Convert a string (a date submission) into a timestamp
	 * if it represents a valid date
	 *  
	 * @param string 	$datestring	Submitted string
	 * @param boolean	$reverse	Whether to reverse the date elements
	 * @return integer				UNIX timestamp of date
	 */
	public function CheckDateTimestamp($datestring, $reverse=FALSE)
	{
		$my_checkdate = 0;
		$mydate = $this->ExplodeDate($datestring);
		if ($reverse)
			$mydate = array_reverse($mydate);
		if (sizeof($mydate)==3 && $mydate[0]>0 && $mydate[1]>0 && $mydate[2]>0)
		{
			if (checkdate($mydate[1],$mydate[0],$mydate[2]))
				$my_checkdate = mktime(0,0,0,$mydate[1],$mydate[0],$mydate[2]);
		}
		return $my_checkdate;
	}

	/**
	 * Convert a string (a date submission) into a formatted date
	 * if it represents a valid date
	 * 
	 * @param unknown_type $datestring
	 * @return Ambigous <number, string>
	 */
	public function CheckDateString($datestring)
	{
		$my_checkdate = "";
		$mydate = $this->ExplodeDate($datestring);
		if (sizeof($mydate)==3 && $mydate[0]>0 && $mydate[1]>0 && $mydate[2]>0)
		{
			if (checkdate($mydate[1],$mydate[0],$mydate[2]))
				$my_checkdate = $this->Strings2Date($mydate[0],$mydate[1],$mydate[2]);
		}
		return $my_checkdate;
	}
	
	/**
	 * Merge date fields into a string
	 * 
	 * @param string $string	Prefix string
	 * @param array $vars		Post vars
	 * @return string
	 */
	public function DateMerge($string,$vars)
	{
        return $vars[$string.'_y']."-" . $vars[$string.'_m']."-" . $vars[$string.'_d'] . (array_key_exists($string.'_th', $vars) ? " " . $vars[$string.'_th'] . ":" . $vars[$string.'_tm'] : "");
	}

	/**
	 * Convert date fields from string to array
	 * 
	 * @param string $datestring
	 * @return array
	 */
	private function ExplodeDate($datestring)
	{
		$mydate = array();
		$datestring = trim($datestring);
		if(substr_count($datestring,"-")==2)
			$mydate = explode("-",$datestring);
		if(substr_count($datestring,".")==2)
			$mydate = explode(".",$datestring);
		if(substr_count($datestring,"/")==2)
			$mydate = explode("/",$datestring);
		return $mydate;
	}
	
	/**
	 * Invert a digit
	 * 
	 * @param integer $number
	 * @return integer
	 */
	public function Invert($number)
	{
		return (($number==1)? 0 : 1);
	}

	/**
	 * Check if a user is already logged in
	 * 
	 * @param boolean 	$check_auth	Whether to check if the user is also authenticated
	 * @return integer				Person ID
	 */
	public function IsThereUser($check_auth=false)
	{
		$session = new Session();
		$id_p = 0;
		if ($session->IsVarSet("user"))
		{
			$user = $session->Get("user");
			$id_p = $user['id'];
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			if ($check_auth && (!$user['auth'] || ($pe->verification_required && $user['verified'] != People::PeopleEmailVerificationCompleted)))
			{
				$id_p = 0;
			}
		}
		return $id_p;
	}
	
	/**
	 * Login URL
	 * 
	 * @param boolean $jumpback	Whether to jumpback to current URL after login
	 * @return string
	 */
	public function LoginUrl($jumpback=true)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		return "/" . $ini->Get("users_path") . "/login.php?jumpback=" . ($jumpback? "1":"0");
	}
	
	/**
	 * Create a portal user
	 * 
	 * @param string	$name			Name
	 * @param string 	$surname		Surname
	 * @param string 	$email			Email
	 * @param integer 	$id_geo			Geographic ID
	 * @param integer 	$id_topic		Topid ID
	 * @param boolean 	$verify_email	Whether to send a verification email
	 * @param boolint 	$contact		Portal contact opt-in
	 * @param boolint	$contact_topic	Current topic opt-in
	 * @return integer					New person ID
	 */
	public function UserCreate($name,$surname,$email,$id_geo,$id_topic=0,$verify_email=true,$contact=0,$contact_topic=0)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$id_p = $pe->UserCreate($name,$surname,$email,$id_geo,$contact,array(),$verify_email,$id_topic,$contact_topic);
		return $id_p;
	}
	
	/**
	 * HTTP GET array (sanitized)
	 * 
	 * @param boolean $escape			Whether to escape array content
	 * @param boolean $force_sanitize	Whether to force sanitazion even if it's from admin interface
	 * @return array
	 */
	public function HttpGet($escape=true,$force_sanitize=false)
	{
		$get = array();
		reset($_GET);
		foreach($_GET as $key => $value)
		{
			if(!is_array($value))
			{
				if($escape)
					$value = $this->SqlQuote($value);
				else
					$value = $this->Unmagic($value);
				if(!is_array($value) && (!$this->is_admin || $force_sanitize))
					$value = $this->Sanitize($value);
			}
			else
			{
				$san_array = array();
				reset($value);
				foreach($value as $akey => $avalue)
				{
					if(!is_array($avalue))
						$san_array[$akey] = $this->Sanitize($avalue);
				}
				$value = $san_array;
			}
			$get[$key] = $value;
		}
		return $get;
	}
	
	/**
	 * HTTP POST array (sanitized)
	 * 
	 * @param boolean $escape			Whether to store values in session (for user feedback)
	 * @param boolean $escape			Whether to escape array content
	 * @param boolean $force_sanitize	Whether to force sanitazion even if it's from admin interface
	 * @return array
	 */
	public function HttpPost($store=false,$escape=true,$force_sanitize=false)
	{
		$post = array();
		reset($_POST);
		foreach($_POST as $key => $value)
		{
			if($escape)
				$value = $this->SqlQuote($value);
			else
				$value = $this->Unmagic($value);
			if(!is_array($value) && (!$this->is_admin || $force_sanitize))
				$value = $this->Sanitize($value);
			$post[$key] = $value;
		}
		if($store)
			$this->va->PostStore();
		return $post;
	}
	
	/**
	 * HTTP POST array (unescaped in case magic-quotes is on)
	 *  
	 * @return array
	 */
	public function HttpPostUnescaped()
	{
		$post = $_POST;
		if ($this->magic_quotes)
		{
			$post = array();
			reset($_POST);
			foreach($_POST as $key => $value)
			{
				$post[$key] = stripslashes($value);
			}
		}
		return $post;
	}
	
	/**
	 * Unescaped value of a HTTP POST variable
	 * 
	 * @param string	$varname		Variable name
	 * @param boolean 	$force_sanitize	Whether to sanitize 
	 * @return mixed
	 */
	public function HttpPostUnescapedVar($varname,$force_sanitize=false)
	{
		$var = $_POST[$varname];
		if ($this->magic_quotes)
			$var = stripslashes($var);
		if($force_sanitize)
			$var = $this->Sanitize($var);
		return $var;
	}
	
	/**
	 * Get jumpback URL
	 * 
	 * @param boolean $delete	Whether to delete it from session
	 * @return string
	 */
	public function JumpBackGet($delete=true)
	{
		$session = new Session();
		$url = $session->Get(JUMPBACK_VARNAME);
		if ($delete)
			$session->Delete(JUMPBACK_VARNAME);
		return $url;
	}
	
	/**
	 * Convert a field value to integer
	 * If not numeric, set to 0
	 * 
	 * @param mixed $value
	 * @return integer
	 */
	public function Null2Zero($value)
	{
		return (int)$value;
	}

	/**
	 * Process the values for a list of checkboxes
	 * returning their values
	 * 
	 * @param string	$input_name	Variable name
	 * @param array		$vars		Submitted variables
	 * @return string
	 */
	public function ParseCheckList($input_name,$vars)
	{
		$value = "";
		$counter = $vars[$input_name.'_counter'];
		for($i=1;$i<=$counter;$i++)
		{
			$value .= $this->CheckBox2Bool($vars[$input_name.'_'.$i]);
			if ($i!=$counter)
				$value .= "|";
		}
		return $value;
	}

	/**
	 * Process the values for a list of checkboxes
	 * returning their labels
	 * 
	 * @param string	$input_name	Variable name
	 * @param array		$vars		Submitted variables
	 * @return string
	 */
	public function ParseCheckListLabels($input_name,$vars)
	{
		$list = array();
		$input_length = strlen($input_name);
		$keys = array_keys($vars);
		reset($keys);
		foreach($keys as $key)
		{
			if (substr($key,0,$input_length)==$input_name)
			{
				$label = substr($key,$input_length+1);
				if($vars[$input_name."_".$label]=="on")
					$list[] = $label;
			}
		}
		return implode("|",$list);
	}

	/**
	 * Redirect if user is not authorised by a specific callback function
	 * 
	 * @param array 	$callback	Callback class and method
	 * @param integer	$request	Callback request
	 * @param string 	$url		URL to redirect to
	 */
	public function RedirectIfNotAuthorized($callback, $request, $url='')
	{
		$id_p = $this->IsThereUser(true);
		if ($id_p > 0)
		{
			// $callback is a function that takes $id_p and $request, and returns true or false
			$authorized = call_user_func($callback, $id_p, $request);
			if (!$authorized)
			{
				if ($url == '')
				{
					$ini = new Ini();
					$url = $ini->Get('pub_web');
				}
				header("Location: {$url}");
				exit();
			}
		}
		else
		{
			$this->RedirectToLoginPage();
		}
	}

	/**
	 * Redirect to login page if user is not logged in
	 * 
	 * @param boolean $check_auth	Whether to check is the user is also authenticated
	 * @return integer				Person ID
	 */
	public function RedirectIfNotLoggedIn($check_auth=true)
	{
		$id_p = $this->IsThereUser($check_auth);
		if (!$id_p>0)
		{
			$this->RedirectToLoginPage();
		}
		return $id_p;
	}

	/**
	 * Redirect to current topic login page 
	 */
	private function RedirectToLoginPage()
	{
		$ini = new Ini();
		$this->SetRequestUrl();
		$get = $this->HttpGet();
		$url = $ini->Get('pub_web') . "/" . $ini->Get('users_path') . "/login.php";
		$url .= isset($get['id_topic']) ? "?id_topic={$get['id_topic']}" : '';
		header("Location: {$url}");
		exit();
	}
	
	/**
	 * Sanitize a string
	 * 
	 * @param string $var	Variable string
	 * @return string
	 */
	private function Sanitize($var)
	{
		$search = array ('@<script[^>]*?>.*?</script>@si');
		$replace = array ('');
		$var = preg_replace($search, $replace, $var);
		$var = strip_tags($var);
		$var = $this->th->UtfClean($var);
		return $var;
	}
	
	/**
	 * Get all submitted parameters and return their serialization
	 * 
	 * @param boolean	$serialized			Whether to serialize to string
	 * @param string	$array_separator	Which string to use for serializing arrays
	 * @param boolean	$allow_empty_values	Whether to allow empty values
	 * @return number
	 */
	public function SerializeParams($serialized=true,$array_separator=",",$allow_empty_values=false)
	{
		$vars = $_POST;
		$values = array();
		$keys = array_keys($vars);
		reset($keys);
		foreach($keys as $key)
		{
			if (substr($key,0,6)=="param_")
			{
				$param = substr($key,6);
				$pvalue = $vars["param_".$param];
				if ($pvalue!="")
				{
					if($pvalue=="on")
						$values[$param] = 1;
					else
					{
						if(is_array($pvalue))
							$pvalue = implode($array_separator,$pvalue);
						$value = ($this->is_admin)?  $pvalue : $this->Sanitize($pvalue);
						$values[$param] = $this->Unmagic($value);
					}
				}
				elseif($allow_empty_values)
				{
					$values[$param] = "";
				}
			}
			if (substr($key,0,7)=="paramd_")
			{
				$param = substr($key,7,strlen($key)-7-2);
				$pdate = $this->DateMerge("paramd_" . $param,$vars);
				$values[$param] = $this->CheckDateTimestamp($pdate,true);
			}
		}
		if($serialized)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$return = $v->Serialize($values);
		}
		else
			$return = $values;
		return $return;
	}
	
	/**
	 * Set which URL to jump back after login
	 * 
	 * @param string $url
	 */
	public function SetJumpbackUrl($url)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set(JUMPBACK_VARNAME,$url);
	}

	/**
	 * Set the URL to jump back to as previous page (REFERER) 
	 */
	public function SetReferer()
	{
		$this->SetJumpbackUrl($_SERVER['HTTP_REFERER']);
	}

	/**
	 * Set the URL to jump back to as current page 
	 */
	public function SetRequestUrl()
	{
		$this->SetJumpbackUrl($_SERVER['REQUEST_URI']);
	}
	
	/**
	 * Escape quotes of a string to be used in a SQL statement
	 * 
	 * @param string 	$sqlstr		SQL string to escape
	 * @param boolean 	$unmagic	Whether to remove existing magic-quotes
	 * @return string
	 */
	public function SqlQuote($sqlstr,$unmagic=true)
	{
		if($unmagic)
			$sqlstr = $this->Unmagic($sqlstr);
		if(!is_array($sqlstr))
		{
			$sqlstr = addslashes($sqlstr);
		}
		return $sqlstr;
	}
	
	/**
	 * Convert a string to a number
	 * 
	 * @param string	$text_value
	 * @param integer	$default		Number to default for non-numeric strings
	 * @return integer
	 */
	public function String2Number($text_value,$default=0)
	{
		$return_value = $default;
		$text_value = str_replace(",",".",$text_value);
		$text_value = trim($text_value);
		if (is_numeric($text_value))
			$return_value = $text_value;
		return $return_value;
	}

	/**
	 * Convert a string to a valid path
	 * 
	 * @param string $text
	 * @return string
	 */
	public function String2Path($text)
	{
		if ($text!="")
		{
			$text = str_replace("/","",$text);
			$text = strtolower($text);
			$text = preg_replace("/[^0-9a-z_]/i",'', $text); 
		}
		return $text;
	}

	/**
	 * Convert a string to an absolute URL
	 * (adding http:// if missing)
	 * 
	 * @param string $text_value
	 * @return string
	 */
	public function String2Url($text_value,$secure=false)
	{
		$ret = $text_value;
		if ($ret!="")
		{
			if ((strpos($ret,'://')===false)) {
				$ret = ($secure? "https://":"http://") . $ret;				
			}
		}
		return $ret;
	}

	/**
	 * Remove HTML tags from a string
	 * 
	 * @param string $text
	 * @return string
	 */
	public function StringClean($text)
	{
	    $text = $this->th->StripHtml($text);
	    // fix &nbsp;
	    $text = str_replace("\xc2\xa0",' ',$text); 
	    return trim($text);
	}
	
	/**
	 * Convert date fields to a date string
	 * 
	 * @param integer $day
	 * @param integer $month
	 * @param integer $year
	 * @return string
	 */
	public function Strings2Date($day,$month,$year)
	{
		$date = $year . "-" . $month . "-" . $day;
		return $date;
	}

	/**
	 * Convert date and time fields into a datetime string
	 * 
	 * @param integer $day
	 * @param integer $month
	 * @param integer $year
	 * @param integer $hour
	 * @param integer $min
	 * @param integer $sec
	 * @return string
	 */
	public function Strings2Datetime($day,$month,$year,$hour,$min,$sec="00")
	{
		return $this->Strings2Date($day,$month,$year) . " " . $this->Strings2Time($hour,$min,$sec);
	}

	/**
	 * Convert time fields to a time string
	 * 
	 * @param integer $hour
	 * @param integer $min
	 * @param integer $sec
	 * @return string
	 */
	public function Strings2Time($hour,$min,$sec="00")
	{
		$min = $this->Null2Zero($min);
		$hour = $this->Null2Zero($hour);
		$time = $hour . ":" . $min . ":" . $sec;
		return $time;
	}

	/**
	 * Removes slashes
	 * To be used only when you are sure that the input has been escaped
	 * (for example, for data to be used for dynamic SQL)
	 * 
	 * @param string $string
	 * @return string
	 */
	public function SubmitEscape($string)
	{
		return ($this->magic_quotes)? stripslashes($string) : $string;
	}
	
	/**
	 * Replace text that has bene previously replaced in order to work inside a textarea editor
	 * 
	 * @param string $text
	 * @return string
	 */
	public function Text($text)
	{
		$text = str_replace("</_textarea>","</textarea>",$text);
		$text = str_replace("&_amp;","&amp;",$text);
		$text = str_replace("&_#","&#",$text);
		$text = str_replace("&_lt","&lt",$text);
		$text = str_replace("&_gt","&gt",$text);
		return $text;
	}
	
	/**
	 * Parse a Topic / Topic Group field to return the two numeric values
	 * 
	 * @param string $id_topic_or_group
	 * @return array 
	 */
	public function TopicOrGroup($id_topic_or_group)
	{
		$id_topic = 0;
		$id_group = 0;
		if($id_topic_or_group!="")
		{
			if (substr($id_topic_or_group,0,2)=="g_")
				$id_group = (int)substr($id_topic_or_group,2);
			else 
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$id_topic = (int)$id_topic_or_group;
				$t = new Topic($id_topic);
				$id_group = (int)$t->id_group;
			}
		}
		return array('id_topic'=>$id_topic,'id_group'=>$id_group);
	}
	
	/**
	 * Remove magic-quotes
	 * 
	 * @param string $text
	 * @return string
	 */
	private function Unmagic($text)
	{
		if ($this->magic_quotes && !is_array($text))
			$text = stripslashes($text);
		return $text;
	}

	/**
	 * Process uploaded files
	 * 
	 * @param string	$file_type		Uploaded file type
	 * @param boolean	$allow_missing	Whether to allow an empty upload
	 * @param integer	$max_size		Maximum file size
	 * @param boolean 	$notify			Whether to notify admin user about errors
	 * @param string	$file_id		File ID
	 * @return array
	 */
	public function UploadedFile($file_type,$allow_missing=false,$max_size=0,$notify=true,$file_id="")
	{
		$file_id = ($file_id!="")? $file_id : $file_type;
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$file_max_size = $max_size>0? $max_size : $fm->MaxFileSize();

		if (is_array($_FILES[$file_id]['tmp_name']))
			$file_tmp = $_FILES[$file_id]['tmp_name'];
		else
			$file_tmp = array($_FILES[$file_id]['tmp_name']);
		
		if (is_array($_FILES[$file_id]['name']))
			$file_name= $_FILES[$file_id]['name'];
		else
			$file_name= array($_FILES[$file_id]['name']);
		
		if (is_array($_FILES[$file_id]['error']))
			$file_error = $_FILES[$file_id]['error'];
		else
			$file_error = array($_FILES[$file_id]['error']);
		if (is_array($_FILES[$file_id]['size']))
			$file_size= $_FILES[$file_id]['size'];
		else
			$file_size= array($_FILES[$file_id]['size']);
		$upload = array();
		$upload['ok'] = false;
		if($file_error[0]>0)
		{
			$upload['error'] = $file_error[0];
			$msg = "";
			$msg_params = array();
			switch($file_error[0])
			{
				case "1":
					$msg = "upload_limit_warn";
					$msg_params = array(ini_get("upload_max_filesize"));
					UserError("The uploaded file exceeds the upload_max_filesize directive in php.ini",array('size'=>$file_size[0]),512);
				break;
				case "2":
					$msg = "upload_limit_warn";
					$msg_params = array(round($file_max_size/(1024*1024),2) . " Mb");
				break;
				case "3":
					$msg = "upload_partial";
				break;
				case "4":
					if(!$allow_missing)
						$msg = "upload_missing";
				break;
				case "6":
					UserError("Missing a temporary folder",array());
				break;
				case "7":
					UserError("Failed to write file to disk",array());
				break;
			}
			if($msg!="" && $notify)
			{
				$this->va->MessageSet("error",$msg,$msg_params);
			}
		}
		else 
		{
			if ($file_size[0]>0)
			{
				// Check PhPeace upload limit (in admin / config)
				if ($file_size[0] > $file_max_size)
				{
					$this->va->MessageSet("error","upload_limit_warn",array(floor($file_max_size/1048576) . " Mb"));
				}
				else
				{
					$filename = $file_name[0];
					$file_ext = $fm->Extension($filename);
					if (is_uploaded_file($file_tmp[0]))
					{
						if($fm->IsGood($filename,$file_type,$this->is_admin))
						{
							$upload['ok'] = true;
							$upload['name'] = $fm->FixFileName($this->SqlQuote($filename));
							$upload['temp'] = $file_tmp[0];
							$upload['ext'] = $file_ext;
							$upload['size'] = $file_size[0];
						}
						else 
						{
							$upload['error'] = "-1";
							if ($notify)
							{
								$this->va->MessageSet("error","reject_upload",array($file_ext));
							}
						}
					}
				}
			}
		}
		return $upload;
	}

	/**
	 * Get file type marker
	 * 
	 * @param integer $id_type
	 * @return string
	 */
	public function UploadFileType($id_type)
	{
		$type = "";
		switch($id_type)
		{
			case "0":
				$type = "any";
			break;
			case "1":
				$type = "doc";
			break;
			case "2":
				$type = "img";
			break;
			case "3":
				$type = "aud";
			break;
			case "4":
				$type = "vid";
			break;
		}
		return $type;
	}
}
?>
