<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!
   
   CREDITS:
   Giacomo Lacava, for most of this code!

********************************************************************/

include_once(SERVER_ROOT."/../classes/cache.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class RssManager
{
	public $group_types;
	public $encoding;

	/**
	* @var Cache */
	private $cache;

	private $pub_web;
	private $max_items_per_feed;
	
	function __construct()
	{
		define('AGGREGATOR_CACHE_TYPE',"aggregated_feed");
		define('AGGREGATOR_TYPE_JSON',"json");
		define('AGGREGATOR_TYPE_XML',"xml");
		
		$this->cache = new Cache;
		$ini = new Ini();
		$this->pub_web = $ini->Get('pub_web');
		$this->max_items_per_feed = 20;
		$this->group_types = array('___','rss','feature');
		$conf = new Configuration();
		$this->encoding = $conf->Get("encoding");
	}

	public function GetAggregation($id_group,$force_reload=false,$aggregationType = AGGREGATOR_TYPE_XML)
	{
		$output = "";
		if($id_group>0)
		{
			$cache_id = $this->GroupCacheId($id_group);
			if(!$this->cache->Exists(AGGREGATOR_CACHE_TYPE,$cache_id) || $force_reload)
			{
				$output = $this->GroupAggregate($id_group);
				$this->cache->Set(AGGREGATOR_CACHE_TYPE,$cache_id,$output);
			}
			else
			{
				// it's fresh!
				$output = $this->cache->Get(AGGREGATOR_CACHE_TYPE,$cache_id);
			}
			// output to json or xml
			if($aggregationType==AGGREGATOR_TYPE_JSON)
			{
				$output = "var feed = '" .  trim(str_replace('"','\"',str_replace("'","\\'",$output))) . "';";
			}
			elseif($aggregationType==AGGREGATOR_TYPE_XML)
			{
				$output = "<?xml version=\"1.0\" encoding=\"" . $this->encoding . "\"?>\n" . $output;
			}
		}
		return $output;
	}
	
	private function GroupAggregate($id_group)
	{
		$feeds = array();
		$xml = "";
		$group = $this->GroupGet($id_group);
		$num_feeds = $this->RssAll($feeds,$id_group,false);
		if($num_feeds>0)
		{
			$aggr_items = array();
			// Load feeds into array
			include_once(SERVER_ROOT."/../classes/rss.php");
			$group_type = $this->group_types[$group['group_type']];
			$rss = new Rss($group_type);
			$counter = 0;
			foreach($feeds as $feed)
			{
				$content = $rss->Get($feed['url'],$group['ttl']);
				if($content!="")
				{
					$dom = new DOMDocument('1.0', $this->encoding);
					$dom->loadXML($content);
					if(isset($dom->encoding))
					{
						$items = $dom->getElementsByTagName("item");
						if($items->length>0)
						{
							if($group_type=="rss" && $group['show_title'])
							{
								$channel = $dom->getElementsByTagName("channel");
								if($channel->length > 0)
								{
									$channel_title = $channel->item(0)->getElementsByTagName("title");
									$feed_title = $channel_title->item(0)->textContent;
								}
							}
							foreach($items as $item)
							{
								$ts = 0;
								switch($group_type)
								{
									case "feature":
										$ts = $item->getAttribute("ts");
									break;
									case "rss":
										$item_date = $item->getElementsByTagName("pubDate");
										$ts = strtotime($item_date->item(0)->textContent);
									break;
								}
								$node_xml = $dom->saveXML($item);
 								if($group_type=="rss" && $group['show_title'] && $feed_title!="")
									$node_xml = str_replace("<title>","<title>[$feed_title] ",$node_xml);
								$aggr_items['item_' . $counter] = array('ts'=>$ts,'xml'=>$node_xml);
								$counter ++;
							}
							
						}
					}
				}
			}
			// sort array
			rsort($aggr_items);
			// limit
			$limit = (int)$group['items'];
			if(!$limit>0)
				$limit = $this->max_items_per_feed;
			$aggr_items = array_slice($aggr_items,0,$limit);
			switch($group_type)
			{
				case "feature":
					$xml = "<feature_group id=\"$id_group\" name=\"" . htmlspecialchars($group['name']) . "\" limit=\"{$group['items']}\">\n";
					$xml .= "<sources>\n";
					foreach($feeds as $feed)
					{
						$xml .= "<source url=\"{$feed['url']}\" />\n";
					}
					$xml .= "</sources>\n";
					$xml .= "<items count=\"" . count($aggr_items) . "\">\n";
					foreach($aggr_items as $agg_item)
					{
						$xml .= $agg_item['xml'] . "\n";
					}
					$xml .= "</items>\n";
					$xml .= "</feature_group>\n";
				break;
				case "rss":
					$xml = "<rss xmlns:atom=\"http://www.w3.org/2005/Atom\" ";
					$xml .= "xmlns:wfw=\"http://wellformedweb.org/CommentAPI/\" ";
					$xml .= "xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" ";
					$xml .= "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" ";
					$xml .= "xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\" ";
					$xml .= "xmlns:media=\"http://search.yahoo.com/mrss/\" ";
					$xml .= "version=\"2.0\">\n";
					$xml .= "<channel>\n";
					$xml .= "<atom:link href=\"{$this->pub_web}/js/rss_group.php?id=$id_group\" rel=\"self\" type=\"application/rss+xml\"/>\n";
					$xml .= "<title>{$group['name']}</title>\n";
					$xml .= "<link>{$this->pub_web}</link>\n";
					$xml .= "<lastBuildDate>" . date("r") . "</lastBuildDate>\n";
					$xml .= "<generator>PhPeace Feed Aggregator</generator>\n";
					$xml .= "<ttl>60</ttl>\n";
					foreach($aggr_items as $agg_item)
					{
						$xml .= $agg_item['xml'] . "\n";
					}
					$xml .= "</channel>\n";
					$xml .= "</rss>\n";
				break;
			}
		}
		return $xml;
	}

	public function GroupDelete( $id_group )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "feeds_groups" );
		$res[] = $db->query( "DELETE FROM feeds_groups WHERE id_group='$id_group' " );
		Db::finish( $res, $db);
		$this->GroupUpdate($id_group);
	}

	public function GroupGet($id_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_group,name,group_type,items,ttl,id_topic,show_title FROM feeds_groups WHERE id_group='$id_group'";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	private function GroupCacheId($id_group)
	{
		return "group_" . $id_group;
	}

	public function GroupStore( $id_group,$name,$group_type,$items,$ttl,$id_topic,$show_title )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "feeds_groups" );
		if ($id_group>0)
		{
			$this->GroupUpdate($id_group);
			$sqlstr = "UPDATE feeds_groups SET name='$name',group_type='$group_type',items='$items',ttl='$ttl',id_topic='$id_topic',show_title='$show_title'
				 WHERE id_group='$id_group'";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		else
		{
			$id_group = $db->nextId( "feeds_groups", "id_group" );
			$sqlstr = "INSERT INTO feeds_groups (id_group,name,group_type,items,ttl,id_topic,show_title) VALUES ($id_group,'$name','$group_type','$items','$ttl','$id_topic','$show_title')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		
		return $id_group;
	}

	private function GroupUpdate($id_group)
	{
		$this->cache->Delete(AGGREGATOR_CACHE_TYPE,$this->GroupCacheId($id_group));
	}
	
	public function Import($url,$id_topic,$id_subtopic,$id_template,$approved,$id_keyword)
	{
		include_once(SERVER_ROOT."/../classes/rss.php");
		$ini = new Ini();
		$rss = new Rss();
		$content = $rss->Get($url,1);
		$counter = 0;
		if($content!="")
		{
			$dom = new DOMDocument('1.0', $this->encoding);
			$dom->loadXML($content);
			if(isset($dom->encoding))
			{
				$items = $dom->getElementsByTagName("item");
				if($items->length>0)
				{
					$db =& Db::globaldb();
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($id_topic);
					$id_visibility = $ini->Get("default_visibility");
					if(!$id_subtopic>0)
					{
						$id_subtopic = $t->HasSubtopicType(0);
					}
					foreach($items as $item)
					{
						$ts = 0;
						$item_date = $item->getElementsByTagName("pubDate");
						$ts = strtotime($item_date->item(0)->textContent);
						$item_title = $item->getElementsByTagName("title");
						$title = $db->SqlQuote(trim($item_title->item(0)->textContent));
						$item_link = $item->getElementsByTagName("link");
						$link = $db->SqlQuote(trim($item_link->item(0)->textContent));
						$item_description = $item->getElementsByTagName("description");
						$description = $db->SqlQuote(trim($item_description->item(0)->textContent));
						$item_guid = $item->getElementsByTagName("guid");
						$guid = $db->SqlQuote(trim($item_guid->item(0)->textContent));
						$show_author = 0;
						$internal_keywords = array();
						if($id_keyword>0)
						{
							$internal_keywords[] = array('id_keyword'=>$id_keyword);
						}
						// check if article is already there with same link and date
						if($ts>0 && strlen($title)>0)
						{
							$rows = array();
							$original_date = date("Y-m-d",$ts);
							$sqlstr = "SELECT id_article,id_topic FROM articles WHERE author_notes='$guid' AND id_topic='$id_topic' ";
							$num_articles = $db->QueryExe($rows, $sqlstr);
							if($num_articles==0)
							{
								include_once(SERVER_ROOT."/../classes/article.php");
								$a = new Article(0);
								$a->ArticleStore($original_date,1,0,"",$guid,$link,$original_date,$show_author,$id_topic,$id_subtopic,'',$title,$description,1,'',0,'',1,$t->id_language,$approved,0,$id_visibility,"",1,1,0,0,0,0,$internal_keywords,1,$id_template,"",0,true);
								$counter ++;
							}
						}
					}
				}
			}
		}
		if($counter>0)
		{
			include_once(SERVER_ROOT."/../classes/log.php");
			$log = new Log();
			$admin_web = $ini->Get("admin_web");
			$log->Write("info","Imported $counter RSS items<br>\nSee $admin_web/topics/subtopic.php?id=$id_subtopic&id_topic=$id_topic<br>\n");
		}
		return $counter;
	}

	public function Groups( &$rows, $id_topic, $paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT fg.id_group,fg.name,fg.id_topic,fg.group_type,COUNT(f.id_feed) AS num_feeds
			FROM feeds_groups fg 
			LEFT JOIN feeds f ON fg.id_group=f.id_group
			WHERE fg.id_topic=$id_topic 
			GROUP BY fg.id_group
			ORDER BY fg.name ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function RssAll( &$rows, $id_group, $paged )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_feed,url FROM feeds WHERE id_group=$id_group ORDER BY id_feed DESC ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	public function RssGet($id_feed)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_feed,url,id_group FROM feeds WHERE id_feed='$id_feed'";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function RssDelete( $id_feed )
	{
		$rss = $this->RssGet($id_feed);
		$this->GroupUpdate($rss['id_group']);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "feeds" );
		$res[] = $db->query( "DELETE FROM feeds WHERE id_feed='$id_feed' " );
		Db::finish( $res, $db);
	}

	public function RssStore( $id_feed,$url,$id_group )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "feeds" );
		if ($id_feed>0)
		{
			$sqlstr = "UPDATE feeds SET url='$url',id_group='$id_group' WHERE id_feed='$id_feed'";
		}
		else
		{
			$id_feed = $db->nextId( "feeds", "id_feed" );
			$sqlstr = "INSERT INTO feeds (id_feed,url,id_group) VALUES ($id_feed,'$url','$id_group')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->GroupUpdate($id_group);
		return $id_feed;
	}

}
?>
