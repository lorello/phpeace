<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/log.php");
include_once(SERVER_ROOT."/../classes/history.php");

class WebFeeds
{
	/** 
	 * @var History */
	private $h;

	private $xmlElement; 	
    private $id_res_type;
	public $widgets_per_page;
    public $minimum_feed_update_interval;
    
	function __construct()
	{
        $this->id_res_type = 25;
        $ini = new Ini();
        $this->widgets_per_page = $ini->GetModule("homepage","widgets_per_page",15);
        $conf = new Configuration();
        $this->minimum_feed_update_interval = $conf->Get("minimum_feed_update_interval");
        $this->h = new History();
	}

    public function WebFeedItemSearchPub(&$rows,$words,$id_category=0)
    {
        if(count($words)>0)
        {
            $db =& Db::globaldb();
            $sqljoin = "";
            for ($i = 0; $i < count($words); $i++)
            {
                $sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = wfi.id_web_feed_item AND si$i.id_res={$this->id_res_type} ";
                $sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
                $wheres[] = "sw$i.word = '$words[$i]'";
                $scores[] = "SUM(si$i.score)";
            }
            if ($id_category == 0)
            	$sqljoin_widget = "
            						INNER JOIN (widget_web_feeds wwf
                            				INNER JOIN widgets w
                            				ON wwf.id_widget = w.id_widget)
                                    ON wfi.id_web_feed=wwf.id_web_feed
                                    ";
            else
            	$sqljoin_widget = "
            						INNER JOIN (widget_web_feeds wwf
                            				INNER JOIN (widgets w
                            							INNER JOIN widgets_category_widgets wcw
                            							ON w.id_widget = wcw.id_widget
                            							AND wcw.id_category = '$id_category'
                            							AND wcw.is_selected = 1)
                            				ON wwf.id_widget = w.id_widget)
                                    ON wfi.id_web_feed=wwf.id_web_feed 
                                    ";
            
            $sqlstr = "SELECT wfi.title, wfic.description, wfi.link_url,
                    w.id_widget, w.title as widget_title, w.description as widget_description,  'web_feed_item' AS item_type,  
                (" . implode("+",$scores) . ") AS total_score 
        	FROM web_feed_items wfi $sqljoin
                $sqljoin_widget
            INNER JOIN web_feed_items_content wfic
            ON wfi.id_web_feed_item = wfic.id_web_feed_item
            WHERE " . implode(" AND ", $wheres) . " AND wfi.sort <= wwf.items_display
            GROUP BY w.id_widget
            ORDER BY total_score DESC,wfi.title";
                                                  
            $num = $db->QueryExe($rows, $sqlstr,true,MYSQL_BOTH,$this->widgets_per_page);
            
            foreach($rows as $key=>$row)
            {
            	$rows[$key]['title'] = stripcslashes($row['title']);
                $rows[$key]['description'] = stripcslashes($row['description']);
                $rows[$key]['widget_title'] = stripcslashes($row['widget_title']);
                $rows[$key]['widget_description'] = stripcslashes($row['widget_description']);
            }
        }
        else 
            $num = 0;
        return $num;
    }
    
    public function FilteredWebFeedItems($from_date, $to_date, &$rows, $paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "
            SELECT wfi.id_web_feed_item, wfi.title, wfi.link_url, wfi.published_date, wfi.status, wfi.last_updated, wf.id_web_feed, wf.title as web_feed_title
            FROM web_feed_items wfi
            INNER JOIN web_feeds wf
            ON wfi.id_web_feed = wf.id_web_feed
            WHERE wfi.is_filtered = 1
            AND wfi.last_updated >= '$from_date'
            AND wfi.last_updated < '$to_date'
            ";
            
        return $db->QueryExe($rows, $sqlstr, $paged);
    }
    
    public function WebFeedsAll(&$rows,$paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "
            SELECT a.id_web_feed, a.title, a.feed_url, a.update_interval, a.last_updated,
             	ifnull(b.num_web_feed_items, 0) as num_web_feed_items
            FROM 
            	(SELECT id_web_feed, title, feed_url, update_interval, last_updated
            	FROM web_feeds) a 
            	LEFT JOIN 
            	(SELECT id_web_feed, count(*) as num_web_feed_items 
            	FROM web_feed_items 
            	GROUP BY id_web_feed) b 
            	ON a.id_web_feed = b.id_web_feed
            GROUP BY id_web_feed
            ORDER BY title
            ";
        $num = $db->QueryExe($rows, $sqlstr, $paged);
        
        foreach ($rows as &$row)
        {
			$row_status = $this->WebFeedLatestStatusGetById($row['id_web_feed']);
			$row['status'] = $row_status['status'];		
        }
		
		return $num;        
    }
	
	public function WebFeedsSearchCriteria(&$rows,$status,$paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "
            SELECT aa.*, SUBSTRING_INDEX(IFNULL(bb.dd,'1-1'), '-',-1) AS status  FROM (SELECT a.id_web_feed, a.title, a.feed_url, a.update_interval, a.last_updated,                 
			IFNULL(b.num_web_feed_items, 0) AS num_web_feed_items         
			FROM                 
			(SELECT id_web_feed, title, feed_url, update_interval, last_updated                FROM web_feeds) a                 
			LEFT JOIN                 
			(SELECT id_web_feed, COUNT(*) AS num_web_feed_items                
			FROM web_feed_items                              
			GROUP BY id_web_feed) b   
			ON a.id_web_feed = b.id_web_feed             
			GROUP BY id_web_feed) aa 
			LEFT JOIN 
			(SELECT id_web_feed,MAX(CONCAT(id_web_feed_status,'-',STATUS)) AS dd FROM web_feeds_status_log GROUP BY id_web_feed) 
			bb 
			ON bb.id_web_feed = aa.id_web_feed    
            	";
		if($status != 2) // ALL
		{
			$sqlstr .= " WHERE SUBSTRING_INDEX(IFNULL(bb.dd,'1-1'), '-',-1) = $status ";
		}
        $sqlstr .=    
			"
            ORDER BY title
            ";
        $num = $db->QueryExe($rows, $sqlstr, $paged);
		return $num;        
    }

	public function WebFeedsSearch(&$rows,$params,$paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "	SELECT wf.id_web_feed, wf.title, wf.feed_url, wf.update_interval, UNIX_TIMESTAMP(wf.last_updated) AS last_updated_ts,
			COUNT(wfi.id_web_feed_item) AS counter
			FROM web_feeds wf
			LEFT JOIN web_feed_items wfi ON wf.id_web_feed=wfi.id_web_feed 
			WHERE 1=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (title LIKE '%{$params['name']}%' OR feed_url LIKE '%{$params['name']}%') ";
		if (strlen($params['content']) > 0)
			$sqlstr .= " AND (wfi.title LIKE '%{$params['content']}%' OR wfi.link_url LIKE '%{$params['content']}%') ";
        $sqlstr .=  " GROUP BY wf.id_web_feed ORDER BY wf.last_updated DESC ";
        $num = $db->QueryExe($rows, $sqlstr, $paged);
		return $num;        
    }

    public function WebFeedGet($id_web_feed)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT id_web_feed, title, feed_url, update_interval, last_updated,
            UNIX_TIMESTAMP(last_updated) AS last_updated_ts
            FROM web_feeds 
            WHERE id_web_feed = '$id_web_feed'";
        $db->query_single($row, $sqlstr);
        return $row;
    }
    
    public function WebFeedGetByUrl($feed_url)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT id_web_feed, title, feed_url, update_interval, last_updated
            FROM web_feeds 
            WHERE feed_url = '$feed_url'";
        $db->query_single($row, $sqlstr);
        return $row;
    }

    public function WebFeedStore($id_web_feed,$title,$feed_url,$update_interval,$ignore_min_update_interval=false)
    {
    	if ($ignore_min_update_interval == false)
    	{
    		if($update_interval<$this->minimum_feed_update_interval)
    			$update_interval = $this->minimum_feed_update_interval;
    	}
        $db =& Db::globaldb();
        $db->begin();
        $db->lock("web_feeds");
        if($id_web_feed>0)
        {
            $sqlstr = "UPDATE web_feeds 
                SET title='$title',feed_url='$feed_url',
                update_interval='$update_interval'
                WHERE id_web_feed='$id_web_feed' ";
            $res[] = $db->query( $sqlstr );
            
        }
        else 
        {
            $id_web_feed = $db->nextId("web_feeds","id_web_feed");
            $sqlstr = "INSERT INTO web_feeds 
                (id_web_feed,title,feed_url,update_interval) 
                VALUES 
                ('$id_web_feed','$title','$feed_url','$update_interval')
                ";
            $res[] = $db->query( $sqlstr );

        }
        Db::finish( $res, $db);
        return $id_web_feed;
    }

    public function WebFeedDelete($id_web_feed)
    {
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feeds" );
        $res[] = $db->query( "DELETE FROM web_feeds WHERE id_web_feed='$id_web_feed' " );

        $db->lock( "web_feed_items" );
        $res[] = $db->query( "DELETE FROM web_feed_items WHERE id_web_feed='$id_web_feed' " );
        Db::finish( $res, $db);
    }

    public function WebFeedItemGet($id_web_feed,$id_web_feed_item)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT wfi.id_web_feed, wfi.id_web_feed_item, wfi.title, wfic.description, wfic.content, wfi.link_url, wfi.is_approved, wfi.is_filtered, wfi.status, wfi.published_date, wfi.last_updated
            FROM web_feed_items wfi
            INNER JOIN web_feed_items_content wfic
            ON wfi.id_web_feed_item = wfic.id_web_feed_item 
            WHERE wfi.id_web_feed = '$id_web_feed'
            AND wfi.id_web_feed_item = '$id_web_feed_item'";
        $db->query_single($row, $sqlstr);
        return $row;
    }

    public function WebFeedItemGetById($id_web_feed_item)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT wfi.id_web_feed, wfi.id_web_feed_item, wfi.title, wfic.description, wfi.link_url, wfi.is_approved, wfi.is_filtered, wfi.status, wfi.published_date, wfi.last_updated
            FROM web_feed_items wfi
            INNER JOIN web_feed_items_content wfic
            ON wfi.id_web_feed_item = wfic.id_web_feed_item 
            WHERE wfi.id_web_feed_item = '$id_web_feed_item'";
        $db->query_single($row, $sqlstr);
        return $row;
    }
    
    public function WebFeedItems(&$rows,$id_web_feed,$paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "
            SELECT id_web_feed_item, title, link_url, is_approved, is_filtered, status, published_date, last_updated
            FROM web_feed_items
            WHERE id_web_feed = $id_web_feed 
            ";
        $sqlstr .= " ORDER BY id_web_feed_item DESC ";
        return $db->QueryExe($rows, $sqlstr, $paged);
    }
    
    public function WebFeedItemDelete($id_web_feed_item)
    {
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feed_items" );
        $res[] = $db->query( "DELETE FROM web_feed_items WHERE id_web_feed_item='$id_web_feed_item' " );
        Db::finish( $res, $db);
		$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["delete"],true);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($this->id_res_type,$id_web_feed_item);
    }

    public function WebFeedItemDeleteByWebFeed($id_web_feed)
    {
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feed_items" );
        $res[] = $db->query( "DELETE FROM web_feed_items WHERE id_web_feed='$id_web_feed' " );
        Db::finish( $res, $db);
    }

    public function WebFeedItemContentDeleteByWebFeed($id_web_feed)
    {
		$rows = array();
		$this->WebFeedItems($rows,$id_web_feed,false);
		
		$db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feed_items_content" );
        foreach ($rows as $row)
        {
        	$id_web_feed_item = $row['id_web_feed_item'];
        	$res[] = $db->query( "DELETE FROM web_feed_items_content WHERE id_web_feed_item='$id_web_feed_item' " );
        }
		Db::finish( $res, $db);
    }
    
    public function WebFeedStatusLogDeleteByWebFeed($id_web_feed)
    {
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feeds_status_log" );
        $res[] = $db->query( "DELETE FROM web_feeds_status_log WHERE id_web_feed='$id_web_feed' " );
        Db::finish( $res, $db);
    }
    
    public function WebFeedItemStore($id_web_feed_item,$is_approved,$is_filtered,$status)
    {
    	$item_old = $this->WebFeedItemGetById($id_web_feed_item);
        $db =& Db::globaldb();
        $db->begin();
        $db->lock("web_feed_items");
        if($id_web_feed_item>0)
        {
            $sqlstr = "UPDATE web_feed_items 
                SET is_approved='$is_approved',is_filtered='$is_filtered',status='$status'
                WHERE id_web_feed_item = '$id_web_feed_item' ";
            
            $res[] = $db->query( $sqlstr );
        }
        Db::finish( $res, $db);

		$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["update"],true);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($is_approved && !$is_filtered)
		{
			$s->IndexQueueAdd($this->id_res_type,$id_web_feed_item,0,0,1);
		}
		else 
		{
			$s->ResourceRemove($this->id_res_type,$id_web_feed_item);
		}
		if($is_approved!=$item_old['is_approved'])
		{
			if($is_approved)
				$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["approve"],true);
			else
				$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["on_hold"],true);
		}
		
        return $id_web_feed_item;
    }
    
    private function WebFeedLatestStatusGet(&$rows,$paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "SELECT id_web_feed, status, max(pull_date)
                FROM web_feeds_status_log
                GROUP BY id_web_feed";
        return $db->QueryExe($rows, $sqlstr, $paged);
    }
    
    private function WebFeedLatestStatusGetById($id_web_feed)
    {
        $row = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT status 
            FROM web_feeds_status_log 
            WHERE id_web_feed = '$id_web_feed'
            ORDER BY pull_date DESC
            LIMIT 1";
        $db->query_single($row, $sqlstr);
        return $row;
    }
    
    /*
    RSS Reporting : Distribution of feed items
    */
    public function DistributionOfFeedItemsGet(&$rows,$from_date, $to_date,$paged=true)
    {
        $db =& Db::globaldb();
        $sqlstr = "
			SELECT DISTINCT wfsl.id_web_feed, wf.title, 
			CONCAT( YEAR(wfsl.pull_date), '-', MONTH(wfsl.pull_date) , '-',DAY(wfsl.pull_date) ) AS pull_date, 
			SUM(wfsl.inserted) AS inserted, SUM(wfsl.updated) AS updated 
			FROM web_feeds_status_log wfsl 
			INNER JOIN web_feeds wf ON wfsl.id_web_feed = wf.id_web_feed 
			WHERE pull_date >= '$from_date'  AND pull_date < '$to_date' 
			GROUP BY wf.title, DATE( wfsl.pull_date) DESC
            ";

        $num = $db->QueryExe($rows, $sqlstr, $paged);
       
        return $num;
    }
    
    
    /*
    RSS Reporting : Average Item Per Feed
    */
    public function AverageItemsPerFeedGet(&$rows, $from_date, $to_date, $paged=true)
    {
        $db =& Db::globaldb();
        $sqlstr = "
			SELECT DISTINCT wfsl.id_web_feed, wf.title, 
				CONCAT( YEAR(wfsl.pull_date), '-', MONTH(wfsl.pull_date) , '-',DAY(wfsl.pull_date) ) AS pull_date,
			AVG(wfsl.inserted) AS average_inserted, ROUND(AVG(wfsl.updated)) AS average_updated
			FROM web_feeds_status_log wfsl 
			INNER JOIN web_feeds wf ON wfsl.id_web_feed = wf.id_web_feed
			WHERE wfsl.pull_date >= '$from_date' AND wfsl.pull_date < '$to_date'
			GROUP BY wf.title, DATE( wfsl.pull_date ) DESC
            ";
       
        $num = $db->QueryExe($rows, $sqlstr, $paged);

        return $num;

    }

    /*
    RSS Reporting : Feed performance below threshold
    */
    public function WebFeedPerformanceBelowThresholdGet(&$rows, $from_date, $to_date, $num_items, $paged=true)
    {
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "
            SELECT wfsl.id_web_feed, wf.title, wfsl.inserted, wfsl.pull_date, wfsl.status
            FROM web_feeds_status_log wfsl
            INNER JOIN web_feeds wf
            ON wfsl.id_web_feed = wf.id_web_feed
            WHERE wfsl.pull_date >= '$from_date' 
            AND wfsl.pull_date < '$to_date'
            AND wfsl.inserted < '$num_items'
            ORDER BY wf.title, pull_date desc
            ";
        return $db->QueryExe($rows, $sqlstr, $paged);
    }

    public function CheckValidWebFeedUrl($url)
    {
    	$fm = new FileManager();
    	$content = $fm->Browse($url,false,true);

        if($content!="")
        {
            try
            {
            	libxml_clear_errors();
               	libxml_use_internal_errors(true);
 				if( ! simplexml_load_string( $content ) ) 
    				return false; 

                $this->xmlElement = new SimpleXmlElement($content);
            
                if (isset($this->xmlElement['version']))
                {
                    return $this->VerifyFeed20();
                }
                else if (isset($this->xmlElement->item))
                {
                    return $this->VerifyFeed10();
                }
                else if (isset($this->xmlElement->entry->link['href']))
                {
                    return $this->VerifyFeedATOM();
                }
                else
                {
                    return false;                
                }
            }
            catch (Exception $ex)
            {
                return false;
            }    
        }
        else
        {
            return false;
        }
    }
    
    private function FormatDate($date)
    {
		$data = explode('-', $date);
		$year = $data[0];
		$month = $data[1];
		$day = $data[2];
		if (strlen($month) == 1)
			$month = '0' . $month;
		if (strlen($day) == 1)
			$day = '0' . $day;
		return $year . '-' . $month . '-' . $day;
    }
    
    private function VerifyFeed10() 
    {
        if (count($this->xmlElement->item) == 0)
            return false;
        else
            return true;
    }
    
    private function VerifyFeed20() 
    {
        if (count($this->xmlElement->channel->item) == 0)
            return false;
        else
            return true;
    }
    
    private function VerifyFeedATOM() 
    {
        if (count($this->xmlElement->entry) == 0)
            return false;
        else
            return true;
    }

}

class WebFeedsImporter
{
	public $insertedCount;
	public $updatedCount;
	private $xmlElement;
	
	/** 
	 * @var Log */
	private $log;
	private $feed_items_validate_per_cron_job;
	private $feeds_import_per_cron_job;
	
	/** 
	 * @var History */
	private $h;
	private $id_res_type;
	
	const MinimumNumberOfFeedItemsNotToBeDeleted = 10; //days
	const WebFeeItemsTTL = 30; //days

	function __construct() 
	{
		$this->id_res_type = 25;
		$this->insertedCount = 0;
		$this->updatedCount = 0;
        $this->log = new Log();
        $conf = new Configuration();
        $this->feed_items_validate_per_cron_job = $conf->Get("feed_items_validate_per_cron_job");
        $this->feeds_import_per_cron_job = $conf->Get("feeds_import_per_cron_job");
        $this->h = new History();
	}
	
	public function ImportRSSFeeds() 
	{
        $sql = "
            SELECT feed_url, id_web_feed, update_interval
            FROM web_feeds 
            WHERE MINUTE(TIMEDIFF(NOW() , last_updated)) + (HOUR(TIMEDIFF(NOW() , last_updated)) * 60) >= update_interval OR last_updated = '0000-00-00 00:00:00'
            ORDER BY last_updated
            LIMIT $this->feeds_import_per_cron_job
        ";
        $rows = array();
        $db =& Db::globaldb();
        $db->QueryExe($rows, $sql);
        
        foreach($rows as $row)
        {
        	if($row['feed_url']!="")
        	{
				$this->ImportFeed($row['feed_url'], $row['id_web_feed'], $row['update_interval']);    		
        	}
        }
	}
	
	public function ImportFeed($url, $id_web_feed, $update_interval) 
	{
		$this->insertedCount = 0;
		$this->updatedCount = 0;
		$deletedCount = 0;
		include_once(SERVER_ROOT."/../classes/cache.php");
		$cache = new Cache();
		$cache->ttl = $update_interval;
		$content = $cache->Get("rss",$url);
    	
		if($content!="")
		{
			try
			{
				libxml_clear_errors();
            	libxml_use_internal_errors(true);
 				if( ! simplexml_load_string( $content ) ) 
                {
		            include_once(SERVER_ROOT."/../classes/ini.php");
		            $ini = new Ini();
		            $feed_url = $ini->Get("admin_web") . "/homepage/web_feed.php?id=$id_web_feed";
		            $this->log->Write('WARNING', "Web Feed Importer: \"$url\" is not a valid feed URL - See $feed_url");
					$this->UpdateFeedLastUpdated($id_web_feed);
					$this->InsertFeedStatus($id_web_feed, 0, 0, 0, 0);	
                }
				else
				{
					$this->xmlElement = new SimpleXmlElement($content);
				    $updated_date = date("Y-m-d H:i");
	                
					if (isset($this->xmlElement['version']))
					{
						$this->ImportFeed20($id_web_feed, $updated_date);
					}
					else if (isset($this->xmlElement->item))
					{
						$this->ImportFeed10($id_web_feed, $updated_date);
					}
					else if (isset($this->xmlElement->entry->link['href']))
					{
						$this->ImportFeedATOM($id_web_feed, $updated_date);
					}
					
					$this->UpdateFeedLastUpdated($id_web_feed);
                    $this->UpdateFeedItemSort($id_web_feed);
					if(($this->insertedCount + $this->updatedCount + $deletedCount)>0)
					{
						$this->log->Write('INFO', "Web Feed Importer: Inserted {$this->insertedCount}, updated {$this->updatedCount} and deleted {$deletedCount} record(s) for \"{$url}\"");
					}
					$this->InsertFeedStatus($id_web_feed, 1, $this->insertedCount, $this->updatedCount, $deletedCount); 
				}
			}
			catch (Exception $ex)
			{
                $this->log->Write('WARNING', "Web Feed Importer: " . $ex->getMessage());
				$this->UpdateFeedLastUpdated($id_web_feed);
                $this->InsertFeedStatus($id_web_feed, 0, 0, 0, 0);
			}	
			
		}
		else
		{
            include_once(SERVER_ROOT."/../classes/ini.php");
            $ini = new Ini();
            $feed_url = $ini->Get("admin_web") . "/homepage/web_feed.php?id=$id_web_feed";
            $this->log->Write('WARNING', "Web Feed Importer: \"$url\" is not a valid feed URL - See $feed_url");
			$this->UpdateFeedLastUpdated($id_web_feed);
			$this->InsertFeedStatus($id_web_feed, 0, $this->insertedCount, $this->updatedCount, $deletedCount);
		}
	}

	public function RemoveExpiredFeedItems() 
	{
        $sql = "
            SELECT id_web_feed FROM web_feeds
        ";
        $rows = array();
        $db =& Db::globaldb();
        $db->QueryExe($rows, $sql);
        foreach($rows as $row)
        {
			$this->RemoveExpireFeedItemsByFeed($row['id_web_feed']);
        }
	}
	
	private function RemoveExpireFeedItemsByFeed($id_web_feed) 
	{
		$value = '';
        $sql = "
            SELECT id_web_feed_item 
            FROM web_feed_items 
            WHERE id_web_feed={$id_web_feed} AND DATEDIFF(NOW(),last_updated) > " . self::WebFeeItemsTTL . "
            ORDER BY last_updated DESC, id_web_feed_item DESC LIMIT " . self::MinimumNumberOfFeedItemsNotToBeDeleted ;
        $rows = array();
        $db =& Db::globaldb();
        $db->QueryExe($rows, $sql);
        if(count($rows)>0)
        {
	        foreach($rows as $row)
	        {
				$value .= $row['id_web_feed_item'].',';
	        }
			$value = rtrim($value, ",");

			$feed_items_to_delete = array();
            $sql = "
                SELECT id_web_feed_item 
                FROM web_feed_items 
                WHERE id_web_feed={$id_web_feed} AND DATEDIFF(NOW(),last_updated) > " . self::WebFeeItemsTTL . " AND id_web_feed_item NOT IN (".$value. ")
            ";
            $db->QueryExe($feed_items_to_delete, $sql);
            if(count($feed_items_to_delete)>0)
            {
	            $db->begin();
	            $db->locktables(array("web_feed_items_content", "web_feed_items"));
	            foreach($feed_items_to_delete as $feed_item)
	            {
	    	        $sql = " DELETE FROM web_feed_items_content WHERE id_web_feed_item='{$feed_item['id_web_feed_item']}'";
                    $res[] = $db->query( $sql );
	    	        $sql = " DELETE FROM web_feed_items WHERE id_web_feed_item='{$feed_item['id_web_feed_item']}'";
	            	$res[] = $db->query( $sql );
	            }
	            Db::finish( $res, $db);
				
				include_once(SERVER_ROOT."/../classes/search.php");
				$s = new Search();
	            foreach($feed_items_to_delete as $feed_item)
	            {
					$this->h->HistoryDelete($this->id_res_type,$feed_item['id_web_feed_item']);
					$s->ResourceRemove($this->id_res_type,$feed_item['id_web_feed_item']);
				}        	
            }
        }
	}
	
	private function ImportFeed10($id_web_feed, $updated_date) 
	{
		foreach ($this->xmlElement->item as $value) 
		{
            $image_url = $this->getImageUrl((isset($value->description)) ? $value->description : '');
			$this->SaveFeedItem($id_web_feed, $value->title, $value->link, $image_url, (isset($value->description)) ? $value->description : '', '0000-00-00 00:00:00', '', $updated_date);
		}
	}
	
	private function ImportFeed20($id_web_feed, $updated_date) 
	{
		foreach ($this->xmlElement->channel->item as $value) 
		{
            $image_url = isset($value->children('media', true)->content) ? $value->children('media', true)->content->attributes()->url : '';
            if ($image_url == '') {
                $image_url = $this->getImageUrl((isset($value->description)) ? $value->description : '');
            }
            
			$this->SaveFeedItem($id_web_feed, $value->title, $value->link, $image_url, $value->description, (isset($value->pubDate)) ? date("Y-m-d H:i", strtotime($value->pubDate)) :'0000-00-00 00:00:00', '', $updated_date);
		}
	}
	
	private function ImportFeedATOM($id_web_feed, $updated_date) 
	{
		foreach ($this->xmlElement->entry as $value) 
		{
            $description = $value->summary;
            $image_url = $this->getImageUrl($description);
            if ($image_url=='')
                $image_url = $this->getImageUrl($description->asXML());
            if ($image_url=='')
                $image_url = $this->getImageUrl((isset($value->content)) ? $value->content : '');
            else
                $description = $description->asXML();
			$this->SaveFeedItem($id_web_feed, $value->title, $value->link['href'], $image_url, $description, (isset($value->published)) ? date("Y-m-d H:i", strtotime($value->published)) : '0000-00-00 00:00:00', (isset($value->content)) ? $value->content : '', $updated_date);
		}
	}
    
    private function getImageUrl($html)
    {
    	$result = array();
        preg_match_all('/<img[^>]+>/i',$html, $result);
        $img = array(); 
        foreach( $result as $img_tag) 
        { 
            preg_match_all('/(src)=("[^"]*")/i',$img_tag[0], $img); 
            if (count($img) > 0)
            {
                return rtrim(ltrim($img[0][0], "src=\""), "\"");
            }
        }
        return '';
    }
    
    private function Replace_Javascript($html)
    {
//        preg_match_all('/<script\b[^>]*>(.*?)<\/script>/is',$var, $result);
//        foreach( $result as $script_tag) 
//        { 
//            return count($script_tag) > 0;
//        }
        return preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $html); 
    }
    
    private function StripTags($html)
    {
        return strip_tags(html_entity_decode($html));
    }
	
	private function InsertFeedStatus($id_web_feed, $status, $inserted, $updated, $deleted) 
	{
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "web_feeds_status_log" );

        $id_web_feed_status = $db->nextId( "web_feeds_status_log", "id_web_feed_status" );
        $sql = "
            INSERT INTO web_feeds_status_log (id_web_feed_status, id_web_feed, pull_date, status, inserted, updated, deleted)
             VALUES ({$id_web_feed_status}, {$id_web_feed}, NOW(), {$status}, {$inserted}, {$updated}, {$deleted})
        ";
        $res[] = $db->query( $sql );
        Db::finish( $res, $db);
	}
	
	public function FeedStatusDeleteOld()
	{
		$sqlstr = "DELETE FROM web_feeds_status_log WHERE DATE_ADD(pull_date, INTERVAL " . self::WebFeeItemsTTL  . " DAY)<CURRENT_TIMESTAMP";
        $db =& Db::globaldb();
		$db->begin();
		$db->lock( "web_feeds_status_log" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function UpdateFeedLastUpdated($id_web_feed) 
	{
        $db =& Db::globaldb();
        $todaytime = $db->getTodayTime();
        $db->begin();
        $db->lock( "web_feeds" );

        $sql = "
            UPDATE web_feeds SET last_updated='$todaytime' WHERE id_web_feed='$id_web_feed'
        ";
        $res[] = $db->query( $sql );
        Db::finish( $res, $db);
	}
    
    private function UpdateFeedItemSort($id_web_feed) 
    {
        $db =& Db::globaldb();
        $sql = "
            UPDATE (SELECT wfi.id_web_feed_item,@rownum:=@rownum+1 AS sort FROM (SELECT @rownum:=0) r, web_feed_items wfi WHERE wfi.id_web_feed='$id_web_feed' ORDER BY wfi.last_updated DESC, wfi.id_web_feed_item) r INNER JOIN web_feed_items wfi ON wfi.id_web_feed_item = r.id_web_feed_item SET wfi.sort=r.sort
        ";
        $db->query( $sql );
    }
	
	private function GetFeedItemFromURL($id_web_feed, $title, $link_url) 
	{
            
        $row = array();
        $db =& Db::globaldb();
		$link_url = $db->SqlQuote($link_url);
		$title = $db->SqlQuote($title);
        $sql = "
            SELECT id_web_feed_item FROM web_feed_items WHERE title='{$title}' AND id_web_feed={$id_web_feed}
        ";
        $db->query_single($row, $sql);

		if (count($row) > 0) 
		{
			return $row['id_web_feed_item'];
		}
		return null;
	}
	
	private function SaveFeedItem($id_web_feed, $title, $link_url, $image_url, $description, $published_date, $content, $updated_date) 
	{
        include_once(SERVER_ROOT."/../classes/texthelper.php");
        $th = new TextHelper();
        $th->Html2Text($title);
        $th->Html2Text($description);
        $th->Html2Text($content);
        
        if ($this->ContainsBlockedWord(array($title, $description, $content)))
        {
            $is_approved = 0;
            $is_filtered = 1;
        }
        else
        {
            $is_approved = 1;
            $is_filtered = 0;
        }
        
        $id_web_feed_item = $this->GetFeedItemFromURL($id_web_feed, $title, $link_url);
		if (isset($id_web_feed_item))
		{
			$this->updatedCount += $this->UpdateFeedItem($id_web_feed_item, $title, $link_url, $image_url, $description, $published_date, $content, $is_approved, $is_filtered, $updated_date);
		}
		else
		{
			$this->InsertFeedItem($id_web_feed, $title, $link_url, $image_url, $description, $published_date, $content, $is_approved, $is_filtered, $updated_date);
			$this->insertedCount++;
		}
	}
	
	private function InsertFeedItem($id_web_feed, $title, $link_url, $image_url, $description, $published_date, $content, $is_approved, $is_filtered, $updated_date) 
	{
        $status = 1;
        $db =& Db::globaldb();
        $db->begin();
        $db->locktables(array("web_feed_items", "web_feed_items_content"));

        $id_web_feed_item = $db->nextId( "web_feed_items", "id_web_feed_item" );
        $title = $db->SqlQuote($title);
        $link_url = $db->SqlQuote($link_url);
        $image_url = $db->SqlQuote($image_url);
        $content = $db->SqlQuote($content);
        $description = $db->SqlQuote($description);
        $sql = "
            INSERT INTO web_feed_items (id_web_feed_item, id_web_feed, title, link_url, image_url, published_date, last_updated, is_approved, is_filtered, status)
            VALUES ({$id_web_feed_item}, {$id_web_feed}, '{$title}', '{$link_url}', '{$image_url}', '{$published_date}', '{$updated_date}', {$is_approved}, {$is_filtered}, {$status})
        ";
        $res[] = $db->query( $sql );

        $sql = "
            INSERT INTO web_feed_items_content (id_web_feed_item, content, description)
             VALUES ({$id_web_feed_item}, '{$content}', '{$description}')
        ";
        $res[] = $db->query( $sql );

        Db::finish( $res, $db);
        
		$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["create"]);
		if($is_approved && !$is_filtered)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd($this->id_res_type,$id_web_feed_item,0,0,1);
		}
		if($is_filtered)
		{
			$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["on_hold"]);
		}
	}
	
	private function UpdateFeedItem($id_web_feed_item, $title, $link_url, $image_url, $description, $published_date, $content, $is_approved, $is_filtered, $updated_date) 
	{
        $status = 1;
        $db =& Db::globaldb();
        $db->begin();
        $db->locktables(array("web_feed_items", "web_feed_items_content"));

        
        $title = $db->SqlQuote($title);
        $link_url = $db->SqlQuote($link_url);
        $image_url = $db->SqlQuote($image_url);
        $content = $db->SqlQuote($content);
        $description = $db->SqlQuote($description);
        // always update the status (approved OR fitlered) because admin can update bad words while content of rss not changed
        $sql = "UPDATE web_feed_items
        		SET is_approved = '$is_approved',
        			is_expired = '0'
        		WHERE id_web_feed_item = '$id_web_feed_item'
        ";
        $res[] = $db->query( $sql);
        
        $sql = "
            UPDATE web_feed_items_content 
            SET content = '{$content}',
            	description = '{$description}' 
            WHERE id_web_feed_item = {$id_web_feed_item} 
            AND (content <> '{$content}' OR description <> '{$description}')
        ";
        $res[] = $db->query( $sql, true );
        if ($db->affected_rows > 0)
        {
            $sql = "
                UPDATE web_feed_items SET title = '{$title}', link_url = '{$link_url}', image_url = '{$image_url}', published_date = '{$published_date}', is_expired = 0, last_updated = '{$updated_date}', status = {$status}
                WHERE id_web_feed_item = {$id_web_feed_item}
            ";
        }
        else
        {
            $sql = "
                UPDATE web_feed_items SET title = '{$title}', link_url = '{$link_url}', image_url = '{$image_url}', published_date = '{$published_date}', is_expired = 0, last_updated = '{$updated_date}', status = {$status}  
                WHERE id_web_feed_item = {$id_web_feed_item} AND (title <> '{$title}' OR link_url <> '{$link_url}' OR image_url <> '{$image_url}' OR published_date <> '{$published_date}')
            ";
        }

        $res[] = $db->query( $sql, true );
        Db::finish( $res, $db);

		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($is_filtered)
		{
			$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["on_hold"],true);
			$s->ResourceRemove($this->id_res_type,$id_web_feed_item);
		}
		else if($is_approved)
		{
			$this->h->HistoryAdd($this->id_res_type,$id_web_feed_item,$this->h->actions["update"],true);
			$s->IndexQueueAdd($this->id_res_type,$id_web_feed_item,0,0,1);
		}
        
        return $db->affected_rows;
	}
    
    private function ContainsBlockedWord($texts)
    {
        include_once(SERVER_ROOT."/../classes/texthelper.php");
        $th = new TextHelper();
        
        foreach($texts as $text)
        {
            if ($th->ContainsBlockedWord($text))
            {
                return true;
            }
        }
        return false;
    }
	
    public function ValidateWebFeedItems()
    {
        include_once(SERVER_ROOT."/../classes/link.php");
        $link = new Link(0);

        $db =& Db::globaldb();
        $sql = "
            SELECT id_web_feed_item, link_url 
            FROM web_feed_items 
            ORDER BY last_checked LIMIT {$this->feed_items_validate_per_cron_job} ";
        $rows = array();
        $db->QueryExe($rows, $sql);
        
        $items_status = array();

        foreach ($rows as $row)
        {
            $link->url = $row['link_url'];
            $link->Check();
            $error = $link->error;
            $items_status[] = array('id_web_feed_item'=>$row['id_web_feed_item'],'status'=>(($error==0)? 1 : 0));
        }
        $db->begin();
        $db->LockTables(array("web_feed_items"));
        foreach($items_status as $item_status)
        {
            $sql = "
                UPDATE web_feed_items
                SET last_checked = NOW(),
                    status = '{$item_status['status']}'
                WHERE id_web_feed_item = '{$item_status['id_web_feed_item']}'
            ";
            $res[] = $db->query( $sql );
        }
        Db::finish( $res, $db);
    }
    
	function __destruct() 
	{
	}		
}

?>
