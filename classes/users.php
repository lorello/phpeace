<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

// days activity logged
define('LOG_DAYS',60);

// days of inactivity before disabling users (2 years)
define('DISABLE_DAYS',720);

class Users
{
	public function Active()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_user,name FROM users WHERE active=1 AND id_user>0 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function AdminUsers($id_module)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT u.id_user,u.name,u.email 
		FROM users u
		INNER JOIN module_users mu ON u.id_user=mu.id_user
		WHERE u.id_user>0 AND u.active=1 AND mu.id_module='$id_module' AND mu.is_admin=1 ORDER BY u.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function AllUsers()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr  = "SELECT id_user,name,connections FROM users  WHERE id_user>0 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function AllUsersP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT users.id_user,name,phone,mobile,email,active,admin_notes,staff_id FROM users WHERE id_user>0 ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function Available() // Only for LDAP
	{
		$available = array();
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$ldap_config = $conf->Get("ldap");
		$connect= ldap_connect("ldap://".$ldap_config['server']);
		if ($connect)
		{
			$bind = ldap_bind($connect,$ldap_config['username'] . "@" . $ldap_config['domain'], $ldap_config['password']);
			if ($bind)
			{
				ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
				ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
				$search = ldap_search($connect,$ldap_config['base'],$ldap_config['filter'],$ldap_config['attributes']);
				$users = ldap_get_entries($connect, $search);
				@ldap_close($connect);
				if($users['count']>0)
				{
					asort($users);
					foreach($users as $user)
					{
						if($user['samaccountname'][0]!="" && $user['name'][0]!="" && $user['mail'][0]!="")
						{
							if (!$this->IsHere($user['samaccountname'][0]))
								$available[] = array('name'=>$user['name'][0],'email'=>$user['mail'][0],'login'=>$user['samaccountname'][0]);
						}
					}
				}
				else
					UserError("LDAP query returned no results",array());
			}
		}
		return $available;
	}

	public function ConfigurationUpdate($path,$user_show)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->Set("user_show",$user_show);
		if($user_show=="0")
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "users" );
			$res[] = $db->query( "UPDATE users SET user_show='0' " );
			Db::finish( $res, $db);
		}
		$ini->SetPath("users_path",$path);
	}

	public function GetLog( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "	SELECT users.name,UNIX_TIMESTAMP(users_log.login) AS login_ts,ip,user_agent
				FROM users_log
				INNER JOIN users ON users_log.id_user=users.id_user
				ORDER BY users_log.login DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	private function IsHere($login)
	{
		$is_here = false;
		include_once(SERVER_ROOT."/../classes/user.php");		
		$u = new User();
		$u->id = 0;
		$u->UserSet($login);
		if ($u->id >0)
			$is_here = true;
		return $is_here;
	}

	public function LastLogin()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT UNIX_TIMESTAMP(login) AS last_login from users_log ORDER BY login DESC LIMIT 1");
		return $row['last_login'];
	}

	public function LogClean()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT u.id_user,COUNT(ul.id_user_log) AS prev_conn,u.connections 
		FROM users_log ul LEFT JOIN users u ON ul.id_user=u.id_user 
		WHERE DATE_ADD(ul.login, INTERVAL " . LOG_DAYS . " DAY)<CURRENT_TIMESTAMP GROUP BY u.id_user";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $user)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "users" );
			$res[] = $db->query( "UPDATE users SET connections=" . ($user['connections'] + $user['prev_conn']) . " WHERE id_user='" . $user['id_user'] . "'" );
			Db::finish( $res, $db);
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users_log" );
		$res[] = $db->query( "DELETE FROM users_log WHERE DATE_ADD(login, INTERVAL " . LOG_DAYS . " DAY)<CURRENT_TIMESTAMP" );
		Db::finish( $res, $db);
	}
	
	public function ModuleUsers( $id_module )
	{
		include_once(SERVER_ROOT."/../classes/modules.php");
		$row = Modules::ModuleGet($id_module);
		$rows = array();
		if ($row['restricted']=="1")
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT users.id_user,name FROM users
				INNER JOIN module_users ON module_users.id_user=users.id_user
				WHERE users.active=1 AND users.id_user>0 AND module_users.id_module=$id_module 
				GROUP BY users.id_user ORDER BY name";
			$db->QueryExe($rows, $sqlstr);
		}
		else
			$rows = $this->Active();
		return $rows;
	}

	public function ModuleUsersAdmins( $id_module, $group )
	{
		if ($group=="user")
			$sqlstr = "SELECT u.id_user,u.name,id_module,is_admin FROM users u
			LEFT JOIN module_users mu ON u.id_user=mu.id_user AND mu.id_module=$id_module
			WHERE u.id_user>0 AND u.active=1 GROUP BY u.id_user ORDER BY u.name";
		else
			$sqlstr = "SELECT u.id_user,u.name,id_module,is_admin FROM users u
			INNER JOIN module_users mu ON u.id_user=mu.id_user AND mu.id_module=$id_module
			WHERE  u.id_user>0 AND u.active=1 GROUP BY u.id_user ORDER BY u.name";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function InactiveUsers()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT u.id_user,u.name,u.start_date,u.last_conn
		FROM users u
		WHERE u.active=1 
		AND (
			(start_date IS NULL AND last_conn IS NULL)
			OR (last_conn IS NOT NULL AND DATE_ADD(last_conn, INTERVAL " . DISABLE_DAYS . " DAY) < CURRENT_DATE )
		)
		ORDER BY u.id_user";
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function NonActive( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT u.id_user,u.name,COUNT(id_article) AS counter FROM users u
		LEFT JOIN articles a USING(id_user) WHERE u.active=0 AND u.id_user>0 GROUP BY u.id_user ORDER BY u.name ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Search( &$rows, $name )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT users.id_user,name,phone,mobile,email,active
		FROM users WHERE id_user>0 AND name LIKE '%$name%' ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function TopicUsersAdmins( $id_topic )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT u.id_user,u.name,tu.id_topic,tu.is_admin,tu.is_contact,tu.pending_notify 
			FROM users u
			LEFT JOIN topic_users tu ON u.id_user=tu.id_user AND tu.id_topic=$id_topic
			WHERE u.id_user>0 AND u.active=1 GROUP BY u.id_user ORDER BY u.name;";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function UserByEmail($unescaped_email)
	{
		$db =& Db::globaldb();
		$email = $db->SqlQuote($unescaped_email);
		$row = array();
		$sqlstr = "SELECT id_user,name FROM users WHERE id_user>0 AND active=1 AND email='$email' LIMIT 1";
		$db->query_single( $row, $sqlstr);
		return $row;
	}

}
?>
