<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/varia.php");

class Log
{
	
	public $levels;
	
	/**
	 * @var FileManager */
	private $fm;
	
	private $max_size;
	
	private $separator = "\n##############################\n\n";
	
	private $filename;
	
	private $log_level;
	
	private $syslog;
	
	private $archive;
	
	private $fatal_error;
	
	function __construct($fatal_error=false)
	{
		$this->filename = $this->LogFilename(time());
		$this->fatal_error = $fatal_error;
		$this->levels = array('Info','Scheduler','Warning','Error');
		if($fatal_error)
		{
			// Do not instantiate $ini and $conf to avoid error loops
			$this->max_size = 20480;
			$this->syslog = false;
			$this->archive = false;
			$this->log_level = 1;
			$this->fm = new FileManager(false);
		}
		else 
		{
			$config = new Configuration();
			$this->max_size = $config->Get("error_log_size");
			$this->syslog = $config->Get("syslog");
			$this->archive = $config->Get("log_archive");
			$this->fm = new FileManager();
			$ini = new Ini();
			$this->log_level = (int)$ini->Get("log_level");
			unset($ini);
		}
	}
	
	private function CheckSize()
	{
		$check = true;
		$info = $this->fm->FileInfo($this->filename);
		if ($info['kb'] > $this->max_size)
			$check = false;
		return $check;
	}
	
	private function LogDetailsText($errors, $ts) {
		$ini = new Ini();
		$msg = "### SUMMARY ###\n";
		$msg .= "Errors: " . ($errors['stats']['levels']['error'] + $errors['stats']['levels']['user error']) . "\n";
		$msg .= "Warnings: " . ($errors['stats']['levels']['warning'] + $errors['stats']['levels']['user warning']) . "\n";
		$msg .= "Scheduler: {$errors['stats']['levels']['scheduler']}\n";
		$msg .= "Info: {$errors['stats']['levels']['info']}\n";
		$msg .= "\nTotal: {$errors['stats']['num']}\n";
		$msg .= $this->separator;
		include_once (SERVER_ROOT . "/../classes/htmlhelper.php");
		$hh = new HtmlHelper( false );
		foreach($errors as $error_detail) {
			if (isset( $error_detail['ts'] ) && $error_detail['ts'] > 0 && is_array( $error_detail['error'] )) {
				$error = $error_detail['error'];
				$msg .= $hh->FormatDateTime( $error['ts'] ) . "\n";
				$msg .= "{$error['type']}" . ($error['type_num'] != ""? " ({$error['type_num']})" : "") . "\n";
				$msg .= $error['msg'] . "\n";
				if ($error['filename'] != "")
					$msg .= "in: {$error['filename']}, line {$error['linenum']}\n";
				if (isset( $error['user_vars'] )) {
					$msg .= "\n";
					foreach($error['user_vars'] as $key => $val) {
						if ($key != "password" && $key != "is_fatal") {
							$msg .= (strpos( $key, "pub_msg" ) === false)? "$key: " : "- ";
							$msg .= nl2br( htmlspecialchars( trim( var_export( $val, true ) ) ) ) . "\n";
						}
					}
				}
				if (isset( $error['stack'] ) && is_array( $error['stack'] ) && count( $error['stack'] ) > 0) {
					$msg .= "\n# STACK\n";

					foreach($error['stack'] as $call) {
						if (isset( $call['file'] ))
							$msg .= $call['file'];
						if (isset( $call['line'] ))
							$msg .= " line " . $call['line'];
						$msg .= " - {$call['function']}";
						if (count( $call['args'] ) > 0) {
							$msg .= " (" . ErrorStackCallArgs( $call['args'] ) . ")";
						}
						$msg .= "\n";
					}
				}
				if (isset( $error['server_vars'] ) && is_array( $error['server_vars'] ) && count( $error['server_vars'] ) > 0) {
					$msg .= "\n# SERVER VARS\n";
					foreach($error['server_vars'] as $varname => $value) {
						$msg .= "$varname: " . var_export( $value, true ) . "\n";
					}
				}

				if (isset( $error['post'] ) && is_array( $error['post'] ) && count( $error['post'] ) > 0) {
					$msg .= "\n# POST VARS\n";
					foreach($error['post'] as $key => $value) {
						$msg .= "$key: <code>" . var_export( $value, true ) . "\n";
					}
				}
				if (isset( $error['get'] ) && is_array( $error['get'] ) && count( $error['get'] ) > 0) {
					$msg .= "\n# GET VARS\n";
					foreach($error['get'] as $key => $value) {
						$msg .= "$key: <code>" . var_export( $value, true ) . "\n";
					}
				}
				$msg .= $this->separator;
			}
		}
		$msg .= "Daily log " . date( 'd/m/Y', $ts ) . "\n";
		$msg .= "PhPeace \"" . $ini->Get( "title" ) . "\"\n";
		$msg .= "Version " . PHPEACE_VERSION . " (Build " . PHPEACE_BUILD . ")\n";
		$msg .= $ini->Get( "admin_web" ) . "\n\n";
		return $msg;
	}
	
	public function Items($filename="")
	{
		if($filename=="")
			$filename = $this->filename;
		$log_content = $this->fm->TextFileRead($this->filename);
		$error_items = array();
		$counter = 1;
		$stats_levels = array('error'=>0,'user error'=>0,'warning'=>0,'user warning'=>0,'user notice'=>0,'info'=>0,'scheduler'=>0);
		$log_lines = explode("|X|X|X|\n",$log_content);
		if(count($log_lines)>0)
		{
			$v = new Varia();
			foreach($log_lines as $log_line)
			{
				$log_line_details = explode("|||",$log_line);
				$ts = (int)$log_line_details[0];
				$error = $v->Deserialize($log_line_details[1],false);
				if($ts>0 && is_array($error))
				{
					$error_type = strtolower($error['type']);
					$error_items[$counter] = array('ts'=>$ts,'error'=>$error);
					$stats_levels[$error_type] = $stats_levels[$error_type] + 1;
					$counter++;			
				}
			}
		}
		$error_items['stats'] = array('num'=>$counter-1,'levels'=>$stats_levels);
		$error_items['stats']['num_real'] = $stats_levels['error'] + $stats_levels['user error'] + $stats_levels['warning'] + $stats_levels['user warning'] + $stats_levels['info'];  
		return $error_items;
	}
	
	private function LogFilename($ts)
	{
		return "log/" . date("Y_m_d",$ts) . ".txt";
	}
	
	public function Send($wipe=true,$day=-1)
	{
		$yesterday_ts = mktime(0, 0, 0, date("m")  , date("d") + $day, date("Y"));
		$yesterday_log = $this->LogFilename($yesterday_ts);
		if($this->fm->Exists($yesterday_log))
		{
			$file_info = $this->fm->FileInfo($yesterday_log);
			if ($file_info['size'] > 0)
			{
				$items = $this->Items($yesterday_log);
				if(is_array($items) && count($items)>0 && $items['stats']['num_real']>0)
				{
					$message = $this->LogDetailsText($items,$yesterday_ts);
					include_once(SERVER_ROOT."/../classes/users.php");
					include_once(SERVER_ROOT."/../classes/mail.php");
					$users = new Users;
					$mail = new Mail();
					$subject = "[{$mail->title}] Daily log " . date('d/m/Y',$yesterday_ts);
					if($file_info['kb'] >= $this->max_size)
						$subject .= " [FULL {$file_info['kb']}/{$this->max_size}]";
					$phpeace_admins = $users->AdminUsers(17);
					foreach($phpeace_admins as $user)
					{
						$mail->SendMail($user['email'],$user['name'],$subject,$message, array());
					}
				}
				if($wipe)
				{
					$this->fm->DirAction("log/archive","check");
					$dirfiles = $this->fm->DirFiles("log");
					if(count($dirfiles)>0)
					{
						include_once(SERVER_ROOT."/../classes/varia.php");
						$va = new Varia();
						foreach($dirfiles as $dirfile)
						{
							$logfile = str_replace($this->fm->path,"",$dirfile);
							$logfile = trim($logfile,"/");
							if($logfile!=$this->filename)
							{
								if($this->archive)
								{
									$archive_logfile = str_replace("log/","log/archive/",$logfile);
									$this->fm->Rename($logfile,$archive_logfile);
									$command = "gzip " . $this->fm->RealPath($archive_logfile);
									$va->Exec($command);
								}
								else 
								{
									$logfile = str_replace("log/","",$logfile);
									$this->fm->Delete("log/$logfile");
								}
							}
						}
					}
				}
			}
		}
	}
	
	private function Read()
	{
		return $this->fm->TextFileRead($this->filename);
	}
	
	public function Store($errarray)
	{
		if ($this->CheckSize())
		{
			$v = new Varia();
			$level = strtoupper($errarray['type']);
			if(($level=="INFO" && $this->log_level<1) || (strpos($level,"SCHEDULER")!==false && $this->log_level<2) || (strpos($level,"WARNING")!==false && $this->log_level<3) || (strpos($level,"ERROR")!==false))
			{
				if(!$this->fatal_error)
				{
					$session = new Session();
					$current_user_login = $session->Get("current_user_login");
					if($current_user_login!="")
					{
						$errarray['user'] = $current_user_login;
					}
				}
				$errarray['phpeace'] = array('version'=>PHPEACE_VERSION,'build'=>PHPEACE_BUILD);
				$errarray['fatal'] = $this->fatal_error;
				$dump = $errarray['ts'] . "|||" . $v->Serialize($errarray,false,false) . "|X|X|X|\n";
				$this->fm->WritePage($this->filename,$dump,"a");	
			}
		}
		if($this->syslog && (strpos($level,"WARNING")!==false && $this->log_level<3) || (strpos($level,"ERROR")!==false))
		{
			openlog("phpeace",LOG_ODELAY,LOG_USER);
			$priority = strpos($level,"ERROR")!==false? LOG_ERR : LOG_WARNING;
			$path = basename($this->fm->path);
			$message = "[{$path}]: $level - " . preg_replace("/[\n\r]/"," ",$errarray['msg']);
			syslog($priority,$message);
			closelog();
		}
	}

	public function Wipe($ts=0)
	{
		if($ts==0)
			$ts = time();
		$this->fm->Delete($this->LogFilename($ts));
	}
	
	public function Write($level,$message,$vars=array())
	{
		$errarray = array();
		$errarray['type'] = strtoupper($level);
		$errarray['ts'] = time();
		$errarray['msg'] = $message;
		$errarray['datetime'] = date("d.m.Y H:i:s (T)");
		if(count($vars)>0)
			$errarray['user_vars'] = $vars;
		$this->Store($errarray);
	}
	
}
?>
