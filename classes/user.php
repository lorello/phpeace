<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
define('USERS_VERIFY_TOKEN_TTL',432000);

class User
{
	public $id;

	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$this->id = (int)$session->Get('current_user_id');
	}

	public function Articles( &$rows )
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$topic_temp = $ini->Get('temp_id_topic');
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_article,headline,UNIX_TIMESTAMP(a.written) AS written_ts,t.name AS topic_name, a.id_topic, a.id_subtopic,
		IF(a.show_author=1,IF(a.author<>'',a.author,u.name),'-') AS author,a.approved,a.id_image, 
        (SELECT COUNT(*) FROM docs_articles WHERE id_article = a.id_article) AS doc_attached, 
        SUBSTRING_INDEX(lu.aa, '-',1) AS last_updated_on,SUBSTRING_INDEX(lu.aa, '-',-1) AS last_updated_by,
        CASE WHEN CASE WHEN IFNULL(published, '0000-00-00 00:00:00') <> '0000-00-00 00:00:00' THEN '1' ELSE '0' END = '1' THEN 'Published' WHEN a.approved = '1' THEN 'Approved' ELSE 'Pending' END AS status 
		FROM articles a LEFT JOIN topics t ON a.id_topic=t.id_topic
		INNER JOIN users u ON a.id_user=u.id_user
        LEFT JOIN (SELECT h.id, CONCAT(UNIX_TIMESTAMP(MAX(h.time)), '-', usr.name) as aa FROM history h INNER JOIN users usr ON usr.id_user=h.id_user WHERE h.id_type=5 GROUP BY h.id) lu ON lu.id=a.id_article
		WHERE a.id_topic<>$topic_temp ";
		if($this->id>0)
			$sqlstr .= " AND a.id_user='$this->id' ";
		$sqlstr .= " ORDER BY a.written DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ArticlesAll()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.id_topic
			FROM articles a
			INNER JOIN topics t ON a.id_topic=t.id_topic
			WHERE a.id_user=$this->id
			AND a.id_topic<>" . $ini->Get('temp_id_topic') . " AND a.show_author=1 
			AND a.author='' AND a.approved=1 AND a.published>0 AND t.visible=1";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ArticlesVisible( &$rows )
	{
		$num = 0;
		if ($this->id > 0)
		{
			$row = $this->UserGet();
			$db =& Db::globaldb();
			$name = $db->SqlQuote($row['name']);
			$sqlstr = "SELECT a.id_article,headline,subhead,halftitle,UNIX_TIMESTAMP(written) AS written_ts,
				a.show_author,a.show_date,a.id_user,a.author,a.author_notes,a.available,
				a.id_topic,a.id_subtopic,'article' as item_type
				FROM articles a
				INNER JOIN articles_subhead USING(id_article)
				WHERE a.id_user=$this->id
				AND a.show_author=1 AND (a.author='' OR a.author LIKE '%$name%' ) AND a.approved=1 AND a.published>0
				ORDER BY a.written DESC";
			$num = $db->QueryExe($rows, $sqlstr, true);
		}
		return $num;
	}

	public function BannersGroups()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_group,name FROM banners_groups WHERE id_user=$this->id ORDER BY name ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function BounceIncrement($score)
	{
		$db =& Db::globaldb();
		$today = $db->getTodayTime();
		$db->begin();
		$db->lock( "users" );
		$res[] = $db->query( "UPDATE users SET bounces=bounces+$score,last_bounce='$today' WHERE id_user='{$this->id}' " );
		Db::finish( $res, $db);
		$user = $this->UserGetFull();
		return $user['bounces'];
	}

	public function TranslationsVisible( &$rows )
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT tra.id_translation,tra.id_language,tra.id_article_trad,UNIX_TIMESTAMP(tra.start_date) AS start_date_ts,tra.id_language_trad,
			a.id_article,a.halftitle,a.headline,ah.subhead,UNIX_TIMESTAMP(written) AS written_ts,'article' AS item_type,
			UNIX_TIMESTAMP(a.published) AS published_ts,a.id_subtopic,a.id_topic,a.available,a.show_author,a.show_date,
			a.id_user,a.author
			FROM translations tra
			INNER JOIN articles a ON tra.id_article_trad=a.id_article
			INNER JOIN topics t ON a.id_topic=t.id_topic AND t.visible=1
			INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
			INNER JOIN subtopics s ON a.id_subtopic=s.id_subtopic
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
			WHERE a.approved=1 AND a.published>0 AND s.visible=1 AND tra.id_user='$this->id' AND tra.completed=1 AND tra.translator=''
			ORDER BY a.published DESC,a.id_article DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function ChangeEmail( $password, $email )
	{
		$return = false;
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$credentials = $this->Credentials();
		$real_password = $credentials['password'];
		$sc = new Scrypt;
		$dpass = $sc->decrypt($real_password);
		if ($password == $dpass)
		{
			$email = trim($email);
			$this->EmailUnique($email);
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "users" );
			$res[] = $db->query( "UPDATE users SET email='$email' WHERE id_user=$this->id" );
			Db::finish( $res, $db);
			$return = true;
		}
		return $return;
	}

	public function ChangePassword( $old_password, $new_password, $isadmin )
	{
		include_once(SERVER_ROOT."/../classes/adminhelper.php");
		$ah = new AdminHelper;
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$credentials = $this->Credentials();
		$password = $credentials['password'];
		$sc = new Scrypt;
		$dpass = $sc->Decrypt($password);
		if ($old_password == $dpass || $isadmin)
		{
			$this->ChangePasswordOk($new_password);
			$ah->MessageSet("password_changed");
		}
		else
			$ah->MessageSet("password_change_failed");
	}

	private function ChangePasswordOk( $new_password )
	{
		include_once(SERVER_ROOT."/../classes/crypt.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$sc = new Scrypt;
		$cpass = $sc->Encrypt($new_password);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$res[] = $db->query( "UPDATE users SET password='$cpass' WHERE id_user=$this->id" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics;
		$tt->ProtectedUpdate($this->id);
	}

	public function Connections()
	{
		$row = $this->UserGet();
		$conn = array();
		$db =& Db::globaldb();
		$db->query_single( $conn, "SELECT COUNT(id_user_log) AS counter FROM users_log WHERE id_user='{$this->id}'");
		return (int)$conn['counter'] + (int)$row['connections'];
	}

	public function Credentials()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT password,login FROM users WHERE id_user='$this->id'");
		return $row;
	}

	public function CredentialsByLogin($login)
	{
		$row = array();
		$db =& Db::globaldb();
		$login = $db->SqlQuote($login);
		$db->query_single( $row, "SELECT password,id_user FROM users WHERE login='$login'");
		return $row;
	}

	public function Disable($id_user)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET active=0 WHERE id_user='$id_user' ";
		echo($sqlstr);
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function EmailUnique( $email )
    {
        $row = array();
        $db =& Db::globaldb();
        $db->query_single( $row, "SELECT id_user,name FROM users WHERE id_user>0 AND email='$email'");
        if ($row['id_user']>0)
        {
            include_once(SERVER_ROOT."/../classes/adminhelper.php");
            $ah = new AdminHelper;
            $ah->MessageSet("user_exists",array($row['id_user'],$row['name'],$email));
            return false;
        }
        return true;
    }

    public function UserUnique( $login )
    {
        $row = array();
        $db =& Db::globaldb();
        $db->query_single( $row, "SELECT id_user,name FROM users WHERE login='$login'");
        if ($row['id_user']>0)
        {
            include_once(SERVER_ROOT."/../classes/adminhelper.php");
            $ah = new AdminHelper;
            $ah->MessageSet("login_exists",array($login));
            return false;
        }
        return true;
    }

	public function Galleries()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_gallery,title FROM galleries WHERE id_user=$this->id ORDER BY released DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function GalleriesUser()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_gallery,title 
			FROM galleries g 
			LEFT JOIN topic_users tu ON g.id_topic=tu.id_topic AND tu.is_admin=1 AND tu.id_user=$this->id 
			LEFT JOIN topics t ON g.id_topic_group=t.id_group LEFT JOIN topic_users tu2 ON t.id_topic=tu2.id_topic AND tu2.is_admin=1 AND tu2.id_user=$this->id 
			WHERE g.topic_admin=1 AND g.id_user<>$this->id AND (tu.id_user=$this->id OR tu2.id_user=$this->id) 
			GROUP BY g.id_gallery 
			ORDER BY released DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ImageDelete()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$fm = new FileManager;
		$fm->Delete("uploads/users/orig/{$this->id}.jpg");
		$i->RemoveWrapper("user_image",$this->id);
		$fm->Delete("pub/{$i->pub_path}users/0/{$this->id}.jpg");
		if($i->isCDN) {
			$i->CDNReset('users',$this->id,$i->convert_format,true);
		}
		$fm->PostUpdate();
	}

	public function ImageUpdate($file)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/images.php");
		$fm = new FileManager;
		$this->ImageDelete();
		$origfile = "uploads/users/orig/{$this->id}.jpg";
		$fm->MoveUpload($file['temp'],$origfile);
		$i = new Images();
		if($i->isCDN) {
			$i->CDNReset('users',$this->id,$i->convert_format);
		}
		$i->ConvertWrapper("user_image",$origfile,$this->id);
		$filename = $this->id . "." . $i->convert_format;
		$fm->Copy("uploads/users/0/$filename","pub/{$i->pub_path}users/0/$filename");
		$fm->PostUpdate();
	}

	public function Lists()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_list,email FROM lists WHERE id_user=$this->id ORDER BY email";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function LoginFailureSet($username)
	{
		$user = array();
		$db =& Db::globaldb();
		$db->query_single( $user, "SELECT login_failures FROM users WHERE login='$username'");
		$failures = (int)($user['login_failures'] + 1);
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET login_failures='$failures',last_failure='" . $db->getTodayTime() . "' WHERE login='$username' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $failures;
	}
	
	public function LoginSuccessful()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET login_failures='0',last_failure='0' WHERE id_user='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function LoginUnique( &$login, $id_user )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_user FROM users WHERE id_user!=$id_user AND login='$login'");
		if ($row['id_user']>0)
		{
			$login .= "_x";
			include_once(SERVER_ROOT."/../classes/adminhelper.php");
			$ah = new AdminHelper;
			$ah->MessageSet("login_exists",array($login));
			$this->LoginUnique($login, $id_user);
		}
	}

	public function MessagesReceived( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT messages.id_message,
				IF(date_read=0,CONCAT('<b>',subject,'</b>'),subject) AS subject,
				UNIX_TIMESTAMP(sent) AS sent_ts,messages.id_sender
				FROM message_users
				INNER JOIN messages ON messages.id_message=message_users.id_message
				WHERE id_receiver=$this->id
				ORDER BY messages.sent DESC, messages.id_message DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function MessagesSent( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_message,subject,UNIX_TIMESTAMP(sent) AS sent_ts,id_user
				FROM messages
				WHERE id_sender=$this->id AND deleted=0
				ORDER BY messages.sent DESC, messages.id_message DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ModuleAdd( $id_module, $is_admin )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "module_users" );
		$sqlstr =  "INSERT INTO module_users (id_user,is_admin,id_module) VALUES ( $this->id, $is_admin, $id_module )";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function ModuleAdmin( $id_module )
	{
		$admin = array();
		$module_admin = false;
		$db =& Db::globaldb();
		$db->query_single( $admin, "SELECT id_user FROM module_users WHERE id_user='$this->id' AND is_admin=1 AND id_module='$id_module' ");
		if ($admin['id_user']>0)
			$module_admin = true;
		return $module_admin;
	}
	
	public function ModuleUserAdmin( $id_module_user, $id_module_admin )
	{
	    $user = array();
	    $db =& Db::globaldb();
	    $sqlstr = "SELECT id_user FROM module_users 
            WHERE id_user='$this->id' 
            AND (id_module='$id_module_user') OR (id_module='$id_module_admin' AND is_admin=1)  ";
	    $db->query_single( $user, $sqlstr);
        return isset($user['id_user']) && $user['id_user']>0;
	}
	
	public function ModuleUpdate($id_module,$is_user,$is_admin)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "module_users" );
		$sqlstr = "DELETE FROM module_users WHERE id_module=$id_module AND id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($is_user)
			$this->ModuleAdd($id_module,(int)$is_admin);
	}

	public function Modules()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT m.id_module,m.path,mu.is_admin,m.restricted
			FROM modules m
			LEFT JOIN module_users mu ON m.id_module=mu.id_module AND mu.id_user=$this->id 
			WHERE m.admin=1 AND (m.internal=1 OR (m.internal=0 AND m.active=1)) AND (mu.id_user=$this->id OR m.restricted=0) 
			GROUP BY m.id_module ORDER BY m.sort";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ModulesRight($restricted_only=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT m.id_module,m.restricted,mu.is_admin,mu.id_user
			FROM modules m
			LEFT JOIN module_users mu ON m.id_module=mu.id_module AND mu.id_user=$this->id
			WHERE m.admin=1 AND (m.internal=1 OR (m.internal=0 AND m.active=1)) ";
		if ($restricted_only)
			$sqlstr .= " AND m.restricted=1 ";
		$sqlstr .= "GROUP BY m.id_module ORDER BY m.sort";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
		
	}

	public function ModulesUser($admin_only=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT m.id_module,path
			FROM modules m 
			INNER JOIN module_users mu ON m.id_module=mu.id_module ";
			$sqlstr .= "WHERE m.active=1 AND mu.id_user=$this->id ";
		$sqlstr .= $admin_only? " AND mu.is_admin=1 " : " AND mu.is_admin=0 ";
		$sqlstr .= "GROUP BY m.id_module ORDER BY m.sort";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function NewMessages()
	{
		$messages = array();
		$db =& Db::globaldb();
		$db->query_single( $messages, "SELECT count(id_message) as counter FROM message_users WHERE id_receiver=$this->id AND NOT date_read>0");
		return $messages['counter'];
	}

	public function PubUserAssociate( $id_p, $password )
	{
		$associated = false;
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		if($pe->Auth($id_p,$password))
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "users" );
			$sqlstr = "UPDATE users SET id_p='$id_p' WHERE id_user=$this->id";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$associated = true;
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/adminhelper.php");
			$ah = new AdminHelper;
			$ah->MessageSet("user_no_auth");
		}
		return $associated;
	}
	
	public function PubUserRemove()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET id_p='0' WHERE id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function SendMessage($sender_name,$sender_email,$comments,$recipient_email,$recipient_name)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/varia.php");
		$mail = new Mail();
		$subject = "[INFO] $recipient_name";
		$msg = "Name: {$sender_name}\n";
		$msg .= "Email: {$sender_email}\n";
		$msg .= "\n\n";
		$msg .= $comments;
		$msg .= "\n\n--\n";
		$msg .= "Message sent from:\n";
		$msg .= "{$mail->pub_web}/tools/author.php?c={$this->id}\n";
		$msg .= "IP: " . Varia::IP() . "\n";
		$msg .= "Date: " . date("r") . "\n\n";
		$extra = array();
		$extra['name'] = $sender_name;
		$extra['email'] = $sender_email;
		$mail->SendMail($recipient_email,$recipient_name,$subject,$msg,$extra);
	}
	
	public function ServiceDelete($id_user_service)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "user_services" );
		$sqlstr = "DELETE FROM user_services 
			WHERE id_user_service='$id_user_service' AND id_user='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function ServiceUserGet($id_user_service)
	{
		$row = array();
        $db =& Db::globaldb();
		$sqlstr = "SELECT id_user_service,service_name,mobile,id_user
			FROM user_services 
			WHERE id_user_service='$id_user_service'  ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function ServiceRegister($service_name,$mobile)
	{
		include_once(SERVER_ROOT."/../classes/services.php");
		$se = new Services();
		$se->UserServiceInsert($this->id,$service_name,$mobile);
	}
	
	public function IsServiceRegistered($service_name)
	{
		$row = array();
        $db =& Db::globaldb();
		$sqlstr = "SELECT id_user_service,service_name,mobile
			FROM user_services 
			WHERE service_name='$service_name' AND id_user='$this->id' ";
		$db->query_single($row, $sqlstr);
		return $row['id_user_service']>0;
	}
	
	public function ServiceUpdate($id_user_service,$mobile)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "user_services" );
		$sqlstr = "UPDATE user_services SET mobile='$mobile' 
			WHERE id_user_service='$id_user_service' AND id_user='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Services(&$rows,$paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_user_service,service_name,mobile
			FROM user_services 
			WHERE id_user='$this->id' 
			ORDER BY service_name ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	public function Templates($id_resource)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_template,name,hide_fields
			FROM templates
			WHERE id_res=$id_resource AND (id_topic=0 AND id_group=0) 
			ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Topics()
	{
		include_once(SERVER_ROOT."/../classes/modules.php");
		if(Modules::AmIAdmin(4))
		{
			$sqlstr = "SELECT t.id_topic,t.name,'1' AS is_admin,tg.name AS group_name,t.description 
			FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group 
			WHERE t.archived=0 ";
		} else if (Modules::AmIUser(21)) {
		    $sqlstr = "SELECT t.id_topic,t.name,'1' AS is_admin,tg.name AS group_name,t.description
			FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group
			WHERE t.id_group<>2 ";
		} else {
			$sqlstr = "SELECT t.id_topic,t.name,tu.is_admin,tg.name AS group_name,t.description 
			FROM topics t
			INNER JOIN topics_groups tg ON t.id_group=tg.id_group
			INNER JOIN topic_users tu ON t.id_topic=tu.id_topic 
			WHERE tu.id_user='$this->id'";
		}
		$sqlstr .= " ORDER BY tg.seq,t.seq,t.name";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicsAdminOnly()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT topics.id_topic,name,is_admin FROM topics INNER JOIN topic_users ON topics.id_topic=topic_users.id_topic WHERE id_user=$this->id AND is_admin=1 AND is_contact=0 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicsNoAdmin()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT topics.id_topic,name,is_admin FROM topics INNER JOIN topic_users ON topics.id_topic=topic_users.id_topic WHERE id_user=$this->id AND is_admin=0 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicsContactOnly()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT topics.id_topic,name,is_contact FROM topics INNER JOIN topic_users ON topics.id_topic=topic_users.id_topic WHERE id_user=$this->id AND is_contact=1 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicsUserOnly()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT topics.id_topic,name,is_contact FROM topics INNER JOIN topic_users ON topics.id_topic=topic_users.id_topic WHERE id_user=$this->id AND is_contact=0 AND is_admin=0 ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TopicAdmin( $id_topic )
	{
		$admin = array();
		$topic_admin = false;
		$db =& Db::globaldb();
		$db->query_single( $admin, "SELECT id_user FROM topic_users WHERE id_user=$this->id AND is_admin=1 AND id_topic=$id_topic");
		if (count($admin)>0)
			$topic_admin = true;
		return $topic_admin;
	}

	public function TopicNotify( $id_topic, $pending_notify )
	{
		if ($id_topic>0 && $this->TopicAdmin($id_topic))
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "topic_users" );
			$res[] = $db->query( "UPDATE topic_users SET pending_notify='$pending_notify' WHERE id_topic=$id_topic AND id_user=$this->id" );
			Db::finish( $res, $db);
		}
	}

	public function UserEmulate( $id_user )
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		if ($session->Get("real_user_id")>0)
		{
			$real_id = $session->Get("real_user_id");
			$real_login = $session->Get("real_user_login");
			$real_id_language = $session->Get("real_id_language");
		}
		else
		{
			$real_id = $session->Get("current_user_id");
			$real_login = $session->Get("current_user_login");
			$real_id_language = $session->Get("id_language");
		}
		$this->id = $real_id;
		if ($this->ModuleAdmin(1))
		{
			$this->id = $id_user;
			$user = $this->UserGet();
			$session->Delete("module_right");
			$session->Delete("module_admin");
			$session->Set("current_user_id",$id_user);
			$session->Set("current_user_login",$user['login']);
			$session->Set("id_language",$user['id_language']);
			$session->Set("real_user_id",$real_id);
			$session->Set("real_user_login",$real_login);
			$session->Set("real_id_language",$real_id_language);
		}
	}

	public function UserEmulateEnd()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set("current_user_id",$session->Get("real_user_id"));
		$session->Set("current_user_login",$session->Get("real_user_login"));
		$session->Set("id_language",$session->Get("real_id_language"));
		$session->Delete("real_user_id");
		$session->Delete("real_user_login");
		$session->Delete("real_id_language");
	}

	public function UserGet()
	{
		$user = array();
		$db =& Db::globaldb();
		$db->query_single( $user, "SELECT name,phone,mobile,email,email_visible,signature,active,user_show,login,
			id_geo,notes,admin_notes,id_language,connections,UNIX_TIMESTAMP(start_date) AS start_date_ts,id_p,
			UNIX_TIMESTAMP(last_conn) AS last_conn_ts,id_tutor,verified,bounces,photo_visible,id_group
			FROM users u INNER JOIN user_notes USING(id_user) WHERE u.id_user='$this->id'");
		return $user;
	}

	public function UserGetFull()
	{
		$user = array();
		$db =& Db::globaldb();
		$db->query_single( $user, "SELECT name,staff_id,department,job_position,phone,mobile,email,email_visible,signature,active,
			user_show,login,id_geo,notes,admin_notes,id_language,connections,UNIX_TIMESTAMP(start_date) AS start_date_ts,id_p,
			UNIX_TIMESTAMP(last_conn) AS last_conn_ts,id_tutor,verified,bounces,photo_visible
			FROM users u INNER JOIN user_notes USING(id_user) WHERE u.id_user='$this->id'");
		return $user;
	}
	
	public function UserInsert( $name,$email,$login,$password,$active,$admin_notes,$id_language,$start_date="",$id_group=0 )
	{
		$id_tutor = (int)($this->id);
		$email = trim($email);
		include_once(SERVER_ROOT."/../classes/ini.php");
		include_once(SERVER_ROOT."/../classes/config.php");
		$ini = new Ini;
		$conf = new Configuration();
		$this->LoginUnique($login,0);
		$this->EmailUnique($email);
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		$cpassword = $sc->encrypt($password);
		$db =& Db::globaldb();
		if($start_date=="")
			$start_date = $db->getTodayTime();
		$db->begin();
		$db->lock( "users" );
		$this->id = $db->nextId( "users", "id_user" );
		$sqlstr =  "INSERT INTO users (id_user,name,login,password,email,active,admin_notes,id_language,start_date,id_tutor,id_group)
		VALUES ($this->id,'$name','$login','$cpassword','$email',$active,'$admin_notes',$id_language,'$start_date',$id_tutor,$id_group)";
		$res[] = $db->query( $sqlstr );
		$db->lock( "user_notes" );
		$res[] = $db->query("INSERT INTO user_notes (id_user,signature,notes) VALUES ($this->id,'','')");
		Db::finish( $res, $db);
		if($conf->Get("force_email_verification"))
		{
			$this->VerifyEmail();
		}
		return $this->id;
	}
	
	public function UserInsertLdap($login,$id_language)
	{
		$id_user = 0;
		include_once(SERVER_ROOT."/../classes/users.php");
		$uu = new Users();
		$ausers = $uu->Available();
		foreach($ausers as $auser)
		{
			if(strcasecmp($auser['login'],$login)==0)
			{
				$db =& Db::globaldb();
				include_once(SERVER_ROOT."/../classes/varia.php");
				$id_user = $this->UserInsert($auser['name'],$auser['email'],strtolower($auser['login']),Varia::Uid(),1,"",$id_language,$db->getTodayTime());
			}
		}
		return $id_user;
	}

	public function UserSet( $login )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_user,name,email,email_visible FROM users WHERE id_user>0 AND active=1 AND login='$login'");
		if ($row['id_user']>0)
			$this->id = $row['id_user'];
		return $row;
	}

	public function UserUpdate( $name,$staff_id,$email,$email_visible,$department,$job_position,$phone,$mobile,$signature,$id_geo,$photo_visible )
	{
		$row = $this->UserGet();
		$db =& Db::globaldb();
		if ($email_visible!=$row['email_visible'] || $signature!=$db->SqlQuote($row['signature']))
		{
			$articles = $this->ArticlesAll();
			include_once(SERVER_ROOT."/../classes/topic.php");
			foreach($articles as $article)
			{
				$t = new Topic($article['id_topic']);
				$t->queue->JobInsert($t->queue->types['article'],$article['id_article'],"update");
			}
		}
		if ($photo_visible!=$row['photo_visible'])
		{
			include_once(SERVER_ROOT."/../classes/images.php");
			$i = new Images();
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager;
			$fm->Delete("pub/{$i->pub_path}users/0/{$this->id}.jpg");
			if($photo_visible)
			{
				$filename = $this->id . "." . $i->convert_format;
				$fm->Copy("uploads/users/0/$filename","pub/{$i->pub_path}users/0/$filename");
			}
			$articles = $this->ArticlesAll();
			include_once(SERVER_ROOT."/../classes/topic.php");
			foreach($articles as $article)
			{
				$t = new Topic($article['id_topic']);
				$t->queue->JobInsert($t->queue->types['article'],$article['id_article'],"update");
			}
		}
		$email = trim($email);
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET name='$name',staff_id='$staff_id',email='$email',email_visible=$email_visible,
			department='$department',job_position='$job_position',phone='$phone',mobile='$mobile',id_geo=$id_geo,photo_visible='$photo_visible'
			WHERE id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		$db->lock( "user_notes" );
		$res[] = $db->query("UPDATE user_notes SET signature='$signature' WHERE id_user=$this->id");
		Db::finish( $res, $db);
	}

	public function UserUpdateByAdmin( $login,$active,$admin_notes,$id_language,$super_right,$start_date,$verified,$id_group )
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$this->LoginUnique($login,$this->id);
		if ($super_right=="1")
			$cond = ",admin_notes='$admin_notes',start_date='$start_date',verified='$verified',id_group='$id_group'";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET login='$login',active=$active,id_language=$id_language $cond
			WHERE id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($session->Get('current_user_id')==$this->id)
			$session->Set('id_language',$id_language);
	}

	public function UserUpdateLanguage( $id_language)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET id_language=$id_language WHERE id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set("id_language",$id_language);
	}

	public function UserUpdatePage( $user_show,$notes )
	{
		$row = $this->UserGet();
		$db =& Db::globaldb();
		if ($user_show!=$row['$user_show'] )
		{
			$articles = $this->ArticlesAll();
			include_once(SERVER_ROOT."/../classes/topic.php");
			foreach($articles as $article)
			{
				$t = new Topic($article['id_topic']);
				$t->queue->JobInsert($t->queue->types['article'],$article['id_article'],"update");
			}
		}
		$db->begin();
		$db->lock( "users" );
		$sqlstr = "UPDATE users SET user_show='$user_show' WHERE id_user=$this->id";
		$res[] = $db->query( $sqlstr );
		$db->lock( "user_notes" );
		$res[] = $db->query("UPDATE user_notes SET notes='$notes' WHERE id_user=$this->id");
		Db::finish( $res, $db);
	}

	public function VerifyEmail($send_email=true)
	{
		if($this->id>0)
		{
			$row = $this->UserGet();
			$row2 = array();
			$db =& Db::globaldb();
			$sqlstr = "SELECT UNIX_TIMESTAMP(token_date) AS tts,token 
				FROM user_verify 
				WHERE id_user='$this->id' ";
			$db->query_single( $row2, $sqlstr);
			$db->begin();
			$today = $db->getTodayTime();
			$db->lock( "user_verify" );
			if($row2['tts']>0)
			{
				$token = $row2['token'];
				$sqlstr =  "UPDATE user_verify SET token_date='$today' WHERE id_user='$this->id'  ";
			}
			else
			{
				include_once(SERVER_ROOT."/../classes/varia.php");
				$token = Varia::Uid();
				$sqlstr = "INSERT INTO user_verify (id_user,token,token_date) VALUES ($this->id,'$token','$today') ";
			}
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			$link = $mail->admin_web . "/gate/check.php?token=$token";
			if($send_email)
			{
				$extra = array();
				include_once(SERVER_ROOT."/../classes/translator.php");
				$trm28 = new Translator($row['id_language'],28);
				$subject = $trm28->Translate("email_check_subj");
				$site_name = $mail->title;
				$site_url = $mail->admin_web;
				$message = $trm28->TranslateParams("email_check_body",array($row['name'],$row['email'],$site_name,$mail->staff_email,$site_url,$link));
				$mail->SendMail($row['email'],$row['name'],$subject,$message,$extra);
			}
		}
		return $link;
	}

	public function VerifyToken($unescaped_token)
	{
		$row = array();
		$db = Db::globaldb();
		$token = $db->SqlQuote($unescaped_token);
		$sqlstr = "SELECT id_user,UNIX_TIMESTAMP(token_date) AS token_ts FROM user_verify WHERE token='$token' ";
		$db->query_single( $row, $sqlstr);
		if($row['id_user']>0)
		{
			$this->id = $row['id_user'];
			if((time() - $row['token_ts'])  > USERS_VERIFY_TOKEN_TTL)
			{
				$ret = 1;
				$this->VerifyEmail();
			}
			else
			{
				$ret = 0;
				$this->Verified();
			}
		}
		else
		{
			$ret = 2;
		}
		return $ret;
	}
	
	public function Verified()
	{
		if($this->id>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "users" );
			$sqlstr = "UPDATE users SET verified=1,bounces=0 WHERE id_user='$this->id' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "user_verify" );
			$sqlstr = "DELETE FROM user_verify WHERE id_user='$this->id' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);		
		}
	}
}
?>
