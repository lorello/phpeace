<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/session.php");

/**
 * Configuration settings
 * If you want to change some configuration setting, you do not need
 * to edit this class. Edit custom/config.php instead
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Configuration
{
	/**
	 * Configuration settings
	 *
	 * @var array
	 */
	private $configuration;

	/**
	 * Load configuration from session if present
	 * otherwise read it and store it session
	 *
	 * @param string	$module
	 * @param boolean	$force_reload
	 */
	public function __construct($module="", $force_reload=false)
	{
		$varname = "config" . ($module!=""? "_" . $module : "");
		$session = new Session();
		if($session->IsVarSet($varname) && !$force_reload)
			$this->configuration = $session->Get($varname);
		else
		{
			$this->configuration = $this->SetDefaults($module);
			$session->Set($varname,$this->configuration);
		}
	}

	/**
	 * Retrieve configuration setting
	 *
	 * @param string $var	Setting name
	 * @return mixed
	 */
	public function Get($var)
	{
		return (isset($this->configuration[$var]))? $this->configuration[$var] : null;
	}

	/**
	 * Set general or module configuration settings default
	 *
	 * @param string $module
	 * @return array	Configuration settings
	 */
	private function SetDefaults($module)
	{
		$values = array();
		$class_name = "Config" . ($module!=""? ucfirst($module) : "");
		include_once(SERVER_ROOT."/../custom/config.php");
		if(class_exists($class_name))
		{
			$conf = new $class_name;
		}
		else
		{
			$conf = new Config();
		}

		switch($module)
		{
			case "ecommerce":
				$values['product_thumb'] = (isset($conf->product_thumb))? $conf->product_thumb : 0;
				$values['product_image'] = (isset($conf->product_image))? $conf->product_image : 2;
				$values['products_per_order'] = (isset($conf->products_per_order) && $conf->products_per_order>0)? $conf->products_per_order : 0;
				$values['notify'] = (isset($conf->notify))? $conf->notify : "";
				$values['currency'] = (isset($conf->currency))? $conf->currency : "GBP";
				$values['pgw'] = (isset($conf->pgw))? $conf->pgw : "paypal";
				$values['send_order_confirm_mail'] = (isset($conf->send_order_confirm_mail) && is_bool($conf->send_order_confirm_mail))? $conf->send_order_confirm_mail : false;
				$values['store_payments'] = (isset($conf->store_payments) && is_bool($conf->store_payments))? $conf->store_payments : true;
				break;
			case "orgs":
				$values['notify_user'] = (isset($conf->notify_user) && is_bool($conf->notify_user))? $conf->notify_user : true;
				$values['notify_admin'] = (isset($conf->notify_admin) && is_bool($conf->notify_admin))? $conf->notify_admin : true;
				break;
			default:
				$values['debug'] = (isset($conf->debug) && is_bool($conf->debug))? $conf->debug : false;
				$dbconf = $conf->dbconf;
				if(!isset($dbconf['impl']) || $dbconf['impl']=="") {
					$dbconf['impl'] = "mysql";
				}
				$values['dbconf'] = $dbconf;
				$values['db_pconn'] = (isset($conf->db_pconn) && is_bool($conf->db_pconn))? $conf->db_pconn : false;
				$values['delayed_inserts'] = (isset($conf->delayed_inserts) && is_bool($conf->delayed_inserts))? $conf->delayed_inserts : true;
				$values['db_charset'] = (isset($conf->db_charset) && $conf->db_charset != "")? $conf->db_charset : "utf8";
				$values['convert_path'] = (isset($conf->convert_path) && $conf->convert_path != "")? $conf->convert_path : "";
				$values['records_per_page'] = (isset($conf->records_per_page) && $conf->records_per_page > 0)? $conf->records_per_page : 15;
				$paths = (isset($conf->paths) && is_array($conf->paths))? $conf->paths : array(	'article'	=>	'/articles/art_',
				'subtopic'	=>	'/indices/index_',
				'gallery'	=>	'/gallery/',
				'article_box'	=>	'/articles/box_',
				'article_doc'	=>	'/docs/',
				'image'		=>	'/images/',
				'image_orig'	=>	'/images/img_',
				'image_zoom'	=>	'/articles/iz_',
				'gallery_image'	=>	'/images/ig_',
				'graphic'	=>	'images/',
				'docs'	=>	'pdocs/');
				if(isset($paths['docs']) && $paths['docs']=="")
				$paths['docs'] = "pdocs/";
				$values['paths'] = $paths;
				$values['visitors_life'] = (isset($conf->visitors_life) && $conf->visitors_life > 0)? $conf->visitors_life : 60;
				$values['verification_required'] = (isset($conf->verification_required) && is_bool($conf->verification_required))? $conf->verification_required : false;
				$values['verify_token_ttl'] = (isset($conf->verify_token_ttl) && $conf->verify_token_ttl > 0)? $conf->verify_token_ttl : 432000;
                $values['unverified_people_ttl'] = (isset($conf->unverified_people_ttl) && $conf->unverified_people_ttl > 0)? $conf->unverified_people_ttl	: 0;
				$values['block_multiple_sessions'] = (isset($conf->block_multiple_sessions) && is_bool($conf->block_multiple_sessions))? $conf->block_multiple_sessions : false;
				$values['img_sizes'] = (isset($conf->img_sizes) && is_array($conf->img_sizes))? $conf->img_sizes : array('80','120','250','400');
				$values['convert_format'] = (isset($conf->convert_format) && $conf->convert_format != "")? $conf->convert_format : "jpg";
				$values['convert_quality'] = (isset($conf->convert_quality) && $conf->convert_quality != "")? $conf->convert_quality : "90";
				$values['default_img_size'] = (isset($conf->default_img_size))? $conf->default_img_size : 1;
				$values['docs_covers_size'] = (isset($conf->docs_covers_size))? (int)$conf->docs_covers_size : -1;
				$values['covers_thumb'] = (isset($conf->covers_thumb))? $conf->covers_thumb : 0;
				$values['covers_size'] = (isset($conf->covers_size))? $conf->covers_size : 2;
				$values['orgs_thumb'] = (isset($conf->orgs_thumb))? $conf->orgs_thumb : 0;
				$values['orgs_size'] = (isset($conf->orgs_size))? $conf->orgs_size : 2;
				$values['orgs_upload'] = (isset($conf->orgs_upload) && is_bool($conf->orgs_upload))? $conf->orgs_upload : false;
				$values['box_sizes'] = (isset($conf->box_sizes) && is_array($conf->box_sizes))? $conf->box_sizes : array('100','200','300','400');
				$values['content_type'] = (isset($conf->content_type) && $conf->content_type != "")? $conf->content_type : "html4_strict";
				$values['disable_autocomplete'] = (isset($conf->disable_autocomplete) && is_bool($conf->disable_autocomplete))? $conf->disable_autocomplete : false;
				$values['user_auth'] = (isset($conf->user_auth) && $conf->user_auth != "")? $conf->user_auth : "internal";
				$values['ldap'] = (isset($conf->ldap) && is_array($conf->ldap))? $conf->ldap : array();
				$values['password_length_min'] = (isset($conf->password_length_min) && $conf->password_length_min > 0)? $conf->password_length_min : 8;
				$values['warranty'] = (isset($conf->warranty) && $conf->warranty != "")? $conf->warranty : "";
				$values['no_more_ie6'] = (isset($conf->no_more_ie6) && is_bool($conf->no_more_ie6))? $conf->no_more_ie6 : false;
				$values['force_email_verification'] = (isset($conf->force_email_verification) && is_bool($conf->force_email_verification))? $conf->force_email_verification : false;
				$values['welcome_quote'] = (isset($conf->welcome_quote) && is_bool($conf->welcome_quote))? $conf->welcome_quote : false;
				$values['max_login_attempts'] = (isset($conf->max_login_attempts) && $conf->max_login_attempts > 0)? $conf->max_login_attempts : 3;
				$values['login_failure_delay'] = (isset($conf->login_failure_delay) && $conf->login_failure_delay > 0)? $conf->login_failure_delay : 10;
				$values['step_articles'] = (isset($conf->step_articles) && $conf->step_articles > 0)? $conf->step_articles : 100;
				$values['cron_timeout'] = (isset($conf->cron_timeout) && $conf->cron_timeout > 0)? $conf->cron_timeout : 1800;
				$values['topic_publish_timeout'] = (isset($conf->topic_publish_timeout) && $conf->topic_publish_timeout > 0)? $conf->topic_publish_timeout : 900;
				$values['encoding'] = (isset($conf->encoding) && $conf->encoding != "")? $conf->encoding : "UTF-8";
				$values['sender_address'] = (isset($conf->sender_address) && is_bool($conf->sender_address))? $conf->sender_address : true;
				$values['specific_sender_address'] = (isset($conf->specific_sender_address) && $conf->specific_sender_address != "")? $conf->specific_sender_address : "";
				$values['bounces_host'] = (isset($conf->bounces_host) && $conf->bounces_host != "")? $conf->bounces_host : "";
				$values['bounces_port'] = (isset($conf->bounces_port) && $conf->bounces_port != "")? $conf->bounces_port : "110/pop3/notls";
				$values['bounces_username'] = (isset($conf->bounces_username) && $conf->bounces_username != "")? $conf->bounces_username : "";
				$values['bounces_password'] = (isset($conf->bounces_password) && $conf->bounces_password != "")? $conf->bounces_password : "";
				$values['bounces_threshold'] = (isset($conf->bounces_threshold) && $conf->bounces_threshold > 0)? $conf->bounces_threshold : 5;
				$values['soft_bounce_score'] = (isset($conf->soft_bounce_score) && is_numeric($conf->soft_bounce_score))? $conf->soft_bounce_score : 0;
				$values['hard_bounce_score'] = (isset($conf->hard_bounce_score) && is_numeric($conf->hard_bounce_score))? $conf->hard_bounce_score : 1;
				$values['calendar_version'] = (isset($conf->calendar_version) && is_numeric($conf->calendar_version))? $conf->calendar_version : 1;
				$values['awstats'] = (isset($conf->awstats) && $conf->awstats != "")? $conf->awstats : "";
				$values['fb_scraper'] = (isset($conf->fb_scraper) && $conf->fb_scraper != "")? $conf->fb_scraper : "";
				$values['error_log_size'] = (isset($conf->error_log_size) && $conf->error_log_size > 0)? $conf->error_log_size : 20480;
				$values['rss_ttl'] = (isset($conf->rss_ttl) && $conf->rss_ttl > 0)? $conf->rss_ttl : 60;
				$values['cache_ttl'] = (isset($conf->cache_ttl) && $conf->cache_ttl > 0)? $conf->cache_ttl : 360;
				$values['apc_cache_ttl'] = (isset($conf->apc_cache_ttl) && $conf->apc_cache_ttl > 0)? $conf->apc_cache_ttl : 0;
				$values['update_diff'] = (isset($conf->update_diff) && isset($conf->update_diff))? $conf->update_diff : 14400;
				$values['log_archive'] = (isset($conf->log_archive) && is_bool($conf->log_archive))? $conf->log_archive : false;
				$values['syslog'] = (isset($conf->syslog) && is_bool($conf->syslog))? $conf->syslog : false;
				$values['formspam_log'] = (isset($conf->formspam_log) && is_bool($conf->formspam_log))? $conf->formspam_log : false;
				$values['htaccess'] = (isset($conf->htaccess) && is_bool($conf->htaccess))? $conf->htaccess : true;
				$values['recaptcha'] = (isset($conf->captcha) && $conf->captcha=="recaptcha")? true : ( (isset($conf->recaptcha) && is_bool($conf->recaptcha))? $conf->recaptcha : false );
				$values['recaptcha_public_key'] = (isset($conf->recaptcha_public_key) && $conf->recaptcha_public_key != "")? $conf->recaptcha_public_key : "";
				$values['recaptcha_private_key'] = (isset($conf->recaptcha_private_key) && $conf->recaptcha_private_key != "")? $conf->recaptcha_private_key : "";
				$values['recaptcha_version'] = (isset($conf->recaptcha_version) && $conf->recaptcha_version != "")? $conf->recaptcha_version : "2";
				$values['fb_app_id'] = (isset($conf->fb_app_id) && $conf->fb_app_id != "")? $conf->fb_app_id : "";
				$values['telegram_bot_api_key'] = (isset($conf->telegram_bot_api_key) && $conf->telegram_bot_api_key != "")? $conf->telegram_bot_api_key : "";
				$values['telegram_callback_key'] = (isset($conf->telegram_callback_key) && $conf->telegram_callback_key != "")? $conf->telegram_callback_key : "";
				$values['rocketchat_webhook'] = (isset($conf->rocketchat_webhook) && $conf->rocketchat_webhook != "")? $conf->rocketchat_webhook : "";
				$values['mastodon_server'] = (isset($conf->mastodon_server) && $conf->mastodon_server != "")? $conf->mastodon_server : "";
				$values['mastodon_admin'] = (isset($conf->mastodon_admin) && $conf->mastodon_admin != "")? $conf->mastodon_admin : "";
				$values['mastodon_admin_token'] = (isset($conf->mastodon_admin_token) && $conf->mastodon_admin_token != "")? $conf->mastodon_admin_token : "";
				$values['mastodon_local'] = (isset($conf->mastodon_local) && $conf->mastodon_local != "")? $conf->mastodon_local : "";
				$values['mastodon_local_token'] = (isset($conf->mastodon_local_token) && $conf->mastodon_local_token != "")? $conf->mastodon_local_token : "";
				$values['dev'] = (isset($conf->dev) && is_bool($conf->dev))? $conf->dev : false;
				$values['track'] = (isset($conf->track) && is_bool($conf->track))? $conf->track: false;
				$values['track_exclude'] = (isset($conf->track_exclude) && is_array($conf->track_exclude))? $conf->track_exclude : array("127.0.0.1");
				$values['track_exclude_ua'] = (isset($conf->track_exclude_ua) && is_array($conf->track_exclude_ua))? $conf->track_exclude_ua : array("Googlebot","Slurp","msnbot");
				$values['track_all'] = (isset($conf->track_all) && is_bool($conf->track_all))? $conf->track_all : false;
				$values['mailjob_limit'] = (isset($conf->mailjob_limit) && $conf->mailjob_limit > 0)? $conf->mailjob_limit : 200;
				$values['mail_from'] = isset($conf->mail_from)? $conf->mail_from : "PhPeace ";
				$values['x_mailer'] = (isset($conf->x_mailer) && is_bool($conf->x_mailer))? $conf->x_mailer : true;
				$values['minimum_feed_update_interval'] = (isset($conf->minimum_feed_update_interval) && $conf->minimum_feed_update_interval > 0)? $conf->minimum_feed_update_interval : 30;
				$values['users_mode'] = (isset($conf->users_mode) && $conf->users_mode != "")? $conf->users_mode : "shared";
				$values['submit_defects'] = (isset($conf->submit_defects) && is_bool($conf->submit_defects))? $conf->submit_defects : true;
				$values['caching'] = (isset($conf->caching) && $conf->caching != "")? $conf->caching : "session";
				$values['scheduler_index'] = (isset($conf->scheduler_index) && $conf->scheduler_index > 0)? $conf->scheduler_index : 50;
				$values['time_weight'] = (isset($conf->time_weight) && $conf->time_weight > 0)? $conf->time_weight : 0;
				$values['min_str_length'] = (isset($conf->min_str_length) && $conf->min_str_length > 0)? $conf->min_str_length : 4;
				$values['metaphone_threshold'] = (isset($conf->metaphone_threshold) && $conf->metaphone_threshold > 0)? $conf->metaphone_threshold : 0;
				$values['indexer_weights'] = (isset($conf->indexer_weights) && is_array($conf->indexer_weights))? $conf->indexer_weights : array('keyword'=>10,'title'=>8,'keyword_related'=>2,'content'=>4,'notes'=>3)	;
				$values['index_pdf'] = (isset($conf->index_pdf) && is_bool($conf->index_pdf))? $conf->index_pdf : false;
				$values['index_doc'] = (isset($conf->index_doc) && is_bool($conf->index_doc))? $conf->index_doc : false;
				$values['no_geo_topic'] = (isset($conf->no_geo_topic) && is_bool($conf->no_geo_topic))? $conf->no_geo_topic : false;
				$values['search_query_store'] = (isset($conf->search_query_store) && is_bool($conf->search_query_store))? $conf->search_query_store : false;
				$values['blackdirs'] = (isset($conf->blackdirs) && is_array($conf->blackdirs))? $conf->blackdirs : array();
				$values['dr_site'] = (isset($conf->dr_site) && is_bool($conf->dr_site))? $conf->dr_site : false;
				$values['mod_rewrite'] = (isset($conf->mod_rewrite) && is_bool($conf->mod_rewrite))? $conf->mod_rewrite : true;
				$values['landing'] = (isset($conf->landing) && is_array($conf->landing))? $conf->landing : array();
				$values['ui'] = (isset($conf->ui) && is_bool($conf->ui))? $conf->ui : false;
				$values['cdn'] = (isset($conf->cdn) && $conf->cdn != "")? $conf->cdn : "";
				$values['cdn_token'] = (isset($conf->cdn_token) && $conf->cdn_token != "")? $conf->cdn_token : "";
				$values['audio_max_size'] = (isset($conf->audio_max_size) && $conf->audio_max_size > 0)? $conf->audio_max_size : 20971520;
				$values['audio_sample_rate'] = (isset($conf->audio_sample_rate) && $conf->audio_sample_rate > 0)? $conf->audio_sample_rate : 22050;
				$values['audio_bitrate'] = (isset($conf->audio_bitrate) && $conf->audio_bitrate > 0)? $conf->audio_bitrate : 96;
				$values['ffmpeg_audio_options'] = (isset($conf->ffmpeg_audio_options) && $conf->ffmpeg_audio_options != "")? $conf->ffmpeg_audio_options : "-acodec libmp3lame -ac 1 ";
				$values['video_resize_width'] = (isset($conf->video_resize_width) && $conf->video_resize_width > 0)? $conf->video_resize_width : 320;
				$values['video_resize_height'] = (isset($conf->video_resize_height) && $conf->video_resize_height > 0)? $conf->video_resize_height : 240;
				$values['video_length_limit'] = (isset($conf->video_length_limit) && $conf->video_length_limit > 0)? $conf->video_length_limit : 1200;
				$values['video_max_size'] = (isset($conf->video_max_size) && $conf->video_max_size > 0)? $conf->video_max_size : 31457280;
				$values['video_bitrate'] = (isset($conf->video_bitrate) && $conf->video_bitrate > 0)? $conf->video_bitrate : 300000;
				$values['video_frame_rate'] = (isset($conf->video_frame_rate) && $conf->video_frame_rate > 0)? $conf->video_frame_rate : 25;
				$values['video_audio_sample_rate'] = (isset($conf->video_audio_sample_rate) && $conf->video_audio_sample_rate > 0)? $conf->video_audio_sample_rate : 22050;
				$values['video_audio_bitrate'] = (isset($conf->video_audio_bitrate) && $conf->video_audio_bitrate > 0)? $conf->video_audio_bitrate : 32;
				$values['video_thumbs'] = (isset($conf->video_thumbs) && $conf->video_thumbs > 0)? $conf->video_thumbs : 3;
				$values['ffmpeg_niceness'] = (isset($conf->ffmpeg_niceness) && $conf->ffmpeg_niceness > 0)? $conf->ffmpeg_niceness : 19;
				$values['ffmpeg_options'] = (isset($conf->ffmpeg_options) && $conf->ffmpeg_options != "")? $conf->ffmpeg_options : "-aspect 4:3 -f flv -acodec libmp3lame -ac 1 ";
				$values['encodings_per_cron_job'] = (isset($conf->encodings_per_cron_job) && $conf->encodings_per_cron_job > 0)? $conf->encodings_per_cron_job : 1;
				$values['video_files'] = (isset($conf->video_files) && is_array($conf->video_files))? $conf->video_files: array('mpg','mpeg','avi','flv','wmv','qt','mov','mp4','rm');
				$values['audio_files'] = (isset($conf->audio_files) && is_array($conf->audio_files))? $conf->audio_files: array('wav','mp3','ogg','aiff','au','mp4','m4a','wma','ra');
				$values['image_files'] = (isset($conf->image_files) && is_array($conf->image_files))? $conf->image_files: array('jpg','jpeg','gif','png','bmp','tif','tiff','ico');
				$values['doc_files'] = (isset($conf->doc_files) && is_array($conf->doc_files))? $conf->doc_files: array('pdf','rtf','txt','doc','docx','xls','xlsx');
				$values['uppercase_warning'] = (isset($conf->uppercase_warning) && is_bool($conf->uppercase_warning))? $conf->uppercase_warning : false;
				$values['attachment_max_size'] = (isset($conf->attachment_max_size) && $conf->attachment_max_size > 0)? $conf->attachment_max_size : 1048576;
				$values['feed_items_validate_per_cron_job'] = (isset($conf->feed_items_validate_per_cron_job) && $conf->feed_items_validate_per_cron_job > 0)? $conf->feed_items_validate_per_cron_job : 100;
				$values['feeds_import_per_cron_job'] = (isset($conf->feeds_import_per_cron_job) && $conf->feeds_import_per_cron_job > 0)? $conf->feeds_import_per_cron_job : 50;
				$values['widgets_cache_default_ttl'] = (isset($conf->widgets_cache_default_ttl) && $conf->widgets_cache_default_ttl > 0)? $conf->widgets_cache_default_ttl : 600;
				$values['soap_timeout'] = (isset($conf->soap_timeout) && $conf->soap_timeout > 0)? $conf->soap_timeout : 5;
				$values['soap_timeout_short'] = (isset($conf->soap_timeout_short) && $conf->soap_timeout_short > 0)? $conf->soap_timeout_short : 1;
				$values['post_update_script'] = (isset($conf->post_update_script) && $conf->post_update_script != "")? $conf->post_update_script : "";
				$values['post_install_script'] = (isset($conf->post_install_script) && $conf->post_install_script != "")? $conf->post_install_script : "";
				$values['upload_host'] = (isset($conf->upload_host) && $conf->upload_host != "")? $conf->upload_host : "";
				$values['user_agent'] = (isset($conf->user_agent) && $conf->user_agent != "")? $conf->user_agent : "";
				$values['widget_library_display'] = (isset($conf->widget_library_display) && $conf->widget_library_display != "")? $conf->widget_library_display : "";
				$values['uss_url'] = (isset($conf->uss_url) && $conf->uss_url != "")? $conf->uss_url : "";
				$values['uss_key'] = (isset($conf->uss_key) && $conf->uss_key != "")? $conf->uss_key : "";
				$values['uss_log_hits'] = (isset($conf->uss_log_hits) && is_bool($conf->uss_log_hits))? $conf->uss_log_hits : false;
				$values['uss_mask'] = (isset($conf->uss_mask) && $conf->uss_mask != "")? $conf->uss_mask : "";
				$values['stopforumspam'] = (isset($conf->stopforumspam) && is_bool($conf->stopforumspam))? $conf->stopforumspam : false;
				$values['stopforumspam_key'] = (isset($conf->stopforumspam_key) && $conf->stopforumspam_key != "")? $conf->stopforumspam_key : "";
				$values['highslide'] = (isset($conf->highslide) && is_bool($conf->highslide))? $conf->highslide : false;
				$values['max_vis'] = (isset($conf->max_vis) && $conf->max_vis > 0)? $conf->max_vis : 20;
				$values['sitemap_keyword_id'] = (isset($conf->sitemap_keyword_id) && $conf->sitemap_keyword_id > 0)? $conf->sitemap_keyword_id : 0;
				$values['orgs_access_protected'] = (isset($conf->orgs_access_protected) && is_bool($conf->orgs_access_protected))? $conf->orgs_access_protected : false;
				$values['orgs_orig'] = (isset($conf->orgs_orig) && is_bool($conf->orgs_orig))? $conf->orgs_orig : false;
				$values['upload_progress'] = false;
				$values['google_api_key'] = (isset($conf->google_api_key) && $conf->google_api_key != "")? $conf->google_api_key : "";
				$values['google_oauth_consumer_key'] = (isset($conf->google_oauth_consumer_key) && $conf->google_oauth_consumer_key != "")? $conf->google_oauth_consumer_key : "";
				$values['google_oauth_consumer_secret'] = (isset($conf->google_oauth_consumer_secret) && $conf->google_oauth_consumer_secret != "")? $conf->google_oauth_consumer_secret : "";
				$values['log_login'] = (isset($conf->log_login) && is_bool($conf->log_login))? $conf->log_login : false;
				$values['bitly_login'] = (isset($conf->bitly_login) && $conf->bitly_login != "")? $conf->bitly_login : "";
				$values['bitly_key'] = (isset($conf->bitly_key) && $conf->bitly_key != "")? $conf->bitly_key : "";
				$values['proxy'] = (isset($conf->proxy) && is_bool($conf->proxy))? $conf->proxy : false;
				$values['proxy_name'] = (isset($conf->proxy_name) && $conf->proxy_name != "")? $conf->proxy_name : "";
				$values['proxy_port'] = (isset($conf->proxy_port) && $conf->proxy_port != "")? $conf->proxy_port : "";
				$values['mobile_salt'] = (isset($conf->mobile_salt) && $conf->mobile_salt!= "")? $conf->mobile_salt : "";
				include_once(SERVER_ROOT."/../custom/config.php");
		}
		return $values;
	}

}
?>
