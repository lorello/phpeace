<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * Timeout (in seconds) for update execution
 *
 */
define('PHPEACE_UPDATE_TIMEOUT',1800);

/**
 * Executes update scripts 
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class PhPeaceUpdates
{
	/**
	 * Is a Database update required
	 *
	 * @var boolean
	 */
	public $db_update_required;

	/**
	 * Previous build number
	 *
	 * @var integer
	 */
	private $version_old;
	
	/**
	 * New build number
	 *
	 * @var integer
	 */
	private $version_new;
	
	/**
	 * Previous DB version
	 *
	 * @var integer
	 */
	private $db_version_old;
	
	/**
	 * New DB version
	 *
	 * @var integer
	 */
	private $db_version_new;
	
	/**
	 * Initiates version variables
	 *
	 * @param int $version_old
	 */
	function __construct($version_old)
	{
		$this->version_old = $version_old;
		$phpeace = new PhPeace();
		$this->version_new = $phpeace->build;
		$this->db_version_new = $phpeace->db_version;
		$db =& Db::globaldb();		
		$this->db_version_old = $db->Version();	
		$this->db_update_required = $this->db_version_new > $this->db_version_old;
	}
	
	/**
	 * Sets a high time limit to avoid timeout
	 *
	 */
	private function SetTimeLimit()
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$v->SetTimeLimit(PHPEACE_UPDATE_TIMEOUT);
	}

	/**
	 * Run update script (if necessary)
	 *
	 */
	public function Update($version=0)
	{
		$version_new = $version>0? $version : $this->version_new;
		$db = Db::globaldb();
		include_once(SERVER_ROOT."/../classes/modules.php");
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		
		$conf = new Configuration();
		$paths = $conf->Get("paths");
		
		if($version_new > $this->version_old)
		{
			switch($version_new)
			{
			    case "330":
					Modules::ModuleDeleteUsers(9);
			        $this->UpdateDB("277");
					break;
				case "329":
			        $fm->Delete("pub/tools/expressInstall.swf");
			        $fm->Delete("pub/tools/mediaplayer.swf");
			        $fm->Delete("pub/crossdomain.xml");
			        break;
				case "327":
					$fm->DirRemove("others/maxmind");
					$fm->DirRemove("others/oauth");
					$fm->DirRemove("others/securimage");
					$fm->DirRemove("others/twitteroauth");
					$fm->DirRemove("others/OpenInviter");
					$fm->DirRemove("others/recaptcha");
			        $fm->DirRemove("admin/js/tinymce-3.3.9");
			        $fm->Delete("modules/irdetobatchprocess.php");
			        $fm->Delete("classes/twitter.php");
			        $fm->Delete("classes/messaging.php");
					$fm->DirRemove("admin/mail");
					Modules::ModuleDeleteUsers(2);
			        $this->UpdateDB("276");
			        break;
			    case "321":
			        $this->UpdateDB("275");
			        break;
			    case "319":
					$fm->Delete("admin/people/person_association.php");
			        break;
			    case "316":
			        $this->UpdateDB("274");
			        break;
		        case "314":
			        $this->UpdateDB("273");
			        break;
			    case "313":
			        $this->UpdateDB("272");
			        break;
			    case "311":
			        $this->UpdateDB("271");
			        break;
			    case "305":
					$this->UpdateDB("270");
					break;
				case "304":
					$fm->DirAction("cache/events_api","check");
					break;
				case "300":
					$this->UpdateDB("269");
					break;
				case "299":
					$this->UpdateDB("268");
					break;
				case "296":
					$this->UpdateDB("267");
					break;
				case "295":
					$this->UpdateDB("266");
					$events = array();
					$db->QueryExe($events, "SELECT id_event FROM events WHERE has_image=1");
					$db =& Db::globaldb();
					$db->begin();
					$db->lock( "events" );
					foreach($events as $event) {
						$origfile = "uploads/events/orig/{$event['id_event']}.jpg";
						$sizes = $fm->ImageSize($origfile);
						$image_ratio = ($sizes['width']>0 && $sizes['height']>0)? $sizes['width']/$sizes['height'] : 0;
						$sqlstr = "UPDATE events SET image_ratio='{$image_ratio}' WHERE id_event='{$event['id_event']}' ";
						$res[] = $db->query($sqlstr);
					}
					Db::finish( $res, $db);
					$fm->DirDelete("lang/ar");
					$fm->DirDelete("lang/dk");
					$fm->DirDelete("lang/es");
					$fm->DirDelete("lang/fr");
					$fm->DirDelete("lang/ja");
					$fm->DirDelete("lang/ms");
					$fm->DirDelete("lang/ru");
					$fm->DirDelete("lang/sv");
					$fm->DirDelete("lang/zh");
					break;
				case "294":
					$fm->Delete("admin/js/tools.js");
					break;
				case "293":
					$this->UpdateDB("265");
					$events = array();
					$db->QueryExe($events, "SELECT id_event,place_details FROM events WHERE LENGTH(place_details)<100");
					$db =& Db::globaldb();
					$db->begin();
					$db->lock( "events" );
					$res = array();
					foreach($events as $event) {
						$address = $db->SqlQuote(trim($event['place_details']));
						if(strlen($address)>0) {
							$sqlstr = "UPDATE events SET address='{$address}',place_details='' WHERE id_event='{$event['id_event']}' ";
							$res[] = $db->query($sqlstr);
						}
					}
					Db::finish( $res, $db);
					break;
				case "292":
					$this->UpdateDB("264");
					break;
				case "288":
					$fm->DirAction("uploads/events","check");
					$fm->DirAction("uploads/events/orig","check");
					$fm->DirAction("uploads/events/0","check");
					$fm->DirAction("uploads/events/1","check");
					$fm->DirAction("uploads/events/2","check");
					$this->UpdateDB("261");
					$this->UpdateDB("262");
					include_once(SERVER_ROOT."/../classes/texthelper.php");
					$th = new TextHelper();
					$events = array();
					$db->QueryExe($events, "SELECT id_event,is_html,description FROM events WHERE is_html=0");
					$db =& Db::globaldb();
					$db->begin();
					$db->lock( "events" );
					foreach($events as $event) {
						$html = $db->SqlQuote($th->Htmlise($event['description']));
						$sqlstr = "UPDATE events SET description='{$html}',is_html='1' WHERE id_event='{$event['id_event']}' ";
						$res[] = $db->query($sqlstr);
					}
					Db::finish( $res, $db);
					$this->UpdateDB("263");
					break;
				case "286":
					include_once(SERVER_ROOT."/../classes/topic.php");
					$subs = array();
					$db->QueryExe($subs, "SELECT s.id_subtopic,s.id_image,s.params,s.id_topic,i.format FROM subtopics s INNER JOIN images i ON s.id_image=i.id_image WHERE s.id_image>0");
					foreach($subs as $sub) {
						$filename = "uploads/images/orig/{$sub['id_image']}.{$sub['format']}";
						$id_image = $fm->Exists($filename)? $sub['id_image'] : '0';
						$t = new Topic($sub['id_topic']);
						$t->SubtopicImageUpdate($sub['id_subtopic'], $id_image);
					}
				break;
				case "285":
					$fm->Delete("others/maxmind/geoip.dat");
					$fm->Delete("others/maxmind/geoip.php");
				break;
				case "284":
			        $fm->DirRemove("admin/js/tinymce-3.3.9");
			        $fm->DirDelete("others/OpenInviter/conf");
			        $fm->DirDelete("others/OpenInviter/images");
			        $fm->DirDelete("others/OpenInviter/plugins");
			        $fm->DirDelete("others/OpenInviter");
			        $fm->DirDelete("others/recaptcha");
			        $fm->DirDelete("others/securimage");
			        $fm->Delete("modules/irdetobatchprocess.php");
		        break;
			    case "283":
			        $fm->DirDelete("others/phpmailer");
			        $fm->DirDelete("others/ymailphp");
			        $fm->DirDelete("others/OpenInviter");
			        $fm->DirDelete("others/php-ofc-library");
			        $fm->DirDelete("uploads/itatour");
			        $fm->DirDelete("admin/ali");
			        $fm->Delete("pub/poll/ofc_data.php");
			        $fm->Delete("pub/widgets/openinviter.php");
			        $fm->Delete("classes/graph.php");
			        $fm->Delete("classes/volsections.php");
			        $fm->Delete("classes/utfhelper.php");
			        $fm->Delete("modules/ali.php");
			        $fm->Delete("modules/parlamonitor.php");
			        $fm->Delete("modules/astro.php");
			        $fm->Delete("modules/astro_log_removal.php");
			        $fm->Delete("modules/astro_video_player.php");
			        $fm->Delete("modules/astroaccount.php");
			        $fm->Delete("modules/astroint.php");
			        $fm->Delete("modules/ebilling.php");
			        $fm->Delete("modules/epg.php");
			        $fm->Delete("modules/epg_schedule.xsd");
			        $fm->Delete("modules/epg_schedule_v2.xsd");
			        $fm->Delete("modules/espn.php");
			        $fm->Delete("modules/friendsandfamily.php");
			        $fm->Delete("modules/iptv_mdu.php");
			        $fm->Delete("modules/irdeto.php");
			        $fm->Delete("modules/irdeto_batch_process.php");
			        $fm->Delete("modules/kiosk.php");
			        $fm->Delete("modules/kosovo.php");
			        $fm->Delete("modules/maxiscontroller.php");
			        $fm->Delete("modules/package.php");
			        $fm->Delete("modules/paymentgatewaybatch.php");
			        $fm->Delete("modules/paymentgatewayservice.php");
			        $fm->Delete("modules/people_account_link.php");
			        $fm->Delete("modules/people_maxis_link.php");
			        $fm->Delete("modules/people_report.php");
			        $fm->Delete("modules/pvr.php");
			        $fm->Delete("modules/sms_gateway.php");
			        $fm->Delete("modules/ticket.php");
			        $fm->Delete("modules/timedotcom.php");
			        $fm->Delete("modules/widget_import.php");
			        $fm->Delete("modules/xti_request.xml");
		        break;
                case "274":
					$db->QueryExe($images, "SELECT id_image,format FROM images WHERE width=0 AND height=0 ");
					$db =& Db::globaldb();
					$db->begin();
					$db->lock( "images" );
					foreach($images as $image) {
						$filename = "uploads/images/orig/{$image['id_image']}.{$image['format']}";
						if($fm->Exists($filename)) {
							$image_size = $fm->ImageSize($filename);
							if($image_size['width']>0 && $image_size['height']>0) {
								$sqlstr = "UPDATE images SET width='{$image_size['width']}',height='{$image_size['height']}' WHERE id_image='{$image['id_image']}' ";
								$res[] = $db->query($sqlstr);
							}
						}
					}
					Db::finish( $res, $db);
                break;
                case "272":
                    $this->UpdateDB("260");
                break;
                case "267":
                    $this->UpdateDB("258");
                    $this->UpdateDB("259");
                break;
                case "264":
                    $this->UpdateDB("257");
                break;
                case "263":
                    $this->UpdateDB("256");
                    $fm->DirAction("pub/js/custom","check");
                    $fm->DirAction("cache/aggregated_feed","check");
                    // delete flash images
					$db->query("DELETE FROM banners WHERE format='swf'");
					$images = array();
					$db->QueryExe($images, "SELECT id_image FROM images WHERE format='swf' ");
					$db->begin();
					$db->lock( "images_articles" );
					foreach($images as $image)
					{
						if($image['id_image']>0) {
							$res[] = $db->query("DELETE FROM images_articles WHERE id_image={$image['id_image']}");
						}
					}
					Db::finish( $res, $db);
					$db->begin();
					$db->lock( "images_galleries" );
					foreach($images as $image)
					{
						if($image['id_image']>0) {
							$res[] = $db->query("DELETE FROM images_galleries WHERE id_image={$image['id_image']}");
						}
					}
					Db::finish( $res, $db);
					$db->query("DELETE FROM images WHERE format='swf'");
                break;
                case "262":
                    $this->UpdateDB("255");
                    // remove itatour
					include_once(SERVER_ROOT."/../classes/styles.php");
					$s = new Styles;
					$styles = $s->StylesAll();
					foreach($styles as $style)
					{
						$fm->Delete("xsl/{$style['id_style']}/itatour.xsl");
						$fm->Delete("pub/css/{$style['id_style']}/itatour.css");
					}
					$fm->Delete("modules/itatour.php");
					$fm->Delete("pub/css/0/itatour.xsl");
					$fm->Delete("xsl/0/itatour.xsl");
					$fm->Delete("lang/en/mod_26.txt");
					$fm->Delete("lang/it/mod_26.txt");
					$fm->Delete("lang/ar/pub_26.txt");
					$fm->Delete("lang/dk/pub_26.txt");
					$fm->Delete("lang/en/pub_26.txt");
					$fm->Delete("lang/es/pub_26.txt");
					$fm->Delete("lang/fr/pub_26.txt");
					$fm->Delete("lang/it/pub_26.txt");
					$fm->Delete("lang/ja/pub_26.txt");
					$fm->Delete("lang/ms/pub_26.txt");
					$fm->Delete("lang/ru/pub_26.txt");
					$fm->Delete("lang/sv/pub_26.txt");
					$fm->Delete("lang/zh/pub_26.txt");
                    $fm->DirAction("admin/itatour","delete");
                    $fm->DirAction("pub/tourop","delete");
                    $fm->DirAction("uploads/itatour/0","delete");
                    $fm->DirAction("uploads/itatour/1","delete");
					$fm->DirAction("uploads/itatour/2","delete");
                    $fm->DirAction("uploads/itatour/orig","delete");
                break;
                case "260":
                    $this->UpdateDB("254");
                break;
                case "256":
					$fm->Delete("classes/sso.php");
					$fm->Delete("pub/widgets/epgprogramwidget.php");
					$fm->Delete("pub/widgets/solat.php");
					$fm->Delete("pub/widgets/ymail.php");
					$fm->Delete("pub/widgets/ymail_login.php");
					// foreach style remove astro and epg .xsl
					include_once(SERVER_ROOT."/../classes/styles.php");
					$s = new Styles;
					$styles = $s->StylesAll();
					foreach($styles as $style)
					{
						$fm->Delete("xsl/{$style['id_style']}/astro.xsl");
						$fm->Delete("xsl/{$style['id_style']}/epg.xsl");
						$fm->Delete("pub/css/{$style['id_style']}/astro.css");
						$fm->Delete("pub/css/{$style['id_style']}/epg.css");
					}
					include_once(SERVER_ROOT."/../classes/styles.php");

					$fm->Delete("lang/ar/pub_29.txt");
					$fm->Delete("lang/ar/pub_30.txt");
					$fm->Delete("lang/dk/pub_29.txt");
					$fm->Delete("lang/dk/pub_30.txt");
					$fm->Delete("lang/en/pub_29.txt");
					$fm->Delete("lang/en/pub_30.txt");
					$fm->Delete("lang/es/pub_29.txt");
					$fm->Delete("lang/es/pub_30.txt");
					$fm->Delete("lang/fr/pub_29.txt");
					$fm->Delete("lang/fr/pub_30.txt");
					$fm->Delete("lang/it/pub_29.txt");
					$fm->Delete("lang/it/pub_30.txt");
					$fm->Delete("lang/ja/pub_29.txt");
					$fm->Delete("lang/ja/pub_30.txt");
					$fm->Delete("lang/ms/pub_29.txt");
					$fm->Delete("lang/ms/pub_30.txt");
					$fm->Delete("lang/ru/pub_29.txt");
					$fm->Delete("lang/ru/pub_30.txt");
					$fm->Delete("lang/sv/pub_29.txt");
					$fm->Delete("lang/sv/pub_30.txt");
					$fm->Delete("lang/zh/pub_29.txt");
					$fm->Delete("lang/zh/pub_30.txt");
					$fm->Delete("wsdl/astro.wsdl");
					$fm->Delete("wsdl/epg.wsdl");
                    $fm->DirAction("admin/astro","delete");
                    $fm->DirAction("admin/epg","delete");
                    $fm->DirAction("pub/astro","delete");
                    $fm->DirAction("pub/epg","delete");
                    $fm->DirAction("uploads/epg/channel_0","delete");
                    $fm->DirAction("uploads/epg/channel_1","delete");
                    $fm->DirAction("uploads/epg/channel_orig","delete");
                    $fm->DirAction("uploads/epg","delete");
                    $this->UpdateDB("253");
					$fm->DirAction("pub/" . $paths['graphic'] . "epg","check");
                break;
                case "257":
					$fm->Delete("AUTHORS");
					$fm->Delete("README");
                    $fm->DirAction("pub/_feeds","delete");
                    $fm->DirAction("pub/_map","delete");
                break;
			}
			$ini = new Ini();
			$ini->Set("script_version",$version_new);
			include_once(SERVER_ROOT."/../classes/log.php");
			$log = new Log();
			$log->Write("info","Executed update script for version $version_new");
		}
		return $version_new;
	}
	
	/**
	 * Updates to the latest version
	 * If executed, return the latest script version, otherwise 0
	 *
	 * @return int
	 */
	public function UpdateAll()
	{
		$ini = new Ini();
		$ini->ForceReload();
		$version_old = (int)$ini->Get("script_version");
		if($version_old>0 && $this->version_new > $version_old)
		{
			for($i=$version_old+1;$i<=$this->version_new;$i++)
			{
				$this->Update($i);
			}
		}
	}
	
	/**
	 * Updates DB to a specific version
	 *
	 * @param int $db_version
	 */
	private function UpdateDB($db_version)
	{
		$db = Db::globaldb();
		// Double check, only run this update if necessary
		$db_current_version = $db->Version();
		if($db_version>$db_current_version)
		{
			$conf = new Configuration();
			$dbconf = $conf->Get("dbconf");
			switch($db_version)
			{
			    case "277":
					$db->ColumnAdd("payments","tx_id","VARCHAR(250)","''");
					$db->query("ALTER TABLE people CHANGE COLUMN email email VARCHAR(150) NOT NULL DEFAULT ''");
					break;
			    case "276":
					// disable internal messaging
			        $db->query("UPDATE modules SET active=0,admin=0 WHERE id_module=2");
			        break;
		        case "275":
			        $db->ColumnAdd("mailjob_recipients","id_geo","SMALLINT(6)","NULL",true);
			        break;
			    case "274":
			        $db->query("UPDATE modules SET path='mastodon' WHERE id_module=29");
			        break;
		        case "273":
			        $db->ColumnAdd("translations","source_url","VARCHAR(255)","''");
			        break;
			    case "272":
			        $db->query("UPDATE prov SET prov='Internet / Virtual' WHERE id_prov=105");
			        break;
			    case "271":
			        $db->ColumnAdd("users","id_group","SMALLINT(3)","0");
			        break;
			    case "270":
					$db->ColumnAdd("translations","id_rev","SMALLINT(4)","0");
					$db->ColumnAdd("translations","id_ad","SMALLINT(4)","0");
					break;
				case "269":
					$db->TableCreate('site_watch', "`id_watch` SMALLINT(4) NOT NULL DEFAULT '0',`url` VARCHAR(250) NOT NULL DEFAULT '',`keyword` VARCHAR(50) NOT NULL DEFAULT '',`notify` VARCHAR(250) NOT NULL DEFAULT '',`counter` SMALLINT(4) NOT NULL DEFAULT '0', PRIMARY KEY (`id_watch`)");
					break;
				case "268":
					$db->TableCreate('keywords_landing', "`id_keyword` mediumint(9) NOT NULL DEFAULT '0',`content` TEXT NOT NULL,`youtube` VARCHAR(150) NOT NULL DEFAULT ''");
					break;
				case "267":
					$db->query("ALTER TABLE features CHANGE COLUMN condition_id condition_id MEDIUMINT(6) UNSIGNED NOT NULL DEFAULT '0'");
					break;
				case "266":
					$db->ColumnAdd("events","image_ratio","DECIMAL(10, 8)","NULL",true);
					break;
				case "265":
					$db->ColumnAdd("events","address","VARCHAR(300)","''");
					break;
				case "264":
					$db->query("ALTER TABLE topic_keywords CHANGE COLUMN only_admin role TINYINT(1) NOT NULL DEFAULT '1'");
					break;
				case "263":
					$db->ColumnAdd("events","facebook_id","VARCHAR(80)","''");
					$db->ColumnAdd("events","latitude","DECIMAL(10, 8)","NULL",true);
					$db->ColumnAdd("events","longitude","DECIMAL(11, 8)","NULL",true);
					break;
				case "262":
					$db->ColumnAdd("events","has_image","TINYINT(1)","0");
					$db->ColumnAdd("events","is_html","TINYINT(1)","0");
				break;
				case "261":
					$db->IndexAdd('people', 'last_bounce');
					$db->IndexAdd('people', 'bounces');
					$db->IndexAdd('topics', 'visible');
				break;
				case "260":
					$db->query("ALTER TABLE lists CHANGE COLUMN description description VARCHAR(800) NOT NULL DEFAULT ''");
				break;
				case "259":
					$db->ColumnAdd("articles","highlight","TINYINT(1)","0");
				break;
				case "258":
					$db->ColumnDelete("global","assets_domain");
				break;
				case "257":
					$db->ColumnAdd("topics","archived","TINYINT(1)","0");
				break;
				case "256":
					$db->ColumnAdd("forms","recipient","VARCHAR(255)","''");
				break;
				case "255":
					$db->query("DROP TABLE IF EXISTS `tourop`");
					$db->query("DROP TABLE IF EXISTS `tourop_bookings`");
					$db->query("DROP TABLE IF EXISTS `tourop_competitions`");
					$db->query("DROP TABLE IF EXISTS `tourop_competitions_bookings`");
					$db->query("DROP TABLE IF EXISTS `tourop_competitions_countries`");
					$db->query("DROP TABLE IF EXISTS `tourop_competitions_groups`");
					$db->query("DROP TABLE IF EXISTS `tourop_contacts`");
					$db->query("DROP TABLE IF EXISTS `tourop_interests`");
					$db->query("DROP TABLE IF EXISTS `tourop_ita_offers`");
					$db->query("DROP TABLE IF EXISTS `tourop_itas`");
					$db->query("DROP TABLE IF EXISTS `tourop_offers`");
					$db->query("DROP TABLE IF EXISTS `tourop_workshop_bookings`");
					$db->query("DROP TABLE IF EXISTS `tourop_workshop_days`");
					$db->query("DROP TABLE IF EXISTS `tourop_workshop_register`");
					$db->query("DROP TABLE IF EXISTS `tourop_workshop_slots`");
					$db->query("DROP TABLE IF EXISTS `tourop_workshops`");
					$db->query("ALTER TABLE sendmail CHANGE COLUMN how_many how_many MEDIUMINT(8) NULL DEFAULT 0");
				break;
				case "254":
					$db->query("UPDATE global SET phpeace_server='https://updates.phpeace.org'");
					$db->query("ALTER TABLE global CHANGE COLUMN phpeace_server phpeace_server VARCHAR(50) NOT NULL DEFAULT 'https://updates.phpeace.org'");
				break;
				case "253":
					$db->query("UPDATE global SET phpeace_server='http://updates.phpeace.org'");
					$db->query("DROP TABLE IF EXISTS `irdeto_pending`");
					$db->query("DROP TABLE IF EXISTS `kiosk_ip`");
					$db->query("DROP TABLE IF EXISTS `kiosk_log`");
					$db->query("DROP TABLE IF EXISTS `maxis_ips`");
					$db->query("DROP TABLE IF EXISTS `people_account_ebilling`");
					$db->query("DROP TABLE IF EXISTS `people_account_entitlement`");
					$db->query("DROP TABLE IF EXISTS `people_account_link`");
					$db->query("DROP TABLE IF EXISTS `people_account_link_log`");
					$db->query("DROP TABLE IF EXISTS `people_account_link_tdc`");
					$db->query("DROP TABLE IF EXISTS `people_account_register`");
					$db->query("DROP TABLE IF EXISTS `people_friends`");
					$db->query("DROP TABLE IF EXISTS `people_maxis_link`");
					$db->query("DROP TABLE IF EXISTS `people_program_link`");
					$db->query("DROP TABLE IF EXISTS `people_tickets`");
					$db->query("DROP TABLE IF EXISTS `subscriber_pvr_book`");
					$db->query("DROP TABLE IF EXISTS `subscriber_pvr_request`");
					$db->query("DROP TABLE IF EXISTS `subscriber_ebilling`");
					$db->query("ALTER TABLE global CHANGE COLUMN phpeace_server phpeace_server VARCHAR(50) NOT NULL DEFAULT 'http://updates.phpeace.org'");
				break;
			}
			include_once(SERVER_ROOT."/../classes/log.php");
			$log = new Log();
			if($db->error_status)
			{
				$log->Write("error","DB update to version $db_version FAILED");
			}
			else
			{
				$db->VersionSet($db_version);
				$log->Write("info","DB updated to version $db_version");
			}
		}
	}
	
	/**
	 * Updates DB to the latest version
	 * If executed, return the latest DB version, otherwise 0
	 *
	 * @return int
	 */
	public function UpdateDBAll()
	{
		$return = 0;
		if($this->db_version_new > $this->db_version_old)
		{
			for($i=$this->db_version_old+1;$i<=$this->db_version_new;$i++)
			{
				$this->UpdateDB($i);
			}
			$return = $this->db_version_new;
		}
		return $return;
	}
}
?>
