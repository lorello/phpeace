<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
define('SERVICES_LOG_DAYS', 60);

class Services
{
	function __construct()
	{
	}
	
	public function ServiceStatusDeleteOld()
	{
		$sqlstr = "DELETE FROM service_status WHERE DATE_ADD(last_update, INTERVAL " . SERVICES_LOG_DAYS . " DAY)<CURRENT_TIMESTAMP";
        $db =& Db::globaldb();
		$db->begin();
		$db->lock( "service_status" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function ServiceStatusChangeNotify($name,$status,$notes)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/modules.php");
		$m = new Mail();
		$status_desc = $status? "UP":"DOWN";
		$subject = "[{$m->title}] $name is $status_desc";
		$message = "Service Status Change Notification\nFrom {$m->admin_web}\n\n$subject\n\n$notes\n\n\n";
		$users = $this->ServiceUsers($name);
		foreach($users as $user)
		{
			$m->SendMail($user['email'],$user['name'],$subject,$message,array());
		}
	}

	public function ServiceStatusGet($name)
	{
		$row = array();
        $db =& Db::globaldb();
		$name = $db->SqlQuote($name);
		$sqlstr = "SELECT status,notes,UNIX_TIMESTAMP(last_update) AS last_update_ts 
			FROM service_status 
			WHERE name='$name' 
			ORDER BY last_update DESC LIMIT 1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function ServiceStatusUpdate($name,$status,$unescaped_notes,$only_if_changed=false,$notify_down_always=false,$only_if_after=0)
	{
		if($name!="")
		{
			$name = strtoupper($name);
			$status_prev = $this->ServiceStatusGet($name);
            if((!$only_if_changed || ($only_if_changed && $status!=$status_prev['status'])) && (time() - ($only_if_after * 24 * 60 * 60)) > $status_prev['last_update_ts'])
			{
		        $db =& Db::globaldb();            
				$todayTime = $db->getTodayTime();
				$notes = $db->SqlQuote($unescaped_notes);
				$db->begin();
				$db->lock( "service_status" );
				$id_service_status = $db->nextId("service_status","id_service_status");
				$sqlstr = "INSERT INTO service_status (id_service_status,name,last_update,status,notes) 
					VALUES ('$id_service_status','$name','$todayTime','$status','$notes')  ";
				$res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
				if(($status!=$status_prev['status'] && ($status_prev['last_update_ts']>0 || $status==0)) || ($notify_down_always && $status==0))
				{
					$this->ServiceStatusChangeNotify($name,$status,$unescaped_notes);
				}
			}
		}
		else 
		{
			UserError("Service update with no service name specified",array('status'=>$status,'notes'=>$unescaped_notes));
		}
	}
	
	public function ServiceUsers($name)
	{
        $db =& Db::globaldb();
        $name = $db->SqlQuote($name);
		$rows = array();
		$sqlstr = "SELECT u.id_user,u.name,u.email,u.mobile,us.mobile AS sms,us.id_user_service 
			FROM user_services us
			INNER JOIN users u ON us.id_user=u.id_user
			WHERE us.service_name='$name' AND u.active=1
			ORDER BY u.name ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function ServicesAvailable()
	{
        $db =& Db::globaldb();
		$services = array();
		$sqlstr = "SELECT DISTINCT name FROM service_status ORDER BY name "; 
		$db->QueryExe($services, $sqlstr);
		return $services;
	}

	public function ServicesStatus(&$rows,$paged=true)
	{
        $db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT s.id_service_status,s.name,UNIX_TIMESTAMP(s.last_update) AS last_update_ts,s.status,s.notes
			FROM service_status s 
			ORDER BY s.last_update DESC ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	public function UserServices(&$rows,$id_user,$paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_user_service,service_name,mobile
			FROM user_services 
			WHERE id_user='$id_user' 
			ORDER BY service_name ";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}
	
	public function UserServicesAvailable($id_user)
	{
		$rows = array();
		$sqlstr = "SELECT DISTINCT ss.name,ss.name
			FROM service_status ss 
			LEFT JOIN user_services us ON ss.name=us.service_name AND us.id_user='$id_user' 
			WHERE us.id_user IS NULL";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UserServiceDelete($id_user_service)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "user_services" );
		$sqlstr = "DELETE FROM user_services 
			WHERE id_user_service='$id_user_service' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function UserServiceInsert($id_user,$service_name,$mobile)
	{
		$service = $this->ServiceStatusGet($service_name);
		if($service['last_update_ts']>0)
		{
			$service_name = strtoupper($service_name);
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "user_services" );
			$id_user_service = $db->nextId( "user_services", "id_user_service" );
			$sqlstr = "INSERT INTO user_services (id_user_service,id_user,service_name,mobile) 
				VALUES ('$id_user_service','$id_user','$service_name','$mobile') ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}

	public function UserServiceUpdate($id_user_service,$mobile)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "user_services" );
		$sqlstr = "UPDATE user_services SET mobile='$mobile' 
			WHERE id_user_service='$id_user_service' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
}

?>
