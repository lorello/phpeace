<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");
include_once(SERVER_ROOT."/../classes/validator.php"); 
define('MIME_VERSION',"1.0");
define('MIME_TRANSFER_ENCODING',"8bit");
define('MIME_BOUNDARY',"phpeace1967");
define('MIME_NEWLINE',"\r\n");
define('NO_MIME_WARNING',"If you are reading this, it means that your e-mail client\r\ndoes not support MIME. Please upgrade your e-mail client\r\nto one that does support MIME.");

class Mail
{
	public $clean_text = true;
	public $pub_web;
	public $admin_web;
	public $force_newsletter;
	public $staff_email;
	public $title;
	public $debug_mail;
	public $process_bounces;
	public $mail_from;
	public $wday_newsletter;

	/** 
	 * @var Ini */
	private $ini;
	/** 
	 * @var TextHelper */
	private $th;
	private $encoding;
	private $set_envelope_sender;
	private $specific_enveloper_sender; 
	private $xmailer;
	private $track_smtp_status;

	function __construct($track_smtp_status=false)
	{
		$this->ini = new Ini;
		$this->title = $this->ini->Get('title');
		$this->staff_email = $this->ini->Get('staff_email');
		$this->pub_web = $this->ini->Get("pub_web");
		$this->admin_web = $this->ini->Get("admin_web");
		$this->debug_mail = $this->ini->Get("debug_mail");
		$conf = new Configuration();
		$this->encoding = $conf->Get("encoding");
		$this->set_envelope_sender = $conf->Get("sender_address");
		$specific_sender_address = $conf->Get("specific_sender_address"); 
		if($this->set_envelope_sender && $specific_sender_address!="") 
		{ 
			$va = new Validator(); 
			if($va->Email($specific_sender_address)) 
			{ 
				$this->specific_enveloper_sender = $specific_sender_address; 
			}
		} 
		$this->specific_sender_address = $conf->Get("specific_sender_address");
		$this->process_bounces = $this->specific_enveloper_sender!="" && $conf->Get("bounces_host")!="";
		$this->mail_from = $conf->Get("mail_from");
		$this->force_newsletter = false;
		$this->track_smtp_status = $track_smtp_status;
		$phpeace = new PhPeace;
		$this->xmailer = $conf->Get("x_mailer")? "PhPeace " . $phpeace->version : "";
		$this->th = new TextHelper();
		$this->wday_newsletter = $this->ini->GetModule("topics","wday_newsletter",1);
	}

	/**
	 * Split a list of recipients (usually written as comma-separated email addresses)
	 * and validates them
	 * 
	 * @param string $address	List of recipients
	 * @param string $separator	Character used to separate addresses
	 * @return array			List of valid recipients
	 */
	public function AddressSplitAndValidate($address,$separator=",")
	{
		$recipients = array();
		if(strpos($address,$separator)!==false)
		{
			$recipients = explode($separator,$address);		
		}
		elseif($address!="")
		{
			$recipients[] = $address;
		}
		$valid_recipients = array();
		foreach($recipients as $recipient)
		{
			$recipient = trim($recipient);
			if($this->CheckEmail($recipient))
			{
				$valid_recipients[] = $recipient;
			}
		}
		return $valid_recipients;
	}
	
	public function AllFriends( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(f.insert_date) AS insert_date_ts,
		f.ip,t.name,a.headline
		FROM friends f
		INNER JOIN articles a ON f.id_article=a.id_article
		INNER JOIN topics t ON a.id_topic=t.id_topic
		ORDER BY f.insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	private function AttachmentFilename($attachment)
	{
		return ($attachment['id'] == 0 && isset($attachment['name']))? "{$attachment['name']}.{$attachment['ext']}" : "att{$attachment['id']}.{$attachment['ext']}";
	}
	
	private function BounceTypeSoft($message_body)
	{
		$is_soft = false;
		$soft_errors = array();
		$soft_errors[] = "Deferred: 421";
		$soft_errors[] = "Warning: could not send message for past";
		foreach($soft_errors as $soft_error)
		{
			if(preg_match("/$soft_error/i",$message_body))
				$is_soft = true;
		}
		return $is_soft;		
	}
	
	public function CheckEmail( $email )
	{
		$va = new Validator();
		return $va->Email($email);
	}

	public function FriendSend($id_article,$sender,$sender_email,$email,$message)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$ip = Varia::IP();
		$db =& Db::globaldb();
		$insert_date = $db->GetTodayTime();
		$db->begin();
		$db->lock( "friends" );
		$nextId = $db->nextId( "friends", "id_friend" );
		$insert = "INSERT INTO friends (id_friend,id_article,insert_date,ip)
			VALUES ($nextId,$id_article,'$insert_date','$ip')";
		$res[] = $db->query( $insert );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/layout.php");
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$row = $a->ArticleGet();
		$l = new Layout(false,false,false);
		$l->TopicInit($row['id_topic'],true);

		$subject = $l->tr->Translate("friend2");
		if ($sender!="")
			$note .= $l->tr->Translate("from") . ": $sender ($sender_email)\n\n";
		if ($message!="")
			$note .= "$message\n\n";

		if ($row['halftitle']!="")
			$note .= "{$row['halftitle']}\n";
		$note .= strtoupper($row['headline']);
		if ($row['subhead']!="")
			$note .= "\n{$row['subhead']}";
		$note .= "\n\n" . $l->irl->PublicUrlTopic("article",array('id'=>$id_article),$l->topic) . "\n\n";
		$note .= $l->tr->Translate("in") . " \"" . $l->topic->name . "\"\n";
		$note .= $l->tr->Translate("pubdate").": " . $l->FormatDate($row['written_ts']) . "\n\n";
		$extra = array();
		$extra['name'] = ($sender!="")? $sender : $l->topic->name;
		$extra['email'] = $sender_email;
		$this->SendMail($email, "", $subject, $note, $extra);
		return true;
	}

	public function NewsletterGet($id_topic)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.name AS topicname,newsletter,id_frequency,id_recipient,UNIX_TIMESTAMP(lastletter) AS lastletter_ts,
			users.name AS username,users.email,t.temail,t.newsletter_format,t.newsletter_rcpt,t.profiling,t.newsletter_people
			FROM topics t 
			LEFT JOIN users ON t.id_recipient=users.id_user
			WHERE t.id_topic=$id_topic";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function NewsletterSend($id_topic)
	{
		include_once(SERVER_ROOT."/../classes/datetime.php");
		$return = array();
		$return['send'] = false;
		$row = $this->NewsletterGet($id_topic);
		$send_people = $row['profiling'] && $row['newsletter_people'];
		if ($row['newsletter'] && ($row['id_recipient']>0 || $row['newsletter_rcpt']!="" || $send_people))
		{
			$seconds = FrequencySeconds($row['id_frequency']);
			$diff = time() - (int)$row['lastletter_ts'] - $seconds;
			$return['id_frequency'] = $row['id_frequency'];
			$return['topic'] = $row['topicname'];
			if ($diff>0 || $this->force_newsletter)
			{
				$is_html = $row['newsletter_format']=="1";
				$return['sent'] = false;
				$return['send'] = true;
				include_once(SERVER_ROOT."/../classes/layout.php");
				$l = new Layout(false,false,false);
				$params = array('timestamp'=>$row['lastletter_ts'],'seconds'=>$seconds,'is_html'=>false);
				$alt_text = "";
				$message = $l->Output("newsletter",0,$id_topic,1,$params);
				if($is_html)
				{
					$alt_text = $message;
					$params['is_html'] = true;
					$message = $l->Output("newsletter",0,$id_topic,1,$params);
				}
				$frequency = $l->tr->Translate("newsletter_frequency");
				$subject = $l->tr->TranslateParams("newsletter_title",array($row['topicname'],$frequency[$row['id_frequency']]));
				if ($l->newsletter_ready && $message!="")
				{
					
					$extra = array();
					$extra['name'] = $row['topicname'];
					if($row['temail']!="" && $this->CheckEmail($row['temail']))
						$extra['email'] = $row['temail'];
					$recipients = array();
					$recipients[] = $row['email'];
					if($row['newsletter_rcpt']!="")
					{
						if(strpos($row['newsletter_rcpt'],",")!==false)
							$recipients = explode(",",$row['newsletter_rcpt']);
						else
							$recipients[] = $row['newsletter_rcpt'];
					}
					foreach($recipients as $recipient)
					{
						if($this->CheckEmail($recipient))
						{
							$this->SendMail($recipient, "", $subject, $message, $extra, $is_html, array(), false, $alt_text);
						}
					}
					if($send_people)
					{
						$num = $l->topic->MailjobSearch(array(), true);
						if($num>0)
						{
							include_once(SERVER_ROOT."/../classes/mailjobs.php");
							$id_module = 4;
							$db = Db::globaldb();
							$message = $db->SqlQuote($message);
							$subject = $db->SqlQuote($subject);
							$sender_name = $db->SqlQuote($l->topic->name);
							$sender_email = $db->SqlQuote(isset($extra['email'])? $extra['email'] : $this->staff_email);
							$start_date = $db->getTodayTime();
							$mj = new Mailjobs($id_module);
							$unescaped_footer = $mj->MessageFooter($id_topic,false,$is_html,true);
							$recipients = $mj->RecipientsGet();
							$mj->Store($start_date,$sender_name,$sender_email,0,$subject,$message,$id_module,$id_topic,$recipients,$unescaped_footer,false,$is_html,array(),false,"",$id_topic,0,true,$alt_text,false);
						}
					}
					$return['sent'] = true;
					$return['user'] = $row['username'];
					if (!$this->force_newsletter)
						$this->SetLastTopicNewsletter($id_topic);
				}
			}
		}
		return $return;
	}

	public function NewsletterGlobalSend()
	{
		include_once(SERVER_ROOT."/../classes/layout.php");
		$l = new Layout(false,false,false);
		$message = $l->Output("gnewsletter",0,0,1,array(),true);
		if($l->newsletter_ready) {
			$this->TextClean($message);
			$this->clean_text = false;
			include_once(SERVER_ROOT."/../classes/users.php");
			$uu = new Users();
			$recipients = $uu->AdminUsers(4);
			$subject = $l->tr->TranslateParams("newsletter_global_title_" . (($this->wday_newsletter == "0")?"d":"w"),array($this->title,date("j.n.Y")));
			foreach($recipients as $recipient) {
				$this->SendMail($recipient['email'], $recipient['name'], $subject, $message, array());
			}
			$this->SetLastNewsletter();
		}
	}

	public function NewsletterUpdate( $id_topic, $newsletter, $format, $id_recipient, $recipient, $id_frequency, $lastletter, $newsletter_people )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topics" );
		$update = "UPDATE topics SET newsletter=$newsletter,id_recipient=$id_recipient,id_frequency=$id_frequency,lastletter='$lastletter',
			newsletter_format='$format',newsletter_rcpt='$recipient',newsletter_people='$newsletter_people'
			WHERE id_topic=$id_topic";
		$res[] = $db->query( $update );
		Db::finish( $res, $db);
	}

	public function ProcessBounces($num_messages=100)
	{
		$conf = new Configuration();
		$bounces_host = $conf->Get("bounces_host");
		$bounces_port = $conf->Get("bounces_port");
		$bounces_username = $conf->Get("bounces_username");
		$bounces_password = $conf->Get("bounces_password");
		$bounces_threshold = $conf->Get("bounces_threshold");
		if (!function_exists('imap_open'))
		{
			UserError("imap missing, cannot process bounces",array());
		}
		else 
		{
			$link = imap_open("{".$bounces_host.":".$bounces_port."}INBOX",$bounces_username,$bounces_password,CL_EXPUNGE);
			if (!$link)
			{
				UserError("Cannot create IMAP connection to {$bounces_host}:{$bounces_port}",array('imap_error'=>imap_last_error()));
			}
			else 
			{
				$num_messages_all = imap_num_msg($link);
				$num_messages_to_download = $num_messages_all>$num_messages? $num_messages : $num_messages_all;
				include_once(SERVER_ROOT."/../classes/people.php");
				$pe = new People();
				$va = new Validator();
				include_once(SERVER_ROOT."/../classes/users.php");
				$uu = new Users();
				include_once(SERVER_ROOT."/../classes/user.php");
				$u = new User();
				include_once(SERVER_ROOT."/../classes/log.php");
				$log = new Log();
				$num_bounces = 0;
				$bounces = array();
				$soft_bounce_score = $conf->Get("soft_bounce_score");
				$hard_bounce_score = $conf->Get("hard_bounce_score");
				for($i=1;$i<=$num_messages_to_download;$i++)
				{
					$message_body =  imap_body($link,$i);

					$match = array();
					preg_match ("/X-ListMember: (.*)/i",$message_body,$match);
					if (is_array($match) && isset($match[1]))
					{
						$user_email = trim($match[1]);
						$score = $this->BounceTypeSoft($message_body)? $soft_bounce_score : $hard_bounce_score;
						if($va->Email($user_email))
						{
							$user = $pe->UserGetByEmail($user_email,false);
							if($user['id_p']>0)
							{
								$tot_bounces = $pe->BounceIncrement($user['id_p'],$score);
								if($tot_bounces>=$bounces_threshold)
								{
									$pe->RevalidateEmail($user['id_p'], 0);
									$log->Write("info","User {$user['name1']} {$user['name2']} has reached the maximum number of bounces",array('bounces'=>$tot_bounces,'threshold'=>$bounces_threshold,'email'=>$user_email,'link'=>"{$this->admin_web}/people/person.php?id={$user['id_p']}"));
								}
							}
							$admin_user = $uu->UserByEmail($user_email);
							if($admin_user['id_user']>0)
							{
								$u->id = $admin_user['id_user'];
								$tot_bounces = $u->BounceIncrement($score);
								if($tot_bounces>=$bounces_threshold)
								{
									$log->Write("warning","Admin User {$admin_user['name']} has reached the maximum number of bounces",array('bounces'=>$tot_bounces,'threshold'=>$bounces_threshold,'email'=>$user_email,'link'=>"{$this->admin_web}/users/user.php?id={$admin_user['id_user']}"));
								}
							}
							if($user['id_p']>0 || $admin_user['id_user']>0)
							{
								$num_bounces++;
								$bounces['pub_msg'.$num_bounces] = $user_email; 
							}
						}
					}
			        imap_delete($link,$i);
				}
				imap_close($link,CL_EXPUNGE);
				if($num_messages_to_download>0)
				{
					$log->Write("info","Processed $num_messages_to_download messages, $num_bounces bounces found",$bounces);
				}
			}
		}
	}

	public function ResetBounces()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT mr.id_p 
			FROM mailjob_recipients mr 
			INNER JOIN people p ON mr.id_p=p.id_p 
			INNER JOIN sendmail mj ON mr.id_mail=mj.id_mail 
			WHERE p.bounces>0 AND DATEDIFF(mj.send_date,p.last_bounce)>30 
			GROUP BY mr.id_p";
		$db->QueryExe($rows, $sqlstr, false);
		if(count($rows)>0)
		{
			$db->begin();
			$db->lock( "people" );
			foreach($rows as $row)
			{
				$res[] = $db->query( "UPDATE people SET bounces=0 WHERE id_p={$row['id_p']}" );
			}
			Db::finish( $res, $db);
		}
	}
	
	public function SendMail($to_email, $to_name, $subject, $message, $extra, $is_html=false, $attachments=array(), $track_bounce=true, $alt_text="" )
	{
		$sent = false;
		if ($this->clean_text)
			$this->TextClean($message,$is_html);
		if($this->th->IsUnicode($subject))
		{
			$subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
		}
		else 
		{
			$subject = trim($subject);
			$this->TextClean($subject);		
		}
		$this->RecipientClean($to_name);
		$to = ($to_name!="" && $to_name!=$to_email)? mb_encode_mimeheader($to_name) . " <$to_email>" : $to_email;
		$from_email = $this->staff_email ;
		$from_name = $this->mail_from . $this->title ;
		if(isset($extra['force']) && $extra['force']) {
			$from_email = ($extra['email']!="" && ctype_print($extra['email']))? $extra['email'] : $this->staff_email ;
			$from_name = ($extra['name']!="" && ctype_print($extra['name']))? $extra['name'] : $this->mail_from . $this->title ;
		}
		$message = wordwrap($message, 70);
		$headers = "From: $from_name <$from_email>" . MIME_NEWLINE;
		$headers .= "Reply-To: $from_email" . MIME_NEWLINE;
		if($this->xmailer!="")
			$headers .= "X-Mailer: {$this->xmailer}" . MIME_NEWLINE;
		if($track_bounce)
			$headers .= "X-ListMember: $to_email" . MIME_NEWLINE;
		if ($extra['header']!='' && ctype_print(($extra['header'])))
			$headers .= $extra['header'] . MIME_NEWLINE;
		$headers .= "MIME-Version: " . MIME_VERSION . MIME_NEWLINE;
		if ($this->debug_mail)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager;
			$filename = $this->ini->Get('mail_output');
			$fm->WritePage( $filename, "\n--------Message--------\n\nDate: " . date("D, d-m-Y G:i:s") . "\nTo: $to\n$headers\nSubject: $subject\n\n$message\n\n", "a" );
			$sent = true;
		}
		else 
		{
			if(count($attachments)>0)
			{
				$headers .= "Content-Type: multipart/mixed; boundary=\"" . MIME_BOUNDARY . "\"" . MIME_NEWLINE;
				$mime_message = NO_MIME_WARNING . MIME_NEWLINE;
				$mime_message .= "--" . MIME_BOUNDARY . MIME_NEWLINE;
				if($is_html && $alt_text!="")
				{
				    $mime_message .= "Content-Type: text/plain; charset=\"{$this->encoding}\"" . MIME_NEWLINE;
				    $mime_message .= "Content-Transfer-Encoding: " . MIME_TRANSFER_ENCODING ."" . MIME_NEWLINE;
				    $mime_message .= MIME_NEWLINE;
				    $mime_message .= $alt_text . MIME_NEWLINE;
				    $mime_message .= "--" . MIME_BOUNDARY . MIME_NEWLINE;
				}
				$mime_message .= "Content-Type: " . ($is_html? "text/html" : "text/plain") . "; charset=\"{$this->encoding}\"" . MIME_NEWLINE;
				$mime_message .= "Content-Transfer-Encoding: " . MIME_TRANSFER_ENCODING ."" . MIME_NEWLINE;
				$mime_message .= MIME_NEWLINE;
				$mime_message .= $message . MIME_NEWLINE;
				$mime_message .= "--" . MIME_BOUNDARY . MIME_NEWLINE;
				$counter = 1;
				foreach($attachments as $attachment)
				{	
					$filename = $this->AttachmentFilename($attachment);
					$mime_message .= "Content-ID: <$filename>" . MIME_NEWLINE;
					$mime_message .= "Content-Type: {$attachment['type']}; name=\"$filename\"" . MIME_NEWLINE;
					$mime_message .= "Content-Transfer-Encoding: base64" . MIME_NEWLINE;
					$mime_message .= "Content-Disposition: attachment; file=\"$filename\"" . MIME_NEWLINE;
					$mime_message .= MIME_NEWLINE;
					$mime_message .= $attachment['enc'] . MIME_NEWLINE;
					$mime_message .= "--" . MIME_BOUNDARY . ($counter==count($attachments)?"--":"") . MIME_NEWLINE;
					$counter ++;
				}
				$message = $mime_message;
			}
			else 
			{
				if($is_html && $alt_text!="")
				{
					$headers .= "Content-Type: multipart/alternative; boundary=\"" . MIME_BOUNDARY . "\"" . MIME_NEWLINE;
					$mime_message = NO_MIME_WARNING . MIME_NEWLINE;
					$mime_message .= "--" . MIME_BOUNDARY . MIME_NEWLINE;
					$mime_message .= "Content-Type: text/plain; charset=\"{$this->encoding}\"" . MIME_NEWLINE;
					$mime_message .= "Content-Transfer-Encoding: " . MIME_TRANSFER_ENCODING ."" . MIME_NEWLINE;
					$mime_message .= MIME_NEWLINE;
					$mime_message .= $alt_text . MIME_NEWLINE;
					$mime_message .= "--" . MIME_BOUNDARY . MIME_NEWLINE;
					$mime_message .= "Content-Type: text/html; charset=\"{$this->encoding}\"" . MIME_NEWLINE;
    				$mime_message .= "Content-Transfer-Encoding: " . MIME_TRANSFER_ENCODING ."" . MIME_NEWLINE;
					$mime_message .= MIME_NEWLINE;
					$mime_message .= $message . MIME_NEWLINE;
					$mime_message .= "--" . MIME_BOUNDARY . "--" . MIME_NEWLINE;
					$message = $mime_message;
				}
				else 
				{
					$content_type = ($is_html)? "text/html" : "text/plain";
					$headers .= "Content-Type: $content_type; charset=\"{$this->encoding}\"" . MIME_NEWLINE;			
					$headers .= "Content-Transfer-Encoding: " . MIME_TRANSFER_ENCODING;
				}
			}
			if ($to_email!="" && $this->CheckEmail($to_email))
			{
				if($this->set_envelope_sender && $track_bounce && $this->specific_sender_address!="") {
					$sent = mail($to, $subject, $message, $headers, "-f {$this->specific_sender_address}"); 
				}
				else {
					$sent = mail($to, $subject, $message, $headers);
				}
	        }
		}
		return $sent;
	}

	private function TextClean( &$text, $is_html=false )
	{
		$this->th->TextClean($text,$is_html);
	}
	
	private function RecipientClean( &$text )
	{
		$text = trim($text);
		$invalid_characters = array(",","<",">");
		$text = str_replace($invalid_characters,"",$text);
	}

    public function SetLastNewsletter()
	{
		$db =& Db::globaldb();
		$update = "UPDATE global SET lastnewsletter='".$db->getTodayDate()."'";
		$db->begin();
		$db->lock( "global" );
		$res[] = $db->query( $update );
		Db::finish( $res, $db);
	}

	private function SetLastTopicNewsletter($id_topic)
	{
		$db =& Db::globaldb();
		$update = "UPDATE topics SET lastletter='".$db->getTodayDate()."' WHERE id_topic=$id_topic";
		$db->begin();
		$db->lock( "topics" );
		$res[] = $db->query( $update );
		Db::finish( $res, $db);
	}

}
?>
