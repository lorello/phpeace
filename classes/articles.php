<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Generic articles methods
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
abstract class Articles
{
	/**
	 * Retrieve all articles pending approval (paginated)
	 *
	 * @param array $rows
	 * @return integer	Num of articles
	 */
	public static function ArticlesToApprove( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT COUNT(id_article) AS counter,topics.id_topic,topics.name
			FROM articles
			INNER JOIN topics ON articles.id_topic=topics.id_topic
			WHERE approved=0 GROUP BY topics.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Retrieve all articles without a subtopic (paginated)
	 *
	 * @param array $rows
	 * @return integer Num of articles
	 */
	public static function ArticlesWithoutSubtopic( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_article,headline,UNIX_TIMESTAMP(written) AS written_ts,t.name AS topic_name,u.name
			FROM articles a
			INNER JOIN topics t USING(id_topic)
			INNER JOIN users u ON a.id_user=u.id_user
			WHERE a.id_subtopic=0 OR a.id_subtopic IS NULL ORDER BY a.written DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Number of approved articles
	 *
	 * @return integer
	 */
	public static function CountApproved()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT count(id_article) AS counter from articles where approved=1");
		return $row['counter'];
	}

	/**
	 * Check if highlight is still valid
	 *
	 * @param integer Num of days
	 */
	public static function HighlightCheck($highlight_days)
	{
	    if($highlight_days>0) {
	        $db =& Db::globaldb();
	        $db->begin();
	        $db->lock( "articles" );
	        $sqlstr = "UPDATE articles SET highlight=0 WHERE highlight=1 AND published < (CURRENT_DATE - INTERVAL $highlight_days DAY)";
	        $res[] = $db->query( $sqlstr );
	        Db::finish( $res, $db);
	    }
	}
	
	/**
	 * Retrieve minimum and maximum timestamp of articles insertion date
	 *
	 * @return array
	 */
	public static function Period()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,	"SELECT UNIX_TIMESTAMP(MIN(written))-86400 AS min_art_ts,UNIX_TIMESTAMP(MAX(written))+86400 AS max_art_ts FROM articles");
		return $row;
	}

	/**
	 * Free-text search of articles (paginated)
	 *
	 * @param array $rows
	 * @param array $params
	 * @param integer $sort_by
	 * @return integer	Num of articles found
	 */
	public static function Search( &$rows, $params, $sort_by=1 )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "	SELECT a.id_article,a.halftitle,a.headline,ah.subhead,UNIX_TIMESTAMP(a.written) AS written_ts,a.id_template,
		IF(a.show_author=1,IF(a.author<>'',a.author,users.name),'-') AS author,topics.name AS topic_name,topics.id_topic,a.id_subtopic,a.id_visibility,
		a.id_image,i.format
		FROM articles a
		INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
		INNER JOIN articles_content ac ON a.id_article=ac.id_article
		LEFT JOIN users ON a.id_user=users.id_user
		INNER JOIN topics ON a.id_topic=topics.id_topic
		LEFT JOIN images i ON a.id_image=i.id_image
		LEFT JOIN template_params tp ON a.id_template=tp.id_template
		LEFT JOIN template_values tv ON tp.id_template_param=tv.id_template_param AND tv.id_res=5 AND tv.id=a.id_article
		WHERE  " . (($params['approved']=="-1")?"1":"a.approved=1 " );
		if (strlen($params['title']) > 0)
			$sqlstr .= " AND (a.headline LIKE '%{$params['title']}%' OR a.halftitle LIKE '%{$params['title']}%' OR ah.subhead LIKE '%{$params['title']}%') ";
		if (strlen($params['author']) > 0)
			$sqlstr .= " AND (a.author LIKE '%{$params['author']}%' OR users.name LIKE '%{$params['author']}%') ";
		if (strlen($params['content']) > 0)
			$sqlstr .= " AND (a.headline LIKE '%{$params['content']}%' OR a.halftitle LIKE '%{$params['content']}%' OR ah.subhead LIKE '%{$params['content']}%'  OR ac.content LIKE '%{$params['content']}%'  OR ac.notes LIKE '%{$params['content']}%' OR a.source LIKE '%{$params['content']}%') ";
		if ($params['id_subtopic'] > 0)
			$sqlstr .= " AND a.id_subtopic={$params['id_subtopic']} ";
		if ($params['id_topic'] > 0)
			$sqlstr .= " AND a.id_topic={$params['id_topic']} ";
		if ($params['id_article'] > 0)
			$sqlstr .= " AND a.id_article<>{$params['id_article']} ";
		if ($params['id_template'] > -1)
			$sqlstr .= " AND a.id_template={$params['id_template']} ";
		if ((strlen($params['written1']) > 2) && (strlen($params['written2']) > 2))
			$sqlstr .= " AND a.written BETWEEN '{$params['written1']}' AND '{$params['written2']}'";
		$sqlstr .= " GROUP BY a.id_article ORDER BY ";
		switch($sort_by)
		{
			case "0":
				$sqlstr .= "a.published DESC,";
			break;
			case "1":
				$sqlstr .= "a.written DESC,";
			break;
			case "2":
				$sqlstr .= "a.id_visibility DESC,a.published DESC,";
			break;
			case "3":
				$sqlstr .= "a.id_visibility DESC,a.written DESC,a.published DESC,";
			break;
			case "4":
				$sqlstr .= "a.original DESC,a.written DESC,";
			break;
			case "5":
				$sqlstr .= "a.headline ASC,";
			break;
		}
		$sqlstr .= "a.id_article DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Free-text search of related articles (paginated)
	 *
	 * @param array $rows
	 * @param array $params
	 * @return integer
	 */
	public static function SearchRelated( &$rows, $params )
	{
		$db =& Db::globaldb();
		$rows = array();
		$id_article = $params['id_article'];
		$sqlstr = "SELECT a.id_article,halftitle,headline,subhead,UNIX_TIMESTAMP(written) AS written_ts,
				IF(show_author=1,IF(author<>'',author,users.name),'-') AS author,topics.name AS topic_name,topics.id_topic,a.id_subtopic
				FROM articles a
				LEFT JOIN articles_related ar ON a.id_article=ar.id_article2 AND ar.id_article1=$id_article
				INNER JOIN articles_subhead ah ON a.id_article=ah.id_article
				INNER JOIN articles_content ac ON a.id_article=ac.id_article
				INNER JOIN users ON a.id_user=users.id_user
				INNER JOIN topics ON a.id_topic=topics.id_topic
				WHERE approved=1 AND a.id_article<>$id_article AND id_article2 IS NULL";
		if (strlen($params['title']) > 0)
			$sqlstr .= " AND (headline LIKE '%{$params['title']}%' OR halftitle LIKE '%{$params['title']}%' OR subhead LIKE '%{$params['title']}%') ";
		if (strlen($params['author']) > 0)
			$sqlstr .= " AND (author LIKE '%{$params['author']}%' OR users.name LIKE '%{$params['author']}%') ";
		if (strlen($params['content']) > 0)
			$sqlstr .= " AND (headline LIKE '%{$params['content']}%' OR halftitle LIKE '%{$params['content']}%' OR subhead LIKE '%{$params['content']}%'  OR content LIKE '%{$params['content']}%'  OR notes LIKE '%{$params['content']}%' OR source LIKE '%{$params['content']}%') ";
		if ($params['id_subtopic'] > 0)
			$sqlstr .= " AND a.id_subtopic={$params['id_subtopic']} ";
		if ($params['id_topic'] > 0)
			$sqlstr .= " AND a.id_topic={$params['id_topic']} ";
		$sqlstr .= " AND written BETWEEN '{$params['written1']}' AND '{$params['written2']}'";
		$sqlstr .= "  ORDER BY a.written DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Index-based search of articles (paginated)
	 *
	 * @param array $rows
	 * @param array $words
	 * @param integer $id_template
	 * @param integer $id_topic
	 * @param integer $id_group
	 * @param integer $time_weight
	 * @return integer Num of articles found
	 */
	public static function SearchPub( &$rows, $words,$id_template,$id_topic=0,$id_group=0,$time_weight=0,$id_subtopic=0 )
	{
		if(is_null($id_template))
			$id_template = -1;
		if(count($words)>0)
		{
			$db =& Db::globaldb();
			$sqljoin = "";
			for ($i = 0; $i < count($words); $i++)
			{
				$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = a.id_article AND si$i.id_res=5 ";
				if($id_group>0)
					$sqljoin .= " AND si$i.id_group='$id_group' ";
				elseif($id_topic>0)
					$sqljoin .= " AND si$i.id_topic='$id_topic' ";
				$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
				$wheres[] = "sw$i.word = '$words[$i]'";
				$scores[] = "SUM(si$i.score)";
			}
			$sqlstr = "SELECT a.id_article,a.halftitle,a.headline,ah.subhead,
				UNIX_TIMESTAMP(a.written) AS written_ts,'article' AS item_type,show_author,author,a.id_subtopic,a.id_topic,
				a.id_user,a.available,a.show_date,a.original,a.show_source,IF(a.jump_to_source,a.source,'') AS jump,a.id_template,a.id_language ";
			if(count($scores)>0)
				$sqlstr .= ", ((" . implode("+",$scores) . ") ";
			if($time_weight>0)
				$sqlstr .= " - DATEDIFF(CURDATE(),a.written)*$time_weight ";
			$sqlstr .= " ) AS total_score ";
			$sqlstr .= "	FROM articles a
				INNER JOIN articles_subhead ah ON a.id_article=ah.id_article $sqljoin ";
				$sqlstr .= " WHERE a.approved=1 AND a.published>0 ";
			if(count($wheres)>0)
				$sqlstr .= " AND " . implode(" AND ", $wheres);
			if($id_template!=0)
			{
				if($id_template==-1)
					$id_template = 0;
				$sqlstr .= " AND a.id_template=$id_template ";
			}
			if($id_subtopic>0)
				$sqlstr .= " AND a.id_subtopic=$id_subtopic ";
			$sqlstr .= " GROUP BY a.id_article ORDER BY ";
			if(count($scores)>0)
				$sqlstr .= " total_score DESC, ";
			$sqlstr .= " a.written DESC";
			$num = $db->QueryExe($rows, $sqlstr,true);
		}
		else 
			$num = 0;
		return $num;
	}
	
	/**
	 * Retrieve all articles associated to a template
	 *
	 * @param integer $id_template
	 * @return array
	 */
	public static function Template($id_template)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_article,headline,UNIX_TIMESTAMP(written) AS written_ts
			FROM articles a
			WHERE a.id_template='$id_template'
			ORDER BY a.written DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
}

/**
 * Web services for articles
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class ArticlesWS
{
	/**
	 * Installation key (used for authorizing requests)
	 *
	 * @var string
	 */
	private $install_key;
	
	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$this->install_key = $ini->Get("install_key");   
	}
	
	/**
	 * Get all articles tagged with a specific keyword
	 *
	 * @param string $keyword	Keyword
	 * @param string $key 		Installation key for authorization
	 * @param string $limit		Limit
	 * @return string			Serialized array of articles
	 * 
	 */
	public function ArticlesByKeyword($keyword,$key,$limit)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
        $articles = array();
		if($key!="" && $key==$this->install_key)
		{
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$id_keyword = $k->KeywordGetId($keyword,0,false);
			if($id_keyword>0)
			{
				$limit = (is_numeric($limit) && $limit>0)? $limit : 15;
				$rows = $k->UseArticles($id_keyword,0,$limit,2);
				if(count($rows)>0)
				{
					include_once(SERVER_ROOT."/../classes/layout.php");
					$l = new Layout(false,false,false);
					foreach($rows as $row)
					{
						$l->TopicInit($row['id_topic'],true);
						$articles[] = $l->ArticleContent($row['id_article'],false,false,true);
					}
				}
			}
		}
		$va = new Varia();
		return $va->Serialize($articles,false,false);
	}
}
?>
