<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

Class GroupHelper
{
	public $type;
	public $top_name;
	public $top_description;
	public $groups;
	public $input_right;

	/** 
	 * @var TreeHelper */
	public $th;
	
	private $table;

	function __construct($type)
	{
		$this->type = $type;
		$this->table = $type."_groups";
		$this->input_right = false;
	}

	function __destruct()
	{
		unset($this->th);
	}

	public function GroupDelete( $id_group )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "$this->table" );
		$res[] = $db->query( "DELETE FROM $this->table WHERE id_group=$id_group" );
		Db::finish( $res, $db);
		$this->GroupReshuffle();
	}
	
	public function GroupGet( $id_group )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT g1.id_group,g1.name AS name,g1.description,g1.id_parent,g1.seq,g2.name AS parent_name,g1.visible
		FROM $this->table g1
		LEFT JOIN $this->table g2 ON g1.id_parent=g2.id_group
		WHERE g1.id_group=$id_group GROUP BY g1.id_group ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function GroupInsert($name,$description,$id_parent,$visible,$keywords="")
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$seq = $this->GroupNextSeq($id_parent);
		$next_id = $db->nextId( $this->table, "id_group" );
		$sqlstr = "INSERT INTO $this->table (id_group,name,description,id_parent,seq,visible)
				VALUES ($next_id,'$name','$description',$id_parent,$seq,$visible)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $next_id, $o->types[$this->type . '_group']);		
		include_once(SERVER_ROOT."/../classes/history.php");
		$h = new History();		
		$h->HistoryAdd($h->types[$this->type . '_group'],$next_id,$h->actions['create']);
		return $next_id;
	}

	private function GroupItemUpdateSeq($id_item,$seq)
	{
		switch ($this->type)
		{
			case "galleries":
				$sqlstr = "";
			break;
			case "topics";
				$sqlstr = "UPDATE topics set seq=$seq WHERE id_topic='$id_item' ";
			break;
		}
		if($sqlstr!="")
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( $this->type );
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}

	public function GroupItems($id_group)
	{
		$rows = array();
		if ($id_group>0)
		{
			$db =& Db::globaldb();
			switch ($this->type)
			{
				case "galleries":
					$sqlstr = "SELECT g.id_gallery AS id_item,g.title AS name,g.description,g.visible,
					UNIX_TIMESTAMP(g.released) AS released_ts,g.show_date,g.author,
					COUNT(ig.id_image) AS counter,'gallery' AS type
					FROM galleries g
					LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery
					WHERE g.id_group=$id_group GROUP BY g.id_gallery ORDER BY g.released DESC";
				break;
				case "topics";
					$sqlstr = "SELECT id_topic AS id_item,name,description,path,visible,show_latest,'topic' AS type,seq
					FROM topics WHERE id_group=$id_group ORDER BY seq,name";
				break;
			}
			$db->QueryExe($rows, $sqlstr);
		}
		return $rows;
	}

	public function GroupItemsVisible($id_group)
	{
		$db =& Db::globaldb();
		$rows = array();
		switch ($this->type)
		{
			case "galleries":
				$sqlstr = "SELECT g.id_gallery,g.title,g.description AS name FROM galleries g
				INNER JOIN galleries_groups gg ON g.id_group=gg.id_group AND gg.visible=1
				WHERE g.visible=1 ";
				if ($id_group>0)
					$sqlstr .= " AND gg.id_group='$id_group' ";
				$sqlstr .= " ORDER BY g.id_gallery";
			break;
			case "topics";
				$sqlstr = "SELECT t.id_topic,t.name,t.path,t.description,t.archived FROM topics t
				INNER JOIN topics_groups tg ON t.id_group=tg.id_group AND tg.visible=1
				WHERE t.visible=1 ";
				if ($id_group>0)
					$sqlstr .= " AND tg.id_group='$id_group' ";
				$sqlstr .= " ORDER BY t.archived,t.seq";
			break;
		}
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function GroupItemMove($id_item,$id_group,$dir)
	{
		$items = $this->GroupItems($id_group);
		$counter = 1;
		$id_item_prev = 0;
		foreach($items as $item)
		{
			if ($item['id_item']==$id_item && $dir==1)
			{
				$this->GroupItemUpdateSeq( $id_item , ($item['seq']-1) );
				$this->GroupItemUpdateSeq( $id_item_prev , $item['seq'] );
			}
			if ($id_item_prev==$id_item && $dir==0)
			{
				$this->GroupItemUpdateSeq( $item['id_item'] , ($item['seq']-1) );
				$this->GroupItemUpdateSeq( $id_item_prev , $item['seq'] );
			}
			$id_item_prev = $item['id_item'];
			$counter ++;
		}
	}

	public function GroupMove($id_group,$dir)
	{
		$this->LoadTree();
		$group_data = $this->th->GroupData($id_group);
		$id_parent = $group_data['id_parent'];
		$children = $this->th->GroupChildren($id_parent);
		$counter = 1;
		$id_seq_prev = 0;
		if (!($dir==1 && $id_group==$children[0]['id_group']) && !($dir==0 && $id_group==$children[count($children)-1]['id_group']))
		{
			foreach($children as $child)
			{
				if ($child['id_group']==$id_group && $dir==1)
				{
					$this->GroupUpdateSeq( $id_group , ($child['seq']-1) );
					$this->GroupUpdateSeq( $id_seq_prev , $child['seq'] );
				}
				if ($id_seq_prev==$id_group && $dir==0)
				{
					$this->GroupUpdateSeq( $child['id_group'] , ($child['seq']-1) );
					$this->GroupUpdateSeq( $id_seq_prev , $child['seq'] );
				}
				$id_seq_prev = $child['id_group'];
				$counter ++;
			}
		}
	}

	private function GroupNextSeq($id_parent)
	{
		$this->LoadTree();
		$children = $this->th->GroupChildren($id_parent);
		return (sizeof($children)+1);
	}

	private function GroupParseTreeForReshuffle($id_root)
	{
		$children = $this->th->GroupChildren($id_root);
		$counter = 1;
		foreach($children as $child)
		{
			$this->GroupUpdateSeq($child['id_group'],$counter);
			$counter ++;
			$this->GroupItemsReshuffle($child['id_group']);
			$this->GroupParseTreeForReshuffle($child['id_group']);
		}
	}
	
	private function GroupItemsReshuffle($id_group)
	{
		$items = $this->GroupItems($id_group);
		$icounter = 1;
		foreach($items as $item)
		{
			$this->GroupItemUpdateSeq($item['id_item'],$icounter);
			$icounter++;
		}
	}

	public function GroupReshuffle()
	{
		$this->LoadTree(true);
		if (count($this->th->tree)>0)
			$this->GroupParseTreeForReshuffle(0);
		return sizeof($this->th->tree);
	}

	public function GroupUpdate($id_group,$name,$description,$id_parent,$id_parent_old,$visible,$keywords="")
	{
		if ($id_parent!=$id_parent_old)
		{
			$seq = $this->GroupNextSeq($id_parent);
			$cond = ",seq=$seq";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "$this->table" );
		$sqlstr = "UPDATE $this->table SET name='$name',description='$description',id_parent=$id_parent,visible=$visible $cond
			WHERE id_group=$id_group";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_group, $o->types[$this->type . '_group']);
		include_once(SERVER_ROOT."/../classes/history.php");
		$h = new History();		
		$h->HistoryAdd($h->types[$this->type . '_group'],$id_group,$h->actions['update']);
		if ($id_parent!=$id_parent_old)
			$this->GroupReshuffle();
	}

	private function GroupUpdateSeq($id_group,$seq)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "$this->table" );
		$res[] = $db->query( "UPDATE $this->table SET seq=$seq WHERE id_group=$id_group" );
		Db::finish( $res, $db);
	}

	public function GroupsAll($only_visible=false,$only_first_level=false)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_group,name,id_parent,seq,description,visible,'$this->type' AS type FROM $this->table WHERE 1=1 ";
		if($only_visible)
			$sqlstr .= " AND visible=1 ";
		if($only_first_level)
			$sqlstr .= " AND id_parent=0 ";
		$sqlstr .= " ORDER BY seq";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function LoadTree( $force=false )
	{
		if (!(isset($this->groups)) || $force==true)
		{
			unset($this->th);
			include_once(SERVER_ROOT."/../classes/treehelper.php");
			$this->groups = $this->GroupsAll(false);
			$this->th = new TreeHelper($this->groups);
		}
	}
	
	public function UserRightSet()
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		include_once(SERVER_ROOT."/../classes/modules.php");
		$module = Modules::ModuleGetId($this->type);
		$u = new User;
		$this->input_right = $u->ModuleAdmin($module['id_module']);
	}

}
?>
