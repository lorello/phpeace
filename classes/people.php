<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
define('COOKIE_NAME',"pauth");
define('VERIFY_TOKEN_TTL',432000);
define('SALESFORCE_URL',"http://emea.salesforce.com/");
define('LOGIN_COUNTER_VARNAME',"people_login_counter");
define('UNIQUE_SESSIONS_DAYS', 5);

class People
{
	public $verification_required;

	private $token_actions;
	private $verify_token_ttl;
    private $log_login;
	private $block_multiple_sessions;
	
	const PeopleEmailVerificationSent = -1;
	const PeopleEmailVerificationMissing = 0;
	const PeopleEmailVerificationCompleted = 1;
	
	function __construct()
	{
		$conf = new Configuration();
		$this->verify_token_ttl = $conf->Get("verify_token_ttl");
		$this->verification_required = $conf->Get("verification_required");
        $this->log_login = $conf->Get("log_login");
		$this->block_multiple_sessions = $conf->Get("block_multiple_sessions");
		$this->token_actions = array('verify_email'	=> 1);
	}

	/**
	 * Authenticates a public visitor based on its password
	 * Also taking into account if email verification is required
	 *
	 * @param int $id_p
	 * @param string $password
	 * @return boolean
	 */
	public function Auth($id_p,$password,$id_topic=0,$hash_password='')
	{
		$auth = false;
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT password,verified FROM people WHERE id_p='$id_p'");
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		$dpass = $sc->decrypt($row['password']);
        
        if (!empty($hash_password))
            $dpass = call_user_func($hash_password, $dpass);                
		
        if($password==$dpass && (!$this->verification_required || $row['verified']==self::PeopleEmailVerificationCompleted ))
		{
			$auth = true;
		}
		if($password==$dpass && $this->verification_required && $row['verified']==self::PeopleEmailVerificationMissing)
		{
			$this->VerifyEmail($id_p, $id_topic);
		}
		return $auth;
	}

	public function AuthAndSet($id_p,$password,$identifier,$token,$set_timeout,$id_topic=0)
	{
		$auth = false;
		if($this->Auth($id_p,$password,$id_topic))
		{
			$auth = true;
			$row = $this->UserGetByIdentifier($identifier);
			$this->UpdateCookie($identifier,$token,$row['connections'],true,$set_timeout);
			if($this->block_multiple_sessions)
			{
				$this->UniqueSessionStore($id_p);
			}
			$row['connections'] = $row['connections'] + 1;
			$user = $this->Row2User($row);
			$user = $this->UserTopicsGroups($id_p,$user);
			$this->SetUserSession($auth,$user);
		}
		return $auth;
	}
	
	public function AuthById($id_p,$auth)
	{
		$row = $this->UserGetById($id_p);
		$this->UpdateCookie($row['identifier'],$row['token'],$row['connections'],true,true);
		if($this->block_multiple_sessions)
		{
			$this->UniqueSessionStore($id_p);
		}
		$row['connections'] = $row['connections'] + 1;
		$user = $this->Row2User($row);
		$user = $this->UserTopicsGroups($id_p,$user);
		$this->SetUserSession($auth,$user);
	}
	
	public function AuthenticateUser($id_p,$name1,$name2,$name3,$email,$token,$timeout,$id_language,$verified,$connections,$email_valid,$identifier)
	{
		// SELECT p.id_p,p.name1,p.name2,p.name3,p.email,p.token,p.timeout,p.id_language,p.verified,p.connections,p.email_valid,p.identifier
	}
	
	private function UserTopicsGroups($id_p,$user)
	{
		$topics = $this->UserTopics($id_p);
		if(count($topics)>0)
		{
			foreach($topics as $topic)
				$user['t'.$topic['id_topic']] = array('xname'=>"topic",'id'=>$topic['id_topic'],'name'=>$topic['name']);
		}
		$groups = $this->UserGroups($id_p);
		if(count($groups)>0)
		{
			foreach($groups as $group)
				$user['g'.$group['id_pt_group']] = array('xname'=>"group",'id'=>$group['id_pt_group'],'name'=>$group['name']);
		}
		return $user;
	}
	
	public function BounceIncrement($id_p,$score)
	{
		$db =& Db::globaldb();
		$today = $db->getTodayTime();
		$sqlstr = "UPDATE people SET bounces=bounces+$score,last_bounce='$today' WHERE id_p='$id_p' ";
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$user = $this->UserGetDetailsById($id_p);
		return $user['bounces'];
	}
	
	public function ConfigurationUpdate($profiling,$users_path)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->Set("profiling",$profiling);
		$ini->SetPath("users_path",$users_path);
	}
	
	public function ContactUpdate($id_p,$portal,$topics,$campaigns)
	{
		$this->ContactUpdatePortal($id_p, $portal);
		$db =& Db::globaldb();
		if(sizeof($campaigns)==0)
			$campaigns = array();
		include_once(SERVER_ROOT."/../classes/campaigns.php");
		$pcampaigns = Campaigns::SubscribedPerson($id_p);
		if(count($pcampaigns)>0)
		{
			$db->begin();
			$db->lock( "topic_campaigns_persons" );
			foreach($pcampaigns as $pcampaign)
			{
				$sqlstr = "UPDATE topic_campaigns_persons  SET contact=";
				$sqlstr .= (in_array($pcampaign['id_topic_campaign'],$campaigns))?  "1":"0";
				$sqlstr .= "  WHERE id_p='$id_p' AND id_topic_campaign='{$pcampaign['id_topic_campaign']}' ";
				$res[] = $db->query($sqlstr);
			}
			Db::finish( $res, $db);
		}
		if(sizeof($topics)==0)
			$topics = array();
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$ptopics = $tt->SubscribedPerson($id_p);
		if(count($ptopics)>0)
		{
			$db->begin();
			$db->lock( "people_topics" );
			foreach($ptopics as $ptopic)
			{
				$sqlstr = "UPDATE people_topics SET contact=";
				$sqlstr .= (in_array($ptopic['id_topic'],$topics))?  "1":"0";
				$sqlstr .= "  WHERE id_p='$id_p' AND id_topic='{$ptopic['id_topic']}' ";
				$res[] = $db->query($sqlstr);
			}
			Db::finish( $res, $db);
		}
	}
	
	public function ContactUpdatePortal($id_p,$portal)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query("UPDATE people SET contact='$portal' WHERE id_p=$id_p ");
		Db::finish( $res, $db);
	}
	
	public function CounterGet()
	{
		$session = new Session();
		return $session->Get(LOGIN_COUNTER_VARNAME);
	}
	
	public function CounterIncrement()
	{
		$session = new Session();
		$counter = $session->IsVarSet(LOGIN_COUNTER_VARNAME)? $session->Get(LOGIN_COUNTER_VARNAME) : 1;
		$session->Set(LOGIN_COUNTER_VARNAME,$counter + 1);
	}
	
	public function CounterReset()
	{
		$session = new Session();
		$session->Set(LOGIN_COUNTER_VARNAME,1);
	}
	
	public function Deactivate($id_p,$email_too=true)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET active='0'";
		if($email_too)
			$sqlstr .= ",email_valid='0' ";
		$sqlstr .= " WHERE id_p='$id_p' ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}

	public function Delete($id_p,$core_tables_only=true)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query("DELETE FROM people WHERE id_p='$id_p' ");
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "people_params" );
		$res[] = $db->query("DELETE FROM people_params WHERE id_p='$id_p' ");
		Db::finish( $res, $db);
		if(!$core_tables_only)
		{
			$payer = array();
			$db->query_single($payer,"SELECT id_payer FROM payers WHERE id_p='$id_p' ");
			if($payer['id_payer']>0)
			{
				$db->begin();
				$db->lock( "payments" );
				$res[] = $db->query( "DELETE FROM payments WHERE id_payer='{$payer['id_payer']}' " );
				Db::finish( $res, $db);
			}
			$tables = $this->PeopleTables();
			foreach($tables as $table)
			{
				$db->begin();
				$db->lock( $table );
				$res[] = $db->query("DELETE FROM $table WHERE id_p='$id_p' ");
				Db::finish( $res, $db);
			}
		}
	}
	
	public function EmailDomainChange($domain_from,$domain_to)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_p,email FROM people WHERE email LIKE '%$domain_from%' ";
		$db->QueryExe($rows, $sqlstr);
		$counter = 0;
		if(count($rows)>0)
		{
			foreach($rows as $row)
			{
				$email = str_replace($domain_from,$domain_to,$row['email']);
				$email = $db->SqlQuote($email);
				$this->UserUpdateEmail($row['id_p'],$email);
				$counter++;
			}
		}
		return $counter;
	}
	
	public function Groups($id_p,$show_all,$id_topic)
	{
		$rows = array();
		$db =& Db::globaldb();
		if($show_all)
		{
			$sqlstr = "SELECT ptg.id_pt_group,ptg.name,pg.id_p,t.name AS topic_name
				FROM people_topics_groups ptg
				LEFT JOIN people_groups pg ON pg.id_pt_group=ptg.id_pt_group  AND pg.id_p=$id_p 
				LEFT JOIN topics t ON ptg.id_topic=t.id_topic ";
			if($id_topic>0)
				$sqlstr .= " WHERE ptg.id_topic='$id_topic' ";
			$sqlstr .= " GROUP BY ptg.id_pt_group  ORDER BY ptg.name ";
		}
		else 
		{
			$sqlstr = "SELECT ptg.id_pt_group,ptg.name,pg.id_p
				FROM people_groups pg
				INNER JOIN people_topics_groups ptg ON pg.id_pt_group=ptg.id_pt_group
				WHERE ptg.id_topic='$id_topic' AND pg.id_p=$id_p ";		
		}
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function GroupAdd($id_p,$id_pt_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_groups" );
		$sqlstr = "INSERT INTO people_groups (id_p,id_pt_group) VALUES ($id_p,$id_pt_group)";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}
	
	public function GroupsDelete($id_p,$id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("people_groups","people_topics_groups"));
		$sqlstr = "DELETE FROM people_groups WHERE id_p=$id_p ";
		if($id_topic>0)
			$sqlstr.= " AND id_pt_group IN (SELECT id_pt_group FROM people_topics_groups WHERE id_topic=$id_topic) ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function IdLanguage($id_topic)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$id_language = (int)$session->Get("id_language");
		if(!$id_language>0)
		{
			if($id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$id_language = $t->id_language;
			}
			else 
			{
				include_once(SERVER_ROOT."/../classes/ini.php");
				$ini = new Ini();
				$id_language = $ini->Get("id_language");
			}
		}
		return $id_language;
	}
	
	public function Import($id_topic,$contact,$id_geo,$id_pt_group,$data)
	{
		$num = 0;
		if($data!="")
		{
			$data = str_replace("\r\n", "\n", $data);
			$lines = explode("\n",$data);
			if(count($lines)>0)
			{
				$id_topic = (int)$id_topic;
				$contact_portal = $contact;
				$contact_topic = $id_topic>0 && $contact;
				$db =& Db::globaldb();
				include_once(SERVER_ROOT."/../classes/validator.php");
				$va = new Validator();
				foreach($lines as $line)
				{
					$line = trim($line);
					if($line!="")
					{
						$vals = explode("|",$line);
						if(count($vals)>0)
						{
							$email = $db->SqlQuote(trim($vals[0]));
							$name1 = isset($vals[1])? $db->SqlQuote(trim($vals[1])) : "";
							$name2 = isset($vals[1])? $db->SqlQuote(trim($vals[2])) : "";
							if($va->Email($email))
							{
								$id_p = $this->UserCreate($name1,$name2,$email,$id_geo,$contact_portal,array(),false,$id_topic,$contact_topic,false);						
								if($id_p>0)
								{
									$num++;
									if($id_pt_group>0)
									{
										$this->TopicGroupAssociate($id_p,$id_pt_group);
									}
								}
							}
						}
					}
				}
			}
		}
		return $num;
	}
	
	public function IsUserAlreadyRegistered($email)
	{
		$user = $this->UserGetByEmail($email);
		$identifier = $this->UserIdentifier($email);
		$user_by_identifier = $this->UserGetByIdentifier($identifier);
		return ($user['id_p']>0 || $user_by_identifier['id_p']>0);
	}
	
	public function MailjobSearch($id_item,$params)
	{
	    include_once(SERVER_ROOT."/../classes/mailjobs.php");
	    $mj = new Mailjobs(28);
	    $sqlstr = "SELECT p.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.email AS mj_email,p.password AS mj_password,p.id_geo AS mj_geo ";
	    if ($params['id_pt_group']>0) {
	        $sqlstr .= "FROM people_topics pt
		           INNER JOIN people p ON pt.id_p=p.id_p 
	               INNER JOIN people_groups pg ON pt.id_p=pg.id_p AND pg.id_pt_group='{$params['id_pt_group']}' 
	               WHERE pt.contact=1 AND p.active=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
	    } else {
	        $sqlstr .= "FROM people p
			WHERE p.active=1 AND p.email_valid=1 AND p.contact=1 AND p.bounces<{$mj->bounces_threshold} ";
	    }
		if (strlen($params['name'])>0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%') ";
		if (strlen($params['email'])>0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if ($params['id_geo']>0)
			$sqlstr .= " AND p.id_geo='{$params['id_geo']}' ";
		if ($params['verified']=="on")
			$sqlstr .= " AND p.verified=" . self::PeopleEmailVerificationCompleted ;
		$sqlstr .= " ORDER BY p.name1 ";
		return $mj->RecipientsSet($sqlstr,true,true);
	}
	
	public function Merge($id_p_from,$id_p_to)
	{
		if($id_p_from>0 && $id_p_to>0)
		{
			$payer_from = array();
			$db =& Db::globaldb();
			$db->query_single($payer_from,"SELECT id_payer FROM payers WHERE id_p='$id_p_from' ");
			if($payer_from['id_payer']>0)
			{
				$payer_to = array();
				$db->query_single($payer_to,"SELECT id_payer FROM payers WHERE id_p='$id_p_to' ");
				if($payer_to['id_payer']>0)
				{
					$db->begin();
					$db->lock( "payments" );
					$sqlstr = "UPDATE payments SET id_payer='{$payer_to['id_payer']}'  WHERE id_payer='{$payer_from['id_payer']}' ";
					$res[] = $db->query($sqlstr);
					Db::finish( $res, $db);
					$db->begin();
					$db->lock( "payers" );
					$res[] = $db->query( "DELETE FROM payers WHERE id_payer='{$payer_from['id_payer']}' " );
					Db::finish( $res, $db);
				}
				else 
				{
					$db->begin();
					$db->lock( "payers" );
					$sqlstr = "UPDATE payers SET id_p='$id_p_to'  WHERE id_payer='{$payer_from['id_payer']}' ";
					$res[] = $db->query($sqlstr);
					Db::finish( $res, $db);
				}
			}
			$tables = $this->PeopleTables();
			foreach($tables as $table)
			{
				$db->begin();
				$db->lock( $table );
				$sqlstr = "UPDATE $table SET id_p='$id_p_to'  WHERE id_p='$id_p_from' ";
				$res[] = $db->query($sqlstr);
				Db::finish( $res, $db);
			}
			$this->Delete($id_p_from);
		}
	}
	
	public function PasswordChange($password_old,$password_new)
	{
		$return = false;
		$user = $this->UserGetByCookie();
		if ($user['id']>0)
		{
			if($this->Auth($user['id'],$password_old))
			{
				include_once(SERVER_ROOT."/../classes/crypt.php");
				$sc = new Scrypt;
				$cpassword = $sc->encrypt($password_new);
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( "people" );
				$sqlstr = "UPDATE people SET password='$cpassword'  WHERE id_p='{$user['id']}' ";
				$res[] = $db->query($sqlstr);
				Db::finish( $res, $db);
				$return = true;
			}
		}
		return $return;
	}
	
	public function PasswordGet($id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT password FROM people WHERE id_p='$id_p' ";
		$db->query_single( $row, $sqlstr);
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		return $sc->decrypt($row['password']);
	}
	
	private function PeopleTables()
	{
		$tables = array();
		$tables[] = "ass";
		$tables[] = "comments";
		$tables[] = "events";
		$tables[] = "form_posts";
		$tables[] = "mailjob_recipients";
		$tables[] = "meeting_participants";
		$tables[] = "mps_queue";
		$tables[] = "orders";
		$tables[] = "payers";
		$tables[] = "people_groups";
		$tables[] = "people_login_log";
		$tables[] = "people_topics";
		$tables[] = "people_unique_sessions";
		$tables[] = "people_verify";
		$tables[] = "poll_people_votes";
		$tables[] = "poll_questions";
		$tables[] = "poll_tokens";
		$tables[] = "reviews";
		$tables[] = "topic_campaigns";
		$tables[] = "topic_campaigns_orgs";
		$tables[] = "topic_campaigns_persons";
		$tables[] = "topic_campaigns_vips";
		$tables[] = "topic_forum_threads";
		$tables[] = "users";
		$tables[] = "visits";
		$tables[] = "widget_abuses";
		$tables[] = "widget_sharings";
		$tables[] = "widgets";
		$tables[] = "widgets_tabs";
		$tables[] = "widgets_tabs_position";
		return $tables;		
	}
	
	public function PortalAssociate($id_p)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET contact=1 WHERE id_p='$id_p' ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}
	
	private function RandomPassword($length=8)
	{
  		$password = "";
		$possible = "123456789abcdefghijkmnpqrstuvwxyzABCDEFGHKLMNPQRSTUVWXYZ"; 
		$i = 0; 
		while ($i < $length)
		{ 
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($password, $char))
			{
				$password .= $char;
				$i++;
			}
		}
		return $password;
	}
	
	public function Register($name,$surname,$email,$password,$auth,$id_geo,$id_language,$unescaped_params,$set_session=true,$email_valid=1,$verified=false)
	{
		$email = trim($email);
		$verified = $verified? (self::PeopleEmailVerificationCompleted) : (self::PeopleEmailVerificationMissing);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$cookie = $this->SetCookie($email,$set_session);
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		$cpassword = $sc->encrypt($password);
		$db =& Db::globaldb();
		$db->begin();
		$today = $db->getTodayTime();
		$db->lock( "people" );
		$id_p = $db->nextId( "people", "id_p" );
		$sqlstr =  "INSERT INTO people (id_p,name1,name2,password,email,identifier,token,active,start_date,last_conn,timeout,id_geo,email_valid,id_language,ip,verified)
		VALUES ($id_p,'$name','$surname','$cpassword','$email','{$cookie['identifier']}','{$cookie['token']}',1,'$today','$today','{$cookie['timeout']}','$id_geo',$email_valid,'$id_language','$ip','$verified')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$db->lock( "people_params" );
		$sqlstr =  "INSERT INTO people_params (id_p,params) VALUES ($id_p,'" . $v->Serialize($unescaped_params) . "')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		// Custom modules actions
		include_once(SERVER_ROOT."/../classes/modules.php");
		if(Modules::IsActiveByPath("widgets") && !($this->verification_required && !$verified))
		{
			// clone the homepage configuration
			include_once(SERVER_ROOT."/../classes/widgets.php");
			$wi = new Widgets();
			$wi->CloneConfiguration(0,$id_p);
		}
		// set sesstion
		if($set_session)
		{
			$row = $this->UserGetByIdentifier($cookie['identifier']);
			$user = $this->Row2User($row);
			if($this->verification_required && !$verified)
			{
				$auth = false;
			}
			$this->SetUserSession($auth,$user);
		}
		return $id_p;
	}
	
	public function Reminder($email,$id_topic)
	{
		$sent = false;
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_p,name1,name2,password,id_language FROM people WHERE email='$email' ";
		$db->query_single( $row, $sqlstr);
		if($row['id_p']>0)
		{
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			$from_email = $mail->staff_email;
			if($id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$id_style = $t->id_style;
				$id_language = $t->id_language;
				$from_name = $t->name;
				if($t->row['temail']!="" && $mail->CheckEmail($t->row['temail']))
					$from_email = $t->row['temail'];
			}
			else 
			{
				$id_language = $row['id_language'];
				$id_style = 0;
				$from_name = $mail->title;
			}
			include_once(SERVER_ROOT."/../classes/translator.php");
			include_once(SERVER_ROOT."/../classes/ini.php");
			include_once(SERVER_ROOT."/../classes/crypt.php");
			$ini = new Ini();
			$sc = new Scrypt;
			$dpass = $sc->decrypt($row['password']);
			$tr0 = new Translator($id_language,0,false,$id_style);
			$url = $mail->pub_web . "/" . $ini->Get("users_path") . "/login.php";
			if($id_topic>0)
				$url .= "?id_topic=$id_topic";
			$extra = array();
			$extra['name'] = $from_name;
			$extra['email'] = $from_email;
			$subject = $tr0->TranslateParams("password_reminder",array($from_name));
			$message = $tr0->TranslateParams("password_reminder_msg",array("{$row['name1']} {$row['name2']}",$from_name,$url,$email,$dpass));
			$mail->SendMail($email, "{$row['name1']} {$row['name2']}", $subject, $message, $extra);
			$sent = true;
		}
		return $sent;
	}
	
	public function RevalidateEmail($id_p,$email_valid)
	{
		$user = $this->UserGetById($id_p);
		if($user['email_valid']!=$email_valid)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "people" );
			$sqlstr = "UPDATE people SET email_valid='$email_valid' WHERE id_p='$id_p' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	public function Row2User($row)
	{
		$user = array();
		$user['id'] = $row['id_p'];
		$user['name'] = $row['name1'] . " " . $row['name2'];
		$user['email'] = $row['email'];
		if($row['verified']==self::PeopleEmailVerificationMissing )
		{
			if($this->VerifyPending($row['id_p']))
			{
				$row['verified'] = self::PeopleEmailVerificationSent ;
			}
		}
		$user['verified'] = $row['verified'];
		$user['conn'] = (int)$row['connections'];
		$user['id_language'] = $row['id_language'];
		$user['name1'] = $row['name1'];
		$user['name2'] = $row['name2'];
		return $user;
	}
	
	public function Search(&$rows, $params, $paged=true)
	{
		include_once(SERVER_ROOT."/../classes/modules.php");

        include_once(SERVER_ROOT."/../classes/geo.php");
        $geo = new Geo();
        $gjoin = $geo->GeoJoin("p.id_geo");

        $db =& Db::globaldb();
        $rows = array();

        $sqlstr = "SELECT p.id_p,p.name1,p.name2,p.name3,IF(p.email_valid,p.email,'') AS email,CONCAT(p.name1,' ',p.name2) AS full_name,p.verified,p.town,p.email AS email_address,p.bounces,
            UNIX_TIMESTAMP(p.start_date) AS start_date_ts,$gjoin[name] AS geo_name,";
        $sqlstr .= ($params['id_topic']>0)? "pt.contact":"p.contact";
        if ($params['id_topic'] > 0)
            $sqlstr .= ",pt.access ";
        $sqlstr .= " FROM people p $gjoin[join] ";
        if ($params['id_topic'] > 0)
        {
            $sqlstr .= " INNER JOIN people_topics pt ON p.id_p=pt.id_p ";
            if ($params['id_pt_group']>0)
            {
                $sqlstr .= " INNER JOIN people_groups pg ON pt.id_p=pg.id_p AND pg.id_pt_group='{$params['id_pt_group']}' ";
            }
        }
        elseif ($params['id_pt_group']>0)
        {
            $sqlstr .= " INNER JOIN people_groups pg ON p.id_p=pg.id_p AND pg.id_pt_group='{$params['id_pt_group']}' ";
        }
        $sqlstr .= " WHERE 1=1 ";
        if (strlen($params['name']) > 0)
            $sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%' OR p.name3 LIKE '%{$params['name']}%') ";
        if (strlen($params['email']) > 0)
            $sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
        if ($params['id_geo'] > 0)
            $sqlstr .= " AND p.id_geo={$params['id_geo']}";
        if (strlen($params['start_date']) > 0)
        {
            $start_date = date('Y-m-d', strtotime($params['start_date']));
            $sqlstr .= " AND p.start_date >= '{$start_date}' ";
        }
        if (strlen($params['end_date']) > 0)
        {
            $end_date = date('Y-m-d', strtotime("+1day",strtotime($params['end_date'])));
            $sqlstr .= " AND p.start_date < '{$end_date}' ";
        }
        if ($params['id_topic'] > 0)
        {
            $sqlstr .= " AND pt.id_topic={$params['id_topic']}";
            if ($params['contact']>0)
            {
                $sqlstr .= " AND pt.contact=" . ($params['contact']==1? 1:0);
            }
            if ($params['access']>0)
            {
                $sqlstr .= " AND pt.access=" . ($params['access']==1? 1:0);
            }
        }
        else 
        {
            if ($params['contact']>0)
            {
                $sqlstr .= " AND p.contact=" . ($params['contact']==1? 1:0);
            }
        }
        if ($params['verified']>0)
        {
            $sqlstr .= " AND p.verified=" . ($params['verified']==1? 1:0);
        }
        if ($params['active']=="on")
            $sqlstr .= " AND p.active=1 ";
        if ($params['id_from']>0)
            $sqlstr .= " AND p.id_p<>{$params['id_from']} ";
        
        $sqlstr .= " GROUP BY p.id_p"; 
        
        if ($params['sort_by']=="date") {
            $sqlstr .= " ORDER BY p.start_date DESC";
        } else {
            $sqlstr .= " ORDER BY p.name2,p.name1";
        }

		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function SearchBounces($email,$bounces)
	{
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "SELECT id_p,name1,name2,email,bounces FROM people 
        		WHERE bounces >= $bounces AND email LIKE '%{$email}%' ORDER BY id_p ";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}

	public function SearchContact($contact)
	{
        $db =& Db::globaldb();
        $rows = array();
        $sqlstr = "SELECT id_p,name1,name2,email FROM people WHERE contact=$contact ORDER BY id_p ";
		$db->QueryExe($rows, $sqlstr, false);
		return $rows;
	}

	public function SendLoginFailAttemptEmail($email,$counter,$id_topic)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		$extra = array();
		if($id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$id_style = $t->id_style;
			$extra['name'] = $t->name;
			if($t->row['temail']!="" && $mail->CheckEmail($t->row['temail']))
				$extra['email'] = $t->row['temail'];
		}
		else 
		{
			$id_style = 0;
		}
		include_once(SERVER_ROOT."/../classes/translator.php");
		$trm28 = new Translator(0,28,false,$id_style);
		$subject = $trm28->Translate("login_attempts");
		$site_name = $mail->title;
		$site_url = $mail->pub_web;
		if($id_topic>0 && $t->profiling)
		{
			$site_name = $t->name;
			$site_url = $t->url;
		}
		$message = $trm28->TranslateParams("login_attempts_warning",array($email,$counter,$site_name,$site_url));
		$mail->SendMail($email,"",$subject,$message,$extra);
	}

	private function SetCookie($email,$write=true)
	{
		$identifier = $this->UserIdentifier($email);
		$token = Varia::Uid();
		$timeout = $this->WriteCookie($identifier,$token,true,$write);
		return array('identifier'=>$identifier,'token'=>$token,'timeout'=>$timeout);
	}
	
	private function SetUser($auth,$reset_cookie)
	{
		$row = $this->UserGetByCookie($reset_cookie);
		if ($row['check'])
		{
			$this->SetUserSession($auth,$row);
		}
		return $row;
	}
	
	public function SetUserCookieAndSession($id_p,$auth=false)
	{
		$row = $this->UserGetById($id_p);
		$this->SetCookie($row['email']);
		$user = $this->Row2User($row);
		$this->SetUserSession($auth,$user);
	}

	public function SetUserSession($auth,$user)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		unset($user['check']);
		$user['auth'] = $auth;
        
        include_once(SERVER_ROOT."/../classes/crypt.php");        
        $sc = new Scrypt();
        $user_session_id = session_id();
        $user['user_session_id'] = $sc->Encrypt($user_session_id);
        
		$session->Set("user",$user);
	}
	
	public function Logout()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Delete("user");
		$session->Cleanup(COOKIE_NAME);
	}
	
	public function TopicAssociate($id_p,$id_topic,$contact,$id_pt_group=0,$notify_topic_owner=true)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_p FROM people_topics WHERE id_p='$id_p' AND id_topic='$id_topic' ";
		$db->query_single( $row, $sqlstr);
		if(!$row['id_p']>0)
		{
			$db->begin();
			$db->lock( "people_topics" );
			$sqlstr =  "INSERT INTO people_topics (id_p,id_topic,contact) VALUES ('$id_p','$id_topic','$contact') ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			
			if($notify_topic_owner)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				if($t->protected=="2")
				{
					include_once(SERVER_ROOT."/../classes/mail.php");
					$mail = new Mail();
					$subject = "{$t->name} - New User";
					$msg .= "New user registration:\n";
					$user = $this->UserGetById($id_p);
					$msg .= "Name: {$user['name1']} {$user['name2']}\n";
					$msg .= "Email: {$user['email']}\n";
					$msg .= "{$mail->admin_web}/topics/visitor.php?id=$id_topic&id_p=$id_p\n";
					$extra = array();
					$contacts = $t->Contacts();
					foreach($contacts as $contact)
					{
						$mail->SendMail($contact['email'],$contact['name'],$subject,$msg,$extra);
					}
				}
			}
		}
		else 
		{
			$db->begin();
			$db->lock( "people_topics" );
			$res[] = $db->query( "UPDATE people_topics SET contact='$contact' WHERE id_p='$id_p' AND id_topic='$id_topic'  " );
			Db::finish( $res, $db);
		}
		if($id_pt_group>0)
		{
			$this->TopicGroupAssociate($id_p,$id_pt_group);
		}
	}
	
	public function TopicGroupAssociate($id_p,$id_pt_group)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_groups" );
		$res[] = $db->query( "REPLACE INTO people_groups (id_p,id_pt_group) VALUES ($id_p,$id_pt_group) " );
		Db::finish( $res, $db);
	}
	
	private function UpdateCookie($identifier,$token,$connections,$update_connections=false,$set_timeout=true)
	{
		$timeout = $this->WriteCookie($identifier,$token,$set_timeout);
        $row = $this->UserGetByIdentifier($identifier);
		$db =& Db::globaldb();
		$today = $db->getTodayTime();
		$db->begin();
		$db->lock("people");
		$sqlstr = "UPDATE people SET timeout='$timeout' ";
		if($update_connections)
        {
            $sqlstr .= ",last_conn='$today',connections=" . ((int)$connections+1);
        }
		$sqlstr .= "  WHERE identifier='$identifier'";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);

		if ($update_connections && $this->log_login)
		{
			$db->begin();
			$db->lock("people_login_log");
			$id_log = $db->nextId( "people_login_log", "id_log" );
			$res[] = $db->query("INSERT INTO people_login_log (id_log, id_p, login_on) VALUES ('$id_log', '{$row['id_p']}', NOW())");
			Db::finish( $res, $db);
        }
	}
	
	public function UserCreate($name,$surname,$email,$id_geo,$contact_portal,$unescaped_params=array(),$verify_email=false,$id_topic=0,$contact_topic=0,$set_session=true,$email_valid=1,$verified=false)
	{
		$row = $this->UserGetByEmail($email);
		if($row['id_p']>0)
		{
			$this->UserParamsUpdate($row['id_p'],$unescaped_params);
			$id_p = $row['id_p'];
			if($set_session)
			{
				$this->SetCookie($email);
				$this->SetUser(false,false);
			}
		}
		else
		{
			if($name=="")
				$name = $email;
			$password = $this->RandomPassword();
			$id_language = $this->IdLanguage($id_topic);
			$id_p = $this->Register($name,$surname,$email,$password,false,$id_geo,$id_language,$unescaped_params,$set_session,$email_valid,$verified);
			if($verify_email && $id_p>0)
				$this->VerifyEmail($id_p,$id_topic,true);
		}
		if($id_topic>0)
			$this->TopicAssociate($id_p,$id_topic,$contact_topic);
		if($contact_portal)
			$this->PortalAssociate($id_p);
		return $id_p;
	}
	
	public function UserGetByEmail($email,$is_escaped=true,$password='')
	{
		$email = trim($email);
		$row = array();
		$db =& Db::globaldb();
		if(!$is_escaped)
			$email = $db->SqlQuote($email);

		$sqlstr = "SELECT id_p,name1,name2,email,identifier,token,timeout,id_language,verified,connections,active FROM people WHERE email='$email' ";

        if ($password != '')
        {
            include_once(SERVER_ROOT."/../classes/crypt.php");
            $sc = new Scrypt;
            $cpassword = $sc->encrypt($password);
            $sqlstr .= "AND password = '$cpassword'";
        }

		$db->query_single( $row, $sqlstr);
		return $row;
	}
	
	public function UserGetById($id_p,$active_only=true)
	{
	    $row = array();
	    $db =& Db::globaldb();
	    $sqlstr = "SELECT p.id_p,p.name1,p.name2,p.name3,p.email,p.token,p.timeout,p.id_language,p.verified,p.connections,p.email_valid,p.identifier
			FROM people p
			WHERE p.id_p='$id_p'";
	    if($active_only)
	        $sqlstr .= " AND p.active=1";
	        $db->query_single( $row, $sqlstr);
	        return $row;
	}
	
	public function UserGetByIdentifier($identifier)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.id_p,p.name1,p.name2,p.email,p.token,p.timeout,p.id_language,p.verified,p.connections,p.email_valid
                    FROM people p
                    WHERE p.identifier='" . $db->SqlQuote($identifier). "' ";
		$db->query_single( $row, $sqlstr);
		return $row;
	}
	
	private function UserGetByCookie($reset_cookie=false)
	{
		$user = array('id'=>"0");
		$user['check'] = false;
		list($identifier,$token,$build) = explode(':', $_COOKIE[COOKIE_NAME]);
		if(ctype_alnum($identifier) && ctype_alnum($token))
		{
			$row = $this->UserGetByIdentifier($identifier);
			$now = time();
			$timeout = (int)$row['timeout'];
			if($row['id_p']>0 && $token==$row['token'] && ($timeout==0 || ($timeout>0 && $now<$timeout)))
			{
				if($identifier==($this->UserIdentifier($row['email'])))
				{
					$user = $this->Row2User($row);
					$user = $this->UserTopicsGroups($row['id_p'],$user);
					$user['check'] = true;
					if($reset_cookie)
						$this->UpdateCookie($identifier,$token,$row['connections']);
				}
			}
		}
		return $user;
	}
	
	public function UserGetByName($name1,$name2)
	{
		$row = array();
		if($name1!="" && $name2!="")
		{
			$db =& Db::globaldb();
			$sqlstr = "SELECT p.*,UNIX_TIMESTAMP(start_date) AS start_date_ts,
				UNIX_TIMESTAMP(last_conn) AS last_conn_ts,pp.params 
				 FROM people p 
				 INNER JOIN people_params pp ON p.id_p=pp.id_p
				 WHERE p.name1='{$name1}' AND p.name2='{$name2}' ";
			$db->query_single( $row, $sqlstr);
		}
		return $row;
	}
	
	public function UserGetDetailsById($id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.*,UNIX_TIMESTAMP(start_date) AS start_date_ts,
			UNIX_TIMESTAMP(last_conn) AS last_conn_ts,pp.params 
			 FROM people p 
			 INNER JOIN people_params pp ON p.id_p=pp.id_p
			 WHERE p.id_p='$id_p' ";
		$db->query_single( $row, $sqlstr);
		return $row;
	}
	
	private function UserGroups($id_p)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT pg.id_pt_group,ptg.name 
			FROM people_groups pg
			INNER JOIN people_topics_groups ptg ON pg.id_pt_group=ptg.id_pt_group
			WHERE pg.id_p='$id_p'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UserIdentifier($email)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		return $v->Hash($email);
	}
	
	public function UserInfo($reset_cookie)
	{
		$user = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		if ($session->IsVarSet("user"))
		{
			$user = $session->Get("user");
		}
		else
		{
			$user = $this->SetUser(false,$reset_cookie);
		}
		return $user;
	}
	
	public function UserParamsGet($id_p,$deserialize=false)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_p,params FROM people_params WHERE id_p='$id_p' ";
		$db->query_single( $row, $sqlstr);
		if($deserialize)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$oparams = $v->Deserialize($row['params']);
			$row = $oparams;
		}
		return $row;
	}

	public function UserParamsUpdate($id_p,$params)
	{
		$row = $this->UserParamsGet($id_p);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$nparams = array();
		$v = new Varia();
		$oparams = $v->Deserialize($row['params']);
		if(is_array($oparams))
		{
			foreach($oparams as $okey=>$ovalue)
			{
				if(array_key_exists($okey,$params))
					$nparams[$okey] = $params[$okey];
				else
					$nparams[$okey] = $ovalue;
			}
		}
		foreach($params as $key=>$value)
		{
			if(!array_key_exists($key,$nparams))
				$nparams[$key] = $value;
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_params" );
		$sparams = $v->Serialize($nparams);
		$sqlstr = "UPDATE people_params SET params='$sparams' WHERE id_p='$id_p' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function UserTicketStore($id_p,$id_ticket,$ticket_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_tickets" );
		$sqlstr = "INSERT INTO people_tickets (id_p,id_ticket,ticket_type,inserted_on) VALUES ($id_p,'$id_ticket','$ticket_type',NOW())";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}

	public function UserTopicSet($id_topic,$topic_user,$topic_auth)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$user = $session->Get("user");
		$user['id_topic'] = $id_topic;
		$user['topic_user'] = $topic_user;
		$user['topic_auth'] = $topic_auth;
		$session->Set("user",$user);
	}
	
	public function UserTopics($id_p)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT pt.id_topic,t.name 
			FROM people_topics pt 
			INNER JOIN topics t ON pt.id_topic=t.id_topic
			WHERE pt.id_p='$id_p' AND pt.access=1";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UserTopicsAll( $id_p )
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$temp_id_topic = $ini->Get('temp_id_topic');
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT t.id_topic,t.name,pt.id_p,pt.contact,tg.name AS group_name 
		FROM topics t 
		INNER JOIN topics_groups tg ON t.id_group=tg.id_group
		LEFT JOIN people_topics pt ON t.id_topic=pt.id_topic AND pt.id_p=$id_p
		WHERE t.id_topic<>$temp_id_topic AND t.profiling=1
		ORDER BY tg.id_group,t.seq,t.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function UserUpdate($id_p,$salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone)
	{
		$sqlstr = "UPDATE people SET name1='$name1',name2='$name2',name3='$name3',address='$address',
			address_notes='$address_notes',postcode='$postcode',town='$town',id_geo='$id_geo',phone='$phone',
			salutation='$salutation'";
		$email = trim($email);
		$row = $this->UserGetById($id_p);
		$user2 = $this->UserGetByEmail($email);
		$db =& Db::globaldb();
		if($email!=$db->SqlQuote($row['email']) && !$user2['id_p']>0)
		{
			$identifier = $this->UserIdentifier($email);
			$sqlstr .= ",email='$email',identifier='$identifier'";
		}
		$sqlstr .= " WHERE id_p='$id_p' ";
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function UserUpdateAdmin($id_p,$id_language,$admin_notes,$verified,$active,$contact,$email_valid)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET id_language='$id_language',admin_notes='$admin_notes',verified='$verified',active='$active',contact='$contact',email_valid='$email_valid'
		 WHERE id_p='$id_p' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function UserUpdateDetails($id_p,$address,$phone)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET address='$address',phone='$phone'
		 	WHERE id_p='$id_p' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function UserUpdateDetails2($id_p,$address,$address_notes,$postcode,$town,$id_geo,$phone)
	{
		$sqlstr = "UPDATE people SET address='$address',
			address_notes='$address_notes',postcode='$postcode',town='$town',id_geo='$id_geo',phone='$phone'
			WHERE id_p='$id_p' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function UserUpdateEmail($id_p,$email)
	{
		$db =& Db::globaldb();
		$email = trim($email);
		$user = $this->UserGetByEmail($email);
		$row = $this->UserGetById($id_p);
		if($email!=$db->SqlQuote($row['email']) && !$user['id_p']>0)
		{
			$identifier = $this->UserIdentifier($email);
			$sqlstr = "UPDATE people SET email='$email',identifier='$identifier' WHERE id_p='$id_p' ";
			$db->begin();
			$db->lock( "people" );
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			return true;
		}
		else 
		{
			return false;
		}
	}

	public function UserUpdateName($id_p,$name1,$name2)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query( "UPDATE people SET name1='$name1',name2='$name2' WHERE id_p='$id_p' " );
		Db::finish( $res, $db);
	}
    
	public function UserUpdateNameEmail($id_p,$name1,$name2,$email)
	{
		$sqlstr = "UPDATE people SET name1='$name1',name2='$name2' ";
		$email = trim($email);
		$user2 = $this->UserGetByEmail($email);
		$row = $this->UserGetById($id_p);
		$db =& Db::globaldb();
		if($email!=$db->SqlQuote($row['email']) && !$user2['id_p']>0)
		{
			$identifier = $this->UserIdentifier($email);
			$sqlstr .= ",email='$email',identifier='$identifier'";
		}
		$sqlstr .= " WHERE id_p='$id_p' ";
		$db->begin();
		$db->lock( "people" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
    
    public function PreEmailValidation($id_p, $email)
    {
        $valid = true;
        $row_old = $this->UserGetById($id_p);
        $db =& Db::globaldb();  
        
        if($db->SqlQuote($row_old['email'])!=$email)    
        {
            $row = $this->UserGetByEmail($email, false);
            $count = count($row);
            
            if($count > 0)
            {
                $valid = false;
            }
        }
        return $valid;
    }
    
	public function UniqueSessionsDeleteOld()
	{
		$sqlstr = "DELETE FROM people_unique_sessions WHERE DATE_ADD(last_update, INTERVAL " . UNIQUE_SESSIONS_DAYS . " DAY)<CURRENT_TIMESTAMP";
        $db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_unique_sessions" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function UniqueSessionIdentifier()
    {
		include_once(SERVER_ROOT."/../classes/varia.php");
		$va = new Varia();
		$ua = $va->UserAgent();
		return md5($ua);
    }

	private function UniqueSessionStore($id_p)
	{
		$db =& Db::globaldb();
		$ip_ua_hash = $db->SqlQuote($this->UniqueSessionIdentifier());
		$today = $db->getTodayTime();
		$db->begin();
		$db->lock( "people_unique_sessions" );
		$sqlstr = "REPLACE INTO people_unique_sessions (id_p,ip_ua,last_update)
			VALUES ($id_p,'$ip_ua_hash','$today')  ";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
	}
	
	public function UserUpdatePub($id_p,$name1,$name2,$email,$phone,$address,$address_notes,$postcode,$town,$id_geo)
	{
		$email_changed = false;
		$row_old = $this->UserGetById($id_p);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET name1='$name1',name2='$name2',phone='$phone',address='$address',address_notes='$address_notes',
			postcode='$postcode',town='$town',id_geo='$id_geo'
		 WHERE id_p='$id_p' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($db->SqlQuote($row_old['email'])!=$email)
		{
			$cookie = $this->SetCookie($email);
			$db->begin();
			$db->lock( "people" );
			$sqlstr = "UPDATE people SET email='$email',identifier='{$cookie['identifier']}',token='{$cookie['token']}',timeout='{$cookie['timeout']}' WHERE id_p='$id_p' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$email_changed = true;
		}
		$row = $this->UserGetById($id_p);
		$user = $this->Row2User($row);
		$user = $this->UserTopicsGroups($id_p,$user);
		$this->SetUserSession(true,$user);
		return $email_changed;
	}

	public function VerifyEmail($id_p,$id_topic=0,$random_password=false,$send_email=true,$redirect='')
	{
		$return_status = 1;
		if(!$id_p>0)
		{
			$user = $this->UserInfo(false);
			$id_p = $user['id'];
		}
		if($id_p>0)
		{
			$return_status = 2;
			$row = $this->UserGetById($id_p);
			if($row['email_valid'])
			{
				$row2 = array();
				$db =& Db::globaldb();
				$sqlstr = "SELECT UNIX_TIMESTAMP(token_date) AS tts,token FROM people_verify WHERE id_p='$id_p' AND id_action='{$this->token_actions['verify_email']}' ";
				$db->query_single( $row2, $sqlstr);
				$db->begin();
				$today = $db->getTodayTime();
				$db->LockTables( array("people_verify", "people") );
				if($row2['tts']>0)
				{
					$token = $row2['token'];
					$sqlstr =  "UPDATE people_verify SET token_date='$today' WHERE id_p='$id_p' AND id_action='{$this->token_actions['verify_email']}' ";
				}
				else
				{
					$token = $row['identifier'];
					$sqlstr = "INSERT INTO people_verify (id_p,token,token_date,id_action,redirect) VALUES ($id_p,'" . $db->SqlQuote($token) . "','$today','{$this->token_actions['verify_email']}','$redirect') ";
				}
				$res[] = $db->query( $sqlstr );
                $sqlstr =  "UPDATE people SET verified = 0 WHERE id_p='$id_p' ";
                $res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
				if($send_email)
				{
					include_once(SERVER_ROOT."/../classes/mail.php");
					$mail = new Mail();
					$extra = array();
					if($id_topic>0)
					{
						include_once(SERVER_ROOT."/../classes/topic.php");
						$t = new Topic($id_topic);
						$id_style = $t->id_style;
						$extra['name'] = $t->name;
						if($t->row['temail']!="" && $mail->CheckEmail($t->row['temail']))
							$extra['email'] = $t->row['temail'];
					}
					else 
					{
						$id_style = 0;
					}
					include_once(SERVER_ROOT."/../classes/translator.php");
					$trm28 = new Translator($row['id_language'],28,false,$id_style);
					$subject = $trm28->Translate("email_check_subj");
					$site_name = $mail->title;
					$site_url = $mail->pub_web;
					if($id_topic>0 && $t->profiling)
					{
						$site_name = $t->name;
						$site_url = $t->url;
					}
					$link = $this->VerifyEmailLink($token,$id_topic);
					$message = $trm28->TranslateParams(($random_password?"email_check_body2":"email_check_body"),array("{$row['name1']} {$row['name2']}",$row['email'],$site_name,$mail->staff_email,$site_url,$link));
					$mail->SendMail($row['email'],"{$row['name1']} {$row['name2']}",$subject,$message,$extra);
				}
				$return_status = 0;
				$user = $this->UserInfo(true);
				$user['verified'] = self::PeopleEmailVerificationSent;
				$this->SetUserSession($user['auth'],$user);
			}
		}
		return $return_status;
	}
	
	private function VerifyEmailLink($token,$id_topic)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$url = $ini->Get("pub_web") . "/" . $ini->Get('users_path') . "/check.php?token=$token";
		if($id_topic>0)
			$url .= "&id_topic=$id_topic";
		return $url;
	}
	
	public function VerifyToken($unescaped_token,$id_topic)
	{
		$row = array();
		$db = Db::globaldb();
		$token = $db->SqlQuote($unescaped_token);
		$sqlstr = "SELECT id_p,UNIX_TIMESTAMP(token_date) AS token_ts,redirect FROM people_verify WHERE token='$token'  AND id_action='{$this->token_actions['verify_email']}' ";
		$db->query_single( $row, $sqlstr);
		if($row['id_p']>0)
		{
			if((time() - $row['token_ts'])  > $this->verify_token_ttl)
				$ret = 1;
			else
			{
				$ret = 0;
				$this->Verified($row['id_p']);
				if($row['redirect']!='')
					$this->SetUserCookieAndSession($row['id_p'],true);
				include_once(SERVER_ROOT."/../classes/modules.php");
				if($this->verification_required && Modules::IsActiveByPath("widgets"))
				{
           			// clone the homepage configuration
					include_once(SERVER_ROOT."/../classes/widgets.php");
					$wi = new Widgets();
					$wi->CloneConfiguration(0,$row['id_p']);
				}
			}
		}
		else
		{
			$ret = 2;
			$user_row = $this->UserGetByIdentifier($token);
			if(isset($user_row['id_p']) && $user_row['id_p']>0)
			{
				if($user_row['verified']=="1")
				{
					$ret = 3;
				}
				else 
				{
					$this->VerifyEmail($user_row['id_p'],$id_topic);
					$ret = 4;
				}
			}
			else 
			{
				include_once(SERVER_ROOT."/../classes/session.php");
				$session = new Session();
				if($session->IsVarSet("user"))
				{
					$user = $session->Get("user");
					$id_p = (int)$user['id'];
					if($id_p>0)
					{
						$user_row = $this->UserGetById($id_p);
						if($user_row['verified']=="1")
						{
							$ret = 3;
						}
						else 
						{
							$this->VerifyEmail($id_p,$id_topic);
							$ret = 4;
						}
					}
				}
				
			}
		}
		return array('status'=>$ret,'redirect'=>$row['redirect']);
	}
	
	public function VerifyPending($id_p)
	{
		$row = array();
		$pending = false;
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(token_date) AS token_ts FROM people_verify WHERE id_p='$id_p'  AND id_action='{$this->token_actions['verify_email']}' ";
		$db->query_single( $row, $sqlstr);
		if($row['token_ts']>0)
			$pending = true;
		return $pending;
	}
	
	public function VerifyPendingClean()
	{
		$sqlstr = "DELETE FROM people_verify WHERE DATE_ADD(token_date, INTERVAL {$this->verify_token_ttl} SECOND)<CURRENT_TIMESTAMP";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_verify" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
    
    public function LoginLogClean()
    {
        $sqlstr = "DELETE FROM people_login_log WHERE DATE_ADD(login_on, INTERVAL 1 MONTH) < NOW()";
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "people_login_log" );
        $res[] = $db->query( $sqlstr );
        Db::finish( $res, $db);
    }
    
    public function RemoveUnverifiedUsers($ttl,$limit=500)
    {
        $rows = array();
        $db =& Db::globaldb();
        $sqlstr = "SELECT id_p, name1, name2, email, start_date 
                    FROM people 
                    WHERE verified = 0 AND DATE_ADD(start_date, INTERVAL $ttl DAY) < NOW()
                    LIMIT $limit ";
        $db->QueryExe($rows, $sqlstr);
        
        $removedUsers = array();
        foreach($rows as $row)
        {
            $this->Delete($row['id_p']);
        }
        $db->begin();
        $db->lock( "people_removed_log" );
        foreach($rows as $row)
        {
            $id_removed_log = $db->nextId( "people_removed_log", "id_removed_log" );
            $name1 = $db->SqlQuote($row['name1']);
            $name2 = $db->SqlQuote($row['name2']);
            $email = $db->SqlQuote($row['email']);
            $start_date = $db->SqlQuote($row['start_date']);
            $sqlstr = "INSERT INTO people_removed_log (id_removed_log, id_p, name1, name2, email, start_date, removed_on) 
            	VALUES ('$id_removed_log', '{$row['id_p']}', '$name1', '$name2', '$email', '$start_date', NOW()) ";
            $res[] = $db->query( $sqlstr );
        }
        Db::finish( $res, $db);
    }
    
    public function RemovedUsersLogClean()
    {
        $sqlstr = "DELETE FROM people_removed_log WHERE DATE_ADD(removed_on, INTERVAL 1 MONTH) < NOW()";
        $db =& Db::globaldb();
        $db->begin();
        $db->lock( "people_removed_log" );
        $res[] = $db->query( $sqlstr );
        Db::finish( $res, $db);
    }
	
	private function Verified($id_p)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people" );
		$sqlstr = "UPDATE people SET verified=1,bounces=0 WHERE id_p='$id_p' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "people_verify" );
		$sqlstr = "DELETE FROM people_verify WHERE id_p='$id_p' AND id_action='{$this->token_actions['verify_email']}' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$user = $this->UserInfo(true);
		$user['verified'] = self::PeopleEmailVerificationCompleted ;
		$this->SetUserSession($user['auth'],$user);
	}
	
	private function WriteCookie($identifier,$token,$set_timeout=true,$write=true)
	{
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$timeout = $set_timeout? time() + 3600 * 24 * $conf->Get("visitors_life") : 0;
		if($write)
		{
			$domain = "";
			if(isset($_SERVER['PHPEACE_SESSION_DOMAIN']))
			{
				$domain = $_SERVER['PHPEACE_SESSION_DOMAIN'];
			}
			include_once(SERVER_ROOT."/../classes/phpeace.php");
			setcookie(COOKIE_NAME,"$identifier:$token:" . PHPEACE_BUILD,$timeout,'/',$domain!=""?$domain:"");
		}
		return $timeout;
	}
	
}

/**
 * Web services for portal users
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class PeopleWS
{
	/**
	 * Installation key (used for authorizing requests)
	 *
	 * @var string
	 */
	private $install_key;
	
	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$this->install_key = $ini->Get("install_key");   
	}
	
	/**
	 * Check if current session user is valid portal account and is logged in
	 *
	 * @param string $encrypted_session_id	Current user session ID 
	 * @param string $key 					Installation key for authorization
	 * @return string						Numeric string of user ID
	 * 
	 */
	public function UserValidAndLoggedIn($encrypted_session_id,$key)
	{
        $id_p = '0';
		if($key!="" && $key==$this->install_key)
		{
            
            include_once(SERVER_ROOT."/../classes/crypt.php");
            $sc = new Scrypt();
            $session_id = $sc->Decrypt($encrypted_session_id); 
            if(!empty($session_id) && preg_match('/^[a-zA-Z0-9]*$/', $session_id))
			{
				session_id($session_id);
				include_once(SERVER_ROOT."/../classes/session.php");
				$session = new Session();
				$session_data = $session->GetDataBySessionId($session_id);
                if(is_array($session_data) && count($session_data)>0 && isset($session_data['user']['id']) && intval($session_data['user']['id'])>0)
                {
					$id_p = $session_data['user']['id'];
				}
			}
		}
		return $id_p;
	}
}

?>
