<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/irl.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/config.php");

class PublishManager
{
	
	/** 
	 * @var Layout */
	public $layout;
	/** 
	 * @var FileManager */
	public $fm;
	/** 
	 * @var Translator */
	public $tr;

	public $scheduler;
	public $force;
	public $step;
	public $steps;

	/** 
	 * @var IRL */
	private $irl;
	/** 
	 * @var Ini */
	private $ini;
	/** 
	 * @var Session */
	private $session;
	/** 
	 * @var XmlHelper */
	private $xh;

	private $done_map;
	private $landing;
	private $step_articles;
	private $time_exec;
	private $mod_rewrite;
	private $topic_publish_timeout;
	private $isCDN;
		
	function __construct()
	{
		$this->irl = new IRL();
		$this->layout = new Layout(false,false,false);
		$this->session = new Session();
		$this->ini = new Ini;
		$this->tr = new Translator($this->session->Get('id_language'),0);
		$this->fm = new FileManager;
		$this->done_map = false;
		$this->scheduler = false;
		$this->force = false;
		$this->step = 0;
		$this->steps = 1;
		$conf = new Configuration();
		$this->landing = $conf->Get("landing");
		$this->step_articles = $conf->Get("step_articles");
		$this->mod_rewrite = $conf->Get("mod_rewrite");
		$this->topic_publish_timeout = $conf->Get("topic_publish_timeout");
		$cdn = $conf->Get("cdn");
		$this->isCDN = $cdn!='';
	}
	
	function __destruct()
	{
		if(isset($this->irl))
			$this->irl->__destruct();
		unset($this->irl);
		unset($this->ini);
		if(isset($this->tr))
			$this->tr->__destruct();
		unset($this->tr);
		unset($this->fm);
		if(isset($this->layout))
			$this->layout->__destruct();
		unset($this->layout);
	}
	
	public function Article($id_article,$is_new=false)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$this->layout->article = $a->ArticleGet();
		if ($this->layout->article['approved'] && $this->layout->article['available'])
		{
			if($this->layout->article['jump_to_source']!="1")
			{
				$url = $this->Output("article",array('id'=>$id_article));
				$images = $a->ImageGetAll();
				if (count($images)>0)
				{
					foreach($images as $image)
					{
						$image['id_article'] = $id_article;
						$this->layout->ItemSet("image",$image);
						$this->ImageArticle($image['id_image'],$image['size'],$id_article,$image['format'],$image['zoom']);
						$this->layout->ItemUnset();
					}
				}
				$docs = $a->DocGetAll();
				if (count($docs)>0)
				{
					foreach($docs as $doc)
					{
						$this->DocArticle($doc['id_doc'],$doc['format']);
					}
				}
				$boxes = $a->BoxGetAll(true);
				if (count($boxes)>0)
				{
					foreach($boxes as $box)
					{
						$this->layout->ItemSet("box",$box);
						$this->Output("article_box",array('id'=>$box['id_box']));
						$this->layout->ItemUnset();
					}
				}
			}
			else 
			{
				$this->MessageSet("publish_article_no_link",array($this->layout->article['headline']));
			}
			$aimage = $a->Image();
			if($aimage['id_image'])
			{
				$this->ImageArticleAssociated($aimage['id_image'],$aimage['size'],$id_article);
			}
			if(!$this->layout->article['jump_to_source'])
			{
				$this->MessageSet($this->tr->Translate("publish_article") . " <a href=\"$url\" target=\"_blank\">{$this->layout->article['headline']}</a>");			
			}
			if ((!($this->layout->article['published_ts'])>0) || $is_new)
			{
				$a->PublishedSet();
				if((!($this->layout->article['published_ts'])>0)) {
				    $this->ArticleNotify($id_article, $url);
				}
			}
			if ($is_new && $this->layout->article['show_latest']=="0")
				$a->ShowLatestSet();
		}
		else
		{
			$this->MessageSet("publish_article_no",array($this->layout->article['headline']));
		}	
		unset($this->layout->article);
	}
	
	private function ArticleNotify($id_article,$url) {
		include_once(SERVER_ROOT."/../classes/contribution.php");
		$co = new Contribution("article");
		$co->Notify($id_article, $url, $this->layout->article['headline']);
		$conf = new Configuration();
		if($conf->Get("rocketchat_webhook")!='' && $this->layout->article['show_latest'] && $this->layout->topic->row['show_latest']) {
			$webhook = parse_url($conf->Get("rocketchat_webhook"));
			$data = array();
			$data['url'] = $url;
			$data['title'] = $this->layout->article['headline'];
			$data['topic'] = $this->layout->topic->name;
			$data['id_topic_group'] = $this->layout->topic->id_group;
			$this->fm->PostData("{$webhook['scheme']}://{$webhook['host']}", $webhook['path'], $data, false, 15, true);
		}
	}

	private function DocArticle($id_doc,$format)
	{
		$orig = $this->irl->PathAbs("article_doc",array('id'=>$id_doc,'format'=>$format));
		$copy = $this->irl->PublicPath("article_doc",array('id'=>$id_doc,'format'=>$format),FALSE,TRUE);
		$this->fm->Copy($orig,$copy);
		if($this->layout->docs_covers_size > -1)
		{
			$cover_orig = $this->irl->PathAbs("doc_cover",array('id'=>$id_doc));
			$cover_copy = $this->irl->PublicPath("doc_cover",array('id'=>$id_doc),TRUE,TRUE);
			$this->fm->Copy($cover_orig,$cover_copy);
		}
	}
	
	public function Error404()
	{
		$this->Output("error404",array());
	}

	public function Everything($queue=false) {
		$this->TopicInit(0);
		$this->JobExecute(array('id_type'=>'11'),0,FALSE);
		$this->TopicInit(0);
		$this->Error404();
		$this->Feeds();
		$this->SiteMap();
		$this->layout->topic->queue->JobDelete( 0, $this->layout->topic->queue->types['map'] );
		$this->JavaScriptCustom();
		$conf = new Configuration();
		if($conf->Get("htaccess")) {
			$this->Redirects();
		}
		if(!$this->isCDN) {
			include_once(SERVER_ROOT."/../classes/galleries.php");
			include_once(SERVER_ROOT."/../classes/gallery.php");
			$gg = new Galleries();
			foreach($gg->Visible() as $gallery) {
				$g = new Gallery($gallery['id_gallery']);
				$g->Publish();
			}
		}
		include_once(SERVER_ROOT."/../classes/sitemap.php");
		$sim = new Sitemap();
		$sim->Generate();
		if($queue) {
			$this->layout->topic->queue->JobsDeleteAll();
			include_once(SERVER_ROOT."/../classes/topics.php");
			include_once(SERVER_ROOT."/../classes/topic.php");
			$topics = new Topics;
			$groups = $topics->gh->GroupsAll();
			foreach($groups as $group) {
				$group_items = $topics->gh->GroupItems($group['id_group']);
				if (count($group_items)>0) {
					foreach($group_items as $item) {
						$t = new Topic($item['id_item']);
						if($t->id!=$topics->temp_id_topic && $t->id_style>0) {
							$t->queue->JobInsert($t->queue->types['all'],$t->id,"confirmed");
						}
					}
				}
			}
		}
	}
	
	public function Feeds()
	{
		$this->fm->DirClean("pub/feeds");
		$this->TopicInit(0);
		include_once(SERVER_ROOT."/../classes/topics.php");
		$url = $this->Output("feeds",array(),true);
		$this->layout->groups = new Topics;
		$this->layout->groups->gh->LoadTree();
		$this->MessageSet($this->tr->TranslateParams("publish_rss",array($url)));
		$this->RssGroup(0);
		$groups = $this->layout->groups->gh->groups;
		foreach($groups as $group)
		{
			if ($group['visible'])
				$this->RssGroup($group['id_group']);
		}
		$topics = $this->layout->groups->Visible();
		foreach($topics as $topic)
		{
			$this->TopicInit($topic['id_topic']);
			$this->RssTopic(false);
		}
		$this->TopicInit(0);
		$this->RssEvents();
		$this->FeedsKeywords();
	}
	
	private function FeedsKeywords()
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology();
		$kfeeds = array();
		$o->KeywordsFeeds($kfeeds,false);
		foreach($kfeeds as $kfeed)
		{
			$this->RssKeyword($kfeed['id_keyword'],$kfeed['keyword'] . " - " . $kfeed['description']);
		}
	}
	
	public function GalleryXml($id_gallery)
	{
		include_once(SERVER_ROOT."/../classes/gallery.php");
		$g = new Gallery($id_gallery);
		$this->layout->TopicInit(0,true);
		$img_sizes = $this->layout->conf->Get("img_sizes");
		$g->sort_order = $g->slides_order;
		$images = $g->Images();
		$im = array();
		$im['id'] = $g->id;
		$im['title'] = $g->title;
		$im['count'] = count($images);
		$width = $img_sizes[$g->image_size];
		$im['width'] = $width;
		$aimage = $g->Image();
		$im['height'] = $aimage['image_height'];
		$im['url'] = $this->layout->irl->PublicUrlGlobal("galleries",array('id_gallery'=>$g->id,'subtype'=>"gallery"));
		if($this->layout->IsModuleActive("media"))
			$im['logo'] = $this->layout->irl->PublicUrlGlobal("watermark",array());
		$images_array = array();
		foreach($images as $image)
		{
			$image['show_orig'] = $g->show_orig;
			$image['show_author'] = $g->show_author;
			if($g->show_author)
				$image['gallery_author'] = $g->author;
			$image['show_date'] = $g->show_date;
			$image['id_licence'] = ($image['id_licence']>0)? $image['id_licence'] : $g->id_licence;
			$pic = $this->layout->GalleryImageItem($image,false,"track");
			$pic['location'] = array('xvalue'=>$pic['src']['url']);
			if($image['link']!="")
				$pic['info'] = array('xvalue'=>$image['link']);
			unset($pic['prev']);
			unset($pic['next']);
			unset($pic['src']);
			unset($pic['licence']);
			$image_info = "<div class=\"image-info\">";
			$image_info .= "<div class=\"caption\">" . ($g->captions_html? $this->layout->th->Htmlise($image['caption'],false) : $image['caption']) . "</div>";
			if($pic['author']!="")
				$image_info .= "<div class=\"author\">" . $this->layout->tr->Translate("author") . ": {$pic['author']}</div>";
			if($pic['date']!="")
				$image_info .= "<div class=\"image-date\">" . $this->layout->tr->Translate("date") . ": {$pic['date']}</div>";
			if($pic['source']!="")
				$image_info .= "<div class=\"source\">" . $this->layout->tr->Translate("source") . ": {$pic['source']}</div>";
			if($g->row['note']!="")
				$image_info .= "<div class=\"notes\">" . $g->row['note'] . "</div>";
			$image_info .= "</div>";
			$pic['annotation'] = array('xvalue'=>htmlspecialchars($image_info));
			$images_array['i_' . $image['id_image']] = $pic; 
		}
		$im['trackList'] = $images_array;
		$xh = new XmlHelper();
		$xh->root_element = "playlist";
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xml .= $xh->Array2Xml($im,false);
		$path = "pub/" . $this->layout->irl->paths['graphic'] . "galleries";
		$this->fm->DirAction("$path/$g->id","check");
		$filename = "$path/{$g->id}/images.xml";
		$this->fm->WritePage($filename,$xml);
		$this->fm->PostUpdate();
	}

	public function Graphics($current_style=true)
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles();
		$sgraphics = $s->GraphicsAll(0,$current_style);
		foreach($sgraphics as $sgraphic)
		{
			$this->ImageGraphic($sgraphic['id_graphic'],$sgraphic['format']);
		}
		if ($current_style && $this->layout->topic->id > 0)
			$this->TopicGraphics();
	}

	public function Home()
	{
		$hometype = $this->ini->GetModule("homepage","hometype",0);
		if($hometype>0)
		{
			$this->fm->Delete("pub/index.html");
			$content = "<?php\ndefine('SERVER_ROOT',\$_SERVER['DOCUMENT_ROOT']);\n";
			$content .= "include_once(SERVER_ROOT.\"/../classes/error.php\");\n";
			$content .= "include_once(SERVER_ROOT.\"/../classes/layout.php\");\n";
			$content .= "\$l = new Layout();\n";
			$content .= "echo \$l->Output(\"homepage\",0,0,1,array(),true);\n";
			$content .= "?>\n";
			$this->fm->WritePage("pub/index.php", $content);
		}
		else 
		{
			$this->fm->Delete("pub/index.php");
			$this->Output("homepage",array(),TRUE);
			foreach($this->landing as $landing) {
				$this->MessageSet("publish_landing",array("{$this->layout->pub_web}/{$landing}",$landing));
				$this->Output("homepage",array('subtype'=>'landing','landing'=>$landing),TRUE);
			}
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology();
			$klanding = array();
			$o->KeywordsLanding($klanding,false);
			foreach($klanding as $keyword) {
				$this->MessageSet("publish_keyword",array("{$this->layout->pub_web}/{$keyword['keyword']}",$keyword['keyword']));
				$this->Output("homepage",array('subtype'=>'keyword','id_keyword'=>$keyword['id_keyword'],'keyword'=>$keyword['keyword']),TRUE);
			}
		}
		$this->MessageSet("publish_home",array($this->layout->pub_web));
	}
	
	public function HomeHighlightCheck() {
	    include_once(SERVER_ROOT."/../classes/articles.php");
	    $highlight_days = $this->ini->GetModule("homepage","highlight_days",7);
	    Articles::HighlightCheck($highlight_days);
	}

	private function ImageArticle($id_image,$size,$id_article,$format,$zoom)
	{
		if(!$this->isCDN) {
			$orig = $this->irl->PathAbs("image",array('id'=>$id_image,'size'=>$size,'format'=>$format));
			$copy = $this->irl->PublicPath("image",array('id'=>$id_image,'size'=>$size,'id_article'=>$id_article,'format'=>$format),FALSE,TRUE);
			$this->fm->Copy($orig,$copy);
			if ($zoom)
	       	{
		        $orig2 = $this->irl->PathAbs("image_orig",array('id'=>$id_image,'format'=>$format));
		        $copy2 = $this->irl->PublicPath("image_orig",array('id'=>$id_image,'format'=>$format),FALSE,TRUE);
		        $this->fm->Copy($orig2,$copy2);
			    $this->Output("image_zoom",array('id'=>$id_image,'id_article'=>$id_article));
			}
		}
	}
	
	private function ImageArticleAssociated($id_image,$size,$id_article)
	{
		if(!$this->isCDN) {
			$orig = $this->irl->PathAbs("image",array('id'=>$id_image,'size'=>$size));
			$copy = $this->irl->PublicPath("image",array('id'=>$id_image,'id_article'=>$id_article,'associated'=>true),FALSE,TRUE);
			$status = $this->fm->Copy($orig,$copy);
			if(!$status)
			{
				include_once(SERVER_ROOT."/../classes/article.php");
				$a = new Article($id_article);
				$a->ImageAssociate(0,0,0,false);
			}
		}
	}
	
	public function ImageGallery($id_image,$image_size,$id_gallery)
	{
		if(!$this->isCDN) {
			$orig = $this->irl->PathAbs("gallery_image",array('id'=>$id_image,'image_size'=>$image_size));
			$copy = $this->irl->PublicPath("gallery_image",array('id'=>$id_image,'id_gallery'=>$id_gallery,'image_size'=>$image_size),FALSE,TRUE);
			$this->fm->Copy($orig,$copy);
		}
	}

	private function ImageGraphic($id_graphic,$format)
	{
		if(!$this->isCDN) {
			$orig = $this->irl->PathAbs("graphic",array('id'=>$id_graphic,'format'=>$format));
			$copy = $this->irl->PublicPath("graphic",array('id'=>$id_graphic,'format'=>$format),TRUE,TRUE);
			$this->fm->Copy($orig,$copy);
		}
	}

	private function ImageSubtopic($id_image,$size,$id_subtopic)
	{
		$size = (int)$size;
		if(!$this->isCDN) {
			$orig = $this->irl->PathAbs("image",array('id'=>$id_image,'size'=>$size));
			$copy = $this->irl->PublicPath("image",array('id'=>$id_image,'id_subtopic'=>$id_subtopic,'size'=>$size),FALSE,TRUE);
			$status = $this->fm->Copy($orig,$copy);
			if(!$status)
				$this->layout->topic->SubtopicImageUpdate($id_subtopic,0,false);
		}
	}

	public function JavascriptCustom()
	{
		$this->fm->DirAction("pub/jsc","check");
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$phpeace = new PhPeace();
		$highslide = $this->layout->conf->Get("highslide"); 
		$languages = $phpeace->Languages();
		$terms = array("hs_fullExpandTitle","hs_loadingText","hs_loadingTitle","hs_restoreTitle");
		foreach($languages as $key => $language)
		{
			$js = "var phpeace = {\npub_web : '" . str_replace("http://","",$this->layout->pub_web) . "'\n};\n\n";
			$js .= "hs.graphicsDir = '{$this->layout->pub_web}/js/highslide/graphics/';\n\n";
			if($this->layout->IsModuleActive("widgets"))
			{
				include_once(SERVER_ROOT."/../classes/widgets.php");
				$wi = new Widgets();
				$js .= "var widgets_abuse_reporting = " . ($wi->widgets_abuse_reporting?"true":"false") . ";\n\n";
				$widgets_labels = array();
				$widgets_labels[] = "add";
				$widgets_labels[] = "cancel";
				$widgets_labels[] = "contact_lists";
				$widgets_labels[] = "contacts";
				$widgets_labels[] = "contacts_loading_wait";
				$widgets_labels[] = "contacts_select";
				$widgets_labels[] = "comments";
				$widgets_labels[] = "copyright";
				$widgets_labels[] = "create";
				$widgets_labels[] = "ie6";
				$widgets_labels[] = "next";
				$widgets_labels[] = "no_emails";
				$widgets_labels[] = "preview_close";
				$widgets_labels[] = "report";
				$widgets_labels[] = "report_error";
				$widgets_labels[] = "report_sent";
				$widgets_labels[] = "report_submit";
				$widgets_labels[] = "report_title";
				$widgets_labels[] = "report_welcome";
				$widgets_labels[] = "retrieving";
				$widgets_labels[] = "save";
				$widgets_labels[] = "sending";
				$widgets_labels[] = "sending_wait";
				$widgets_labels[] = "sent";
				$widgets_labels[] = "sent_error";
				$widgets_labels[] = "sent_failed";
				$widgets_labels[] = "sent_problem";
				$widgets_labels[] = "sent_shared";
				$widgets_labels[] = "share";
				$widgets_labels[] = "share_select";
				$widgets_labels[] = "share_title";
				$widgets_labels[] = "tab_add";
				$widgets_labels[] = "tab_remove";
				$widgets_labels[] = "tab_remove_question";
				$widgets_labels[] = "title";
				$widgets_labels[] = "view_edit";
				$widgets_labels[] = "view_only";
				$widgets_labels[] = "widget_add_edit";
				$widgets_labels[] = "widget_collapse";
				$widgets_labels[] = "widget_remove";
				$widgets_labels[] = "widget_remove_question";
				$widgets_labels[] = "widgets_tab_add";
				$widgets_labels[] = "widgets_tab_no";
				$trm32 = new Translator($this->layout->tr->id_language, 32, false);
				$w_labels = array();
				foreach($widgets_labels as $label)
				{
					$tra_label = $trm32->Translate($label);
					$w_labels[] = "$label : '" . $this->layout->th->TextReplaceForJavascript($tra_label) . "'";
				}
				$js .= "var widgets_labels = {\n" . implode(",\n",$w_labels) . "\n};\n\n";
			}
			if($highslide)
			{
				$js .= "// Highslide\n";
				$js .= "hs.showCredits = false;\n";
				$js .= "hs.outlineType = 'rounded-white';\n";
				$js .= "hs.wrapperClassName = 'highslide-white';\n";
				if($key>0)
				{
					$tr_temp = new Translator($key,0,false);
					foreach($terms as $term)
					{
						$hskey = str_replace("hs_","hs.",$term);
						$js .= "$hskey = '" . $this->layout->th->TextReplaceForJavascript($tr_temp->Translate(strtolower($term))) . "';\n";
					}
				}
			}
			$this->fm->WritePage("pub/jsc/custom_{$key}.js",$js);
		}
	}
	
	public function JobExecute( $job, $id_topic=0, $from_queue=TRUE )
	{
		if(!$from_queue)
			$this->SetTimeout();
		$url = $this->layout->topic->url;
		switch($job['id_type'])
		{
			case "1":  // article
				switch($job['action'])
				{
					case "delete":
						$path = $this->irl->PublicPath("article",array('id'=>$job['id_item']),false,true);
						$this->fm->Delete($path);
						// TODO Check if there are any associated documents, zoomable images or symlinked images, and remove them
						$this->MessageSet("publish_article_deleted",array($job['id_item']));
					break;
					case "update":
					    $this->Article($job['id_item'],isset($job['param']) && $job['param']=='new');
					break;
				}
			break;
			case "2":  // article_box
				switch($job['action'])
				{
					case "delete":
						$path = $this->irl->PublicPath("article_box",array('id'=>$job['id_item']),false);
						$this->fm->Delete($path);
					break;
					case "update":
						$this->Output("article_box",array('id'=>$job['id_item']));
					break;
				}
			break;
			case "3":  // event, not used
			case "4":  // link, not used
			case "5":  // book, not used
			break;
			case "6":  // gallery
				switch($job['action'])
				{
					case "update_xml":
						if($job['id_item']>0)
							$this->GalleryXml($job['id_item']);
					break;
				}
			break;
			case "7":  // xdomain
				if($job['id_item']>0 && $job['id_item']==$id_topic)
				{
					if($this->layout->topic->row['domain']!="")
					{
						if(!$this->fm->Exists("uploads/graphics/favicon/{$id_topic}.ico"))
						{
							$this->fm->HardCopy("uploads/graphics/favicon/0.ico","uploads/graphics/favicon/{$id_topic}.ico");
						}
						$this->fm->Copy("uploads/graphics/favicon/{$id_topic}.ico","pub/{$this->layout->topic->path}/favicon.ico");
					}
					else 
					{
						$this->fm->Delete("pub/{$this->layout->topic->path}/favicon.ico");
					}
				}
			break;
			case "8":  // subtopic
				switch($job['action'])
				{
					case "delete":
						$this->SubtopicRemove($job['id_item']);
					break;
					case "update":
						$this->Subtopic($job['id_item']);
					break;
				}
			break;
			case "9":  // topic homepage
				$url = $this->TopicHome();
				$this->MessageSet("publish_topic_home",array($url));
			break;
			case "10":  // latest
				if (!$this->done_map && !$this->scheduler && $this->layout->topic->row['show_latest']=="1" && $this->layout->topic->row['visible']=="1")
				{
					$this->RssTopic();
					$this->FeedsKeywords();
					$id_subtopic_map = $this->layout->topic->HasSubtopicType(4);
					if ($id_subtopic_map > 0)
						$this->Subtopic($id_subtopic_map);
					$this->SiteMap();
					$this->Home();
				}
			break;
			case "11":  // homepage
				if (!$this->scheduler)
				{
					$this->Home();
				}
			break;
			case "12":  // map
			{
				if (!$this->scheduler && !$this->done_map)
					$this->SiteMap();
			}
			break;
			case "13":  // all
				$this->TimeStart();
				$notify = $this->TopicEverything($job['id_item']);
				if($notify && $this->scheduler) 
					$this->NotifyUser($job['id_user'],$job['id_item']);
			break;
		}
		if ($job['id_type'] != $this->layout->topic->queue->types['all'] && $from_queue)
		{
			$this->layout->TopicInit($id_topic);
			$this->layout->topic->queue->JobDelete( $job['id_job'], $job['id_type'] );
		}
		$this->fm->PostUpdate();
	}

	public function Log( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT topics.name AS topic_name,users.name AS user_name,UNIX_TIMESTAMP(pl.publish) AS publish_ts
		FROM publish_log pl
		INNER JOIN users ON pl.id_user=users.id_user
		INNER JOIN topics ON pl.id_topic=topics.id_topic
		ORDER BY pl.publish DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function LogTopic( &$rows, $id_topic )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT users.name AS name,UNIX_TIMESTAMP(publish_log.publish) AS publish_ts
		FROM publish_log
		LEFT JOIN users ON publish_log.id_user=users.id_user
		WHERE publish_log.id_topic='$id_topic'
		ORDER BY publish_log.publish DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function MessageGet($format=true)
	{
		$msg_list = $this->session->Get("pmsg");
		$this->session->Delete("pmsg");
		$out = $format? "" : array();
		if (is_array($msg_list) && count($msg_list)>0)
		{
			$counter = 1;
			foreach($msg_list as $msg)
			{
				if($format)
					$out .= "<li>$msg</li>\n";
				else
					$out['pub_msg_'.$counter] = $msg;
				$counter ++;
			}
		}
		return $format? "<ul class=\"msg\">$out</ul>" : $out;
	}

	public function MessageSet($msg,$params=array())
	{
		$pmsg = $this->session->Get("pmsg");
		if (is_array($pmsg) && count($pmsg)>0)
			$smsg = $pmsg;
		else
			$smsg = array();
		$smsg[] = $this->tr->TranslateParams($msg,$params);
		$this->session->Set("pmsg",$smsg);
	}
	
	private function MicrotimeFloat()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	public function NotifyUser($id_user,$id_topic)
	{
		if($id_user>0 && $id_topic>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			include_once(SERVER_ROOT."/../classes/user.php");
			include_once(SERVER_ROOT."/../classes/mail.php");
			$seconds = $this->TimeStat();
			$t = new Topic($id_topic);
			$u = new User();
			$u->id = $id_user;
			$user = $u->UserGet();
			$mail = new Mail();
			$subject = "{$t->name}";
			$tr = new Translator($user['id_language'],0);
			$msg = $tr->TranslateParams("publish_completed",array($t->name,$t->url));
			$msg .= $tr->TranslateParams("execution_time",array($seconds)) . "\n\n";
			$mail->SendMail($user['email'],$user['name'],$subject,$msg,array());
		}
	}

	public function Output($type,$params,$global=FALSE)
	{
		$path = $this->irl->PublicPath($type,$params,$global,TRUE);
		$this->layout->global = $global;
		if ($global)
		{
			$html = $this->layout->Output($type,$params['id'],0,1,$params,true);
			$url = $this->layout->irl->PublicUrlGlobal($type,$params);
		}
		else
		{
			$html = $this->layout->Output($type,$params['id'],$this->layout->topic->id,1,$params);			
			$url = $this->layout->irl->PublicUrlTopic($type,$params,$this->layout->topic);
		}
		$this->fm->WritePage($path, $html);
		$html = null;
		unset($html);
		return $url;
	}
	
	public function Redirects()
	{
		if($this->layout->conf->Get("htaccess"))
		{
			$htaccess = "ErrorDocument 404 /404.html\n";
			$htaccess .= "ErrorDocument 403 \"No indexing allowed\n";
			$htaccess .= "Options +FollowSymLinks -Indexes -Includes\n";
			$redirects = array();
			$this->layout->irl->Redirects( $redirects, false, true );
			foreach($redirects as $redirect)
			{
				$htaccess .= "RedirectMatch ";
				if($redirect['id_type']=="1")
					$htaccess .= "301 ";
				
				$from = preg_quote($redirect['url_from']);
				$htaccess .= "(?i)^{$from}$ {$redirect['url_to']}\n";
			}
			if($this->mod_rewrite)
			{
				$htaccess .= "RewriteEngine On\n";
				$htaccess .= "RewriteRule ^rest/.*$ /tools/rest.php [L]\n";
				foreach($this->landing as $landing) {
					$htaccess .= "RewriteRule ^{$landing}$ /{$landing}.html [L,NC]\n";
				}
				include_once(SERVER_ROOT."/../classes/ontology.php");
				$o = new Ontology();
				$klanding = array();
				$o->KeywordsLanding($klanding,false);
				foreach($klanding as $keyword) {
					$htaccess .= "RewriteRule ^{$keyword['keyword']}$ /{$keyword['keyword']}.html [L,NC]\n";
				}
			}
			$this->fm->WritePage( "pub/.htaccess" , $htaccess);
			$this->MessageSet($this->tr->Translate("redirect_published"));		
		}
	}
	
	private function RssGroup($id_group)
	{
		$this->Output("rss",array('id'=>$id_group,'subtype'=>"topic_group"),true);
	}

	private function RssEvents()
	{
		$url = $this->Output("rss",array('subtype'=>"events"),true);
		$this->MessageSet("publish_events_rss",array($url));
	}

	private function RssKeyword($id_keyword,$name)
	{
		$url = $this->Output("rss",array('id'=>$id_keyword,'subtype'=>"keyword"),true);
		$this->MessageSet("publish_topic_rss",array($url,$name));
	}

	private function RssSubtopic($id_subtopic,$name)
	{
		$url = $this->Output("rss",array('id'=>$id_subtopic,'subtype'=>"subtopic"),true);
		$this->MessageSet("publish_topic_rss",array($url,$name));
	}

	private function RssTopic($propagate=true)
	{
		$url = $this->Output("rss",array('id'=>$this->layout->topic->id,'subtype'=>"topic"),true);
		$this->MessageSet("publish_topic_rss",array($url,$this->layout->topic->name));
		if ($propagate)
		{
			include_once(SERVER_ROOT."/../classes/topics.php");
			$this->layout->groups = new Topics;
			$this->layout->groups->gh->LoadTree();
			$parents = array();
			$this->layout->groups->gh->th->GroupParents($this->layout->topic->row['id_group'],$parents);
			foreach($parents as $group)
				$this->RssGroup($group['id_group']);
			$this->RssGroup(0);
		}
	}
	
	public function SiteMap()
	{
		$this->Output("map",array('id'=>0),TRUE);
		include_once(SERVER_ROOT."/../classes/topics.php");
		$this->layout->groups = new Topics;
		$groups = $this->layout->groups->gh->GroupsAll(true);
		foreach($groups as $group)
		{
			$this->Output("map",array('id'=>$group['id_group'],'subtype'=>"topics"),TRUE);
		}
		$this->done_map = true;
		$this->MessageSet("publish_map",array($this->layout->pub_web . "/" . $this->ini->Get("map_path")));
	}

	public function StepsCounter($num_articles)
	{
		$steps = floor(($num_articles-1)/($this->step_articles)) + 1;
		if($steps==0)
			$steps = 1;
		return $steps;
	}
	
	private function Subtopic($id_subtopic)
	{
		$subtopic = $this->layout->topic->SubtopicGet($id_subtopic);
		if ($subtopic['visible']!="4" && $this->layout->topic->SubtopicPublishable($subtopic['id_type']))
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$this->layout->ItemSet("subtopic",$subtopic);
			$this->SubtopicPager($id_subtopic,$subtopic['id_type'],array('id_item'=>$subtopic['id_item'],'id_subitem'=>$subtopic['id_subitem']));
			if ($subtopic['id_image']>0)
			{
				$params = $v->Deserialize($subtopic['params']);
				$this->ImageSubtopic($subtopic['id_image'],$params['image_size'],$id_subtopic);
				$this->ImageSubtopic($subtopic['id_image'],2,$id_subtopic);
			}
			$children = $this->layout->topic->SubtopicSubtopics($id_subtopic);
			foreach($children as $child)
			{
				if($child['id_image']>0)
				{
					$cparams = $v->Deserialize($child['params']);
					$this->ImageSubtopic($child['id_image'],$cparams['image_size'],$child['id_subtopic']);
				}
			}
			if($subtopic['has_feed'])
			{
				$this->RssSubtopic($id_subtopic,$subtopic['name']);
			}
			$this->layout->topic->SubtopicPublishedSet($id_subtopic);
			$this->layout->ItemUnset();
			$url = $this->layout->irl->PublicUrlTopic("subtopic",array('id'=>$id_subtopic,'id_type'=>$subtopic['id_type']),$this->layout->topic);
			$this->MessageSet($this->tr->Translate("subtopic_updated") . " <a href=\"$url\" target=\"_blank\">{$subtopic['name']}</a>");
		}
	}

	private function SubtopicPager($id_subtopic,$id_subtopic_type,$params)
	{
		include_once(SERVER_ROOT."/../classes/pagetypes.php");
		$pt = new PageTypes();
		$this->layout->id_type = $pt->types["subtopic"];
		$this->layout->subtype = $params['id_item'];
		$this->layout->id_style = (int)($this->layout->topic->id_style);
		$subtopic_pages = $this->layout->Subtopic($id_subtopic);
		$counter = 0;
		foreach($subtopic_pages as $spage)
		{
		    $path = $this->irl->PublicPath("subtopic",array('id'=>$id_subtopic,'id_type'=>$id_subtopic_type,'page'=>$counter),FALSE,TRUE);
		    $items = $spage['subtopic']['content']['items'];
			if(is_array($items) && count($items)>0)
			{
				foreach($items as $item)
				{
					if(is_array($item) && isset($item['type']) && $item['type']=="article" && isset($item['image']['id']) && $item['image']['id']>0 && $item['id']>0)
					{
						$this->ImageArticleAssociated($item['image']['id'],$item['image']['size'],$item['id']);
					}
				}
			}
		    $spage['page'] = $this->layout->CurrentPage('subtopic',false,array('page'=>$counter));
		    if($this->layout->conf->Get("ui")) {
		        $this->layout->StructuredData($spage);
		    }
			$xml = $this->layout->xh->Array2Xml($spage);
			$html = $this->layout->xh->Transform("subtopic",$xml,$this->layout->id_style);
			$this->layout->DoctypeHTML5Fix($html);
			$this->fm->WritePage($path, $html);
			$counter++;
		}
		unset($subtopic_pages);
		if ($id_subtopic_type==$this->layout->topic->subtopic_types['gallery'] && $params['id_item']>0 && $params['id_subitem']!=2)
		{
			$images = $this->layout->topic->SubtopicGalleryImages($params['id_item'],$id_subtopic,$params['id_subitem']=="3");
			$pm2 = new PublishManager();
			$pm2->TopicInit($this->layout->topic->id);
			$pm2->layout->id_style = $this->layout->id_style;
			$pm2->layout->cache_topic = $this->layout->cache_topic;
			$pm2->layout->CommonCache("gallery_item");
			foreach($images as $image)
			{
				$params = array('id'=>$image['id_image'],'id_gallery'=>$image['id_gallery'],'id_subtopic'=>$id_subtopic);
				$pm2->Output("gallery_item",$params);
				$pm2->ImageGallery($image['id_image'],$image['image_size'],$image['id_gallery']);
				$pm2->ImageGallery($image['id_image'],$image['thumb_size'],$image['id_gallery']);
			}
			unset($pm2->layout->cache_topic);
			unset($pm2->layout->cache_common);
		}
	}
	
	private function SubtopicRemove($id_subtopic)
	{
		$subtopic = $this->layout->topic->SubtopicGet($id_subtopic);
		$this->fm->Delete($this->irl->PublicPath("rss",array('id'=>$id_subtopic,'subtype'=>"subtopic"),true));
		if ($this->layout->topic->SubtopicPublishable($subtopic['id_type']))
		{
			$this->fm->Delete($this->irl->PublicPath("subtopic",array('id'=>$id_subtopic,'id_type'=>$subtopic['id_type']),FALSE,TRUE));
			$this->MessageSet("subtopic_deleted",array($subtopic['name']));
			$this->layout->topic->SubtopicPublishedSet($id_subtopic,false);
		}
	}

	private function SetTimeout()
	{
		if(!$this->scheduler)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$va = new Varia();
			$va->SetTimeLimit($this->topic_publish_timeout);
		}
	}
	
	public function TimeStart()
	{
		$this->time_exec = $this->MicrotimeFloat();
	}
	
	private function TimeStat()
	{
		return number_format($this->MicrotimeFloat() - $this->time_exec,3);
	}

	public function Topic($id_topic)
	{
		if ( $id_topic!=$this->ini->Get('temp_id_topic'))
		{
			$this->SetTimeout();
			$this->TopicInit($id_topic);
			if($id_topic>0)
			{
				$this->layout->id_style = (int)($this->layout->topic->id_style);
				$this->fm->DirTopic("pub/{$this->layout->topic->path}","check",$this->layout->topic->row['domain']!="");
				$this->MessageSet("publish_summary",array($this->layout->topic->name));
				if ($this->layout->topic->id_style>0)
				{
					if ($this->layout->topic->path!="")
					{
						$this->TopicGraphics();
						if($this->session->Get('module_admin')==1 || ($this->session->Get('current_user_id')>0 && $this->layout->topic->AmIAdmin()) || $this->scheduler)
						{
							$jobs = $this->layout->topic->queue->JobsAll();
							if (count($jobs)>0)
							{
								$this->layout->cache_topic = $this->layout->TopicCommon();
								$this->TopicXml();
								$this->TopicHtaccess();
								foreach($jobs as $job)
								{
									$this->JobExecute( $job, $id_topic );
								}
								$this->TopicLogInsert($id_topic);
								unset($this->layout->cache_topic);
							}
							else
								$this->MessageSet("publish_queue_empty");
						}
						else
						{
							$this->MessageSet("user_no_auth_topic",array($this->layout->topic->name));
						}
					}
					else 
					{
						$this->MessageSet("missing_path_topic",array($this->layout->topic->name,$id_topic));
					}
				}
				else
				{
					$this->MessageSet("missing_style",array($this->layout->topic->name,$id_topic));
					$this->layout->TopicInit($id_topic);
					$this->layout->topic->queue->JobsDelete(true);
				}
			}
			else 
			{
				$jobs = $this->layout->topic->queue->JobsAll();
				if (count($jobs)>0)
				{
					foreach($jobs as $job)
						$this->JobExecute( $job, $id_topic );
				}
			}
			$this->MessageSet("</br>");
		}
		else
		{
			$this->MessageSet("publish_bin");
		}
	}

	public function TopicHtaccess()
	{
		if($this->layout->conf->Get("htaccess") && $this->layout->topic->path !="")
		{
			$urls = $this->layout->irl->FriendlyUrlAll($this->layout->topic->id);
			if((count($urls)>0 && $this->layout->topic->row['friendly_url']>0) || $this->layout->topic->protected=="1" || ($this->layout->topic->row['domain']!="" && $this->mod_rewrite))
			{
				$htaccess = "";
				if($this->layout->topic->protected=="1")
				{
					$htaccess = "AuthUserFile {$this->fm->path}/pub/{$this->layout->topic->path}/.htpasswd\nAuthGroupFile /dev/null\nAuthName \"{$this->layout->topic->name}\"\nAuthType Basic\nRequire valid-user\n";
					$htpasswd = $this->layout->topic->PasswordHtpasswd();
					$this->fm->WritePage("pub/{$this->layout->topic->path}/.htpasswd",$htpasswd);
				}
				else 
				{
					$this->fm->Delete("pub/{$this->layout->topic->path}/.htpasswd");
				}
				if($this->mod_rewrite)
				{
					$htaccess .= "RewriteEngine On\n";
					// $htaccess .= "RewriteBase /{$this->layout->topic->path}\n";
					if($this->layout->topic->row['domain']!="")
						$htaccess .= "RewriteRule ^js/(.*) {$this->layout->pub_web}/js/$1 [L,P]\n";
				}
				foreach($urls as $url)
				{
					if($this->mod_rewrite)
					{
						if(strpos($url['url'],$this->layout->topic->domain)!==false)
						{
							$red_url = str_replace($this->layout->topic->domain,"",$url['url']);
							if(strpos($red_url,'tools/form.php')!==false)
							{
								$htaccess .= "RewriteRule ^{$url['furl']}$ {$red_url} [L,QSA]\n";
							}
							else
								$htaccess .= "RewriteRule ^{$url['furl']}$ {$red_url} [L,NC]\n";
						}
						else
							$htaccess .= "RewriteRule ^{$url['furl']}$ {$url['url']} [L,P]\n";
					}
					else
					{
						$htaccess .= "Redirect /{$this->layout->topic->path}/{$url['furl']} {$url['url']}\n";	
					}
				}
				$this->fm->WritePage("pub/{$this->layout->topic->path}/.htaccess", $htaccess);
			}
			else
			{
				$this->fm->Delete("pub/{$this->layout->topic->path}/.htaccess");
			}
		}
	}
	
	public function TopicEverything($id_topic)
	{
		$notify = false; 
		if ( $id_topic!=$this->ini->Get('temp_id_topic') && $id_topic>0 )
		{
			if(!$this->force)
				$this->SetTimeout();
			include_once(SERVER_ROOT."/../classes/article.php");
			$this->TopicInit($id_topic);
			if ($this->layout->topic->id_style>0 && $this->layout->topic->path!="")
			{
				if ($this->session->Get('module_admin')==1 || $this->layout->topic->AmIAdmin()  || $this->scheduler)
				{
					$this->layout->id_style = (int)($this->layout->topic->id_style);
					$path = "pub/{$this->layout->topic->path}";
					$this->fm->DirTopic($path,"check",$this->layout->topic->row['domain']!="");
					$articles = $this->layout->topic->Articles();
					$this->steps = $this->StepsCounter(count($articles));
					$current_step = $this->step;
					if($this->scheduler && $this->steps>1 && !$this->force)
						$this->MessageSet("publish_scheduler_no");
					else
					{
						$this->layout->cache_topic = $this->layout->TopicCommon();
						if ($this->step==1 || $this->force)
						{
							$this->Graphics();
							$this->TopicXml();
							if ($this->layout->topic->row['domain']!="")
							{
								$this->fm->HardCopy("pub/robots.txt","$path/robots.txt",false);
								if(!$this->fm->Exists("uploads/graphics/favicon/{$id_topic}.ico"))
								{
									$this->fm->HardCopy("uploads/graphics/favicon/0.ico","uploads/graphics/favicon/{$id_topic}.ico");
								}
								$this->fm->Copy("uploads/graphics/favicon/{$id_topic}.ico","$path/favicon.ico");
								$this->Error404();
							}
						}
						if (($this->step>=1 && $current_step<=($this->steps)) || $this->steps==1 || $this->force)
						{
							$this->layout->CommonCache("article");
							$acounter = 1;
							foreach($articles as $article)
							{
								if ( ($acounter <= (($current_step)*($this->step_articles))) && ($acounter > (($current_step-1) * ($this->step_articles)) ) || $this->steps==1 || $this->force)
								{
									$a = new Article($article['id_article']);
									$this->layout->article = $a->ArticleGet();
									if($this->layout->article['jump_to_source']!="1")
									{
										$url = $this->Output("article",array('id'=>$article['id_article']));
										if (!($this->layout->article['published_ts'])>0)
										{
											$a->PublishedSet();
											$this->ArticleNotify($article['id_article'], $url);
										}
									}
									unset($a);
									unset($this->layout->article);
								}
								$acounter ++;
							}
							unset($articles);
							unset($this->layout->cache_common);
						}
						$this->MessageSet("publish_step",array(($current_step),$this->steps));
						$this->step = $current_step + 1;
						if ($this->steps==1 || $current_step==($this->steps) || $this->force)
						{
							include_once(SERVER_ROOT."/../classes/varia.php");
							$v = new Varia();
							// Subtopics
							$subtopics = $this->layout->topic->Subtopics();
							$this->layout->CommonCache("subtopic");
							foreach($subtopics as $subtopic)
							{
								if ($subtopic['id_image']>0)
								{
									$params = $v->Deserialize($subtopic['params']);
									$this->ImageSubtopic($subtopic['id_image'],$params['image_size'],$subtopic['id_subtopic']);
									$this->ImageSubtopic($subtopic['id_image'],2,$subtopic['id_subtopic']);
								}
								if ($this->layout->topic->SubtopicPublishable($subtopic['id_type']))
								{
									$this->layout->ItemSet("subtopic",$subtopic);
									$this->layout->TopicInit($id_topic);
									$this->SubtopicPager($subtopic['id_subtopic'],$subtopic['id_type'],array('id_item'=>$subtopic['id_item'],'id_subitem'=>$subtopic['id_subitem']));
									if($subtopic['has_feed'])
									{
										$this->RssSubtopic($subtopic['id_subtopic'],$subtopic['name']);
									}
									$this->layout->ItemUnset();
								}
							}
							$this->layout->topic->SubtopicPublishedSetAll();
							unset($subtopics);
							unset($this->layout->cache_common);
							// Images
							$images = $this->layout->topic->ImagesApproved();
							if(count($images)>0)
							{
								$this->layout->CommonCache("image_zoom");
								foreach($images as $image)
								{
									$this->layout->ItemSet("image",$image);
									$this->ImageArticle($image['id_image'],$image['size'],$image['id_article'],$image['format'],$image['zoom']);
									$this->layout->ItemUnset();
								}
								unset($this->layout->cache_common);
							}
							unset($images);
							$aimages = $this->layout->topic->ImagesAssociated();
							foreach($aimages as $aimage)
							{
								$this->ImageArticleAssociated($aimage['id_image'],$aimage['size'],$aimage['id_article']);
							}
							unset($aimages);
							// Docs
							$docs = $this->layout->topic->Docs();
							foreach($docs as $doc)
							{
								$this->DocArticle($doc['id_doc'],$doc['format']);
							}
							unset($docs);
							// Boxes
							$boxes = $this->layout->topic->ArticlesBoxes();
							if(count($boxes)>0)
							{
								$this->layout->CommonCache("article_box");
								foreach($boxes as $box)
								{
									$this->layout->ItemSet("box",$box);
									$this->Output("article_box",array('id'=>$box['id_box']));
									$this->layout->ItemUnset();
								}
								unset($this->layout->cache_common);
							}
							unset($boxes);
							$this->TopicHome();
							$this->TopicHtaccess();
							$this->TopicLogInsert($id_topic);
							$this->layout->TopicInit($id_topic);
							$this->layout->topic->queue->JobsDelete(true,true);
							$url = $this->layout->topic->url;
							$name = $this->layout->topic->name;
							$this->RssTopic();
							if (!$this->done_map)
								$this->SiteMap();
							$this->Home();
							$this->MessageSet("publish_over",array($name,$this->steps));
							$this->MessageSet("<a href=\"$url\" target=\"_blank\">$url</a>");
							$this->fm->PostUpdate();
						}
						unset($this->layout->cache_topic);
						$notify = true; 
					}
				}
				else
					$this->MessageSet("user_no_auth_topic",array($this->layout->topic->name));
			}
			else
			{
				$this->MessageSet("missing_style",array($this->layout->topic->name,$id_topic));
			}
		}
		return $notify; 
	}

	private function TopicGraphics()
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles();
		$tgraphics = $s->GraphicsAll($this->layout->topic->id_style);
		foreach($tgraphics as $tgraphic)
		{
			$this->ImageGraphic($tgraphic['id_graphic'],$tgraphic['format']);
		}
	}

	public function TopicHome()
	{
		if(strlen($this->layout->topic->path)>0)
			$this->fm->DirTopic("pub/{$this->layout->topic->path}","check");					
		return $this->Output("topic_home",array());
	}

	public function TopicInit($id_topic)
	{
		$this->layout->TopicInit($id_topic,true);
		$this->irl->topic = $this->layout->topic;
	}
	
	private function TopicLogInsert($id_topic)
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$db->begin();
		$db->lock( "publish_log" );
		$id_log = $db->nextId( "publish_log", "id_publish_log" );
		$id_user = (int)($this->session->Get('current_user_id'));
		$sqlstr = "INSERT INTO publish_log (id_publish_log,id_user,id_topic,publish)
			VALUES ($id_log,$id_user,$id_topic,'$today_time')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function TopicXml()
	{
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		$xh->root_element = "topic_info";
		$xml = $xh->Array2Xml($this->layout->cache_topic);
		$path = $this->irl->PublicPath("topic_xml",array(),false,true);
		$this->fm->WritePage($path,$xml);
	}

	public function TopicsLogCLean()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "publish_log" );
		$res[] = $db->query( "DELETE FROM publish_log WHERE (DATE_ADD(publish, INTERVAL 30 DAY))<CURRENT_TIMESTAMP");
		Db::finish( $res, $db);
	}

}
?>
