<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Campaigns Management
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Campaign
{
	/**
	 * Campaign ID
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 * Variable name to store campaign action in session
	 *
	 * @var string
	 */
	public $session_var;

	/**
	 * Initialize local properties
	 *
	 * @param integer $id_campaign	Campaign ID
	 */
	function __construct($id_campaign,$from="")
	{
		$this->id = $id_campaign;
		$this->session_var = "camp_{$this->id}_{$from}";
	}

	/**
	 * Delete Account Use
	 *
	 * @param integer $id_use
	 */
	public function AccountDelete($id_use)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$p->AccountUseDelete($id_use);
	}

	/**
	 * Retrieve Account Use
	 *
	 * @param integer $id_use
	 * @return array
	 */
	public function AccountGet($id_use)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		return $p->AccountUseGet($id_use);
	}

	/**
	 * Associate current campaign with account
	 * (i.e. create/update an Account Use)
	 *
	 * @param integer $id_use
	 * @param integer $id_account
	 * @return integer
	 */
	public function AccountStore($id_use,$id_account)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		if($id_use>0)
			$p->AccountUseUpdate($id_use,$p->account_usage_types['campaign'],$this->id,$id_account);
		else
			$id_use = $p->AccountUseInsert($p->account_usage_types['campaign'],$this->id,$id_account);
		return $id_use;
	}

	/**
	 * Delete current campaign with all its signatures
	 *
	 */
	public function CampaignDelete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns" );
		$res[] = $db->query( "DELETE FROM topic_campaigns WHERE id_topic_campaign='$this->id' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_campaigns_persons" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_persons WHERE id_topic_campaign='$this->id' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_campaigns_orgs" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_orgs WHERE id_topic_campaign='$this->id' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "topic_campaigns_vips" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_vips WHERE id_topic_campaign='$this->id' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['campaign']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['campaign'],$this->id);
	}

	/**
	 * Retrieve info of current campaign
	 *
	 * @return array
	 */
	public function CampaignGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_topic,id_topic_campaign,name,UNIX_TIMESTAMP(start_date) AS start_date_ts,thanks,money,approve_comments,
			description,active,users_type,orgs_sign,id_payment_type,amount,editable,description_long,is_html,notify,id_p,promoter,notify_text,
			promoter_email,thanks_email,pre_sign,id_mp_group,signatures_per_page
		    FROM topic_campaigns WHERE id_topic_campaign=$this->id");
		return $row;
	}

	/**
	 * Create new campaign
	 *
	 * @param integer	$id_topic			Topic ID
	 * @param date		$start_date			Initial date
	 * @param string	$name				Title
	 * @param string	$keywords			Associated keywords
	 * @param string	$thanks				Message of thank shown after signing
	 * @param boolint	$money				If the campaign collects donations
	 * @param boolint	$approve_comments	If comments are moderated
	 * @param string	$description		Description
	 * @param integer	$active				Status (0=Disabled, 1=Active, 2=Over)
	 * @param integer	$users_type			Users allowed to sign (0=Registered, 1=Anybody)
	 * @param boolint	$orgs_sign			If organisations can sign
	 * @param string	$description_long	Long description
	 * @param boolint	$is_html			If long description can be htlised
	 * @param boolint	$notify				Email addresses to send signing notifications
	 * @param string	$promoter			Campaign promoter
	 * @param string	$notify_text		Text of signing notifications
	 * @param string	$promoter_email		Campaign promoter email
	 * @param string	$thanks_email		Message of thank sent to signer
	 * @param string	$pre_sign			Text shown on signature page
	 * @param integer	$signatures_per_page	Signatures per page
	 */
	public function CampaignInsert($id_topic,$start_date,$name,$keywords,$thanks,$money,$approve_comments,$description,$active,$users_type,$orgs_sign,$description_long,$is_html,$notify,$id_p,$promoter,$notify_text,$promoter_email,$thanks_email,$pre_sign,$signatures_per_page)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns" );
		$this->id = $db->nextId( "topic_campaigns", "id_topic_campaign" );
		$sqlstr = "INSERT INTO topic_campaigns (id_topic_campaign,name,id_topic,start_date,thanks,money,approve_comments,description,active,users_type,orgs_sign,description_long,is_html,notify,id_p,promoter,notify_text,promoter_email,thanks_email,pre_sign,signatures_per_page)
				VALUES ($this->id,'$name',$id_topic,'$start_date','$thanks',$money,'$approve_comments','$description','$active','$users_type','$orgs_sign','$description_long','$is_html','$notify','$id_p','$promoter','$notify_text','$promoter_email','$thanks_email','$pre_sign','$signatures_per_page')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['campaign']);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($active>0 && $t->visible)
		$s->IndexQueueAdd($o->types['campaign'],$this->id,$id_topic,$t->id_group,1);
	}

	/**
	 * Update Campaign details
	 *
	 * @param integer	$id_campaign		Campaign ID
	 * @param integer	$id_topic			Topic ID
	 * @param date		$start_date			Initial date
	 * @param string	$name				Title
	 * @param string	$keywords			Associated keywords
	 * @param string	$thanks				Message of thank shown after signing
	 * @param boolint	$money				If the campaign collects donations
	 * @param boolint	$approve_comments	If comments are moderated
	 * @param string	$description		Description
	 * @param integer	$active				Status (0=Disabled, 1=Active, 2=Over)
	 * @param integer	$users_type			Users allowed to sign (0=Registered, 1=Anybody)
	 * @param boolint	$orgs_sign			If organisations can sign
	 * @param integer	$id_payment_type	Cost centre ID
	 * @param string	$amount				Possible amounts to donate (comma-separated)
	 * @param boolint	$editable			If amount can be changed by donor
	 * @param string	$description_long	Long description
	 * @param boolint	$is_html			If long description can be htlised
	 * @param boolint	$notify				Email addresses to send signing notifications
	 * @param string	$promoter			Campaign promoter
	 * @param string	$notify_text		Text of signing notifications
	 * @param string	$promoter_email		Campaign promoter email
	 * @param string	$thanks_email		Message of thank sent to signer
	 * @param string	$pre_sign			Text shown on signature page
	 * @param integer	$signatures_per_page	Signatures per page
	 */
	public function CampaignUpdate($id_campaign,$id_topic,$start_date,$name,$keywords,$thanks,$money,$approve_comments,$description,$active,$users_type,$orgs_sign,$id_payment_type,$amount,$editable,$description_long,$is_html,$notify,$promoter,$notify_text,$promoter_email,$thanks_email,$pre_sign,$signatures_per_page)
	{
		$prev = $this->CampaignGet();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns" );
		$sqlstr = "UPDATE topic_campaigns
				SET name='$name',id_topic=$id_topic,start_date='$start_date',thanks='$thanks',money=$money,
				approve_comments='$approve_comments',description='$description',active='$active',users_type='$users_type',
				orgs_sign='$orgs_sign',id_payment_type='$id_payment_type',amount='$amount',editable='$editable',
				description_long='$description_long',is_html='$is_html',notify='$notify',promoter='$promoter',
				notify_text='$notify_text',promoter_email='$promoter_email',thanks_email='$thanks_email',pre_sign='$pre_sign',
				signatures_per_page='$signatures_per_page'
				WHERE id_topic_campaign='$id_campaign' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_campaign, $o->types['campaign']);
		$this->id = $id_campaign;
		$this->QueueAdd($id_topic);
		include_once(SERVER_ROOT."/../classes/topic.php");
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$t = new Topic($id_topic);
		if($active>0 && $t->visible)
		{
			$s->IndexQueueAdd($o->types['campaign'],$id_campaign,$id_topic,$t->id_group,1);
		}
		elseif($prev['active']>0)
		{
			$s->ResourceRemove($o->types['campaign'],$id_campaign);
		}
	}

	/**
	 * Insert donation for current campaign
	 * Store payment info
	 * Set session information for finalizing payment
	 *
	 * @param integer 	$id_p		Person ID
	 * @param integer 	$amount		Amount to donate
	 * @param integer 	$id_account	Account ID
	 * @param string 	$currency	Currency name
	 */
	public function DonationInsert($id_p,$amount,$id_account,$currency)
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$id_use = $p->AccountUseGetById(1,$this->id,$id_account);
		$db =& Db::globaldb();
		$today = $db->getTodayDate();
		$row = $this->CampaignGet();
		$id_payer = $p->PayerAdd($id_p);
		$id_payment = $p->PaymentStore(0,$today,$id_payer,$id_account,$amount,"Campaign {$row['name']}","",$row['id_payment_type'],1,0,0,0,$id_use,$currency);
		$p->SessionSet(1,$this->id,$id_payment);
	}

	/**
	 * Retrieve payment details in case a donation for this campaign
	 * is pending finalization
	 *
	 * @return array	Payment information
	 */
	public function DonationPending()
	{
		include_once(SERVER_ROOT."/../classes/payment.php");
		$p = new Payment();
		$id_payment = $p->SessionGet(1,$this->id);
		$payment = array('id_payment'=>$id_payment);
		if($id_payment>0)
		{
			$row = $p->PaymentGet($id_payment);
			$payment['amount'] = $row['amount'];
			$payment['id_currency'] = $row['id_currency'];
			$currencies = $p->currencies;
			$payment['currency'] = $currencies[$row['id_currency']];
			$payment['id_payment_type'] = $row['id_payment_type'];
			$payment['id_payer'] = $row['id_payer'];
			$payment['id_account'] = $row['id_account'];
			if($row['verified'])
			$payment['id_payment'] = 0;
		}
		return $payment;
	}

	/**
	 * Verify if signer is important
	 *
	 * @param integer $id_p
	 * @return boolean
	 */
	public function IsVip($id_p)
	{
		$row = array();
		$sqlstr = "SELECT id_p FROM topic_campaigns_vips WHERE id_topic_campaign='$this->id' AND id_p='$id_p' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->query_single($row, $sqlstr);
		return ($row['id_p']>0);
	}

	/**
	 * Search recipients of mailjob for current campaign
	 * and store them in session
	 *
	 * @param 	array 	$params	Search parameters
	 * @return 	integer			Number of recipients
	 */
	public function MailjobSearch($params)
	{
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs(5);
		$table = ($params['orgs']=="on")? "topic_campaigns_orgs" : "topic_campaigns_persons";
		if ($params['funding']=="on")
		{
			$sqlstr = "SELECT p.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.email AS mj_email,p.password AS mj_password,p.id_geo AS mj_geo
			 FROM people p 
			 INNER JOIN payers pa ON p.id_p=pa.id_p 
			 INNER JOIN payments py ON py.id_payer=pa.id_payer AND py.amount>0 
			 INNER JOIN $table tcp ON p.id_p=tcp.id_p AND tcp.id_topic_campaign='{$this->id}' AND tcp.contact=1 AND p.active=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
		}
		else
		{
			$sqlstr = "SELECT tcp.id_p AS mj_id,CONCAT(p.name1,' ',p.name2) AS mj_name,p.email AS mj_email,p.password AS mj_password,p.id_geo AS mj_geo
			 FROM $table tcp INNER JOIN people p ON tcp.id_p=p.id_p 
				WHERE tcp.id_topic_campaign='{$this->id}' AND tcp.contact=1 AND p.active=1 AND p.email_valid=1 AND p.bounces<{$mj->bounces_threshold} ";
		}
		if (strlen($params['name'])>0)
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%') ";
		if (strlen($params['email'])>0)
			$sqlstr .= " AND (p.email LIKE '%{$params['email']}%') ";
		if ($params['id_geo']>0)
			$sqlstr .= " AND p.id_geo='{$params['id_geo']}' ";
		$sqlstr .= " GROUP BY p.id_p ORDER BY p.name1 ";
		return $mj->RecipientsSet($sqlstr,true,true);
	}

	/**
	 * Promoter name and email for campaign notifications
	 * 
	 * @param array $campaign
	 * @param Topic $topic
	 * @return array 
	 */
	public function CampaignPromoter($campaign,$topic)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		$sender_name = $topic->name;
		$sender_email = $topic->row['temail']!=""? $topic->row['temail'] : $mail->staff_email;
		
		if($campaign['promoter']!="" && $campaign['promoter_email']!="" && $mail->CheckEmail($campaign['promoter_email']))
		{
			$sender_email = $campaign['promoter_email'];
			$sender_name = $campaign['promoter'];
		}
		elseif($campaign['id_p']>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$promoter = $pe->UserGetById($campaign['id_p']);
			if($mail->CheckEmail($promoter['email']))
			{
				$sender_email = $promoter['email'];
				$sender_name = $promoter['name1'] . " " . $promoter['name2'];
			}
		}
		return array('name'=>$sender_name,'email'=>$sender_email);
	}
	
	/**
	 * Send notification of current campaign signature
	 *
	 * @param string $signer_name
	 * @param string $signer_email
	 * @param integer $id_p
	 * @param integer $id_geo
	 */
	public function NotifySignature($signer_name,$signer_email,$id_p,$id_geo)
	{
		$campaign = $this->CampaignGet();
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/irl.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($campaign['id_topic']);
		$mail = new Mail();
		$irl = new IRL();
		$url = $irl->PublicUrlGlobal("campaign",array('id'=>$this->id,'id_topic'=>($campaign['id_topic'])));
		$subject = $campaign['name'];

		$promoter = $this->CampaignPromoter($campaign,$t);
		
		if($campaign['thanks_email']!="")
		{
			$message = $campaign['thanks_email'] . "\n\n" . $campaign['name'] . "\n" . $url;
			$extra = array('name'=>$promoter['name'],'email'=>$promoter['email']);
			$mail->SendMail($signer_email,$signer_name,$subject,$message,$extra);			
		}

		if($campaign['notify_text']!="")
		{
			$recipients = $mail->AddressSplitAndValidate($campaign['notify']);
			include_once(SERVER_ROOT."/../classes/modules.php");
			if(count($recipients)>0)
			{
				$tr = new Translator($t->id_language,4,false,$t->id_style);
				$message = $campaign['notify_text'] . "\n\n$signer_name\n$signer_email\n\n-----------\n" . $tr->TranslateParams("notification_footer",array($campaign['name'],$url,"{$promoter['name']} <{$promoter['email']}>"));
				$extra = array('name'=>$signer_name,'email'=>$signer_email);
				foreach($recipients as $recipient)
				{
					$mail->SendMail($recipient,"",$subject,$message,$extra,false,array(),false);
				}
			}
		}
	}

	/**
	 * Verify if organisation has already signed current campaign
	 *
	 * @param integer $id_p
	 * @return boolean
	 */
	private function OrgCheck($id_p)
	{
		$check = false;
		$row = array();
		$sqlstr = "SELECT COUNT(tco.id_org) AS counter
			FROM topic_campaigns_orgs tco
			WHERE tco.id_topic_campaign='$this->id' AND tco.id_p='$id_p' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->query_single($row, $sqlstr);
		if ($row['counter']>0)
			$check = true;
		return $check;
	}

	/**
	 * Delete organisation signature from current campaign
	 *
	 * @param integer $id_org
	 */
	public function OrgDelete( $id_org )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns_orgs" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_orgs WHERE id_topic_campaign=$this->id AND id_org=$id_org" );
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve organisation
	 *
	 * @param integer $id_org
	 * @return array
	 */
	public function OrgGet( $id_org )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(tco.insert_date) AS insert_date,tco.id_topic_campaign,
				p.name1 AS name,p.address,tco.id_p,
				p.id_geo,p.phone,p.email,tco.comments,tco.comment_approved,tco.ip,tco.contact
				FROM topic_campaigns_orgs tco
				INNER JOIN people p ON tco.id_p=p.id_p
				WHERE id_org=$id_org";
		$db->query_single($row, $sqlstr);
		if($row['id_p']>0)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$params = $pe->UserParamsGet($row['id_p']);
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$deparams = $v->Deserialize($params['params']);
			$row['person'] = $deparams['contact'];
			$row['web'] = $deparams['web'];
		}
		return $row;
	}

	/**
	 * Retrieve org by people ID
	 *
	 * @param integer $id_p
	 * @return array
	 */
	public function OrgGetById( $id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(tco.insert_date) AS insert_date,tco.id_topic_campaign,
			CONCAT(p.name1,' ',p.name2) AS name,p.name1,p.name2,
			p.email,p.id_geo,pp.params,tco.comments,p.id_p,
			tco.comment_approved,tco.ip,tco.contact,tco.id_org
			FROM topic_campaigns_orgs tco
			INNER JOIN people p ON tco.id_p=p.id_p
			INNER JOIN people_params pp ON p.id_p=pp.id_p
			WHERE tco.id_p='$id_p' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Add organisation signer to current campaign
	 *
	 * @param integer $id_p
	 * @param string $comments
	 * @param boolint $comment_approved
	 * @param boolint $contact
	 * @return boolean	A new signer has been inserted
	 */
	public function OrgInsert($id_p,$comments,$comment_approved,$contact)
	{
		if($id_p>0 && ($this->OrgCheck($id_p) || $this->PersonCheck($id_p)))
		{		
			return false;
		}
		else
		{
			$db =& Db::globaldb();
			$today_time = $db->GetTodayTime();
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$ip = $v->IP();
			$db->begin();
			$db->lock( "topic_campaigns_orgs" );
			$id_org = $db->nextId( "topic_campaigns_orgs", "id_org" );
			$sqlstr = "INSERT INTO topic_campaigns_orgs (id_org,comments,comment_approved,id_topic_campaign,insert_date,ip,contact,id_p)
					VALUES ($id_org,'$comments',$comment_approved,$this->id,'$today_time','$ip',$contact,'$id_p')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			return true;
		}
	}

	/**
	 * Update organisation signature
	 *
	 * @param integer $id_org
	 * @param string $comments
	 * @param boolint $comment_approved
	 * @param boolint $contact
	 */
	public function OrgUpdate($id_org,$comments,$comment_approved,$contact)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns_orgs" );
		$sqlstr = "UPDATE topic_campaigns_orgs
				SET comments='$comments',comment_approved='$comment_approved',contact='$contact'
				WHERE id_org='$id_org' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Retrieve approved signer organisations (paginated)
	 *
	 * @param array $rows
	 * @param boolint $approved
	 * @param boolean 	$paged		Paginate results
	 * @return integer		Num of signers
	 */
	public function OrgsApprove( &$rows, $approved, $paged=true )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$gjoin = $geo->GeoJoin("p.id_geo");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_org,p.name1 AS name,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,p.email,comments,'org' AS item_type,tco.contact,$gjoin[name] AS geo_name,p.id_geo
			FROM topic_campaigns_orgs tco 
			INNER JOIN people p ON tco.id_p=p.id_p $gjoin[join] 
			WHERE id_topic_campaign=$this->id AND comment_approved=$approved ORDER BY insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Remove signer from current campaign
	 *
	 * @param integer $id_person
	 */
	public function PersonDelete( $id_person )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns_persons" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_persons WHERE id_topic_campaign='$this->id' AND id_person='$id_person' " );
		Db::finish( $res, $db);
	}

	/**
	 * Verify if person has already signed current campaign
	 *
	 * @param integer $id_p
	 * @return boolean
	 */
	public function PersonCheck($id_p)
	{
		$check = false;
		$row = array();
		$sqlstr = "SELECT COUNT(tcp.id_person) AS counter
			FROM topic_campaigns_persons tcp
			WHERE tcp.id_topic_campaign='$this->id' AND tcp.id_p='$id_p' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->query_single($row, $sqlstr);
		if ($row['counter']>0)
			$check = true;
		return $check;
	}

	/**
	 * Retrieve signer by person ID
	 *
	 * @param integer $id_person
	 * @return array
	 */
	public function PersonGet( $id_person )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(tcp.insert_date) AS insert_date,tcp.id_topic_campaign,
			CONCAT(p.name1,' ',p.name2) AS name,p.name1,p.name2,
			p.email,p.id_geo,pp.params,tcp.comments,p.id_p,
			tcp.comment_approved,tcp.ip,tcp.contact
			FROM topic_campaigns_persons tcp
			INNER JOIN people p ON tcp.id_p=p.id_p
			INNER JOIN people_params pp ON p.id_p=pp.id_p
			WHERE tcp.id_person='$id_person' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Retrieve signer by people ID
	 *
	 * @param integer $id_p
	 * @return array
	 */
	public function PersonGetById( $id_p)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(tcp.insert_date) AS insert_date,tcp.id_topic_campaign,
			CONCAT(p.name1,' ',p.name2) AS name,p.name1,p.name2,
			p.email,p.id_geo,pp.params,tcp.comments,p.id_p,
			tcp.comment_approved,tcp.ip,tcp.contact,tcp.id_person
			FROM topic_campaigns_persons tcp
			INNER JOIN people p ON tcp.id_p=p.id_p
			INNER JOIN people_params pp ON p.id_p=pp.id_p
			WHERE tcp.id_p='$id_p' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	/**
	 * Create a new user
	 *
	 * @param string	$name
	 * @param string	$surname
	 * @param string	$email
	 * @param integer	$id_geo
	 * @param array		$unescaped_params
	 * @param boolint	$contact
	 * @param integer	$id_topic
	 * @param boolean	$verify_email
	 * @param string	$phone
	 * @param string	$address
	 * @return integer				New people ID
	 */
	public function UserCreate($name,$surname,$email,$id_geo,$unescaped_params,$contact,$id_topic,$verify_email=false,$phone="",$address="")
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$contact_topic = 0;
		$contact_portal = 0;
		if($contact)
		{
			$contact_portal = 1;
			if($id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				if($t->profiling)
				{
					$contact_topic = 1;
				}
			}
		}
		$id_p = $pe->UserCreate($name,$surname,$email,$id_geo,$contact_portal,$unescaped_params,$verify_email,$id_topic,$contact_topic);
		if($phone!="" || $address != "")
			$pe->UserUpdateDetails($id_p,$address,$phone);
		return $id_p;
	}

	/**
	 * Add signer to current campaign
	 *
	 * @param integer 		$id_p
	 * @param string 		$comments
	 * @param boolint 		$comment_approved
	 * @param boolint 		$contact
	 * @param timestamp 	$insert_ts
	 * @return boolean		A new signer has been inserted
	 */
	public function PersonInsert($id_p,$comments,$comment_approved,$contact,$insert_ts=0)
	{
		if($id_p>0 && ($this->PersonCheck($id_p) || $this->OrgCheck($id_p)))
		{
			return false;
		}
		else
		{
			$db =& Db::globaldb();
			$today_time = $db->GetTodayTime($insert_ts);
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$ip = $v->IP();
			$db->begin();
			$db->lock( "topic_campaigns_persons" );
			$nextid = $db->nextId( "topic_campaigns_persons", "id_person" );
			$sqlstr = "INSERT INTO topic_campaigns_persons (id_person,comments,comment_approved,id_topic_campaign,insert_date,ip,contact,id_p)
					VALUES ($nextid,'$comments',$comment_approved,$this->id,'$today_time','$ip','$contact','$id_p')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			return true;
		}
	}

	/**
	 * Update signer comment and preferences
	 *
	 * @param integer $id_person
	 * @param string $comments
	 * @param boolint $comment_approved
	 * @param boolint $contact
	 */
	public function PersonUpdate($id_person,$comments,$comment_approved,$contact)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns_persons" );
		$sqlstr = "UPDATE topic_campaigns_persons
				SET comments='$comments',comment_approved=$comment_approved,contact=$contact
				WHERE id_person='$id_person' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Approved signers (paginated) of current campaign
	 *
	 * @param array 	$rows		Results
	 * @param boolint	$approved	Whether approved or not
	 * @param boolean 	$paged		Paginate results
	 * @return integer				Num of signers
	 */
	public function PersonsApprove( &$rows, $approved, $paged=true )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$gjoin = $geo->GeoJoin("p.id_geo");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT tcp.id_person,CONCAT(p.name1,' ',p.name2) AS name,
			UNIX_TIMESTAMP(tcp.insert_date) AS insert_date_ts,$gjoin[name] AS geo_name,
			p.email,tcp.comments,tcp.comment_approved,'person' AS item_type,tcp.contact,p.id_geo
			FROM topic_campaigns_persons tcp
			INNER JOIN people p ON tcp.id_p=p.id_p $gjoin[join] 
			WHERE tcp.id_topic_campaign='$this->id' AND tcp.comment_approved='$approved' 
			ORDER BY insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Number of approved signers of current campaign
	 *
	 * @param boolint $approved
	 * @return integer
	 */
	public function PersonsApproveCounter( $approved)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT COUNT(tcp.id_person) AS counter
			FROM topic_campaigns_persons tcp
			WHERE tcp.id_topic_campaign='$this->id' AND tcp.comment_approved='$approved' ";
		$db->query_single($row, $sqlstr);
		return $row['counter'];
	}

	/**
	 * Add current campaign's subtopic to publishing queue
	 *
	 * @param integer $id_topic
	 */
	private function QueueAdd($id_topic)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$subtopic = $this->Subtopic();
		if ($subtopic['id_subtopic']>0)
		{
			$t = new Topic($id_topic);
			$t->queue->JobInsert($t->queue->types['subtopic'],$subtopic['id_subtopic'],"update");
		}
	}

	/**
	 * Full-text search of campaign signers
	 *
	 * @param array 	$rows	Results
	 * @param array 	$params	Search paramenters
	 * @param boolean 	$paged	Paginate results
	 * @return integer			Num of signers
	 */
	public function Search( &$rows, $params, $paged )
	{
		$table = ($params['type']=="0")? "topic_campaigns_persons" : "topic_campaigns_orgs";
		$id = ($params['type']=="0")? "id_person" : "id_org";
		$db =& Db::globaldb();
		$rows = array();
		if ($params['emails']=="on")
		{
			$sqlstr = "SELECT DISTINCT p.email";		
		}
		else
		{
			$sqlstr = "SELECT tcp.$id,CONCAT(p.name1,' ',p.name2) AS name,UNIX_TIMESTAMP(tcp.insert_date) AS insert_date_ts,
				p.email,tcp.comments,tcp.contact";		
		}
		$sqlstr .= " FROM $table tcp INNER JOIN people p ON tcp.id_p=p.id_p ";
		if ($params['vip']=="on")
		{
			$sqlstr .= " INNER JOIN topic_campaigns_vips tcv ON tcp.id_p=tcv.id_p ";		
		}
		$sqlstr .= " WHERE tcp.id_topic_campaign='{$params['id_campaign']}' ";
		if (strlen($params['name'])>0)
		{
			$sqlstr .= " AND (p.name1 LIKE '%{$params['name']}%' OR p.name2 LIKE '%{$params['name']}%') ";
		}
		if ($params['id_geo'] > 0)
		{
			$sqlstr .= " AND p.id_geo={$params['id_geo']}";
		}
		if (strlen($params['email']) > 0)
		{
			$sqlstr .= " AND p.email LIKE '%{$params['email']}%'";
		}
		if (strlen($params['comments']) > 0)
		{
			$sqlstr .= " AND tcp.comments LIKE '%{$params['comments']}%'";
		}
		if ($params['vip']=="on")
		{
		    $sqlstr .= " AND tcv.id_topic_campaign='{$params['id_campaign']}' ";
		}
		if ($params['emails']=="on")
		{
			$sqlstr .= " AND tcp.contact=1";
		}
		$sqlstr .= "  ORDER BY insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Set that current user has signed current campaign
	 *
	 */
	public function SignatureSessionSet()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$session->Set($this->session_var,1);
	}

	/**
	 * Change current campaign status
	 *
	 * @param integer $status
	 */
	public function StatusChange($status)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns" );
		$sqlstr = "UPDATE topic_campaigns SET active='$status' WHERE id_topic_campaign='$this->id' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Subtopic associated to current campaign
	 *
	 * @return integer	Subtopic ID
	 */
	private function Subtopic()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT s.id_subtopic,s.name,s.header,s.footer FROM subtopics s
			WHERE s.id_type=7 AND s.id_item='$this->id' ");
		return $row;
	}

	/**
	 * Total donations by a person for current campaign
	 *
	 * @param integer $id_p
	 * @return integer
	 */
	public function SupportByPerson($id_p)
	{
		$sqlstr = "SELECT p.amount FROM payments p
			INNER JOIN accounts_use au ON p.id_use=au.id_use AND au.type=2 AND au.id_item='$this->id' 
			INNER JOIN payers pa ON p.id_payer=pa.id_payer AND pa.id_p='$id_p' ";
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		return (int)$row['amount'];
	}

	/**
	 * Total donations of current campaign
	 *
	 * @return integer
	 */
	public function SupportTotal()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT SUM(p.amount) AS total FROM payments p 
			INNER JOIN accounts_use au ON p.id_use=au.id_use AND au.type=2 AND au.id_item='$this->id' ");
		return (int)$row['total'];
	}

	/**
	 * Retrieve all important signatures of current campaign
	 *
	 * @return array
	 */
	public function Vips()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT CONCAT(p.name1,' ',p.name2) AS name,p.id_p,tco.id_p AS oid_p 
			FROM people p
			INNER JOIN topic_campaigns_vips tcv ON p.id_p=tcv.id_p AND tcv.id_topic_campaign='$this->id'
            LEFT JOIN topic_campaigns_orgs tco ON p.id_p=tco.id_p 
            GROUP BY tcv.id_p  
			ORDER BY oid_p DESC, name ASC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Set a person signature importance
	 *
	 * @param integer $id_p
	 * @param boolint $is_vip
	 */
	public function VipSet($id_p,$is_vip)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_campaigns_vips" );
		$res[] = $db->query( "DELETE FROM topic_campaigns_vips WHERE id_topic_campaign=$this->id AND id_p='$id_p' " );
		Db::finish( $res, $db);
		if($is_vip)
		{
			$db->begin();
			$db->lock( "topic_campaigns_vips" );
			$sqlstr = "INSERT INTO topic_campaigns_vips (id_topic_campaign,id_p) VALUES ($this->id,'$id_p')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
}
?>
