# PhPeace 

PeaceLink Portal Management System

## Release

- run `dev/scripts/phdistrib.sh`
- upload the `update_XYZ.tar.gz` file to PhPeace server
- Start next release
  - update build number in `classes/phpeace.php#L49`
  - update version number in `classes/phpeace.php#L56`
  - push

## Installation

Currently on PHP 8.2

Check PHP version `update-alternatives --list php` and in case set it `update-alternatives --config php`

Packages required: `apt install php8.2 libapache2-mod-php8.2 php8.2-mysql php8.2-xsl php8.2-curl php8.2-imagick php8.2-imap php8.2-cli php8.2-common php8.2-opcache php8.2-phpdbg php8.2-readline php8.2-xml libapache2-mod-php8.2 php8.2-mbstring php8.2-soap`

### php.ini

```
short_open_tag = On
display_errors = Off
error_log = /tmp/php_errors.log
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING & ~E_NOTICE
```
