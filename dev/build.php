<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

abstract class Build
{
	public static function MainDBSchema($tables_to_exclude)
	{
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$db->Schema("install/dbschema.sql",$tables_to_exclude);
	}

	public static function InitScript()
	{
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$tables = array("geo_countries","relations","prov","reg","cron","uk_countries","uk_counties","eire_counties");
		$db->Dump("install/init.sql",$tables,FALSE,TRUE);
		include_once(SERVER_ROOT."/../classes/modules.php");
		$modules = Modules::AvailableModules(false);
		$active_modules = self::DefaultModules();
		$sqlstr = "\nINSERT INTO `modules` (`id_module`, `path`, `sort`, `restricted`, `layout`, `internal`, `active`, `admin`, `global`) VALUES ";
		$inserts = array();
		foreach($modules as $module)
		{
			$mod_sql = "({$module['id_module']},'{$module['path']}',{$module['sort']},{$module['restricted']},{$module['layout']},{$module['internal']},";
			$mod_sql .= in_array($module['path'],$active_modules)? "1":"0";
			$mod_sql .= ",{$module['admin']},{$module['global']})";
			$inserts[] = $mod_sql;
		}
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->WritePage("install/init.sql",$sqlstr . implode(",",$inserts) . ";","a");
	}
	
	private static function DefaultModules()
	{
		$modules = array();
		$modules[] = "users";
		$modules[] = "mail";
		$modules[] = "homepage";
		$modules[] = "topics";
		$modules[] = "articles";
		$modules[] = "quotes";
		$modules[] = "galleries";
		$modules[] = "layout";
		$modules[] = "events";
		$modules[] = "ontology";
		$modules[] = "admin";
		$modules[] = "phpeace";
		$modules[] = "gate";
		$modules[] = "stats";
		$modules[] = "banners";
		$modules[] = "people";
		return $modules;
	}
	
	public static function DumpTables($tables,$name)
	{
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$db->Dump("install/{$name}.sql",$tables,true,false);
	}
}
?>
