#! /bin/sh
#
# =============================
# BUILD CONFIGURATION
# This is an example file.
# If you want to create builds and updates,
# copy this file in your custom directory,
# rename it buildconfig.sh
# and customize the settings below
# =============================

# Git repository 
GITREPO="https://github.com/peacelink/phpeace"

# Git branch
GITBRANCH="master"

# If you want to get a specific revision, uncomment this
# REVISION="2576"

# Directory where packages will be dropped
# Must be writable by user running this script
DESTDIR="/var/www/phpeace_builds"

# Directory used for temporary files
# Must be writable by user running this script
TEMPDIR="/tmp/phpeace_temp"

# Apache user and group
# www-data for Debian / Ubuntu
# apache for Gentoo
APACHE_USER="www-data"
APACHE_GROUP="www-data"

# Host to upload the installer package
# Must be configured in .ssh/config
# 
DISTRIBHOST="host.phpeace.org"
