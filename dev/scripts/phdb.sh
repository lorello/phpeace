#!/bin/bash
#
# Rebuilds the DB schema and the initialization SQL scripts for new installations
#
#########################

CURRENTDIR=`dirname $0`
cd $CURRENTDIR
php $CURRENTDIR/phdb.php
