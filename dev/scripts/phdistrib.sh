#!/bin/bash
#
# Build script
# Creates installer and update package for current version
#
# If "test" is passed as the first argument, it runs as a unit test
# If "tag" is passed as the first argument, it creates a tag for current version

# Load configuration
CURRENTDIR=`dirname $0`
CONFIGFILE="$CURRENTDIR/../../custom/buildconfig.sh"
if [ ! -f "$CONFIGFILE" ]; then echo "ERROR: Missing configuration file $CONFIGFILE"; exit 1; fi
source $CONFIGFILE

if [ "$1" == "test" ]; then
	if [ -d "$DESTDIR/testing" ]; then rm -f -r "$DESTDIR/testing"; fi
	mkdir "$DESTDIR/testing"
	DESTDIR="$DESTDIR/testing"
fi

# Make some configuration checks
if [ ! -d "$DESTDIR" ]; then echo "ERROR: Missing destination directory $DESTDIR"; exit 1; fi
if [ ! -w "$DESTDIR" ]; then echo "ERROR: Destination directory $DESTDIR is not writable by user $USER"; exit 1; fi
if [ ! -n "$GITREPO" ]; then echo "ERROR: Git repository is not set"; exit 1; fi
if [ ! -d "$DESTDIR/updates" ]; then mkdir "$DESTDIR/updates"; fi

# Create temporary directory
if [ -d "$TEMPDIR" ]; then echo "ERROR: Temporary directory $TEMPDIR already exists, please remove it"; exit 1; fi
mkdir $TEMPDIR
if [ ! -d "$TEMPDIR" ]; then echo "ERROR: Could not create temporary directory $TEMPDIR"; exit 1; fi

if [ "$1" == "test" ]; then
	cp -r $CURRENTDIR/../../classes $TEMPDIR
	cp -r $CURRENTDIR/../../install $TEMPDIR
	cp -r $CURRENTDIR/../../admin $TEMPDIR
	cp -r $CURRENTDIR/../../dev $TEMPDIR
	cp -r $CURRENTDIR/../../lang $TEMPDIR
	cp -r $CURRENTDIR/../../modules $TEMPDIR
	cp -r $CURRENTDIR/../../others $TEMPDIR
	cp -r $CURRENTDIR/../../scripts $TEMPDIR
	cp -r $CURRENTDIR/../../tests $TEMPDIR
	cp -r $CURRENTDIR/../../wsdl $TEMPDIR
	mkdir $TEMPDIR/xsl
	cp -r $CURRENTDIR/../../xsl/0 $TEMPDIR/xsl
	mkdir $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/books $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/campaign $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/dodc $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/mastodon $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/ecomm $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/events $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/forum $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/galleries $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/js $TEMPDIR/pub
	rm -r $TEMPDIR/pub/js/custom
	cp -r $CURRENTDIR/../../pub/lists $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/logos $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/media $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/meet $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/orgs $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/phpeace $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/poll $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/quotes $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/search $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/tools $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/users $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/uss $TEMPDIR/pub
	cp -r $CURRENTDIR/../../pub/widgets $TEMPDIR/pub
	mkdir $TEMPDIR/pub/css
	cp -r $CURRENTDIR/../../pub/css/0 $TEMPDIR/pub/css
else
	# Git options
	GIT_OPTIONS="--depth=1"

	if [ -n "$REVISION" ]; then
		GIT_OPTIONS="-r $REVISION $GIT_OPTIONS"
	fi

	# Get code from source control
	git clone $GIT_OPTIONS $GITREPO $TEMPDIR

	if [ ! -d "$TEMPDIR/classes" ]; then echo "Could not retrieve source code from repository $GITREPO"; exit 1; fi
fi

cd $TEMPDIR
mkdir custom
mv $TEMPDIR/install/*.php $TEMPDIR/custom

# Fix executable permissions
chmod 744 $TEMPDIR/dev/scripts/*.sh
for scriptdir in `ls $TEMPDIR/scripts`
do
	chmod 744 $TEMPDIR/scripts/$scriptdir/*.sh
done

# Find out current version and build number
VERSION=`$TEMPDIR/dev/scripts/phversion.sh`
BUILD=`$TEMPDIR/dev/scripts/phbuild.sh`
UPDATE="update_$BUILD"

if [[ -n "$UPDATE" && -n "$VERSION" ]]; then

	# BUILD UPDATE
	
	mv xsl/0 xsl0
	mv pub/css/0 css0
	rm -r $TEMPDIR/pub/css
	
	tar -Pcf $UPDATE.tar --exclude admin/js/custom.js   --exclude admin/include/css/custom.css  --exclude admin/include/css/article.css admin
	tar -Prf $UPDATE.tar modules
	tar -Prf $UPDATE.tar lang
	tar -Prf $UPDATE.tar --exclude others/maxmind/geoip.dat others
	tar -Prf $UPDATE.tar classes
	tar -Prf $UPDATE.tar scripts
	tar -Prf $UPDATE.tar wsdl
	tar -Prf $UPDATE.tar xsl0
	
	cd $TEMPDIR/pub
	find * -maxdepth 0 -type d -print | while read x; 
	do 
		if [[ $x != "phpeace" && $x != "tools"  && $x != "logos" && $x != "js" ]]; then
			mv $x _$x;
		fi
	done 
	cd $TEMPDIR
	
	mv install/COPYING $TEMPDIR
	mv install/AUTHORS.md $TEMPDIR
	mv install/OTHERS $TEMPDIR
	mv install/README.md $TEMPDIR
	tar -Prf $UPDATE.tar COPYING
	tar -Prf $UPDATE.tar AUTHORS.md
	tar -Prf $UPDATE.tar OTHERS
	tar -Prf $UPDATE.tar README.md
	
	if [ "$(id -u)" == "0" ]; then
		chown -R $APACHE_USER:$APACHE_GROUP $TEMPDIR
	fi
	tar -Prf $UPDATE.tar --exclude pub/index.html --exclude pub/robots.txt --exclude pub/phpeace/install.php --exclude pub/phpeace/info.php pub
	gzip -f $UPDATE.tar
	
	mv $UPDATE.tar.gz $DESTDIR/updates
	
	echo "Update file for $VERSION: $DESTDIR/updates/$UPDATE.tar.gz"

	# BUILD INSTALLER
	
	INSTALL="phpeace_$VERSION"
	
	cd $TEMPDIR
	mv install/htaccess $TEMPDIR/pub/.htaccess
	
	rm -r $TEMPDIR/xsl
	rm -r $TEMPDIR/dev
	rm -r $TEMPDIR/tests
		
	if [ "$(id -u)" == "0" ]; then
		chown -R $APACHE_USER:$APACHE_GROUP $TEMPDIR
	fi
	
	tar -Pcf $INSTALL.tar *
	gzip -f $INSTALL.tar
	
	mv $INSTALL.tar.gz $DESTDIR
	
	rm -f -r $TEMPDIR

	echo "Installer for $VERSION: $DESTDIR/$INSTALL.tar.gz"
	
	# Version tag
	if [[ "$1" == "tag" ]]; then
		cd $CURRENTDIR/../../ 
		echo "Creating tag for version $VERSION"
		git tag -a $VERSION -m "Tag $VERSION (build $BUILD)"
		git push --tags
		scp $DESTDIR/$INSTALL.tar.gz $DISTRIBHOST:/data/phpeace/phpeace/distrib/
		# change symlink 
		# upload release https://gitlab.com/peacelink/phpeace/-/issues/11
	fi
fi


