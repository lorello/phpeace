#! /bin/sh
#
# Runs all Unit tests
#
#########################

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

phpunit --stderr $CURRENTDIR/../../tests/cases/alltests.php
