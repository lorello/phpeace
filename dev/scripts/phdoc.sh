#! /bin/sh
#
# Creates documentation using PhPDocumentor
#

# The directory where documentation will be saved
TARGETDIR="~/temp/doc"

#########################


CURRENTDIR=`dirname $0`

mkdir $TARGETDIR
phpdoc -dn PhPeace -o HTML:frames:phpedit -d $CURRENTDIR/../../classes -t ~/temp/doc

