<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************-->

<xsl:import href="common.xsl" />

<xsl:output method="html" encoding="UTF-8" indent="no" doctype-system="http://www.w3.org/TR/html4/strict.dtd"  doctype-public="-//W3C//DTD HTML 4.01//EN" />

<xsl:include href="common_global.xsl" />

<xsl:variable name="current_page_title" select="/root/keyword/@k"/>

<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
	<xsl:if test="/root/keyword">
		<div id="keyword">
			<h1><xsl:value-of select="/root/keyword/@k"/></h1>
			<xsl:if test="/root/keyword/@description != ''">
				<div class="description"><xsl:value-of select="/root/keyword/@description"/></div>
			</xsl:if>
			<xsl:call-template name="items">
				<xsl:with-param name="root" select="/root/keyword/articles"/>
				<xsl:with-param name="node" select="/root/keyword"/>
			</xsl:call-template>
		</div>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

