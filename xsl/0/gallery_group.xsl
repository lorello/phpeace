<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************-->

<xsl:import href="common.xsl" />

<xsl:output method="html" encoding="UTF-8" indent="no" doctype-system="http://www.w3.org/TR/html4/strict.dtd"  doctype-public="-//W3C//DTD HTML 4.01//EN" />

<xsl:include href="common_global.xsl" />

<xsl:variable name="current_page_title">
<xsl:value-of select="/root/tree/@name"/>
<xsl:if test="/root/gallery/@name"> - <xsl:value-of select="/root/gallery/@name"/></xsl:if>
</xsl:variable>


<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
<xsl:call-template name="breadcrumb"/>
<xsl:choose>
<xsl:when test="$subtype='group'">
<xsl:call-template name="galleries">
<xsl:with-param name="node" select="/root/group/galleries"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="$subtype='gallery'">
<xsl:call-template name="items">
<xsl:with-param name="root" select="/root/gallery/images"/>
<xsl:with-param name="node" select="/root/gallery"/>
</xsl:call-template>
<xsl:call-template name="licence">
<xsl:with-param name="i" select="/root/gallery"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="$subtype='slideshow'">
<xsl:call-template name="gallerySlideshow">
<xsl:with-param name="node" select="/root/gallery"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="$subtype='image'">
<xsl:call-template name="galleryImageItem">
<xsl:with-param name="i" select="/root/gallery/image"/>
</xsl:call-template>
</xsl:when>
</xsl:choose>
</xsl:template>


<!-- ###############################
     GALLERY [summary]
     ############################### -->
<xsl:template match="gallery" mode="summary">
<div id="gallery">
<h2>
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="."/>
<xsl:with-param name="condition" select="$subtype!='gallery'"/>
</xsl:call-template>
</h2>
<xsl:if test="description!=''">
<div class="gallery-description"><xsl:value-of select="description" disable-output-escaping="yes"/></div>
</xsl:if>
<xsl:if test="@date!='' ">
<div class="gallery-date"><xsl:value-of select="concat(key('label','date')/@tr,': ',@date)"/></div>
</xsl:if>
<xsl:if test="@author!='' ">
<div class="gallery-author"><xsl:value-of select="concat(key('label','author')/@tr,': ',@author)"/></div>
</xsl:if>
<xsl:if test="@counter!='' ">
<div class="notes"><xsl:value-of select="concat(@counter,' ',key('label','images')/@tr)"/></div>
</xsl:if>
<h3 class="slideshow">
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="slideshow"/>
<xsl:with-param name="name" select="slideshow/@label"/>
</xsl:call-template>
</h3>
<xsl:call-template name="licenceInfo">
<xsl:with-param name="i" select="."/>
</xsl:call-template>
</div>
</xsl:template>


<!-- ###############################
     GALLERIES
     ############################### -->
<xsl:template name="galleries">
<xsl:param name="node"/>
<ul class="galleries">
<xsl:for-each select="$node/gallery">
<li>
<xsl:call-template name="galleryItem">
<xsl:with-param name="i" select="."/>
<xsl:with-param name="with_details" select="true()"/>
</xsl:call-template>
</li>
</xsl:for-each>
</ul>
</xsl:template>


<!-- ###############################
     LEFT BAR
     ############################### -->
<xsl:template name="leftBar">
<xsl:if test="$subtype!='group' ">
<xsl:apply-templates mode="summary" select="/root/gallery"/>
</xsl:if>
<h2>
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="/root/tree"/>
</xsl:call-template>
</h2>
<xsl:apply-templates select="/root/tree/groups"/>
<xsl:call-template name="leftBottom"/>
</xsl:template>


<!-- ###############################
      MENU ITEM
      ############################### -->
<xsl:template mode="groupitem" match="group">
<xsl:param name="level"/>
<li><xsl:attribute name="class">level<xsl:value-of select="$level"/><xsl:if test="@id = /root/group/@id or @id=/root/gallery/@id_group"><xsl:text> </xsl:text>selected</xsl:if></xsl:attribute>
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="."/>
<xsl:with-param name="name" select="@name"/>
</xsl:call-template>
<xsl:if test="groups">
<xsl:apply-templates select="groups">
<xsl:with-param name="level" select="$level + 1"/>
</xsl:apply-templates>
</xsl:if>
</li>
</xsl:template>


<!-- ###############################
      MENU ITEMS
      ############################### -->
<xsl:template match="groups">
<xsl:param name="level" select="'1'"/>
<ul class="groups">
<xsl:apply-templates mode="groupitem">
<xsl:with-param name="level" select="$level"/>
</xsl:apply-templates>
</ul>
</xsl:template>


<!-- ###############################
     GALLERY SLIDESHOW
     ############################### -->
<xsl:template name="gallerySlideshow">
<xsl:param name="node"/>
<xsl:call-template name="slideshow">
<xsl:with-param name="id" select="$node/@id"/>
<xsl:with-param name="width" select="$node/@width"/>
<xsl:with-param name="height" select="$node/@height"/>
<xsl:with-param name="images" select="$node/@xml"/>
<xsl:with-param name="watermark"><xsl:if test="$node/@watermark"><xsl:value-of select="$node/@watermark"/></xsl:if></xsl:with-param>
<xsl:with-param name="shuffle" select="$node/@shuffle"/>
<xsl:with-param name="bgcolor" select="'0x000000'"/>
<xsl:with-param name="jscaptions" select="true()"/>
</xsl:call-template>
<div id="slide-caption" name="slide-caption" class="gallery-image"></div>
</xsl:template>



</xsl:stylesheet>
