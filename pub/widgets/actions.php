<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/session.php");    	
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$session = new Session();
$wi = new Widgets();

$fh = new FormHelper(true,32,false);
$post = $fh->HttpPost();    

$from = isset($post['from']) ? $post['from'] : '';

$id_p = $wi->WidgetUserGet();

$conf = new Configuration();
$pub_widget = '';
if ($conf->Get("upload_host") != '')
{
    include_once(SERVER_ROOT."/../classes/irl.php");
    $irl = new IRL();
    $pub_widget = $irl->PublicUrlGlobal('widget',array()) . "widgets/";
}

if ($from == "create_rss_widget")
{
	// create RSS widget
	include_once(SERVER_ROOT."/../classes/web_feeds.php");
	$wf = new WebFeeds();
	
	$url = "create.php";
	if ($id_p > 0)
	{
		$id_widget = 0;
        // strip html tag of RSS widget title, description
		$title = strip_tags(html_entity_decode($post['widget_rss_title']));
		$description = strip_tags(html_entity_decode($post['widget_rss_desc']));
		$description = strip_tags(html_entity_decode($post['widget_rss_desc']));
		$content = '';
		$feed_url = $post['widget_rss_url'];
		$rss_layout = 1;
		$rss_items_display = 5;
		$id_web_feed = 0;
		$widget_type = WIDGET_TYPE_RSS;

		$session->Set("title",stripslashes($title));
		$session->Set("description",stripslashes($description));   
		$session->Set("url",stripslashes($feed_url));

        $error_link_url = 0;
        if ($feed_url != '')
        {
            include_once(SERVER_ROOT."/../classes/link.php");
            $link = new Link(0);
            $link->url = $feed_url;
            $error_desc_link_url = $link->Check();
            $error_link_url = $link->error;
        }
		
		if ($title == '' )
		{
			$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
			$url = "create.php?error=1&type=1";			
		} 
		elseif ($feed_url == '')
		{
			$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_feed"), array(), false);   
			$url = "create.php?error=1&type=1";			
		} 
		else if ($error_link_url <> 0)
        {
            $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
            $url = "create.php?error=1&type=1";            
        }
		else if (!$wf->CheckValidWebFeedUrl($feed_url) )
		{
			$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
			$url = "create.php?error=1&type=1";			
		}
		else
		{
			$wi->PublicWidgetStore($id_widget, $title, $description, $content, $feed_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
			$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_rss_added"), array(), false);
		}
	}
	else
	{
		$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
	}
	header("Location: {$url}");
}

if ($from == "create_manual_widget")
{
	// create manual widget
    $url = $pub_widget . "create.php";

	if ($id_p > 0)
	{
		$id_widget = 0;
		// strip html tag of manual widget title, description, content
		$title = strip_tags(html_entity_decode($post['widget_manual_title']));
		$description = strip_tags(html_entity_decode($post['widget_manual_desc']));
		$content = strip_tags(html_entity_decode($post['widget_manual_content']));
		$link_url = $post['widget_manual_link'];
		$rss_layout = '';
		$rss_items_display = 0;
		$id_web_feed = 0;
		$widget_type = WIDGET_TYPE_MANUAL;
		
		$session->Set("title",stripslashes($title));
		$session->Set("description",stripslashes($description));   
		$session->Set("content",stripslashes($content));   
		$session->Set("linkurl",stripslashes($link_url));
		
		if ($link_url == 'http://')
			$link_url = '';
		// checking for valid link
		$error_link_url = 0;
		if ($link_url != '')
		{
			include_once(SERVER_ROOT."/../classes/link.php");
	        $link = new Link(0);
	        $link->url = $link_url;
	        $error_desc_link_url = $link->Check();
	        $error_link_url = $link->error;
		}

		if ($title == '' )
		{
			$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
			$url .= "?error=1&type=0#create-manual-widget";  			
		}   
		else if ($error_link_url <> 0)
		{
			$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
			$url .= "?error=1&type=0#create-manual-widget";  			
		}
		else
		{
			$file = $fh->UploadedFile("img",true,0,false);    
			if($file['ok'])
			{
				// file is uploaded
				$id_widget = $wi->PublicWidgetStore(0, $title, $description, $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
        		$wi->WidgetImageUpdate($id_widget,$file);
				$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_manual_added"), array(), false);			
			}
			else
			{
				if ($file['error'] == '-1')
				{
					$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_image"), array(), false);
					$url .= "?error=1&type=0#create-manual-widget";  			
				}
				else
				{
					// no file uploaded
					$id_widget = $wi->PublicWidgetStore(0, $title, $description, $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
					$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_manual_added"), array(), false);			
				}
			}
		}
	}
	else
	{
		$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
	}
	
	header("Location: {$url}");
}

if ($from == "create_advance_manual_widget")
{
    // create manual widget
    $url = $pub_widget . "create.php";

    if ($id_p > 0)
    {
        $id_widget = 0;
        // strip html tag of manual widget title, description, content
        $title = strip_tags(html_entity_decode($post['widget_advance_manual_title']));
        $description = strip_tags(html_entity_decode($post['widget_advance_manual_desc']));
        $content = strip_tags(html_entity_decode($post['widget_advance_manual_content']));
        $link_url = $post['widget_advance_manual_link'];
        $rss_layout = '';
        $rss_items_display = 0;
        $id_web_feed = 0;
        $widget_type = WIDGET_TYPE_MANUAL_ADVANCE;
        
		$session->Set("title",stripslashes($title));
		$session->Set("description",stripslashes($description));   
		$session->Set("content",stripslashes($content));   
		$session->Set("linkurl",stripslashes($link_url));
        
        if ($link_url == 'http://')
            $link_url = '';
        // checking for valid link
        $error_link_url = 0;
        if ($link_url != '')
        {
            include_once(SERVER_ROOT."/../classes/link.php");
            $link = new Link(0);
            $link->url = $link_url;
            $error_desc_link_url = $link->Check();
            $error_link_url = $link->error;
        }

        if ($title == '' )
        {
            $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
            $url .= "?error=1&type=3#create-advance-manual-widget";
        }   
        else if ($error_link_url <> 0)
        {
            $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
            $url .= "?error=1&type=3#create-advance-manual-widget";
        }
        else
        {
            $file = $fh->UploadedFile("img",true,0,false);    
            if($file['ok'])
            {
                // file is uploaded
                $id_widget = $wi->PublicWidgetStore(0, $title, $description, '', '', $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
                $id_sub_widget = $wi->PublicWidgetStore(0, '', '', $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, WIDGET_TYPE_MANUAL, $id_p,$id_widget);
                $wi->WidgetImageUpdate($id_sub_widget,$file);
                $fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_advanced_added"), array(), false);            
                $url = "edit.php?id={$id_widget}";
            }
            else
            {
                if ($file['error'] == '-1')
                {
                    $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_image"), array(), false);
                    $url .= "?error=1&type=3#create-advance-manual-widget";              
                }
                else
                {
                    // no file uploaded
                    $id_widget = $wi->PublicWidgetStore(0, $title, $description, '', '', $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
                    $id_sub_widget = $wi->PublicWidgetStore(0, '', '', $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, WIDGET_TYPE_MANUAL, $id_p,$id_widget);
                    $fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_advanced_added"), array(), false);            
                    $url = "edit.php?id={$id_widget}";
                }
            }
        }
    }
    else
    {
        $fh->va->FeedbackAdd("error", "Authentication error", array(), false);
    }
    
    header("Location: {$url}");
}

if ($from == "delete_widget")
{
	// soft delete the widget OR remove widget share by someone else
	if ($id_p > 0)
	{
		$len_widget = strlen($post['id']);
		$id_widget = substr($post['id'], 7, $len_widget-7) ;
		$widget = $wi->WidgetGet($id_widget);
		if ($widget['id_p'] == $id_p)      
			$wi->WidgetSoftDelete($id_widget,true);
		else
			$wi->WidgetSharingDeleteByUserAndWidget($id_p, $id_widget);	
	    $fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_deleted"), array(), false);
	}
	else
	{
		$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
	}
    $url = "mywidgets.php";
	header("Location: {$url}");
}

if ($from == "edit_rss_widget")
{
	// edit RSS widget
	include_once(SERVER_ROOT."/../classes/web_feeds.php");
	$wf = new WebFeeds();
	
	if ($id_p > 0)
	{
		$id_widget = $post['id'];
		if($wi->IsWidgetEditable($id_p,$id_widget))
		{
			// strip html tag of RSS widget title, description  
			$title = strip_tags(html_entity_decode($post['widget_rss_title']));
			$description = strip_tags(html_entity_decode($post['widget_rss_desc']));
			$content = '';
			$feed_url = $post['widget_rss_url'];
			$rss_layout = 1;
			$rss_items_display = 5;
			$id_web_feed = 0;
			$widget_type = WIDGET_TYPE_RSS;
			
			if ($title == '' )
			{
				$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
			} 
			elseif ($feed_url == '')
			{
				$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_feed"), array(), false);   
			} 
			else if (!$wf->CheckValidWebFeedUrl($feed_url) )
			{
				$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
			}
			else
			{
				$wi->PublicWidgetStore($id_widget, $title, $description, $content, $feed_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
				$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_updated"), array(), false);
			}
		}
		else
		{
			$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
		}
	}
	else
	{
		$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
	}
	$url = "edit.php?id={$id_widget}";
	header("Location: {$url}");
}

if ($from == "edit_manual_widget")
{
	// edit manual widget
	if ($id_p > 0)
	{
		$id_widget = $post['id'];
		if($wi->IsWidgetEditable($id_p,$id_widget))
		{
			// strip html tag of manual widget title, description, content  
			$title = strip_tags(html_entity_decode($post['widget_manual_title']));
			$description = strip_tags(html_entity_decode($post['widget_manual_desc']));
            $content = strip_tags(html_entity_decode($post['widget_manual_content']));
			$link_url = $post['widget_manual_link'];
			$rss_layout = '';
			$rss_items_display = 0;
			$id_web_feed = 0;
			$widget_type = WIDGET_TYPE_MANUAL;
			
			// checking for valid link
			$error_link_url = 0;
			if ($link_url == 'http://')
				$link_url = '';
			if ($link_url != '')
			{
				include_once(SERVER_ROOT."/../classes/link.php");
		        $link = new Link(0);
		        $link->url = $link_url;
		        $error_desc_link_url = $link->Check();
		        $error_link_url = $link->error;
			}
	
			if ($title == '' )
			{
				$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
			}   
			else if ($error_link_url <> 0)
			{
				$fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
			}
			else
			{
				$file = $fh->UploadedFile("img",true,0,false);    
				if($file['ok'])
				{
					// file is uploaded
					$id_widget = $wi->PublicWidgetStore($id_widget, $title, $description, $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
	        		$wi->WidgetImageUpdate($id_widget,$file);
					$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_updated"), array(), false);			
				}
				else
				{
					// no file uploaded
					$id_widget = $wi->PublicWidgetStore($id_widget, $title, $description, $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
					$fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_updated"), array(), false);			
				}
			}			
		}
		else
		{
			$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
		}
	}
	else
	{
		$fh->va->FeedbackAdd("error", "Authentication error", array(), false);
	}
    $url = $pub_widget . "edit.php?id={$id_widget}";
	header("Location: {$url}");
}
   
if ($from == "edit_advance_manual_widget")
{
    // edit manual widget
    if ($id_p > 0)
    {
        $id_widget = $post['id'];      
        $id_sub_widget = $post['id_sub'];   
        $action = $post['action'];   
        if($wi->IsWidgetEditable($id_p,$id_widget))
        {
            if ($action == 'delete')
            {
                $wi->PublicWidgetChildDelete($id_sub_widget);    
            }
            else
            {
                // strip html tag of manual widget title, description, content  
                $title = strip_tags(html_entity_decode($post['widget_advance_manual_title']));
                $description = strip_tags(html_entity_decode($post['widget_advance_manual_desc']));
                $content = strip_tags(html_entity_decode($post['widget_advance_manual_content']));
                $link_url = $post['widget_advance_manual_link'];
                $rss_layout = 1;
                $rss_items_display = 0;
                $id_web_feed = 0;
                $widget_type = WIDGET_TYPE_MANUAL_ADVANCE;
                
                // checking for valid link
                $error_link_url = 0;
                if ($link_url == 'http://')
                    $link_url = '';
                if ($link_url != '')
                {
                    include_once(SERVER_ROOT."/../classes/link.php");
                    $link = new Link(0);
                    $link->url = $link_url;
                    $error_desc_link_url = $link->Check();
                    $error_link_url = $link->error;
                }
        
                if ($title == '' )
                {
                    $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_title"), array(), false);   
                }   
                else if ($error_link_url <> 0)
                {
                    $fh->va->FeedbackAdd("error", $fh->va->tr->Translate("error_url"), array(), false);
                }
                else
                {
                    $file = $fh->UploadedFile("img",true,0,false);    
                    if($file['ok'])
                    {
                        // file is uploaded
                        $id_widget = $wi->PublicWidgetStore($id_widget, $title, $description, '', '', $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
                        $id_sub_widget = $wi->PublicWidgetStore($id_sub_widget, '', '', $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, WIDGET_TYPE_MANUAL, $id_p,$id_widget);
                        $wi->WidgetImageUpdate($id_sub_widget,$file);
                        $fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_updated"), array(), false);            
                    }
                    else
                    {
                        // no file uploaded
                        $id_widget = $wi->PublicWidgetStore($id_widget, $title, $description, '', '', $rss_layout, $rss_items_display, $id_web_feed, $widget_type, $id_p);
                        if ($content != '' || $link_url != '')
                            $id_sub_widget = $wi->PublicWidgetStore($id_sub_widget, '', '', $content, $link_url, $rss_layout, $rss_items_display, $id_web_feed, WIDGET_TYPE_MANUAL, $id_p,$id_widget);
                        $fh->va->FeedbackAdd("notice", $fh->va->tr->Translate("widget_updated"), array(), false);            
                    }
                }            
            }
        }
        else
        {
            $fh->va->FeedbackAdd("error", "Authentication error", array(), false);
        }
    }
    else
    {
        $fh->va->FeedbackAdd("error", "Authentication error", array(), false);
    }
    $url = $pub_widget . "edit.php?id={$id_widget}";
    header("Location: {$url}");
}
?>
