<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,12,false);
$fh->va->NoSpam("link");
$post = $fh->HttpPost();

$from = $post['from'];

if ($from=="event")
{
	$start_date 	= $fh->CheckDateString($post['start_date']) . " " . $fh->Strings2Time($post['start_date_h'],$post['start_date_i']);
	$length		= $post['length'];
	$allday		= $fh->Checkbox2bool($post['allday']);
	$id_event_type	= $post['id_event_type'];
	$title		= $fh->StringClean($post['title']);
	$description	= $post['description'];
	$place		= $post['place'];
	$address		= $post['address'];
	$id_geo	= $post['id_geo'];
	$place_details	= $post['place_details'];
	$contact_name	= $post['contact_name'];
	$email		= $post['email'];
	$phone		= $post['phone'];
	$id_topic 	= $fh->Null2Zero($post['id_topic']);
	$link		= $fh->String2Url($post['link']);

	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserInfo(false);
	$id_p = (int)$user['id'];

	$id_new_event = 0;
	$fh->va->NotEmpty($title,"title");
	//$fh->va->NotEmpty($place,"place");
	$fh->va->GreaterThenZero($id_event_type,"id_event_type");
	$fh->va->Email($email);
	$fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	$fh->va->DateInFuture($fh->CheckDateTimestamp($post['start_date']));
	$ini = new Ini();
	$events_insert = $ini->Get("events_insert");
	if($fh->va->return && $events_insert)
	{
		include_once(SERVER_ROOT."/../classes/event.php");
		$event = new Event();
		$id_new_event = $event->EventInsert($start_date, $length, $allday, $id_event_type, $title, $description, $place, $address, $id_geo,
			$place_details, $contact_name, $email, $phone, $link,0,$id_topic,1,0,"",0,$id_p,0);
		$file = $fh->UploadedFile("img",true,5120000);
		if ($file['ok']) {
			$event->ImageUpdate($id_new_event,$file);
		}
	}
	header("Location: insert.php?id=$id_new_event&id_topic=$id_topic");
}

if($from=='facebook_event') {
	$conf = new Configuration();
	$url= "index.php";
	if($conf->Get("fb_scraper")!='') {
		$fh->va->Captcha($post);
		include_once(SERVER_ROOT."/../classes/event.php");
		$e = new Event();
		$link		= $fh->String2Url($post['link'],true);
		$match = array();
		preg_match('@(?:https?:\/\/)?(?:www\.)?facebook\.com\/events\/?([^/?]*)@i',$link,$match);
		if(isset($match[1])) {
			$event = $e->EventGetByFacebookId($match[1]);
			if(isset($event['id_event'])) {
				$fh->va->return = false;
				$msg = 'Evento già segnalato';
				if($event['approved']) {
					$msg .= ": <a href=\"event.php?id={$event['id_event']}\">{$event['title']}</a>";
				}
				$fh->va->FeedbackAdd('error', $msg, array(), false);
			}
		} else {
			$fh->va->return = false;
			$fh->va->FeedbackAdd('error', 'Link non corretto', array(), false);
			$url= "insert.php";
		}
		if($fh->va->return) {
			$id_new_event = $e->FacebookImport($link);
			if(count($e->errors)>0) {
				$fh->va->FeedbackAdd('error', 'Segnalazione ricevuta, ma si è verificato un errore, si prega di non re-inviare il link, se ne occuperà al più presto la redazione', array(), false);
			} else {
				$fh->va->FeedbackAdd('notice', 'Segnalazione ricevuta, verrà approvata al più presto', array(), false);
				$url = "insert.php?id=$id_new_event";
			}		
		}
	}
	header("Location: $url");
}

?>

