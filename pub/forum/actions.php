<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/forum.php");
$fh = new FormHelper(true,4,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);

$from	= $post['from'];

if ($from=="thread_insert")
{
	$id_forum	= (int)$post['id_forum'];
	$id_topic	= (int)$post['id_topic'];
	$url = "insert.php?id=$id_forum";
	if($id_forum>0)
	{
		$f = new Forum($id_forum);
		$forum = $f->ForumGet();
		$source	= $post['source'];
		$title		= $post['title'];
		$description	= $post['description'];
		$description_long	= $post['description_long'];
		$id_topic = $forum['id_topic'];
		$fh->va->NotEmpty($title,"title");
		$fh->va->Captcha($post);
		$fh->va->NoForumSpam();
		if($fh->va->return)
		{
			$id_p = $fh->IsThereUser();
			$accept_proposal = false;
			if($forum['users_type']=="0" || $forum['active']!="1")
			{
				$fh->va->MessageSet("error","forum_closed");
			}
			elseif($forum['users_type']=="1") 
			{
				$id_p = $fh->IsThereUser(true);
				if($id_p>0)
				{
					$accept_proposal = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_authenticate");
					$url = $fh->LoginUrl();
				}
			}
			elseif($forum['users_type']=="2") 
			{
				if($id_p>0)
				{
					$accept_proposal = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_register");
					$url = $fh->LoginUrl();
				}
			}
			elseif($forum['users_type']=="3") 
			{
				$name		= $post['name'];
				$email		= $post['email'];
				if($fh->va->Email($email))
				{
					$accept_proposal = true;
					$names = explode(" ",$name,2);
					$id_p = $fh->UserCreate($names[0],$names[1],$email,0,$id_topic,true);
				}
			}
			
			if($accept_proposal)
			{
				$approved = $forum['approve_threads']? "0" : "1";
				$f->ThreadInsertPub($title,$description,$description_long,$source,$id_p,$approved);
				$fh->va->MessageSet("notice","thanks_campaign");
				$url = "index.php?id=$id_forum";
			}
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

?>
