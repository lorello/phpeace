<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$id_feature = (int)$get['id'];
$id_topic = (int)$get['id_topic'];
$json = isset($get['json']);

include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout();
$l->TopicInit($id_topic,true);

include_once(SERVER_ROOT."/../classes/varia.php");
$v = new Varia();

if($id_feature>0)
{
	$feature = $l->pt->ft->FeatureGet($id_feature);
	if($feature['active']=="1" && $feature['public']=="1")
	{
		if(isset($get['transform'])) {
			$params = array();
			$params['subtype'] = "feature";
			$params['id_feature'] = $id_feature;
			$params['feature'] = $feature;
			$params['common'] = true;
			$html = $l->Output("random_item",0,$id_topic,1,$params);
			echo $html;
		} else {
			$feature_params = $v->Deserialize($feature['params']);
			$irows = $l->pt->ft->PageFunction($feature['id_function'],$feature_params,array());
			$items = array();
			$seq = 1;
			foreach($irows as $irow)
			{
				$l->TopicInit($irow['id_topic']);
				$irow['item_type'] = $l->pt->ft->item_type;
				$items['i_' . $seq] = $l->Item($irow);
				$seq++;
			}
			$functions = $l->tr->Translate("page_functions");
			$f_array = array('xname'=>"feature",'id'=>$id_feature,'name'=>$feature['name'],'id_user'=>$feature['id_user'],'id_function'=>$feature['id_function'],'function'=>$functions[$feature['id_function']],'params'=>$feature_params,'info'=>($l->pt->ft->info),'type'=>($l->pt->ft->item_type),'items'=>$items,'description'=>$feature['description']);
			
			if($json)
			{
				$output = $l->JsonEncode($f_array);
				header('Content-type: application/json');
			}
			else
			{
				$output = $l->xh->Array2Xml($f_array);
				header('Content-type: text/xml');
			}
			echo $output;
		}
	}
}

?>

