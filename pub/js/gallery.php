<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
	
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$l = new Layout;
$async = (int)$get['a'];

$params = array();
$params['id_image'] = $get['img'];
$params['id_gallery'] = $get['id'];
$params['id_group'] = $get['id_g'];
$params['w'] = $get['w'];
$params['random'] = $get['r'];
$params['jump'] = (isset($get['j']) && $get['j']=="0")? "0" : "1";
$params['subtype'] = "image";
$id_topic = (int)$get['id_topic'];

$html = $l->Output("random_item",0,$id_topic,1,$params);

print $l->Javascript($html,true,$async=="1");
?>
