<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$events = json_encode(array());

// default 2 weeks, no more than 3 months
$days = isset($get['days'])? min($get['days'],90) : 14;

$start = isset($get['start'])? $get['start'] : date("Y-m-d");
$start_ts = strtotime($start);
$end_ts = isset($get['end'])? strtotime($get['end']) : $start_ts + $days*24*3600;

if($start_ts > 0 && $end_ts > 0) {
   include_once(SERVER_ROOT."/../classes/cache.php");
   $cache = new Cache;
   $events = $cache->Get('events',"{$start_ts}-{$end_ts}");
}
header('Content-type: application/json');
echo $events;
?>

