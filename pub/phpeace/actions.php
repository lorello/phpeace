<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");
$fh = new FormHelper(true,17,false);
$post = $fh->HttpPost();

$phpeace = new PhPeace;

$from		= $post['from'];

if ($from=="register")
{
	$install_key 	= $post['install_key'];
	$id_version 	= $post['id_version'];
	$id_language 	= $post['id_language'];
	$name	 	= $post['name'];
	$contact 	= $post['contact'];
	$email 		= $post['email'];
	$admin_web	= $post['admin_web'];
	$id_client = 0;
	$fh->va->NotEmpty($name,"name");
	$fh->va->Email($email);
	$fh->va->PhPeaceKeyCheck($install_key);
	if($fh->va->return)
	{
		$id_client = $phpeace->Register($install_key,$id_version,$name,$contact,$email,$admin_web);
	}
	header("Location: register.php?id=$id_client&k=$install_key&id_language=$id_language");
}

if ($from=="client_update")
{
	$install_key 	= $post['install_key'];
	$admin_web	= $post['admin_web'];
	$php_version	= $post['php_version'];
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$modules	= $v->Deserialize($fh->HttpPostUnescapedVar('modules'),false);
	if(is_array($modules) && $phpeace->KeyCheck($install_key) && $admin_web!="")
	{
		echo $phpeace->ClientUpdateRemote($install_key,$admin_web,$php_version,$post['modules']);
	}
}
?>
