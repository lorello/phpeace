<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/phpeace.php");
$phpeace = new Phpeace;
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

if ($phpeace->main)
{
	$id_version = $get['id'];
	$install_key = $get['k'];

	$client = $phpeace->ClientCheck($install_key);

	if ($client['approved'])
	{
		$hash = (int)$get['hash'];
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$filename = $phpeace->FileNameUpdate($id_version) . ".tar.gz";
		if($hash==1)
		{
			echo $fm->Hash("distrib/$filename");
		}
		else 
		{
			$fm->Transfer("distrib/$filename");		
			$phpeace->ClientUpdateLast($client['id_client'], 0);
		}
	}
}
?>
