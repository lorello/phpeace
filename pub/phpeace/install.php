<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/installer.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$fh = new FormHelper(false,0,false);
$step = (int)$_POST['step'];

$post = $step>0? $fh->HttpPost() : $_POST;

$id_language = $post['id_language'];

$back_block = false;
$next_block = false;
$validate = true;

$in = new Installer($step,($id_language>0)?$id_language:2);

switch($fh->ActionGet($post))
{
	case "language":
		if ($id_language>0 && $id_language!=$in->trm->id_language)
			$in->SetLanguage($id_language);
	break;
	case "next":
		$validate = $in->Validate();
		if($validate)
		{
			$in->step++;
		}
	break;
	case "back":
		$in->step--;
	break;
}

$page_title = $in->trm->Translate("install_title") . (($in->step>0)? " - " . $in->trm->TranslateParams("install_step",array($in->step,$in->steps)) : "");
$page_subtitle = $in->trm->Translate("install_title_page".$in->step);

include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper();
header("Content-Type: " . $ah->content_header); 
echo $ah->DocType();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="<?=$ah->content_header;?>" >
<title><?=$page_title;?></title>
<style type="text/css" media="screen">
@import url(/phpeace/install.css);
</style>
</head>
<body>
<img id="logo" src="/logos/phpeace.png" alt="PhPeace Logo">
<h2><?=$page_title;?></h2>
<h3><?=$page_subtitle;?></h3>
<?php
if(count($in->messages)>0)
{
	echo "<ul class=\"msg\">";
	foreach($in->messages as $message)
		echo "<li>$message</li>\n";
	echo "</ul>";
}
?>

<form name="form1" method="POST">
<input type="hidden" name="step" value="<?=$in->step;?>">
<?php
switch($in->step)
{
	case 0:
		?>
		<input type="hidden" name="action" value="language">
		<?=$in->trm->Translate("language");?>:  <select name="id_language">
		<?php
			foreach($in->trm->Translate("languages") as $key=>$lang)
				echo "<option value=\"". ($key+1) . "\"" . ((($key+1)==($in->trm->id_language))? " selected":"") . ">$lang</option>\n";
		?>
		</select>
		<input name="action_language" type="submit" value="<?=$in->trm->Translate("change");?>">
		<?=$in->trm->TranslateParams("install_intro",array($in->steps));?>
		<?php
	break;
	case 1:
		echo "<ul>";
		$testall = true;
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		if ($conf->Get("user_auth")=="ldap")
			$in->test_cases[] = "ldap";
		foreach($in->test_cases as $case)
		{
			echo "<li>" . $in->trm->Translate("test_".$case) . "... ";
			if ($in->Test($case))
			{
				echo $in->trm->Translate("test_ok");
				if ($in->test_msg!="")
					echo "<div>" . $in->test_msg . "</div>";
			}
			else
			{
				$testall = false;
				$critical = $in->TestCritical($case);
				if ($critical)
					$next_block = true;
				echo "<span class=\"error\">" . $in->trm->Translate("test_ko") . "</span>";
				foreach($in->errors as $error)
					echo "<div>" . (($critical)? "<b>" . $in->trm->Translate("error_critical") . "</b> ":"" ) . "$error</div>";
			}
			echo "</li>\n";
		}
		echo "</ul>";
		if (!$testall)
		{
			echo "<p class=\"error\">" . $in->trm->Translate("test_failed_warning") . "</p>\n";
			if ($next_block)
				echo "<p class=\"error\">" . $in->trm->Translate("test_next_block") . "</p>\n";
		}
		else 
			echo "<p>" . $in->trm->Translate("test_passed") . "</p>\n";
	break;
	case 2:
		if ($in->IsInstalled())
		{
			echo "<p>" . $in->trm->Translate("error_is_installed") . "</p>\n";
			$next_block = true;
		}
		else
		{
			$in->DatabaseInit();
			if ($in->IsInstalled())
			{
				echo "<p>" . $in->trm->Translate("database_initialised") . "</p>\n";
				$back_block = true;
			}
			else
			{
				echo "<p>" . $in->trm->Translate("database_initialised_ko") . "</p>\n";
				$next_block = true;
			}
		}
	break;
	case 3:
		if($in->lock->LockCheck())
		{
			$back_block = true;
			include_once(SERVER_ROOT."/../classes/htmlhelper.php");
			$hh = new HtmlHelper(false);
			$hh->tr = new Translator($id_language,0);
			include_once(SERVER_ROOT."/../classes/ini.php");
			$ini = new Ini;
			echo $hh->input_table_open();
			$paths2 = array('map','gallery','users','events','search','books','campaign','forum','quotes','media');
			foreach($paths2 as $path2)
			{
				$path = $path2 . "_path";
				echo $hh->input_text($in->trm->Translate($path),$path,$ini->Get($path),20,0,1);
			}
			echo $hh->input_table_close();
		}
	break;
	case 4:
		if($in->lock->LockCheck())
		{
			if($validate)
			{
				$in->DirectoriesInit();
			}
			$back_block = true;
			include_once(SERVER_ROOT."/../classes/htmlhelper.php");
			$hh = new HtmlHelper(false);
			$hh->tr = new Translator($id_language,0);
			echo $hh->input_table_open();
			$admin_web = (substr($_SERVER['SERVER_NAME'],0,4)=="www.")? str_replace("www","admin",$_SERVER['SERVER_NAME']) : "admin." . $_SERVER['SERVER_NAME'];
			echo $hh->input_text($in->trm->Translate("admin_web"),"admin_web",$admin_web,30,0,1);
			echo $hh->input_text($in->trm->Translate("pub_web"),"pub_web",$_SERVER['SERVER_NAME'],30,0,1);
			echo $hh->input_text($in->trm->Translate("title"),"title","",20,0,1);
			echo $hh->input_text($in->trm->Translate("description"),"description","",50,0,1);
			echo $hh->input_text($in->trm->Translate("staff_email"),"staff_email","",30,0,1);
			echo $hh->input_table_close();
		}
	break;
	case 5:
		if($in->lock->LockCheck())
		{
			$back_block = true;
			include_once(SERVER_ROOT."/../classes/htmlhelper.php");
			$hh = new HtmlHelper(false);
			$hh->tr = new Translator($id_language,0);
			echo $hh->input_table_open();
			echo $hh->input_text("login","login","",20,0,1);
			echo $hh->input_text("password","password","",20,0,1);
			include_once(SERVER_ROOT."/../classes/config.php");
			$conf = new Configuration();
			if ($conf->Get("user_auth")=="internal")
			{
				echo $hh->input_text($in->trm->Translate("name"),"name","",30,0,1);
				echo $hh->input_text("email","email","",30,0,1);
			}
			echo $hh->input_table_close();
		}
	break;
	case 6:
		if($in->lock->LockCheck())
		{
			include_once(SERVER_ROOT."/../classes/ini.php");
			$ini = new Ini;
			$admin_web = $ini->Get("admin_web");
			echo "<p>" . $in->trm->Translate("install_end") . " <a href=\"$admin_web\">$admin_web</a></p>\n";
			$back_block = true;
			$next_block = true;
			$in->lock->LockRemove();
			$fm = new FileManager();
			$fm->Delete("pub/phpeace/install.php");
			$fm->Delete("pub/phpeace/install.css");
			$fm->Delete("pub/phpeace/info.php");
			$fm->PostUpdate();
		}
	break;
}

if ($in->step>0)
	echo "<input type=\"hidden\" name=\"id_language\" value=\"" . $in->trm->id_language . "\">";

$back_cond = ($in->step>0) && !$back_block;
$next_cond = ($in->step != $in->steps) && !$next_block;
?>
<div id="button-bar">
<input type="submit" name="action_back" value="<?=$in->trm->Translate("install_step_back")?>" <?=(!$back_cond)? "disabled=\"disabled\"":""; ?>>
<input type="submit" name="action_next" value="<?=$in->trm->Translate("install_step_next")?>" <?=(!$next_cond)? "disabled=\"disabled\"":""; ?>>
</div>
</form>
</body>
</html>

