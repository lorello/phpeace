<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,4,false);

$post = $fh->HttpPost(true);
$from = $post['from'];

if ($from=="link")
{
    $fh->va->NoSpam("link");
    include_once(SERVER_ROOT."/../classes/topic.php");
    include_once(SERVER_ROOT."/../classes/link.php");
    include_once(SERVER_ROOT."/../classes/db.php");
    $db =& Db::globaldb();
    $insert_date    = $db->getTodayTime();
    $link        = $fh->String2Url($post['link']);
    $title        = $post['title'];
    $description    = $post['description'];
    $id_topic    = $post['id_topic'];
    $id_subtopic    = $post['id_subtopic'];
    $keywords    = "";
    $approved    = 0;
    $vote         = 3;
    $ikeywords    = array();
    $t = new Topic($id_topic);
    $id_deposit = $t->HasSubtopicType(0);
    $id_language = $t->id_language;
    $l = new Link(0);
    $fh->va->NotEmpty($title,"title");
    $fh->va->NotEmpty($link,"link",false);
    $fh->va->NoForumSpam();
	$fh->va->Captcha($post);
    if($fh->va->return)
    {
        $fh->va->MessageSet("notice","thanks_link");
        $l->LinkInsert($insert_date,$link,$title,$description,$keywords,$vote,$id_language,$id_topic,$id_deposit,$approved,$ikeywords);
    }
    header("Location: thanks.php?id_subtopic=$id_subtopic&id_topic=$id_topic&from=link");
}

if ($from=="friend")
{
    $fh->va->NoSpam();
    $unescaped_post = $fh->HttpPost(false,false);
    $id_article        = (int)$post['id_article'];
    $id_topic        = (int)$post['id_topic'];
    $id_subtopic    = (int)$post['id_subtopic'];
    $sender         = $unescaped_post['sender'];
    $sender_email    = $unescaped_post['sender_email'];
    $email            = $unescaped_post['email'];
    $message        = $unescaped_post['message'];
    $fh->va->Email($email);
    $fh->va->Email($sender_email,"sender_email");
    $fh->va->NoForumSpam($sender_email);
    $fh->va->Captcha($post);
    if ($fh->va->return)
    {
        include_once(SERVER_ROOT."/../classes/mail.php");
        $mail = new Mail();
        $sent = $mail->FriendSend($id_article,$sender,$sender_email,$email,$message);
        if($sent)
            $fh->va->MessageSet("notice","send_friend_ok",array($email));
        else
            $fh->va->MessageSet("error","send_friend_ko");
        header("Location: thanks.php?id_subtopic=$id_subtopic&id_topic=$id_topic&from=send_friend");
    }
    else
        header("Location: friend.php?id=$id_article");
}

if ($from=="form")
{
    $fh->va->NoSpam();
    $unescaped_params    = $fh->SerializeParams(false);
    include_once(SERVER_ROOT."/../classes/topic.php");
    $id_topic     = $post['id_topic'];
    $id_subtopic     = $post['id_subtopic'];
    $id_form     = $post['id_form'];
    $id_doc     = $fh->Null2Zero($post['id_doc']);
    $contact    = $fh->Checkbox2bool($post['contact']);
    $t = new Topic($id_topic);
    include_once(SERVER_ROOT."/../classes/forms.php");
    $fo = new Forms();
    $row = $fo->FormGet($id_form);
    $trf = new Translator($t->id_language,0,false,$t->id_style);
    $params = array();
    $params_values = array();
    $fparams = $fo->Params($id_form);
    $id_geo = (int)$post['id_geo'];
    $id_p     = ($post['id_p']>0 && ($row['require_auth']=="1" || $row['require_auth']=="2"))? $post['id_p'] : 0;
    $file = array();
    $id_pt_groups = array();
    $email = "";
        
    foreach($fparams as $fparam)
    {
        if($fparam['type']=="upload" || $fparam['type']=="upload_image")
        {
			$file_type = ($fparam['type']=="upload_image")? "img" : $fh->UploadFileType($fparam['type_params']);   
			$file = $fh->UploadedFile($file_type,true,0,true,"param_".$fparam['id_param']);  
			if($file['error']=="-1")
				$fh->va->return = false;
			if($fparam['type']=="upload_image")
			{
				$file['id_gallery'] = (int)$fparam['type_params'];
			}
        }
        else
        {
            $value = $unescaped_params[$fparam['id_param']];
            if($fparam['type']=="geo")
            {
                include_once(SERVER_ROOT."/../classes/geo.php");
                $geo = new Geo();
                $geo_location = (int)$fparam['type_params'];
                if($geo_location>0 && !$row['profiling']>0)
                    $geo->geo_location = $geo_location;
                if($id_geo >0)
                    $value = $geo->GeoGet($id_geo,$geo->geo_location);
                $label = ($row['multilanguage']=="1")? $trf->TranslateTry($geo->Label()) : $fparam['label'];
            }
            else
                $label = ($row['multilanguage']=="1")? $trf->TranslateTry($fparam['label']) : $fparam['label'];        
			if($fparam['type']=="checkbox" || $fparam['type']=="subscription")
			{
				$value = $value? "ok":"no";
			}
			if($fparam['type']=="dropdown_open" && $unescaped_params[$fparam['id_param'] . '_other']!='')
			{
				$value = $unescaped_params[$fparam['id_param'] . '_other'];
			}
			$params[] = array('name'=>$label,'value'=>$value,'email_subject'=>$fparam['email_subject'],'use'=>($fparam['type']=="text" && is_numeric($fparam['type_params']))? $fparam['type_params']:0);
            $params_values[$fparam['id_param']] = $value;
            
			if($fparam['type']=="subscription")
			{
	           	$id_pt_groups[] = (int)$fparam['type_params'];
			}
            
            if($fparam['mandatory'] && $fparam['type']!="mchoice")
            {
				$fh->va->NotEmpty($value,$label,false);
				if($fparam['type']=="text" && $fparam['type_params']=="4")
                    $fh->va->Email($value);
            }
            if($fparam['type']=="text" && $fparam['type_params']=="4")
            	$email = $value;
            if($fparam['type']=="text" && $fparam['type_params']=="5" && is_array($file))
            	$file['caption'] = $value;
        }
    }
    if($row['privacy']=="1")
    {
        $privacy    = $fh->Checkbox2bool($post['privacy']);
        if (!$fh->va->IsTrue($privacy))
            $fh->va->MessageSet("error","error_privacy");
    }
    $fh->va->Captcha($post);    
    $fh->va->NoForumSpam($email);
    if ($fh->va->return)
    {
    	$id_post = 0;
    	$score = 0;
    	if($row['weights'])
    	{
    		$score = $fo->PostScore($id_form,$params);
    		$action = $fo->PostAction($id_form,$score);
    		if(isset($action['redirect']) && $action['redirect']!="")
    			$row['redirect'] = $action['redirect'];
    	}
    	if(($row['profiling']!="0"))
    	{
        	$id_p = $t->FormUserCreate($params,$id_geo,$contact,$id_p,$row['id_pt_group'],$id_pt_groups);
    		
	        // Donations
	        if($row['profiling']=="2")
	        {
	            $amount		= $fh->String2Number($post['amount']);
	            $amount2	= $fh->String2Number($post['amount2']);
	            $amount		= ($amount2>0)? $amount2 : $fh->String2Number($post['amount']);
	            $currency	= $post['currency'];
	            $id_account	= $post['id_account'];
	            $payment_name = ($id_topic>0? $fh->SqlQuote($t->name,false) . " - ":"") . $row['name'];
	            if($amount>0)
	            {
	                $id_payment = $t->DonationInsert($id_p,$amount,$id_account,$row['id_payment_type'],$payment_name,$id_form,$currency);
	            }
	        }
	        if($id_p>0)
	        {
		        // Store
		        if($row['store'])
		        {
			        $id_post = $t->FormPostStore($id_form,$id_p,$params,$file,$id_payment,$score);
		        }
	        }
    	}
        
    	if($row['thanks_email']!="")
    	{
            $message_email = $fo->ThankParams($fparams, $params_values, $row['thanks_email'], $id_post);
    		$t->FormPostFeedback($id_form,$id_p,$email,$row['name'],$message_email);
    	}
    	
    	$action_email = ($row['weights'] && isset($action['recipient']))? $action['recipient']:"";
        $t->FormPostNotify($row['id_recipient'],$row['recipient'],$id_form,$row['name'],$params,$contact,$id_post,$row['weights'],$score,$action_email,$row['hide_empty_fields']);
        
        // Thanks
        if($row['profiling']!="2")
        {
            if($row['thanks']!="")
            {
            	$message = $fo->ThankParams($fparams, $params_values, $row['thanks'], $id_post);
                $fh->va->FeedbackAdd("notice",$message,array(),false);
            }
            else
                $fh->va->MessageSet("notice","thanks_contact");
        }
        $url = $row['redirect']!=""? $row['redirect'] : "thanks.php?id_subtopic=$id_subtopic&id_topic=$id_topic&from=contact";
		if($id_doc>0)
		{
			include_once(SERVER_ROOT."/../classes/doc.php");
			$d = new Doc($id_doc);
			if($d->FormCheck($id_subtopic))
			{
				$doc = $d->DocGet();
				if($doc['id_doc']>0)
				{
					$src = "uploads/docs/{$doc['id_doc']}.{$doc['format']}";
					include_once(SERVER_ROOT."/../classes/file.php");
					$fm = new FileManager();
					$fm->GetLocalFile($src);
				}
			}
		}
    }
    else
    {
	    $url = "form.php?id=$id_subtopic&id_topic=$id_topic";
    }
   	header("Location: $url");
}

if ($from=="user_contact")
{
    $fh->va->NoSpam();
    $id_user     = (int)$post['id_user'];
    $fh->va->Captcha($post);
    if($id_user>0)
    {
        include_once(SERVER_ROOT."/../classes/user.php");
        $u = new User();
        $u->id = $id_user;
        $user = $u->UserGet();
        if($user['active'] && $user['user_show'])
        {
            $unescaped_post = $fh->HttpPost(false,false);
            $comments    = $unescaped_post['comments'];
            $email        = $unescaped_post['email'];
            $name        = $unescaped_post['name'];
            $fh->va->NotEmpty($name,"name");
            $fh->va->Email($email);
            $fh->va->NoForumSpam($email);
            if($fh->va->return)
            {
                $u->SendMessage($name,$email,$comments,$user['email'],$user['name']);
                $fh->va->MessageSet("notice","thanks_contact");
                header("Location: author.php?u=$id_user");
            }
            else 
                header("Location: author.php?c=$id_user");
        }
    }
}

if ($from=="article")
{
    $fh->va->NoSpam();
    $id_topic    = $post['id_topic'];
    $id_subtopic    = $post['id_subtopic'];
    $author        = $post['author'];
    $author_notes        = $post['author_notes'];
    $source        = $post['source'];
    $halftitle        = $post['halftitle'];
    $headline        = $post['headline'];
    $subhead        = $post['subhead'];
    $content        = $post['content'];
    $notes            = $post['notes'];
    $name            = $post['name'];
    $email            = $post['email'];
    $fh->va->NotEmpty($headline,"title");
    $fh->va->Email($email);
    $fh->va->NoForumSpam($email);
	$fh->va->Captcha($post);
    if ($fh->va->return)
    {
        include_once(SERVER_ROOT."/../classes/article.php");
        $a = new Article(0);
        $a->ArticleSubmit($author, $author_notes, $source, $id_topic, $halftitle, $headline, $subhead, $content, $notes, $name, $email, 1);
        $fh->va->MessageSet("notice","thanks_article");
    }
    header("Location: thanks.php?id_subtopic=$id_subtopic&id_topic=$id_topic&from=article" );
}

if ($from=="comment")
{
    $fh = new FormHelper(true,0,false);
    $fh->va->NoSpam();
    $subtype = "insert";
    $name        = $post['name'];
    $email        = $post['email'];
    $title        = $post['title'];
    $comment    = $post['comment'];
    $id_topic     = $post['id_topic'];
    $id_item     = $post['id_item'];
    $id_parent     = $post['id_parent'];
    $id_r         = (int)$post['id_r'];
    $id_p = $fh->IsThereUser();
    $verify_email = true;
    switch($id_r)
    {
        case 5:
            include_once(SERVER_ROOT."/../classes/article.php");
            $a = new Article($id_item);
            $art = $a->ArticleGet();
            $approved = ($art['moderate_comments']=="1")? 0:1;
            if($art['allow_comments']=="0")
            {
                $fh->va->return = false;
                $fh->va->MessageSet("error","comments_not_allowed");
            }
            switch($art['topic_allow_comments'])
            {
                case "0":
                    $fh->va->return = false;
                    $fh->va->MessageSet("error","comments_not_allowed");
                break;
                case "1":
                    if(!$id_p>0)
                    {
                        $fh->va->MessageSet("error","register_before_comment");
                        $fh->va->return = false;
                    }
                break;
                case "2":
                    if(!$id_p>0)
                    {
                        $fh->va->NotEmpty($name,"name");
                        $fh->va->Email($email);
                        $verify_email = false;
                    }
                break;
            }
        break;
        case 10:
            include_once(SERVER_ROOT."/../classes/forum.php");
            $f = new Forum(0);
            $thread = $f->ThreadGet($id_item);
            $f->id = $thread['id_topic_forum'];
            $forum = $f->ForumGet();
            if($forum['active']!="1")
            {
                $fh->va->return = false;
                $fh->va->MessageSet("error","comments_not_allowed");
            }
            else 
            {
                $approved = ($forum['approve_comments']=="1")? 0:1;
                switch($forum['comments'])
                {
                    case "0":
                        $fh->va->return = false;
                        $fh->va->MessageSet("error","comments_not_allowed");
                    break;
                    case "1":
                    case "2":
                        if(!$id_p>0)
                        {
                            $fh->va->MessageSet("error","register_before_comment");
                            $fh->va->return = false;
                        }
                    break;
                    case "3":
                        if(!$id_p>0)
                        {
                            $fh->va->NotEmpty($name,"name");
                            $fh->va->Email($email);
                            $verify_email = false;
                        }
                    break;
                }
            }
        break;
        case 20:
            include_once(SERVER_ROOT."/../classes/polls.php");
            $pl = new Polls();
            $question = $pl->QuestionGet($id_item);
            $poll = $pl->PollGet($question['id_poll']);
            $approved = ($poll['moderate_comments']=="1")? 0:1;
            switch($poll['comments'])
            {
                case "0":
                    $fh->va->return = false;
                    $fh->va->MessageSet("error","comments_not_allowed");
                break;
                case "1":
                case "2":
                    if(!$id_p>0)
                    {
                        $fh->va->MessageSet("error","register_before_comment");
                        $fh->va->return = false;
                    }
                break;
                case "3":
                    if(!$id_p>0)
                    {
                        $fh->va->NotEmpty($name,"name");
                        $fh->va->Email($email);
                        $verify_email = false;
                    }
                break;
            }
        break;
    }
    $fh->va->NotEmpty($title,"title");
    $fh->va->NotEmpty($comment,"comment");
	$fh->va->Captcha($post);
    $fh->va->NoForumSpam($email);
    if ($fh->va->return) 
    {
        include_once(SERVER_ROOT."/../classes/resources.php");
        $r = new Resources();
        $type = $r->TypeLookup($id_r);
        include_once(SERVER_ROOT."/../classes/comments.php");
        $co = new Comments($type,$id_item);
        include_once(SERVER_ROOT."/../classes/varia.php");
        $v = new Varia();
        $names = explode(" ",$name,2);
        $id_p = ($id_p>0)? $id_p : $fh->UserCreate($names[0],$names[1],$email,0,$id_topic,$verify_email);
        $co->CommentInsertPublic( $id_parent,$title,$comment,$id_p,$id_topic,$approved,$v->Serialize(array()) );
        $subtype = "browse";
        if($approved)
            $fh->va->MessageSet("notice","comment_approved");
        else
            $fh->va->MessageSet("notice","comment_to_be_approved");
    }
    $url = "comment.php?id_r=$id_r&id_item=$id_item&subtype=$subtype";
    if ($id_topic>0)
        $url .= "&id_topic=$id_topic";
    header("Location: $url");
}
?>
