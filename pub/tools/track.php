<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$url = $get['u'];
$title = $get['t'];
$width = $get['w'];
$height = $get['h'];
$color = $get['c'];
$referer = $get['r'];
$id_topic = $get['id_t'];
$id_topic_group = $get['id_tg'];
$cd = (int)$get['cd'];

include_once(SERVER_ROOT."/../classes/tracker.php");
$tk = new Tracker();
$tk->Track($cd,$url,$title,$width,$height,$color,"","","",$referer,$id_topic,$id_topic_group);

include_once(SERVER_ROOT."/../classes/ini.php");
$ini = new Ini;
$pub_web = $ini->Get('pub_web');
header("Location: $pub_web/logos/error.gif");
?>
