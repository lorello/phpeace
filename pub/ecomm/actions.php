<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");
$fh = new FormHelper(true,27,false);
$post = $fh->HttpPost(true);

$action	= $post['action'];
$from		= $post['from'];
$id_topic = isset($post['id_topic']) ? (int)$post['id_topic'] : 0;

if ($from=="product_purchase")
{
	$id_p = $fh->IsThereUser();
	$id_product	= (int)$post['id_product'];
	$phone			= $post['phone'];
	$address		= $post['address'];
	$postcode		= $post['postcode'];
	$town			= $post['town'];
	$address_notes	= $post['address_notes'];
	$id_geo			= (int)$post['id_geo'];
	$delivery_notes	= $post['delivery_notes'];

	$fh->va->NotEmpty($address,"address");
	$fh->va->NotEmpty($town,"town");
	$fh->va->GreaterThenZero($id_geo,"country");
	
	$url = "order_preview.php?id=$id_product&id_topic=$id_topic";
	if($fh->va->return && $id_p>0 && $id_product>0)
	{
		// create order
		$ec = new eCommerce();
		$quantity = 1;
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$pe->UserUpdateDetails2($id_p,$address,$address_notes,$postcode,$town,$id_geo,$phone);
        include_once(SERVER_ROOT."/../classes/session.php");
        $session = new Session();
        if ($session->IsVarSet('shopping_cart'))
        {
            $id_order = $ec->OrderMultiProductCreate($id_p,$delivery_notes,$session->Get('shopping_cart'));
			if($id_order>0)
			{
	            $session->Delete('shopping_cart');
	            $session->Set("shopping_cart_order_id", $id_order);
				$url_payment = $ec->PaymentProcess($id_p,$id_order,$id_topic);
				if($url_payment!="")
				{
					$url = $url_payment;
				}
			}
        }
	}
	header("Location: $url ");
}

if ($from=="order_pay")
{
	$id_p = $fh->IsThereUser();
	$id_order	= (int)$post['id_order'];
	
	$url = "order.php?id=$id_order&id_topic=$id_topic";
	if($id_p>0 && $id_order>0)
	{
		$ec = new eCommerce();
		if($ec->IsUserAuthorized($id_p,$id_order))
		{
			$url_payment = $ec->PaymentProcess($id_p,$id_order,$id_topic);
			if($url_payment!="")
			{
				$url = $url_payment;
			}
		}
	}
	header("Location: $url ");
}
?>
