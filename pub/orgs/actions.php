<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$fh = new FormHelper(true,8,false);

$fh->va->NoSpam("website");

$post = $fh->HttpPost();

$from = $post['from'];

if ($from=="insert")
{
	$id_asso_type	= $post['id_asso_type'];
	$name		= $post['name'];
	$name2	= $post['name2'];
	$address	= $post['address'];
	$postcode	= $post['postcode'];
	$town		= $post['town'];
	$id_geo	= $fh->Null2Zero($post['id_geo']);
	$notes		= $post['notes'];
	$phone	= $post['phone'];
	$fax		= $post['fax'];
	$email		= $post['email'];
	$website	= $fh->String2Url($post['website']);
	$contact_name = $post['contact_name'];
	$in_charge_of	= $fh->Checkbox2Bool($post['in_charge_of']);
	$name_p	= $post['name_p'];
	$email_p	= $post['email_p'];
	$fh->va->GreaterThenZero($id_asso_type,"asso_type");
	$fh->va->NotEmpty($name,"name");
	if(isset($post['email']))
	{
		$fh->va->Email($email);
	}
    $fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserInfo(false);
	if(!$user['id']>0)
	{
		$fh->va->NotEmpty($name_p,"name_p");
		$fh->va->Email($email_p,"email_p");
	}
	if($fh->va->return)
	{
		$asso = new Asso(0);
		$as = new Assos();
		$unescaped_params	= $fh->SerializeParams(false);
		$id_new = $asso->InsertPub($id_asso_type,$name,$name2,$address,$postcode,$town,$id_geo,$phone,$fax,$email,$website,$contact_name,$in_charge_of,$email_p,$name_p,$as->id_topic,$notes,$unescaped_params);
		if($as->orgs_upload)
		{
			$file		= $fh->UploadedFile("img",true);
			if ($file['ok'])
				$asso->ImageUpdate($file);
		}
		$session = new Session();
		$session->Set("res",$id_new);
		$url_after_insert = $as->url_after_insert;
		if($url_after_insert!="")
		{
			$fh->va->FeedbackAdd("notice","thanks");	
			header("Location: $url_after_insert");
		}
		else
		{
			$keywords = $as->Keywords();
			if(count($keywords)>0)
			{
				header("Location: insert.php?id=$id_new");
			}
			else
			{
				$fh->va->FeedbackAdd("notice","thanks");	
				header("Location: index.php");
			}
		}
	}
	else
		header("Location: insert.php?");
}

if($from=="insert_keywords")
{
	$id_asso	= $post['id_asso'];
	$as = new Assos();
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$keywords = $as->Keywords();
	$kws = "";
	$num_params = 0;
	foreach($keywords as $keyword)
	{
		if ($fh->Checkbox2Bool($post[ 'k'.$keyword['id_keyword']]))
		{
			$kws .= $keyword['keyword'] . ",";
			$kparams = $k->Params($keyword['id_keyword'],5,true);
			$num_params += count($kparams);
		}
	}
	$kws = rtrim($kws,",");
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	$o->InsertKeywords($fh->SqlQuote($kws,false), $id_asso, $o->types['org']);
	if($num_params>0)
	{
		header("Location: insert.php?id=$id_asso&kparams=1");
	}
	else 
	{
		$session = new Session();
		$session->Delete("res");
		$fh->va->FeedbackAdd("notice","thanks");	
		header("Location: index.php");
	}
}

if($from=="insert_keyword_params")
{
	$id_asso	= $post['id_asso'];
	$unescaped_params	= $fh->SerializeParams(false);
	$asso = new Asso($id_asso);
	$asso->KeywordParamsUpdate($unescaped_params,true);
	$session = new Session();
	$session->Delete("res");
	$fh->va->FeedbackAdd("notice","thanks");	
	header("Location: index.php");
}

?>

