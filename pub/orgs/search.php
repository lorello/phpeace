<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();
$l = new Layout();

$unescaped_get = $fh->HttpGet(false);

$params = array();
$params['id_module'] = 8;
$params['module'] = "orgs";
$params['from'] = $unescaped_get['from'];
$params['name'] = $unescaped_get['name'];
$params['text'] = $unescaped_get['text'];
$params['town'] = $unescaped_get['town'];
$params['address'] = $unescaped_get['address'];
$params['id_assotype'] = (int)$unescaped_get['id_assotype'];
$params['id_geo'] = (int)$unescaped_get['id_geo'];
$params['id_k'] = (int)$unescaped_get['id_k'];
$params['advanced'] = (int)$get['advanced'];
$params['subtype'] = "search";

foreach($unescaped_get as $key=>$value)
{
	if (substr($key,0,3)=="kp_" && $value!="")
	{
		$params[$key] = $value;
	}
}

$page = (int)$get['p'];

include_once(SERVER_ROOT."/../modules/assos.php");
$as = new Assos();

echo $l->Output("common",1,$as->id_topic,$page,$params);
?>
