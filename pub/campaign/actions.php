<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/campaign.php");
$fh = new FormHelper(true,4,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);
$unescaped_post = $fh->HttpPost(false,false);

$action	= $post['action'];
$from		= $post['from'];

if ($action=="insert")
{
	$id_campaign	= $post['id_campaign'];
	$id_topic	= $post['id_topic'];
	$comments	= $post['comments'];
	$email		= $post['email'];
	$name		= $post['name'];
	$surname	= $post['surname'];
	$contact	= $fh->Checkbox2bool($post['contact']);
	$privacy	= $fh->Checkbox2bool($post['privacy']);
	
	$c = new Campaign($id_campaign,$from);
	$redirect = "index";
	if (!$fh->va->FirstTime($c->session_var))
	{
		$fh->va->MessageSet("error","already_signed");
	}
	$fh->va->NotEmpty($name,"name");
	$fh->va->Email($email);
	$fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	if (!$fh->va->IsTrue($privacy))
		$fh->va->MessageSet("error","error_privacy");
	if($fh->va->return)
	{
		$id_p = $fh->IsThereUser();
		$row = $c->CampaignGet();
		if($row['active']=="1")
		{
			if($row['users_type']=="0" && !$id_p>0)
			{
				$fh->va->MessageSet("error","login_first");
				$url = $fh->LoginUrl();
			}
			else 
			{
				$comment_approved = ($comments!="" && $row['approve_comments'])? 0:1;
				$id_geo	= $post['id_geo'];
				$amount	= $fh->String2Number($post['amount']);
				$amount2	= $fh->String2Number($post['amount2']);
				$amount	= ($amount2>0)? $amount2 : $fh->String2Number($post['amount']);
				$currency	= $post['currency'];
				$id_account	= $post['id_account'];
				$thanks	= 1;
				if ($from=="person")
				{
					$unescaped_params = $fh->SerializeParams(false);
					if(!$id_p>0)
					{
						$id_p = $c->UserCreate($name,$surname,$email,$id_geo,$unescaped_params,$contact,$row['id_topic'],true);
					}
					if($c->PersonInsert($id_p,$comments,$comment_approved,$contact))
					{
					    $c->SignatureSessionSet();
					    if($row['thanks_email']!="" || $row['notify_text']!="")
							$c->NotifySignature("$name $surname",$email,$id_p,$id_geo);
					}
					else
					{
						$fh->va->MessageSet("error","already_signed_email",array($email));
						$thanks = 0;
					}
					$redirect = "person";
				}
				if ($from=="org" && $row['orgs_sign'])
				{
					$contact_main	= $unescaped_post['contact_main'];
					$address	= $post['address'];
					$phone	= $post['phone'];
					$website	= $fh->String2Url($unescaped_post['website']);
					$unescaped_params = (array('web'=>$website,'contact'=>$contact_main));
					$id_p = $c->UserCreate($name,"",$email,$id_geo,$unescaped_params,$contact,$row['id_topic'],true,$phone,$address);
					if($c->OrgInsert($id_p,$comments,$comment_approved,$contact))
					{
					    $c->SignatureSessionSet();
					    if($row['thanks_email']!="" || $row['notify_text']!="")
							$c->NotifySignature($name,$email,$id_p,$id_geo);
					}
					else
					{
						$fh->va->MessageSet("error","already_signed_email",array($email));
						$thanks = 0;
					}
					$redirect = "org";
				}
				if($id_p>0 && $row['money'] && $amount>0)
				{
					$c->DonationInsert($id_p,$amount,$id_account,$currency);
				}
				$url = "index.php?id=$id_campaign&thanks=$thanks&ca=$comment_approved";
			}
		}
		else 
		{
				$url = "index.php?id=$id_campaign";
		}
	}
	else
	{
		$url = "$redirect.php?id=$id_campaign" ;
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

if ($action=="propose")
{
	$url = "index.php?id=0";
	$id_topic	= $post['id_topic'];
	$title		= $post['title'];
	$description	= $post['description'];
	$name		= $post['name'];
	$email		= $post['email'];
	$privacy	= $fh->Checkbox2bool($post['privacy']);
	$fh->va->NotEmpty($title,"title");
	$fh->va->NotEmpty($name,"name");
	$fh->va->Email($email);
	$fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	if (!$fh->va->IsTrue($privacy))
		$fh->va->MessageSet("error","error_privacy");
	if($fh->va->return)
	{
		$id_p = $fh->IsThereUser();
		$c = new Campaign(0,true);
		$db =& Db::globaldb();
		$today = $db->getTodayDate();
		$c->CampaignInsert($id_topic,$today,$title,"","",0,1,$description,0,1,0,"",0,"",$id_p,$name,"",$email,"","",0);
		$fh->va->MessageSet("notice","thanks_campaign");
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

?>
