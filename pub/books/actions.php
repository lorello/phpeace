<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,16,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);

$from = $post['from'];

if ($from=="review")
{
	$name		= $post['name'];
	$email		= $post['email'];
	$vote		= $fh->Null2Zero($post['vote']);
	$review	= $post['review'];
	$id_topic 	= (int)$post['id_topic'];
	$id_book 	= $post['id_book'];
	$id_p = $fh->IsThereUser();
	$unescaped_params	= $fh->SerializeParams(false);
	include_once(SERVER_ROOT."/../classes/ini.php");
	$ini = new Ini();
	if($id_p>0)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$row = $pe->UserGetById($id_p);
		if($row['id_language']>0)
			$id_language = $row['id_language'];
	}
	elseif($id_topic>0)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$id_language = $t->id_language;
	}
	else
		$id_language = $ini->Get("id_language");
	switch($ini->Get("books_reviews"))
	{
		case "0":
			$fh->va->return = false;
			$fh->va->MessageSet("error","reviews_not_allowed");
		break;
		case "1":
			if(!$id_p>0)
			{
				$fh->va->MessageSet("error","register_before_review");
				$fh->va->return = false;
			}
		case "2":
			if(!$id_p>0)
			{
				$fh->va->NotEmpty($name,"name");
				$fh->va->Email($email);
			}
		break;
	}
	$fh->va->Captcha($post);
	$fh->va->NotEmpty($review,"review");
	$fh->va->NoForumSpam($email);
	if ($fh->va->return) 
	{
		include_once(SERVER_ROOT."/../modules/books.php");
		$b = new Book($id_book);
		$b->ReviewInsertPublic($review,$vote,$name,$email,$id_p,$id_language,$unescaped_params,$id_topic);
		$fh->va->MessageSet("notice","review_to_be_approved");
		$url = "index.php?id=$id_book";
	}
	else
		$url = "index.php?id=$id_book&reviews=insert";
	if ($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url");
}

?>

