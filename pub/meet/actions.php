<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
$fh = new FormHelper(true,23,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);

$action	= $post['action'];
$from		= $post['from'];

if ($from=="meeting" && $action=="subscribe")
{
	$id_meeting	= $post['id_meeting'];
	$id_topic	= $post['id_topic'];
	$url = "index.php?id=$id_meeting";
	$me = new Meetings();
	if($id_meeting>0)
	{
		$row = $me->MeetingGet($id_meeting);
		$id_p = $fh->IsThereUser();
		if($id_p>0)
		{
			// check that $id_p already requested but not approved yet
			if($me->IsUserPending($id_meeting,$id_p))
			{
				$fh->va->return = false;
				$fh->va->MessageSet("error","pending_approval");
			}
			if($row['is_private']=="1")
			{
				$fh->va->return = false;
				$fh->va->MessageSet("error","not_authorized");
			}
			if($fh->va->return)
			{
				$id_topic = $row['id_topic'];
				$slots = array();
				$me->MeetingSlots($slots,$id_meeting,false);
				foreach($slots as $slot)
				{
					$slot_status = (int)$post['slot'.$slot['id_meeting_slot'].'_join'];
					$slot_comments = $post['slot'.$slot['id_meeting_slot'].'_comments'];
					$me->ParticipantUpdate($slot['id_meeting_slot'],$id_p,$slot_status,$slot_comments,$id_meeting);
				}
				$fh->va->MessageSet("notice","thank_you");
			}
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}


?>
