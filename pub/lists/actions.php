<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,11,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);

$from = $post['from'];

if ($from=="list")
{
	$action = $fh->ActionGet($post);
	$email		= $post['email'];
	$id_topic 	= (int)$post['id_topic'];
	$id_list 	= $post['id_list'];
	$fh->va->Email($email);
	$fh->va->GreaterThenZero($id_list);
	$fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	if ($fh->va->return && ($action=="subscribe" || $action=="unsubscribe")  )
	{
		include_once(SERVER_ROOT."/../modules/list.php");
		$ml = new MailingList($id_list);
		$ml->Request($action,$email);
		$fh->va->MessageSet("notice",$action . "_" . $ml->impl,array($email,$ml->email));
	}
	$url = "index.php?id=$id_list";
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url");
}

?>
