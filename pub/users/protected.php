<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,4,false);
$get = $fh->HttpGet();

include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout(true,true);

$id_topic = (int)$get['id_topic'];

$user = $l->PeopleUser();
$id_p = (int)$user['id'];
$is_auth = $id_p>0 && $user['auth'];

if($id_topic>0)
{
	$l->TopicInit($id_topic);
	if($l->topic->row['protected']=="2")
	{
		if($is_auth)
		{
			$l->topic->IsVisitorAuthorized($id_p);
		}
		else 
		{
			$l->Stop("pls login");
		}
	}
	else 
	{
		$topics = $user['topics'];
		if(!is_array($topics) || !in_array($id_topic,$topics))
			$l->Stop("subscribers only");
	}
}
elseif(!$is_auth)
	$l->Stop("no auth");

$id_gtype = $get['id_gtype'];
$id_type = $get['id_type'];
$id = (int)$get['id'];
$page = (int)$get['p'];
$params = array();
foreach($get as $key=>$value)
{
	if ($value!="")
	{
		$params[$key] = $value;
		if ($key=="offset")
			$params['ts'] = time() + ($value*86400);
	}
}

$unescaped_get = $fh->HttpGet(false);
$params['q'] = $unescaped_get['q'];

if($get['id_style']>0)
	$params['id_style'] = (int)$get['id_style'];

if (!isset($id_gtype) && !isset($id_type))
	$id_gtype = 0;

if (isset($id_gtype))
{
	$xml = $l->PageTypeGlobal($id_gtype,$id,$page,$params);
	$type = array_search($id_gtype,$l->pt->gtypes);
}
else
{
	$xml = $l->PageType($id_type,$id_topic,$id,$page,$params);
	$type = ($params['module']!="")? $params['module'] : array_search($id_type,$l->pt->types);
}

if (isset($id_gtype) && $id_gtype==1)
	header('Content-type: text/xml');

$html = $l->xh->Transform($type,$xml,$l->id_style);
$l->DoctypeHTML5Fix($html);
echo $html;
?>
