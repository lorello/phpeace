<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/people.php");
$fh = new FormHelper(true,28,false);
$post = $fh->HttpPost();

$from = $post['from'];
$url = "index.php?";


if ($from=="login")
{
	$email		= $post['email'];
	$password	= $post['password'];
	$security	= (int)$post['security'];
	$id_topic 	= $post['id_topic'];
	$url = "login.php?" . (($id_topic>0)? "id_topic=$id_topic":"");
	$fh->va->Email($email);
	$fh->va->NoForumSpam($email);
	$jumpback = false;
	if ($fh->va->return) 
	{
		$pe = new People();
		$user = $pe->UserGetByEmail($email);
		if ($user['id_p']>0)
		{
			if($user['active']=="1")
			{
				if($fh->va->return && $pe->AuthAndSet($user['id_p'],$password,$user['identifier'],$user['token'],$security==1, $id_topic))
				{
					$pe->CounterReset();
					$url = "index.php?" . (($id_topic>0)? "id_topic=$id_topic":"");
	
					if($security==0)
					{
						$fh->va->MessageSet("notice","remember_logout");
					}
	
					$jumpback = $fh->JumpBackGet();
					if($jumpback != "")
					{
						$url = $jumpback;
					}
				}
				else
				{
					$counter = $pe->CounterGet();
					if($counter == 3)
					{
						$pe->SendLoginFailAttemptEmail($email, $counter, $id_topic);
					}
					$fh->va->MessageSet("error","auth_error");
					$pe->CounterIncrement();
				}			
			}
			else 
			{
				$fh->va->MessageSet("error","deactivate_warning");
			}
		}
		else
		{
			$counter = $pe->CounterGet();
			if($counter == 3)
				$pe->SendLoginFailAttemptEmail($email, $counter, $id_topic);
			$fh->va->MessageSet("error","auth_error");
			$pe->CounterIncrement();
		}
	}
	header("Location: $url");
}

if ($from=="password")
{
	$password_old	= $post['password_old'];
	$password_new	= $post['password_new'];
	$password_verify	= $post['password_verify'];
	$id_topic 	= $post['id_topic'];
	$fh->va->Password($password_old);
	$fh->va->Password($password_new);
	$fh->va->PasswordsEqual($password_new,$password_verify);
	if ($fh->va->return) 
	{
		$pe = new People();
		$change = $pe->PasswordChange($password_old,$password_new);
		if($change)
		{
			$fh->va->MessageSet("notice","password_changed");
			$url = "index.php";
		}
		else 
		{
			$fh->va->MessageSet("error","auth_error");
			$url = "password.php";
		}
	}
	else
		$url = "password.php";
	$url .= ($id_topic>0)? "?id_topic=$id_topic":"";
	header("Location: $url");
}

if ($from=="register")
{
	$url = "register.php";
	$name		= $post['name'];
	$surname	= $post['surname'];
	$email		= $post['email'];
	$security	= (int)$post['security'];
	$password	= $post['password'];
	$password_verify	= $post['password_verify'];
	$id_topic 	= $post['id_topic'];
	$privacy	= $fh->Checkbox2Bool($post['privacy']);
	$subscribe	= $fh->Checkbox2Bool($post['subscribe']);
	$id_pt_groups 	= $post['id_pt_groups'];
    $link_acc = false;
    if (array_key_exists('link_account', $post))
        $link_acc = $fh->Checkbox2Bool($post['link_account']);

	$fh->va->NotEmpty($name,"name");
	$fh->va->Email($email);
	$fh->va->Password($password);
	$fh->va->PasswordsEqual($password,$password_verify);
	$fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	if (!$fh->va->IsTrue($privacy))
		$fh->va->MessageSet("error","error_privacy");

	if ($fh->va->return) 
	{
		$pe = new People();
		$user = $pe->UserGetByEmail($email);
		if ($pe->IsUserAlreadyRegistered($email))
		{
			$fh->HttpPost(true);
			$fh->va->MessageSet("error","error_email_in_use",array()); 
		}
		else
		{
			$id_language = $pe->IdLanguage($id_topic);
			$unescaped_params = $fh->SerializeParams(false,",",true);
			$id_p = $pe->Register($name,$surname,$email,$password,true,0,$id_language,$unescaped_params,true,1,false);
			if($id_p>0)
			{
				$contact_topic = 0;
				if($subscribe)
				{
					$pe->PortalAssociate($id_p);
					if($id_topic>0)
					{
						include_once(SERVER_ROOT."/../classes/topic.php");
						$t = new Topic($id_topic);
						if($t->profiling)
						{
							$pe->TopicAssociate($id_p,$id_topic,1);
						}
					}
				}
				if(is_array($id_pt_groups) && count($id_pt_groups)>0)
				{
					foreach($id_pt_groups as $id_pt_group)
					{
						if(is_numeric($id_pt_group) && $id_pt_group>0)
							$pe->GroupAdd($id_p,$id_pt_group);
					}
				}
				$fh->va->MessageSet("notice","registration_ok_but_check");
				$jumpback = $fh->JumpBackGet();
				$ret = $pe->VerifyEmail($id_p,$id_topic,false,true,$jumpback);
				if($ret>0)
					$fh->va->MessageSet("error","error_check" . $ret);
				else
					$fh->va->MessageSet("notice","registration_check_sent",array($email));
				$url = "index.php?";
				if($jumpback!="")
				{
					$url = $jumpback;
				}
			}
			else
				$fh->va->MessageSet("error","registration_ko");
		}
	}
	else
	{
		$fh->HttpPost(true);
	}
	
	if ($id_topic>0 && strpos($url,"id_topic")===false)
		$url .= ((strpos($url,"?")===false)? "?":"&") . "id_topic=$id_topic";
	
	header("Location: $url");
}

if ($from=="reminder")
{
	$email		= $post['email'];
	$id_topic 	= $post['id_topic'];
	$fh->va->Email($email);
    $fh->va->Captcha($post);
	$fh->va->NoForumSpam();
	if($fh->va->return)
	{
		$pe = new People();
		$sent = $pe->Reminder($email,$id_topic);
		if($sent)
			$fh->va->MessageSet("notice","reminder_sent",array($email));
		else
			$fh->va->MessageSet("error","email_not_found",array($email));
	}
	else
		$url = "reminder.php?";
	$url .= ($id_topic>0)? "id_topic=$id_topic":"";
	header("Location: $url");
}

if ($from=="verify")
{
	$id_p = $fh->IsThereUser();
	$id_topic 	= $post['id_topic'];
	$pe = new People(); 
	$user = $pe->UserGetById($id_p);
	$ret = $pe->VerifyEmail($id_p,$id_topic);
	if($ret>0)
		$fh->va->MessageSet("error","error_check" . $ret);
	else
		$fh->va->MessageSet("notice","registration_check_sent",array($user['email']));
	$url .= ($id_topic>0)? "id_topic=$id_topic":"";
	header("Location: $url");
}

if ($from=="contact")
{
	$pe = new People();
	$user = $pe->UserInfo(false);
	$id_topic 	= $post['id_topic'];
	if($user['auth'])
	{
		$portal		= $fh->Checkbox2Bool($post['portal']);
		$topics 	= $post['topics'];
		$campaigns 	= is_array($post['campaigns'])? $post['campaigns'] : array();
		$pe->ContactUpdate($user['id'],$portal,$topics,$campaigns);
		$fh->va->MessageSet("notice","options_changed");
	}
	else 
		$fh->va->MessageSet("error","auth_error");
	$url = "mail.php";
	if ($id_topic>0)
		$url .=  "?id_topic=$id_topic";
	header("Location: $url");
}

if ($from=="data")
{
	$pe = new People();
	$user = $pe->UserInfo(false);
	$id_topic 	= $post['id_topic'];
    $url = "index.php";
	if($user['auth'])
	{
		$id_p = $user['id'];
		$name		= $post['name'];
		$surname	= $post['surname'];
		$email		= trim($post['email']);
		$phone		= $post['phone'];
		$address 	= $post['address'];
		$address_notes 	= $post['address_notes'];
		$postcode		= $post['postcode'];
		$town		= $post['town'];
		$id_geo		= $post['id_geo'];
		$fh->va->NotEmpty($name,"name");
		$fh->va->Email($email);
		if ($fh->va->return) 
		{
            if($pe->PreEmailValidation($id_p, $email))
            {
                $email_changed = $pe->UserUpdatePub($id_p,$name,$surname,$email,$phone,$address,$address_notes,$postcode,$town,$id_geo);
                $user_params = $fh->SerializeParams(false);
				if(count($user_params)>0)
					$pe->UserParamsUpdate($id_p, $user_params);
                $fh->va->MessageSet("notice","data_changed",array('tra'=>true));    
                if($email_changed)
                {
                    $fh->va->MessageSet("notice","registration_check_sent",array($email));
                    $pe->VerifyEmail($id_p,$id_topic);
                }
            }
	        else
            {
                $url = "data.php";
                $fh->va->FeedbackAdd("error", "error_email_in_use", array());
            }
		}
	}
	else 
		$fh->va->MessageSet("error","auth_error");
	if ($id_topic>0)
		$url .=  "?id_topic=$id_topic";
	header("Location: $url");
}

if ($from=="cancel")
{
	$action = $fh->ActionGet($post);
	$id_topic 	= $post['id_topic'];
    $url = "data.php";
	$id_p = $fh->IsThereUser(true);
    if($action=="cancel_yes" && $id_p>0)
	{
		$pe = new People();
		$pe->Delete($id_p,false);
		$pe->Logout();
		$fh->va->MessageSet("notice","deactivate_warning");
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$url = $ini->Get('pub_web');
	}
	elseif($id_topic>0)
		$url .=  "?id_topic=$id_topic";
	header("Location: $url");
}
?>
