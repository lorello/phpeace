<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,6,false);
$post = $fh->HttpPost();

$fh->va->NoSpam();

$from	= $post['from'];

if ($from=="quote")
{
	include_once(SERVER_ROOT."/../classes/quotes.php");
	$quote 	= $post['quote'];
	$author 	= $post['author'];
	$author_notes	= $post['author_notes'];
	$id_language	= $fh->Null2Zero($post['id_language']);
	if(!$id_language>0)
		$id_language 	= $fh->va->tr->id_language;
	$id_topic 	= $fh->Null2Zero($post['id_topic']);
	$id_quote = 0;
	$fh->va->NotEmpty($quote,"quote");
	$fh->va->NotEmpty($author,"author");
    $fh->va->Captcha($post);
	$fh->va->NoForumSpam();
	if($fh->va->return)
	{
		$q = new Quotes;
		$id_quote = $q->QuoteInsert($quote, $author, $author_notes, 0, "", 0, $id_topic);
	}
	header("Location: insert.php?id_topic=$id_topic&id=$id_quote");
}

?>
