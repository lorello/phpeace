<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/polls.php");
$fh = new FormHelper(true,4,false);
$fh->va->NoSpam();
$post = $fh->HttpPost(true);

$action	= $post['action'];
$from		= $post['from'];

if ($action=="vote")
{
	$id_poll	= $post['id_poll'];
	$id_topic	= $post['id_topic'];
	$url = "index.php?id=$id_poll";
	$pl = new Polls();
	if($id_poll>0)
	{
		$row = $pl->PollGet($id_poll);
		$id_p = $fh->IsThereUser();
		// check number of answers > 0
		switch($row['id_type'])
		{
			case "0":
				$id_question	= $post['id_question'];
				if (!$fh->va->FirstTime($pl->SessionVar($id_poll)))
					$fh->va->MessageSet("error","already_voted");
				else
				{
					$fh->va->GreaterThenZero($id_question);
					if(!$fh->va->return)
					{
						$fh->va->MessageSet("error","no_question_selected");
					}
				}
			break;
			case "1":
				$questions = $post['id_question'];
				if (!$fh->va->FirstTime($pl->SessionVar($id_poll)))
					$fh->va->MessageSet("error","already_voted");
				elseif(!$fh->va->IsTrue(is_array($questions) && count($questions)>0))
					$fh->va->MessageSet("error","no_question_selected");
			break;
			case "2":
				$question_found = false;
				$questions = array();
				$pl->Questions($questions,$id_poll,$row['sort_questions_by']);
				foreach($questions as $question)
				{
					$vote = (int)$post['vote_' . $question['id_question']];
					if($vote >0)
					{
						$question_found = true;
						if($id_p>0 && $pl->VoteCheck($id_p,$id_poll,$row['id_type'],$question['id_question']))
						{
							$fh->va->return = false;
							$fh->va->MessageSet("error","already_voted");
						}
					}
				}
				if(!$fh->va->IsTrue($question_found))
					$fh->va->MessageSet("error","no_question_selected");
			break;
			case "3":
				$id_question	= $post['id_question'];
				$vote	= $post['vote'];
				$fh->va->GreaterThenZero($id_question);
				$fh->va->GreaterThenZero($vote);
				if(!$fh->va->return)
				{
					$fh->va->MessageSet("error","no_question_selected");
					$url = "question.php?id=$id_poll&id_question=$id_question";
				}
				elseif($id_p>0 && $pl->VoteCheck($id_p,$id_poll,$row['id_type'],$id_question))
				{
					$fh->va->return = false;
					$fh->va->MessageSet("error","already_voted");
				}
			break;
		}
    	$fh->va->Captcha($post);
		$fh->va->NoForumSpam();
		if($fh->va->return)
		{

			$lets_vote = false;
			$id_topic = $row['id_topic'];
			if($row['users_type']=="0" || $row['active']!="1")
			{
				$fh->va->MessageSet("error","poll_closed");
			}
			elseif($row['users_type']=="1") 
			{
				$id_p = $fh->IsThereUser(true);
				if($id_p>0)
				{
					if($pl->VoteCheck($id_p,$id_poll,$row['id_type']))
						$fh->va->MessageSet("error","already_voted");
					else
						$lets_vote = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_authenticate");
					$url = $fh->LoginUrl();
				}
			}
			elseif($row['users_type']=="2") 
			{
				if($id_p>0)
				{
					if($pl->VoteCheck($id_p,$id_poll,$row['id_type']))
						$fh->va->MessageSet("error","already_voted");
					else
						$lets_vote = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_register");
					$url = $fh->LoginUrl();
				}
			}
			elseif($row['users_type']=="3") 
			{
				$cookie = $_COOKIE["poll_" . $id_poll];
				if($id_p>0 || $cookie=="")
				{
					if($pl->VoteCheck($id_p,$id_poll,$row['id_type']))
						$fh->va->MessageSet("error","already_voted");
					else
						$lets_vote = true;
				}
				else 
				{
					$lets_vote = true;
				}
			}
			
			if($lets_vote)
			{
				switch($row['id_type'])
				{
					case "0":
						$pl->Vote($id_poll,$id_question,$id_p,1);
					break;
					case "1":
						if(is_array($questions))
						{
							foreach($questions as $id_question)
								$pl->Vote($id_poll,$id_question,$id_p,1);
						}
					break;
					case "2":
						$questions = array();
						$pl->Questions($questions,$id_poll,$row['sort_questions_by']);
						$vote = 0;
						foreach($questions as $question)
						{
							$vote = (int)$post['vote_' . $question['id_question']];
							if($vote >0)
							{
								$pl->Vote($id_poll,$question['id_question'],$id_p,$vote);
							}
						}
					break;
					case "3":
						$pl->Vote($id_poll,$id_question,$id_p,$vote);
					break;
				}
				$url .= "&thanks=1";
			}
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

if ($action=="group")
{
	$id_poll	= $fh->String2Number($post['id_poll']);
	$id_topic	= $fh->String2Number($post['id_topic']);
	$id_questions_group	= $fh->String2Number($post['id_questions_group']);
	$seq	= 	$fh->String2Number($post['seq']);
	$id_p = $fh->IsThereUser();
	$url = "index.php?id=$id_poll";
	$pl = new Polls();
	if($id_poll>0)
	{
		$repeat = $fh->Checkbox2Bool($post['repeat']);
		if($repeat && $id_p>0)
		{
			$pl->VoteDelete($id_p,$id_poll);
		}
		$row = $pl->PollGet($id_poll);
		if($id_p>0 && $pl->VoteCheck($id_p,$id_poll,$row['id_type']))
		{
			$fh->va->MessageSet("error","already_voted");
		}
		else
		{
			$action = $fh->ActionGet($post);
			
			$user_params = $fh->SerializeParams(false,"|");
			if(count($user_params)>0)
			{
				$pl->PersonParamsSessionSet($id_poll,$user_params);
			}
			
			$groups = array();
			$num_groups = $pl->QuestionsGroups($groups,$id_poll);
			if($num_groups>0)
			{
				$questions = array();
				if($seq>0 && $id_questions_group>0)
				{
					$pl->Questions($questions,$id_poll,1,1,$id_questions_group);
					foreach($questions as $question)
					{
						$varname = "id_question".$question['id_question'];
						if(isset($post[$varname]))
						{
							$pl->QuestionSessionSet($id_poll,$question['id_question'],$post[$varname]);
						}
						elseif($action=="next") 
						{
							$fh->va->MessageSet("error","\"{$question['question']}\": " . $fh->va->tr->Translate("no_question_selected"));
							$fh->va->return = false;
						}
					}
				}
				if($fh->va->return)
				{
					$next_id_questions_group = $pl->QuestionsGroupGetnext($id_poll,$seq,$action=="next");
					if($next_id_questions_group>0)
					{
						$url = "group.php?id=$id_poll&id_questions_group=$next_id_questions_group";
					}
					else
					{
						if($seq==1 && $action=="previous") 
						{
							$url = "index.php?id=$id_poll";
						}
						else
						{
							$url = "register.php?id=$id_poll";
						}
					}
				}
				else 
				{
					$url = "group.php?id=$id_poll&id_questions_group=$id_questions_group";
				}
			}
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

if ($action=="propose")
{
	$id_poll	= $post['id_poll'];
	$id_topic	= $post['id_topic'];
	$url = "question.php?id=$id_poll";
	$pl = new Polls();
	if($id_poll>0)
	{
		$row = $pl->PollGet($id_poll);
		$title	= $post['title'];
		$description	= $post['description'];
		$description_long	= $post['description_long'];
		$id_topic = $row['id_topic'];
		$fh->va->NotEmpty($title,"title");
    	$fh->va->Captcha($post);
		$fh->va->NoForumSpam();
		if($fh->va->return)
		{
			$id_p = $fh->IsThereUser();
			$accept_proposal = false;
			if($row['submit_questions']=="0" || $row['active']!="1")
			{
				$fh->va->MessageSet("error","poll_closed");
			}
			elseif($row['submit_questions']=="1") 
			{
				$id_p = $fh->IsThereUser(true);
				if($id_p>0)
				{
					$accept_proposal = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_authenticate");
					$url = $fh->LoginUrl();
				}
			}
			elseif($row['submit_questions']=="2") 
			{
				if($id_p>0)
				{
					$accept_proposal = true;
				}
				else 
				{
					$fh->va->MessageSet("error","must_register");
					$url = $fh->LoginUrl();
				}
			}
			elseif($row['submit_questions']=="3") 
			{
				$accept_proposal = true;
			}
			
			if($accept_proposal)
			{
				$pl->QuestionInsertPub($id_poll,$title,$description,$description_long,$id_p);
				$fh->va->MessageSet("notice","thanks_campaign");
				$url = "index.php?id=$id_poll";
			}
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

if ($action=="register")
{
	$id_poll	= $fh->String2Number($post['id_poll']);
	$id_topic	= $fh->String2Number($post['id_topic']);
	$id_p = $fh->IsThereUser();
	$name1	= $post['name1'];
	$name2	= $post['name2'];
	$email	= $post['email'];
	$url = "register.php?id=$id_poll";
	$pl = new Polls();
	$fh->va->Email($email);
    $fh->va->Captcha($post);
	$fh->va->NoForumSpam($email);
	if($id_poll>0 && $fh->va->return)
	{
		$row = $pl->PollGet($id_poll);
		$id_topic = $row['id_topic'];
		if(!$id_p>0)
		{
			$id_p = $fh->UserCreate($name1,$name2,$email,0,$id_topic,false,0,1);
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$pe->UserUpdateNameEmail($id_p,$name1,$name2,$email);
		}
		if($pl->VoteCheck($id_p,$id_poll,$row['id_type']))
		{
			$fh->va->MessageSet("error","already_voted");
		}
		else
		{
			$unescaped_post = $fh->HttpPost(true,false);
			$comments	= $unescaped_post['comments'];
			$pl->PersonParamsSessionStore($id_poll,$id_p);
			$user_params = $fh->SerializeParams(false);
			$pl->PersonParamsStore($id_p,$user_params);
			$pl->QuestionSessionStore($id_poll,$id_p);
			$pl->PromoterNotify($id_poll,$id_p,$comments);
			$pl->PersonTokenNotify($id_poll,$id_p);
			$url = "index.php?id=$id_poll&thanks=1";
		}
	}
	if($id_topic>0)
		$url .= "&id_topic=$id_topic";
	header("Location: $url ");
}

?>
