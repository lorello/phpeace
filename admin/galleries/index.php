<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

echo $hh->ShowTitle($title);

if($conf->Get("users_mode")=="shared" || $ah->ModuleAdmin(4))
{
	echo "<h3><a href=\"tree.php\">" . $hh->tr->Translate("galleries_tree") . "</a></h3>";
	echo "<h3><a href=\"galleries.php\">" . $hh->tr->Translate("galleries_all") . "</a></h3>\n";
}

$user = new User();
$rows = $user->Galleries();
if (count($rows)>0)
{
	echo "<h3>" . $hh->tr->Translate("galleries_administrator") . "</h3>\n<ul>";
	foreach($rows as $row)
		echo "<li><a href=\"ops.php?id={$row['id_gallery']}\">{$row['title']}</a></li>\n";
	echo "</ul>\n";
}
$rows = $user->GalleriesUser();
if (count($rows)>0)
{
	echo "<h3>" . $hh->tr->Translate("galleries_user") . "</h3>\n<ul>";
	foreach($rows as $row)
		echo "<li><a href=\"ops.php?id={$row['id_gallery']}\">{$row['title']}</a></li>\n";
	echo "</ul>\n";
}

if ($module_admin)
{
	include_once(SERVER_ROOT."/../classes/galleries.php");
	$gg = new Galleries();
	
	echo "<h3><a href=\"gallery.php?id=0\">" . $hh->tr->Translate("gallery_new") . "</a></h3>\n";
	if($conf->Get("users_mode")=="shared" || $ah->ModuleAdmin(4))
	{
		echo "<h3><a href=\"homepage.php?id=0\">" . $hh->tr->Translate("homepage") . "</a></h3>\n";
		$galleries = $gg->Homepage();
		if(count($galleries)>0)
		{
			echo "<ul>";
			foreach($galleries as $gallery)
			{
				echo "<li>{$gallery['name']}";
				if($gg->hometype=="1")
					echo " - (<a href=\"actions.php?from2=homepage&action3=delete&id={$gallery['id_item']}\">" . $hh->tr->Translate("remove") . "</a>)";
				echo "</li>";
			}
			echo "</ul>";
		}
		echo "<h3><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></h3>\n";
		
		echo $hh->input_code("galleries_html","galleries_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php\"></script>",60,2);
		echo $hh->input_code("galleries_html_random","galleries_html_random","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php?r=1\"></script>",60,2);

	}
}
include_once(SERVER_ROOT."/include/footer.php");
?>
