<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/galleries.php");

$title[] = array('homepage','');
echo $hh->ShowTitle($title);

echo "<p>" . $hh->tr->Translate("gallery_homepage_choose") ."</p>\n";

$gg = new Galleries();
$row = array();
$num = $gg->HomepageAvailable($row);
$table_headers = array('date','gallery','in','topic','images');
$table_content = array('{FormatDate($row[released_ts])}','{LinkTitle("actions.php?from2=homepage&action3=add&id=$row[id_gallery]",$row[title])}',
'{PathToGallery($row[id_group],$row[id_gallery])}',
'$row[topic_name]','<div class=\"right\">$row[counter]</div>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
