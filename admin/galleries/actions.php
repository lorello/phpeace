<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/gallery.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];
$action3	= $_GET['action3'];
$from2	= $_GET['from2'];

if ($from=="gallery")
{
	$action 	= $fh->ActionGet($post);
	$id_gallery 	= $post['id_gallery'];
	$visible 	= $fh->Checkbox2bool($post['visible']);
	$id_group	= $post['id_group'];
	$released	= $fh->DateMerge("released",$post);
	$show_date 	= $fh->Checkbox2bool($post['show_date']);
	$title 		= $post['title'];
	$description	= $post['description'];
	$note 		= $post['note'];
	$keywords	= $post['keywords'];
	$sort_order	= $post['sort_order'];
	$associated_image	= $post['associated_image'];
	$show_orig 	= $fh->Checkbox2bool($post['show_orig']);
	$show_author	= $fh->Checkbox2bool($post['show_author']);
	$share_images	= $fh->Checkbox2bool($post['share_images']);
	$captions_html	= $fh->Checkbox2bool($post['captions_html']);
	$topic_admin	= $fh->Checkbox2bool($post['topic_admin']);
	$author	= $post['author'];
	$id_licence	= $fh->Null2Zero($post['id_licence']);
	$id_topic	= $post['id_topic'];
	$id_user	= $post['id_user'];
	$topic_info	= $fh->TopicOrGroup($post['id_topic_or_group']);
	$conf = new Configuration();
	$images_per_page 	= $fh->String2Number($post['images_per_page'],$conf->Get("records_per_page"));
	$g	= new Gallery($id_gallery);
	$params	= $fh->SerializeParams();
	$update_sizes = (($post['thumb_size_old'] != $post['param_thumb_size']) || ($post['image_size_old'] != $post['param_image_size']))? true : false;
	$update_order = ($post['sort_order_old'] != $post['sort_order'])? true : false;
	if($action=="store")
	{
		if ($id_gallery>0)
			$g->GalleryUpdate($title,$description,$released,$id_user,$show_orig,$visible,$sort_order,$show_date,$show_author,$note,$topic_info['id_topic'],$keywords,$id_group,$params,$update_sizes,$associated_image,$author,$images_per_page,$id_licence,$share_images,$update_order,$topic_info['id_group'],$captions_html,$topic_admin);
		if ($id_gallery==0)
			$id_gallery = $g->GalleryInsert($title,$description,$released,$id_user,$show_orig,$visible,$sort_order,$show_date,$show_author,$note,$topic_info['id_topic'],$keywords,$id_group,$params,$associated_image,$author,$images_per_page,$id_licence,$share_images,$topic_info['id_group'],$captions_html,$topic_admin);
		$url = "ops.php?id=$id_gallery";
	}
	if($action=="delete")
	{
		$g->GalleryDelete();
		$url = "index.php";
	}
	header("Location: $url");
}

if ($from=="group")
{
	include_once(SERVER_ROOT."/../classes/galleries.php");
	$name 		= $post['name'];
	$description	= $post['description'];
	$id_group	= $post['id_group'];
	$id_parent	= $fh->Null2Zero($post['id_parent']);
	$id_parent_old	= $fh->Null2Zero($post['id_parent_old']);
	$visible 		= $fh->Checkbox2bool($post['visible']);
	$galleries = new Galleries();
	if ($action2=="update")
		$galleries->gh->GroupUpdate($id_group,$name,$description,$id_parent,$id_parent_old,$visible);
	if ($action2=="insert")
		$galleries->gh->GroupInsert($name,$description,$id_parent,$visible);
	if ($action2=="delete")
		$galleries->gh->GroupDelete($id_group);
	header("Location: tree.php");
}

if ($from2=="group")
{
	include_once(SERVER_ROOT."/../classes/galleries.php");
	$galleries = new Galleries;
	$id_group	= $_GET['id'];
	if ($action3=="up")
		$galleries->gh->GroupMove($id_group,1);
	if ($action3=="down")
		$galleries->gh->GroupMove($id_group,0);
	header("Location: tree.php");
}

if ($from2=="homepage")
{
	include_once(SERVER_ROOT."/../classes/galleries.php");
	$galleries = new Galleries;
	$id_gallery	= $_GET['id'];
	if ($action3=="add")
		$galleries->HomepageAdd($id_gallery);
	if ($action3=="delete")
		$galleries->HomepageDelete($id_gallery);
	header("Location: index.php");
}

if ($from2=="import")
{
	$id_image	= $_GET['id'];
	$id_gallery	= $_GET['id_gallery'];
	$id_gallery_from	= $_GET['id_gallery_from'];
	include_once(SERVER_ROOT."/../classes/image.php");
	$im = new Image($id_image);
	$im->GalleryInsert($id_gallery,"",$id_gallery_from);
	header("Location: images.php?id=$id_gallery");
}

if ($from=="image")
{
	$action 	= $fh->ActionGet($post);
	$id_image	= $post['id_image'];
	$id_gallery	= $post['id_gallery'];
	$id_gallery_from	= $post['id_gallery_from'];
	$image_date	= $fh->CheckDateString($post['image_date']);
	$author	= $post['author'];
	$caption	= $post['caption'];
	$source	= $post['source'];
	$link		= $fh->String2Url($post['link']);
	$id_licence	= $post['id_licence'];
	$keywords	= $post['keywords'];
	$p		= $post['p'];
	$file		= $fh->UploadedFile("img",true);
	$caption_import	= $post['caption_import'];
	include_once(SERVER_ROOT."/../classes/image.php");
	$im = new Image($id_image);
	if($action=="store")
	{
		if($id_image>0)
		{
			if($id_gallery_from>0)
			{
				$im->GalleryImport($id_gallery,$link,$caption_import,$id_gallery_from);
			}
			else 
			{
				if ($im->AdminRight())
					$im->ImageUpdate($id_image,$image_date,$source,$author,$id_licence,$caption,$keywords,$file);
				$im->GalleryUpdate($id_gallery,$link);
			}
		}
		elseif($file['ok'])
		{
			$im->ImageInsert($image_date,$source,$author,$id_licence,$caption,$keywords,$file);
			if ($im->id>0 && $id_gallery>0)
				$im->GalleryInsert($id_gallery,$link);
		}
	}
	if ($action=="delete")
		$im->GalleryDelete($id_image,$id_gallery);
	if ($action=="remove")
		$im->GalleryRemove($id_image,$id_gallery);
	header("Location: images.php?id=$id_gallery&p=$p");
}

if ($from=="config_update")
{
	$path 		= $post['path'];
	$title	 	= $post['title'];
	$description 		= $post['description'];
	$gal_hometype 	= $post['gal_hometype'];
	$gal_homelimit 	= $fh->String2Number($post['gal_homelimit'],6);
	include_once(SERVER_ROOT."/../classes/galleries.php");
	$galleries = new Galleries;
	$galleries->ConfigurationUpdate($path, $title, $description,$gal_hometype,$gal_homelimit);
	header("Location: index.php");
}
?>

