<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");
include_once(SERVER_ROOT."/../classes/images.php");

$id_gallery = $_GET['id_gallery'];
$id_gallery_from = $_GET['id_gallery_from'];

$g = new Gallery($id_gallery);
$g_from = new Gallery($id_gallery_from);

if (($module_admin || $g->AmIUser($ah->current_user_id)) && $g_from->row['share_images'])
	$input_right = 1;

$title[] = array($g->title,'ops.php?id='.$id_gallery);
$title[] = array('images','images.php?id='.$id_gallery);
$title[] = array('image_import','image_import.php?id_gallery='.$id_gallery);
$title[] = array($g_from->title,'');

echo $hh->ShowTitle($title);

echo $hh->tr->Translate("image_choose");

if($input_right)
{
	$i = new Images();
	$num = $i->Gallery( $row, $id_gallery_from, $id_gallery );
	$table_headers = array('image','caption');
	$table_content = array('{LinkImage("image.php?id=$row[id_image]&id_gallery='.$id_gallery.'&id_gallery_from='.$id_gallery_from.'","$row[id_image]",0,"$row[format]")}','$row[caption]');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
