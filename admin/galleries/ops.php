<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");

$id = $_GET['id'];

$g = new Gallery($id);

if ($g->AmIUser($ah->current_user_id) || $module_admin)
	$input_right = 1;

$title[] = array($g->title,'');
echo $hh->ShowTitle($title);

$image = $g->Image();
if($image['id_image']>0)
{
	include_once(SERVER_ROOT."/../classes/images.php");
	$i = new Images();
	$filename = "images/{$g->thumb_size}/{$image['id_image']}.{$i->convert_format}";
	echo "<img src=\"/images/upload.php?src=$filename&format={$image['format']}\" width=\"{$i->img_sizes[$g->thumb_size]}\" alt=\"" . htmlspecialchars($image['caption']) . "\" border=\"0\">\n";	
}

echo "<p>" . $hh->tr->Translate("images") . ": " . $g->CountImages() . "</p>\n";
echo "<p><a href=\"gallery.php?id=$id\">" . $hh->tr->Translate("settings_main") . "</a></p>\n";
echo "<p><a href=\"images.php?id=$id\">" . $hh->tr->Translate("browse_images") . "</a></p>\n";

if ($input_right)
{
	echo "<h3>" . $hh->tr->Translate("image_add") . "</h3>\n";
	echo "<ul>\n";
	echo "<li><a href=\"image.php?id_gallery=$id&id=0\">" . $hh->tr->Translate("image_upload") . "</a></li>\n";
	echo "<li><a href=\"image_import.php?id_gallery=$id\">" . $hh->tr->Translate("image_import") . "</a></li>\n";
	echo "</ul>\n";
	
	if ($g->visible)
		echo "<p><a href=\"publish.php?id=$id\">" . $hh->tr->Translate("publish_all") . "</a></p>\n";
}

if ($g->visible || $g->id_topic>0 || $g->id_topic_group>0)
{
	echo "<h3>" . $hh->tr->Translate("published_info") ."</h3>";
	echo "<ul>";
	if ($g->visible==1)
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$url = $irl->PublicUrlGlobal("galleries",array('id_gallery'=>$id,'subtype'=>"gallery"));
		echo "<li>" . $hh->tr->Translate("gallery") ." - URL <a href=\"$url\" target=\"_blank\">$url</a></li>";
	}
	if ($g->id_topic>0 || $g->id_topic_group>0)
	{
		$topics = $g->Topics();	
		foreach($topics as $topic)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($topic['id_topic']);
			if ($topic['id_subtopic']>0)
			{
				include_once(SERVER_ROOT."/../classes/irl.php");
				$irl = new IRL();
				$url = $irl->PublicUrlTopic("subtopic",array('id'=>$topic['id_subtopic']),$t);
				echo "<li><a href=\"/topics/subtopic.php?id_topic={$topic['id_topic']}&id={$topic['id_subtopic']}\">$t->name</a> - URL <a href=\"$url\" target=\"_blank\">$url</a></li>";
			}
		}
	}
	echo "</ul>";
}
else
	echo "<p>" . $hh->tr->Translate("visible_no") . "</p>\n";

if ($id>0)
{
	echo $hh->input_code("gallery_html","gallery_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php?id=$id\"></script>",60,2);
	echo $hh->input_code("gallery_group_html_random","gallery_group_html_random","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php?id=$id&r=1\"></script>",60,2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
