<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");
include_once(SERVER_ROOT."/../classes/images.php");

$id = $_GET['id'];
$g = new Gallery($id);

$hh->RecordsPerPage($g->images_per_page);

$title[] = array($g->title,'ops.php?id='.$id);
$title[] = array('images','');
echo $hh->ShowTitle($title);

$i = new Images();
$num = $i->Gallery( $row, $id,0,$g->sort_order );
$table_headers = array('image','caption');
$table_content = array('{LinkImage("image.php?id=$row[id_image]&id_gallery='.$id.'&id_gallery_from=$row[id_gallery_from]&p='.$current_page.'","$row[id_image]",0,"$row[format]")}','{Htmlize($row[caption],1)}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin || $g->AmIUser($ah->current_user_id))
{
	echo "<h3>" . $hh->tr->Translate("image_add") . "</h3>\n";
	echo "<ul>\n";
	echo "<li><a href=\"image.php?id_gallery=$id&id=0\">" . $hh->tr->Translate("image_upload") . "</a></li>\n";
	echo "<li><a href=\"image_import.php?id_gallery=$id\">" . $hh->tr->Translate("image_import") . "</a></li>\n";
	echo "</ul>\n";
}


include_once(SERVER_ROOT."/include/footer.php");
?>

