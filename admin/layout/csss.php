<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$title[] = array('CSS','');

$id_style = $_GET['id_style'];

echo $hh->ShowTitle($title);

$types = $hh->tr->Translate("page_types");
$types_global = $hh->tr->Translate("page_types_global");

$active_modules = Modules::AvailableModules();
$t_modules = $hh->tr->Translate("modules_names");

echo "<ul>\n<li>Global";
echo "<ul>\n";
foreach($pt->gtypes as $key=>$type)
{
	if($key!="rss")
		echo "<li><a href=\"css_global.php?id=$type\">" . $types_global[$type] . "</a> - $key.css</li>\n";
}
echo "</ul></li>\n";
echo "<li>Specific (redefinable by styles)<ul>\n";
foreach($pt->types as $key=>$type)
	if ($key!="root" && $key!="tools")
	echo "<li><a href=\"css_styles.php?id=$type\">" . $types[$type] . "</a> - $key.css</li>\n";
echo "</ul></li>\n";
echo "<li>Modules (redefinable by styles)<ul>\n";
foreach($active_modules as $module)
{
	if($module['layout'] && !$module['internal'] && !$module['global'])
		echo "<li><a href=\"css_module_styles.php?id={$module['id_module']}\">" . $t_modules[$module['id_module']] . "</a> - " . $module['path'] . ".css</li>\n";
}
echo "</ul></li>\n";
echo "<li><a href=\"css_exts.php\">Extensions</a> (redefinable by styles)</li>\n";
echo "<li><a href=\"css_customs.php\">Custom</a></li>\n";
echo "</ul>\n";


include_once(SERVER_ROOT."/include/footer.php");
?>
