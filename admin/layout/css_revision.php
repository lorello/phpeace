<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/css.php");

$csm = new CssManager();
$revision = $_GET['id'];

$rev = $csm->RevisionGet($revision);
$csm = new CssManager($rev['type']);

$id_pagetype = (int)$rev['id_pagetype'];
$id_style = (int)$rev['id_style'];
$id_css = (int)$rev['id_css'];
$csm->id_pagetype = $id_pagetype;

$title[] = array('CSS','csss.php');

switch($rev['type'])
{
	case "specific":
		$types = $hh->tr->Translate("page_types");
		$title[] = array($types[$id_pagetype],'css_styles.php?id=' . $id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css.php?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','css.php?id=' . $id_pagetype . '&id_style=0');
	break;
	case "global":
		$types = $hh->tr->Translate("page_types_global");
		$title[] = array($types[$id_pagetype],'css_global.php?id=' . $id_pagetype);
	break;
	case "module":
		$t_modules = $hh->tr->Translate("modules_names");
		$title[] = array($t_modules[$id_pagetype],'css_module_styles.php?id='.$id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css_module?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','css_module?id=' . $id_pagetype . '&id_style=0');
	break;
	case "custom":
		$title[] = array('Custom CSS','css_customs.php');
		$id_pagetype = $id_css;
		$row2 = $csm->CssGet($id_pagetype,0);
		$title[] = array($row2['name'],'css_custom.php?id=' . $id_pagetype);
	break;
	case "ext":
		$id_pagetype = $id_css;
		$title[] = array('CSS extensions','css_exts.php');
		$row2 = $csm->CssGet($id_pagetype,$id_style);
		$title[] = array($row2['name'],'css_ext_styles.php?id='.$id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css_ext.php?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','');
	break;
}


$title[] = array('Revisions','css_revisions.php?id_style=' . $id_style . '&type=' . $csm->type . '&id_pagetype=' . $id_pagetype);
$title[] = array('#'.$revision,'');

echo $hh->ShowTitle($title);

$hhf = new HHFunctions();
echo $hh->input_form_open();
echo $hh->input_table_open();
echo $hh->input_text("date","date",$hh->FormatDateTime($rev['change_time_ts']),20,0,0);
echo $hh->input_text("user","user",$hhf->UserLookup($rev['id_user']),40,0,0);
if($rev['tag']!="")
	echo $hh->input_text("tag","tag",$rev['tag'],40,0,0);
echo $hh->input_textarea("css","css_cp",$rev['content'],80,30,"",1,"css");
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer_cp.php");
?>

