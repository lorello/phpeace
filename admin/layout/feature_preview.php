<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/topic.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);

$id_feature = (int)$_GET['id'];

if ($ah->ModuleAdmin(10))
	$input_right = 1;

if($input_right && $id_feature>0)
{
	$l = new Layout(false,false,false);
	$l->TopicInit(0,true);
	$l->pt->ft->topic = $l->topic;

	$row = $l->pt->ft->FeatureGet($id_feature);
	$functions = $l->tr->Translate("page_functions");
	
	$v = new Varia();
	$custom_params = isset($row['params'])? $v->Deserialize($row['params']) : array();
	$irows = $l->pt->ft->PageFunction($row['id_function'],$custom_params,array());
	$items = array();
	$seq = 1;
	
	$l2 = clone $l;
	$l2->TopicInit(0,true);
	$l2->pt->ft->TopicInit(0);
	foreach($irows as $irow)
	{
		$irow['item_type'] = $l2->pt->ft->item_type;
		$items['i_' . $seq] = $l2->Item($irow);
		$seq++;
	}
	$feature = array('xname'=>"feature",'id'=>$row['id_feature'],'name'=>$row['name'],'id_user'=>$row['id_user'],'id_function'=>$row['id_function'],'function'=>$functions[$row['id_function']],'params_in'=>array(),'params'=>$custom_params,'info'=>($l2->pt->ft->info),'type'=>($l2->pt->ft->item_type),'items'=>$items,'description'=>$row['description']);

	header('Content-type: text/xml');
	print $l->xh->Array2Xml($feature,false);
}
?>
