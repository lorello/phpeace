<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$id_style = $_GET['id_style'];

$title[] = array('CSS','csss.php');
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
$row = $s->StyleGet($id_style);
$title[] = array($row['name'],'style.php?id='.$id_style);

echo $hh->ShowTitle($title);

$types = $hh->tr->Translate("page_types");
$types_global = $hh->tr->Translate("page_types_global");

$active_modules = Modules::AvailableModules();
$t_modules = $hh->tr->Translate("modules_names");

echo "<p>CSS of style <a href=\"style.php?id=$id_style\">" . $row['name'] . "</a></p>\n";

echo "<ul>\n<li>Specific";
echo "<ul>\n";
foreach($pt->types as $type=>$id_type)
	echo "<li><a href=\"css.php?id=$id_type&id_style=$id_style\">" . $types[$id_type] . "</a></li>\n";
echo "</ul></li>\n";
echo "<li>Modules<ul>\n";
foreach($active_modules as $module)
{
	if($module['layout'] && !$module['internal'] && !$module['global'])
		echo "<li><a href=\"css_module.php?id={$module['id_module']}&id_style=$id_style\">" . $t_modules[$module['id_module']] . "</a></li>\n";
}
echo "</ul></li>\n";
echo "<li>Extensions<ul>\n";
include_once(SERVER_ROOT."/../classes/css.php");
$csm = new CssManager("ext");
$exts = $csm->CssAll();
foreach($exts as $ext)
	echo "<li><a href=\"css_ext.php?id={$ext['id_css']}&id_style=$id_style\">" . $ext['name'] . "</a></li>\n";		
echo "</ul></li>\n";
echo "<li>Custom<ul>\n";
$csm = new CssManager("custom");
$custs = $csm->CssAll($id_style);
foreach($custs as $cust)
	echo "<li><a href=\"css_custom.php?id={$cust['id_css']}&id_style=$id_style\">" . $cust['name'] . "</a></li>\n";		
echo "</ul></li>\n";
echo "</ul>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

