<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

echo "<p><a href=\"styles.php\">Styles</a></p>\n";

echo "<p><a href=\"label_styles.php\">Labels</a></p>\n";

echo "<p><a href=\"boxes_types.php\">Articles Boxes types</a></p>\n";

if(!$conf->Get("ui")) {
	echo "<p><a href=\"graphics.php\">Graphics</a></p>\n";
	echo "<p><a href=\"favicons.php\">Favicons</a></p>\n";
	echo "<p><a href=\"csss.php\">CSS</a></p>\n";
	echo "<p><a href=\"jss.php\">JavaScript</a></p>\n";
	echo "<p><a href=\"xsls.php\">Page types (XSL)</a></p>\n";
	echo "<p><a href=\"graphics.php\">Graphics</a></p>\n";
} else {
	echo "<p><a href=\"page_types.php\">Page types</a></p>\n";
}

echo "<p><a href=\"xml_choose.php\">XML preview</a></p>\n";

if ($ah->session->Get("module_admin")==1)
{
	echo "<p><a href=\"features_all.php\">Features</a></p>\n";
	echo "<p><a href=\"config.php\">Configuration</a></p>\n";
	if(!$conf->Get("ui")) {
		echo "<p><a href=\"config_xsl.php\">XSL Configuration</a></p>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
