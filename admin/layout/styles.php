<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Styles','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
$num = $s->StylesP( $row );

$table_headers = array('style','description');
$table_content = array('{LinkTitle("style.php?id=$row[id_style]",$row[name])}','$row[description]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin)
	echo "<p><a href=\"style.php?id=0\">Create new style</a></p>\n";

echo $hh->input_form("get","/topics/preview.php");
echo $hh->input_hidden("id_type",1);
echo $hh->input_table_open();
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
echo $hh->input_row("graphic_style","id_style",0,$s->StylesAll(),"choose_option",0,$module_admin);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics(0,0,$tt->AllTopics(),"choose_option",$module_admin);
$actions[] = array('action'=>"preview",'label'=>"preview",'right'=>$module_admin);
echo $hh->input_actions($actions,$module_admin);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

