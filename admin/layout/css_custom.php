<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/css.php");
include_once(SERVER_ROOT."/../classes/styles.php");

$s = new Styles();

$id_css = $_GET['id'];
$title[] = array('CSS','csss.php');
$title[] = array('Custom CSS','css_customs.php');

$csm = new CssManager("custom");

if ($id_css>0)
{
	$row = $csm->CssGet($id_css,0);
	$id_style = $row['id_style'];
	$row['css'] = $csm->CssGetLocal($id_css,$id_style);
	$title[] = array($row['name'],'');
}
else
{
	$title[] = array("new",'');
	$id_style = 0;
}

if ($id_css>0 && $id_style>0)
{
	$input_right = $s->InputRight($id_style);
}
if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php");

echo $hh->input_hidden("from","css_custom");
echo $hh->input_hidden("id_css",$id_css);
echo $hh->input_table_open();

if ($id_css>0)
{
	echo $hh->input_code("css_include","Add this CSS to your cssCustom template in <a href=\"/layout/xsl.php?id=20&id_style=$id_style\">root.xsl</a>","<link type=\"text/css\" rel=\"stylesheet\" href=\"{\$css_url}/$id_style/custom_{$id_css}.css{/root/publish/@css_version}\" media=\"screen\"/>");
$num = $csm->Revisions($rows,$id_css,$id_style);
if ($num>0)
	echo $hh->input_note("Revisions: <a href=\"css_revisions.php?id_style=$id_style&type=custom&id_pagetype=$id_css\">$num</a>");
}

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_row("graphic_style","id_style",$id_style,$s->StylesAll(),"all",0,$input_right);
echo $hh->input_textarea("css","css",$row['css'],80,30,"",$input_right,"css");

$actions = array();
$actions[] = array('action'=>(($id_css>0)?"store":"insert"),'label'=>"submit",'right'=>$input_right);
if ($id_css>0)
{
	$actions[] = array('action'=>"store_close",'label'=>"submit_close",'right'=>$input_right);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id_css>0));
}
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer_cp.php");
?>
