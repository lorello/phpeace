<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/javascript.php");
include_once(SERVER_ROOT."/../classes/styles.php");

$s = new Styles();
$jsm = new JavascriptManager();

$id = $_GET['id'];
$id_style = $_GET['id_style'];

$title[] = array('Javascripts','jss.php');

if ($id>0)
{
	$row = $jsm->JsGet($id);
	$title[] = array($row['name'],'jss.php?id='.$id);
	$id_style = $row['id_style'];
}
else
{
	$title[] = array("new",'');
}

if ($id>0 && $row['id_style']>0)
{
	$input_right = $s->InputRight($row['id_style']);
}
if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

?>
<form method="post" action="actions.php" name="form1">
<input type="hidden" name="from" value="js">
<input type="hidden" name="id_js" value="<?=$id;?>">
<input type="hidden" name="id_style" value="<?=$id_style;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
if ($id>0)
{
	echo $hh->input_code("js_include","To include this script","<script type=\"text/javascript\" src=\"" . str_replace("pub",$hh->ini->Get("pub_web"),$jsm->JsFilename($id_style,$id)) . "\"></script>");
	echo $hh->input_code("js_include_xsl","In XSL","<script type=\"text/javascript\" src=\"" . str_replace("pub","{/root/site/@base}",$jsm->JsFilename($id_style,$id)) . "{/root/publish/@js_version}\"></script>");
}
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_row("graphic_style","id_style",$row['id_style'],$s->StylesAll(),"all",0,$input_right);
echo $hh->input_textarea("script","script",$row['script'],80,30,"",$input_right);

$actions = array();
$actions[] = array('action'=>(($id>0)?"store":"insert"),'label'=>"submit",'right'=>$input_right);
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id>0));
echo $hh->input_actions($actions,$input_right);
?>
</table>
</form>
<?php

include_once(SERVER_ROOT."/include/footer.php");
?>
