<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_module = $_GET['id'];

$title[] = array('Page types',$ui?'page_types.php':'xsls.php');
$t_modules = $hh->tr->Translate("modules_names");
$title[] = array($t_modules[$id_module],'');
echo $hh->ShowTitle($title);


if(!$ui) {
	echo "<p><a href=\"xsl_module.php?id=$id_module&id_style=0\">Generic XSL</a></p>";
}
if ($id_module>0) {
	echo "<p><a href=\"xml_choose.php?id_module=$id_module\">Sample XML</a></p>";
}

include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
$num = $s->StylesP( $row );

$label = $ui? 'page type':'xsl';
$table_headers = array($label,'style');
$table_content = array('{LinkTitle("xsl_module.php?id=' . $id_module . '&id_style=$row[id_style]","'.$t_modules[$id_module].'")}',
'{LinkTitle("style.php?id=$row[id_style]",$row[name])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

