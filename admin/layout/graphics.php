<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");


$hh = new HHFunctions();
$title[] = array('Graphics','');
echo $hh->ShowTitle($title);

$id_style = $_GET['id_style'];

include_once(SERVER_ROOT."/../classes/images.php");
$i = new Images();
$num = $i->Graphics( $row, $id_style );

$table_headers = array('thumb','&nbsp;','size','description','style');
$table_content = array('{ThumbImage($row[id_graphic],"graphics",$row[format])}',
'{LinkTitle("graphic.php?id=$row[id_graphic]'.($id_style>0?"&from_style=$id_style":"") . '","open")}','<div class=\"right\">$row[width] x $row[height]</div>','$row[name]','$row[style_name]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_right)
	echo "<p><a href=\"graphic.php?id=0&id_style=$id_style\">Insert new graphic</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

