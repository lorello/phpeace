<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();

$hhf = new HHFunctions ;

$id_type = ($_GET['id_type']>0)? $_GET['id_type']:0;

$types = $hh->tr->Translate("page_types_global");

if($ui) {
	$title[] = array('Page types','page_types.php');
	$title[] = array($types[$id_type],'page_type_global.php?id=' . $id_type);
} else {
	$title[] = array('Page types','xsls.php');
	$title[] = array($types[$id_type],'xsl_global.php?id=' . $id_type);
}

$title[] = array('Features','');
echo $hh->ShowTitle($title);

$num = $pt->ft->GlobalPageFeatures($row,$id_type);

$table_headers = array('feature','description','active','author','public_xml');
$table_content = array('{LinkTitle("feature_global.php?id=$row[id_feature]&id_type='.$id_type.'",$row[name])}',
'$row[description]','{Bool2YN($row[active])}','{UserLookup($row[id_user])}','{Bool2YN($row[public])}');
echo $hh->showTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"feature_global.php?id=0&id_type=$id_type\">add</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

