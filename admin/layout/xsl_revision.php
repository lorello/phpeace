<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles;

$xsm = new XslManager();
$revision = $_GET['id'];

$rev = $xsm->RevisionGet($revision);
$xsm = new XslManager($rev['type']);

$id_pagetype = (int)$rev['id_pagetype'];
$id_style = (int)$rev['id_style'];
$id_xsl = (int)$rev['id_xsl'];
$xsm->id_pagetype = $id_pagetype;

if ($id_style>0)
{
	$style = $s->StyleGet($id_style);
	$style_name = $style['name'];
}
else 
	$style_name = "Generic XSL";

$title[] = array('Page types','xsls.php');

switch($rev['type'])
{
	case "specific":
		$types = $hh->tr->Translate("page_types");
		$title[] = array($types[$id_pagetype],'xsl_styles.php?id=' . $id_pagetype);
		$title[] = array($style_name,'xsl.php?id=' . $id_pagetype . '&id_style=' . $id_style);
	break;
	case "global":
		$types = $hh->tr->Translate("page_types_global");
		$title[] = array($types[$id_pagetype],'xsl_global.php?id=' . $id_pagetype);
	break;
	case "module":
		$t_modules = $hh->tr->Translate("modules_names");
		$title[] = array($t_modules[$id_pagetype],'xsl_module_styles.php?id='.$id_pagetype);
		$title[] = array($style_name,'xsl_module?id=' . $id_pagetype . '&id_style=' . $id_style);
	break;
	case "custom":
		$title[] = array('Custom XSL','xsl_customs.php');
		$id_pagetype = $id_xsl;
		$row2 = $xsm->XslGet($id_pagetype,0);
		$title[] = array($row2['name'],'xsl_custom.php?id=' . $id_pagetype);
	break;
	case "ext":
		$id_pagetype = $id_xsl;
		$title[] = array('XSL extensions','xsl_exts.php');
		$row2 = $xsm->XslGet($id_pagetype,$id_style);
		$title[] = array($row2['name'],'xsl_ext_styles.php?id='.$id_pagetype);
		$title[] = array($style_name,'xsl_ext.php?id=' . $id_pagetype . '&id_style=' . $id_style);
	break;
}

$title[] = array('Revisions','xsl_revisions.php?id_style=' . $id_style . '&type=' . $xsm->type . '&id_pagetype=' . $id_pagetype);
$title[] = array('#'.$revision,'');

echo $hh->ShowTitle($title);

$hhf = new HHFunctions();
echo $hh->input_form_open();
echo $hh->input_table_open();
if($rev['valid']=="0")
	echo $hh->input_note("WARNING! This XSL is not well-formed, it has been saved in the revision history but the live file has not been updated, please fix it ASAP",true);
echo $hh->input_text("date","date",$hh->FormatDateTime($rev['change_time_ts']),20,0,0);
echo $hh->input_text("user","user",$hhf->UserLookup($rev['id_user']),40,0,0);
if($rev['tag']!="")
	echo $hh->input_text("tag","tag",$rev['tag'],40,0,0);
echo $hh->input_textarea("XSL","xsl",$rev['content'],80,30,"",1);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

