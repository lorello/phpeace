<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Favicons','favicons.php');

$id_topic = (int)$get['id'];

if($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title1 = $t->name;
	if ($t->edit_layout && $t->AmIAdmin())
		$input_right = 1;
}
else
{
	$title1 = $ini->Get("title");
}
	

$title[] = array($title1,'');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>

<p>Must be in ICO format, width 32x32 or 16x16 px</p>

<?php
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("from","favicon");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();

$filename = "graphics/favicon/{$id_topic}.ico";
include_once(SERVER_ROOT."/../classes/file.php");
$fm = new FileManager();
if ($fm->Exists("uploads/$filename"))
	echo "<tr><td>Current favicon</td><td><img src=\"/images/upload.php?src=$filename\" /></td>\n";
echo $hh->input_upload("substitute_with","img",50,$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
