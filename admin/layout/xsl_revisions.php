<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles;

$id_pagetype = (int)$_GET['id_pagetype'];
$id_style = (int)$_GET['id_style'];
$id_xsl = (int)$_GET['id_xsl'];
$type = $_GET['type'];

$xsm = new XslManager($type);

if ($id_style>0)
{
	$style = $s->StyleGet($id_style);
	$style_name = $style['name'];
}
else 
	$style_name = "Generic XSL";
	
$title[] = array('Page types','xsls.php');
$xsm->id_pagetype = $id_pagetype;

switch($type)
{
	case "specific":
		$types = $hh->tr->Translate("page_types");
		$title[] = array($types[$id_pagetype],'xsl_styles.php?id=' . $id_pagetype);
		$title[] = array($style_name,'xsl.php?id=' . $id_pagetype . '&id_style=' . $id_style);
	break;
	case "global":
		$types = $hh->tr->Translate("page_types_global");
		$title[] = array($types[$id_pagetype],'xsl_global.php?id=' . $id_pagetype);
	break;
	case "module":
		$t_modules = $hh->tr->Translate("modules_names");
		$title[] = array($t_modules[$id_pagetype],'xsl_module_styles.php?id='.$id_pagetype);
		$title[] = array($style_name,'xsl_module?id=' . $id_pagetype . '&id_style=' . $id_style);
	break;
	case "custom":
		$title[] = array('Custom XSL','xsl_customs.php');
		$row2 = $xsm->XslGet($id_pagetype,0);
		$title[] = array($row2['name'],'xsl_custom.php?id=' . $id_pagetype);
		$id_xsl = $id_pagetype;
	break;
	case "ext":
		$title[] = array('XSL extensions','xsl_exts.php');
		$row2 = $xsm->XslGet($id_pagetype,$id_style);
		$title[] = array($row2['name'],'xsl_ext_styles.php?id='.$id_pagetype);
		$title[] = array($style_name,'xsl_ext.php?id=' . $id_pagetype . '&id_style=' . $id_style);
		$id_xsl = $id_pagetype;
	break;
}

$title[] = array('Revisions','');
echo $hh->ShowTitle($title);

$row = array();
$num = $xsm->Revisions($row,$id_xsl,$id_style);

$table_headers = array('revision','date','user','tag','valid','');
$table_content = array('<div class=\"right\">#$row[revision]</div>','{FormatDateTime($row[change_time_ts])}','{UserLookup($row[id_user])}',
'$row[tag]','{Bool2YN($row[valid])}','{LinkTitle("xsl_revision.php?id=$row[revision]",' . $hh->tr->Translate("details") . ')}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

