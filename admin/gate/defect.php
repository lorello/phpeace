<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/users.php");

$id = $_GET['id'];
$id_module = $_GET['id_module'];

$title[] = array('bug_new','');

$input_right = $conf->Get("submit_defects");
echo $hh->ShowTitle($title);

?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			description: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","bug_notify");
echo $hh->input_table_open();
echo $hh->input_note("bug_note");
$uu = new Users();
echo $hh->input_row("from","id_user",$ah->current_user_id,$uu->Active(),"",0,0);
$names = $hh->tr->Translate("modules_names");
echo $hh->input_array("module","id_module",$id_module,$names,$input_right);
echo $hh->input_textarea("description","description","",80,7,"",$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

