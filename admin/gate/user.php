<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;
$id = $u->id;
$row = $u->UserGetFull();

$active = $row['active'];
if ($active!=1)
	$ah->MessageSet("user_no_active_warn");
$title[] = array($row['name'],'');

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
else
{
	$tabs = array();
	$tabs[] = array('user_data','');
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/gate/user_page.php');
	$tabs[] = array('image_associated','/gate/user_image.php');
	$tabs[] = array('responsibilities','/gate/user_roles.php');
	$tabs[] = array('services','/gate/user_services.php');
	$tabs[] = array('user_stats','/gate/user_stats.php');
	echo $hh->Tabs($tabs);
	
	$input_right = 1;
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});
</script>

<?php
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","user");
	echo $hh->input_table_open();
	$fm = new FileManager;
	$i = new Images();
	$filename = "users/0/$id".".".$i->convert_format;
	$filename_orig = "users/orig/{$id}.jpg";
	if($fm->Exists("uploads/$filename"))
	{
		echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"/gate/user_image.php\"><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[0]}\"></a></td></tr>";
	}
	
	echo $hh->input_text("name","name",$row['name'],30,0,$input_right && $conf->Get("user_auth")=="internal");
	echo $hh->input_text("email","email",$row['email'],30,0,0,( $conf->Get("user_auth")=="internal" && ($id==$ah->current_user_id))? " (<a href=\"user_email.php\">" . $hh->tr->Translate("email_change") . "</a>)" : "");
	echo $hh->input_text("password","pass","******",20,0,0,($id==$ah->current_user_id)? " (<a href=\"user_password.php\">" . $hh->tr->Translate("password_change") . "</a>)" : "");
	echo $hh->input_text("staff id","staff_id",$row['staff_id'],30,0,$input_right && $conf->Get("user_auth")=="internal");
	echo $hh->input_text("department","department",$row['department'],30,0,$input_right && $conf->Get("user_auth")=="internal");
	echo $hh->input_text("job position","job_position",$row['job_position'],30,0,$input_right && $conf->Get("user_auth")=="internal");
	echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);
	echo $hh->input_text("mobile","mobile",$row['mobile'],20,0,$input_right);
	echo $hh->input_array("show_email","email_visible",($row['email_visible'] + 1),$hh->tr->Translate("show_email_options"),$input_right);
	echo $hh->input_checkbox("photo_visible","photo_visible",$row['photo_visible'],0,$input_right);
	echo $hh->input_geo($row['id_geo'],$input_right);
	echo $hh->input_array("language","id_language",$row['id_language'],$ah->Languages(),$input_right);
	echo $hh->input_textarea("signature","signature",$row['signature'],50,4,"",$input_right);
	$pub_username = "----";
	if($row['id_p']>0)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$pub_user = $pe->UserGetById($row['id_p']);
		$pub_username = "{$pub_user['name1']} {$pub_user['name2']}";
	}
	echo $hh->input_text("pub_user","pub_user",$pub_username,20,0,0,($id==$ah->current_user_id)? " (<a href=\"user_pub.php?id={$row['id_p']}\">" . $hh->tr->Translate("change") . "</a>)" : "");
	echo $hh->input_submit("submit","",$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
include_once(SERVER_ROOT."/include/footer.php");
?>


