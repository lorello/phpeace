<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = (int)$_GET['id'];

$u = new User;
$id = $u->id;
$row = $u->UserGet();
$active = $row['active'];
if ($active!=1)
{
	$ah->MessageSet("user_no_active_warn");
}

$t = new Topic($id_topic);

$title[] = array($row['name'],'user.php');
$title[] = array('responsibilities','user_roles.php');
$title[] = array($t->name,'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/gate/user.php');
if($hh->ini->Get("user_show"))
	$tabs[] = array('user_link','/gate/user_page.php');
$tabs[] = array('image_associated','/gate/user_image.php');
$tabs[] = array('responsibilities','/gate/user_roles.php');
$tabs[] = array('services','/gate/user_services.php');
$tabs[] = array('user_stats','/gate/user_stats.php');
echo $hh->Tabs($tabs);

$input_right = 1;

echo $hh->input_form_open();
echo $hh->input_hidden("from","user_topic");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
echo $hh->input_note("<a href=\"/topics/ops.php?id=$id_topic\">" . $t->name . "</a>: " . $hh->tr->Translate("settings"));
echo $hh->input_checkbox("pending_notify","pending_notify",$t->UserNotify($ah->current_user_id),0,$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();



include_once(SERVER_ROOT."/include/footer.php");
?>


