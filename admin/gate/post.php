<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper;
$post = $fh->HttpPost();

$title[] = array('session_see','');
echo $hh->ShowTitle($title);
?>
<form action="<?=$post['post_uri']?>" method="post">
<?
echo $hh->input_table_open();

$post_vars = unserialize(base64_decode($post['post_dump']));
echo $hh->input_text("URL","",$post['post_uri'],80,0,0);
$magic_quotes = false;

if(is_array($post_vars))
{
	foreach($post_vars as $key=>$value)
	{
		if ($magic_quotes)
			$value = stripslashes($value);
		if(!is_array($value))
		{
			if (strlen($value)<100)
				echo $hh->input_text($key,$key,$value,80,0,1);
			else
				echo $hh->input_textarea($key,$key,$value,60,10,"",1);
		}
	}
}
echo $hh->input_submit("submit","",1);
echo $hh->input_table_close();
?>
</form>
<?
include(SERVER_ROOT."/include/footer.php");
?>
