<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$ah = new AdminHelper();
$fh = new FormHelper(true,0);
$post = $fh->HttpPost();
$get = $fh->HttpGet();
$sanitized_post = $fh->HttpPost(false,true,true);

if(count($post)==0)
	header("Location: /gate/index.php");

$uri = $post['uri'];
$action = $post['action'];
$from2	= $get['from2'];

if ( $action == "login" )
{
	$user = $sanitized_post['user'];
	$pass = $sanitized_post['pass'];
	$token = $post['token'];
	$redirect = "index.php";
	$fh->va->NotEmpty($user,"username");
	$fh->va->NotEmpty($pass,"password");
	if ($fh->va->return && ($ah->session->Get("token"))!="")
	{
		if ($token==($ah->session->Get("token"))  )
		{
			$token_age = time() - $ah->session->Get("token_time");
			if ($token_age < 300 )
			{
				$ah->Enter( $user, $pass );
				$redirect = str_replace( "logout.php", "", $uri );
				$ah->session->Delete("token");
				$ah->session->Delete("token_time");
			}
			else
				$ah->MessageSet("token_expired");
		}
		else
			$ah->MessageSet("auth_error");
	}
	else
		$ah->MessageSet("session_error");
	header("Location: $redirect");
}

if ( $action == "reminder" )
{
	$unescaped_email = $fh->HttpPostUnescapedVar("email",true);
	$fh->va->Email($unescaped_email,"email");
	if ($fh->va->return)
		$ah->PasswordReminder( $unescaped_email );
	header("Location: index.php");
}

$from = $post['from'];
if ($from=="bug_notify")
{
	$ah->CheckAuth();
	$id_user 	= $post['id_user'];
	$id_module 	= $post['id_module'];
	$description 	= $fh->HttpPostUnescapedVar("description");
	include_once(SERVER_ROOT."/../classes/phpeace.php");
	$phpeace = new PhPeace;
	$phpeace->BugNotify($id_user,$id_module,$description);
	header("Location: index.php");
}

if ($from=="user")
{
	$ah->CheckAuth();
	$name	 	= $post['name'];
	$staff_id	 	= $post['staff_id'];
	$department	 	= $post['department'];
	$job_position	 	= $post['job_position'];
	$email		= $post['email'];
	$email_visible 	= (int)$post['email_visible'] - 1;
	$photo_visible 	= $fh->Checkbox2Bool($post['photo_visible']);
	$phone	= $post['phone'];
	$mobile	= $post['mobile'];
	$id_geo	= $post['id_geo'];
	$id_language	= $post['id_language'];
	$signature	= $post['signature'];
	$fh->va->Email($email,"email");
	$fh->va->NotEmpty($name,"name");
	if($fh->va->return)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$u->UserUpdate( $name,$staff_id,$email,$email_visible,$department,$job_position,$phone,$mobile,$signature,$id_geo,$photo_visible );
		$u->UserUpdateLanguage($id_language);
	}
	header("Location: user.php");
}

if ($from=="user_page")
{
	$ah->CheckAuth();
	$user_show	= $fh->Checkbox2Bool($post['user_show']);
	$notes		= $post['notes'];
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User();
	$u->UserUpdatePage( $user_show,$notes );
	header("Location: user.php");
}

if ($from=="user_email")
{
	$ah->CheckAuth();
	$password	= $post['password'];
	$email		= $post['email'];
	$fh->va->Email($email,"email");
	$fh->va->NotEmpty($password,"password");
	if($fh->va->return)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$return = $u->ChangeEmail( $password, $email );
		$ah->MessageSet( $return? "email_changed" : "email_change_fail");
	}
	header("Location: user.php");
}

if ($from=="user_topic")
{
	$ah->CheckAuth();
	$id_topic	= (int)$post['id_topic'];
	$pending_notify	= $fh->Checkbox2Bool($post['pending_notify']);
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User();
	$u->TopicNotify( $id_topic,$pending_notify );
	header("Location: /topics/ops.php?id=$id_topic");
}

if ($from=="user_password")
{
	$ah->CheckAuth();
	$old_password	= $post['password_old'];
	$new_password	= $post['password'];
	$new_password_verify	= $post['password_verify'];
	$fh->va->NotEmpty($new_password,"password");
	$fh->va->PasswordsEqual($new_password,$new_password_verify);
	if($fh->va->return)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$u->ChangePassword( $old_password, $new_password, false );
	}
	header("Location: user.php");
}

if ($from=="user_pub")
{
	$id_p		= $post['id_p'];
	$password	= $post['password'];
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User();
	if($action=="remove")
		$u->PubUserRemove();
	if($action=="submit")
	{
		$fh->va->NotEmpty($password,"password");
		if($fh->va->return)
		{
			$associated = $u->PubUserAssociate( $id_p, $password );
			if($associated)
				$ah->PubUserSet($id_p);
		}
	}
	header("Location: user.php");
}

if ($from=="user_service")
{
	$ah->CheckAuth();
	$id_user_service	= $fh->Null2Zero($post['id_user_service']);
	$action = $fh->ActionGet($post);
	if($id_user_service>0)
	{
		$mobile = $fh->Checkbox2Bool($post['mobile']);
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		if($action=="store")
			$u->ServiceUpdate($id_user_service,$mobile);
		if($action=="delete")
			$u->ServiceDelete($id_user_service);
	}
	header("Location: user_services.php");
}

if ($from=="user_image")
{
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User();
	$id_user 	= $post['id_user'];
	$file		= $fh->UploadedFile("img",true);
	$u->id = $id_user;
	$action = $fh->ActionGet($post);
	if ($action=="store" && $file['ok'])
		$u->ImageUpdate($file);
	if ($action=="delete")
		$u->ImageDelete();
	header("Location: user.php");
}

if ($from2=="mailjob_test")
{
	$ah->CheckAuth();
	$unescaped_post = $fh->HttpPost(false,false,false);
	include_once(SERVER_ROOT."/../classes/mailjobs.php");
	$mj = new Mailjobs();
	$auto_footer	= $fh->String2Number($post['auto_footer'],1);
	$send_password	= $fh->Checkbox2Bool($post['send_password']);	
	$is_html		= $fh->Checkbox2Bool($post['is_html']);
	$id_article		= $fh->String2Number($post['id_article']);
	$embed_images	= $fh->Checkbox2Bool($post['embed_images']);
	$id_topic		= (int)$get['id_topic'];
	$mj->Test($id_topic,$is_html,$id_article,$unescaped_post['name'],$unescaped_post['email'],$unescaped_post['subject'],$unescaped_post['body'],$unescaped_post['alt_text'],$auto_footer,$send_password,$embed_images);
}
?>
