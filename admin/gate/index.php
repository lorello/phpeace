<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../modules/books.php");
include_once(SERVER_ROOT."/../classes/quotes.php");

echo $hh->ShowTitle($title);

$quotes = new Quotes();

$u = new User;
$row2 = $u->UserGet();
echo "<p>" . $hh->tr->TranslateParams("welcome_gate_in",array($row2['name'],$ini->Get('title'))) . "</p>\n";

$connections = $u->Connections();
if($connections>0)
{
	echo "<p><div class=\"notes\">" . $hh->tr->TranslateParams("conn_info1",array($connections));
	if ($row2['last_conn_ts']>0)
		echo "<br>" . $hh->tr->TranslateParams("conn_info2",array($hh->FormatDateTime($row2['last_conn_ts']))) . "</div>";
	echo "</p>\n";
}

if(Modules::AmIUser(2))
{
	// new messages
	$newmessages = $u->NewMessages();
	if ( $newmessages > 0 )
		echo "<p><a href=\"/mail/inbox.php\">" . $hh->tr->TranslateParams("messages_new",array($newmessages)) . "</a></p>\n";
}

// phpeace registration
if (!$phpeace->main && !$phpeace->registered && !$dev_right)
{
	if ($phpeace->AmIAdmin())
	{
		if ($phpeace->IsRegistered())
			$ini->Set("registered","1");
		else
		{
			$trm17 = new Translator($hh->tr->id_language,17);
			echo "<p>" . $trm17->TranslateParams("register",array($phpeace->RegisterLink($hh->tr->id_language))) . "</p>";
		}
	}
}

// post dump
$post_dump = $ah->session->Get("post_dump");
$post_uri = $ah->session->Get("post_uri");
if ($post_dump!="" && $post_uri!="")
{
	$post_vars = unserialize($post_dump);
	if (is_array($post_vars))
	{
		echo "<p>" . $hh->tr->Translate("session_post");
		?>
		<form action="post.php" method="post" target="_blank">
		<input type="hidden" name="from" value="postdump">
		<input type="hidden" name="post_dump" value="<?=base64_encode($post_dump)?>">
		<input type="hidden" name="post_uri" value="<?=$post_uri?>">
		<input type="submit" class="input-submit" value="<?=$hh->tr->Translate("session_see");?>">
		</form>
		</p>
		<?php
	}
	$ah->session->Delete("post_uri");
	$ah->session->Delete("post_dump");
}

// pending articles, links, threads
include_once(SERVER_ROOT."/../classes/topic.php");
$pa = "<h3>" . $hh->tr->Translate("to_approve_articles") . ":</h3>\n";
$pc = "<h3>" . $hh->tr->Translate("comments_to_approve") . ":</h3>\n";
$pl = "<h3>" . $hh->tr->Translate("to_approve_links") . ":</h3>\n";
$pe = "<h3>" . $hh->tr->Translate("events_to_approve") . ":</h3>\n";
$pt = "<h3>" . $hh->tr->Translate("forum") . ": " . $hh->tr->Translate("to_approve_threads") . ":</h3>\n";
$pcf = "<h3>" . $hh->tr->Translate("forum") . ": " . $hh->tr->Translate("comments_to_approve") . ":</h3>\n";
$pcc = "<h3>" . $hh->tr->Translate("polls") . ": " . $hh->tr->Translate("comments_to_approve") . ":</h3>\n";
$numPa = 0;
$numPc = 0;
$numPl = 0;
$numPe = 0;
$numPt = 0;
$numPcf = 0;
$numPcc = 0;
if (count($userTopics)>0)
{
	foreach($userTopics as $userTopic)
	{
		if($userTopic['id_topic'] != $ini->Get('temp_id_topic'))
		{
			$topic = new Topic($userTopic['id_topic']);
			if ($topic->AmIAdmin())
			{
				$art_appr = array();
				$numArticles = $topic->ArticlesApproved( $art_appr, 0 );
				$numComments = $topic->CommentsApproved(0);
				$links = $topic->PendingLinks();
				$numEvents = count($topic->Events(0));
				$threads = $topic->PendingThreads();
				$numThreadsComments = $topic->PendingThreadsComments();
				$numPollsComments = $topic->PendingPollsComments();
				$numLinks = count($links);
				$numThreads = count($threads);
				$numPa += $numArticles;
				$numPc += $numComments;
				$numPl += $numLinks;
				$numPe += $numEvents;
				$numPt += $numThreads;
				$numPcf += $numThreadsComments;
				$numPcc += $numPollsComments;
				if ($numArticles>0)
					$pa .= "<li>$numArticles " . $hh->tr->Translate("in") . " <a href=\"/topics/articles.php?id=$topic->id&appr=0\">$topic->name</a></li>\n";
				if ($numComments>0)
					$pc .= "<li>$numComments " . $hh->tr->Translate("in") . " <a href=\"/topics/comments_topic.php?id=$topic->id&appr=0\">$topic->name</a></li>\n";
				if ($numLinks>0)
					$pl .= "<li>$numLinks " . $hh->tr->Translate("in") . " <a href=\"/topics/links.php?id=$topic->id&appr=0\">$topic->name</a></li>\n";
				if ($numEvents>0 && $topic->HasSubtopicType($topic->subtopic_types['calendar'])>0)
					$pe .= "<li>$numEvents " . $hh->tr->Translate("in") . " <a href=\"/topics/events.php?id_topic=$topic->id&approved=0\">$topic->name</a></li>\n";
				if ($numThreads>0)
					foreach($threads as $thread)
						$pt .= "<li>$topic->name: <a href=\"/topics/forum_threads.php?id={$thread['id_topic_forum']}&id_topic=$topic->id&approved=0\">{$thread['name']}</a> ({$thread['counter']})</li>\n";
				if ($numThreadsComments>0)
					$pcf .= "<li>$numThreadsComments " . $hh->tr->Translate("in") . " <a href=\"/topics/comments_forums.php?id=$topic->id\">$topic->name</a></li>\n";
				if ($numPollsComments>0)
					$pcc .= "<li>$numPollsComments " . $hh->tr->Translate("in") . " <a href=\"/topics/comments_polls.php?id=$topic->id\">$topic->name</a></li>\n";
			}
		}
	}
	if ($numPa>0)
		echo "<ul>$pa</ul>\n";
	if ($numPc>0)
		echo "<ul>$pc</ul>\n";
	if ($numPl>0)
		echo "<ul>$pl</ul>\n";
	if ($numPe>0)
		echo "<ul>$pe</ul>\n";
	if ($numPt>0)
		echo "<ul>$pt</ul>\n";
	if ($numPcf>0)
		echo "<ul>$pcf</ul>\n";
	if ($numPcc>0)
		echo "<ul>$pcc</ul>\n";
}

if (Modules::IsActive(21))
{
	include_once(SERVER_ROOT."/../classes/translations.php");
	$tra = new Translations();
	$rtra = array();
	$num_tra_atwork = $tra->TranslationsUserP( $rtra, "atwork", $ah->current_user_id );
	$num_tra_pending = $tra->TranslationsUserP( $rtra, "pending", $ah->current_user_id );
	if(($num_tra_atwork + $num_tra_pending)>0)
	{
		echo "<h3>" . $hh->tr->Translate("translations") . "</h3>\n";
		echo "<ul>\n";
		if($num_tra_atwork>0)
			echo "<li><a href=\"/translations/translations.php?id_user{$ah->current_user_id}&s=atwork\">Traduzioni da completare</a> ($num_tra_atwork)</li>";
		if($num_tra_pending>0)
			echo "<li><a href=\"/translations/translations.php?id_user{$ah->current_user_id}&s=pending\">Traduzioni che potresti fare</a> ($num_tra_pending)</li>";
		echo "</ul>\n";
	}
}

// pending events
if (Modules::IsActive(12) && $u->ModuleAdmin(12))
{
	$row = array();
	include_once(SERVER_ROOT."/../classes/events.php");
	$ee = new Events();
	$pending_events = $ee->EventsApproved( $row, 0 );
	if ($pending_events>0)
		echo "<p><a href=\"/events/events.php?approved=0\">$pending_events " . $hh->tr->Translate("events_to_approve") . "</a></p>\n";
}

// pending quotes
if (Modules::IsActive(6) && $u->ModuleAdmin(6))
{
	$row = array();
	$pendingQuotes = $quotes->QuotesApproved( $row, 0, 0);
	if ($pendingQuotes>0)
		echo "<p><a href=\"/quotes/quotes.php?approved=0\">$pendingQuotes " . $hh->tr->Translate("quotes_to_approve") . "</a></p>\n";
}

// pending orgs
if (Modules::IsActive(8) && $u->ModuleAdmin(8))
{
	include_once(SERVER_ROOT."/../modules/assos.php");
	$as = new Assos();
	$inserts = $as->PendingInserts();
	if (count($inserts)>0)
	{
		echo "<h3>Risorse da approvare:</h3><ul>";
			foreach($inserts as $insert)
				echo "<li><a href=\"/orgs/org.php?id={$insert['id_ass']}\">{$insert['nome']}</a></li>\n";
		echo "</ul>\n";
	}
}

// pending reviews
if (Modules::IsActive(16) && in_array(16,$adminModules))
{
	$row = array();
	include_once(SERVER_ROOT."/../modules/books.php");
	$bb = new Books();
	$pendingReviews = $bb->Reviews( $row, 0 );
	$trm16 = new Translator($hh->tr->id_language,16);
	if ($pendingReviews>0)
		echo "<p><a href=\"/books/reviews.php?approved=0\">$pendingReviews " . $trm16->Translate("reviews_to_approve") . "</a></p>\n";
}

// Widgets Dashboard
if (Modules::IsActive(3) && $u->ModuleAdmin(3) && Modules::IsActive(32))
{
	include_once(SERVER_ROOT."/../classes/widgets.php");
	$wi = new Widgets();
    echo "<h3>Widgets Information</h3>";
    echo "<ul>";

    $widgets = WidgetsDashBoard::TotalWidgetsByTypeGet();
    echo "<li>Type<ul>";
    foreach($widgets as $key=>$value)
    {
        echo "<li>Number of {$wi->types[$key]} widgets: <strong>";
        echo $hh->Wrap($value,"<a href=\"/homepage/widgets.php?widget_type=$key\">","</a>",$value>0);
        echo "</strong></li>";
    }
    echo "</ul></li>";

    $widgets = WidgetsDashBoard::TotalWidgetsByStatusGet();
    $statuses = $wi->Statuses(true);
    echo "<li>Status<ul>";
    foreach($widgets as $key=>$value)
    {
        echo "<li>Number of {$statuses[$key]} widgets: <strong>";
        echo $hh->Wrap($value,"<a href=\"/homepage/widgets.php?status=$key\">","</a>",$value>0);
        echo "</strong></li>";
    }
    echo "</ul></li>";

    $widgets = WidgetsDashBoard::TotalWidgetByWebFeedStatusGet();
    echo "<li>Data source status<ul>";
    foreach ($widgets as $key=>$value)
    {
        switch ($key)
        {
            case "success":
                $title = "Number of widgets with success data source status";
                break;
            case "failed":
                $title = "Number of widgets with failed data source status";
                break;
        }
        echo "<li>" . $title . ": <strong>" . $value . "</strong></li>";
    }    
    echo "</ul></li>";

    $widgets = WidgetsDashBoard::TotalSharedWidgetsGet();
    echo "<li>Sharing<ul>";
    foreach ($widgets as $key=>$value)
    {
        switch ($key)
        {
            case "share":
                $title = "Number of Shared widgets";
                break;
            case "notshare":
                $title = "Number of Non-shareable widgets";
                break;
        }
        echo "<li>" . $title . ": <strong>" . $value . "</strong></li>";
    }
    echo "</ul></li>";
	echo "</ul>";
}

if($conf->Get("welcome_quote"))
{
	$qrnd = $quotes->RandomQuote(0,$hh->tr->id_language);
	echo "<div>" . nl2br($qrnd['quote']) . "</div><div style=\"font-style: italic;\">" . nl2br($qrnd['author']) . "</div>\n";
}

include(SERVER_ROOT."/include/footer.php");
?>
