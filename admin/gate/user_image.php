<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;
$id = $u->id;
$row = $u->UserGet();

$active = $row['active'];
if ($active!=1)
	$ah->MessageSet("user_no_active_warn");

$title[] = array($row['name'],'user.php?id='.$id);
$title[] = array("image_associated",'');
echo $hh->ShowTitle($title);

if ($ah->current_user_id==$id || $module_admin)
	$input_right = 1;

$trm8 = new Translator($hh->tr->id_language,8);
$fm = new FileManager;
$maxfilesize = $fm->MaxFileSize();

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
else
{
	$tabs = array();
	$tabs[] = array('user_data','/gate/user.php');
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/gate/user_page.php');
	$tabs[] = array('image_associated','');
	$tabs[] = array('responsibilities','/gate/user_roles.php');
	$tabs[] = array('services','/gate/user_services.php');
	$tabs[] = array('user_stats','/gate/user_stats.php');
	echo $hh->Tabs($tabs);
	
	$input_right = 1;
}

echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_user",$id);
echo $hh->input_hidden("from","user_image");
echo $hh->input_table_open();
echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
echo $hh->input_note($trm8->Translate("jpg_only"));

$i = new Images();
$filename = "users/0/$id".".".$i->convert_format;
$filename_orig = "users/orig/{$id}.jpg";
if($fm->Exists("uploads/$filename_orig"))
{
	echo "<tr><td align=\"right\">&nbsp;</td><td><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[0]}\">";
	if ($fm->Exists("uploads/$filename_orig"))
		echo "&nbsp;<a href=\"/images/upload.php?src=$filename_orig\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a> (" . $fm->Size("uploads/$filename_orig") . ")";
	echo $hh->input_upload("substitute_with","img",50,$input_right);
}
else
	echo $hh->input_upload("choose_file","img",50,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $fm->Exists("uploads/$filename"));
echo $hh->input_actions($actions,$input_right && $id>0);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
