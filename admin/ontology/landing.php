<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology();

$trm13 = new Translator($hh->tr->id_language,13);

$id = $_GET['id'];

$title[] = array('Landing','landings.php');
if ($id>0)
{
	$row = $o->KeywordLandingGet($id);
	$title[] = array($row['keyword'],'');
}
else
{
	$title[] = array('add_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","landing");
echo $hh->input_hidden("id_keyword",$id);
echo $hh->input_table_open();

echo $hh->input_text($trm13->Translate("keyword"),"keyword",$row['keyword'],"50",0,$input_right && !$id>0);
echo $hh->input_wysiwyg("content","content",$row['content'],true,20,$input_right,true);
echo $hh->input_text("youtube playlist","youtube",$row['youtube'],50,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($id>0 && $input_right));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($module_admin && $id>0)
	echo "<p><a href=\"keyword.php?id=$id\">Keyword definition</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

