<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];

if ($from=="keyword")
{
	$action 	= $fh->ActionGet($post);
	$id_keyword	= $post['id_keyword'];
	$id_type	= $post['id_type'];
	$keyword	= $post['keyword'];
	$description	= $post['description'];
	$qid_type	= $post['qid_type'];
	$id_language	= $post['id_language'];
	$p		= $post['p'];
	$file		= $fh->UploadedFile("img",true);
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	if ($action=="insert")
	{
		$k->KeywordInsert($keyword,$description,$id_type,$file);
	}
	if ($action=="update")
	{
		$check = $k->KeywordGetId($keyword);
		if($check>0 && $check!=$id_keyword)
			$ah->MessageSet("keyword_duplicate",array($keyword));
		else 
			$k->KeywordUpdate($id_keyword,$keyword,$description,$id_type,$file,$id_language,$qid_type);
	}
	if ($action=="delete")
	{
		$k->KeywordDelete($id_keyword);
	}
	header("Location: keywords.php?id_type=$qid_type&p=$p");
}

if ($from=="feed")
{
	$action = $fh->ActionGet($post);
	$id_keyword	= $post['id_keyword'];
	$keyword	= $post['keyword'];
	$description	= $post['description'];
	$public 		= $fh->Checkbox2Bool($post['public']);
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	if ($action=="update")
	{
		if($id_keyword>0)
		{
				$o->KeywordFeedUpdate($id_keyword,$description,$public);
		}
		else 
		{
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$id_feed = $k->KeywordGetId($keyword);
			if(!$id_feed>0)
				$ah->MessageSetTranslated("keyword &quot;$keyword&quot; not found");
			else 
			{
				$o->KeywordFeedInsert($id_feed,$description,$public);
			}
		}
	}
	if ($action=="delete")
		$o->KeywordFeedDelete($id_keyword);
	header("Location: feeds.php");
}

if ($from=="landing")
{
	$action = $fh->ActionGet($post);
	$id_keyword	= $post['id_keyword'];
	$keyword	= $post['keyword'];
	$content	= $post['content'];
	$youtube	= $post['youtube'];
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	if ($action=="update")
	{
		if($id_keyword>0)
		{
			$o->KeywordLandingUpdate($id_keyword,$content,$youtube);
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/keyword.php");
			$k = new Keyword();
			$id_k = $k->KeywordGetId($keyword);
			if(!$id_k>0)
				$ah->MessageSetTranslated("keyword &quot;$keyword&quot; not found");
				else
				{
					$o->KeywordLandingInsert($id_k,$content,$youtube);
				}
		}
	}
	if ($action=="delete")
		$o->KeywordLandingDelete($id_keyword);
	header("Location: landings.php");
}

if ($from=="relation")
{
	$action 	= $fh->ActionGet($post);
	$id_relation	= $post['id_relation'];
	$name			= $post['name'];
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	if ($action=="insert")
		$o->RelationInsert( $name );
	if ($action=="update")
		$o->RelationUpdate( $id_relation, $name );
	header("Location: relations.php");
}

if ($from=="statement") 
{
	$action = $fh->ActionGet($post);
	$id_statement	= $post['id_statement'];
	$id_domain		= $post['id_domain'];
	$id_relation	= $post['id_relation'];
	$id_range		= $post['id_range'];
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	if ($action=="insert")
		$o->StatementInsert( $id_domain,$id_relation,$id_range );
	if ($action=="update")
		$o->StatementUpdate( $id_statement,$id_domain,$id_relation,$id_range );
	if ($action=="delete")
		$o->StatementDelete( $id_statement );
	if ($id_keyword>0) {
		header("Location: keyword.php?id=$id_keyword");
	} else {
		header("Location: statements.php?id_relation=$id_relation");
	}
}

if ($from=="statement_type") 
{
	$domain			= $post['domain'];
	$range			= $post['range'];
	$id_relation	= $post['id_relation'];
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	if($action2=="insert" && $id_relation>0)
	{
		$id_domain = $k->KeywordGetId($domain);
		$id_range = $k->KeywordGetId($range);
		if($id_domain>0 && $id_range>0)
		{
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$o->StatementInsert( $id_domain,$id_relation,$id_range );
		}
		else 
		{
			if(!$id_domain>0)
				$ah->MessageSetTranslated("keyword &quot;$domain&quot; not found");
			if(!$id_range>0)
				$ah->MessageSetTranslated("keyword &quot;$range&quot; not found");
		}
	}
	header("Location: statements.php?id_relation=$id_relation");
}

if ($from=="merge")
{
	$action = $fh->ActionGet($post);
	$id_keyword_from	= $post['id_keyword_from'];
	$id_keyword_to	= $post['id_keyword_to'];
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	if ($action=="update")
		$o->Merge($id_keyword_from,$id_keyword_to);
	header("Location: index.php");
}

if ($from=="notused_remove")
{
	if($module_admin)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->NotUsedRemove();
	}
	header("Location: index.php");
}
?>

