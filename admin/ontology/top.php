<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;

$trm13 = new Translator($hh->tr->id_language,13);

$id_type = $_GET['id_type'];

$ktypes = $hh->tr->Translate("keyword_types");

$title[] = array($ktypes[$id_type],'tree.php?id_type=' . $id_type);
$title[] = array($trm13->Translate("primary_keywords"),'');

echo $hh->ShowTitle($title);

$num = $o->Top( $rows, $id_type, 3 );

$table_headers = array('keyword');
$table_content = array('{LinkTitle("keyword.php?id=$row[id_keyword]",$row[keyword])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

echo "<p><a href=\"keyword.php?id=0&id_type=$id_type\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

