<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$id_relation = $_GET['id_relation'];

$title[] = array('Statements','');
echo $hh->ShowTitle($title);

$o = new Ontology;
$num = $o->Statements( $rows, $id_relation );

$table_headers = array('domain','relation','range');
$table_content = array('{LinkTitle("keyword.php?id=$row[id_keyword_domain]",$row[keyword_domain])}','{LinkTitle("statement.php?id=$row[id_statement]",$row[name])}',
'{LinkTitle("keyword.php?id=$row[id_keyword_range]",$row[keyword_range])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if($module_admin)
	echo "<p>Add new statement: <a href=\"statement.php?id=0&id_relation=$id_relation\">By selection</a> /
	 <a href=\"statement_type.php?id=0&id_relation=$id_relation\">Typing</a> </p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

