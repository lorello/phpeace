<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$trm13 = new Translator($hh->tr->id_language,13);

$id = $_GET['id'];
$id_relation = $_GET['id_relation'];
$id_range = $_GET['id_range'];

$title[] = array($trm13->Translate("statements"),'statements.php');
if ($id>0)
{
	$action2 = "update";
	$o = new Ontology;
	$row = $o->StatementGet($id);
	$title[] = array('change','');
	$id_relation = $row['id_relation'];
}
else
{
	$action2 = "insert";
	$title[] = array('add_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			id_domain: {
				required: true,
				min: 1
			}
		}
	});
});
</script>

<?php
echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","statement");
echo $hh->input_hidden("id_statement",$id);
echo $hh->input_table_open();

$o = new Ontology;
$keywords = $o->Keywords();

echo $hh->input_row("domain","id_domain",$row['id_domain'],$keywords,"choose_option",0,$input_right);
echo $hh->input_row("relation","id_relation",$id_relation,$o->RelationsAll(),"choose_option",0,$input_right);

if ($id_range>0)
	echo $hh->input_row("range<input type=hidden name=\"id_range\" value=\"$id_range\">","id_range",$id_range,$keywords,"",0,0);
else
	echo $hh->input_row("range","id_range",$row['id_range'],$keywords,"choose_option",0,$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($id>0 && $input_right));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
