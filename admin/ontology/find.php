<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$title[] = array('results','');
echo $hh->ShowTitle($title);

$approved = $fh->Checkbox2bool($_GET['approved']);
$id_type = $_GET['id_type'];
$q = $_GET['q'];

$params = array( 'q' => $q, 'id_type' => $id_type, 'approved' => $approved );

$o = new Ontology();
$num = $o->Search( $row, $params );

$table_headers = array('keyword','description','type');
$table_content = array('{LinkTitle("keyword.php?id=$row[id_keyword]&p='.$current_page.'&approved='.$approved.'&id_type='.$id_type.'",$row[keyword])}','$row[description]','{KeywordType($row[id_type])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

