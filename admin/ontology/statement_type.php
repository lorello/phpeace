<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Statements','statements.php');
$title[] = array('add_new','');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","statement_type");
echo $hh->input_hidden("action2","insert");
echo $hh->input_table_open();
	
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = $o->Keywords();

echo $hh->input_text("domain","domain","","30",0,$input_right);
echo $hh->input_row("relation","id_relation",0,$o->RelationsAll(),"choose_option",0,$input_right);
echo $hh->input_text("range","range","","30",0,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
