<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $_GET['id'];

$row = $me->MeetingGet($id_meeting);
$id_user = $row['id_user'];
$status = $row['status'];

if ($module_admin || $me->AmIAdmin($id_meeting,$ah->current_user_id))
	$input_right = 1;

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($row['title'],'meeting.php?id='.$id_meeting);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'');
$tabs[] = array($trm23->Translate("participants"),'meeting_participants.php?id='.$id_meeting);
echo $hh->Tabs($tabs);

$num = $me->MeetingSlots($rows,$id_meeting);

$table_headers = array('date','hour','title','num');
$table_content = array('{FormatDate($row[start_date_ts])}','{FormatTime($row[start_date_ts])}',
'{LinkTitle("meeting_slot.php?id=$row[id_meeting_slot]&id_meeting='.$id_meeting.'",$row[title])}','{LinkTitle("meeting_participants.php?id='.$id_meeting.'&id_slot=$row[id_meeting_slot]",$row[num_participants])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_right)
	echo "<p><a href=\"meeting_slot.php?id=0&id_meeting=$id_meeting\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
