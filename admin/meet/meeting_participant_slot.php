<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/people.php");
$pe = new People();

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting_slot = $_GET['id_meeting_slot'];
$id_p = $_GET['id_p'];

$slot = $me->MeetingSlotGet($id_meeting_slot);
$id_meeting = $slot['id_meeting'];
$meeting = $me->MeetingGet($slot['id_meeting']);
$row = $me->ParticipantSlotGet($id_meeting_slot,$id_p);

if ($module_admin || ($id_meeting>0 && $me->AmIAdmin($id_meeting,$ah->current_user_id)))
	$input_right = 1;

$user = $pe->UserGetById($id_p);

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($meeting['title'],'meeting.php?id='.$id_meeting);
$title[] = array("{$user['name1']} {$user['name2']} ",'meeting_participant.php?id_p='.$id_p.'&id_meeting='.$id_meeting);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),"meeting_participants.php?id=$id_meeting&status={$row['status']}");
echo $hh->Tabs($tabs);

$statuses = $trm23->Translate("participation");

echo "<h3><a href=\"meeting_participant.php?id_meeting=$id_meeting&id_p=$id_p\">{$user['name1']} {$user['name2']}</a></h3>" ;

echo "<p>" . $trm23->Translate("slot") . ": " . $hh->FormatDateTime($slot['start_date_ts']) . ": <b>{$slot['title']}</b></p>";

echo $hh->input_form_open();
echo $hh->input_hidden("from","meeting_participant_slot");
echo $hh->input_hidden("id_meeting",$id_meeting);
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_hidden("id_meeting_slot",$id_meeting_slot);
echo $hh->input_table_open();

echo $hh->input_array("status","status",$row['status'],$statuses,$input_right);		
echo $hh->input_textarea("comments","comments",$row['comments'],50,5,"",$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
