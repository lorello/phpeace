<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

$trm23 = new Translator($hh->tr->id_language,23);

echo "<h3>" . $trm23->Translate("meetings") . "</h3>\n";
echo "<ul>\n";
echo "<li><a href=\"meetings.php\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"meeting.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
