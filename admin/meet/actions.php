<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/meetings.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper(true,23);
$post = $fh->HttpPost();

$action 	= $fh->ActionGet($post);
$from		= $post['from'];

$me = new Meetings();

if ($from=="meeting")
{
	$id_meeting		= (int)$post['id_meeting'];
	$title			= $post['title'];
	$description	= $post['description'];
	$status		= (int)$post['status'];
	$id_topic		= (int)$post['id_topic'];
	$is_private		= $fh->Checkbox2bool($post['is_private']);
	$id_user	= $post['id_user'];
	
	if($action=="store")
	{
		$id_meeting = $me->MeetingStore($id_meeting,$title,$description,$status,$id_topic,$is_private,$id_user);
		$url = "meeting.php?id=$id_meeting";
	}
	if ($action=="delete")
	{
		$me->MeetingDelete($id_meeting);
		$url = "meetings.php";
	}
	header("Location: $url");
}

if ($from=="meeting_slot")
{
	$id_meeting		= (int)$post['id_meeting'];
	$id_meeting_slot	= (int)$post['id_meeting_slot'];
	$title			= $post['title'];
	$description		= $post['description'];
	$start_date 	= $fh->Strings2Datetime($post['start_date_d'],$post['start_date_m'],$post['start_date_y'],$post['start_date_h'],$post['start_date_i']);
	$length		= $post['length'];
	
	if($action=="store")
	{
		$me->MeetingSlotStore($id_meeting_slot,$id_meeting,$start_date,$length,$title,$description);
	}
	if ($action=="delete")
	{
		$me->MeetingSlotDelete($id_meeting_slot);
	}
	header("Location: meeting_slots.php?id=$id_meeting");
}

if ($from=="meeting_participant_slot")
{
	$id_meeting		= (int)$post['id_meeting'];
	$id_p			= (int)$post['id_p'];
	$id_meeting_slot	= (int)$post['id_meeting_slot'];
	$status		= $post['status'];
	$comments		= $post['comments'];
	if($action=="store")
	{
		$me->ParticipantUpdate($id_meeting_slot,$id_p,$status,$comments);
	}
	header("Location: meeting_participant.php?id_p=$id_p&id_meeting=$id_meeting&status=$status");
}

if ($from=="meeting_import")
{
	$id_meeting		= (int)$post['id_meeting'];
	$people		= $fh->HttpPostUnescapedVar("people");
	$id_pt_group		= (int)$post['id_pt_group'];
	
	if($action=="import")
	{
		$me->MeetingImport($id_meeting,$people,$id_pt_group);
	}
	header("Location: meeting_participants.php?id=$id_meeting");
}


?>
