<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/people.php");
$pe = new People();

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $_GET['id_meeting'];
$id_p = $_GET['id_p'];
$status = (int)$_GET['status'];

$meeting = $me->MeetingGet($id_meeting);

if ($module_admin || ($id_meeting>0 && $me->AmIAdmin($id_meeting,$ah->current_user_id)))
	$input_right = 1;

$row = $pe->UserGetById($id_p);

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($meeting['title'],'meeting.php?id='.$id_meeting);
$title[] = array("{$row['name1']} {$row['name2']} ",'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),"meeting_participants.php?id=$id_meeting&status=$status");
echo $hh->Tabs($tabs);

echo "<h3><a href=\"/topics/visitor.php?id={$meeting['id_topic']}&id_p=$id_p\">{$row['name1']} {$row['name2']}</a></h3>" ;

$statuses = $trm23->Translate("participation");

$slots = $me->ParticipantSlots($id_meeting,$id_p);
if(count($slots)>0)
{
	echo "<ul>";
	foreach($slots as $slot)
	{
		echo "<li>";
		echo $hh->FormatDateTime($slot['start_date_ts']) . ": <b>{$slot['title']}</b>";
		echo "<ul>";
		echo "<li>Status: <a href=\"meeting_participant_slot.php?id_meeting_slot={$slot['id_meeting_slot']}&id_p=$id_p\">{$statuses[$slot['status']]}</a></li>";
		if($slot['comments']!="")
		{
			echo "<li>" . $hh->tr->Translate("comments") . ": <em>{$slot['comments']}</em></li>";
		}
		echo "</ul>";
		echo "</li>";
	}
	echo "</ul>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
