<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

echo "<!DOCTYPE html>\n";
define('TINYMCE_VERSION',"4.9.2");
$css_version = $hh->ini->GetModule("layout","css_version","");
$js_version = $hh->ini->GetModule("layout","js_version","");
?>
<html>
<head>
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Content-Type" content="<?=$ah->content_header;?>" >
<title><?=$page_title;?></title>
<link rel="icon" href="/favicon.ico" type="image/x-icon" >
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" >
<link rel="stylesheet" href="/include/css/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="/include/css/default.css?v=<?=PHPEACE_BUILD;?>" media="screen">
<link type="text/css" rel="stylesheet" href="/include/css/custom.css?v=<?=$css_version;?>" media="screen">
<script type="text/javascript" src="/js/lib.js"></script>
<?php
if($hh->tr->lang=="it")
{
	echo "<script type=\"text/javascript\" src=\"/js/lib_it.js\"></script>\n";
}
?>
<script type="text/javascript" src="/js/main.js?v=<?=PHPEACE_BUILD;?>"></script>
<?php
if(isset($tinymce) && $tinymce && ($hh->tr->id_language)>0)
{
	echo "<script type=\"text/javascript\" src=\"/js/tinymce-". TINYMCE_VERSION . "/tinymce.min.js\"></script>\n";
	echo "<script type=\"text/javascript\" src=\"/js/tinymce-". TINYMCE_VERSION . "/tiny_config.php?lang={$hh->tr->lang}&v2=" . PHPEACE_BUILD . ($tinymce_article>0? "&id=$tinymce_article" : "") . "\"></script>\n";
}
?>
<script type="text/javascript" src="/js/custom.js?v=<?=$js_version;?>"></script>
</head>
