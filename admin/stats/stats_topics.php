<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");

$id_group = (int)$_GET['id_g'];

if($id_group>0)
{
	$title[] = array("topics",'stats_topics.php');
	include_once(SERVER_ROOT."/../classes/topics.php");
	$tt = new Topics();
	$group = $tt->gh->GroupGet($id_group);
	$title[] = array($group['name'],'');
}
else 
	$title[] = array("topics",'');
	
echo $hh->ShowTitle($title);

if ($module_right)
{
	$tk = new Tracker();
	
	$num = $tk->StatsTopics( $row, $id_group );
	
	$table_headers = array( $id_group>0? 'topic' : 'group','visits','pages');
	if($id_group>0)
	{
		$table_content = array('{LinkTitle("stats_topic.php?id_topic=$row[id]",$row[name])}',
		'<div class=\"right\">$row[visits]</div>','<div class=\"right\">$row[pages]</div>');	
	}
	else 
	{
		$table_content = array('{LinkTitle("stats_topics.php?id_g=$row[id]",$row[name])}',
		'<div class=\"right\">$row[visits]</div>','<div class=\"right\">$row[pages]</div>');	
	}
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
