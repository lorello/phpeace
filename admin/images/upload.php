<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");

$ah = new AdminHelper;
$ah->CheckAuth(false);

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper();
$get = $fh->HttpGet();

include_once(SERVER_ROOT."/../classes/file.php");
$fm = new FileManager();

$file = explode('/',$get['src']);
if($ah->cdn!='' && count($file)==3) {
	include_once(SERVER_ROOT."/../classes/irl.php");
	$irl = new IRL();
	$ext = $fm->Extension($file['2']);
	$format = $get['format'];
	if($format!='' && $format!='jpg') {
		$file['2'] = str_replace(".{$ext}",".{$format}",$file['2']);
	}
	// $url = $irl->CDNURL($file['0'], $file['2'], $file['1']);
	$orig = false;
	if($file['0']=='videos') {
		$orig_file = $fm->RealPath("uploads/{$file['0']}/{$file['1']}/{$file['2']}");
		$orig = true;
	} else {
		$orig_file = $fm->RealPath("uploads/{$file['0']}/orig/{$file['2']}");
		$orig = $file['1']=='orig';
	}
	if($fm->Exists($orig_file,true)) {
		list($orig_width, $orig_height, $type, $attr) = getimagesize($orig_file);
		if($orig) {
		    $new_width = $orig_width;
		    $new_height = $orig_height;
		} else {
			if($file['1']>=100) {
				$new_width = $file['1'];
			} else {
				$size = (int)$file['1'];
				if($file['0']=='orgs' || $file['0']=='events') {
					$size++;
				}
				$new_width = $irl->img_sizes[$size];
			}
		    $new_height = $orig_height * $new_width / $orig_width;
		}
		$image = new Imagick($orig_file);
		$image->resizeImage($new_width, $new_height, imagick::FILTER_LANCZOS, 1);
		$image->setImageCompression(Imagick::COMPRESSION_JPEG);
		$image->setImageCompressionQuality('90');
		header("Content-type: image/jpeg");
		echo $image;
	}
} else {
	$src = "uploads/" . $get['src'];
	$fm->GetLocalFile($src);
}
?>
