<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/banners.php");
include_once(SERVER_ROOT."/../classes/file.php");

$id = $_GET['id'];
$id_group = $_GET['id_group'];

$b = new Banners;
$group = $b->GroupGet($id_group);

$title[] = array('banner_groups','banners_groups.php');
$title[] = array($group['name'],'banners.php?id='.$id_group);

if ($id>0)
{
	$row = $b->BannerGet($id);
	$title[] = array('Banner','');
	$popup = $row['popup'];
}
else
{
	$title[] = array('banner_add','');
	$popup = 1;
}

if ($module_admin || $group['id_user']==$ah->current_user_id)
	$input_right = 1;

if ($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

$fm = new FileManager;
$maxfilesize = $fm->MaxFileSize();
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			link: "required",
			alt_text: "required"
		}
	});
});
</script>
<?php
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_group",$id_group);
echo $hh->input_hidden("id_banner",$id);
echo $hh->input_hidden("from","banner");
echo $hh->input_table_open();

if($group['width']>0 && $group['height']>0)
	echo $hh->input_note($hh->tr->TranslateParams("banner_size",array($group['width'],$group['height'])));
if ($id==0)
{
	echo $hh->input_upload("choose_file","img",50,$input_right);
}
else
{
	include_once(SERVER_ROOT."/../classes/file.php");
	$sizes = $fm->ImageSize("uploads/banners/orig/{$id}.{$row['format']}");
	echo $hh->show_banner($id,$group['width'],$group['height'],$row['format']);
	echo $hh->input_text("size","size_x","{$sizes['width']}x{$sizes['height']} px",50,0,0);
	echo $hh->input_upload("substitute_with","img",50,$input_right);
}

echo $hh->input_text("link","link",$row['link'],50,0,$input_right);
echo $hh->input_textarea("caption","alt_text",$row['alt_text'],80,4,"",$input_right);
echo $hh->input_date("date_start","start_date",$row['start_date_ts'],$input_right);
echo $hh->input_checkbox("expires","can_expire",$row['can_expire'],0,$input_right);
echo $hh->input_date("date_end","expire_date",$row['expire_date_ts'],$input_right);
echo $hh->input_checkbox("popup","popup",$popup,0,$input_right);

$groups = array();
$b->Groups($groups,false);
echo $hh->input_row("banner_group","id_group",$id_group,$groups,"",0,$input_super_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);

echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if ($id>0)
{
	echo $hh->input_code("banner_html","banner_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/banner.php?id=$id\"></script>",60,2);
	echo $hh->input_code("banner_xsl","banner_xsl","<xsl:call-template name=\"banner\"><xsl:with-param name=\"id\" select=\"'$id'\"/></xsl:call-template>",60,2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

