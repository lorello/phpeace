<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_from = (int)$get['id_from'];
$id_to = (int)$get['id_to'];
$from = $get['from'];

$title[] = array("Tools",'tools.php');
$title[] = array("merge",'');
echo $hh->ShowTitle($title);

$dodc = new DodContractors();

$trm25 = new Translator($hh->tr->id_language,25);

$input_right = $module_admin;

if($id_from>0 && $id_to>0)
{
	$office_from = $dodc->OfficeGet($id_from);
	$office_to = $dodc->OfficeGet($id_to);

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","office_merge");
	echo $hh->input_hidden("id_from",$id_from);
	echo $hh->input_hidden("id_to",$id_to);
	echo $hh->input_table_open();
	echo $hh->input_text("from","from_name","{$office_from['name']} ({$office_from['code']})",50,0,0);
	echo $hh->input_text("to","to_name","{$office_to['name']} ({$office_to['code']})",50,0,0);
	$actions = array();
	$actions[] = array('action'=>"merge",'label'=>"merge",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
elseif($from=="from" || $from=="to") 
{
	$params = array(	'name' => $get['name'] );
	if($id_from>0)
		$params['id_from'] = $id_from;
	
	$num = $dodc->OfficeSearch($row, $params);
	
	$table_headers = array('code','name');
	$table_content = array('$row[code]','{LinkTitle("office_merge.php?' . ($from=="from"? 'id_from=$row[id_office]':'id_from='.$id_from.'&&id_to=$row[id_office]') . '","$row[name]")}');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}
else 
{
	echo $hh->input_form("get","office_merge.php");
	echo $hh->input_table_open();
	if($id_from>0)
	{
		echo $hh->input_hidden("from","to");
		echo $hh->input_hidden("id_from",$id_from);
		$office_from = $dodc->OfficeGet($id_from);
		echo $hh->input_text("from","from_name","{$office_from['name']} ({$office_from['code']})","30",0,0);
	}
	else 
		echo $hh->input_hidden("from","from");
	echo $hh->input_text("name","name","","30",0,$input_right);
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>
