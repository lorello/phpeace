<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_contractor = $_GET['id'];

$duns = $_GET['duns'];

$dodc = new DodContractors();

if(strlen($duns)==9 && is_numeric($duns) && $module_admin)
{
	echo $dodc->DunsLookup($duns);
}
else
{
	$trm25 = new Translator($hh->tr->id_language,25);
	$row = $dodc->ContractorGet($id_contractor);
	if($row['id_contractor']>0)
	{
		$ctotal = $dodc->ContractorTotalGet($id_contractor);
		$title[] = array("Contractors",'contractors.php?country='.$row['country_code']);
		$title[] = array($row['name'],'contractor.php?id='.$id_contractor);
		$duns = $row['duns'];
		$title[] = array($duns,'');
		echo $hh->ShowTitle($title);	
		
		echo "<h1>{$row['name']}</h1>";
		echo "<ul>";
		echo "<li><a href=\"http://www.usaspending.gov/fpds/fpds.php?reptype=r&database=fpds&detail=1&first_year_range=2000&last_year_range=2008&duns_number={$row['duns']}\" target=\"_blank\">USASpending</a></li>";
		echo "<li><a href=\"http://www.alacrastore.com/storecontent/dnb2/{$row['duns']}\" target=\"_blank\">Alacra</a></li>";
		echo "<li><a href=\"duns.php?duns={$row['duns']}\" target=\"_blank\">CCR</a></li>"; 
		echo "</ul>";
	}
}
			
include_once(SERVER_ROOT."/include/footer.php");
?>
