<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_category = $_GET['id'];

$trm25 = new Translator($hh->tr->id_language,25);

if($id_category>0)
{
	$dodc = new DodContractors();
	$row = $dodc->CategoryGet($id_category);
	if($row['id_category']>0)
	{
		$title[] = array($trm25->Translate("categories"),'categories.php');
		$title[] = array($row['description'],'');
		echo $hh->ShowTitle($title);
		
		echo "<h1>{$row['description']}</h1>";

		echo "<ul>";
		$num = $dodc->CategoryContractors( $row, $id_category );
		echo "<li><a href=\"category_contractors.php?id=$id_category\">" . $trm25->Translate("contractors") . "</a> ($num)</li>";
		echo "</ul>";
		
		// fornitori
		// contratti in ordine cronologico
		// contratti per periodo
		// contratti per servizio
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
