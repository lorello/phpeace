<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/mastodon.php");

$m = new Mastodon();

echo $hh->ShowTitle($title);

echo "<h3>Mastodon</h3>\n";

echo "<p>Server: <a href=\"{$m->server}\" target=\"_blank\">{$m->server}</a></p>";

$admin = $m->Account($m->mastodon_admin);
echo "<p>Admin: {$admin['display_name']} <a href=\"{$admin['url']}\" target=\"_blank\">{$admin['url']}</a></p>";

$local = $m->Account($m->mastodon_local);
echo "<p>Account: {$local['display_name']} <a href=\"{$local['url']}\" target=\"_blank\">{$local['url']}</a></p>";

if($module_admin) {
    echo "<h3>Amministrazione</h3>\n";
    echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>";
    echo "<p><a href=\"admin_followers.php\">@{$admin['username']} followers</a></p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
