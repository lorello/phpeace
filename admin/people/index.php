<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$input_right = $module_right;

$trm28 = new Translator($hh->tr->id_language,28);

echo $hh->ShowTitle($title);

echo $hh->input_form("get","search.php");
echo $hh->input_table_open();

echo $hh->input_text("name","name","","30",0,$input_right);
echo $hh->input_text("email","email","","30",0,$input_right);
echo $hh->input_text("Start Date","start_date","","30",0,$input_right);
echo $hh->input_text("End Date","end_date","","30",0,$input_right);
echo $hh->input_geo(0,$input_right);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics();
echo $hh->input_topics(0,0,$topics,"all_option",$module_right);

$tgroups = array();
$num_groups = $tt->PeopleGroups($tgroups);
if($num_groups>0)
	echo $hh->input_row("group","id_pt_group",0,$tgroups,"all_option",0,$input_right);

echo $hh->input_array("email_verified","verified",0,$hh->tr->Translate("any_yes_no"),$module_right); 
echo $hh->input_array("contact_email","contact",0,$hh->tr->Translate("any_yes_no"),$module_right); 
echo $hh->input_checkbox("active_only","active",1,0,$input_right);

$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if ($module_admin)
{
	echo "<p><a href=\"groups.php\">" . $hh->tr->Translate("groups") . "</a></p>\n";
	if($conf->Get("track") || $conf->Get("track_all"))
	{
		echo "<p><a href=\"visits.php\">" . $hh->tr->Translate("visits") . "</a></p>\n";
		echo "<h3>" . $hh->tr->Translate("stats") . "</h3>\n";
		echo "<ul>\n";
		echo "<li><a href=\"stats.php\">" . $hh->tr->Translate("periodic") . "</a></li>\n";
		echo "<li><a href=\"stats_topics.php\">" . $hh->tr->Translate("topics") . "</a></li>\n";
		echo "</ul>\n";
	}
	echo "<p><a href=\"mailjob.php?act=filter\">" . $hh->tr->Translate("mailjob") . "</a></p>\n";
	echo "<p><a href=\"person.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
	echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>\n";

	echo "<h3>" . $hh->tr->Translate("mainteinance") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"import.php\">" . $hh->tr->Translate("import") . "</a></li>\n";
	echo "<li><a href=\"merge.php\">" . $hh->tr->Translate("merge") . "</a></li>\n";
	echo "<li><a href=\"domain_change.php\">" . $trm28->Translate("domain_change") . "</a></li>\n";
	echo "</ul>";
}
	
include_once(SERVER_ROOT."/include/footer.php");
?>
