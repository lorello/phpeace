<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

$title[] = array('results','');
echo $hh->ShowTitle($title);

$params = array('name' => $get['name'],
			'email' => $get['email'],
			'contact' => (int)$get['contact'],
			'active' => $get['active'],
			'verified' => $get['verified'],
			'id_geo' => $get['id_geo'],
			'id_topic' => $get['id_topic'],
			'id_pt_group' => $get['id_pt_group'],
            'member_type' => (array_key_exists('member_type', $get) ? $get['member_type'] : ''),
            'entitlement' => (array_key_exists('entitlement', $get) ? $get['entitlement'] : array()),
            'start_date' => $get['start_date'],
            'end_date' => $get['end_date'],
            'sort_by' => $get['sort_by']
			);

$num = $pe->Search( $row, $params);

$table_headers = array('name','email','verified','contact_email',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date');
$table_content = array('{LinkTitle("person.php?id=$row[id_p]","$row[full_name]")}','$row[email]','{Bool2YN($row[verified])}',
'{Bool2YN($row[contact])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');

if ($module_admin) {
	$table_headers[] = '&nbsp;';
	$table_content[] = '{LinkTitle("actions.php?from2=peoplesearch&action3=remove&id_p=$row[id_p]","' . $hh->tr->Translate("remove") . '")}';
}

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if($module_admin)
	echo "<p><a href=\"csv.php?" . $_SERVER['QUERY_STRING'] . "\">CSV dump</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

