<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

$trm28 = new Translator($hh->tr->id_language,28);

$id_p = $_GET['id'];

$row = $pe->UserGetDetailsById($id_p);

if($module_admin && isset($row['id_p']))
	$input_right = 1;

if ($row['bounces']>=$conf->Get("bounces_threshold"))
{
	$ah->MessageSet("bounces_warning",array($conf->Get("bounces_threshold")));
}

$title[] = array($row['name1'] . " " . $row['name2'],'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/people/person.php?id='.$id_p);
$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
$tabs[] = array('groups','/people/person_groups.php?id='.$id_p);
$tabs[] = array('history','/people/person_history.php?id='.$id_p);
$tabs[] = array('topics','/people/person_topics.php?id='.$id_p);
$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
if($conf->Get("track") || $conf->Get("track_all"))
	$tabs[] = array('visits','/people/visits.php?id_p='.$id_p);
echo $hh->Tabs($tabs);

echo $hh->input_form_open();
echo $hh->input_hidden("from","person_delete");
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_table_open();

echo "<p>" . $hh->tr->TranslateParams("person_delete",array($row['name1'] . " " . $row['name2'])) . "</p>";

echo $hh->input_table_open();
echo $hh->input_separator("are_you_sure");
$actions = array();
$actions[] = array('action'=>"delete_ok",'label'=>"yes",'right'=>$input_right);
$actions[] = array('action'=>"delete_no",'label'=>"no",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


