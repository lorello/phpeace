<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");
include_once(SERVER_ROOT."/../classes/topics.php");
$post = $fh->HttpPost();

$id_module = 28;

$pe = new People();

$mj = new Mailjobs($id_module);
$recipients = $mj->RecipientsGet();
$num_recipients = sizeof($recipients);

$id_item = 1;
$act = $_GET['act'];  // type, filter, search, showdest, message, send, reset
$format = (isset($_GET['format']))? $_GET['format'] : "text";  // article, html or text

$title[] = array('mailjob','');

if ($module_admin)
{
	if($id_item>0 && $mj->MailJobCheck($id_module,$id_item))
	{
		$ah->MessageSet("mailjob_already_present");
		echo $hh->ShowTitle($title);
	}
	else
	{
		echo $hh->ShowTitle($title);
		switch($act)
		{
			case "filter":
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . "</p>";
					echo "<h4><a href=\"mailjob.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
					echo "<p>" . $hh->tr->Translate("mailjob_filter_again") . "</p>";
				}
				else
					echo "<p>" . $hh->tr->Translate("mailjob_filter") . "</p>";
				echo $hh->input_form("post","mailjob.php?id_item=$id_item&act=search");
				echo $hh->input_table_open();
				echo $hh->input_text("name","name","",30,0,1);
				echo $hh->input_text("email","email","",30,0,1);
				echo $hh->input_geo(0,1);
				$tgroups = array();
				$tt = new Topics();
				$num_groups = $tt->PeopleGroups($tgroups);
				if($num_groups>0)
				    echo $hh->input_row("group","id_pt_group",0,$tgroups,"all_option",0,1);
			    echo $hh->input_checkbox("verified","verified",0,0,1);
			    echo $hh->input_checkbox("search_new","reset",1,0,1);
				$actions[] = array('action'=>"search",'label'=>"search",'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "search":
				$db =& Db::globaldb();
				$search_params = array();
				$search_params['name'] = $post['name'];
				$search_params['email'] = $post['email'];
				$search_params['id_geo'] = $post['id_geo'];
				$search_params['verified'] = $post['verified'];
				$search_params['id_pt_group'] = $post['id_pt_group'];
				$num = $pe->MailjobSearch($id_item,$search_params);
				if($num>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4>" . $hh->tr->Translate("mailjob_send") . ": <a href=\"mailjob.php?act=message&format=text&id_item=$id_item\">TEXT</a> / <a href=\"mailjob.php?act=message&format=html&id_item=$id_item\">HTML</a> / <a href=\"mailjob.php?act=message&format=article&id_item=$id_item\">Article</a></h4>";
					echo "<p><a href=\"mailjob.php?act=showdest&id_item=$id_item\">" . $hh->tr->Translate("mailjob_showdest") . "</a></p>";
				}
				else 
					echo "<p>" . $hh->tr->TranslateParams("num_results",array(0)) . "</p>";
					
			break;
			case "showdest":
				$delete = (int)$_GET['delete'];
				if($delete>0)
				{
					$mj->RecipientDelete($delete);
					$recipients = $mj->RecipientsGet();
					$num_recipients = sizeof($recipients);
				}
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4>" . $hh->tr->Translate("mailjob_send") . ": <a href=\"mailjob.php?act=message&format=text&id_item=$id_item\">TEXT</a> / <a href=\"mailjob.php?act=message&format=html&id_item=$id_item\">HTML</a> / <a href=\"mailjob.php?act=message&format=article&id_item=$id_item\">Article</a></h4>";
				}
				$row = array_slice($recipients,($current_page-1)*$records_per_page,$records_per_page);
				$table_headers = array('name','email','');
				$table_content = array('{LinkTitle("person.php?id=$row[id]",$row[name])}','$row[email]','{LinkTitle("mailjob.php?act=showdest&id_item='.$id_item.'&delete=$row[id]","(delete)")}');
				echo $hh->ShowTable($row, $table_headers, $table_content, sizeof($recipients));
			break;
			case "message":
			    echo "<h3>" . $mj_types[$id_item] . " (<a href=\"mailjob.php?act=reset\">". $hh->tr->Translate("reset") .  "</a>)</h3>";
			    if($num_recipients>0)
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
				include_once(SERVER_ROOT."/../classes/user.php");
				$u = new User();
				$user = $u->UserGet();
				$feedback_test = $hh->tr->TranslateParams("message_sent",array($user['email']));
				?>
				<script type="text/javascript" src="/js/jquery/jquery.form.js"></script> 
				<script type="text/javascript">
				$().ready(function() {
					$("#form1").validate({
							rules: {
								name: "required",
								subject: "required",
								email: {
									required: true,
									email: true
								}
							}
						});
					$(".input-submit").click(function(e){
						if(($(this).attr("name"))=="action_test") {
							var options = {
								url: "/gate/actions.php?from2=mailjob_test&id_topic=<?=$id_topic;?>",
								success:       showResponse
							};
							$('#form1').ajaxSubmit(options);
							return false; 
						}
						else
						{
							return true;
						}
						
					});
				});
				function showResponse(responseText, statusText, xhr, $form)  {
					if(statusText=="success") {
						alert("<?=$feedback_test;?>");
					} 
				} 
				</script>
				<?php	
				echo $hh->input_form("post","mailjob.php?act=send");
				echo $hh->input_hidden("MAX_FILE_SIZE",$mj->attachment_max_size);
				echo $hh->input_hidden("id_item",$id_item);
				echo $hh->input_table_open();
				echo $hh->Input_date("date","start_date",0,1);
				echo $hh->input_time("hour","start_date",0,1);
				echo $hh->input_separator("sender");
				echo $hh->input_text("name","name",$user['name'],30,0,1);
				echo $hh->input_text("email","email",$user['email'],30,0,1);
				echo $hh->input_separator("message");
				echo $hh->input_note($hh->tr->Translate("format") . ": " . strtoupper($format));
				echo $hh->input_text("subject","subject","",50,0,1);
				if($format=="html")
				{
					echo $hh->input_wysiwyg("text_html","body","",1,20,1,true);
					echo $hh->input_textarea("alt_text","alt_text","",80,10,"",1);
				}
				if($format=="text") 
				{
					echo $hh->input_textarea("text","body","",80,30,"",1);
				}
				if($format=="article") 
				{
					echo $hh->input_text("id_article","id_article","",5,0,1);
					echo $hh->input_checkbox("embed_images","embed_images",1,0,1);
					echo $hh->input_textarea("alt_text","alt_text","",80,10,"",1);
				}
				echo $hh->input_array("mailjob_footer","auto_footer",2,$hh->tr->Translate("mailjob_footer_options"),0);
				echo $hh->input_checkbox("send_password","send_password","0",0,0);				
				echo $hh->input_checkbox("track","track","1",0,0);				
				echo $hh->input_text("track_redirect","track_redirect","",30,0,1);
				echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor(($mj->attachment_max_size)/1024))));
				echo $hh->input_upload("attachment","doc",50,1);
				$actions[] = array('action'=>"send",'label'=>"send",'right'=>1);
				$actions[] = array('action'=>"test",'label'=>$hh->tr->TranslateParams("mailjob_test",array($user['email'])),'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "send":
				include_once(SERVER_ROOT."/../classes/formhelper.php");
				$fh = new FormHelper(true);
				$post = $fh->HttpPost();
				$start_date 	= $fh->Strings2Datetime($post['start_date_d'],$post['start_date_m'],$post['start_date_y'],$post['start_date_h'],$post['start_date_i']);
				$name		= $post['name'];
				$email		= $post['email'];
				$subject	= $post['subject'];
				$body		= $post['body'];
				$alt_text	= $post['alt_text'];
				$id_article	= $fh->Null2Zero($post['id_article']);
				$is_html	= $fh->Checkbox2bool($post['is_html']);
				$auto_footer		= $fh->String2Number($post['auto_footer'],1);
				$embed_images		= $fh->Checkbox2bool($post['embed_images']);
				$send_password	= $fh->Checkbox2bool($post['send_password']);
				$track		= $fh->Checkbox2bool($post['track']);
				$track_redirect		= $fh->String2Url($post['track_redirect']);
				$file		= $fh->UploadedFile("doc",true,$mj->attachment_max_size);
				$id_item	= $post['id_item'];
				$fh->va->NotEmpty($name,"name");
				$fh->va->Email($email);
				$fh->va->NotEmpty($subject,"title");
				if($fh->va->return)
				{
					$unescaped_footer = $auto_footer>0? $mj->MessageFooter(0,$send_password,$is_html,$auto_footer==2) : "";
					$mj->Store($start_date,$name,$email,$ah->current_user_id,$subject,$body,$id_module,$id_item,$recipients,$unescaped_footer,$send_password,$is_html,$file,$track,$track_redirect,0,$id_article,$auto_footer,$alt_text,$embed_images);
					echo "<p>" . $hh->tr->Translate("mailjob_queued") . "</p>";
				}
				else 
					echo $hh->MessageShow();
			break;
			case "reset":
				$mj->RecipientsDelete();
				echo "<p><a href=\"mailjob.php?act=filter\">" . $hh->tr->Translate("mailjob_reset") . "</a></p>";
			break;
		}
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
