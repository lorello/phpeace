<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper();
$get = $fh->HttpGet();
$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

if($module_admin)
{
	$params = array(	'name' => $get['name'],
				'email' => $get['email'],
				'contact' => (int)$get['contact'],
				'active' => $get['active'],
				'verified' => $get['verified'],
				'id_geo' => $get['id_geo'],
				'id_topic' => $get['id_topic'],
				'id_pt_group' => $get['id_pt_group'],
                'member_type' => (array_key_exists('member_type', $get) ? $get['member_type'] : ''),
                'entitlement' => (array_key_exists('entitlement', $get) ? $get['entitlement'] : array()),
	            'start_date' => $get['start_date'],
	            'end_date' => $get['end_date']
				);
	
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$ini = new Ini();
	$rows = array();
	$num = $pe->Search( $rows, $params, false);

	$headers = array("INSERT_DATE","NAME1","NAME2","NAME3","EMAIL","CONTACT","VERIFIED");
    $headers[] = 'ADMIN_LINK';
    
	include_once(SERVER_ROOT."/../classes/texthelper.php");
	$th = new TextHelper();
			
	$link = $ini->get("admin_web") . "/people/person.php?id=";
	$data = array();
	foreach($rows as $row)
	{
		$data_row = array();
		$data_row[] = date("Y-m-d H:i:s",$row['start_date_ts']);
		$data_row[] = $row['name1'];
		$data_row[] = $row['name2'];
		$data_row[] = $row['name3'];
		$data_row[] = $row['email'];
		$data_row[] = $row['contact'] == 1 ? 'Yes' : 'No';
		$data_row[] = $row['verified'] == 1 ? 'Yes' : 'No';
		$data_row[] = $link . $row['id_p'];
		$data[] = $data_row;
	}
	echo $th->CSVDump("contacts_t{$params['id_topic']}_g{$params['id_pt_group']}",$headers,$data);
}

?>

