<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

$id_p = $_GET['id'];

$row = $pe->UserGetDetailsById($id_p);

$title[] = array($row['name1'] . " " . $row['name2'],'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/people/person.php?id='.$id_p);
$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
$tabs[] = array('groups','/people/person_groups.php?id='.$id_p);
$tabs[] = array('history','');
$tabs[] = array('topics','/people/person_topics.php?id='.$id_p);
$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
if($conf->Get("track") || $conf->Get("track_all"))
	$tabs[] = array('visits','/people/visits.php?id_p='.$id_p);
echo $hh->Tabs($tabs);

$trm28 = new Translator($hh->tr->id_language,28);
$hhf = new HHFunctions();

echo "<ul>";
include_once(SERVER_ROOT."/../classes/forms.php");
$fo = new Forms();
$posts = $fo->PostsPerson($id_p);
if(count($posts)>0)
{
	echo "<li><h3>Posts</h3><ul>";
	foreach($posts as $post)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($post['hdate_ts']) . "</div>";
		echo "<a href=\"/topics/form_post.php?id={$post['id_post']}&id_form={$post['id_form']}&id_topic={$post['id_topic']}\">{$post['name']}</a></li>";
	}
	echo "</ul></li>\n";
}
include_once(SERVER_ROOT."/../classes/campaigns.php");
$campaigns = Campaigns::ProposedPerson($id_p);
if(count($campaigns)>0)
{
	echo "<li><h3>" . $hh->tr->Translate("campaigns") . "</h3><ul>";
	foreach($campaigns as $campaign)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($campaign['hdate_ts']) . "</div>";
		echo "<a href=\"/topics/campaign.php?id={$campaign['id_topic_campaign']}&id_topic={$campaign['id_topic']}\">{$campaign['name']}</a></li>";
	}
	echo "</ul></li>\n";
}
$signatures = Campaigns::SignedPerson($id_p);
if(count($signatures)>0)
{
	echo "<li><h3>" . $hh->tr->Translate("signatures") . "</h3><ul>";
	foreach($signatures as $signature)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($signature['hdate_ts']) . "</div>";
		echo "<a href=\"/topics/campaign_person.php?id={$signature['id_person']}&id_c={$signature['id_topic_campaign']}&id_topic={$signature['id_topic']}\">{$signature['name']}</a></li>";
	}
	echo "</ul></li>\n";
}
$signatures = Campaigns::SignedOrg($id_p);
if(count($signatures)>0)
{
    echo "<li><h3>" . $hh->tr->Translate("signatures") . "</h3><ul>";
    foreach($signatures as $signature)
    {
        echo "<li>";
        echo "<div>" . $hh->FormatDate($signature['hdate_ts']) . "</div>";
        echo "<a href=\"/topics/campaign_org.php?id={$signature['id_org']}&id_c={$signature['id_topic_campaign']}&id_topic={$signature['id_topic']}\">{$signature['name']}</a></li>";
    }
    echo "</ul></li>\n";
}
include_once(SERVER_ROOT."/../classes/polls.php");
$pl = new Polls();
$polls = $pl->VotedPerson($id_p);
if(count($polls)>0)
{
	echo "<li><h3>" . $hh->tr->Translate("polls") . "</h3><ul>";
	foreach($polls as $poll)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($poll['hdate_ts']) . "</div>";
		echo "<a href=\"/topics/poll_question_answer.php?id_p=$id_p&id={$poll['id_question']}&id_poll={$poll['id_poll']}\">{$poll['title']}</a></li>";
	}
	echo "</ul></li>\n";
}
include_once(SERVER_ROOT."/../classes/comments.php");
$co1 = new Comments("article",0);
$comments1 = $co1->Submitted($id_p);
$co2 = new Comments("thread",0);
$comments2 = $co2->Submitted($id_p);
$co3 = new Comments("question",0);
$comments3 = $co3->Submitted($id_p);
if(count($comments1)>0 || count($comments2)>0 || count($comments3)>0)
{
	echo "<li><h3>" . $hh->tr->Translate("comments") . "</h3><ul>";
	foreach($comments1 as $comment1)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($comment1['hdate_ts']) . "</div>";
		echo $hh->tr->Translate("article") . ": <a href=\"/topics/comments.php?type=article&id_item={$comment1['id_article']}\">{$comment1['headline']}</a></li>";
	}
	foreach($comments2 as $comment2)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($comment2['hdate_ts']) . "</div>";
		echo $hh->tr->Translate("forum") . ": <a href=\"/topics/comments.php?type=thread&id_item={$comment2['id_thread']}\">{$comment2['title']}</a></li>";
	}
	foreach($comments3 as $comment3)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($comment3['hdate_ts']) . "</div>";
		echo $hh->tr->Translate("poll_question") . ": <a href=\"/topics/comments.php?type=question&id_item={$comment3['id_question']}\">{$comment3['question']}</a></li>";
	}
	echo "</ul></li>\n";
}

if($ah->IsModuleActive(12))
{
	include_once(SERVER_ROOT."/../classes/events.php");
	$ee = new Events();
	$events = $ee->Submitted($id_p,0);
	if(count($events)>0)
	{
		echo "<li><h3>" . $hh->tr->Translate("events") . "</h3><ul>";
		foreach($events as $event)
		{
			echo "<li>";
			echo "<div>" . $hh->FormatDate($event['hdate_ts']) . "</div>";
			echo $hh->FormatDate($event['start_date_ts']) . ": {$event['type']} - <a href=\"/events/event.php?id={$event['id_event']}\">{$event['title']}</a></li>";
		}
		echo "</ul></li>\n";
	}
}
if($ah->IsModuleActive(16))
{
	$trm16 = new Translator($hh->tr->id_language,16);
	include_once(SERVER_ROOT."/../modules/books.php");
	$bb = new Books();
	$reviews = $bb->ReviewsPerson($id_p);
	if(count($reviews)>0)
	{
		echo "<li><h3>" . $trm16->Translate("reviews") . "</h3><ul>";
		foreach($reviews as $review)
		{
			echo "<li>";
			echo "<div>" . $hh->FormatDate($review['hdate_ts']) . "</div>";
			echo "<a href=\"/books/review.php?id={$review['id_review']}&id_book={$review['id_book']}\">{$review['title']}</a></li>";
		}
		echo "</ul></li>\n";
	}
}
if($ah->IsModuleActive(8))
{
	include_once(SERVER_ROOT."/../modules/assos.php");
	$as = new Assos();
	$orgs = $as->Mantained($id_p);
	if(count($orgs)>0)
	{
		echo "<li><h3>" . $hh->tr->Translate("orgs") . "</h3><ul>";
		foreach($orgs as $org)
		{
			echo "<li>";
			echo "<div>" . $hh->FormatDate($org['hdate_ts']) . "</div>";
			echo "<a href=\"/orgs/org.php?id={$org['id_ass']}\">{$org['nome']}</a></li>";
		}
		echo "</ul></li>\n";
	}
}
if($ah->IsModuleActive(27))
{
	$trm27 = new Translator($hh->tr->id_language,27);
	include_once(SERVER_ROOT."/../classes/ecommerce.php");
	$ec = new eCommerce();
	$orders = $ec->OrdersPerson($id_p);
	if(count($orders)>0)
	{
		echo "<li><h3>" . $trm27->Translate("orders") . "</h3><ul>";
		foreach($orders as $order)
		{
			echo "<li>";
			echo "<div>" . $hh->FormatDate($order['hdate_ts']) . "</div>";
			echo "<a href=\"/ecommerce/order.php?id={$order['id_order']}\">" . $trm27->Translate("order") . " #{$order['id_order']}</a></li>";
		}
		echo "</ul></li>\n";
	}
}
include_once(SERVER_ROOT."/../classes/payment.php");
$p = new Payment();
$payments = array();
$num_payments = $p->PaymentsPerson($payments,$id_p,false,true);
if($num_payments>0)
{
	echo "<li><h3>" . $hh->tr->Translate("payments") . "</h3><ul>";
	foreach($payments as $payment)
	{
		echo "<li>";
		echo "<div>" . $hh->FormatDate($payment['hdate_ts']) . "</div>";
		echo "<strong>" . ($payment['is_in']?'':'-') . $payment['amount'] . "</strong> - ";
		echo $hhf->LinkTitle("/payments/payment.php?id=" . $payment['id_payment'],($payment['description']!=''?$payment['description']:$payment['tx_id'])) . "</li>";
	}
	echo "</ul></li>\n";
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>

