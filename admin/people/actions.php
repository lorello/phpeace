<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/people.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper(true,28);
$post = $fh->HttpPost();
$get = $fh->HttpGet();

$from = $post['from'];
$action = $fh->ActionGet($post);
$from2		= $get['from2'];
$action3	= $get['action3'];

$pe = new People();

if ($from=="person")
{
	$id_p		= $post['id_p'];
	$salutation	= $post['salutation'];
	$name1	= $post['name1'];
	$name2	= $post['name2'];
	$name3	= $post['name3'];
	$email		= $post['email'];
	$address	= $post['address'];
	$address_notes	 = $post['address_notes'];
	$postcode	= $post['postcode'];
	$town		= $post['town'];
	$id_geo	= $post['id_geo'];
	$phone	= $post['phone'];
	if ($action=="update")
	{
		$user = $pe->UserGetByEmail($email);
		if($user['id_p']>0 && $user['id_p']!=$id_p)
		{
			$fh->va->MessageSet("error","user_exists",array($email));
		}
		else 
		{
			$fh->va->feedback = false;
			$email_valid = ($fh->va->Email($email))? "1":"0";
			if($id_p>0)
			{
				$pe->UserUpdate($id_p,$salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone);
				$user_params = $fh->SerializeParams(false);
				if(count($user_params)>0)
					$pe->UserParamsUpdate($id_p, $user_params);
				$pe->RevalidateEmail($id_p,$email_valid);
			}
			else 
			{
				$id_p = $pe->UserCreate($name1,$name2,$email,$id_geo,0,array(),false,0,0,false,$email_valid);
				$pe->UserUpdate($id_p,$salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone);
			}
		}
	}
	if ($action=="swap_names")
	{
		$pe->UserUpdate($id_p,$salutation,$name2,$name1,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone);
	}
	header("Location: person.php?id=$id_p");
}

if ($from=="person_admin")
{
	if ($action=="update")
	{
		$id_p	 	= $post['id_p'];
		$id_language	= $post['id_language'];
		$admin_notes 	= $post['admin_notes'];
		$verified	= $fh->Checkbox2Bool($post['verified']);
		$active		= $fh->Checkbox2Bool($post['active']);
		$contact	= $fh->Checkbox2Bool($post['contact']);
		$email_valid	= $fh->Checkbox2Bool($post['email_valid']);
		$pe->UserUpdateAdmin($id_p,$id_language,$admin_notes,$verified,$active,$contact,$email_valid);
		header("Location: person.php?id=$id_p");
	}
	
	if ($action=="send_password")
	{
		$id_p	 	= $post['id_p'];
		$id_language	= $post['id_language'];
		$admin_notes 	= $post['admin_notes'];
		$verified	= $fh->Checkbox2Bool($post['verified']);
		$active		= $fh->Checkbox2Bool($post['active']);
		$contact	= $fh->Checkbox2Bool($post['contact']);
		$email_valid	= $fh->Checkbox2Bool($post['email_valid']);
		$pe->UserUpdateAdmin($id_p,$id_language,$admin_notes,$verified,$active,$contact,$email_valid);
		
		$row = $pe->UserGetById($id_p);
		$id_topic = 0;
		$pe->Reminder($row['email'],$id_topic);
		$ah->MessageSet("password_sent",array($row['email']));
		header("Location: person.php?id=$id_p");
	}

	if ($action=="email_verify")
	{
		$id_p	 	= $post['id_p'];
		$row = $pe->UserGetById($id_p);
		$trm28 = new Translator($ah->session->Get("id_language"),28);
		$id_topic = 0; 
		$ret = $pe->VerifyEmail($id_p, $id_topic);
		if($ret>0)
			$ah->MessageSetTranslated($trm28->Translate("error_check" . $ret));
		else
			$ah->MessageSetTranslated($trm28->TranslateParams("registration_check_sent",array($row['email'])));
		header("Location: person.php?id=$id_p");
	}

	if ($action=="delete")
	{
		$id_p	 	= $post['id_p'];
		header("Location: person_delete.php?id=$id_p");
	}
}

if ($from=="person_delete")
{
	$action 	= $fh->ActionGet($post);
	$id_p	 	= $post['id_p'];
	if ($action == "delete_ok")
	{
		$user = $pe->UserGetById($id_p);
		$pe->Delete($id_p,false);
		$trm28 = new Translator($ah->session->Get("id_language"),28);
		$ah->MessageSetTranslated($trm28->TranslateParams("person_deleted",array($user['name1'] . " " . $user['name2'])));
		$url = "index.php";
	}
	else
		$url = "person_admin.php?id=$id_p";
	header("Location: " . $url);
}

if($from2=="peoplesearch" && $action3=="remove") {
	$module_admin = $ah->CheckModule();
	$id_p	 	= (int)$get['id_p'];
	if($id_p>0 && $module_admin) {
		$user = $pe->UserGetById($id_p);
		$pe->Delete($id_p,false);
		$trm28 = new Translator($ah->session->Get("id_language"),28);
		$ah->MessageSetTranslated($trm28->TranslateParams("person_deleted",array($user['name1'] . " " . $user['name2'])));
	}
	header("Location: index.php");
}

if ($from=="person_topics")
{
	$id_p	 	= $post['id_p'];
	include_once(SERVER_ROOT."/../classes/topic.php");
	foreach($pe->UserTopicsAll($id_p) as $topic)
	{
		$user_check = $fh->Checkbox2Bool($post[ $topic['id_topic'] ]);
		if ($user_check && !$topic['contact']>0)
		{
			$pe->TopicAssociate($id_p,$topic['id_topic'],1,0,false);
		}
		if (!$user_check && $topic['contact']>0)
		{
			$pe->TopicAssociate($id_p,$topic['id_topic'],0,0,false);
		}
	}
	header("Location: person.php?id=$id_p");
}


if ($from=="visit")
{
	$action 	= $fh->ActionGet($post);
	if ($action=="delete")
	{
		$id_visit	 	= $post['id_visit'];
		include_once(SERVER_ROOT."/../classes/tracker.php");
		$tk = new Tracker();
		$tk->VisitDelete($id_visit);
		header("Location: visits.php");
	}
}

if ($from=="merge")
{
	$id_from	= $post['id_from'];
	$id_to		= $post['id_to'];
	if($id_from>0 && $id_to>0 && $module_admin)
		$pe->Merge($id_from,$id_to);
	header("Location: person.php?id=$id_to");
}

if ($from=="people_import" && $module_admin)
{
	$id_topic		= $fh->Null2Zero($post['id_topic']);
	$contact		= $fh->Checkbox2bool($post['contact']);
	$id_geo		= $fh->Null2Zero($post['id_geo']);
	$data		= $post['data'];
	$num = $pe->Import($id_topic,$contact,$id_geo,0,$data);
	if($num>0)
	{
		$ah->MessageSetTranslated("$num accounts created");
	}
	header("Location: index.php");
}

if ($from=="config_update" && $module_admin)
{
	$profiling		= $fh->Checkbox2bool($post['profiling']);
	$users_path		= $post['users_path'];
	$pe->ConfigurationUpdate($profiling,$users_path);
	header("Location: index.php");
}

if ($from=="domain_change" && $module_admin)
{
	$domain_from	= $post['domain_from'];
	$domain_to		= $post['domain_to'];
	$num_changed = $pe->EmailDomainChange($domain_from,$domain_to);
	$ah->MessageSetTranslated("$num_changed email changed");
	header("Location: index.php");
}

if ($from=="people_group")
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$action 		= $fh->ActionGet($post);
	$id_pt_group		= $post['id_pt_group'];
	$name 			= $post['name'];
	$t = new Topic(0);
	if ($action=="update")
		$t->PeopleGroupStore($id_pt_group,$name);
	if ($action=="delete")
		$t->PeopleGroupDelete($id_pt_group);
	header("Location: groups.php");
}

if ($from=="person_groups")
{
	$id_p		= $post['id_p'];
	$pe->GroupsDelete($id_p,0);
	$pgroups = $pe->Groups($id_p,true,0);
	foreach($pgroups as $pgroup)
	{
		if ($fh->Checkbox2Bool($post["group{$pgroup['id_pt_group']}"]))
		{
			$pe->GroupAdd($id_p,$pgroup['id_pt_group']);
		}
	}
	header("Location: person.php?id=$id_p");
}


?>
