<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

if($module_admin)
	$input_right = 1;

$id_p = $_GET['id'];

if($id_p>0)
{
	$row = $pe->UserGetDetailsById($id_p);
	$title[] = array($row['name1'] . " " . $row['name2'],'');
	if ($row['bounces']>=$conf->Get("bounces_threshold"))
	{
		$ah->MessageSet("bounces_warning",array($conf->Get("bounces_threshold")));
	}
}
else
{
	$title[] = array($hh->tr->Translate("add_new"),'');
}


echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','');
$tabs[] = array('user_admin',$id_p>0? '/people/person_admin.php?id='.$id_p:'');
$tabs[] = array('groups',$id_p>0? '/people/person_groups.php?id='.$id_p:'');
$tabs[] = array('history',$id_p>0? '/people/person_history.php?id='.$id_p:'');
$tabs[] = array('topics',$id_p>0? '/people/person_topics.php?id='.$id_p:'');
$tabs[] = array('user_stats',$id_p>0? '/people/person_stats.php?id='.$id_p:'');
if($conf->Get("track") || $conf->Get("track_all"))
	$tabs[] = array('visits',$id_p>0? '/people/visits.php?id_p='.$id_p:'');
echo $hh->Tabs($tabs);
	
echo $hh->input_form_open();
echo $hh->input_hidden("from","person");
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_table_open();

echo $hh->input_text("salutation","salutation",$row['salutation'],10,0,$input_right);
echo $hh->input_text("name","name1",$row['name1'],30,0,$input_right);
echo $hh->input_text("name2","name2",$row['name2'],30,0,$input_right);
echo $hh->input_text("name3","name3",$row['name3'],30,0,$input_right);
echo $hh->input_text("email","email",$row['email'],50,0,$input_right);
echo $hh->input_textarea("address","address",$row['address'],80,4,"",$input_right);
echo $hh->input_textarea("address_notes","address_notes",$row['address_notes'],80,4,"",$input_right);
echo $hh->input_text("postcode","postcode",$row['postcode'],15,0,$input_right);
echo $hh->input_text("town","town",$row['town'],50,0,$input_right);
echo $hh->input_geo($row['id_geo'],$input_right);
echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);

if($id_p>0)
{
	$params = $pe->UserParamsGet($id_p,true);
	if(is_array($params) && count($params)>0)
	{
		echo $hh->input_separator("parameters");
		foreach($params as $key=>$value)
		{
			echo $hh->input_text($key,"param_" . $key,$value,50,0,$input_right);
		}
	}
}

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
//$actions[] = array('action'=>"swap_names",'label'=>"swap_names",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

