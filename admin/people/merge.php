<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

$id_from = (int)$get['id_from'];
$id_to = (int)$get['id_to'];
$from = $get['from'];

$input_right = $module_right;

$title[] = array('merge','');

echo $hh->ShowTitle($title);

if($id_from>0 && $id_to>0)
{
	$user_from = $pe->UserGetDetailsById($id_from);
	$user_to = $pe->UserGetDetailsById($id_to);

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","merge");
	echo $hh->input_hidden("id_from",$id_from);
	echo $hh->input_hidden("id_to",$id_to);
	echo $hh->input_table_open();
	echo $hh->input_text("from","from_name",$user_from['name1'] . " " . $user_from['name2'],50,0,0);
	echo $hh->input_text("to","to_name",$user_to['name1'] . " " . $user_to['name2'],50,0,0);
	$actions = array();
	$actions[] = array('action'=>"merge",'label'=>"merge",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
elseif($from=="from" || $from=="to") 
{
	$params = array(	'name' => $get['name'],
				'email' => $get['email']	);
	if($id_from>0)
		$params['id_from'] = $id_from;
	
	$num = $pe->Search($row, $params);
	
	$table_headers = array('name','email','verified','contact_email',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date');
	$table_content = array('{LinkTitle("merge.php?' . ($from=="from"? 'id_from=$row[id_p]':'id_from='.$id_from.'&&id_to=$row[id_p]') . '","$row[name2] $row[name1]")}','$row[email]','{Bool2YN($row[verified])}',
	'{Bool2YN($row[contact])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}
else 
{
	echo $hh->input_form("get","merge.php");
	echo $hh->input_table_open();
	if($id_from>0)
	{
		echo $hh->input_hidden("from","to");
		echo $hh->input_hidden("id_from",$id_from);
		$user_from = $pe->UserGetDetailsById($id_from);
		echo $hh->input_text("from","from_name",$user_from['name1'] . " " . $user_from['name2'],"30",0,0);
	}
	else 
		echo $hh->input_hidden("from","from");
	echo $hh->input_text("name","name","","30",0,$input_right);
	echo $hh->input_text("email","email","","30",0,$input_right);
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
	
}

	
include_once(SERVER_ROOT."/include/footer.php");
?>
