<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/topics.php");

$id_topic = (int)$_GET['id_topic'];

$t = new Topic($id_topic);

$tt = new Topics();
$group = $tt->gh->GroupGet($t->id_group);

$months = $hh->tr->Translate("month");

$title[] = array("stats",'stats_topics.php');
$title[] = array($group['name'],'stats_topics.php?id_g=' . $t->id_group);
$title[] = array($t->name,'');

echo $hh->ShowTitle($title);

if ($module_right)
{
	$tk = new Tracker();
	
	if($current_page==1)
	{
		$smonth = $tk->StatsTopic($id_topic);
		$tot_visits = $smonth['visits'];
		$tot_pages = $smonth['pages'];
		echo "<ul>";
		echo "<li>" . $hh->tr->Translate("pages") . ": <b>" . $tot_pages . "</b></li>";
		echo "<li>" . $hh->tr->Translate("visits") . ": <b>" . $tot_visits . "</b></li>";
		echo "<li>" . $hh->tr->Translate("pages") . "/" . $hh->tr->Translate("visit") . ": <b>" . number_format($tot_pages/$tot_visits,2) . "</b></li>";
		echo "</ul>";	
	}
	
	$row = array();
	$num2 = $tk->StatsPagesTopic( $row, $id_topic );
	
	$table_headers = array('page','hits');
	$table_content = array('{LinkTitle($row[url],$row[title])}','<div class=\"right\">$row[pages]</div>');
	
	echo "<h3>" . $hh->tr->Translate("pages") . "</h3>";
	echo $hh->ShowTable($row, $table_headers, $table_content, $num2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
