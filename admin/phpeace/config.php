<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if ($module_admin)
	$input_right=1;

$title[] = array('configuration','');
echo $hh->ShowTitle($title);

$trm17 = new Translator($hh->tr->id_language,17);

echo $hh->input_form_open();
echo $hh->input_hidden("from","config_update");
echo $hh->input_table_open();

echo $hh->input_date($trm17->Translate("install_date"),"install_date",$hh->ini->Get("install_date_ts"),$input_right);

echo $hh->input_separator("Mail");
echo $hh->input_checkbox("debug","debug_mail",$hh->ini->Get("debug_mail"),0,$input_right);
echo $hh->input_text("debug output","mail_output",$hh->ini->Get("mail_output"),30,0,$input_right);

echo $hh->input_separator("PhPeaceNet");
$is_main_server = $hh->ini->Get("main");
echo $hh->input_checkbox($trm17->Translate("main_server"),"main",$is_main_server,0,0);
if(!$is_main_server)
	echo $hh->input_text($trm17->Translate("main_server_url"),"phpeace_server",$hh->ini->Get("phpeace_server"),30,0,$input_right);

echo $hh->input_note($trm17->TranslateParams("phpeace_update_check",array("/admin/schedules.php")));

echo $hh->input_separator("info");
echo $hh->input_text("db version","db_version",$hh->ini->Get("db_version"),30,0,0);
echo $hh->input_text("build","build",$phpeace->build,30,0,0);

echo $hh->input_submit("submit","",$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

