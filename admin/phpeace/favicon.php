<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Favicon','');

if ($module_right)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>

<p>Must be in favicon.ico format, width 32x32 or 16x16 px</p>

<form method="post" action="actions.php" name="form1" enctype="multipart/form-data">
<input type="hidden" name="from" value="favicon">
<input type="hidden" name="action2" value="update">
<table border=0 cellpadding=0 cellspacing=7>

<?php
include_once(SERVER_ROOT."/../classes/file.php");
$fm = new FileManager();
if ($fm->Exists("uploads/custom/favicon.ico"))
	echo "<tr><td>Current favicon</td><td><img src=\"/images/upload.php?src=custom/favicon.ico\" /></td>\n";
echo $hh->input_upload("substitute_with","img",50,$input_right);
echo $hh->input_submit("submit","",$input_right);
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
