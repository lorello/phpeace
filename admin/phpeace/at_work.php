<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if ($module_admin)
	$input_right=1;

$trm17 = new Translator($hh->tr->id_language,17);
	
$title[] = array($trm17->Translate("maintenance"),'');
echo $hh->ShowTitle($title);

echo $trm17->Translate("block_users_warning");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;
echo "<ul>";
foreach($uu->AdminUsers(17) as $admin)
	echo "<li>{$admin['name']}</li>";
echo "</ul>";
?>
<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="at_work">
<table cellpadding=2 cellspacing=3 border=0>
<?php
echo $hh->input_checkbox($trm17->Translate("block_users"),"at_work",$hh->ini->Get("at_work"),0,$input_right);
echo $hh->input_submit("submit","",$input_right);
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

