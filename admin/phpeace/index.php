<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$phpeace = new Phpeace;

echo $hh->ShowTitle($title);

$trm17 = new Translator($hh->tr->id_language,17);
?>

<div class="box2c">

<?php
if ($module_right)
{
?>
<h2>PhPeace Custom Layout</h2>
<ul>
<li><a href="css.php">CSS</a></li>
<li><a href="logo.php">Logo</a></li>
<li><a href="favicon.php">favicon.ico</a></li>
<li><a href="js.php">JavaScript</a></li>
</ul>
<?php
}
if ($module_admin)
{
?>
<h2><?=$trm17->Translate("maintenance");?></h2>
<ul>
<li><a href="config.php"><?=$hh->tr->Translate("configuration");?></a></li>
<?php 
if  (!$phpeace->main && !$dev_right)
	echo "<li><a href=\"register.php\">" . $trm17->Translate("registration") . "</a></li>\n";
?>
<li><a href="at_work.php"><?=$trm17->Translate("block_users");?></a><br>(<?=$trm17->Translate("block_users_but");?>)</li>
</ul>

<h2>Updates</h2>
<ul>
<?php
	if ($phpeace->phpeace_server!="" && !$phpeace->main && !$dev_right)
	{
		echo "<li><a href=\"version_check.php\">From main server</a> ($phpeace->phpeace_server)</li>\n";
		echo "<li><a href=\"version_force.php\">Force update from main server</a></li>\n";
	}
	if ($phpeace->main)
	{
		echo "<li><a href=\"version_upload.php\">Manual</a></li>\n";
	}
	if ($dev_right)
	{
		echo "<li><a href=\"update.php\">Run local update script for current version</a></li>\n";
	}
	include_once(SERVER_ROOT."/../classes/phpeace_updates.php");
	$pu = new PhPeaceUpdates(0);
	if($pu->db_update_required)
	{
		echo "<li><a href=\"db_update.php\">DB update is required, please click here</a></li>\n";
	}
	echo "</ul>";

	if ($phpeace->main)
	{
		echo "<h2>PhPeace Server</h2>\n";
		echo "<ul>";
		echo "<li><a href=\"clients.php\">Clients</a></li>\n";
		echo "</ul>";
	}
}

echo "</div>\n";

if  ($module_admin)
{
	echo "<div class=\"box2c\">\n";
	
	echo "<h2>Info & monitoring</h2>";
	echo "<ul>";
	echo $hh->ListItem("Version",$phpeace->version);
	echo $hh->ListItem("Build",$phpeace->build);
	echo $hh->ListItem("Script_version",$hh->ini->Get("script_version"),true,$phpeace->build);
	$db =& Db::globaldb();
	echo $hh->ListItem("DB_version",$db->Version(),true,PHPEACE_DB_VERSION);
	echo $hh->ListItem("Key",$ini->Get("install_key"));
	echo $hh->ListItem("PHP","<a href=\"php.php\">" . phpversion() . "</a>");
	echo $hh->ListItem("MySQL","<a href=\"mysql.php\">" . $db->ServerInfo() . "</a>");
	echo $hh->ListItem("Apache",apache_get_version());
		
	$modules = Modules::AvailableModules();
	$emodules = array();
	$t_modules = $hh->tr->Translate("modules_names");
	foreach($modules as $module)
	{
		if($module['internal']!="1")
		{
			$emodules[] = $t_modules[$module['id_module']];
		}
	}
	asort($emodules);
	echo $hh->ListItem($hh->tr->Translate("modules"),$emodules);
	echo "<li><a href=\"tests.php\">Tests</a></li>";
	echo "<li><a href=\"/gate/authors.php\">Authors & Credits</a></li>";
	echo "</ul>";

	echo "</div>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
