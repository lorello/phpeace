<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Tests','');
echo $hh->ShowTitle($title);

echo "<h3>Tests</h3>";
echo "<ul>";
include_once(SERVER_ROOT."/../classes/installer.php");
$in = new Installer(1,$hh->tr->id_language);
$test_cases = $in->test_cases;

if ($conf->Get("user_auth")=="ldap")
	$test_cases[] = "ldap";
unset($test_cases[array_search('is_installed',$test_cases)]);
foreach($test_cases as $case)
{
	echo "<li>" . $in->trm->Translate("test_".$case) . "... ";
	if ($in->Test($case))
	{
		echo $in->trm->Translate("test_ok");
		if ($in->test_msg!="")
			echo "<div>" . $in->test_msg . "</div>";
	}
	else
	{
		$testall = false;
		$critical = $in->TestCritical($case);
		if ($critical)
			$next_block = true;
		echo "<span class=\"error\">" . $in->trm->Translate("test_ko") . "</span>";
		foreach($in->errors as $error)
			echo "<div>" . (($critical)? "<b>" . $in->trm->Translate("error_critical") . "</b> ":"" ) . "$error</div>";
	}
	echo "</li>\n";
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>
