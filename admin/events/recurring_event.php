<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/event.php");

$id = $_GET['id'];

if ($id>0)
{
	$action2 = "update";
	$event = new Event();
	$title[] = array('anniversary','');
	$row = $event->RecurringGet($id);
	$approved = $row['approved'];
}
else
{
	$input_right = 1;
	$action2 = "insert";
	$title[] = array('anniversary_add','');
	$approved = 0;
}

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function dosubmit(action)
{
	f=document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.description.value=="")
	{
		strAlert=strAlert+"<?=$hh->tr->Translate("missing_description");?>\n";
		boolOK = false;
	}
	if (boolOK==true)
	{
		f.action2.value = action;
		f.submit();
	}
	else
		alert(strAlert);
}

</script>

<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="recurring">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="id_event" value="<?=$id;?>">
<table border=0 cellpadding=2 cellspacing=2>

<?php
echo $hh->input_day("day","r_day",$row['r_day'],$input_right);
echo $hh->input_month("month_item","r_month",$row['r_month'],$input_right);
echo $hh->input_text("anniversary_year","r_year",$row['r_year'],5,0,$input_right);
echo $hh->input_note("anniversary_note");
echo $hh->input_textarea("description","description",$row['description'],80,10,"",$input_right);
echo $hh->input_text("link_more_info","url",$row['url'],50,0,$input_right);
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['r_event'],$keywords,$input_right);
echo $hh->input_checkbox("approved","approved",$approved,0,$input_super_right);

if ($input_right==1)
{
	echo "<tr><td>&nbsp;</td><td>";
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") ."\" onClick=\"dosubmit('$action2')\">";
	if ($id>0)
		echo "&nbsp;<input type=\"button\" value=\"" . $hh->tr->Translate("delete") ."\" onClick=\"dosubmit('delete')\">";
	echo "</td></tr>\n";
}
?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


