<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/events.php");

$ee = new Events();
echo $hh->ShowTitle($title);

$events_web = $hh->ini->Get("pub_web") . "/" . $hh->ini->Get("events_path");
echo "<p>" . $hh->tr->Translate("published_info") . " <a href=\"$events_web\" target=\"_blank\">$events_web</a>
 - <a href=\"../topics/preview.php?id_type=5&subtype=next\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a></p>\n";

echo "<h3>" . $hh->tr->Translate("calendar") . "</h3>\n";
$num1 = $ee->EventsApproved( $row, 0 );
echo "<ul><li><a href=\"events.php?approved=0\">" . $hh->tr->Translate("events_to_approve") . "</a> ($num1)</li>\n";
$num2 = $ee->EventsApproved( $row, 1 );
echo "<li><a href=\"events.php?approved=1\">" . $hh->tr->Translate("events_approved") . "</a> ($num2)</li>\n";
echo "<li><a href=\"event.php?id=0\">" . $hh->tr->Translate("event_add") . "</a></li>\n";
echo "<li><a href=\"search.php\">" . $hh->tr->Translate("search") . "</a></li>\n";

if ($module_admin)
  echo "<li><a href=\"event_types.php\">" . $hh->tr->Translate("event_types") . "</a></li\n";
echo "</ul>\n";

if($conf->Get("fb_scraper")!='') {
	echo "<h3>Facebook event import</h3>\n";
	echo $hh->input_form("post","actions.php",true);
	echo $hh->input_hidden("from","facebook_import");
	echo $hh->input_table_open();
	echo $hh->input_text("link","link",'',50,0,1);
	$actions = array();
	$actions[] = array('action'=>"import",'label'=>"submit",'right'=>1);
	echo $hh->input_actions($actions,1);
	echo $hh->input_table_close() . $hh->input_form_close();
}

/*
echo "<h3>iCALendar (ics) import</h3>\n";
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("from","event_import");
echo $hh->input_table_open();
echo $hh->input_upload(".ics file","cal",50,1);
$actions = array();
$actions[] = array('action'=>"import",'label'=>"submit",'right'=>1);
echo $hh->input_actions($actions,1);
echo $hh->input_table_close() . $hh->input_form_close();
*/
echo "<h3>" . $hh->tr->Translate("anniversaries") . "</h3>\n";
$num1 = $ee->Recurring( $row, 0 );
echo "<ul><li><a href=\"recurring_events.php?approved=0\">" . $hh->tr->Translate("anniversaries_to_approve") . "</a> ($num1)</li>\n";
$num2 = $ee->Recurring( $row, 1 );
echo "<li><a href=\"recurring_events.php?approved=1\">" . $hh->tr->Translate("anniversaries_approved") . "</a> ($num2)</li>\n";
echo "<li><a href=\"recurring_months.php\">" . $hh->tr->Translate("anniversaries_per_months") . "</a></li>\n";
echo "<li><a href=\"recurring_event.php?id=0\">" . $hh->tr->Translate("anniversary_add") . "</a></li>\n";
echo "</ul>\n";

if ($module_admin)
	echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
