<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/event.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();
$get = $fh->HttpGet();
$conf = new Configuration();
$calendar_version = $conf->Get("calendar_version");

if(count($post)==0 && count($get)==0)
	header("Location: /gate/index.php");

$action2	= $post['action2'];
$from		= $post['from'];
$action3	= $get['action3'];
$from2		= $get['from2'];

if ($from=="event")
{
	$module		= $post['module'];
	$id_event 	= $post['id_event'];
	$id_topic 	= $fh->Null2Zero($post['id_topic']);
	$start_date 	= $fh->Strings2Datetime($post['start_date_d'],$post['start_date_m'],$post['start_date_y'],$post['start_date_h'],$post['start_date_i']);
	$length		= $post['length'];
	$allday		= $fh->Checkbox2bool($post['allday']);
	$id_event_type	= $post['id_event_type'];
	$title		= $fh->StringClean($post['title']);
	$description	= $post['description'];
	$place		= $post['place'];
	$address		= $post['address'];
	$id_geo		= $post['id_geo'];
	$place_details	= $post['place_details'];
	$contact_name	= $post['contact_name'];
	$email		= $post['email'];
	$phone		= $post['phone'];
	$link		= $fh->String2Url($post['link']);
	$id_article	= $fh->Null2Zero($post['id_article']);
	$visible_topic_group	= $fh->Checkbox2bool($post['visible_topic_group']);
	$id_group 	= 0;
	if($calendar_version==1) {
		if($id_topic>0 && $visible_topic_group)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$id_group = $t->id_group;
		}
		$portal 	= $fh->Checkbox2bool($post['portal']);
		$jump_to_article 	= $fh->Checkbox2bool($post['jump_to_article']);
	} else {
		$portal = 1;
		$jump_to_article = 0;
	}
	$keywords 	= $post['keywords'];
	$approved 	= $fh->Checkbox2bool($post['approved']);
	$approved_old	= $fh->Null2Zero($post['approved_old']);
	if($module=="topics")
		$url = "/topics/events.php?id_topic=$id_topic&approved=$approved";
	else
		$url = "/events/events.php?approved=$approved";
	if ($action2=="approve")
		$approved = 1;
	$event = new Event();
	$file		= $fh->UploadedFile("img",true);
	if ($action2=="update" || $action2=="approve" || $action2=="geocode") {
		$event->EventUpdate($id_event, $start_date, $length, $allday, $id_event_type, $title, $description, $place, $address, $id_geo,
				$place_details, $contact_name, $email, $phone, $link, $id_article, $id_topic, $portal, $approved, $keywords, $approved_old,$jump_to_article,$id_group);
		if($file['ok']) {
			$event->ImageUpdate($id_event,$file);
		}
		if ($action2=="geocode") {
			$event->Geocode($id_event);
			$url = "/events/event.php?id=$id_event";
		}
	}
	if ($action2=="insert")
		$event->EventInsert($start_date, $length, $allday, $id_event_type, $title, $description, $place, $address, $id_geo,
			$place_details, $contact_name, $email, $phone, $link, $id_article, $id_topic, $portal, $approved, $keywords,$jump_to_article,0,$id_group);
	if ($action2=="delete")
		$event->EventDelete($id_event, $id_topic);
	if ($approved==1)
	{
		include_once(SERVER_ROOT."/../classes/events.php");
		$ee = new Events();
		if($module=="topics")
			$ee->id_topic = $id_topic;
		$row = array();
		$pending_events = $ee->EventsApproved( $row, 0 );
		if ($pending_events>0)
			$approved = 0;
	}
	header("Location: $url");
}

if ($from=="event_import") {
	$file		= $fh->UploadedFile("cal");
	$url = "index.php";
	if($file['ok']) {
		$event = new Event();
		$event->IcsImport($file);
	}
	header("Location: $url");
}
	
if ($from=="facebook_import") {
	$link		= $fh->String2Url($post['link'],true);
	$url = "index.php";
	$match = array();
	$e = new Event();
	preg_match('@(?:https?:\/\/)?(?:www\.)?facebook\.com\/events\/?([^/?]*)@i',$link,$match);
	if(isset($match[1])) {
		$event = $e->EventGetByFacebookId($match[1]);
		if(isset($event['id_event'])) {
			$ah->MessageSet("Facebook event already exists: <a href=\"/events/event.php?id={$event['id_event']}\">{$event['title']}</a>");
		} else {
			$id_event = $e->FacebookImport($link);
			if($id_event>0) {
				$url = "/events/event.php?id=$id_event";
				$ah->MessageSet("event_review");
			} elseif(count($e->errors)>0) {
				foreach($e->errors as $error) {
					$ah->MessageSet($error);
				}
			}
		}
	} else {
		$ah->MessageSet('Wrong link');
	}
	header("Location: $url");
}

if ($from2=="event_image")
{
	$id_event		= $get['id_event'];
	if ($action3=="delete")
	{
		$event = new Event();
		$event->ImageDelete($id_event);
		$url = "/events/event.php?id=$id_event";
		header("Location: $url");
	}
}

if ($from=="recurring")
{
	$id_event	 	= $post['id_event'];
	$r_day 			= $post['r_day'];
	$r_month 		= $post['r_month'];
	$r_year 		= $fh->String2Number($post['r_year']);
	$description	= $post['description'];
	$url			= $fh->String2Url($post['url']);
	$approved 		= $fh->Checkbox2bool($post['approved']);
	$keywords 		= $post['keywords'];
	if ($r_month>0 && $r_day>0)
	{
		$event = new Event();
		if ($action2=="update")
			$event->RecurringUpdate($id_event, $r_day, $r_month, $r_year, $description, $url, $approved, $keywords);
		if ($action2=="insert")
			$id_event = $event->RecurringInsert($r_day, $r_month, $r_year, $description, $url, $approved, $keywords);
		if ($action2=="delete")
			$event->RecurringDelete($id_event);
		header("Location: recurring_events.php?approved=$approved");
	}
	else
		header("Location: index.php");

}

if ($from=="type")
{
	$id_event_type 	= $post['id_event_type'];
	$type 			= $post['type'];
	$event = new Event();
	if ($action2=="update")
		$event->TypeUpdate($id_event_type, $type);
	if ($action2=="insert")
		$event->TypeInsert($type);
	header("Location: event_types.php");
}

if ($from=="config_update")
{
	$path 		= $post['path'];
	$title	 	= $post['title'];
	$events_insert	= $fh->Checkbox2bool($post['events_insert']);
	$search_events	= $fh->Checkbox2bool($post['search_events']);
	include_once(SERVER_ROOT."/../classes/events.php");
	$ee = new Events();
	$ee->ConfigurationUpdate($path, $title,$events_insert,$search_events);
	header("Location: index.php");
}

?>
