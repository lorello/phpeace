<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/event.php");


$id = $_GET['id'];

$title[] = array('event_types','event_types.php');

if ($id>0)
{
	$action2 = "update";
	$event = new Event();
	$title[] = array('change','');
	$row = $event->TypeGet($id);
}
else
{
	$action2 = "insert";
	$title[] = array('new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function dosubmit() {
	f=document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.type.value=="") {
		strAlert=strAlert+"Manca il tipo\n";
		boolOK = false;
	}
	if (boolOK==true) {
		f.submit();
	}
	else {
		alert(strAlert);
	}
}

</script>

<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="type">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="id_event_type" value="<?=$id;?>">
<table border=0 cellpadding=2 cellspacing=2>

<?php
echo $hh->input_text("type","type",$row['type'],40,0,$input_right);

if ($input_right==1)
	echo "<tr><td>&nbsp;</td><td><input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"dosubmit()\"></td></tr>\n";
?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


