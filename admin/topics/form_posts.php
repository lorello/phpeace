<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");

$id_topic = $get['id_topic'];
$id_form = $get['id'];

if ($module_admin)
	$input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('forms','forms.php?id='.$id_topic);
$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('posts','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
	$posts = array();
	$num_posts = $fo->Posts($posts,$id_form,$id_topic);
	$tabs[] = array("Posts ($num_posts)","");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
if($form['weights'])
	$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);
	
if($input_right)
{
	$num = $fo->Posts($row,$id_form,$id_topic);
	$table_headers = array('date','name');
    $table_content = array('{FormatDateTimeMilliseconds($row[post_time_ts],$row[miliSec])}','{LinkTitle("form_post.php?id=$row[id_post]&id_topic=' . $id_topic . '&id_form='.$id_form.'","$row[name]")}');
    
    if(!$id_topic>0)
	{
		$table_headers[] = "topic";
		$table_content[] = '$row[topic_name]';
	}
	if($form['weights'])
	{
		$table_headers[] = "score";
		$table_content[] = '<div class=right>$row[score]</div>';
	}
	echo $hh->showTable($row, $table_headers, $table_content, $num);
	
	echo "<p><a href=\"form_posts_csv.php?id=$id_form&id_topic=$id_topic\">CSV</a></p>";
}
                                                                                                                       
include_once(SERVER_ROOT."/include/footer.php");
?>

