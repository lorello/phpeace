<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/topics.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
$hh = new HtmlHelper();
$page_title = $hh->tr->Translate("topic_group_location");
include_once(SERVER_ROOT."/../admin/include/head.php");

$id_group = $_GET['id_group'];
$id_parent = $_GET['id_parent'];
?>
<body>
<script type="text/javascript">
function set_group(id_group,group)
{
	opener.document.forms['form1'].id_parent.value = id_group;
	opener.document.forms['form1'].descriz_id_parent.value = group;
	self.close();
}
</script>
<div class="popup-tree">
<?php
$hh->groups = new Topics;
$hh->groups->gh->LoadTree();

if (isset($hh->groups->gh->th->tree))
{
	echo "<p>" . $hh->tr->Translate("group_groups_choose") . "</p>\n";
	echo $hh->GroupsParentsTree($id_group,$id_parent);
}
else
	echo "<p>" . $hh->tr->Translate("missing_groups") . "</p>\n<p><input type=button onClick=\"self.close()\" value=\"" . $hh->tr->Translate("close") . "\"></p>\n";
?>
</div>
</body>
</html>

