<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/events.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$ee = new Events();

$approved = $_GET['approved'];
if (!isset($approved))
	$approved = 1;
$id_topic = (int)$_GET['id_topic'];

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$ee->id_topic = $id_topic;
	$ee->id_group = $t->id_group;
}

$title[] = array((($approved==1)? "events_approved" : "events_to_approve"),'');
echo $hh->ShowTitle($title);

$num = $ee->EventsApproved( $row, $approved );
$table_headers = array('when','type','title','placing');
$table_content = array('{FormatDateTime($row[start_date_ts])','$row[type]','{LinkTitle("event.php?id=$row[id_event]&id_topic='.$id_topic.'",$row[title])}','$row[place]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_right)
	echo "<p><a href=\"event.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("event_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

