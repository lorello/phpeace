<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/texthelper.php");

$id_content = $_GET['id'];
$id_topic = $_GET['id_topic'];
$id_type = $_GET['id_type'];
$old_id_type = $id_type;

if ($module_admin)
	$input_right = 1;

$wysiwyg_right = 0;

$t = new Topic($id_topic);
if ($id_topic>0)
{
	$wysiwyg_right = $t->wysiwyg;
	if ($t->AmIAdmin())
	{
		$input_right = 1;
		$input_super_right = 1;
	}
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}
else 
{
	$wysiwyg_right = 1;
}
$title[] = array('texts','texts.php?id='.$id_topic);

$th = new TextHelper();
if ($id_content>0)
{
	$action2 = "update";
	$row = $th->ContentGet($id_content);
	$title[] = array($row['description'],'');
	$id_type = $row['id_type'];
	$description = $row['description'];
	$old_id_type = $row['id_type'];
	$wysiwyg = $row['wysiwyg'];
}
elseif($id_type>0 && $id_topic>=0) // Predefined types
{
	$action2 = "store";
	$row = $t->TextGetRow($id_type);
	$id_content = $row['id_content'];
	if ($id_content>0) // update
	{
		$title[] = array( $row['description'],'');
		$description = $row['description'];
		$old_id_type = $row['id_type'];
		$wysiwyg = $row['wysiwyg'];
	}
	else // insert
	{
		$texts_types = $hh->tr->Translate("texts_types");
		$title[] = array( $texts_types[$id_type],'');
		$description = $texts_types[$id_type];
		$wysiwyg = $wysiwyg_right;
	}
}
else
{
	$action2 = "insert";
	$title[] = array('text_add','');
	$id_type = 0;
	$old_id_type = 0;
	$wysiwyg = $wysiwyg_right;
}

echo $hh->ShowTitle($title);
?>

<form action="actions.php" method="post">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="from" value="content">
<input type="hidden" name="id_content" value="<?=$id_content;?>">
<input type="hidden" name="id_topic" value="<?=$id_topic;?>">
<input type="hidden" name="old_id_type" value="<?=$old_id_type;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php

echo $hh->input_text("description","description",$description,50,0,$input_right);
echo $hh->input_array("type","id_type",$id_type,$hh->tr->Translate("texts_types"),0);
echo $hh->input_wysiwyg("content","content",$row['content'],$row['is_html'],20,$input_right,$wysiwyg_right && $wysiwyg);
echo $hh->input_checkbox("Editor HTML WYSIWYG","wysiwyg",$wysiwyg,0,$input_right && $wysiwyg_right);
if(!$id_type>0)
{
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	$keywords = array();
	echo $hh->input_keywords($id_content,$o->types['text'],$keywords,$input_right);
	
	if($id_topic>0)
	{
		$tikeywords = $t->KeywordsInternal($o->types['text']);
		echo $hh->input_internal_keywords($id_content,$tikeywords,"text",$input_super_right,$input_right);
	}
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id_content>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
