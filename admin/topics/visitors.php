<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$id_topic = $get['id'];

$t = new Topic($id_topic);
$row = $t->row;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('visitors','');

echo $hh->ShowTitle($title);

if($t->profiling)
{
	$row = array();
	if($get['from']=="search")
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$params = array(	'name' => $get['name'],
				'email' => $get['email'],
				'id_geo' => $get['id_geo'],
				'id_pt_group' => $get['id_pt_group'],
				'contact' => (int)$get['contact'],
				'access' => (int)$get['access'],
                'id_topic' => $id_topic,
                'sort_by' => $get['sort_by']
				);
		$num = $pe->Search( $row, $params);
	}
	else
		$num = $t->People($row);

	if($t->row['protected']=="2")
	{
		$table_headers = array('name','email','contact_email','verified',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date','access','group');
		$table_content = array('{LinkTitle("visitor.php?id=' . $id_topic . '&id_p=$row[id_p]&p='.$current_page.'","$row[full_name]")}',
		'$row[email]','{Bool2YN($row[contact])}','{Bool2YN($row[verified])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}','{Bool2YN($row[access])}');
	}
	else 
	{
		$table_headers = array('name','email','contact_email','verified',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date');
		$table_content = array('{LinkTitle("visitor.php?id=' . $id_topic . '&id_p=$row[id_p]&p='.$current_page.'","$row[full_name]")}',
		'$row[email]','{Bool2YN($row[contact])}','{Bool2YN($row[verified])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');
	}
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);

	$input_right = $module_right;	
	echo $hh->input_form("get","visitors.php");
	echo $hh->input_table_open();
	echo $hh->input_hidden("from","search");
	echo $hh->input_hidden("id",$id_topic);
	echo $hh->input_text("name","name",$get['name'],"30",0,$input_right);
	echo $hh->input_text("email","email",$get['email'],"30",0,$input_right);
	echo $hh->input_geo($_GET['id_geo'],$input_right);
	$tgroups = array();
	$num_groups = $t->PeopleGroups($tgroups,false,false);
	if($num_groups>0)
		echo $hh->input_row("group","id_pt_group",$get['id_pt_group'],$tgroups,"all_option",0,$input_right);
	echo $hh->input_array("contact_email","contact",$get['contact'],$hh->tr->Translate("any_yes_no"),1); 
	if($t->row['protected']=="2")
		echo $hh->input_array("access","access",$get['access'],$hh->tr->Translate("any_yes_no"),1); 
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>

