<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = (int)$_GET['id'];

if($module_admin || Modules::AmIAdmin(28))
	$input_right = 1;

$t = new Topic($id_topic);
$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('visitors','visitors.php?id='.$id_topic);
$title[] = array('Visitors import','');

echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","people_import");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
	
echo $hh->input_checkbox("contact_email","contact",0,0,$input_right);
$tgroups = array();
$num_groups = $t->PeopleGroups($tgroups);
if($num_groups>0)
	echo $hh->input_row("group","id_pt_group",0,$tgroups,"none_option",0,$input_right);

echo $hh->input_geo(0,$input_right);

echo $hh->input_note("Use | as separator, in this order: email|name1|name2");
echo $hh->input_textarea("data","data","",80,30,"",$input_right);
	
echo $hh->input_submit("submit","",$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer_cp.php");
?>
