<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);

include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");
$ini = new Ini();

$id_topic = $_GET['id_topic'];
$id_campaign = $_GET['id'];

if ($ah->ModuleAdmin(4))
	$input_right = 1;

$c = new Campaign($id_campaign);
$campaign = $c->CampaignGet();

$t = new Topic($id_topic);
if ($id_topic>0 && $id_topic==$campaign['id_topic'] && $t->AmIAdmin())
	$input_right = 1;

$rows = array();
$num = $c->OrgsApprove( $rows, 1, false );

if($input_right && $num>0)
{
	$headers = array("TIMESTAMP","DATE","NAME","EMAIL","LOCATION","CONTACT","ADMIN_LINK");
	$link = $ini->get("admin_web") . "/topics/campaign_org.php?id_c=$id_campaign&id_topic=$id_topic&id=";
	$data = array();
	foreach($rows as $row)
	{
		$data_row = array();
		$data_row[] = $row['insert_date_ts'];
		$data_row[] = date("r",$row['insert_date_ts']);
		$data_row[] = $row['name'];
		$data_row[] = $row['email'];
		$data_row[] = $row['geo_name'];
		$data_row[] = $row['contact'];
		$data_row[] = $link . $row['id_org'];
		$data[] = $data_row;
	}

    include_once(SERVER_ROOT."/../classes/modules.php");
	include_once(SERVER_ROOT."/../classes/texthelper.php");
	$th = new TextHelper();
	echo $th->CSVDump("campaign_{$id_campaign}_orgs",$headers,$data);
}
?>

