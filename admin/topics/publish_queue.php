<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('publishing_queue','');

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

$num = $t->queue->JobsAllP( $row );

$table_headers = array('type','id','action','parameters','user');
$table_content = array('{QueueType($row[id_type])}','$row[id_item]','$row[action]','$row[param]','{UserLookup($row[id_user])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

if($input_right)
{
	$all_present = $t->queue->JobPresentAll();
	if ($num>0)
	{
		include_once(SERVER_ROOT."/../classes/publishmanager.php");
		$pm = new PublishManager();
		$articles = $t->Articles();
		$steps = $pm->StepsCounter(count($articles));
		if($steps>1 && $all_present)
		{
			echo "<p>" . $hh->tr->TranslateParams("publish_heavy_warning",array(count($articles))) . "</p>";
			echo "<p>" . $hh->tr->Translate("publish_queued") . "</a></p>\n";
			echo "<p>" . $hh->tr->TranslateParams("publish_force",array("publish.php?id=$id_topic&type=all&ok=1",$steps)) . "</p>";
		}
		else
			echo "<p><a href=\"publish.php?id=$id_topic\">" . $hh->tr->Translate("publish") . "</a></p>\n";

	}

	if (!$all_present && $input_super_right==1)
		echo "<p><a href=\"publish.php?id=$id_topic&type=all\">" . $hh->tr->Translate("publish_all") . "</a></p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
