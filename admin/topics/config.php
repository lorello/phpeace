<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$trm4 = new Translator($hh->tr->id_language,4);

if ($module_admin)
	$input_right=1;

$title[] = array('configuration','');
echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			campaign_path: "required",
			poll_path: "required",
			forum_path: "required",
			map_path: "required",
			search_path: "required",
			temp_id_topic: {
				required: true,
				min: 1
			}
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","config_update");
echo $hh->input_table_open();

echo $hh->input_array("language","id_language",$hh->ini->Get("id_language"),$phpeace->Languages($hh->tr->id_language),$input_right);

include_once(SERVER_ROOT."/../classes/topics.php");
$topics = new Topics;
echo $hh->input_row($trm4->Translate("topic_bin"),"temp_id_topic",$hh->ini->Get("temp_id_topic"),$topics->AllTopics(true),"",0,$input_right);
echo $hh->input_text($trm4->Translate("max_latest"),"latest",$hh->ini->Get("latest"),2,0,$input_right);
echo $hh->input_checkbox($trm4->Translate("show_licences"),"licences",$hh->ini->Get("licences"),0,$input_right);
echo $hh->input_array($trm4->Translate("licence_default"),"id_licence",$hh->ini->Get("id_licence"),$hh->tr->Translate("licences"),$input_right);

include_once(SERVER_ROOT."/../classes/mail.php");
$mail = new Mail();
$combo_values[0] = $hh->tr->Translate("every_day");
$wk = $hh->tr->Translate("weekdays");
for($i=0;$i<7;$i++)
	$combo_values[] = $wk[$i];
$combo_values[8] = $hh->tr->Translate("row_none");
echo $hh->input_array($trm4->Translate("gnewsletter_day"),"wday_newsletter",$mail->wday_newsletter,$combo_values,$input_right);
echo $hh->input_text($trm4->Translate("deposit"),"topic_store",$hh->ini->Get("topic_store"),30,0,$input_right);

$max_vis = $conf->Get("max_vis");
for($i=$max_vis;$i>0;$i--)
	$combo_values2["" . $i . ""] = $i;
echo $hh->input_array($trm4->Translate("visibility_default"),"default_visibility",$hh->ini->Get("default_visibility"),$combo_values2,$input_right);
echo $hh->input_array($trm4->Translate("map_sort_by"),"map_sort_by",$hh->ini->Get("map_sort_by"),$hh->tr->Translate("sort_by_options"),$input_right);

echo $hh->input_separator("path");
echo $hh->input_text("map","map_path",$hh->ini->Get("map_path"),20,0,$input_right);
echo $hh->input_text("search_engine","search_path",$hh->ini->Get("search_path"),20,0,$input_right);
echo $hh->input_text("campaigns","campaign_path",$hh->ini->Get("campaign_path"),20,0,$input_right);
echo $hh->input_text("forum","forum_path",$hh->ini->Get("forum_path"),20,0,$input_right);
echo $hh->input_text("polls","poll_path",$hh->ini->Get("poll_path"),20,0,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
