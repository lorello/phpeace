<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forum.php");

$id_forum = $_GET['id'];
$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','forums.php?id='.$id_topic);

if ($id_forum>0)
{
	$f = new Forum($id_forum);
	$row = $f->ForumGet();
	$title[] = array($row['name'],'');
	$status = $row['active'];
}
else
{
	$title[] = array('add_new','');
	$status = 1;
}

echo $hh->ShowTitle($title);


if ($id_forum>0)
{
	$url = $ini->Get("pub_web") . "/" . $ini->Get("forum_path") . "/index.php?id=$id_forum&id_topic=$id_topic";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";
	$rows = array();
	echo "<h3>" . $hh->tr->Translate("threads") . "</h3>\n";
	echo "<ul>";
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("thread",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_forum);
	if($co_pending>0)
		echo "<li><a href=\"comments_forum.php?id=$id_forum&approved=0\">" . $hh->tr->Translate("comments_to_approve") . " ($co_pending)</a></li>\n";
	echo "<li><a href=\"forum_thread.php?id_forum=$id_forum&id=0\">".$hh->tr->Translate("add_new")."</a></li>\n";
	$num1 = $f->ThreadsApprove( $rows, 1 );
	if($num1>0)
		echo "<li><a href=\"forum_threads.php?id=$id_forum&approved=1\">" . $hh->tr->Translate("approveds") . " ($num1)</a></li>\n";
	$num2 = $f->ThreadsApprove( $rows, 0 );
	if($num2>0)
		echo "<li><a href=\"forum_threads.php?id=$id_forum&approved=0\">" . $hh->tr->Translate("threads_to_approve") . " ($num2)</a></li>\n";
	if ($input_right)
		echo "<li><a href=\"forum_mail.php?act=filter&id_item=$id_forum\">" . $hh->tr->Translate("mailjob") . "</a></li>\n";
	echo "</ul>";
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","forum");
echo $hh->input_hidden("id_forum",$id_forum);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($id_topic,0,$tt->AllTopics(),"",$input_super_right);

echo $hh->input_date("date_start","start_date",$row['start_date_ts'],$input_right);
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,5,"",$input_right);
echo $hh->input_textarea("thanks_text","thanks",$row['thanks'],60,3,"",$input_right);

echo $hh->input_separator("threads");
echo $hh->input_array("open_to","users_type",$row['users_type'],$hh->tr->Translate("profiling_options"),$input_right);
echo $hh->input_checkbox("moderate_threads","approve_threads",$row['approve_threads'],0,$input_right);
echo $hh->input_checkbox("source_request","source",$row['source'],0,$input_right);

echo $hh->input_separator("comments");
echo $hh->input_array("allow_comments","comments",$row['comments'],$hh->tr->Translate("profiling_options"),$input_right);
echo $hh->input_checkbox("moderate_comments","approve_comments",$row['approve_comments'],0,$input_right);

echo $hh->input_separator("administration");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
echo $hh->input_keywords($id_forum,$o->types['forum'],$keywords,$input_right);
echo $hh->input_array("status","active",$status,$hh->tr->Translate("status_options"),$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_forum>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

