<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/mail.php");
include_once(SERVER_ROOT."/../classes/datetime.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);
$row = $t->row;

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('newsletter','');

echo $hh->ShowTitle($title);

$mail = new Mail();
$row = $mail->NewsletterGet($id_topic);
?>
<form action="actions.php" method="post" name="form1">
<div class="box">
<p>
<?php
if ($row['lastletter_ts']>0)
	echo $hh->tr->TranslateParams("newsletter_last",array($hh->formatDate($row['lastletter_ts'])));
else
	echo $hh->tr->Translate("newsletter_never");
	
$articles = $t->LatestArticles($row['lastletter_ts']);
	
echo "</p><p>" . $hh->tr->TranslateParams("newsletter_articles_new",array(count($articles))) . "</p>\n";
?>
<?php
if ($row['newsletter']>0)
{
	echo "<p>" .  $hh->tr->TranslateParams("newsletter_next",array($hh->FormatDate(max($row['lastletter_ts']+FrequencySeconds($row['id_frequency']),time()))))  . "</p>\n";
	if ($input_right==1)
		echo "<input type=\"submit\" class=\"input-submit\" name=\"action_send\" value=\"" . $hh->tr->Translate("newsletter_send_now") . "\">\n";
}
?>
</div><div class="box2b">
<h3><?=$hh->tr->Translate("newsletter_settings");?></h3>
<input type="hidden" name="from" value="newsletter">
<input type="hidden" name="id_topic" value="<?=$id_topic;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
echo $hh->input_checkbox("newsletter_send","newsletter",$row['newsletter'],0,$input_right);
echo $hh->input_array("format","newsletter_format",$row['newsletter_format'],$hh->tr->Translate("newsletter_format"),$input_right);
if($row['newsletter_format']=="1" && Modules::AmIAdmin(10))
	echo $hh->input_note("Ensure that <a href=\"/layout/xsl.php?id=12&id_style={$t->id_style}\">XSL</a> output is set to html");
echo $hh->input_array("frequency","id_frequency",$row['id_frequency'],$hh->tr->Translate("newsletter_frequency"),$input_right);
echo $hh->input_row("recipient","id_recipient",$row['id_recipient'],$t->Users(),"none_option",0,$input_right);
echo $hh->input_text("recipient_email","newsletter_rcpt",$row['newsletter_rcpt'],40,0,$input_right);
$newsletter_people = $t->profiling && $row['newsletter_people'];
if($t->profiling)
	echo $hh->input_checkbox("newsletter_people","newsletter_people",$newsletter_people,0,$input_right);
echo $hh->input_date("last_time","lastletter",$row['lastletter_ts'],$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
?>
</table>
</div>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

