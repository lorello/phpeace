<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

echo "<div class=\"box\">";
echo "<h2>" . $hh->tr->Translate("topics") . "</h2>\n";

if($conf->Get("users_mode")=="shared" || $module_admin)
{
	echo "<h3><a href=\"tree.php\">" . $hh->tr->Translate("topics_tree") . "</a></h3>";
	echo "<h3><a href=\"topics.php\">" . $hh->tr->Translate("topics_all") . "</a></h3>\n";
}

include_once(SERVER_ROOT."/../classes/user.php");
$u = new User();
$topics1 = $u->TopicsContactOnly();
if (count($topics1)>0)
{
	echo "<h3>" . $hh->tr->Translate("topics_contact") . "</h3>\n<ul>";
	foreach($topics1 as $topic1)
		echo "<li><a href=\"ops.php?id=$topic1[id_topic]\">$topic1[name]</a></li>\n";
	echo "</ul>\n";
}

$topics2 = $u->TopicsAdminOnly();
if (count($topics2)>0)
{
	echo "<h3>" . $hh->tr->Translate("topics_administrator") . "</h3>\n<ul>";
	foreach($topics2 as $topic2)
		echo "<li><a href=\"ops.php?id=$topic2[id_topic]\">$topic2[name]</a></li>\n";
	echo "</ul>\n";
}

$topics3 = $u->TopicsNoAdmin();
if (count($topics3)>0)
{
	echo "<h3>" . $hh->tr->Translate("topics_collaborator") . "</h3>\n<ul>";
	foreach($topics3 as $topic3)
		echo "<li><a href=\"ops.php?id=$topic3[id_topic]\">$topic3[name]</a></li>\n";
	echo "</ul>\n";
}
echo "</div>";

if ($module_admin)
{
	echo "<div class=\"box\">";
	echo "<h2>" . $hh->tr->Translate("contents") . "</h2>\n";
	echo "<p><a href=\"texts.php\">" . $hh->tr->Translate("texts") . "</a></p>";
	echo "<p><a href=\"text.php?id_topic=0&id_type=6\">" . $hh->tr->Translate("privacy_warning") . "</a></p>\n";
	echo "<p><a href=\"forms.php\">" . $hh->tr->Translate("forms") . "</a></p>\n";
	echo "<p><a href=\"templates.php\">" . $hh->tr->Translate("templates") . "</a></p>\n";
	echo "<p><a href=\"features.php\">" . $hh->tr->Translate("features") . "</a></p>\n";
	echo "<p><a href=\"features_global.php\">" . $hh->tr->Translate("features_global") . "</a></p>\n";
	echo "<p><a href=\"rss_groups.php\">RSS feeds</a></p>\n";
	echo "<p><a href=\"xml_fragments.php\">" . $hh->tr->Translate("xml_fragments") . "</a></p>\n";
	echo "</div>";
	echo "<div class=\"box\">\n";
	echo "<h2>" . $hh->tr->Translate("management") . "</h2>\n";
	echo "<p><a href=\"topic.php?id=0\">" . $hh->tr->Translate("topic_new") . "</a></p>\n";
	echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>\n";
	echo "</div>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
