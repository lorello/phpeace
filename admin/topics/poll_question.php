<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_poll = $_GET['id_poll'];
$id_question = $_GET['id'];
$id_questions_group = $_GET['id_questions_group'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
$title[] = array('poll_questions',"poll_questions.php?id=$id_poll&approved=-1");

if ($id_question>0)
{
	$row = $pl->QuestionGet($id_question);
	$title[] = array($row['question'],'');
	$approved = $row['approved'];
	$id_questions_group = $row['id_questions_group'];
}
else
{
	$title[] = array("add_new",'');
	$approved = 1;
}
echo $hh->ShowTitle($title);


if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

if($id_question>0 && $poll['id_type']!="4")
{
	$co = new Comments("question",$id_question);
	$co_pending = $co->CommentsPending(0,$t->id);
	$co_approved = $co->CommentsPending(1,$t->id);
	if($poll['comments']>0 || ($co_approved + $co_pending)>0 || $module_admin)
	{
		echo "<div class=\"box\">";
		echo "<h3><a href=\"comments_tree.php?type=question&id_item=$id_question\">" . $hh->tr->Translate("comments") . "</a></h3>\n";
		echo "<ul>";
		if($co_pending>0)
			echo "<li><a href=\"comments.php?type=question&id_item=$id_question&appr=0\">" . $hh->tr->Translate("to_approve") . "</a> (" . $co_pending . ")</li>\n";
		echo "<li><a href=\"comment.php?id=0&type=question&id_item=$id_question\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
		echo "</ul>";
		if($input_right)
		{
			$vote_row = array();
			$num_votes = $pl->Votes( $vote_row, $id_poll, $id_question,$poll['id_type']);
			
			echo "<h3><a href=\"poll_question_answers.php?id=$id_question&id_poll=$id_poll\">" . $hh->tr->Translate("votes") . "</a> ($num_votes)</h3>\n";
		}
		echo "</div>";
	}
}

echo $hh->input_form_open();
echo $hh->input_hidden("id_question",$id_question);
echo $hh->input_hidden("id_poll",$id_poll);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","poll_question");
echo $hh->input_table_open();

if($id_question>0)
{
	echo $hh->input_date("insert_date","insert_date",$row['insert_date_ts'],0);
	if($row['id_p']>0)
	{
		echo $hh->input_text("IP","ip",$row['ip'],15,0,0);
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetById($row['id_p']);
		echo $hh->input_text("author","author","{$user['name1']} {$user['name2']}",30,0,0);
	}
}

echo $hh->input_text("poll_question","question",$row['question'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,5,"",$input_right);
if($poll['id_type']=="3")
{
	echo $hh->input_separator("details");
	echo $hh->input_wysiwyg("description_long","description_long",$row['description_long'],$row['is_html'],10,$input_right,$t->wysiwyg);
}
if($poll['id_type']=="4")
{
	$qgroups = array();
	$num_groups = $pl->QuestionsGroups($qgroups,$id_poll,false);
	echo $hh->input_row("group","id_questions_group",$id_questions_group,$qgroups,"choose_option",0,$input_right);
	echo $hh->input_text("weight","weight",$id_question>0?$row['weight']:1,5,0,$input_right);
}
echo $hh->input_separator("administration");
echo $hh->input_checkbox("approved","approved",$approved,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_question>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

echo "<p><a href=\"poll_question_weights.php?id_poll=$id_poll&id_question=$id_question\">" . $hh->tr->Translate("poll_answers_weights") . "</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>
