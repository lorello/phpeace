<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/polls.php");
include_once(SERVER_ROOT."/../classes/topic.php");
$post = $fh->HttpPost();

$id_poll = $get['id'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);

$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
$title[] = array('status_changes','');

echo $hh->ShowTitle($title);

if($id_poll>0 && $id_topic>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");
	
	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"");
	echo $hh->Tabs($tabs);

	if($input_right)
	{
		include_once(SERVER_ROOT."/../classes/scheduler.php");
		$sc = new Scheduler();
		$changes = $sc->StatusChanges(19,$id_poll);
		if(count($changes)>0)
		{
			$status_options = $hh->tr->Translate("status_options");
			echo "<ul>";
			foreach($changes as $change)
			{
				echo "<li><a href=\"status_change.php?id_res=19&id=$id_poll&id_topic=$id_topic&date=" . urlencode($change['change_date']) . "\">";
				echo $hh->FormatDate($change['change_date_ts']) . "</a>: {$status_options[$change['status']]}</li>";
			}
			echo "</ul>";
		}
		echo "<p><a href=\"status_change.php?id_res=19&id=$id_poll&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
