<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/template.php");

$te = new Template();

$id_topic = (int)$_GET['id_topic'];

if($module_admin)
	$input_right = 1;
	
if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	if($t->AmIAdmin())
		$input_right = 1;	
}

$title[] = array('templates','');

echo $hh->ShowTitle($title);

$row  = array();
$num = $te->Templates($row,$id_topic);

$table_headers = array('name','resources_type','topic','records');
$table_content = array('{LinkTitle("template.php?id=$row[id_template]&id_topic='.$id_topic.'",$row[name])}',
	'{ResourceLookup($row[id_res])}','{TopicGroup($row[id_topic],$row[id_group])}','<div class=\"right\">$row[counter]</div>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($input_right)
	echo "<p><a href=\"template.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
