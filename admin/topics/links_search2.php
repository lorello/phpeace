<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/links.php");

$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('Links','links.php?id='.$id_topic);
$title[] = array('search','links_search.php?id_topic='.$id_topic);
$title[] = array('results','');

echo $hh->ShowTitle($title);

$params = array( 'url' => $get['url'], 'title' => $get['title'] , 'id_subtopic' => $get['id_subtopic'] , 'id_topic' => $get['id_topic'] );

$links = new Links();
$num = $links->Search( $row, $params );

$table_headers = array('date','link','url','in');
$table_content = array('{FormatDate($row[insert_date_ts])}','{LinkTitle("link.php?id=$row[id_link]&id_topic='.$id_topic.'",$row[title])}','$row[url]','{PathToSubtopic($row[id_topic],$row[id_subtopic])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

