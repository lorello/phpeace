<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
include_once(SERVER_ROOT."/../classes/irl.php");
$fo = new Forms();

$id_subtopic = $_GET['id'];
$id_topic = $_GET['id_topic'];
$id_parent = $_GET['id_parent'];

$t = new Topic($id_topic);
$trow = $t->row;

$irl = new IRL();
$irl->topic = $t;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('subtopics_tree','subtopics.php?id='.$id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

if ($id_subtopic>0)
{
	$action2 = "update";
	$row = $t->SubtopicGet($id_subtopic);
	$name = $row['name'];
	$title[] = array($name,'');
	$id_parent = $row['id_parent'];
	$visible = $row['visible'];
	$sort_by = $row['sort_by'];
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$params = $v->Deserialize($row['params']);
}
else
{
	$action2 = "insert";
	$visible = 1;
	$title[] = array('subtopic_add','');
	$name = "";
	$sort_by = 3;
}

if ($id_parent>0)
{
	$row2 = $t->SubtopicGet($id_parent);
	$parent_name = $row2['name'];
}

echo $hh->ShowTitle($title);

if ($id_subtopic>0)
{
	if ($row['id_type']==1 || ($row['id_type']==2 && $row['tot_articles']==0))
		$tools[] = array('subtopic_article_new','/articles/article.php?w=topics&id=0&id_topic='.$id_topic.'&id_subtopic='.$id_subtopic);
	$sub_preview = $row;
	$sub_preview['id'] = $id_subtopic;
	if ($input_right)
	{
		if($row['id_type']>0)
			$tools[] = array('subtopic_child_add',"subtopic.php?id=0&id_topic=$id_topic&id_parent=$id_subtopic");
		$tools[] = array('history','history.php?id_type='.$ah->r->types['subtopic'].'&id_topic='.$id_topic.'&id='.$id_subtopic);
		if ($input_super_right == 1 && $visible!="4" && $t->SubtopicPublishable($row['id_type']))
			$tools[] = array('publish',"/topics/publish_subtopic.php?id=$id_topic&id_subtopic=$id_subtopic");
		$tools[] = array('preview',$irl->Preview("subtopic",$sub_preview));
		if (Modules::AmIAdmin(10) || ($t->id>0 && $t->edit_layout && $t->AmIAdmin()))
		{
			$irl2 = new IRL();
			$xml_url = str_replace("preview.php","/layout/xml.php",$irl2->Preview("subtopic",$sub_preview));
			$tools[] = array('XML',$xml_url);
		}
	}
	echo $hh->Toolbar($tools);
}
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});

var subtopic_subtypes = new Array();
subtopic_subtypes[1] = new Array();
subtopic_subtypes[2] = new Array();
subtopic_subtypes[3] = new Array();
subtopic_subtypes[4] = new Array();
subtopic_subtypes[4][0] = new Array();
subtopic_subtypes[4][0]['id'] = "from_subtopic";
subtopic_subtypes[4][0]['label'] = "from_subtopic";
subtopic_subtypes[4][0]['type'] = "text";
subtopic_subtypes[4][0]['size'] = "5";
subtopic_subtypes[4][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[4][0][0] = 1;

subtopic_subtypes[5] = new Array();
subtopic_subtypes[5][0] = new Array();
subtopic_subtypes[5][0]['id'] = "id_form";
subtopic_subtypes[5][0]['label'] = "Form";
subtopic_subtypes[5][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[5][0]['type'] = "list";
subtopic_subtypes[5][0]['options'] = new Array();
subtopic_subtypes[5][0]['options'][0] = "<?=$hh->tr->Translate("choose_option");?>";
<?php
$forms = array();
$fo->FormsTopic($forms,$id_topic,false);
foreach($forms as $form)
	echo "subtopic_subtypes[5][0]['options'][{$form['id_form']}] = \"" . htmlspecialchars($form['name']) . "\";\n";
?>
subtopic_subtypes[5][0][0] = <?=count($forms);?>;
subtopic_subtypes[6] = new Array();
subtopic_subtypes[6][0] = new Array();
subtopic_subtypes[6][0]['id'] = "link";
subtopic_subtypes[6][0]['label'] = "URL";
subtopic_subtypes[6][0]['type'] = "text";
subtopic_subtypes[6][0]['size'] = "40";
subtopic_subtypes[6][0]['value'] = "<?=$row['link'];?>";
subtopic_subtypes[6][0][0] = 1;
subtopic_subtypes[7] = new Array();
subtopic_subtypes[7][0] = new Array();
subtopic_subtypes[7][0]['id'] = "id_campaign";
subtopic_subtypes[7][0]['label'] = "<?=$hh->tr->Translate("campaign");?>";
subtopic_subtypes[7][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[7][0]['type'] = "list";
subtopic_subtypes[7][0]['options'] = new Array();
subtopic_subtypes[7][0]['options'][0] = "<?=$hh->tr->Translate("list");?>";
<?php
$campaigns = $t->Campaigns();
foreach($campaigns as $campaign)
	echo "subtopic_subtypes[7][0]['options'][{$campaign['id_topic_campaign']}] = \"" . htmlspecialchars($campaign['name']) . "\";\n";
?>
subtopic_subtypes[7][0][0] = <?=count($campaigns);?>;
subtopic_subtypes[7][1] = new Array();
subtopic_subtypes[7][1]['id'] = "id_campaign_subtype";
subtopic_subtypes[7][1]['label'] = "<?=$hh->tr->Translate("initial_page");?>";
subtopic_subtypes[7][1]['value'] = "<?=(int)$row['id_subitem'];?>";
subtopic_subtypes[7][1]['type'] = "list";
subtopic_subtypes[7][1]['options'] = new Array();
<?php
$campaigns2 = $irl->Subitems(7,false);
foreach($campaigns2 as $campaign2)
	echo "subtopic_subtypes[7][1]['options'][{$campaign2[0]}] = \"{$campaign2[1]}\";\n";
?>
subtopic_subtypes[7][1][0] = <?=count($campaigns2);?>;

subtopic_subtypes[8] = new Array();
subtopic_subtypes[8][0] = new Array();
subtopic_subtypes[8][0]['id'] = "id_forum";
subtopic_subtypes[8][0]['label'] = "<?=$hh->tr->Translate("forum");?>";
subtopic_subtypes[8][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[8][0]['type'] = "list";
subtopic_subtypes[8][0]['options'] = new Array();
subtopic_subtypes[8][0]['options'][0] = "<?=$hh->tr->Translate("list");?>";
<?php
$forums = $t->Forums();
foreach($forums as $forum)
	echo "subtopic_subtypes[8][0]['options'][{$forum['id_topic_forum']}] = \"" . htmlspecialchars($forum['name']) . "\";\n";
?>
subtopic_subtypes[8][0][0] = <?=count($forums);?>;
subtopic_subtypes[8][1] = new Array();
subtopic_subtypes[8][1]['id'] = "id_forum_subtype";
subtopic_subtypes[8][1]['label'] = "<?=$hh->tr->Translate("initial_page");?>";
subtopic_subtypes[8][1]['value'] = "<?=(int)$row['id_subitem'];?>";
subtopic_subtypes[8][1]['type'] = "list";
subtopic_subtypes[8][1]['options'] = new Array();
<?php
$forums2 = $irl->Subitems(8,false);
foreach($forums2 as $forum2)
	echo "subtopic_subtypes[8][1]['options'][{$forum2[0]}] = \"{$forum2[1]}\";\n";
?>
subtopic_subtypes[8][1][0] = <?=count($forums2);?>;

subtopic_subtypes[9] = new Array();
subtopic_subtypes[9][0] = new Array();
subtopic_subtypes[9][0]['id'] = "id_gallery";
subtopic_subtypes[9][0]['label'] = "<?=$hh->tr->Translate("gallery");?>";
subtopic_subtypes[9][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[9][0]['type'] = "list";
subtopic_subtypes[9][0]['options'] = new Array();
subtopic_subtypes[9][0]['options'][0] = "<?=$hh->tr->Translate("row_none");?>";
<?php
$galleries = $t->Galleries(  ($conf->Get("users_mode")=="isolated" && !$module_admin)? $ah->current_user_id : 0);
foreach($galleries as $gallery)
	echo "subtopic_subtypes[9][0]['options'][{$gallery['id_gallery']}] = \"" . htmlspecialchars($gallery['title']) . "\";\n";
?>
subtopic_subtypes[9][0][0] = <?=count($galleries);?>;

subtopic_subtypes[9][1] = new Array();
subtopic_subtypes[9][1]['id'] = "id_gallery_subtype";
subtopic_subtypes[9][1]['label'] = "<?=$hh->tr->Translate("initial_page");?>";
subtopic_subtypes[9][1]['value'] = "<?=(int)$row['id_subitem'];?>";
subtopic_subtypes[9][1]['type'] = "list";
subtopic_subtypes[9][1]['options'] = new Array();
<?php
$galleries2 = $irl->Subitems(9,false);
foreach($galleries2 as $gallery2)
	echo "subtopic_subtypes[9][1]['options'][{$gallery2[0]}] = \"{$gallery2[1]}\";\n";
?>
subtopic_subtypes[9][1][0] = <?=count($galleries2);?>;

subtopic_subtypes[10] = new Array();
subtopic_subtypes[10][0] = new Array();
subtopic_subtypes[10][0]['id'] = "events_subtype";
subtopic_subtypes[10][0]['label'] = "<?=$hh->tr->Translate("initial_page");?>";
subtopic_subtypes[10][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[10][0]['type'] = "list";
subtopic_subtypes[10][0]['options'] = new Array();
<?php
$etypes = $irl->Subitems(10,false);
foreach($etypes as $etype)
	echo "subtopic_subtypes[10][0]['options'][{$etype[0]}] = \"{$etype[1]}\";\n";
?>
subtopic_subtypes[10][0][0] = <?=count($etypes);?>;

subtopic_subtypes[11] = new Array();

subtopic_subtypes[13] = new Array();
subtopic_subtypes[13][0] = new Array();
subtopic_subtypes[13][0]['id'] = "insert_type";
subtopic_subtypes[13][0]['label'] = "<?=$hh->tr->Translate("resources_type");?>";
subtopic_subtypes[13][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[13][0]['type'] = "list";
subtopic_subtypes[13][0]['options'] = new Array();
<?php
$itypes = $hh->tr->Translate("insert_types");
foreach($itypes as $key=>$itype)
	if($itype!="___")
		echo "subtopic_subtypes[13][0]['options'][{$key}] = \"{$itype}\";\n";
?>
subtopic_subtypes[13][0][0] = <?=count($itypes);?>;

subtopic_subtypes[14] = new Array();

subtopic_subtypes[15] = new Array();
subtopic_subtypes[15][0] = new Array();
subtopic_subtypes[15][0]['id'] = "id_resource";
subtopic_subtypes[15][0]['label'] = "<?=$hh->tr->Translate("resources_type");?>";
subtopic_subtypes[15][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[15][0]['type'] = "list";
subtopic_subtypes[15][0]['options'] = new Array();
<?php
foreach($hh->tr->Translate("resources") as $r_key=>$resource_type)
	if(in_array($r_key,$t->SubtopickeywordFilterTypes()))
		echo "subtopic_subtypes[15][0]['options'][{$r_key}] = \"" . htmlspecialchars($resource_type) . "\";\n";
?>
subtopic_subtypes[15][0][0] = 0;
subtopic_subtypes[15][1] = new Array();
subtopic_subtypes[15][1]['id'] = "param_keyword_topic";
subtopic_subtypes[15][1]['label'] = "<?=$hh->tr->Translate("topic");?>";
subtopic_subtypes[15][1]['value'] = "<?=(int)$params['keyword_topic'];?>";
subtopic_subtypes[15][1]['type'] = "list";
subtopic_subtypes[15][1]['options'] = new Array();
<?php
$kw_topics = $hh->tr->Translate("topic_or_all");
foreach($kw_topics as $kw_key=>$kw_topic)
		echo "subtopic_subtypes[15][1]['options'][{$kw_key}] = \"{$kw_topic}\";\n";
?>
subtopic_subtypes[15][1][0] = <?=count($kw_topics);?>;

subtopic_subtypes[15][2] = new Array();
subtopic_subtypes[15][2]['id'] = "filter_keyword";
subtopic_subtypes[15][2]['label'] = "<?=$hh->tr->Translate("keyword");?>";
subtopic_subtypes[15][2]['type'] = "text";
subtopic_subtypes[15][2]['size'] = "20";	
<?php
$filter_keyword = "";
if($row['id_subitem']>0)
{
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$keyrow = $k->KeywordGet($row['id_subitem']);
	$filter_keyword = $keyrow['keyword'];
}
elseif($params['filter_keywords']!="")
	$filter_keyword = $params['filter_keywords'];
?>
subtopic_subtypes[15][2]['value'] = "<?=$filter_keyword;?>";
subtopic_subtypes[15][2][0] = 1;

subtopic_subtypes[15][3] = new Array();
subtopic_subtypes[15][3]['id'] = "param_keyword_year";
subtopic_subtypes[15][3]['label'] = "<?=$hh->tr->Translate("anniversary_year");?>";
subtopic_subtypes[15][3]['type'] = "text";
subtopic_subtypes[15][3]['size'] = "4";
subtopic_subtypes[15][3]['value'] = "<?=$params['keyword_year'];?>";
subtopic_subtypes[15][3][0] = 1;

subtopic_subtypes[16] = new Array();

subtopic_subtypes[17] = new Array();
subtopic_subtypes[17][0] = new Array();
subtopic_subtypes[17][0]['id'] = "id_poll";
subtopic_subtypes[17][0]['label'] = "<?=$hh->tr->Translate("poll");?>";
subtopic_subtypes[17][0]['value'] = "<?=(int)$row['id_item'];?>";
subtopic_subtypes[17][0]['type'] = "list";
subtopic_subtypes[17][0]['options'] = new Array();
subtopic_subtypes[17][0]['options'][0] = "<?=$hh->tr->Translate("list");?>";
<?php
$polls = array();
$t->Polls($polls,false);
foreach($polls as $poll)
	echo "subtopic_subtypes[17][0]['options'][{$poll['id_poll']}] = \"" . htmlspecialchars($poll['title']) . "\";\n";
?>
subtopic_subtypes[17][0][0] = <?=count($polls);?>;
subtopic_subtypes[17][1] = new Array();
subtopic_subtypes[17][1]['id'] = "id_poll_subtype";
subtopic_subtypes[17][1]['label'] = "<?=$hh->tr->Translate("initial_page");?>";
subtopic_subtypes[17][1]['value'] = "<?=(int)$row['id_subitem'];?>";
subtopic_subtypes[17][1]['type'] = "list";
subtopic_subtypes[17][1]['options'] = new Array();
<?php
$polls2 = $irl->Subitems(17,false);
foreach($polls2 as $poll2)
	echo "subtopic_subtypes[17][1]['options'][{$poll2[0]}] = \"{$poll2[1]}\";\n";
?>
subtopic_subtypes[17][1][0] = <?=count($polls2);?>;

subtopic_subtypes[18] = new Array();

var subtopic_modules = new Array();
<?php
$subtopic_modules = $t->Modules();
foreach($subtopic_modules as $subtopic_module)
{
	echo "subtopic_modules[{$subtopic_module[0]}] = new Array();\n";
	$submodules = $irl->Subitems($subtopic_module[0],true,$id_topic);
	$label = $irl->SubItemsLabel($subtopic_module[0]);
	if(count($submodules)>0)
	{
		echo "subtopic_modules[{$subtopic_module[0]}][0] = new Array();\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['id'] = \"id_subitem\";\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['label'] = \"" . $hh->tr->Translate($label) . "\";\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['value'] = \"" . (int)$row['id_subitem'] . "\";\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['type'] = \"list\";\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['options'] = new Array();\n";
		echo "subtopic_modules[{$subtopic_module[0]}][0]['options'][0] = \"" . $hh->tr->Translate("choose_option") . "\";";
		$display_id = false;
		foreach($submodules as $submodule)
		{
			echo "subtopic_modules[{$subtopic_module[0]}][0]['options'][{$submodule[0]}] = \"{$submodule[1]}\";\n";
			if($submodule[2])
			{
				$display_id = true;
			}
		}
		echo "subtopic_modules[{$subtopic_module[0]}][0][0] = " . count($submodules) . ";\n";
		if($display_id)
		{
			echo "subtopic_modules[{$subtopic_module[0]}][1] = new Array();\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1]['id'] = \"id_subitem_id\";\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1]['label'] = \"ID\";\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1]['value'] = \"" . $row['id_subitem_id'] . "\";\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1]['type'] = \"text\";\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1]['size'] = \"9\";\n";
			echo "subtopic_modules[{$subtopic_module[0]}][1][0] = 1;\n";
		}
	}
}
?>
</script>
<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","subtopic");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_subtopic",$id_subtopic);
echo $hh->input_hidden("id_parent_old",$id_parent);
echo $hh->input_table_open("subtopic-type");
if($id_subtopic>0)
{
	$furl_row = $irl->FriendlyUrlGet(2,$id_subtopic);
	$subtopic_types = $t->subtopic_types;
	if($row['id_type']!=$subtopic_types['url'])
	{
		$url = $t->SubtopicPublishable($row['id_type'])? $irl->PublicUrlTopic("subtopic",$sub_preview,$t,false) : $irl->PublicUrlGlobal("subtopic",$sub_preview,false);
	}
	else 
		$url = $row['link'];
	$subtopic_note = "ID: $id_subtopic - Link: <a href=\"$url\" target=\"_blank\">$url</a>";
	if($furl_row['furl']!="")
	{
		$subtopic_furl = $t->url . "/" . $furl_row['furl'];
		$subtopic_note .= " (<a href=\"$subtopic_furl\" target=\"_blank\">$subtopic_furl</a>)";
	}
	if ($row['id_type']<3 && $row['tot_articles']>0)
		$subtopic_note .= "<br>" . $hh->tr->TranslateParams("subtopic_articles",array($id_topic,$id_subtopic,$row['tot_articles']));
	if($row['has_feed'])
	{
		$subtopic_rss = $irl->PublicUrlGlobal("rss",array('id'=>$id_subtopic,'subtype'=>"subtopic"));
		$subtopic_note .= "<p>Feed: <a href=\"$subtopic_rss\" target=\"_blank\">$subtopic_rss</p>";
	}
	echo $hh->input_note($subtopic_note);
}
echo $hh->input_separator("definition");
echo $hh->input_text("topic","topic",$t->name,40,0,0);
echo $hh->input_text("name","name",$name,50,0,$input_right);
echo $hh->input_array("status","visible",$visible,$hh->tr->Translate("subtopic_visibility"),$input_right && (!$id_subtopic>0 || $row['id_type']!="3"));

if($id_subtopic>0 && $trow['friendly_url']>0)
{
	$furl = "";
	$furl_row = $irl->FriendlyUrlGet(2,$id_subtopic);
	if(isset($furl_row['id']) && $furl_row['furl']!="")
	{
		$furl = $furl_row['furl'];
	}
	else
	{
		$furl = $irl->FriendlyUrlSuggest($id_topic,$t->row['friendly_url'],2,$name,"",0);
	}
	echo $hh->input_text("Friendly URL","furl",$furl,20,0,$input_right,"",false,0,"<em>{$t->url}/</em>");
}

echo $hh->input_subtopic("son_of","id_parent","'subtopic_subtopic.php?&id_topic='+document.forms['form1'].id_topic.value+'&id_subtopic='+document.forms['form1'].id_subtopic.value+'&id_parent='+document.forms['form1'].id_parent.value",$id_parent,$id_topic,$input_right);
$alltypes = array();
foreach($hh->tr->Translate("subtopic_types") as $skey=>$stype)
{
	if($skey!="12")
		$alltypes["s_".$skey] = $stype;
}
foreach($subtopic_modules as $subtopic_module)
{
	$alltypes["m_".$subtopic_module[0]] = htmlspecialchars($hh->tr->Translate("module") . " " . $tr_modules[$subtopic_module[0]]);
}

$current_type = ($row['id_type']!="12")? "s_" . $row['id_type'] : "m_" . $row['id_item'];
echo $hh->input_array("type","id_type",$current_type,$alltypes,$input_right,"subSubtopic(this.value," . (int)$input_right . ")");

echo "<tr><td class=\"input-label\">" . $hh->tr->Translate("parameters") . "</td><td><div id=\"type_item\"></div></td></tr>\n";
echo $hh->input_separator("content");
echo $hh->input_textarea("description","description",$row['description'],50,3,"",$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
echo $hh->input_keywords($id_subtopic,$o->types['subtopic'],$keywords,$input_right);
if($id_subtopic>0)
{
	include_once(SERVER_ROOT."/../classes/images.php");
	$i = new Images();
	echo "<tr><td align=\"right\">" . $hh->tr->Translate("image_associated") . "</td><td valign=middle>";
	if ($row['id_image']>0)
	{
		$img_sizes = $i->img_sizes;
		echo "<img src=\"/images/upload.php?src=images/0/{$row['id_image']}.{$i->convert_format}\" width=\"{$img_sizes[0]}\" border=\"0\" align=\"left\" >&nbsp;\n";
	}
	echo "(<a href=\"subtopic_images.php?id=$id_subtopic&id_topic=$id_topic\">" . $hh->tr->Translate("change") . "</a>)</td></tr>\n";
	if ($row['id_image']>0)
		echo $hh->input_array("width","param_image_size",$params['image_size'],$i->img_sizes,$input_right);
}
echo $hh->input_array("sort_articles_by","sort_by",$sort_by,$hh->tr->Translate("sort_by_options"),$input_right && (!$id_subtopic>0 || $row['id_type']!="3"));

$combo_values = array();
$combo_values[0] = "Default ($t->records_per_page)";
for($i=1;$i<=50;$i++)
	$combo_values["".$i.""] = $i;
echo $hh->input_array("articles_per_page","records_per_page",$row['records_per_page'],$combo_values,$input_right);
if($id_subtopic>0)
	echo $hh->input_checkbox("Feed","has_feed",$row['has_feed'],0,$row['id_type']=="1" && $input_right);

echo $hh->input_textarea("page_header","header",$row['header'],80,5,"",$input_right);
echo $hh->input_textarea("page_footer","footer",$row['footer'],80,5,"",$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
if ($id_subtopic>0)
{
	$t->LoadTree();
	$tot_subtopics = count($t->SubtopicChildren($id_subtopic));
	$tot_docs = count($t->SubtopicDocs($id_subtopic));
	
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$row['tot_articles']==0 && $tot_subtopics==0 && $tot_docs==0 && $input_right);
	if (!($row['tot_articles']==0 && $tot_subtopics==0))
		echo $hh->input_note($hh->tr->TranslateParams("subtopic_remove_note",array($tot_subtopics,$row['tot_articles'])));
}

echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close();
echo $hh->input_form_close();

if ($id_subtopic>0)
{
?>
<script type="text/javascript">
subSubtopic("<?=$current_type;?>",<?=(int)$input_right;?>)
</script>
<?php	
}

include_once(SERVER_ROOT."/include/footer.php");
?>

