<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/images.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('images_gallery','');

echo $hh->ShowTitle($title);

$i = new Images();
$num = $i->Topic( $row, $id_topic );
$table_headers = array('image','caption','format','size','&nbsp;');
$table_content = array('{Graphic("/images/upload.php?src=images/0/$row[id_image].'.$i->convert_format.'&format=$row[format]",'.$i->img_sizes[0].',floor('.$i->img_sizes[0].'*$row[ratio]),false,$row[id_image])}',
'$row[caption]','<div class=\"center\">$row[format]</div>','$row[width]x$row[height] px',
'{LinkTitle("/articles/image.php?id=$row[id_image]&id_topic='.$id_topic.'&w=topics","' . $hh->tr->Translate("open") .'")}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

