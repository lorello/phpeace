<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();

$id_gtype = ($_GET['id_gtype']!="")? (int)$_GET['id_gtype']:-1;

if ($module_admin)
	$input_right = 1;

$title[] = array('features_global','');
echo $hh->ShowTitle($title);

if ($id_gtype>=0)
{
	$page_types_global = $hh->tr->Translate("page_types_global");	
	echo "<p>" . $hh->tr->Translate("features") . ": <b>" . $page_types_global[$id_gtype] . "</b></p>";
	$num = $pt->ft->GlobalPageFeatures($row,$id_gtype);
}
else
{
	$num = $pt->ft->GlobalPageFeaturesAll($row);
}

$table_headers = array('feature','description','page_type','function','active','author');
$table_content = array('{LinkTitle("feature_global.php?id=$row[id_feature]",$row[name])}',
'$row[description]','{PageTypeGlobal($row[id_type])}','{PageFunction($row[id_function])}','{Bool2YN($row[active])}','{UserLookup($row[id_user])}');
echo $hh->showTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"feature_global.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

