<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPostUnescaped();

$id_article = $fh->Null2Zero($post['id_article']);
$id_topic = $post['id_topic'];

if ($post['headline']!="")
{
	$article = array('id_article'	=> $id_article,
			'id_user'	=> $post['id_user'],
			'id_topic'	=> $id_topic,
			'id_subtopic'	=> $fh->Null2Zero($post['id_subtopic']),
			'halftitle'	=> $post['halftitle'],
			'headline'	=> $post['headline'],
			'subhead'	=> $post['subhead'],
			'content'	=> $post['content'],
			'notes'		=> $post['notes'],
			'source'	=> $post['source'],
			'author'	=> $post['author'],
			'author_notes'	=> $post['author_notes'],
			'written_ts'	=> mktime(0,0,0,$post['written_m'],$post['written_d'],$post['written_y']),
			'show_date'	=> $fh->Checkbox2bool($post['show_date']),
			'show_author'	=> $fh->Checkbox2bool($post['show_author']),
			'show_source'	=> $fh->Checkbox2bool($post['show_source']),
			'is_html'	=> $fh->Checkbox2bool($post['is_html']),
			'id_licence'	=> $post['id_licence'],
			'align'		=> (int)$post['align'],
			'headline_visible'	=> $fh->Checkbox2bool($post['headline_visible']),
			'original'	=> $fh->CheckDateString($post['original']),
			'id_language'	=> $post['id_language']-1,
			'allow_comments'	=>	$post['allow_comments'],
			'topic_allow_comments'	=>	$post['topic_allow_comments']
		);
	$l = new Layout(true);

	$l->article = $article;
	echo $l->Output("article",$id_article,$id_topic,1,array());
}
?>

