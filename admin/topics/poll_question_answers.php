<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");

$id_poll = $_GET['id_poll'];
$id_question = (int)$_GET['id'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
if($id_question>0)
{
	$row = $pl->QuestionGet($id_question);
	$title[] = array($row['question'],'poll_question.php?id='.$id_question.'&id_poll='.$id_poll);
}
$title[] = array('votes','');

echo $hh->ShowTitle($title);

if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes','');
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

if($input_right)
{
	$row = array();
	$num = $pl->Votes( $row, $id_poll, $id_question, $poll['id_type']);
	$table_headers = array('date','name','poll_question','vote');
	$table_content = array('{FormatDateTime($row[insert_date_ts])}','{LinkTitle("poll_question_answer.php?id_p=$row[id_p]&id=$row[id_question]&id_poll='.$id_poll.'",$row[name])}','$row[question]','<div class=\"right\">$row[vote]</div>');
	if($poll['id_type']=="4")
	{
		unset($table_headers[2]);
		unset($table_content[2]);
		unset($table_headers[3]);
		unset($table_content[3]);
	}
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	
	if($poll['id_type']=="4")
	{
		echo "<p><a href=\"poll_dump.php?id=$id_poll\" target=\"_blank\">XML dump</a></p>";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
