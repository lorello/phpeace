<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);

include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
$ini = new Ini();

$id_topic = $_GET['id_topic'];
$id_form = $_GET['id'];

if ($ah->ModuleAdmin(4))
	$input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

$t = new Topic($id_topic);
if ($id_topic>0 && $t->AmIAdmin())
	$input_right = 1;

$rows = $fo->PostsAll($id_form,$id_topic);

if($input_right && count($rows)>0)
{
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	
	$headers = array("TIMESTAMP","DATE","NAME","NAME2","NAME3");
	if($form['weights'])
		$headers[] = "SCORE";
	$row0 = $rows[0];
	$post = $v->Deserialize($row0['post']);
	if(is_array($post) && count($post)>0)
	{
		foreach($post as $post_item)
		{
			$headers[] = strtoupper($post_item['name']);
		}
	}
	$headers[] = "ADMIN_LINK";

	
	$link = $ini->get("admin_web") . "/topics/form_post.php?id_form=$id_form&id_topic=$id_topic&id=";
	$data = array();
	foreach($rows as $row)
	{
		$data_row = array();
		$data_row[] = $row['post_time_ts'];
		$data_row[] = date("r",$row['post_time_ts']);
		$data_row[] = $row['name1'];
		$data_row[] = $row['name2'];
		$data_row[] = $row['name3'];
		if($form['weights'])
			$data_row[] = $row['score'];
		$post = $v->Deserialize($row['post']);
		if(is_array($post) && count($post)>0)
		{
			reset($post);
			foreach($post as $post_item)
			{
				$data_row[] = $post_item['value'];
			}
		}
		$data_row[] = $link . $row['id_post'];
		$data[] = $data_row;
	}

    include_once(SERVER_ROOT."/../classes/modules.php");
	include_once(SERVER_ROOT."/../classes/texthelper.php");
	$th = new TextHelper();
    echo $th->CSVDump("form_posts_$id_form",$headers,$data);
}
?>

