<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/publishmanager.php");
$pm = new PublishManager();

$id_topic = $_GET['id'];
$id_article = $_GET['id_article'];

$pm->TopicInit($id_topic);

$title[] = array($pm->layout->topic->name,'ops.php?id='.$id_topic);
$title[] = array('publish_article','');

echo $hh->ShowTitle($title);

if (($pm->layout->topic->AmIAdmin() || $module_admin) && $id_topic!=$hh->ini->Get('temp_id_topic') && $pm->layout->topic->id_style>0 )
{
	$pm->JobExecute(array('id_type'=>'1','action'=>'update','id_item'=>$id_article),$id_topic,FALSE);
	$id_subtopic = $pm->layout->topic->SubtopicArticle($id_article);
	if($id_subtopic>0)
		$pm->JobExecute(array('id_type'=>'8','action'=>'update','id_item'=>$id_subtopic),$id_topic,FALSE);
	echo $pm->MessageGet();
}
else
	echo "<p>" . $hh->tr->Translate("user_no_auth") . "</p>";
	
include_once(SERVER_ROOT."/include/footer.php");
?>

