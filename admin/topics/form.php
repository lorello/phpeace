<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
$fo = new Forms();

$id_topic = (int)$_GET['id_topic'];
$id_form = $_GET['id'];

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

$is_system = false;

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$users = $t->Users();
}
else 
{
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users();
	$users = $uu->Active();
	$t = new Topic(0);
}
$users[] = array('id_user'=>0,'name'=>"--" . $hh->tr->Translate("contact_main") . "--");

$title[] = array('forms','forms.php?id='.$id_topic);


if ($id_form>0)
{
	$row = $fo->FormGet($id_form);
	$title[] = array($row['name'],'');
	$id_user = $row['id_user'];
	if ($id_topic>0 && $id_topic==$row['id_topic'] && $t->AmIAdmin())
		$input_right = 1;
	if ($id_user == "0")
		$is_system = true;
	$form_topic = $row['id_topic'];
}
else
{
	$title[] = array('add_new','');
	$id_user = $ah->current_user_id;
	$form_topic = $id_topic;
	if ($id_topic>0 && $t->AmIAdmin())
		$input_right = 1;
}

echo $hh->ShowTitle($title);

if($id_form>0)
{
	$tabs = array();
	$tabs[] = array("form","");
	$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
	if($row['store'])
	{
		$posts = array();
		$num_posts = $fo->Posts($posts,$id_form,$id_topic);
		$tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
        $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
	}
	if($row['weights'])
		$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
	$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
	echo $hh->Tabs($tabs);
}

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","form");
echo $hh->input_hidden("id_form",$id_form);
echo $hh->input_hidden("id_topic_current",$id_topic);
echo $hh->input_table_open();

if($id_form>0 && $row['store']=="1" && $row['profiling']=="0" && $row['require_auth']="3")
{
	echo $hh->input_note("form_profiling_warning",true);
}
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($form_topic,0,$tt->AllTopics(),"all_option",$input_super_right);
echo $hh->input_checkbox("multilanguage","multilanguage",$id_form>0?$row['multilanguage']:0,0,$input_right);
echo $hh->input_array("type","profiling",$id_form>0?$row['profiling']:1,$hh->tr->Translate("form_types"),$input_right);
$tgroups = array();
$num_groups = $t->PeopleGroups($tgroups,false,true);
if($num_groups>0)
	echo $hh->input_row("group","id_pt_group",$row['id_pt_group'],$tgroups,"none_option",0,$input_right);
echo $hh->input_checkbox("privacy_warning","privacy_warning",$row['privacy_warning'],0,$input_right);
echo $hh->input_user("recipient",$users,$row['id_recipient'],$input_right,"id_recipient");
echo $hh->input_textarea("recipient_email","recipient",$row['recipient'],80,1,"",$input_right);
echo $hh->input_checkbox("hide_empty_fields","hide_empty_fields",$id_form>0?$row['hide_empty_fields']:0,0,$input_right);
echo $hh->input_array("open_to","require_auth",$id_form>0?$row['require_auth']:3,$hh->tr->Translate("profiling_options"),$input_right);
echo $hh->input_checkbox("form_store","store",$id_form>0?$row['store']:1,0,$input_right);
echo $hh->input_checkbox("form_weights","weights",$id_form>0?$row['weights']:0,0,$input_right);
if($conf->Get("recaptcha")!="")
{
	echo $hh->input_checkbox("captcha","captcha",($id_form>0? $row['captcha']:1),0,$input_right);
}
echo $hh->input_textarea("thanks_text","thanks",$row['thanks'],80,4,"",$input_right);
echo $hh->input_textarea("thanks_email","thanks_email",$row['thanks_email'],80,4,"",$input_right);
echo $hh->input_text("redirect","redirect",$row['redirect'],60,0,$input_right);
$hhf = new HHFunctions();
echo $hh->input_text("author","id_user",$hhf->UserLookup($id_user),50,0,0);

if($row['profiling']>1)
{
	$trm15 = new Translator($hh->tr->id_language,15);
	if(!$row['id_payment_type']>0)
		echo $hh->input_note($trm15->Translate("missing_payment_type"),TRUE);
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$types = array();
	$p->Types($types,$id_topic,false);
	echo $hh->input_row($trm15->Translate("payment_type"),"id_payment_type",$row['id_payment_type'],$types,"choose_option",0,$input_right);

	$currencies = $p->currencies;
	$currency = $currencies[$ini->Get("default_currency")];
	echo $hh->input_text("amount","amount",$row['amount'],15,0,$input_right,$currency . " <br><span class=\"notes\">(" . $hh->tr->Translate("amount_note") . ")</span>");
	echo $hh->input_checkbox("editable","editable",$row['editable'],0,$input_right);
	
	echo "<tr><td valign=\"top\">";
	echo "<h3 style=\"text-align:right;\">" . $hh->tr->Translate("accounts") . "</h3>\n";
	echo "</td><td>";
	$accounts = $p->AccountsUse(2,$id_form);
	echo "<ul>";
	if(count($accounts)>0)
	{
		$currencies = $p->currencies;
		$currency = $currencies[$ini->Get("default_currency")];
		$account_types = $hh->tr->Translate("account_types");
		foreach($accounts as $account)
		{
			echo "<li><a href=\"form_account.php?id={$account['id_use']}&id_form=$id_form&id_topic=$id_topic\">{$account['name']}</a>";
			echo " ({$account_types[$account['id_type']]}";
			if($account['amount']!="")
				echo " - {$account['amount']} $currency";
			echo ")</li>\n";
		}
	}
	if ($input_right)
	{
		$av_accounts = $p->AccountsUseAvailable(2,$id_form,$id_topic);
		if(count($av_accounts)>0)
			echo "<li><a href=\"form_account.php?id=0&id_form=$id_form&id_topic=$id_topic\">" . $hh->tr->Translate("associate") . "</a></li>\n";	
		else
			echo "<li><a href=\"account.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></li>\n";	
	}
	echo "</ul>";
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right));
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && !$is_system && $id_form>0));
echo $hh->input_actions($actions,$input_right || $is_system);

echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>

