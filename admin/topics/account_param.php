<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$id_topic = (int)$_GET['id_topic'];
$id_account = (int)$_GET['id_account'];
$id_param = (int)$_GET['id'];

$t = new Topic($id_topic);
if ($t->AmIAdmin() || $module_admin)
	$input_right = 1;
$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('accounts','accounts.php?id='.$id_topic);

$p = new Payment();
$row = $p->AccountGet($id_account);
$aparams = $p->AccountParams($row['id_type']);
$params = $p->AccountParamsGet($id_account,false);
$param = $aparams[$id_param];
$title[] = array($row['name'],'account.php?id='.$id_account.'&id_topic='.$id_topic);
$title[] = array($param,'');
$active = $row['active'];
if($id_topic != $row['id_topic'])
	$input_right = 0;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","account_param");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_account",$id_account);
echo $hh->input_hidden("param",$param);
echo $hh->input_table_open();
echo $hh->input_param($param,$params[$param],$id_topic,$input_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>
