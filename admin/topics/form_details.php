<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");

$id_topic = $get['id_topic'];
$id_form = $get['id'];
$start_date = $get['start_date'];
$end_date = $get['end_date'];

if ($module_admin)
    $input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
    $t = new Topic($id_topic);
    if ($t->AmIAdmin())
        $input_right = 1;
    $title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('forms','forms.php?id='.$id_topic);
$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('Reports','form_summary.php?id='.$id_form.'&id_topic='.$id_topic);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
    $posts = array();
    $num_posts = $fo->Posts($posts,$id_form,$id_topic);
    $tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
if($form['weights'])
    $tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);
    
if($input_right)
{
    $num = $fo->PostsDetail($rows,$id_form,$id_topic,$start_date,$end_date);
    include_once(SERVER_ROOT."/../classes/varia.php");
    $v = new Varia();
    
    $table_headers = array("date","name");
    $table_content = array('{FormatDate($row[post_time])}',
    '{LinkTitle("form_post.php?id=$row[id_post]&id_topic={$id_topic}&id_form='.$id_form.'","$row[name1]&nbsp;$row[name2]&nbsp;$row[name2]")}');    
    if($form['weights'])
    {
        $table_headers[] = "score";
        $table_content[] = '<div class=right>$row[score]</div>';
    }
    
    $data = array();
    foreach($rows as $row)
    {
        $data_row = array();
        $data_row['id_post'] = $row['id_post'];
        $data_row['post_time'] = $row['post_time_ts'];
        $data_row['name1'] = $row['name1'];
        $data_row['name2'] = $row['name2'];
        $data_row['name3'] = $row['name3'];
        if($form['weights'])
            $data_row['score'] = $row['score'];
        $post = $v->Deserialize($row['post']);
        if(is_array($post) && count($post)>0)
        {
            reset($post);
            $i = 1;
            foreach($post as $post_item)
            {
                $data_row['v'.$i] = $post_item['value'];
                $i++;
            }
        }
        $data_row['admin_link'] = $link . $row['id_post'];
        $data[] = $data_row;
    }                                
    if(is_array($post) && count($post)>0)
    {
        reset($post);
        $i = 1;
        foreach($post as $post_item)
        {
            $table_headers[] = strtoupper($post_item['name']);
            $table_content[] = '$row[v' . $i . ']';
            $i++;
        }
    }
    echo $hh->showTable($data, $table_headers, $table_content, $num);
    
    echo "<p><a href=\"form_details_csv.php?id=$id_form&id_topic=$id_topic&start_date=$start_date&end_date=$end_date\">CSV</a></p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

