<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id_topic'];
$id_type = $_GET['id'];

$t = new Topic($id_topic);

$trm15 = new Translator($hh->tr->id_language,15);
$p = new Payment();

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($trm15->Translate("payment_types"),'payment_types.php?id='.$id_topic);

if ($id_type>0)
{
	$row = $p->Type($id_type);
	$title[] = array('change','');
	$type_topic = $row['id_topic'];
}
else
{
	$row = "";
	$action2 = "insert";
	$title[] = array('add_new','');
	$type_topic = $id_topic;
}

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","payment_type");
echo $hh->input_hidden("id_payment_type",$id_type);
echo $hh->input_table_open();

echo $hh->input_text($trm15->Translate("payment_type"),"payment_type",$row['payment_type'],30,0,$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($type_topic,0,$tt->AllTopics(),"all_option",0);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
// $actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_type>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

