<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/keyword.php");

$id_topic = $_GET['id_topic'];
$id_keyword = $_GET['id'];

$t = new Topic($id_topic);
$k = new Keyword();
$row = $k->KeywordGet($id_keyword);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('settings_main','topic.php?id='.$id_topic);
$title[] = array('keywords_internal','topic_keywords.php?id='.$id_topic);
$keyword = $t->KeywordGet($id_keyword);
$role = $keyword['id_keyword']>0? $keyword['role'] : 1;

if($id_keyword>0)
{
	$title[] = array($row['keyword'],'');
}
else
{
	$title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

if($id_keyword>0)
{
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","topic_keyword");
	echo $hh->input_hidden("id_topic",$id_topic);
	echo $hh->input_hidden("id_keyword",$id_keyword);
	echo $hh->input_table_open();
	echo $hh->input_text("keyword","keyword",$row['keyword'],50,0,0);
	echo $hh->input_textarea("description","description",$row['description'],80,7,"",0);
	$resource_types = $hh->tr->Translate("resources");
	$resource_types[0] = $hh->tr->Translate("all_option");
	foreach(array_keys($resource_types) as $key)
	{
		if($key!=5 && $key!=6 && $key!=7 && $key!=14)
		{
			unset($resource_types[$key]);
		}
	}
	echo $hh->input_array("resources_types","id_res_type",$keyword['id_res_type'],$resource_types,$input_right);
	$role_options = $hh->tr->Translate("role_options");
	echo $hh->input_array("management","role",$role,$role_options,$input_right);
	$actions = array();
	$actions[] = array('action'=>($keyword['id_keyword']>0?"update":"insert"),'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($keyword['id_keyword']>0 && $input_right));
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
else
{
	$rows = array();
	$num = $t->KeywordsInternalAvailable($rows);

	$table_headers = array('keyword','description');
	$table_content = array('{LinkTitle("topic_keyword.php?id_topic='.$id_topic.'&id=$row[id_keyword]",$row[keyword])}','$row[description]');

	echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

