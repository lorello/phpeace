<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_topic = (int)$_GET['id'];

$t = new Topic($id_topic);
$row = $t->row;

$user_right = 0;
if ($module_admin || $t->AmIUser())
	$user_right = 1;
if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin )
	$input_super_right = 1;


$title[] = array($t->name,'');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
if ($id_topic==$tt->temp_id_topic)
	echo "<p>" . $hh->tr->Translate("trashbin_info") . "</p>\n";
else
{
	echo "<div>" . $hh->tr->Translate("published_info") ." <a href=\"$t->url\" target=\"_blank\">$t->url</a>";
	echo " - <a href=\"preview.php?id_topic=$id_topic&id_type=1\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a>";
	echo "<div class=\"notes\">ID: $id_topic";
	
	$last = $t->LastPublish();
	if ($last>0)
		echo " - " . $hh->tr->TranslateParams("published_last",array($hh->FormatDateTime($last)));
	echo "</div>\n";
	echo "</div>\n";
}

if ($input_right==1)
{
	echo "<div class=\"box\">\n";
	echo "<h2>" . $hh->tr->Translate("management") . "</h2>\n";
	echo "<h3>" . $hh->tr->Translate("topic") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"topic.php?id=$id_topic\">" . $hh->tr->Translate("settings_main") . "</a></li>\n";
	if ($id_topic!=$tt->temp_id_topic)
	{
		echo "<li><a href=\"subtopics.php?id=$id_topic\" title=\"" . $hh->tr->Translate("subtopics_tree") . "\">" . $hh->tr->Translate("structure") . "</a></li>\n";
		echo "<li><a href=\"templates.php?id_topic=$id_topic\">" . $hh->tr->Translate("templates") . "</a></li>\n";
		if($t->row['domain']!="")
			echo "<li><a href=\"robots.php?id=$id_topic\">robots.txt</a></li>\n";
	}
	echo "</ul>";
	if ($id_topic!=$tt->temp_id_topic)
	{
		echo "<h3>Homepage</h3>\n";
		echo "<ul>";
		echo "<li><a href=\"homepage.php?id=$id_topic\">" . $hh->tr->Translate("content") . "</a></li>\n";
		echo "<li><a href=\"topic_gra.php?id=$id_topic\">" . $hh->tr->Translate("layout_settings") . "</a></li>\n";
		echo "<li><a href=\"preview.php?id_topic=$id_topic&id_type=1\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a></li>\n";
		echo "</ul>";
		echo "<h3>" . $hh->tr->Translate("modules") . "</h3>\n";
		echo "<ul>";
		echo "<li><a href=\"forms.php?id=$id_topic\">" . $hh->tr->Translate("forms") . "</a></li>\n";
		echo "<li><a href=\"newsletter.php?id=$id_topic\">Newsletter</a></li>\n";
		echo "<li><a href=\"campaigns.php?id=$id_topic\">" . $hh->tr->Translate("campaigns") . "</a></li>\n";
		echo "<li><a href=\"forums.php?id=$id_topic\">" . $hh->tr->Translate("forum") . "</a></li>\n";
		echo "<li><a href=\"polls.php?id=$id_topic\">" . $hh->tr->Translate("polls") . "</a></li>\n";
		if($t->HasSubtopicTypeItem($t->subtopic_types['module'],16))
			echo "<li><a href=\"books_config.php?id=$id_topic\">" . $hh->tr->Translate("books") . "</a></li>\n";
		echo "<li><a href=\"rss_groups.php?id_topic=$id_topic\">RSS feeds</a></li>\n";
		echo "</ul>";
		if($t->profiling)
		{
			$prows = array();
			$nump = $t->PeopleNum();
			echo "<h3>" . $hh->tr->Translate("profiling_topic") . "</h3>\n";
			echo "<ul>";
			echo "<li><a href=\"visitors.php?id=$id_topic\">" . $hh->tr->Translate("list") . " - " . $hh->tr->Translate("search") . "</a> ($nump)</li>\n";
			echo "<li><a href=\"mailjob.php?id_topic=$id_topic&act=filter\">" . $hh->tr->Translate("mailjob") . "</a>";
			include_once(SERVER_ROOT."/../classes/mailjobs.php");
			$mj = new Mailjobs();
			$mjrow = array();
			$num_mj = $mj->MailJobsAll($row ,$id_topic);
			if($num_mj>0)
			{
				echo" / <a href=\"mailjobs.php?id=$id_topic\">" . $hh->tr->Translate("archive") . "</a>\n";
			}

			echo "</li>\n";
			if($conf->Get("track") || $conf->Get("track_all"))
			{
				echo "<li><a href=\"visits.php?id=$id_topic\">" . $hh->tr->Translate("visits") . "</a></li>\n";
			}
			echo "<li><a href=\"visitors_groups.php?id=$id_topic\">" . $hh->tr->Translate("groups") . "</a></li>\n";
			if($module_admin || Modules::AmIAdmin(28))
				echo "<li><a href=\"visitors_import.php?id=$id_topic\">" . $hh->tr->Translate("import") . "</a></li>\n";
			echo "</ul>";
		}
		if($t->HasFunding() || $t->HasSubtopicTypeItem($t->subtopic_types['module'],27))
		{
			$trm15 = new Translator($hh->tr->id_language,15);
			echo "<h3>" . $hh->tr->Translate("funding") . "</h3>\n";
			echo "<ul>";
			echo "<li><a href=\"accounts.php?id=$id_topic\">" . $hh->tr->Translate("accounts") . "</a></li>\n";
			echo "<li><a href=\"payment_types.php?id=$id_topic\">" . $trm15->Translate("payment_types") . "</a></li>\n";
			echo "</ul>";
		}
	}

	if ($id_topic!=$tt->temp_id_topic)
	{
		echo "<h3>" . $hh->tr->Translate("publishing") . "</h3>";
		echo "<ul>";
		echo "<li><a href=\"publish.php?id=$id_topic\"><b>" . $hh->tr->Translate("publish") . "</b></a></li>\n";
		if ($input_super_right==1)
		{
			echo "<li><a href=\"publish_queue.php?id=$id_topic\">" . $hh->tr->Translate("publishing_queue") . "</a></li>\n";
			echo "<li><a href=\"publish.php?id=$id_topic&type=all\">" . $hh->tr->Translate("publish_all") . "</a></li>\n";
		}
		echo "<li><a href=\"publish_log.php?id=$id_topic\">" . $hh->tr->Translate("publishing_log") . "</a></li>\n";
		echo "</ul>";
	}
	echo "</div>\n";
}
else
	$t->only_visible = 1;

echo "<div class=\"box\">\n";
echo "<h2>" . $hh->tr->Translate("contents") . "</h2>\n";
echo "<h3>" . $hh->tr->Translate("articles") . "</h3>\n";
echo "<ul>";
$num = $t->ArticlesApproved( $row, 1 );
echo "<li><a href=\"articles.php?id=$id_topic\">" . $hh->tr->Translate("approveds") . "</a> ($num)</li>\n";
$num2 = $t->ArticlesApproved( $row, 0 );
if ($user_right && $num2>0)
	echo "<li><a href=\"articles.php?id=$id_topic&appr=0\" class=\"to-approve\">" . $hh->tr->Translate("to_approve") . "</a> ($num2)</li>\n";
$num3 = $t->ArticlesApproved( $row, -1 );
if ($num3>0)
	echo "<li><a href=\"articles.php?id=$id_topic&appr=-1\">" . $hh->tr->Translate("at_work") . "</a> ($num3)</li>\n";
if ($user_right==1)
{
	echo "<li><a href=\"/articles/article.php?w=topics&id=0&id_topic=$id_topic\">" . $hh->tr->Translate("article_add") . "</a></li>\n";
	echo "<li><a href=\"articles_search.php?id_topic=$id_topic\">" . $hh->tr->Translate("search") . "</a></li>\n";
}

$co = new Comments("article",0);
$co_approved = $co->CommentsPending(1,$t->id);
$co_to_approve = $co->CommentsPending(0,$t->id);
if($t->row['comments']>0 || $co_approved>0 || $co_to_approve>0)
{
	echo "<li><h4>" . $hh->tr->Translate("comments") . "</h4>\n";
	echo "<ul>";
	echo "<li><a href=\"comments_topic.php?id=$id_topic\">" . $hh->tr->Translate("approveds") . "</a> (" . $co_approved . ")</li>\n";
	if($co_to_approve>0)
		echo "<li><a href=\"comments_topic.php?id=$id_topic&appr=0\" class=\"to-approve\">" . $hh->tr->Translate("to_approve") . "</a> ($co_to_approve)</li>\n";
	echo "</ul>";
	echo "</li>\n";
}
echo "</ul>";

$templates = $t->Templates(5);
foreach($templates as $template)
{
	echo "<h3>{$template['name']}</h3>\n";
	echo "<ul>";
	$num = $t->ArticlesApproved( $row, 1, $template['id_template'] );
	if($num>0)
		echo "<li><a href=\"articles.php?id=$id_topic&id_template={$template['id_template']}\">" . $hh->tr->Translate("approveds") . "</a> ($num)</li>\n";
	$num2 = $t->ArticlesApproved( $row, 0, $template['id_template'] );
	if ($user_right && $num2>0)
		echo "<li><a href=\"articles.php?id=$id_topic&appr=0&id_template={$template['id_template']}\" class=\"to-approve\">" . $hh->tr->Translate("to_approve") . "</a> ($num2)</li>\n";
	$num3 = $t->ArticlesApproved( $row, -1, $template['id_template'] );
	if ($num3>0)
		echo "<li><a href=\"articles.php?id=$id_topic&appr=-1&id_template={$template['id_template']}\">" . $hh->tr->Translate("at_work") . "</a> ($num3)</li>\n";
	if ($user_right==1)
		echo "<li><a href=\"/articles/article.php?w=topics&id=0&id_topic=$id_topic&id_template={$template['id_template']}&id_subtopic={$template['default_subtopic']}\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
	echo "</ul>";
}


if ($t->HasSubtopicType($t->subtopic_types['links'])>0)
{
	include_once(SERVER_ROOT."/../classes/links.php");
	$links = new Links;
	echo "<h3>Links</h3>\n";
	echo "<ul>";
	$tot_links = $links->CountTopicApproved($id_topic,1);
	echo "<li><a href=\"links.php?id=$id_topic\">" . $hh->tr->Translate("approveds") . "</a> ($tot_links)</li>\n";
	$tot_pending = $links->CountTopicApproved($id_topic,0);
	if ($input_right==1)
	{
		if($tot_pending>0)
			echo "<li><a href=\"links.php?id=$id_topic&appr=0\" class=\"to-approve\">" . $hh->tr->Translate("to_approve") . "</a> ($tot_pending)</li>\n";
		$tot_errors = $links->CountTopicError($id_topic);
		if ($tot_errors>0)
			echo "<li><a href=\"links.php?id=$id_topic&error=1\">" . $hh->tr->Translate("to_check") . "</a> ($tot_errors)</li>\n";
	}
	elseif($tot_pending>0)
		echo "<li>" . $hh->tr->Translate("to_approve") . " ($tot_pending)</li>\n";
	if ($user_right==1)
	{
		echo "<li><a href=\"link.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("link_add") . "</a></li>\n";
		echo "<li><a href=\"links_search.php?id_topic=$id_topic\">" . $hh->tr->Translate("search") . "</a></li>\n";
	}
	if ($input_super_right==1)
		echo "<li><a href=\"links_check.php?id_topic=$id_topic\">" . $hh->tr->Translate("check") . "</a></li>\n";
	echo "</ul>";
}

if ($t->HasSubtopicType($t->subtopic_types['calendar'])>0)
{
	echo "<h3>" . $hh->tr->Translate("calendar") . "</h3>\n";
	echo "<ul>";
	$num_events1 = count($t->Events());
	if ($num_events1 > 0)
		echo "<li><a href=\"events.php?id_topic=$id_topic\">" . $hh->tr->Translate("events_approved") . "</a> ($num_events1)</li>\n";
	if ($user_right==1)
	{
		$num_events2 = count($t->Events(0));
		if ($num_events2 > 0)
			echo "<li><a href=\"events.php?id_topic=$id_topic&approved=0\" class=\"to-approve\">" . $hh->tr->Translate("events_to_approve") . "</a> ($num_events2)</li>\n";
		echo "<li><a href=\"event.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("event_add") . "</a></li>\n";
	}
	echo "</ul>";
}

if($t->HasSubtopicTypeItem($t->subtopic_types['module'],16))
{
	include_once(SERVER_ROOT."/../modules/books.php");
	$bb = new Books();
	$row = array();
	$num_reviews1 = $bb->Reviews( $row, 1, $id_topic );
	$row = array();
	$num_reviews2 = $bb->Reviews( $row, 0, $id_topic );
	$trm16 = new Translator($hh->tr->id_language,16);
	echo "<h3>" . $hh->tr->Translate("reviews") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"books_reviews.php?approved=1&id_topic=$id_topic\">" . $trm16->Translate("reviews_approved") . "</a> ($num_reviews1)</li>\n";
	if ($num_reviews2 > 0 && $user_right==1)
		echo "<li><a href=\"books_reviews.php?approved=0&id_topic=$id_topic\" class=\"to-approve\">" . $trm16->Translate("reviews_to_approve") . "</a> ($num_reviews2)</li>\n";
	echo "<li><a href=\"books_review.php?id=0&id_topic=$id_topic\">" . $trm16->Translate("review_add") . "</a></li>\n";
	echo "</ul>";
}


if ($input_right)
{
	$threads = $t->PendingThreads();
	$cof = new Comments("thread",0);
	$cof_pending = $cof->CommentsPending(0,$id_topic);
	if(count($threads)>0 || $cof_pending>0)
	{
		echo "<h3>" . $hh->tr->Translate("forum") . "</h3>\n";
		echo "<ul>";
		if (count($threads)>0 )
		{
			echo "<li>" . $hh->tr->Translate("threads_to_approve");
			echo "<ul>";
			foreach($threads as $thread)
				echo "<li><a href=\"forum_threads.php?id={$thread['id_topic_forum']}&id_topic=$id_topic&approved=0\" class=\"to-approve\">{$thread['name']}</a> ({$thread['counter']})</li>\n";
			echo "</ul>";
			echo "</li>";
		}
		if($cof_pending>0)
			echo "<li><a href=\"comments_forums.php?id=$id_topic\" class=\"to-approve\">" . $hh->tr->Translate("comments_to_approve") . " ($cof_pending)</a></li>\n";
		echo "</ul>";
	}

	$persons = $t->PendingCampaignPersons();
	$orgs = $t->PendingCampaignOrgs();
	if (count($persons)>0 || count($orgs)>0)
	{
		echo "<h3>" . $hh->tr->Translate("campaigns") . "</h3>\n";
		echo "<ul>";
		if(count($persons)>0)
		{
			echo "<li>" . $hh->tr->Translate("comments_to_approve");
			echo "<ul>";
			foreach($persons as $person)
				echo "<li><a href=\"campaign_persons.php?id={$person['id_topic_campaign']}&id_topic=$id_topic&appr=0\" class=\"to-approve\">{$person['name']}</a> ({$person['counter']})</li>\n";
			echo "</ul>";
			echo "</li>";
		}
		if(count($orgs)>0)
		{
			echo "<li>" . $hh->tr->Translate("comments_to_approve");
			echo "<ul>";
			foreach($orgs as $org)
				echo "<li><a href=\"campaign_orgs.php?id={$org['id_topic_campaign']}&id_topic=$id_topic&appr=0\" class=\"to-approve\">{$org['name']}</a> ({$org['counter']})</li>\n";
			echo "</ul>";
			echo "</li>";
		}
		echo "</ul>";
	}

	$coq = new Comments("question",0);
	$coq_pending = $coq->CommentsPending(0,$id_topic);
	if($coq_pending>0)
	{
		echo "<h3>" . $hh->tr->Translate("polls") . "</h3>\n";
		echo "<ul>";
		echo "<li><a href=\"comments_polls.php?id=$id_topic\" class=\"to-approve\">" . $hh->tr->Translate("comments_to_approve") . " ($coq_pending)</a></li>\n";
		echo "</ul>";
		
	}
}

if (Modules::IsActive(6) && $t->HasSubtopicType($t->subtopic_types['quotes'])>0)
{
	include_once(SERVER_ROOT."/../classes/quotes.php");
	$qu = new Quotes;
	$row = array();
	$num_quotes1 = $qu->QuotesApproved( $row, $id_topic, 0 );
	$row = array();
	$num_quotes2 = $qu->QuotesApproved( $row, $id_topic, 1);
	echo "<h3>" . $hh->tr->Translate("quotes") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"quotes.php?approved=1&id_topic=$id_topic\">" . $hh->tr->Translate("quotes_approved") . "</a> ($num_quotes2)</li>\n";
	if ($num_quotes1 > 0 && $user_right==1)
		echo "<li><a href=\"quotes.php?approved=0&id_topic=$id_topic\" class=\"to-approve\">" . $hh->tr->Translate("quotes_to_approve") . "</a> ($num_quotes1)</li>\n";
	echo "<li><a href=\"quote.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("quotes_new") . "</a></li>\n";
	echo "</ul>";
}

echo "<h3>" . $hh->tr->Translate("images") . "</h3>\n";
echo "<ul>";
echo "<li><a href=\"images.php?id=$id_topic\">" . $hh->tr->Translate("images_articles") . "</a></li>\n";
echo "</ul>";

echo "<h3>" . $hh->tr->Translate("documents") . "</h3>\n";
echo "<ul>";
echo "<li><a href=\"docs.php?id=$id_topic\">" . $hh->tr->Translate("docs_articles") . "</a></li>\n";
echo "</ul>";

if ($input_right==1 && $id_topic!=$tt->temp_id_topic)
{
	echo "<h3>" . $hh->tr->Translate("features") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"features.php?id=$id_topic\">" . $hh->tr->Translate("features") . "</a></li>\n";
	echo "</ul>";
	echo "<h3>" . $hh->tr->Translate("texts") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"texts.php?id=$id_topic\">" . $hh->tr->Translate("texts") . "</a></li>\n";
	echo "</ul>";
}

if ($conf->Get("awstats")!="" && $t->row['awstats']!="")
{
	echo "<h3>" . $hh->tr->Translate("stats") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"awstats.php?id=$id_topic\" target=\"stats\">" . $hh->tr->Translate("webstats") . "</a></li>\n";
	echo "</ul>";
}

echo "</div>\n";
echo "<div class=\"box\">\n";
echo "<h2>" . $hh->tr->Translate("users") . "</h2>\n";

$contacts = $t->Contacts();
echo "<h3>" . $hh->Wrap("contact_main","<a href=\"topic_add.php?id=$id_topic&g=contact\">","</a>",$input_right==1) . "</h3>\n";
echo "<ul>";
foreach($contacts as $contact)
	echo "<li><a href=\"/users/user.php?id={$contact['id_user']}\">{$contact['name']}</a></li>\n";
echo "</ul>";

$admins = $t->Admins();
echo "<h3>" . $hh->Wrap("administrators","<a href=\"topic_add.php?id=$id_topic&g=admin\">","</a>",$input_right==1) . "</h3>\n";
echo "<ul>";
foreach($admins as $admin)
{
	echo "<li><a href=\"/users/user.php?id={$admin['id_user']}\">{$admin['name']}</a>";
	if($admin['id_user']==$ah->current_user_id)
		echo "<div class=\"notes\">[<a href=\"/gate/user_topic.php?id=$id_topic\">" . $hh->tr->Translate("settings") . "</a>]</div>";
	echo "</li>\n";	
}
echo "</ul>";

$users = $t->Admins(0);
echo "<h3>" . $hh->Wrap("collaborators","<a href=\"topic_add.php?id=$id_topic&g=user\">","</a>",$input_right==1) . "</h3>\n";
echo "<ul>";
foreach($users as $user)
	echo "<li><a href=\"/users/user.php?id={$user['id_user']}\">{$user['name']}</a></li>\n";
echo "</ul>";

echo "</div>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

