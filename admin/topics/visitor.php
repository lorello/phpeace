<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];
$id_p = $_GET['id_p'];

$t = new Topic($id_topic);
$row = $t->row;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('visitors','visitors.php?id='.$id_topic.'&p='.$current_page);

if($t->profiling)
{
	$pe = new People();
	
	if($module_admin || $t->AmIAdmin()) 
		$input_super_right = 1;
	
	$row = $pe->UserGetDetailsById($id_p);
	
	$title[] = array($row['name1'] . " " . $row['name2'],'');
	
	echo $hh->ShowTitle($title);
	
	echo $hh->input_form_open();
	echo $hh->input_table_open();
	echo $hh->input_hidden("from","visitor");
	echo $hh->input_hidden("id_topic",$id_topic);
	echo $hh->input_hidden("id_p",$id_p);
	echo $hh->input_text("name","name1",$row['name1'],30,0,$input_right);
	echo $hh->input_text("name2","name2",$row['name2'],30,0,$input_right);
	echo $hh->input_text("email","email",$ah->ModuleAdmin(28)? "<a href=\"/people/person.php?id=$id_p\">{$row['email']}</a>" : $row['email'],50,0,$input_right);
	echo $hh->input_textarea("address","address",$row['address'],80,4,"",$input_right);
	echo $hh->input_textarea("address_notes","address_notes",$row['address_notes'],80,4,"",$input_right);
	echo $hh->input_text("postcode","postcode",$row['postcode'],15,0,$input_right);
	echo $hh->input_text("town","town",$row['town'],50,0,$input_right);
	echo $hh->input_geo($row['id_geo'],$input_right);
	echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);

	if($id_p>0)
	{
		$params = $pe->UserParamsGet($id_p,true);
		if(count($params)>0)
		{
			foreach($params as $key=>$value)
			{
				if($key=="salesforce_id")
				{
					$key = "salesforce";
					$value = "<a href=\"" . SALESFORCE_URL . "$value\" target=\"_blank\">$value</a>";
				}
				echo $hh->input_text($key,"param_$key",$value,10,0,0);
			}
		}
		$tgroups = array();
		$num_tgroups = $t->PeopleGroups($tgroups);
		if($num_tgroups>0)
		{
			$pgroups = $pe->Groups($id_p,false,$id_topic);
			echo $hh->input_text("groups","visitor_groups",$hh->th->ArrayMulti2StringIndex($pgroups,'name'),30,0,0,$hh->Wrap("change","<a href=\"visitor_groups.php?id_p=$id_p&id_topic=$id_topic\">","</a>",$input_super_right));
		}
		$tperson = $t->PersonGet($id_p);
		echo $hh->input_checkbox("contact_email","contact",$tperson['contact'],0,$input_super_right);
		if($t->row['protected'] == "2")
			echo $hh->input_checkbox("access","access",$tperson['access'],0,$input_super_right);
		echo $hh->input_textarea("user_notes","admin_notes",$tperson['admin_notes'],50,4,"",$input_super_right);
		
		$actions = array();
		$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_super_right);
		$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_super_right && $id_p>0);
		echo $hh->input_actions($actions,$input_super_right);
		echo $hh->input_table_close() . $hh->input_form_close();
	
		include_once(SERVER_ROOT."/../classes/layout.php");
		$l = new Layout(false,false,false);
		$l->TopicInit($id_topic);
		$l->TranslatorInit($hh->tr->id_language,0);
		$history = $l->PeopleHistory($id_p,$id_topic);
		$hhf = new HHFunctions();
		
		$history_counter = 0;
		$history_output = "<h2>" . $hh->tr->Translate("history") . "</h2>";
		if(is_array($history['campaigns']) && count($history['campaigns'])>0)
		{
			$history_counter += count($history['campaigns']);
			$history_output .= "<h3>" . $hh->tr->Translate("campaigns") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['campaigns'] as $campaign)
				$history_output .= "<li>" . $hhf->LinkTitle("/topics/campaign.php?id={$campaign['id']}&id_topic=$id_topic",$campaign['hdate']) . ": <b>{$campaign['title']}</b></li>";
			$history_output .= "</ul>";
		}
		unset($history['signatures']['label']);
		if(is_array($history['signatures']) && count($history['signatures'])>0)
		{
			$history_counter += count($history['signatures']);
			$history_output .= "<h3>" . $hh->tr->Translate("signatures") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['signatures'] as $signature)
				$history_output .= "<li>" . $hhf->LinkTitle("/topics/campaign_person.php?id={$signature['id_person']}&id_c={$signature['id']}&id_topic=$id_topic",$signature['hdate']) . ": <b>{$signature['title']}</b></li>";
			$history_output .= "</ul>";
		}
		unset($history['polls']['label']);
		if(is_array($history['polls']) && count($history['polls'])>0)
		{
			$history_counter += count($history['polls']);
			$history_output .= "<h3>" . $hh->tr->Translate("polls") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['polls'] as $poll)
				$history_output .= "<li>" . $hhf->LinkTitle("/topics/poll_question_answer.php?id_p={$poll['id_person']}&id_poll={$poll['id']}&id={$poll['id_question']}",$poll['hdate']) . ": <b>{$poll['title']}</b></li>";
			$history_output .= "</ul>";
		}
		unset($history['comments']['label']);
		if(is_array($history['comments']) && count($history['comments'])>0)
		{
			$history_counter += count($history['comments']);
			$history_output .= "<h3>" . $hh->tr->Translate("comments") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['comments'] as $comment)
				$history_output .= "<li>{$comment['hdate']}: {$comment['topic']['name']} - <b>{$comment['headline']['xvalue']}</b></li>";
			$history_output .= "</ul>";
		}
		if(is_array($history['events']) && count($history['events'])>0)
		{
			$history_counter += count($history['events']);
			$history_output .= "<h3>" . $hh->tr->Translate("events") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['events'] as $event)
				$history_output .= "<li>{$event['hdate']}: <b>{$event['title']}</b></li>";
			$history_output .= "</ul>";
		}
		unset($history['reviews']['label']);
		if(is_array($history['reviews']) && count($history['reviews'])>0)
		{
			$history_counter += count($history['reviews']);
			$history_output .= "<h3>" . $hh->tr->Translate("reviews") . "</h3>";
			$history_output .= "<ul>";
			foreach($history['reviews'] as $review)
				$history_output .= "<li>{$review['hdate']}: <b>{$review['book']['title']}</b></li>";
			$history_output .= "</ul>";
		}
		if(is_array($history['mailjobs']) && count($history['mailjobs'])>0)
		{
			$history_counter += count($history['mailjobs']);
			$history_output .= "<h3>" . $hh->tr->Translate("mailjobs") . "</h3>";
			$history_output .= "<ul>";
			$statuses = $hh->tr->Translate("mailjob_status");
			foreach($history['mailjobs'] as $mailjob)
			{
				$history_output .= "<li>{$mailjob['hdate']}: <b>{$mailjob['title']}</b> (" . $statuses[$mailjob['status']] . ")</li>";
			}
			$history_output .= "</ul>";
		}
		
		
		if($history_counter>0)
			echo $history_output;

	
		if($input_super_right && $conf->Get("track") || $conf->Get("track_all"))
		{
			include_once(SERVER_ROOT."/../classes/tracker.php");
			$tk = new Tracker();

			$num = $tk->Visits( $row, $id_p);
			$qs = "&id_p=$id_p&id_topic=$id_topic";
			$table_headers = array('date','ip','pages','length','cookie');
			$table_content = array('{FormatDateTime($row[visit_date_ts])}','$row[ip]',
			'{LinkTitle("visitor_visit.php?id=$row[id_visit]'.$qs.'","$row[pages]",0,"",true,"right")}','<div class=\"right\">$row[length]</div>','{Bool2YN($row[cookie])}');
			echo "<h3>" . $hh->tr->Translate("visits") . "</h3>";
			echo $hh->ShowTable($row, $table_headers, $table_content, $num);

		}
	}
}


include_once(SERVER_ROOT."/include/footer.php");
?>

