<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");

$id_topic = $_GET['id_topic'];
$id_campaign = $_GET['id_campaign'];
$id_use = (int)$_GET['id'];

$t = new Topic($id_topic);

$c = new Campaign($id_campaign);
$campaign = $c->CampaignGet();
if($id_use>0)
{
	$row = $c->AccountGet($id_use);
}
else 
{
	$row = array();
}

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);
$title[] = array($campaign['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);
$title[] = array("account",'');
echo $hh->ShowTitle($title);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

echo $hh->input_form_open();
echo $hh->input_hidden("from","campaign_account");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_campaign",$id_campaign);
echo $hh->input_hidden("id_use",$id_use);
echo $hh->input_table_open();

if($id_use>0)
{
	$accounts = array();
	$t->Accounts( $accounts, false );
}
else 
{
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$accounts = $p->AccountsUseAvailable(1,$c->id,$id_topic);
}

echo $hh->input_row("account","id_account",$row['id_account'],$accounts,"",0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_use>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
