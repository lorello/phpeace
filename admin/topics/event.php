<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/event.php");
include_once(SERVER_ROOT."/../classes/events.php");
include_once(SERVER_ROOT."/../classes/geo.php");

$ee = new Events();

$id = $_GET['id'];
$id_topic = $_GET['id_topic'];

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
	{
		$input_right = 1;
		$input_super_right = 1;
	}
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
}


if ($id>0)
{
	$action2 = "update";
	$event = new Event();
	$row = $event->EventGet($id);
	$approved = $row['approved'];
	$title[] = array((($approved==1)? "events_approved" : "events_to_approve"),'events.php?approved='.$approved.'&id_topic='.$id_topic);
	$title[] = array($row['title'],'');
	$event_topic = $row['id_topic'];
	$visible_portal = $row['portal'];
	$article_title = htmlspecialchars($row['headline']);
	if ($ah->current_user_id==($event->CreatorId($id)))
		$input_right = 1;
}
else
{
	$action2 = "insert";
	$title[] = array('event_add','');
	$event_topic = $id_topic;
	$visible_portal = 1;
	if ($module_admin)
		$approved = 1;
	if ($id_topic>0 && $t->AmIUser())
		$input_right = 1;
	$article_title = "";
}

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">

function doaction(action)
{
	f = document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.start_date_h.value=="" && !(f.allday.checked))
	{
		strAlert += "- " + "<?=$hh->tr->Translate("hour");?> <?=$hh->tr->Translate("or");?> <?=$hh->tr->Translate("all_day_long");?>\n";
		boolOK = false;
	}
	if (f.id_event_type.selectedIndex==0)
	{
		strAlert += "- " + "<?=$hh->tr->Translate("event_type");?>\n";
		boolOK = false;
	}
	if (f.title.value=="")
	{
		strAlert += "- " + "<?=$hh->tr->Translate("title");?>\n";
		boolOK = false;
	}
	if (f.place.value=="")
	{
		strAlert += "- " + "<?=$hh->tr->Translate("town");?>\n";
		boolOK = false;
	}
	if (action=="delete")
	{
		boolOK = true;
	}
	if (boolOK==true)
	{
		f.action2.value = action;
		f.submit();
	}
	else
		alert("<?=$hh->tr->Translate("mandatory_fields");?>\n" + strAlert);
}

function reconcile_dates()
{
	f = document.forms['form1'];
	var s_day	= f.start_date_d.selectedIndex
	var s_month	= f.start_date_m.selectedIndex
	var e_day	= f.end_date_d.selectedIndex
	var e_month	= f.end_date_m.selectedIndex
	if ( e_month < s_month )
		f.end_date_m.selectedIndex = f.start_date_m.selectedIndex;
	if ( (e_day < s_day) && (e_month == s_month) )
		f.end_date_d.selectedIndex = f.start_date_d.selectedIndex;
}

</script>
<?php
if ($id>0 && $input_right)
{
	$tools[] = array('history','history.php?id_type=8&id_topic='.$id_topic.'&id='.$id);
	$tools[] = array('preview','/topics/preview.php?id_type=5&subtype=event&id_topic='.$id_topic.'&id='.$id);
	if (Modules::AmIAdmin(10))
		$tools[] = array('XML','/layout/xml.php?id_type=5&subtype=event&id_topic='.$id_topic.'&id='.$id);
	echo $hh->Toolbar($tools);
}

?>

<form name="form1" action="/events/actions.php" method="post">
<input type="hidden" name="from" value="event">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="id_event" value="<?=$id;?>">
<input type="hidden" name="module" value="<?=$ah->session->Get("module_path");?>">
<input type="hidden" name="approved_old" value="<?=$row['approved'];?>">
<table border=0 cellpadding=2 cellspacing=2>

<?php
echo $hh->input_separator("when");
echo $hh->input_note("event_note");
$hh->mandatory = TRUE;
echo $hh->InputDateEvent("Date","start_date",$row['start_date_ts'],$input_right," onChange=\"reconcile_dates()\"",1);
echo $hh->input_time("hour","start_date",$row['start_date_ts'],$input_right);
$hh->mandatory = FALSE;
echo $hh->input_text("event_length","length",$row['length'],5,0,$input_right);
echo $hh->input_checkbox("all_day_long","allday",$row['allday'],0,$input_right);
echo $hh->InputDateEvent("date_end","end_date",$row['end_date_ts'],$input_right,"",0);

echo $hh->input_separator("what");
$hh->mandatory = TRUE;
echo $hh->input_row("event_type","id_event_type",$row['id_event_type'],$ee->Types(),"choose_option",0,$input_right);
echo $hh->input_textarea("title","title",$row['title'],80,2,"",$input_right);
$hh->mandatory = FALSE;
echo $hh->input_textarea("description","description",$row['description'],80,10,"",$input_right);

echo $hh->input_separator("where");
$hh->mandatory = TRUE;
echo $hh->input_textarea("town","place",$row['place'],80,2,"",$input_right);
$hh->mandatory = FALSE;
echo $hh->input_geo($row['id_geo'],$input_right);
echo $hh->input_textarea("address_notes","place_details",$row['place_details'],80,3,"",$input_right);

echo $hh->input_separator("who");
echo $hh->input_text("contact_main","contact_name",$row['contact_name'],50,0,$input_right);
echo $hh->input_text("email_one","email",$row['email'],50,0,$input_right);
echo $hh->input_text("phone","phone",$row['phone'],50,0,$input_right);

echo $hh->input_separator("more_info");
echo $hh->input_text("link_one","link",$row['link'],50,0,$input_right);

if ($row['id_article']>0)
{
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($row['id_article']);
	$a->ArticleLoad();
	$headline = $a->headline;
	$article_topic = $a->id_topic;
}
else 
{
	$headline = "";
	$article_topic = 0;
}

echo $hh->input_article("article_related","id_article","'/articles/popup_search.php?w=event&id_article='+document.forms['form1'].id_article.value+'&id_topic=$article_topic'",$row['id_article'],$headline,$article_topic,$input_right);
echo $hh->input_checkbox("jump_to_article","jump_to_article",$row['jump_to_article'],0,$input_right);

echo $hh->input_separator("administration");
if($id>0)
{
	$insert_date_ts = $event->CreatorTime($id);
	if($insert_date_ts>0)
	{
		echo $hh->input_note($hh->tr->Translate("date_insert") . ": " . $hh->FormatDateTime($insert_date_ts));
	}
}

echo $hh->input_topics($event_topic,$row['id_group'],$ee->Topics(),"none_option",$input_super_right,false,"id_topic");
echo $hh->input_checkbox("visible_topic_group","visible_topic_group",$row['id_group']>0,0,$input_super_right);
echo $hh->input_checkbox("visible_portal","portal",$visible_portal,0,$input_super_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
if ($id_topic>0)
	$o->GetKeywords($event_topic,$o->types['topic'], $keywords,0,true);
echo $hh->input_keywords($id,$o->types['event'],$keywords,$input_right);

if($row['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe =new People();
	$user = $pe->UserGetById($row['id_p']);
	echo $hh->input_text("submitted_by","person", "{$user['name1']} {$user['name2']}",50,0,0);
}

echo $hh->input_checkbox("approved","approved",$approved,0,$input_super_right);

if ($input_right==1)
{
	echo "<tr><td>&nbsp;</td><td>";
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") ."\" onClick=\"doaction('$action2')\">";
	if ($id>0)
		echo "&nbsp;<input type=\"button\" value=\"" . $hh->tr->Translate("delete") ."\" onClick=\"doaction('delete')\">";
	echo "</td></tr>\n";
}
?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


