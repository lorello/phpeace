<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");

$id_topic = $_GET['id_topic'];
$id_campaign = $_GET['id'];
$appr = $_GET['appr'];

if ($appr=="")
	$appr = 1;

$t = new Topic($id_topic);

$c = new Campaign($id_campaign);
$row = $c->CampaignGet();

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);
$title[] = array($row['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);
$title[] = array((($appr=="1")?"approveds":"to_approve"),'');
echo $hh->ShowTitle($title);

$num = $c->PersonsApprove( $row, $appr );

$table_headers = array('date','name','email','comments');
$table_content = array('{FormatDate($row[insert_date_ts])}','{LinkTitle("campaign_person.php?id=$row[id_person]&id_c='.$id_campaign.'&id_topic='.$id_topic.'&p='.$current_page.'&appr='.$appr.'",$row[name])}',
'{ContactEmail($row[email],$row[contact])}','$row[comments]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

