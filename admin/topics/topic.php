<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id = $_GET['id'];

if ($id>0)
{
	$action2 = "update";
	$t = new Topic($id);
	$row = $t->row;
	$home_type = $row['home_type'];
	$title[] = array($t->name,'ops.php?id='.$id);
	$title[] = array('settings_main','');
	$id_language = $row['id_language'];
	$share_images = $row['share_images'];
	$share_docs = $row['share_docs'];
	$visible = $row['visible'];
	$show_latest = $row['show_latest'];
	$id_group = $row['id_group'];
	$group_name = $row['group_name'];
}
else
{
	$action2 = "insert";
	$title[] = array('topic_new','');
	$home_type = 1;
	$id_language = $hh->ini->Get("id_language");
	$share_images = 1;
	$share_docs = 1;
	$visible = 1;
	$show_latest = 1;
	$id_group = 0;
	$group_name = "";
	include_once(SERVER_ROOT."/../classes/topics.php");
	$tt = new Topics();
	$topic_groups = $tt->gh->GroupsAll();
	if(count($topic_groups)==1)
	{
		$id_group = $topic_groups[0]['id_group'];
		$group = $tt->gh->GroupGet($id_group);
		$group_name = $group['name'];
	}
}

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required",
			id_group: {
				required: true,
				min: 1
			}
<?php if ($input_super_right==1) { ?>
			,path: "required"
<?php } ?>
		}
	});
});
</script>
<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","topic");
echo $hh->input_hidden("id_topic",$id);
echo $hh->input_table_open();

echo $hh->input_link("placing","id_group","'topic_group.php?&id_group='+document.forms['form1'].id_group.value+'&id_topic='+document.forms['form1'].id_topic.value",$id_group,$group_name,$input_right);

if($id>0)
	echo $hh->input_text("ID","ID",$id,5,0,0);

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,3,"",$input_right);
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['topic'],$keywords,$input_right);
echo $hh->input_textarea("topic_home_keywords","home_keywords",$row['home_keywords'],60,2,"",$input_right);
echo $hh->input_checkbox("sharing_images","share_images",$share_images,0,$input_right);
echo $hh->input_checkbox("sharing_docs","share_docs",$share_docs,0,$input_right);
echo $hh->input_array("allow_comments","comments",($row['comments']),$hh->tr->Translate("allow_feedback"),$input_right);
echo $hh->input_checkbox("moderate_comments","moderate_comments",$row['moderate_comments'],0,$input_right);
echo $hh->input_array("campaigns_open","campaigns_open",$row['campaigns_open'],$hh->tr->Translate("profiling_options"),$input_right);
echo $hh->input_text("topic_email","temail",$row['temail'],40,0,$input_right);
echo $hh->input_array("language","id_language",$id_language,$phpeace->Languages($hh->tr->id_language),$input_right);

echo $hh->input_separator("administration");
echo $hh->input_checkbox("layout_by_admins","edit_layout",$row['edit_layout'],0,$input_super_right);

if($id>0)
{
	$ikeywords = $t->KeywordsInternal(0);
	echo $hh->input_text("keywords_internal","keywords_internal",$hh->th->ArrayMulti2StringIndex($ikeywords,'keyword'),30,0,0,$hh->Wrap("change","<a href=\"topic_keywords.php?id=$id\">","</a>",$input_super_right));
}

echo $hh->input_text("domain","domain",$row['domain'],30,0,$input_super_right,$hh->tr->TranslateParams("domain_different",array($hh->ini->Get("pub_web"))));
echo $hh->input_text("path","path",$row['path'],30,0,$input_super_right);

$furl_options = $hh->tr->Translate("furl_options");
echo $hh->input_array("Friendly URLs","friendly_url",$row['friendly_url'],$furl_options,$input_super_right && $conf->Get("htaccess"));

echo $hh->input_checkbox("profiling_topic","profiling",$row['profiling'],0,$input_super_right && $ini->Get("profiling"));
echo $hh->input_checkbox("visible_online","visible",$visible,0,$input_super_right);
echo $hh->input_checkbox("visible_latest","show_latest",$show_latest,0,$input_super_right);
echo $hh->input_checkbox("archive","archived",$row['archived'],0,$input_super_right);
$access_options = $hh->tr->Translate("access_options");
if(!$conf->Get("htaccess"))
	unset($access_options[1]);
echo $hh->input_array("access","protected",$row['protected'],$access_options,$input_super_right);
echo $hh->input_checkbox("custom_visibility","custom_visibility",$row['custom_visibility'],0,$input_super_right);
echo $hh->input_text("awstats","awstats",$row['awstats'],30,0,$input_super_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$module_admin);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

