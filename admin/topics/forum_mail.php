<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/forum.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");
include_once(SERVER_ROOT."/../classes/topic.php");
$post = $fh->HttpPost();

$id_module = 2; // Have to use this (mail' id) to identify forums, which do not have a specific module

$mj = new Mailjobs($id_module);
$recipients = $mj->RecipientsGet();
$num_recipients = sizeof($recipients);

$id_item = $_GET['id_item'];

$f = new Forum($id_item);
$forum = $f->ForumGet();
$id_topic = $forum['id_topic'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','forums.php?id='.$id_topic);
$title[] = array($forum['name'],'forum.php?id='.$id_item.'&id_topic='.$id_topic);
$title[] = array('mailjob','');

$act = $_GET['act'];  // type, filter, search, showdest, message, send, reset

if ($module_admin || $t->AmIAdmin())
{
	if($id_item>0 && $mj->MailJobCheck($id_module,$id_item))
	{
		$ah->MessageSet("mailjob_already_present");
		echo $hh->ShowTitle($title);
	}
	else
	{
		echo $hh->ShowTitle($title);
		switch($act)
		{
			case "filter":
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . "</p>";
					echo "<h4><a href=\"forum_mail.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
					echo "<p>" . $hh->tr->Translate("mailjob_filter_again") . "</p>";
				}
				else
					echo "<p>" . $hh->tr->Translate("mailjob_filter") . "</p>";
				echo $hh->input_form("post","forum_mail.php?id_item=$id_item&act=search");
				echo $hh->input_table_open();
				echo $hh->input_text("name","name","",30,0,1);
				echo $hh->input_text("email","email","",30,0,1);
				echo $hh->input_geo(0,1);
				$actions[] = array('action'=>"search",'label'=>"search",'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "search":
				$db =& Db::globaldb();
				$search_params = array();
				$search_params['name'] = $post['name'];
				$search_params['email'] = $post['email'];
				$search_params['id_geo'] = $post['id_geo'];
				$num = $f->MailjobSearch($id_topic,$search_params);
				if($num>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num)) . " <a href=\"forum_mail.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4><a href=\"forum_mail.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
					echo "<p><a href=\"forum_mail.php?act=showdest&id_item=$id_item\">" . $hh->tr->Translate("mailjob_showdest") . "</a></p>";
				}
				else 
					echo "<p>" . $hh->tr->TranslateParams("num_results",array(0)) . "</p>";
					
			break;
			case "showdest":
				$delete = (int)$_GET['delete'];
				if($delete>0)
				{
					$mj->RecipientDelete($delete);
					$recipients = $mj->RecipientsGet();
					$num_recipients = sizeof($recipients);
				}
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"forum_mail.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4><a href=\"forum_mail.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
				}
				$row = array_slice($recipients,($current_page-1)*$records_per_page,$records_per_page);
				$table_headers = array('name','email','');
				$table_content = array('$row[name]','$row[email]','{LinkTitle("forum_mail.php?act=showdest&id_item='.$id_item.'&delete=$row[id]","(delete)")}');
				echo $hh->ShowTable($row, $table_headers, $table_content, sizeof($recipients));
			break;
			case "message":
				if($num_recipients>0)
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"forum_mail.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
				?>
				<script type="text/javascript">
					$().ready(function() {
					$("#form1").validate({
							rules: {
								name: "required",
								subject: "required",
								body: "required",
								email: {
									required: true,
									email: true
								}
							}
						});
					});
				</script>
				<?php	
				echo $hh->input_form("post","forum_mail.php?act=send&id_item=$id_item");
				echo $hh->input_hidden("id_item",$id_item);
				echo $hh->input_table_open();
				include_once(SERVER_ROOT."/../classes/user.php");
				$u = new User();
				$user = $u->UserGet();
				echo $hh->Input_date("date","start_date",0,1);
				echo $hh->input_time("hour","start_date",0,1);
				echo $hh->input_separator("sender");
				echo $hh->input_text("name","name",$user['name'],30,0,1);
				echo $hh->input_text("email","email",$user['email'],30,0,1);
				echo $hh->input_separator("message");
				echo $hh->input_text("subject","subject","",50,0,1);
				echo $hh->input_textarea("text","body","",80,20,"",1);
				if($mj->ProfilingGet())
				{
					echo $hh->input_checkbox("send_password","send_password","0",0,0);
					echo $hh->input_checkbox("track","track","1",0,0);
					echo $hh->input_text("track_redirect","track_redirect","",30,0,1);
				}
				$actions[] = array('action'=>"send",'label'=>"send",'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "send":
				include_once(SERVER_ROOT."/../classes/formhelper.php");
				$fh = new FormHelper(true);
				$post = $fh->HttpPost();
				$start_date 	= $fh->Strings2Datetime($post['start_date_d'],$post['start_date_m'],$post['start_date_y'],$post['start_date_h'],$post['start_date_i']);
				$name		= $post['name'];
				$email		= $post['email'];
				$subject	= $post['subject'];
				$body		= $post['body'];
				$send_password	= $fh->Checkbox2bool($post['send_password']);
				$track		= $fh->Checkbox2bool($post['track']);
				$track_redirect		= $fh->String2Url($post['track_redirect']);
				$id_item	= $post['id_item'];
				$fh->va->NotEmpty($name,"name");
				$fh->va->Email($email);
				$fh->va->NotEmpty($subject,"title");
				$fh->va->NotEmpty($body,"text");
				if($fh->va->return)
				{
					$unescaped_footer = $mj->MessageFooter($id_topic,$send_password);
					$mj->Store($start_date,$name,$email,$ah->current_user_id,$subject,$body,$id_module,$id_item,$recipients,$unescaped_footer,$send_password,0,array(),$track,$track_redirect,$id_topic);
					echo "<p>" . $hh->tr->Translate("mailjob_queued") . "</p>";
				}
				else 
					echo $hh->MessageShow();
			break;
			case "reset":
				$mj->RecipientsDelete();
				echo "<p><a href=\"forum_mail.php\">" . $hh->tr->Translate("mailjob_reset") . "</a></p>";
			break;
		}
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
