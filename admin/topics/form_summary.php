<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");

$id_topic = $get['id_topic'];
$id_form = $get['id'];
$date = (int)$get['date'];
$by_day = $get['by_day'];

if ($module_admin)
    $input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

$period = $fo->PostsPeriod($id_form);
$start_date = !empty($get['start_date']) ? $get['start_date'] : date('Y-m-d', $period['min_post_ts']);
$end_date = !empty($get['end_date']) ? $get['end_date'] : date('Y-m-d', $period['max_post_ts']);


if ($id_topic>0)
{
    $t = new Topic($id_topic);
    if ($t->AmIAdmin())
        $input_right = 1;
    $title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('forms','forms.php?id='.$id_topic);
$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('reports','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
    $posts = array();
    $num_posts = $fo->Posts($posts,$id_form,$id_topic);
    $tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","");
}
if($form['weights'])
    $tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);
    
if($input_right)
{
    $num = $fo->PostsSummary($row,$id_form,$id_topic,$start_date,$end_date,$by_day);
    echo $hh->input_form_open();
    echo $hh->input_table_open();
    echo $hh->input_hidden("from","form_summary");
    echo $hh->input_hidden("id_form",$id_form);
    echo $hh->input_hidden("id_topic",$id_topic);
    echo $hh->input_date("from","start_date",strtotime($start_date),$input_right,false);
    echo $hh->input_date("to","end_date",strtotime($end_date),$input_right,false);
    echo $hh->input_checkbox("group_by_day","by_day",$by_day,0,$input_right);
    $actions = array();
    $actions[] = array('action'=>"list",'label'=>"list",'right'=>$input_right);
    echo $hh->input_actions($actions,$input_right);
    echo $hh->input_table_close() . $hh->input_form_close(); 
                                   
    if ($by_day)
    {
        $table_headers = array('date','posts');
        $table_content = array('{FormatDate($row[post_date_ts])}','{LinkTitle("form_details.php?start_date=$row[start_date]&end_date=$row[end_date]&id_topic=' . $id_topic . '&id='.$id_form.'","$row[count]")}');
        echo $hh->showTable($row, $table_headers, $table_content, $num);
    }
    else
    {
    	$num_posts = (int)$row[0]['count'];
        echo "<p>" . $hh->Wrap("$num_posts Posts","<a href=\"form_details.php?id=$id_form&id_topic=$id_topic&start_date=$start_date&end_date=$end_date\">", "</a>", $num_posts > 0) . "</p>";
    }
}

include_once(SERVER_ROOT."/include/footer.php");
?>

