<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");
include_once(SERVER_ROOT."/../classes/geo.php");

$id_org = $_GET['id'];
$id_p = $_GET['id_p'];
$id_campaign = $_GET['id_c'];
$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);

$c = new Campaign($id_campaign);
$row2 = $c->CampaignGet();
$title[] = array($row2['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);
$approve_comments = $row2['approve_comments'];

if ($id_org>0 || $id_p>0)
{
	$row = $id_org>0? $c->OrgGet($id_org) : $c->OrgGetById($id_p);
	if($id_p>0)
		$id_org = $row['id_org'];
	$action2 = "update";
	$insert_date = $row['insert_date'];
	$comment_approved = $row['comment_approved'];
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$params = $v->Deserialize($row['params']);
	$title[] = array($row['name'],'');
}
else
{
	$action2 = "insert";
	$comment_approved = $approve_comments;
	$title[] = array('add_new','');
}
echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("id_org",$id_org);
echo $hh->input_hidden("id_campaign",$id_campaign);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("approve_comments",$approve_comments);
echo $hh->input_hidden("from","org");
echo $hh->input_hidden("p",$current_page);
echo $hh->input_hidden("id_p",$row['id_p']);
echo $hh->input_table_open();

if ($id_org>0)
{
	echo $hh->input_date("insert_date","insert_date",$insert_date,0);
	echo $hh->input_time("hour","insert_date",$insert_date,0);
	echo $hh->input_text("ip","ip",$row['ip'],5,0,0);
	$email = (Modules::AmIAdmin(28))? "<a href=\"/people/person.php?id={$row['id_p']}\">{$row['email']}</a>" : $row['email'];
}
else
	$email = "";

echo $hh->input_text("name","name",$row['name'],30,0,$id_org==0);
echo $hh->input_text("referente","person",$row['person'],30,0,$id_org==0);
echo $hh->input_text("address","address",$row['address'],30,0,$id_org==0);
echo $hh->input_geo($row['id_geo'],$id_org==0);
echo $hh->input_text("phone","phone",$row['phone'],30,0,$id_org==0);
echo $hh->input_text("email","email",$email,30,0,$id_org==0);
echo $hh->input_text("website","org_web",$row['web'],30,0,$id_org==0);
if ($row2['money'] && $row['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	echo $hh->input_text("campaign_funding","support",$c->SupportByPerson($row['id_p']),10,0,0,$p->default_currency_desc);
}

echo $hh->input_textarea("comments","comments",$row['comments'],60,5,"",$input_right);
echo $hh->input_checkbox("contact_email","contact",$row['contact'],0,$input_right);
echo $hh->input_separator("administration");
echo $hh->input_checkbox("important","is_vip",$c->IsVip($row['id_p']),0,$input_right);
echo $hh->input_checkbox("approved","comment_approved",$comment_approved,0,$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_org>0);
$actions[] = array('action'=>"move_person",'label'=>"Cambia in adesione personale",'right'=>$input_right && $id_org>0 && $row['id_p']>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

