<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");

$id_topic = $_GET['id_topic'];
$id_campaign = $_GET['id'];

$t = new Topic($id_topic);
$c = new Campaign($id_campaign);
$row = $c->CampaignGet();

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);
$title[] = array($row['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);
$title[] = array('search','');
echo $hh->ShowTitle($title);
?>
<form action="campaign_search2.php" method="GET">
<input type=hidden name="id_c" value="<?=$id_campaign; ?>">
<input type=hidden name="id_topic" value="<?=$id_topic; ?>">
<table border=0 cellpadding=0 cellspacing=7>
<?php
$combo_values = $hh->tr->Translate("signers_types");
if($row['orgs_sign']>0)
	echo $hh->input_array("type","type","",$combo_values,1);
else 
	echo $hh->input_hidden("type",0);

echo $hh->input_text("name","name","",30,0,1);
echo $hh->input_text("email","email","",30,0,1);
echo $hh->input_text("comment","comments","",30,0,1);
echo $hh->input_geo(0,1);
echo $hh->input_checkbox("email_only","emails",0,0,1);
echo $hh->input_checkbox("important","vip",0,0,1);
echo "<tr><td>&nbsp;</td><td><input type=\"submit\" class=\"input-submit\" value=\"cerca\"><input type=reset value=\"cancella\"></td></tr>\n";
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

