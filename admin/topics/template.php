<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/template.php");

$hhf = new HHFunctions();

$id_template = (int)$_GET['id'];
$id_topic = (int)$_GET['id_topic'];

if($module_admin)
	$input_right = 1;
	
if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	if($t->AmIAdmin())
		$input_right = 1;	
}

$title[] = array('templates','templates.php?id_topic='.$id_topic);

$te = new Template();

if ($id_template>0)
{
	$row = $te->TemplateGet($id_template);
	$title[] = array($row['name'],'');
	$te->ResourceSet($row['id_res']);
	$template_topic = $row['id_topic']; 
	$template_group = $row['id_group']; 
	$resources = $te->Resources($id_template);
	$num_resources = count($resources);
}
else
{
	$title[] = array('add_new','');
	$template_topic = $id_topic;
	$template_group = 0;
	$num_resources = 0;
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","template");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
if($id_template>0)
	echo $hh->input_text("ID","id_template",$id_template,10,0,0);
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
$resource_types = $te->ResourceTypes($hh->tr->Translate("resources"));
echo $hh->input_array("resources_types","id_res_type",$row['id_res_type'],$resource_types,$input_right);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($template_topic,$template_group,$tt->User($ah->current_user_id),"all_option",$input_right,true,"id_topic_or_group");
echo $hh->input_text("subtopic_default","default_subtopic",$row['default_subtopic'],5,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_template>0 && $num_resources==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id_template>0)
{
	echo "<h3>" . $hh->tr->Translate("additional_fields");
	if($input_right)
		echo " (<a href=\"template_param.php?id_template=$id_template&id_topic=$id_topic&id=0\">" . $hh->tr->Translate("add_new") . "</a>)";
	echo "</h3>";
	$params = $te->Params($id_template);
	if(count($params)>0)
	{
		echo "<ul class=\"tree\">\n";
		$counter = 1;
		foreach($params as $param)
		{
			echo "<li>";
			if ($counter>1)
				echo "<a href=\"actions.php?from2=template_param&id_template=$id_template&id={$param['id_template_param']}&action3=up\">{$hh->img_arrow_up_on}</a>";
			else
				echo $hh->img_arrow_up_off;
			if ($counter<count($params))
				echo "<a href=\"actions.php?from2=template_param&id_template=$id_template&id={$param['id_template_param']}&action3=down\">{$hh->img_arrow_down_on}</a>";
			else
				echo $hh->img_arrow_down_off;
			echo "&nbsp;" . $hh->Wrap($hhf->LinkTitle("template_param.php?id={$param['id_template_param']}&id_template=$id_template&id_topic=$id_topic",$param['label']),"<strong>","</strong>",$param['public']) . " (" . $hh->tr->Translate("paramtype_" . $param['type']) . ")</li>\n";
			$counter ++;
		}
		echo "</ul>\n";
	}	
	echo "<h3>" . $hh->tr->Translate("hidden_fields");
	if($input_right)
		echo " (<a href=\"template_hide.php?id_template=$id_template&id_topic=$id_topic\">" . $hh->tr->Translate("change") . "</a>)";
	echo "</h3>";
	$tr_fields = array();
	foreach($row['hidden_fields'] as $field)
		$tr_fields[] = $hh->tr->TranslateTry($field);
	echo $hh->Array2String($tr_fields);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
