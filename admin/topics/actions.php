<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper(true,4);
$post = $fh->HttpPost();
$get = $fh->HttpGet();

if(count($post)==0 && count($_GET)==0)
	header("Location: /gate/index.php");

$action2	= $post['action2'];
$from		= $post['from'];
$action3	= $get['action3'];
$from2		= $get['from2'];

// TOPIC //

if ($from=="topic")
{
	$action 		= $fh->ActionGet($post);
	$id_topic	= $post['id_topic'];
	$t = new Topic($id_topic);
	$name 		= $post['name'];
	$description	= $post['description'];
	$keywords	= $post['keywords'];
	$home_keywords	= $post['home_keywords'];
	$share_images	= $fh->Checkbox2bool($post['share_images']);
	$share_docs		= $fh->Checkbox2bool($post['share_docs']);
	$comments	= (int)$post['comments'];
	$moderate_comments	= $fh->Checkbox2bool($post['moderate_comments']);
	$edit_layout	= $fh->Checkbox2bool($post['edit_layout']);
	$domain	= $fh->String2Url($post['domain']);
	$path		= $fh->String2Path($post['path']);
	$id_language	= $post['id_language'];
	$visible	= $fh->Checkbox2bool($post['visible']);
	$show_latest	= $fh->Checkbox2bool($post['show_latest']);
	$protected	= $post['protected'];
	$custom_visibility	= $fh->Checkbox2bool($post['custom_visibility']);
	$profiling			= $fh->Checkbox2bool($post['profiling']);
	$archived			= $fh->Checkbox2bool($post['archived']);
	$friendly_url	= $fh->String2Number($post['friendly_url']);
	$campaigns_open	= $post['campaigns_open'];
	$temail		= $post['temail'];
	$awstats	= $post['awstats'];
	$id_group	= $post['id_group'];
	$fh->va->NotEmpty($name,"name");
	$fh->va->NotEmpty($path,"path");
	$fh->va->GreaterThenZero($id_group,"placing");
	if($fh->va->return)
	{
		if ($action=="update")
		{
			$t->TopicUpdate($name,$description,$keywords,$path,$id_language,$visible,$id_group,$show_latest,$domain,$share_images,$share_docs,$edit_layout,$protected,$awstats,$comments,$moderate_comments,$profiling,$campaigns_open,$custom_visibility,$temail,$home_keywords,$friendly_url,$archived);
			header("Location: ops.php?id=$id_topic");
		}
		if ($action=="insert")
		{
			$id_topic = $t->TopicInsert($name,$description,$keywords,$path,$id_language,$visible,$id_group,$show_latest,$domain,$share_images,$share_docs,$edit_layout,$protected,$awstats,$comments,$moderate_comments,$profiling,$campaigns_open,$custom_visibility,$temail,$home_keywords,$friendly_url,$archived);
			header("Location: topic_gra.php?id=$id_topic");
		}
	}
	else 
		header("Location: topic.php?id=$id_topic");
	if ($action=="delete")
		header("Location: topic_delete.php?id=$id_topic");
}

if ($from=="topic_delete")
{
	$action 		= $fh->ActionGet($post);
	$module_admin = $ah->CheckModule();
	$id_topic	= $post['id_topic'];
	$t = new Topic($id_topic);
	if ($action=="delete" && $module_admin)
		$t->Delete();
	header("Location: index.php");
}

if ($from=="topic_gra")
{
	$id_topic			= $post['id_topic'];
	$t				= new Topic($id_topic);
	$id_style 			= $post['id_style'];
	$articles_per_page		= $post['articles_per_page'];
	$menu_depth			= $post['menu_depth'];
	$home_type			= $post['home_type'];
	$home_sort_by		= $post['home_sort_by'];
	$show_path			= $fh->Checkbox2bool($post['show_path']);
	$show_print			= $fh->Null2Zero($post['show_print']);
	$wysiwyg			= $fh->Checkbox2bool($post['wysiwyg']);
	$associated_image_size = $fh->String2Number($post['associated_image_size']);
	$action 		= $fh->ActionGet($post);
	if ($action=="update")
	{
		$t->TopicUpdateGraphic($id_style,$articles_per_page,$menu_depth,$home_type,$show_path,$show_print,$home_sort_by,$wysiwyg,$associated_image_size);
		header("Location: ops.php?id=$id_topic");
	}
}

if ($from2=="topic")
{
	$id_topic		= $_GET['id_topic'];
	$id_image		= $_GET['id_image'];
	$t = new Topic($id_topic);
	if ($action3=="image")
	{
		$t->TopicImageUpdate($id_image);
		header("Location: topic_gra.php?id=$id_topic");
	}
}

if ($from=="group")
{
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/topics.php");
	$name 		= $post['name'];
	$description	= $post['description'];
	$id_group	= $post['id_group'];
	$id_parent	= $fh->Null2Zero($post['id_parent']);
	$id_parent_old	= $fh->Null2Zero($post['id_parent_old']);
	$visible 		= $fh->Checkbox2bool($post['visible']);
	$keywords	= $post['keywords'];
	$topics = new Topics();
	if ($action=="update")
		$topics->gh->GroupUpdate($id_group,$name,$description,$id_parent,$id_parent_old,$visible,$keywords);
	if ($action=="insert")
		$topics->gh->GroupInsert($name,$description,$id_parent,$visible,$keywords);
	if ($action=="delete")
		$topics->gh->GroupDelete($id_group);
	$topics->QueueMapUpdate();
	header("Location: tree.php");
}

if ($from2=="group")
{
	include_once(SERVER_ROOT."/../classes/topics.php");
	$topics = new Topics;
	$id_group	= $_GET['id'];
	if ($action3=="up")
		$topics->gh->GroupMove($id_group,1);
	if ($action3=="down")
		$topics->gh->GroupMove($id_group,0);
	$topics->QueueMapUpdate();
	header("Location: tree.php");
}

if ($from2=="group_item")
{
	include_once(SERVER_ROOT."/../classes/topics.php");
	$topics = new Topics;
	$id_item	= $_GET['id_item'];
	$id_group	= $_GET['id_group'];
	if ($action3=="up")
		$topics->gh->GroupItemMove($id_item,$id_group,1);
	if ($action3=="down")
		$topics->gh->GroupItemMove($id_item,$id_group,0);
	$topics->QueueMapUpdate();
	header("Location: tree.php");
}

// TOPIC HOMEPAGE //

if ($from2=="homepage")
{
	$id_article	= $_GET['id'];
	$current	= $_GET['current'];
	$home_type	= $_GET['home_type'];
	$id_topic	= $_GET['id_topic'];
	$t = new Topic($id_topic);
	if ($action3=="remove")
		$t->HomepageArticleRemove($id_article);
	if ($action3=="swap_main")
		$t->HomepageArticleSwapStatus($current, $id_article);
	if ($action3=="add")
		$t->HomepageArticleAdd($id_article);
	header("Location: homepage.php?id=$id_topic");
}


// SUBTOPIC //

if ($from=="subtopic")
{
	$action 	= $fh->ActionGet($post);
	$id_subtopic		= $post['id_subtopic'];
	$id_topic		= $post['id_topic'];
	$t = new Topic($id_topic);
	$id_parent_old	= $post['id_parent_old'];
	$id_type		= (substr($post['id_type'],0,1)=="s")? (int)substr($post['id_type'],2) : $t->subtopic_types['module'];
	$link 			= $fh->String2Url($post['link']);
	$id_topic_forum	= $post['id_topic_forum'];
	$id_topic_forum_link	= $fh->Null2Zero($post['id_topic_forum_link']);
	$id_gallery		= $post['id_gallery'];
	$name 			= $post['name'];
	$visible		= $fh->Null2Zero($post['visible']);
	$id_parent		= $fh->Null2Zero($post['id_parent']);
	$description		= $post['description'];
	$keywords		= $post['keywords'];
	$header 		= $post['header'];
	$footer 		= $post['footer'];
	$sort_by		= $post['sort_by'];
	$furl			= $post['furl'];
	$records_per_page		= $fh->Null2Zero($post['records_per_page']);
	$has_feed	= $fh->Checkbox2bool($post['has_feed']);
	$params		= $fh->SerializeParams();
	$id_item = 0;
	$id_subitem = 0;
	$id_subitem_id = '';
	switch($id_type)
	{
		case $t->subtopic_types['map'];
			$id_item = $fh->Null2Zero($post['from_subtopic']);
		break;
		case $t->subtopic_types['contact'];
			$id_item = $fh->Null2Zero($post['id_form']);
		break;
		case $t->subtopic_types['campaign'];
			$id_item = $fh->Null2Zero($post['id_campaign']);
			$id_subitem = $fh->Null2Zero($post['id_campaign_subtype']);			
		break;
		case $t->subtopic_types['forum'];
			$id_item = $fh->Null2Zero($post['id_forum']);
			$id_subitem = $fh->Null2Zero($post['id_forum_subtype']);			
		break;
		case $t->subtopic_types['gallery'];
			$id_item = $fh->Null2Zero($post['id_gallery']);
			$id_subitem = $fh->Null2Zero($post['id_gallery_subtype']);	
		break;
		case $t->subtopic_types['calendar'];
			$id_item = $fh->Null2Zero($post['events_subtype']);
		break;
		case $t->subtopic_types['module'];
			$id_item = $fh->Null2Zero(substr($post['id_type'],2));
			$id_subitem = $fh->Null2Zero($post['id_subitem']);
			$id_subitem_id = $post['id_subitem_id'];
		break;
		case $t->subtopic_types['insert'];
			$id_item = $fh->Null2Zero($post['insert_type']);
		break;
		case $t->subtopic_types['insert'];
			$id_item = $fh->Null2Zero($post['insert_type']);
		break;
		case $t->subtopic_types['keyword_filter'];
			$id_item = $fh->Null2Zero($post['id_resource']);
			$filter_keywords = explode(",",$post['filter_keyword']);
			$num_keywords = count($filter_keywords);
			if($num_keywords==1)
			{
				include_once(SERVER_ROOT."/../classes/keyword.php");
				$k = new Keyword();
				$kw = $k->KeywordCheck($filter_keywords[0],1);
				$id_subitem = $kw['id_keyword'];
			}
			elseif($num_keywords>1)
			{
				$v = new Varia();
				$deparams = $v->Deserialize(stripslashes($params));
				$deparams['filter_keywords'] = $post['filter_keyword'];
				$params = $v->Serialize($deparams);
			}
		break;
		case $t->subtopic_types['poll'];
			$id_item = $fh->Null2Zero($post['id_poll']);
			$id_subitem = $fh->Null2Zero($post['id_poll_subtype']);
		break;
	}
	if ($action=="update")
		$t->SubtopicUpdate($id_subtopic,$name,$description,$id_parent,$id_parent_old,$header,$footer,$id_type,$visible,$id_item,$id_subitem,$id_subitem_id,$link,$keywords,$params,$sort_by,$records_per_page,$has_feed,$furl);
	if ($action=="insert")
		$t->SubtopicInsert($name,$description,$id_parent,$header,$footer,$id_type,$visible,$id_item,$id_subitem,$id_subitem_id,$link,$keywords,$params,$sort_by,$records_per_page);
	if ($action=="delete")
		$t->SubtopicDelete($id_subtopic);
	header("Location: subtopics.php?id=$id_topic");
}

if ($from2=="subtopic")
{
	$id_topic		= $_GET['id_topic'];
	$id_subtopic	= $_GET['id'];
	$id_image		= $_GET['id_image'];
	$t = new Topic($id_topic);
	if ($action3=="up")
	{
		$t->SubtopicMove($id_subtopic,1);
		header("Location: subtopics.php?id=$id_topic");
	}
	if ($action3=="down")
	{
		$t->SubtopicMove($id_subtopic,0);
		header("Location: subtopics.php?id=$id_topic");
	}
	if ($action3=="image")
	{
		$t->SubtopicImageUpdate($id_subtopic,$id_image);
		header("Location: subtopic.php?id=$id_subtopic&id_topic=$id_topic");
	}
}

// CAMPAIGNS //

if ($from=="campaign")
{
	include_once(SERVER_ROOT."/../classes/campaign.php");
	$id_campaign	= $post['id_campaign'];
	$id_topic		= $post['id_topic'];
	$start_date		= $fh->DateMerge("start_date",$post);
	$name			= $post['name'];
	$keywords		= $post['keywords'];
	$users_type		= $post['users_type'];
	$description		= $post['description'];
	$thanks		= $post['thanks'];
	$active			= $post['active'];
	$money		= $fh->Checkbox2bool($post['money']);
	$id_payment_type	= $post['id_payment_type'];
	$amount		= $post['amount'];
	$editable		= $fh->Checkbox2bool($post['editable']);
	$orgs_sign		= $fh->Checkbox2bool($post['orgs_sign']);
	$approve_comments	= $fh->Checkbox2bool($post['approve_comments']);
	$description_long	= $post['description_long'];
	$is_html		= $fh->Checkbox2bool($post['is_html']);
	$notify			= $post['notify'];
	$notify_text		= $post['notify_text'];
	$promoter		= $post['promoter'];
	$promoter_email	= $post['promoter_email'];
	$thanks_email		= $post['thanks_email'];
	$pre_sign		= $post['pre_sign'];
	$signatures_per_page		= $fh->Null2Zero($post['signatures_per_page']);
	$c = new Campaign($id_campaign);
	$action 		= $fh->ActionGet($post);
	if ($action=="insert")
		$c->CampaignInsert($id_topic,$start_date,$name,$keywords,$thanks,$money,$approve_comments,$description,$active,$users_type,$orgs_sign,$description_long,$is_html,$notify,0,$promoter,$notify_text,$promoter_email,$thanks_email,$pre_sign,$signatures_per_page);
	if ($action=="update")
		$c->CampaignUpdate($id_campaign,$id_topic,$start_date,$name,$keywords,$thanks,$money,$approve_comments,$description,$active,$users_type,$orgs_sign,$id_payment_type,$amount,$editable,$description_long,$is_html,$notify,$promoter,$notify_text,$promoter_email,$thanks_email,$pre_sign,$signatures_per_page);
	if ($action=="delete")
		$c->CampaignDelete();
	header("Location: campaigns.php?id=$id_topic");
}

if ($from=="campaign_account")
{
	include_once(SERVER_ROOT."/../classes/campaign.php");
	$action 	= $fh->ActionGet($post);
	$id_campaign	= $post['id_campaign'];
	$c = new Campaign($id_campaign);
	$id_use		= $post['id_use'];
	$id_topic		= $post['id_topic'];
	$id_account		= $post['id_account'];
	if ($action=="update")
		$c->AccountStore($id_use,$id_account);
	if ($action=="delete")
		$c->AccountDelete($id_use);
	header("Location: campaign.php?id=$id_campaign&id_topic=$id_topic");
}

// CAMPAIGN SIGNATURES //

if ($from=="person")
{
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/campaign.php");
	$name			= $post['name'];
	$surname		= $post['surname'];
	$email			= $post['email'];
	$id_geo		= $post['id_geo'];
	$unescaped_params	= $fh->SerializeParams(false);
	$comments		= $post['comments'];
	$comment_approved 	= $fh->Checkbox2bool($post['comment_approved']);
	$id_person		= $post['id_person'];
	$id_campaign		= $post['id_campaign'];
	$id_topic		= $post['id_topic'];
	$p			= $post['p'];
	$id_p			= $post['id_p'];
	$appr			= $post['appr'];
	$contact		= $fh->Checkbox2bool($post['contact']);
	$is_vip			= $fh->Checkbox2bool($post['is_vip']);
	$c = new Campaign($id_campaign);
	if ($action=="insert")
	{
		$fh->va->Email($email);
		$fh->va->NotEmpty($name,"name");
		if($fh->va->return)
		{
			$id_p = $c->UserCreate($name,$surname,$email,$id_geo,$unescaped_params,$contact,$id_topic);
			if($c->PersonInsert($id_p,$comments,$comment_approved,$contact))
				$c->VipSet($id_p,$is_vip);
			else
				$ah->MessageSet("signature_already",array($email));
		}
	}
	if ($action=="update")
	{
		$c->PersonUpdate($id_person,$comments,$comment_approved,$contact);
		$c->VipSet($id_p,$is_vip);
	}
	if ($action=="delete")
		$c->PersonDelete($id_person);
	$rows = array();
	$approved = ($c->PersonsApproveCounter(0)>0)? 0:1;
	header("Location: campaign_persons.php?id=$id_campaign&id_topic=$id_topic&p=$p&appr=$approved");
}

if ($from=="org")
{
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/campaign.php");
	$name			= $post['name'];
	$address		= $post['address'];
	$id_geo		= $post['id_geo'];
	$phone		= $post['phone'];
	$email			= $post['email'];
	$comments		= $post['comments'];
	$comment_approved	= $fh->Checkbox2bool($post['comment_approved']);
	$id_org		= $post['id_org'];
	$id_campaign		= $post['id_campaign'];
	$id_topic		= $post['id_topic'];
	$p			= $post['p'];
	$id_p			= $post['id_p'];
	$appr			= $post['appr'];
	$contact		= $fh->Checkbox2bool($post['contact']);
	$is_vip			= $fh->Checkbox2bool($post['is_vip']);
	$c = new Campaign($id_campaign);
	if ($action=="insert")
	{
		$fh->va->Email($email);
		$fh->va->NotEmpty($name,"name");
		if($fh->va->return)
		{
			$unescaped_post = $fh->HttpPost(false,false);
			$unescaped_params = (array('web'=>$unescaped_post['org_web'],'contact'=>$unescaped_post['person']));
			$id_p = $c->UserCreate($name,"",$email,$id_geo,$unescaped_params,$contact,$id_topic,false,$phone,$address);
			if($c->OrgInsert($id_p,$comments,$comment_approved,$contact))
				$c->VipSet($id_p,$is_vip);
			else
				$ah->MessageSet("signature_already",array($email));
		}
	}
	if ($action=="update")
	{
		$c->OrgUpdate($id_org,$comments,$comment_approved,$contact);
		$c->VipSet($id_p,$is_vip);
	}
	if ($action=="delete")
		$c->OrgDelete($id_org);
	$rows = array();
	if ($action=="move_person")
	{
	    // remove org
	    $c->OrgDelete($id_org);
	    // add person
	    $c->PersonInsert($id_p,$comments,$comment_approved,$contact);
	    $person = $c->PersonGetById($id_p);
	    $redirect = "campaign_person.php?id={$person['id_person']}&id_c=$id_campaign&id_topic=$id_topic";
	} else {
	    $approved = ($c->OrgsApprove($rows,0,1)>0)? 0:1;
	    $redirect = "campaign_orgs.php?id=$id_campaign&id_topic=$id_topic&p=$p&appr=$approved";
	}
	header("Location: $redirect");
}

// POLLS //

if ($from=="poll")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_topic		= $post['id_topic'];
	$start_date		= $fh->DateMerge("start_date",$post);
	$id_type		= $post['id_type'];
	$title			= $post['title'];
	$description		= $post['description'];
	$intro			= $post['intro'];
	$thanks		= $post['thanks'];
	$users_type		= $post['users_type'];
	$submit_questions	= $post['submit_questions'];
	$keywords		= $post['keywords'];
	$is_private		= $fh->Checkbox2bool($post['is_private']);
	$action 		= $fh->ActionGet($post);
	$comments		= $post['comments'];
	$moderate_comments	= $fh->Checkbox2bool($post['moderate_comments']);
	$promoter		= $post['promoter'];
	$promoter_email		= $post['promoter_email'];
	$notify_answers		= $fh->Checkbox2bool($post['notify_answers']);
	$vote_min		= $post['vote_min'];
	$vote_max		= $post['vote_max'];
	$vote_threshold	= $fh->String2Number($post['vote_threshold']);
	$sort_questions_by	= $post['sort_questions_by'];
	$active			= $post['active'];
	if ($action=="update")
		$pl->PollStore($id_poll,$id_topic,$start_date,$id_type,$title,$description,$intro,$thanks,$users_type,$keywords,$is_private,$comments,$moderate_comments,$vote_min,$vote_max,$vote_threshold,$submit_questions,$sort_questions_by,$promoter,$promoter_email,$notify_answers,$active);
	if ($action=="delete")
		$pl->PollDelete($id_poll);
	header("Location: polls.php?id=$id_topic");
}

if ($from=="poll_vote")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_p			= $post['id_p'];
	$id_question	= $fh->Null2Zero($post['id_question']);
	$action 		= $fh->ActionGet($post);
	if ($action=="delete")
		$pl->VoteDelete($id_p,$id_poll,$id_question);
	header("Location: poll_question_answers.php?id_poll=$id_poll");
}

if ($from=="poll_votename")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_topic		= $post['id_topic'];
	$vote			= $post['vote'];
	$name			= $post['name'];
	$action 		= $fh->ActionGet($post);
	if ($action=="update")
		$pl->VoteNameStore($id_poll,$vote,$name);
	if ($action=="delete")
		$pl->VoteNameDelete($id_poll,$vote);
	header("Location: poll_answers.php?id_poll=$id_poll");
}

if ($from2=="poll_votename_move")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= (int)$_GET['id_poll'];
	$vote		= (int)$_GET['vote'];
	$move		= $_GET['move'];
	$pl->VoteNameMove($id_poll,$vote,$move=="up");
	header("Location: poll_answers.php?id_poll=$id_poll");
}

if ($from=="poll_question")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_topic		= $post['id_topic'];
	$id_question		= $post['id_question'];
	$insert_date		= $fh->DateMerge("insert_date",$post);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$question		= $post['question'];
	$description		= $post['description'];
	$description_long	= $post['description_long'];
	$is_html		= $fh->Checkbox2bool($post['is_html']);
	$id_questions_group		= $fh->String2Number($post['id_questions_group']);
	$weight		= $fh->String2Number($post['weight'],1);
	$action 		= $fh->ActionGet($post);
	if ($action=="update")
		$pl->QuestionStore($id_question,$id_poll,$question,$description,$insert_date,$approved,$description_long,$is_html,$id_questions_group,$weight);
	if ($action=="delete")
		$pl->QuestionDelete($id_question);
	header("Location: poll_questions.php?id=$id_poll&approved=-1");
}

if ($from=="poll_question_weights")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_topic		= $post['id_topic'];
	$id_question		= $post['id_question'];
	$action 		= $fh->ActionGet($post);
	if ($action=="update")
	{
		$votenames = $pl->QuestionVotesWeights($id_poll,$id_question);
		foreach($votenames as $votename)
		{
			$weight		= $fh->String2Number($post['question_weight_'.$votename['vote']]);
			$pl->QuestionVoteWeightStore($id_poll,$id_question,$votename['vote'],$weight);
		}
	}
	header("Location: poll_question.php?id_poll=$id_poll&id=$id_question");
}

if ($from2=="poll_question_move")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_question	= (int)$_GET['id_question'];
	$id_questions_group		= (int)$_GET['id_questions_group'];
	$move		= $_GET['move'];
	$question = $pl->QuestionGet($id_question);
	$pl->QuestionMove($id_question,$move=="up",$id_questions_group);
	if($id_questions_group>0)
		header("Location: poll_questions_group.php?id_poll={$question['id_poll']}&id=$id_questions_group");
	else
		header("Location: poll_questions.php?id={$question['id_poll']}&approved=1");
}

if ($from=="poll_questions_group")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_poll		= $post['id_poll'];
	$id_topic		= $post['id_topic'];
	$id_questions_group		= $post['id_questions_group'];
	$name		= $post['name'];
	$action 		= $fh->ActionGet($post);
	if ($action=="update")
		$pl->QuestionsGroupStore($id_questions_group,$id_poll,$name);
	$rows = array();
	header("Location: poll_questions_groups.php?id=$id_poll");
}

if ($from2=="poll_questions_group_move")
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$id_questions_group		= (int)$_GET['id_questions_group'];
	$move		= $_GET['move'];
	$group = $pl->QuestionsGroupGet($id_questions_group);
	$pl->QuestionsGroupMove($id_questions_group,$move=="up");
	header("Location: poll_questions_groups.php?id={$group['id_poll']}");
}


// NEWSLETTER //

if ($from=="newsletter")
{
	include_once(SERVER_ROOT."/../classes/mail.php");
	$mail = new Mail();
	$id_topic	= $post['id_topic'];
	$action 	= $fh->ActionGet($post);
	if ($action=="update")
	{
		$newsletter		= $fh->Checkbox2bool($post['newsletter']);
		$id_recipient	= $post['id_recipient'];
		$id_frequency	= $post['id_frequency'];
		$lastletter		= $fh->DateMerge("lastletter",$post);
		$format			= $fh->Null2Zero($post['newsletter_format']);
		$recipient		= $post['newsletter_rcpt'];
		$newsletter_people		= $fh->Checkbox2bool($post['newsletter_people']);
		$mail->NewsletterUpdate( $id_topic, $newsletter, $format, $id_recipient, $recipient, $id_frequency, $lastletter, $newsletter_people );
		header("Location: newsletter.php?id=$id_topic");
	}
	if ($action=="send")
	{
		$mail->force_newsletter = true;
		$return = $mail->NewsletterSend($id_topic);
		include_once(SERVER_ROOT."/../classes/adminhelper.php");
		$ah = new AdminHelper;
		include_once(SERVER_ROOT."/../classes/translator.php");
		$tr = new Translator($ah->session->Get('id_language'),0);
		$frequency = $tr->Translate("newsletter_frequency");
		$t = new Topic($id_topic);
		if ($return['sent'])
			$ah->MessageSet("newsletter_sent_ok",array($frequency[$return['id_frequency']],$return['topic'],$return['user']));
		else
			$ah->MessageSet("newsletter_sent_ko",array($frequency[$return['id_frequency']],$return['topic']));
		header("Location: ops.php?id=$id_topic");
	}
}


// FORUM //

if ($from=="forum")
{
	include_once(SERVER_ROOT."/../classes/forum.php");
	$id_forum		= $post['id_forum'];
	$id_topic		= $post['id_topic'];
	$start_date		= $fh->DateMerge("start_date",$post);
	$name			= $post['name'];
	$keywords		= $post['keywords'];
	$thanks		= $post['thanks'];
	$description		= $post['description'];
	$users_type		= $post['users_type'];
	$approve_threads	= $fh->Checkbox2bool($post['approve_threads']);
	$comments		= $post['comments'];
	$approve_comments	= $fh->Checkbox2bool($post['approve_comments']);
	$uploads		= $fh->Checkbox2bool($post['uploads']);
	$source		= $fh->Checkbox2bool($post['source']);
	$active			= $post['active'];
	$f = new Forum($id_forum);
	$fh->va->NotEmpty($name,"name");
	if($fh->va->return)
	{
		$action = $fh->ActionGet($post);
		if($action=="update")
			$f->ForumStore($id_topic,$start_date,$name,$description,$keywords,$thanks,$users_type,$approve_threads,$comments,$approve_comments,$uploads,$source,$active);
		if ($action=="delete")
			$f->ForumDelete();
	}
	header("Location: forums.php?id=$id_topic");
}

if ($from=="thread")
{
	include_once(SERVER_ROOT."/../classes/forum.php");
	$id_thread		= $post['id_thread'];
	$id_forum		= $post['id_forum'];
	$id_topic		= $post['id_topic'];
	$approved		= $post['approved'];
	$p			= $post['p'];
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$source		= $post['source'];
	$title			= $post['title'];
	$description		= $post['description'];
	$description_long	= $post['description_long'];
	$is_html		= $fh->Checkbox2bool($post['is_html']);
	$keywords		= $post['keywords'];
	$thread_approved	= $fh->Checkbox2bool($post['thread_approved']);
	$f = new Forum($id_forum);
	$fh->va->NotEmpty($title,"title");
	if($fh->va->return)
	{
		$action = $fh->ActionGet($post);
		if($action=="update")
			$f->ThreadStore($id_thread,$insert_date,$source,$title,$description,$description_long,$is_html,$thread_approved,$keywords);
		if($action=="delete")
			$f->ThreadDelete( $id_thread );
		header("Location: forum_threads.php?id=$id_forum&id_topic=$id_topic&approved=$approved&p=$p");
	}
	else 
		header("Location: forum_thread.php?id_forum=$id_forum&id_topic=$id_topic&id=0");
}

// USERS //

if ($from=="users")
{
	$id_topic 	= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/topic.php");
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users();
	$users = $uu->TopicUsersAdmins($id_topic);
	$t = new Topic($id_topic);
	if ($action2=="add_user")
	{
		$t->UsersDelete();
		foreach($users as $user)
		{
			if ($fh->Checkbox2Bool($post[ $user['id_user'] ]))
			{
				$is_admin = ($user['is_admin']==1)? 1 : 0;
				$is_contact = ($user['is_contact']==1)? 1 : 0;
				$pending_notify = ($user['pending_notify']==1)? 1 : 0;
				$t->UserInsert( $user['id_user'], $is_admin, $is_contact, $pending_notify );
			}
		}
		if ($t->protected=="1")
			$t->PasswordUpdate();
	}
	if ($action2=="add_admin")
	{
		foreach($users as $user)
		{
			$user_check = $fh->Checkbox2Bool($post[ $user['id_user'] ]);
			$is_admin = ($user['is_admin']==1)? 1 : 0;
			$is_contact = ($user['is_contact']==1)? 1 : 0;
			$pending_notify = ($user['pending_notify']==1)? 1 : 0;
			if (!$user_check && $is_admin==1)
				$t->UserUpdate( $user['id_user'], 0, 0, 0);
			if ($user_check && $is_admin==0)
				$t->UserUpdate( $user['id_user'], 1, $is_contact, $pending_notify);
		}
	}
	if ($action2=="add_contact")
	{
		foreach($users as $user)
		{
			$user_check = $fh->Checkbox2Bool($post[ $user['id_user'] ]);
			$is_admin = ($user['is_admin']==1)? 1 : 0;
			$is_contact = ($user['is_contact']==1)? 1 : 0;
			$pending_notify = ($user['pending_notify']==1)? 1 : 0;
			if (!$user_check && $is_contact==1)
				$t->UserUpdate( $user['id_user'], $is_admin, 0, $pending_notify);
			if ($user_check && $is_contact==0)
				$t->UserUpdate( $user['id_user'], $is_admin, 1, $pending_notify);
		}
	}
	header("Location: ops.php?id=$id_topic");
}


// LINKS //

if ($from=="link")
{
	include_once(SERVER_ROOT."/../classes/link.php");
	$id_link		= $post['id_link'];
	$error404		= $fh->Checkbox2bool($post['error404']);
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$url			= $fh->String2Url($post['url']);
	$title			= $post['title'];
	$description	= $post['description'];
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$vote			= $post['vote'];
	$id_language	= $post['id_language'];
	$id_topic		= $post['id_topic'];
	$id_subtopic	= $post['id_subtopic'];
	$approved		= $fh->Checkbox2bool($post['approved']);
	$approved_old	= $fh->Null2Zero($post['approved_old']);
	$l = new Link($id_link);
	if ($action2=="update")
		$l->LinkUpdate($error404,$insert_date,$url,$title,$description,$keywords,$vote,$id_language,$id_topic,$id_subtopic,$approved,$approved_old,$ikeywords);
	if ($action2=="delete")
		$l->LinkDelete();
	if ($action2=="insert")
		$l->LinkInsert($insert_date,$url,$title,$description,$keywords,$vote,$id_language,$id_topic,$id_subtopic,$approved,$ikeywords);
	include_once(SERVER_ROOT."/../classes/links.php");
	$links = new Links;
	if ($links->CountTopicApproved($id_topic,0) > 0)
		header("Location: links.php?id=$id_topic&appr=0");
	else
		header("Location: links.php?id=$id_topic");
}


// CONFIGURATION //

if ($from=="config_update")
{
	include_once(SERVER_ROOT."/../classes/topics.php");
	$id_language		= $post['id_language'];
	$temp_id_topic	= $post['temp_id_topic'];
	$latest		 	= $post['latest'];
	$wday_newsletter	= $post['wday_newsletter'];
	$topic_store 		= $post['topic_store'];
	$default_visibility	= $post['default_visibility'];
	$map_sort_by		= $post['map_sort_by'];
	$campaign_path 	= $post['campaign_path'];
	$poll_path	 	= $post['poll_path'];
	$forum_path 		= $post['forum_path'];
	$map_path		= $post['map_path'];
	$search_path	= $post['search_path'];
	$licences		= $fh->Checkbox2bool($post['licences']);
	$id_licence 		= $post['id_licence'];
	$topics = new Topics;
	$topics->ConfigurationUpdate($id_language,$temp_id_topic,$latest,$wday_newsletter,$topic_store,$default_visibility,$campaign_path,$forum_path,$licences,$id_licence,$poll_path,$map_path,$search_path,$map_sort_by);
	header("Location: index.php");
}


/* TEXTS */

if ($from=="content")
{
	$action 		= $fh->ActionGet($post);
	$id_content		= $post['id_content'];
	$content	 	= $post['content'];
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$id_topic		= $fh->Null2Zero($post['id_topic']);
	$id_type		= $fh->Null2Zero($post['id_type']);
	$old_id_type		= $fh->Null2Zero($post['old_id_type']);
	$is_html		= $fh->Checkbox2bool($post['is_html']);
	$wysiwyg		= $fh->Checkbox2bool($post['wysiwyg']);
	include_once(SERVER_ROOT."/../classes/texthelper.php");
	$th = new TextHelper();
	include_once(SERVER_ROOT."/../classes/translator.php");
	$tr = new Translator(0,0);
	$text_types = $tr->Translate("texts_types");
	$description	= ($post['description']!="")? $post['description'] : $text_types[$id_type];
	if ($action=="delete")
		$th->ContentDelete($id_content);
	else
	{
		if ($action2=="insert")
			$th->ContentInsert($description,$content,$id_topic,$id_type,$keywords,$is_html,$ikeywords,$wysiwyg);
		if ($action2=="update")
			$th->ContentUpdate($id_content,$description,$content,$id_topic,$id_type,$old_id_type,$keywords,$is_html,$ikeywords,$wysiwyg);
		if ($action2=="store")
			$th->ContentStore($description,$content,$id_topic,$id_type,$old_id_type,$keywords,$is_html,$ikeywords,$wysiwyg);
	}
	header("Location: texts.php?id=$id_topic");
}


/* FEATURES */

if ($from=="feature")
{
	$action 		= $fh->ActionGet($post);
	$id_feature		= $post['id_feature'];
	$name		 	= $post['name'];
	$active		= $fh->Checkbox2bool($post['active']);
	$public		= $fh->Checkbox2bool($post['public']);
	$description	= $post['description'];
	$id_topic		= $fh->Null2Zero($post['id_topic']);
	if(substr($post['id_type'],0,1)=="m")
	{
		$id_type 	= 0;
		$id_module	= $fh->Null2Zero(substr($post['id_type'],1));
	}
	else 
	{
		$id_type	= $fh->Null2Zero($post['id_type']);
		$id_module	= 0;
	}
	$id_function	= $fh->Null2Zero($post['id_function']);
	$condition_var	= $post['condition_var'];
	$condition_type	= $fh->Null2Zero($post['condition_type']);
	$condition_value	= $post['condition_value'];
	$condition_id	= $fh->Null2Zero($post['condition_id']);
	$old_id_type	= $fh->Null2Zero($post['old_id_type']);
	$old_id_function	= $fh->Null2Zero($post['old_id_function']);
	if ($id_topic>0)
	{
		$t			= new Topic($id_topic);
		$id_style = $t->id_style;
	}
	else
	{
		$id_style = 0;
	}
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="delete")
	{
		$pt->FeatureDelete($id_feature,$id_style,$id_type);
		header("Location: features.php?id=$id_topic");
	}
	if ($action=="activeswap")
	{
		$pt->ft->FeatureActiveSwap($id_feature,$active);
		header("Location: features.php?id=$id_topic");
	}
	if ($action=="store")
	{
		$changed = $id_type!=$old_id_type || $id_function!=$old_id_function;
		$id_feature = $pt->FeatureStore( $id_feature,$name,$description,$id_style,$id_type,$id_module,$id_function,$active,$public,$condition_var,$condition_type,$condition_value,$condition_id,$ah->current_user_id,$changed );
		$params = $pt->ft->params[$id_function];
		if (count($params)>0)
			header("Location: feature_params.php?id=$id_feature&id_topic=$id_topic");
		else
			header("Location: feature.php?id=$id_feature&id_topic=$id_topic");
	}
}

if ($from=="feature_params")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$id_topic	= $post['id_topic'];
	$params	= $fh->SerializeParams();
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="store")
	{
		$pt->PageFunctionsParamsStore($id_feature,$params);
	}
	header("Location: feature.php?id=$id_feature&id_topic=$id_topic");
}

if ($from=="feature_global")
{
	$action 		= $fh->ActionGet($post);
	$id_feature		= $post['id_feature'];
	$name		 	= $post['name'];
	$active		= $fh->Checkbox2bool($post['active']);
	$public		= $fh->Checkbox2bool($post['public']);
	$description	= $post['description'];
	$id_type		= $fh->Null2Zero($post['id_type']);
	$id_function	= $fh->Null2Zero($post['id_function']);
	$old_id_type	= $fh->Null2Zero($post['old_id_type']);
	$old_id_function	= $fh->Null2Zero($post['old_id_function']);
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="delete")
	{
		$pt->GlobalFeatureDelete($id_feature,$id_type);
		header("Location: features_global.php");
	}
	if ($action=="activeswap")
	{
		$pt->ft->FeatureActiveSwap($id_feature,$active);
		header("Location: features_global.php");
	}
	if ($action=="store")
	{
		$id_feature = $pt->GlobalFeatureStore( $id_feature,$name,$description,$id_type,$id_function,$old_id_type,$old_id_function,$active,$public,$ah->current_user_id );
		$params = $pt->ft->params[$id_function];
		if (count($params)>0)
			header("Location: feature_global_params.php?id=$id_feature");
		else
			header("Location: features_global.php");
	}
}

if ($from=="feature_global_params")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$params		= $fh->SerializeParams();
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="store")
	{
		$pt->PageFunctionsParamsStore($id_feature,$params);
	}
	header("Location: feature_global.php?id=$id_feature");
}

if ($from=="xml_fragment")
{
	$action 	= $fh->ActionGet($post);
	$id_xml		= $post['id_xml'];
	$name		= $post['name'];
	$uxml		= $fh->HttpPostUnescapedVar("xml");
	include_once(SERVER_ROOT."/../classes/xmlhelper.php");
	$xh = new XmlHelper();
	if ($action=="delete")
	{
		$xh->XmlFragmentDelete($id_xml);
	}
	if ($action=="store")
	{
		$xh->XmlFragmentStore( $id_xml,$name,$uxml );
	}
	header("Location: xml_fragments.php");
}


/* BOOKS */

if ($from=="books_config")
{
	$id_topic	= $post['id_topic'];
	$books_home_type	= $post['books_home_type'];
	$books_reviews	= $post['books_reviews'];
	$show_reviews	= $post['show_reviews'];
	include_once(SERVER_ROOT."/../modules/books.php");
	$bb = new Books();
	$bb->TopicConfigStore($id_topic,$books_home_type,$books_reviews,$show_reviews);
	header("Location: ops.php?id=$id_topic");
}

if($from2=="books_homepage")
{
	if($ah->ModuleAdmin(16))
	{
		$id_item = $_GET['id'];
		$id_topic = $_GET['id_topic'];
		include_once(SERVER_ROOT."/../modules/books.php");
		$bb = new Books();
		if($action3=="delete")
			$bb->HomepageDelete($id_item,$id_topic);
		if($action3=="add")
			$bb->HomepageUpdate($id_item,$id_topic,$home_type==1);
	}
	header("Location: books_config.php?id=$id_topic");
}


/* COMMENTS */

if ($from=="comment")
{
	$id_comment	= (int)$post['id_comment'];
	$id_topic	= $post['id_topic'];
	$type		= $post['type'];
	$id_item	= $post['id_item'];
	$id_parent	= (int)$post['id_parent'];
	$title	 	= $post['title'];
	$comment	= $post['comment'];
	$approved	= $fh->Checkbox2bool($post['approved']);
	$pub_user	= $ah->session->Get("user");
	$id_p		= (int)$pub_user['id'];
	$insert_date 	= $fh->Strings2Datetime($post['insert_date_d'],$post['insert_date_m'],$post['insert_date_y'],$post['insert_date_h'],$post['insert_date_i']);
	$params = array();
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments($type,$id_item);
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$action 		= $fh->ActionGet($post);
	$url = "comments_tree.php?type=$type&id_item=$id_item";
	if ($action=="delete")
	{
		$co->CommentDelete($id_comment);
	}
	else 
	{
		$fh->va->NotEmpty($title,"title");
		if($fh->va->return)
		{
			if ($action=="update")
				$co->CommentUpdate($id_comment,$title,$comment,$approved,$id_topic);
			if ($action=="insert")
				$co->CommentInsert($id_parent,$title,$comment,$insert_date,$v->IP(),$approved,$id_p,$id_topic,$v->Serialize(array()));
		}
		else 
		{
			$url ="comment.php?type=$type&id_item=$id_item&id=$id_comment";
		}
	}
	header("Location: $url");
}

if ($from=="form")
{
	$action 		= $fh->ActionGet($post);
	$id_form		= $post['id_form'];
	$name		 	= $post['name'];
	$profiling		= $post['profiling'];
	$id_pt_group		= $fh->Null2Zero($post['id_pt_group']);
	$multilanguage	= $fh->Checkbox2bool($post['multilanguage']);
	$privacy_warning	= $fh->Checkbox2bool($post['privacy_warning']);
	$weights	= $fh->Checkbox2bool($post['weights']);
	$store	= $fh->Checkbox2bool($post['store']);
	$captcha	= $fh->Checkbox2bool($post['captcha']);
	$id_topic		= $fh->Null2Zero($post['id_topic']);
	$id_topic_current	= $post['id_topic_current'];
	$id_recipient		= $post['id_recipient'];
	$recipient          = $post['recipient']; 
	$require_auth		= $post['require_auth']; 
	$id_payment_type	= $post['id_payment_type'];
	$amount		= $post['amount'];
	$thanks		= $post['thanks'];
	$thanks_email	= $post['thanks_email'];
	$redirect		= $post['redirect'];
	$editable		= $fh->Checkbox2bool($post['editable']);
	$hide_empty_fields	= $fh->Checkbox2bool($post['hide_empty_fields']);
	include_once(SERVER_ROOT."/../classes/forms.php");
	$fo = new Forms();
	if ($action=="delete")
	{
		$fo->FormDelete($id_form);
		header("Location: forms.php?id=$id_topic");
	}
	if ($action=="store")
	{
		$id_form2 = $fo->FormStore($id_form,$id_topic,$multilanguage,$profiling,$name,$ah->current_user_id,$id_recipient,$recipient,$privacy_warning,$id_payment_type,$amount,$editable,$thanks,$id_pt_group,$store,$captcha,$require_auth,$thanks_email,$redirect,$weights,$hide_empty_fields);
		if ($id_form>0)
			header("Location: forms.php?id=$id_topic_current");
		else
			header("Location: form_params.php?id=$id_form2&id_topic=$id_topic_current");
	}
}


/* FORM */

if ($from=="form_account")
{
	include_once(SERVER_ROOT."/../classes/forms.php");
	$action 	= $fh->ActionGet($post);
	$id_form	= $post['id_form'];
	$fo = new Forms();
	$id_use		= $post['id_use'];
	$id_topic		= $post['id_topic'];
	$id_account		= $post['id_account'];
	if ($action=="update")
		$fo->AccountStore($id_form,$id_use,$id_account);
	if ($action=="delete")
		$fo->AccountDelete($id_use);
	header("Location: form.php?id=$id_form&id_topic=$id_topic");
}

if ($from=="form_param")
{
	$action 		= $fh->ActionGet($post);
	$unescaped_post	= $fh->HttpPost(false,false);
	$id_form		= $post['id_form'];
	$id_param		= $post['id_param'];
	$label		 	= $unescaped_post['label'];
	$value		 	= $unescaped_post['value'];
	$id_type		= $post['id_type'];
	$id_type_old		= $post['id_type_old'];
	$type_params		= $post['type_params'];
	$type_weights		= $post['type_weights'];
	$id_topic		= $post['id_topic'];
	$mandatory		= $fh->Checkbox2bool($post['mandatory']);
	$email_subject          = $fh->Checkbox2bool($post['email_subject']); 
	include_once(SERVER_ROOT."/../classes/forms.php");
	$fo = new Forms();
	$url = "form_params.php?id_topic=$id_topic&id=$id_form";
	if ($action=="delete")
	{
		$fo->ParamDelete($id_param,$id_form);
	}
	if ($action=="update")
	{            
		$id_param = $fo->ParamUpdate($id_form,$id_param,$label,$value,$id_type,$type_params,$mandatory,$email_subject,$type_weights);
		if(($id_type=="0" || $id_type=="4" || $id_type=="5" || $id_type=="6" || $id_type=="9" || $id_type=="10" || $id_type=="11" || $id_type=="14") && $id_type!=$id_type_old)
            $url = "form_param.php?id=$id_param&id_form=$id_form&id_topic=$id_topic";
	}
	header("Location: $url");
}

if ($from=="form_action")
{
	$action 		= $fh->ActionGet($post);
	$unescaped_post	= $fh->HttpPost(false,false);
	$id_form		= $post['id_form'];
	$id_action		= $post['id_action'];
	$score_min		= $post['score_min'];
	$score_max		= $post['score_max'];
	$recipient		= $post['recipient'];
	$redirect		= $fh->String2Url($post['redirect']);
	$id_topic		= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/forms.php");
	$fo = new Forms();
	if ($action=="delete")
	{
		$fo->ActionDelete($id_action);
	}
	if ($action=="update")
	{            
		$fo->ActionStore($id_action, $id_form, $score_min, $score_max, $recipient, $redirect);
	}
	header("Location: form_actions.php?id_topic=$id_topic&id=$id_form");
}

if ($from=="form_post")
{
	$action 		= $fh->ActionGet($post);
	$id_form		= $post['id_form'];
	$id_post		= $post['id_post'];
	$id_topic		= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/forms.php");
	$fo = new Forms();
	if ($action=="delete")
	{
		$fo->PostDelete($id_post);
	}
	header("Location: form_posts.php?id=$id_form&id_topic=$id_topic");
}

if ($from2=="form_param")
{
	$id_form	= $_GET['id_form'];
	$id_topic	= $_GET['id_topic'];
	$id_param	= $_GET['id'];
	include_once(SERVER_ROOT."/../classes/forms.php");
	$fo = new Forms();
	if ($action3=="up")
		$fo->MoveParam($id_form,$id_param,1);
	if ($action3=="down")
		$fo->MoveParam($id_form,$id_param,0);
	header("Location: form_params.php?id=$id_form&id_topic=$id_topic");
}

if ($from=="form_summary")
{
    $id_form    = $post['id_form'];
    $id_topic    = $post['id_topic'];
    $start_date = $fh->DateMerge("start_date",$post);
    $end_date = $fh->DateMerge("end_date",$post);
    $by_day = $fh->Checkbox2bool($post['by_day']);
    
    $url = "form_summary.php?id=$id_form&id_topic=$id_topic&by_day=$by_day"; 
    if ($start_date != '')
        $url .= "&start_date=$start_date";
    if ($end_date != '')
        $url .= "&end_date=$end_date";
    header("Location: $url");
}


/* ACCOUNTS */

if ($from=="account")
{
	$action 		= $fh->ActionGet($post);
	$id_account		= $post['id_account'];
	$id_type		= $post['id_type'];
	$name		 	= $post['name'];
	$active			= $fh->Checkbox2bool($post['active']);
	$id_topic		= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/payment.php");
	$d = new Payment();
	if ($action=="delete")
		$d->AccountDelete($id_account);
	if ($action=="update")
		$id_account = $d->AccountStore($id_topic,$id_account,$id_type,$name,$active,$ah->current_user_id);
	header("Location: accounts.php?id=$id_topic");
}

if ($from=="account_param")
{
	$action 		= $fh->ActionGet($post);
	$id_account		= $post['id_account'];
	$id_topic		= $post['id_topic'];
	$param		= $post['param'];
	$value			= $post['param_' . $param];
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	if ($action=="store")
		$p->AccountParamStore($id_account,$param,$value);
	header("Location: account.php?id=$id_account&id_topic=$id_topic");
}

if ($from=="payment_type")
{
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$action 		= $fh->ActionGet($post);
	$id_payment_type	= $post['id_payment_type'];
	$payment_type	= $post['payment_type'];
	$id_topic		= $post['id_topic'];
	if ($action=="delete")
		$p->TypeDelete($id_payment_type);
	if ($action=="update")
		$p->TypeStore($id_payment_type,$payment_type,$id_topic);
	header("Location: payment_types.php?id=$id_topic");
}

if ($from=="visitor")
{
	$action 		= $fh->ActionGet($post);
	$id_topic		= $post['id_topic'];
	$t 			= new Topic($id_topic);
	$id_p			= $post['id_p'];
	$contact		= $fh->Checkbox2bool($post['contact']);
	$access			= $fh->Checkbox2bool($post['access']);
	$admin_notes	= $post['admin_notes'];
	if ($action=="update")
		$t->PersonContactUpdate($id_p,$contact,$access,$admin_notes);
	if ($action=="delete")
		$t->PersonContactDelete($id_p);
	header("Location: visitors.php?id=$id_topic");
}

if ($from=="rss_feed")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/rssmanager.php");
	$rsm = new RssManager();
	$id_topic		= $post['id_topic'];
	$id_group 	= $post['id_group'];
	$id_feed 	= $post['id_feed'];
	$url 		= $fh->String2Url($post['url']);
	if ($action=="delete")
		$rsm->RssDelete( $id_feed );
	if ($action=="update")
		$rsm->RssStore( $id_feed,$url,$id_group );
	header("Location: rss_group.php?id=$id_group&id_topic=$id_topic");
}

if ($from=="rss_group")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/rssmanager.php");
	$rsm = new RssManager();
	$id_group 	= $post['id_group'];
	$id_topic		= $post['id_topic'];
	$name 		= $post['name'];
	$group_type 		= $post['group_type'];
	$items 		= $post['items'];
	$ttl 		= $post['ttl'];
	$show_title			= $fh->Checkbox2bool($post['show_title']);
	if ($action=="delete")
		$rsm->GroupDelete( $id_group );
	if ($action=="update")
		$rsm->GroupStore($id_group,$name,$group_type,$items,$ttl,$id_topic,$show_title);
	header("Location: rss_groups.php?id_topic=$id_topic");
}

if ($from=="template")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	$id_template 	= $post['id_template'];
	$id_topic		= $post['id_topic'];
	$id_res			= $post['id_res_type'];
	$name 			= $post['name'];
	$topic_info		= $fh->TopicOrGroup($post['id_topic_or_group']);
	$default_subtopic	= $fh->Null2Zero($post['default_subtopic']);
	if ($action=="delete")
		$te->TemplateDelete($id_template);
	if ($action=="update")
	{
		$te->ResourceSet($id_res);
		$te->TemplateStore($id_template,$name,$topic_info['id_group'],$topic_info['id_topic'],"",$default_subtopic);
	}
	header("Location: templates.php?id_topic=$id_topic");
}

if ($from=="template_param")
{
	$action 		= $fh->ActionGet($post);
	$id_topic		= $post['id_topic'];
	$id_template	= $post['id_template'];
	$id_res			= $post['id_res'];
	$id_template_param		= $post['id_template_param'];
	$label		 	= $post['label'];
	$id_type		= $post['id_type'];
	$id_type_old	= $post['id_type_old'];
	$type_params	= $post['type_params'];
	if($id_type=="1")
		$type_params = $fh->Checkbox2bool($post['type_params']);
	$default_value	= $post['default_value'];
	$public			= $fh->Checkbox2bool($post['public']);
	$index_include	= $fh->Checkbox2bool($post['index_include']);
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	$te->ResourceSet($id_res);
	$url = "template.php?id_topic=$id_topic&id=$id_template";
	if ($action=="delete")
	{
		$te->ParamDelete($id_template_param,$id_template);
	}
	if ($action=="update")
	{
		$id_param = $te->ParamStore($id_template_param,$id_template,$label,$id_type,$type_params,$default_value,$public,$index_include);
		if(($id_type=="4" || $id_type=="5" || $id_type=="6") && $id_type!=$id_type_old)
			$url = "template_param.php?id=$id_param&id_template=$id_template&id_topic=$id_topic";
	}
	header("Location: $url");
}

if ($from2=="template_param")
{
	$id_template		= $_GET['id_template'];
	$id_topic			= $_GET['id_topic'];
	$id_template_param	= $_GET['id'];
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	if ($action3=="up")
		$te->ParamMove($id_template,$id_template_param,1);
	if ($action3=="down")
		$te->ParamMove($id_template,$id_template_param,0);
	header("Location: template.php?id_topic=$id_topic&id=$id_template");
}

if ($from=="template_hide")
{
	$action 		= $fh->ActionGet($post);
	$id_template		= $post['id_template'];
	$id_topic			= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	$hidden_fields		= $fh->ParseCheckListLabels("hidden_fields",$post);
	if ($action=="update")
		$te->HideFields($id_template,$hidden_fields);
	header("Location: template.php?id_topic=$id_topic&id=$id_template");
}

if ($from=="topic_keyword")
{
	$action 		= $fh->ActionGet($post);
	$id_topic		= $post['id_topic'];
	$id_keyword		= $post['id_keyword'];
	$role 			= $fh->String2Number($post['role']);
	$id_res_type	= $post['id_res_type'];
	$module_admin = $ah->CheckModule();
	$t = new Topic($id_topic);
	if($module_admin && $id_keyword>0)
	{
		if ($action=="delete")
			$t->KeywordDelete($id_keyword);
		if ($action=="insert")
			$t->KeywordAdd($id_keyword,$id_res_type,$role);
		if ($action=="update")
			$t->KeywordUpdate($id_keyword,$id_res_type,$role);
	}
	header("Location: topic_keywords.php?id=$id_topic");
}

if ($from=="people_group")
{
	$action 		= $fh->ActionGet($post);
	$id_pt_group		= $post['id_pt_group'];
	$id_topic		= $post['id_topic'];
	$name 			= $post['name'];
	$t = new Topic($id_topic);
	if ($action=="update")
		$t->PeopleGroupStore($id_pt_group,$name);
	if ($action=="delete")
		$t->PeopleGroupDelete($id_pt_group);
	header("Location: visitors_groups.php?id=$id_topic");
}

if ($from=="visitor_groups")
{
	$id_p		= $post['id_p'];
	$id_topic	= $post['id_topic'];
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$pe->GroupsDelete($id_p,$id_topic);
	$t = new Topic($id_topic);
	$tgroups = array();
	$t->PeopleGroups($tgroups);
	foreach($tgroups as $tgroup)
	{
		if ($fh->Checkbox2Bool($post["group{$tgroup['id_pt_group']}"]))
		{
			$pe->GroupAdd($id_p,$tgroup['id_pt_group']);
		}
	}
	header("Location: visitor.php?id_p=$id_p&id=$id_topic");
}

if ($from=="status_change")
{
	$id			= $post['id'];
	$id_res		= $post['id_res'];
	$id_topic	= $post['id_topic'];
	$change_date		= $fh->CheckDateString($post['change_date']);
	$status		= $post['status'];
	$action 		= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/scheduler.php");
	$sc = new Scheduler();
	if($action=="update")
		$sc->StatusChangeStore($id_res,$id,$change_date,$status);
	if($action=="delete")
		$sc->StatusChangeDelete($id_res,$id,$change_date);
	switch($id_res)
	{
		case "11":
			$url = "campaign_status.php?id=$id";
		break;
		case "19":
			$url = "poll_status.php?id=$id";
		break;
	}
	header("Location: $url");
}

if ($from=="robots")
{
	$id_topic	= $post['id_topic'];
	$action = $fh->ActionGet($post);
	if ($id_topic>0)
	{
		$t = new Topic($id_topic);
		$txt	= $fh->HttpPostUnescapedVar("txt");
		$fm = new FileManager;
		$fm->WritePage("pub/{$t->path}/robots.txt",$txt);
	}
	header("Location: ops.php?id=$id_topic");
}

if ($from=="people_import")
{
	$module_admin 	= $ah->CheckModule();
	$id_topic		= $fh->Null2Zero($post['id_topic']);
	$id_pt_group		= $fh->Null2Zero($post['id_pt_group']);
	$contact		= $fh->Checkbox2bool($post['contact']);
	$id_geo		= $fh->Null2Zero($post['id_geo']);
	$data		= $post['data'];
	if($module_admin || Modules::AmIAdmin(28))
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$num = $pe->Import($id_topic,$contact,$id_geo,$id_pt_group,$data);
		if($num>0)
		{
			$ah->MessageSetTranslated("$num accounts created");
		}	
	}
	header("Location: ops.php?id=$id_topic");
}
?>
