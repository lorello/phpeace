<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
include_once(SERVER_ROOT."/../classes/resources.php");
$fo = new Forms();

$id_topic = (int)$get['id_topic'];
$id_form = (int)$get['id_form'];
$id_param = (int)$get['id'];

if ($module_admin)
    $input_right = 1;

$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
    $t = new Topic($id_topic);
    if ($id_topic==$form['id_topic'] && $t->AmIAdmin())
        $input_right = 1;
    $title[] = array($t->name,'ops.php?id='.$id_topic);
    $trs = new Translator($hh->tr->id_language,0,false,$t->id_style);
}
else 
{
	$t = new Topic(0);
    $trs = new Translator($hh->tr->id_language,0,false,0);
}

$title[] = array('forms','forms.php?id='.$id_topic);

$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array("fields",'form_params.php?id='.$id_form.'&id_topic='.$id_topic);

if ($id_param>0)
{
    $row = $fo->Param($id_param,$id_form);
    $title[] = array($row['label'],'');
}
else
{
    $title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
    $posts = array();
    $num_posts = $fo->Posts($posts,$id_form,$id_topic);
    $tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
if($form['weights'])
	$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);

$r = new Resources();
$types = $r->ParamsTypes(false,$form['store'],true);

echo $hh->input_form_open();
echo $hh->input_hidden("from","form_param");
echo $hh->input_hidden("id_form",$id_form);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_param",$id_param);
echo $hh->input_hidden("id_type_old",array_search($row['type'],$types));
echo $hh->input_table_open();
$param_label = "";
if($form['multilanguage'] && $id_param>0)
{
    $param_label = $trs->Translate($row['label']);
    if($row['type']=="geo")
    {
        include_once(SERVER_ROOT."/../classes/geo.php");
        $geo = new Geo();
        $geo_location = (int)$row['type_params'];
        if($geo_location>0 && !$form['profiling']>0)
            $geo->geo_location = $geo_location;
        $geo_label = $geo->Label();
        $param_label = $trs->Translate($geo_label);
    }
    $param_label = " ($param_label)";
}
echo $hh->input_text(($form['multilanguage']? "label":"name"),"label",$row['label'],20,0,$input_right,$param_label);
$t_types = array();       
foreach($types as $key=>$type)
    $t_types[$key] = $hh->tr->Translate("paramtype_" . $type);

echo $hh->input_array("type","id_type",array_search($row['type'],$types),$t_types,$input_right);
if($row['type']=="dropdown" || $row['type']=="mchoice" || $row['type']=="dropdown_open" || $row['type']=="mchoice_open" || $row['type']=="radio" )
{
    if($row['params']=="")
        echo $hh->input_note("select_insert");
    echo $hh->input_text("parameters","type_params",$row['type_params'],60,0,$input_right);
}
if($form['weights'] && ($row['type']=="checkbox" || $row['type']=="dropdown" || $row['type']=="mchoice" || $row['type']=="dropdown_open" || $row['type']=="mchoice_open" || $row['type']=="radio"))
{
    if($row['weights']=="")
        echo $hh->input_note("select_weights");
	echo $hh->input_text("poll_answers_weights","type_weights",$row['type_weights'],30,0,$input_right);
}

if($row['type']=="subscription")
{
	$tgroups = array();
	$num_groups = $t->PeopleGroups($tgroups,false,true);
	if($num_groups>0)
		echo $hh->input_row("group","type_params",$row['type_params'],$tgroups,"none_option",0,$input_right);
}
if($row['type']=="upload")
{
    echo $hh->input_array("file","type_params",$row['type_params'],$hh->tr->Translate("upload_options"),$input_right);
}
if($row['type']=="upload_image")
{
    include_once(SERVER_ROOT."/../classes/galleries.php");
    $gg = new Galleries();
    $galleries = $gg->AllGalleries(true);
    if(count($galleries)>0)
        echo $hh->input_galleries($row['type_params'],$galleries,"choose_option",$input_right,"type_params");
}
if($row['type']=="geo" && !$form['profiling']>0)
{
    echo $hh->input_array("geo_location","type_params",($row['type_params']>0)?$row['type_params']:$hh->ini->Get("geo_location"),$hh->tr->Translate("geo_options"),$input_right);
}

if($row['type']=="text")
{
	echo $hh->input_array("usage","type_params",is_numeric($row['type_params'])?$row['type_params']:0,$hh->tr->Translate("form_param_text_uses"),$input_right);
}
echo $hh->input_text("value","value",$row['value'],20,0,$input_right);
echo $hh->input_checkbox("mandatory","mandatory",$row['mandatory'],0,$input_right);
echo $hh->input_checkbox("email_subject","email_subject",$row['email_subject'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_param>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if ($id_param>0)
{
	echo "<p>" . $hh->tr->Translate("label") . ": <input type=\"text\" name=\"label_form_param_{$id_param}\" size=\"14\" class=\"code-box\" value=\"[[FP{$id_param}]]\"  onClick=\"this.select();\"></input></p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
