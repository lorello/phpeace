<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/articles.php");

$id_topic = $_GET['id_topic'];
$sort_by = (int)$_GET['sort_by'];
if(!$sort_by>0)
	$sort_by = 1;

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('search','articles_search.php?id_topic='.$id_topic);
$title[] = array('results','');
echo $hh->ShowTitle($title);

$params = array(	'title' => $get['title'],
					'author' => $get['author'],
					'content' => $get['content'],
					'id_topic' => (int)$get['id_topic'],
					'id_subtopic' => (int)$get['id_subtopic'],
					'id_article' => (int)$get['id_article'],
					'id_template' => (int)$get['id_template'],
					'approved' => (int)$get['approved'],
					'written1' => $get['written1_y']."-".$get['written1_m']."-".$get['written1_d'],
					'written2' => $get['written2_y']."-".$get['written2_m']."-".$get['written2_d']
					);

$num = Articles::Search( $row, $params, $sort_by );

$table_headers = array('image','date','title','in','author','vis');
$table_content = array('{ThumbImage($row[id_image],"images",$row[format],$row[id_image])}','{FormatDate($row[written_ts])}','{LinkTitle("../articles/article.php?w=topics&id=$row[id_article]",$row[headline])}',
'{PathToSubtopic($row[id_topic],$row[id_subtopic])}','$row[author]','<div class=\"right\">$row[id_visibility]</div>');

$topic_templates = $t->Templates(5);
if(count($topic_templates)>0 && $get['id_template']=="-1")
{
	$table_headers[] = "template";
	$table_content[] = '{TemplateLookup($row[id_template])}';
}

echo $hh->showTable($row, $table_headers, $table_content, $num);

$sort_options = $hh->tr->Translate("sort_by_options");
echo "<p class=\"notes\">" . $hh->tr->Translate("sort_articles_by") . ": " . $sort_options[$sort_by] . "</p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

