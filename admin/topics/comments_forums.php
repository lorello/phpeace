<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$approved = 0;

$id_topic = $_GET['id'];
$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','forums.php?id='.$id_topic);
$title[] = array("comments_to_approve",'');
echo $hh->ShowTitle($title);

$row = array();
$co = new Comments("thread",0);
$num = $co->CommentsApproved( $row, $approved, $id_topic );

$table_headers = array('date','author','thread','comment');
$table_content = array('{FormatDate($row[insert_date_ts])}','$row[name]',
'{LinkTitle("forum_thread.php?id=$row[id_item]&id_forum=$row[id_topic_forum]",$row[thread])}',
'{LinkTitle("comment.php?type=thread&id=$row[id_comment]&id_item=$row[id_item]",$row[title])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

