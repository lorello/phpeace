<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = (int)$_GET['id_topic'];
$id_pt_group = (int)$_GET['id'];

$t = new Topic($id_topic);
if ($t->AmIAdmin() || $module_admin)
	$input_right = 1;
$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('groups','visitors_groups.php?id='.$id_topic);

if ($id_pt_group>0)
{
	$row = $t->PeopleGroupGet($id_pt_group);
	$title[] = array($row['name'],'');
	if($id_topic != $row['id_topic'])
		$input_right = 0;
}
else
{
	$title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","people_group");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_pt_group",$id_pt_group);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],30,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_pt_group>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
