<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/rssmanager.php");

$id_feed = (int)$_GET['id'];
$id_group = (int)$_GET['id_group'];
$id_topic = (int)$_GET['id_topic'];

if($module_admin)
	$input_right = 1;
	
if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	if($t->AmIAdmin())
		$input_right = 1;	
}

$title[] = array('RSS feeds','rss_groups.php?id_topic='.$id_topic);

$rsm = new RssManager();

$group = $rsm->GroupGet($id_group);
$title[] = array($group['name'],"rss_group.php?id=$id_group&id_topic=$id_topic");

if ($id_feed>0)
{
	$row = $rsm->RssGet($id_feed);
	$title[] = array($row['id_feed'],'');
}
else
{
	$title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","rss_feed");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
if($id_feed>0)
	echo $hh->input_text("ID","id_feed",$id_feed,50,0,0);
$groups = array();
$rsm->Groups($groups,$id_topic,false);
echo $hh->input_row("group","id_group",$id_group,$groups,"",0,$input_right);
echo $hh->input_text("URL","url",$row['url'],50,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_feed>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
