<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");

$id_campaign = $_GET['id'];
$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);

if ($id_campaign>0)
{
	$c = new Campaign($id_campaign);
	$action2 = "update";
	$row = $c->CampaignGet();
	$title[] = array($row['name'],'');
	$approve_comments = $row['approve_comments'];
	$orgs_sign = $row['orgs_sign'];
	$active = $row['active'];
}
else
{
	$action2 = "insert";
	$approve_comments = 1;
	$orgs_sign = 1;
	$active = 1;
	$title[] = array('campaign_new','');
}

if($ini->Get("profiling")=="0")
	$ah->MessageSet("profiling_off");
echo $hh->ShowTitle($title);

if($id_campaign>0)
{
	$url = $ini->Get("pub_web") . "/" . $ini->Get("campaign_path") . "/index.php?id=$id_campaign&id_topic=$id_topic";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","campaign");
echo $hh->input_hidden("id_campaign",$id_campaign);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($id_topic,0,$tt->AllTopics(),"",$input_super_right);

echo $hh->input_date("date_start","start_date",$row['start_date_ts'],$input_right);
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,10,"",$input_right);
echo $hh->input_textarea("thanks_text","thanks",$row['thanks'],80,7,"",$input_right);
echo $hh->input_textarea("thanks_email","thanks_email",$row['thanks_email'],80,5,"",$input_right);

echo $hh->input_separator("promoter");
echo $hh->input_text("name","promoter",$row['promoter'],50,0,$input_right);
echo $hh->input_text("email","promoter_email",$row['promoter_email'],50,0,$input_right);
echo $hh->input_separator("details");
echo $hh->input_wysiwyg("description_long","description_long",$row['description_long'],$row['is_html'],15,$input_right,$t->wysiwyg);

echo $hh->input_separator("signatures");
echo $hh->input_textarea("sign_text","pre_sign",$row['pre_sign'],80,5,"",$input_right);
echo $hh->input_array("users","users_type",$row['users_type'],$hh->tr->Translate("allow_users"),$input_right);
echo $hh->input_checkbox("orgs_sign","orgs_sign",$orgs_sign,0,$input_right);
echo $hh->input_checkbox("moderate_comments","approve_comments",$approve_comments,0,$input_right);
$combo_values = array();
$combo_values[0] = "Default ($t->records_per_page)";
for($i=1;$i<=50;$i++)
	$combo_values["".$i.""] = $i;
echo $hh->input_array("signatures_per_page","signatures_per_page",$row['signatures_per_page'],$combo_values,$input_right);

echo $hh->input_separator("sign_notify");
echo $hh->input_text("recipient","notify",$row['notify'],50,0,$input_right);
echo $hh->input_textarea("notify_text","notify_text",$row['notify_text'],80,5,"",$input_right);

echo $hh->input_separator("funding");
echo $hh->input_checkbox("funding","money",$row['money'],0,$input_right);
if($id_campaign>0 && $row['money'])
{
	$trm15 = new Translator($hh->tr->id_language,15);
	if(!$row['id_payment_type']>0)
		echo $hh->input_note($trm15->Translate("missing_payment_type"),TRUE);
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$types = array();
	$p->Types($types,$id_topic,false);
	echo $hh->input_row($trm15->Translate("payment_type"),"id_payment_type",$row['id_payment_type'],$types,"choose_option",0,$input_right);

	$currencies = $p->currencies;
	$currency = $currencies[$ini->Get("default_currency")];
	echo $hh->input_text("amount","amount",$row['amount'],15,0,$input_right,$currency . " <br><span class=\"notes\">" . $hh->tr->Translate("amount_note") . "</span>");
	echo $hh->input_checkbox("editable","editable",$row['editable'],0,$input_right);
	
	echo "<tr><td valign=\"top\">";
	echo "<h3 style=\"text-align:right;\">" . $hh->tr->Translate("accounts") . "</h3>\n";
	echo "</td><td>";
	$accounts = $p->AccountsUse(1,$c->id);
	echo "<ul>";
	if(count($accounts)>0)
	{
		$currencies = $p->currencies;
		$currency = $currencies[$ini->Get("default_currency")];
		$account_types = $hh->tr->Translate("account_types");
		foreach($accounts as $account)
		{
			echo "<li><a href=\"campaign_account.php?id={$account['id_use']}&id_campaign=$id_campaign&id_topic=$id_topic\">{$account['name']}</a>";
			echo " ({$account_types[$account['id_type']]}";
			if($account['amount']!="")
				echo " - {$account['amount']} $currency";
			echo ")</li>\n";
		}
	}
	if ($input_right)
	{
		$av_accounts = $p->AccountsUseAvailable(1,$c->id,$id_topic);
		if(count($av_accounts)>0)
			echo "<li><a href=\"campaign_account.php?id=0&id_campaign=$id_campaign&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></li>\n";	
	}
	echo "</ul>";
}
echo $hh->input_separator("administration");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
echo $hh->input_keywords($id_campaign,$o->types['campaign'],$keywords,$input_right);
echo $hh->input_array("status","active",$active,$hh->tr->Translate("status_options"),$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_campaign>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if ($id_campaign>0)
{
	echo "<p><a href=\"/topics/text.php?id_topic=$id_topic&id_type=6\">" . $hh->tr->Translate("privacy_warning") . "</a></p>\n";
	$num1 = $c->PersonsApproveCounter( 1 );
	$num2 = $c->OrgsApprove( $rows, 1 );
	echo "<h3>" . $hh->tr->Translate("signatures") . "</h3>\n<ul>";
	echo "<li><a href=\"campaign_persons.php?id=$id_campaign&id_topic=$id_topic&appr=1\">$num1 " . $hh->tr->Translate("persons") . "</a>";
	echo " (<a href=\"campaign_person.php?id=0&id_c=$id_campaign&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a>)</li>\n";
	if($orgs_sign)
	{
		echo "<li><a href=\"campaign_orgs.php?id=$id_campaign&id_topic=$id_topic&appr=1\">$num2 " . $hh->tr->Translate("orgs") . "</a>";
		echo " (<a href=\"campaign_org.php?id=0&id_c=$id_campaign&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a>)</li>\n";
	}
	echo "<li><a href=\"campaign_search.php?id=$id_campaign&id_topic=$id_topic\">" . $hh->tr->Translate("search") . "</a></li>\n";
	if ($input_right && $active>0)
		echo "<li><a href=\"campaign_mail.php?act=filter&id_item=$id_campaign\">" . $hh->tr->Translate("mailjob") . "</a></li>\n";
	echo "</ul>";

	echo "<h3>" . $hh->tr->Translate("to_approve") . "</h3>\n<ul>";
	$num3 = $c->PersonsApproveCounter( 0 );
	echo "<li><a href=\"campaign_persons.php?id=$id_campaign&id_topic=$id_topic&appr=0\">$num3 " . $hh->tr->Translate("persons") . "</a></li>\n";
	if($orgs_sign)
	{
		$num4 = $c->OrgsApprove( $rows, 0 );
	 	echo "<li><a href=\"campaign_orgs.php?id=$id_campaign&id_topic=$id_topic&appr=0\">$num4 " . $hh->tr->Translate("orgs") . "</a></li>\n";
	}
	echo "</ul>\n";

	if($num1>0 || ($orgs_sign && $num2>0))
	{
		echo "<h3>CSV dump</h3>\n<ul>";
		if($num1>0)
			echo "<li><a href=\"campaign_persons_csv.php?id=$id_campaign&id_topic=$id_topic\">$num1 " . $hh->tr->Translate("persons") . "</a></li>\n";
		if($orgs_sign && $num2>0)
		{
		 	echo "<li><a href=\"campaign_orgs_csv.php?id=$id_campaign&id_topic=$id_topic\">$num2 " . $hh->tr->Translate("orgs") . "</a></li>\n";
		}
		echo "</ul>\n";	
	}

	echo "<h3><a href=\"campaign_status.php?id=$id_campaign\">" . $hh->tr->Translate("status_changes") . "</a></h3>";
	
	if ($row['money'])
	{
		echo "<h3>" . $hh->tr->Translate("funding") . "</h3>\n<ul>";
		$total = $c->SupportTotal();
		echo "<p>" . $hh->tr->Translate("total") . ": $total</p>\n";
	}

//	if($module_admin)
//		echo "<p><a href=\"campaign_import.php?id=$id_campaign&id_topic=$id_topic\">Import signatures from SQL dump</a></p>\n";
	
}
include_once(SERVER_ROOT."/include/footer.php");
?>

