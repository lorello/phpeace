<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/topic.php");
$ah = new AdminHelper;
$ah->CheckAuth();

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

$qs = $_SERVER['QUERY_STRING'];

$conf = new Configuration();
$awstats_path = $conf->Get("awstats");
 
if ($awstats_path!="" && $t->row['awstats']!="" && ($ah->ModuleAdmin(4) || $t->AmIUser()))
{
	$awstats = $t->row['awstats'];
	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager;
	$url = "$awstats_path/awstats.pl?config=$awstats&$qs";
	$stats = $fm->Browse($url,true);
	$stats = str_replace("awstats.pl?config=$awstats&amp;","awstats.php?",$stats);
	$stats = str_replace("awstats.pl?config=$awstats&","awstats.php?",$stats);
	$stats = str_replace("<input type=\"hidden\" name=\"config\" value=\"$awstats\" />","<input type=\"hidden\" name=\"config\" value=\"$awstats\" />\n<input type=\"hidden\" name=\"id\" value=\"$id_topic\" />",$stats);
	echo $stats;
}

?>

