<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/link.php");

$id_link = $_GET['id'];
$id_topic = $_GET['id_topic'];
$error = $_GET['error'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
{
	$input_right = 1;
	$input_super_right = 1;
}

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('Links','links.php?id='.$id_topic);

if ($id_link>0)
{
	$l = new Link($id_link);
	$action2 = "update";
	$row = $l->LinkGet();
	$id_language = $row['id_language'];
	$vote = $row['vote'];
	$approved = $row['approved'];
	$title[] = array($row['title'],'');
	$id_subtopic = $row['id_subtopic'];
}
else
{
	$action2 = "insert";
	$id_language = $t->id_language;
	$vote = 3;
	if ($t->AmIUser())
		$input_right = 1;
	$title[] = array('link_new','');
	$id_subtopic = 0;
	$links_subtopics = $t->SubtopicsByType($t->subtopic_types['links']);
	if(count($links_subtopics)==1)
		$id_subtopic = $links_subtopics[0]['id_subtopic'];
}
echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function dosubmit(strAction)
{
	f=document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.id_topic.selectedIndex==0)
	{
		strAlert = strAlert+"<?=$hh->tr->Translate("article_v_topic");?>\n";
		boolOK = false;
	}
	if (f.title.value=="")
	{
		strAlert=strAlert+"<?=$hh->tr->Translate("article_v_title");?>\n";
		boolOK = false;
	}
	if (!(f.id_subtopic.value>0))
	{
		strAlert=strAlert+"<?=$hh->tr->Translate("article_v_subtopic");?>\n";
		boolOK = false;
	}
	if (boolOK==true)
	{
		f.action2.value = strAction;
		f.target = '';
		f.submit();
	}
	else
		alert(strAlert);
}

</script>

<form method="post" action="actions.php" name="form1">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="id_link" value="<?=$id_link;?>">
<input type="hidden" name="from" value="link">
<input type="hidden" name="approved_old" value="<?=$row['approved'];?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
if ($id_link>0)
{
	echo $hh->input_separator("check");
	echo "<tr><td>&nbsp;</td><td><a href=\"link_check.php?id=$id_link&id_topic=$id_topic\">" . $hh->tr->Translate("click_here") ."</a> " . $hh->tr->TranslateParams("link_check",array("<a href=\"{$row['url']}\" target=\"_blank\">{$row['url']}</a>"));
	if  ($error==1)
		echo "<br>" . $hh->tr->Translate("link_check_ok");
	echo $hh->input_checkbox("error404","error404",$row['error404'],0,$input_right);
	echo "</td></tr>\n";
}
echo $hh->input_separator("insertion");
echo $hh->input_date("date","insert_date",$row['insert_date'],$input_right);
echo $hh->input_text("link","url",$row['url'],75,0,$input_right);
echo $hh->input_textarea("title","title",$row['title'],75,2,"",$input_right);
echo $hh->input_textarea("description","description",$row['description'],75,5,"",$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
if ($id_topic>0)
	$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
if ($id_subtopic>0)
	$o->GetKeywords($id_subtopic,$o->types['subtopic'],$keywords,0,true);
echo $hh->input_keywords($id_link,$o->types['link'],$keywords,$input_right);

$combo_values = array('5'=>'*****','4'=>'****','3'=>'***','2'=>'**','1'=>'*');
echo $hh->input_array("evaluation","vote",$vote,$combo_values,$input_right);

echo $hh->input_array("language","id_language",$id_language,$hh->tr->Translate("languages"),$input_right);

echo $hh->input_separator("placing");

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
if ($input_super_right==1)
	$topics = $tt->AllTopics();
else
	$topics = $tt->User($ah->current_user_id);
	
echo $hh->input_topics($id_topic,0,$topics,"choose_option",$input_right);
echo $hh->input_subtopic("subtopic","id_subtopic","'link_subtopic.php?id_topic='+document.forms['form1'].id_topic.value+'&id_subtopic='+document.forms['form1'].id_subtopic.value",$id_subtopic,$id_topic,$input_right);

if($id_topic>0)
{
	$tikeywords = $t->KeywordsInternal($o->types['link']);
	echo $hh->input_internal_keywords($id_link,$tikeywords,"link",$input_super_right,$input_right);
}

echo $hh->input_separator("administration");
echo $hh->input_checkbox("approved","approved",$approved,0,$input_super_right);
echo "<tr><td>&nbsp;</td><td>";
if ($input_right==1)
{
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"dosubmit('$action2')\">";
	if ($id_link>0)
		echo "&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"" . $hh->tr->Translate("delete") . "\" onClick=\"dosubmit('delete')\">";
}
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


