<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();

$id_topic = (int)$_GET['id_topic'];
$id_type = ($_GET['id_type']!="")? (int)$_GET['id_type']:-1;
$id_feature = $_GET['id'];

if ($module_admin)
	$input_right = 1;
$layout_admin = $ah->ModuleAdmin(10);
$is_system = false;

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('Features','features.php?id='.$id_topic);


if ($id_feature>0)
{
	$row = $pt->ft->FeatureGet($id_feature);
	if(!(isset($row['id_feature'])))
		$hh->Stop();
	$ptype = $pt->ft->FeatureGetTypeAndModule($id_feature);
	$id_type = $ptype['id_type'];
	$id_module = $ptype['id_module'];
	$title[] = array($row['name'],'');
	$id_user = $row['id_user'];
	$active = $row['active'];
	if ($id_user == "0")
	{
		$is_system = true;
		$input_right = 0;
	}
}
else
{
	$title[] = array('add_new','');
	$id_user = $ah->current_user_id;
	$active = 1;
}


echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();

echo $hh->input_form_open();
echo $hh->input_hidden("from","feature");
echo $hh->input_hidden("id_feature",$id_feature);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("old_id_function",$row['id_function']);
echo $hh->input_hidden("old_id_type",$id_type);
echo $hh->input_table_open();

if ($id_feature>0 && $ah->ModuleUser(10))
{
	$id_style = (int)$t->id_style;
	if($id_module>0)
	{
		$xsl_link = "/layout/xsl_module.php?id=$id_module&id_style=$id_style";
		$module = Modules::ModuleGet($id_module);
		$xsl_pagetype = $module['path'];
	}
	else 
	{
		$xsl_link = "/layout/xsl.php?id=$id_type&id_style=$id_style";
		$xsl_pagetype = array_search($id_type,$pt->types);
	}
	echo $hh->input_note($hh->input_code("xsl_include","To call this feature in " . (($id_type==0 && $id_module==0)? " any XSL":"<a href=\"$xsl_link\">{$xsl_pagetype}.xsl</a>"),"<xsl:apply-templates select=\"/root/" . (($id_type==0 && $id_module==0)? "c_":"" ) . "features/feature[@id='$id_feature']\" />"));
	echo $hh->input_note("<a href=\"feature_preview.php?id=$id_feature&id_topic=$id_topic\" target=\"_blank\">XML " . $hh->tr->Translate("preview") . "</a>");
	if($row['public'])
	{
		echo $hh->input_note("Public link: " . $ini->Get("pub_web") . "/js/feature.php?id=$id_feature");
	}
}
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,3,"",$input_right);
$hhf = new HHFunctions();
$ptypes = $hh->tr->Translate("page_types");
unset($ptypes['20']);
unset($ptypes['21']);
$active_modules = Modules::AvailableModules();
if(count($active_modules)>0)
{
	$t_modules = $hh->tr->Translate("modules_names");
	foreach($active_modules as $module)
	{
		if($module['layout'] && !$module['internal'] && !$module['global'])
		{
			$ptypes['m'.$module['id_module']] = "Module: " . $t_modules[$module['id_module']];
		}
	}
}
echo $hh->input_array("page_type","id_type",$id_module>0?"m".$id_module:$id_type,$ptypes,$input_right);

$desc = $hh->tr->Translate("function");
if ($id_feature>0)
{
	$params = $pt->ft->params[$row['id_function']];
	if (count($params)>0)
	{
		$desc .= " (<a href=\"feature_params.php?id=$id_feature&id_topic=$id_topic\">" . $hh->tr->Translate("parameters") ."</a>)";
	}
}
echo $hh->input_array($desc,"id_function",$row['id_function'],$hh->tr->Translate("page_functions"),$input_right);

echo $hh->input_separator("conditions");
echo $hh->input_text("ID","condition_id",$row['condition_id'],5,0,$input_right);
echo $hh->input_condition("querystring",$row['condition_var'],$row['condition_type'],$row['condition_value'],$input_right);

echo $hh->input_separator("administration");
echo $hh->input_checkbox("public_xml","public",$row['public'],0,$input_right);
echo $hh->input_text("author","id_user",$hhf->UserLookup($id_user),50,0,0);
echo $hh->input_checkbox("active","active",$active,0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right && !$is_system));
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>(!$is_system && $id_feature>0 && ($module_admin || $layout_admin)));
$actions[] = array('action'=>"activeswap",'label'=>(($row['active'])?"deactivate":"activate"),'right'=>$is_system);
echo $hh->input_actions($actions,$input_right || $is_system);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

