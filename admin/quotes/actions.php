<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/quotes.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$from		= $post['from'];

if ($from=="quote")
{
	$action 	= $fh->ActionGet($post);
	$id_quote 	= $post['id_quote'];
	$quote 	= $post['quote'];
	$module 	= $post['module'];
	$author 	= $post['author'];
	$notes	 	= $post['notes'];
	$id_language 	= $post['id_language'];
	$id_topic 	= $fh->String2Number($post['id_topic']);
	$approved 	= $fh->Checkbox2bool($post['approved']);
	$keywords 	= $post['keywords'];
	$q = new Quotes;
	if ($action=="update")
		$q->QuoteUpdate($id_quote, $quote, $author, $notes, $approved, $keywords, $id_language, $id_topic);
	if ($action=="insert")
		$q->QuoteInsert($quote, $author, $notes, $approved, $keywords, $id_language, $id_topic);
	if ($action=="delete")
		$q->QuoteDelete($id_quote);
	$row = array();
	$num = $q->QuotesApproved( $row, $id_topic, 0 );
	$go_approved = ($num>0)? 0:1;
	if($module=="topics")
		header("Location: /topics/quotes.php?approved=$go_approved&id_topic=$id_topic");
	else
		header("Location: /quotes/quotes.php?approved=$go_approved");
}

if ($from=="config_update")
{
	$path 		= $post['path'];
	$q = new Quotes;
	$q->ConfigurationUpdate($path);
	header("Location: index.php");
}
?>
