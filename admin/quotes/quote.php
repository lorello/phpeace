<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/quotes.php");

$id_quote = $_GET['id'];
$approved = (int)$_GET['approved'];

$title[] = array((($approved==1)? "quotes_approved" : "quotes_to_approve"),'quotes.php?id='.$id_quote.'&approved='.$approved);

if ($id_quote>0)
{
	$action2 = "update";
	$quotes = new Quotes;
	$row = $quotes->QuoteGet($id_quote);
	$title[] = array('change','');
}
else
{
	$action2 = "insert";
	$title[] = array('quotes_new','');
	$input_right = 1;
}

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			author: "required",
			quote: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","quote");
echo $hh->input_hidden("id_quote",$id_quote);
echo $hh->input_table_open();
echo $hh->input_textarea("quote","quote",$row['quote'],80,7,"",$input_right);
echo $hh->input_text("author","author",$row['author'],50,0,$input_right);
echo $hh->input_textarea("author_notes","notes",$row['notes'],80,4,"",$input_right);
echo $hh->input_array("language","id_language",$row['id_language'],$hh->tr->Translate("languages"),$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($row['id_topic'],0,$tt->AllTopics(),"all_option",$input_right);

echo $hh->input_checkbox("approved","approved",$row['approved'],0,$input_super_right);
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id_quote,$o->types['quote'],$keywords,$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($id_quote>0 && $input_super_right));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

