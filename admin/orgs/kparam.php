<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$title[] = array('categories','topics.php');

$trm8 = new Translator($hh->tr->id_language,8);

$id_keyword = $_GET['id_k'];
$id_kparam = $_GET['id'];

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->LoadTree(ASSO_KEYWORD_TYPE);
if($id_keyword>0)
{
	$parents = $o->KeywordPath($id_keyword);
	foreach($parents as $parent)
		if($parent['id_keyword']!=$id_keyword)
			$title[] = array($parent['keyword'],'topic.php?id=' . $parent['id_keyword']);
}

include_once(SERVER_ROOT."/../classes/keyword.php");
$k = new Keyword();
if($id_keyword>0)
{
	$row = $k->KeywordGet($id_keyword);
	$title[] = array($row['keyword'],'topic.php?id='.$id_keyword);
}

if ($id_kparam>0)
{
	$row2 = $k->ParamGet($id_kparam);
	$title[] = array($row2['label'],'');
}
else
{
	$title[] = array($trm8->Translate("field_add"),'');
}

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$types = $k->ParamsTypes(true);

echo $hh->input_form_open();
echo $hh->input_hidden("from","keyword_param");
echo $hh->input_hidden("id_keyword",$id_keyword);
echo $hh->input_hidden("id_kparam",$id_kparam);
echo $hh->input_hidden("id_type_old",array_search($row2['type'],$types));
echo $hh->input_table_open();
echo $hh->input_text("name","label",$row2['label'],20,0,$input_right);
$t_types = array();
foreach($types as $key=>$type)
	$t_types[$key] = $hh->tr->Translate("paramtype_" . $type);

echo $hh->input_array("type","type",array_search($row2['type'],$types),$t_types,$input_right);
if($row2['type']=="dropdown" || $row2['type']=="mchoice")
{
	if($row2['params']=="")
		echo $hh->input_note("select_insert");
	echo $hh->input_text("parameters","kparam_params",$row2['params'],20,0,$input_right);
}
echo $hh->input_checkbox("search_advanced","index_include",$row2['index_include'],0,$input_right);
echo $hh->input_checkbox("public","public",$row2['public'],0,$input_right);
echo $hh->input_checkbox("list_include","list_include",$row2['list_include'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_kparam>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>

