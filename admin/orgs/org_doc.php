<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$id_org = $_GET['id_org'];
$id_doc = $_GET['id'];

$asso = new Asso($id_org);

$o = new Ontology;
$fm = new FileManager;
$maxfilesize = $fm->MaxFileSize();

if ($module_admin || $module_right)
	$input_right = 1;

$keywords = array();
if($id_doc>0)	
{
	include_once(SERVER_ROOT."/../classes/doc.php");
	$d = new Doc($id_doc);
	$doc = $d->DocGet();
	$doc_title = $doc['title'];
	$id_language = $doc['id_language'];
	$id_licence = $doc['id_licence'];
	$filename = "docs/{$id_doc}.{$doc['format']}";
}
else 
{
	$doc_title = "add_new";
	$id_language = $hh->tr->id_language;
	$id_licence = $hh->ini->Get("id_licence");	
}

$title[] = array($asso->name,'org.php?id='.$id_org);
$title[] = array("docs",'org_docs.php?id='.$id_org);
$title[] = array($doc_title,'');
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('details','org.php?id='.$id_org);
$tabs[] = array('categories','keywords.php?id='.$id_org);
$tabs[] = array('additional_fields','params.php?id='.$id_org);
$tabs[] = array('image_associated','org_image.php?id='.$id_org);
$tabs[] = array('docs','org_docs.php?id='.$id_org);
echo $hh->Tabs($tabs);
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			title: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_org",$id_org);
echo $hh->input_hidden("from","org_doc");
echo $hh->input_hidden("id_doc",$id_doc);
echo $hh->input_table_open();

if($id_doc>0)
{
	echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a>" . $hh->FileSize($filename) . "</td></tr>\n";
	echo $hh->input_upload("substitute_with","doc",50,$input_right);
}
else 
{
	echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
	echo $hh->input_upload("choose_file","doc",50,$input_right);
}

echo $hh->input_text("title","title",$doc['title'],"50",0,$input_right);
echo $hh->input_textarea("description","description",$doc['description'],70,5,"",$input_right);
echo $hh->input_text("author","author",$doc['author'],80,0,$input_right);
echo $hh->input_textarea("source","source",$doc['source'],70,3,"",$input_right);
echo $hh->input_array("language","id_language",$id_language,$hh->tr->Translate("languages"),$input_right);
echo $hh->input_keywords($id_doc,$o->types['document'],$keywords,$input_right);
if ($hh->ini->Get("licences"))
	echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"doc_remove",'right'=>$input_right && $id_doc>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if ($id_doc==0)
	echo $hh->tr->Translate("doc_instr");

include_once(SERVER_ROOT."/include/footer.php");
?>

