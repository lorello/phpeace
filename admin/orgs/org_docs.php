<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$id = $_GET['id'];

$asso = new Asso($id);

if ($module_admin || $module_right)
	$input_right = 1;

$title[] = array($asso->name,'org.php?id='.$id);
$title[] = array("docs",'');
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('details','org.php?id='.$id);
$tabs[] = array('categories','keywords.php?id='.$id);
$tabs[] = array('additional_fields','params.php?id='.$id);
$tabs[] = array('image_associated','org_image.php?id='.$id);
$tabs[] = array('docs','');
echo $hh->Tabs($tabs);

$row = array();
$num = $asso->Docs( $row );

$table_headers = array('document','description','file');
$table_content = array('{LinkTitle("org_doc.php?id=$row[id_doc]&id_org='.$id.'",$row[title])}','$row[description]','{FileSize("docs/".$row[id_doc].".".$row[format])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if($input_right)
	echo "<p><a href=\"org_doc.php?id=0&id_org=$id\">" . $hh->tr->Translate("add_new"). "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

