<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$as = new Assos();

$title[] = array('types','types.php');

$id = $_GET['id'];

if ($id>0)
{
	$row = $as->Type($id);
	$title[] = array('change','');
	$assos = $as->AssoType($id);
	$num_assos = count($assos);
}
else
{
	$title[] = array('add_new','');
	$num_assos = 0;
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","ass_type");
echo $hh->input_hidden("id_type",$id);
echo $hh->input_table_open();
echo $hh->input_text("type","type",$row['ass_tipo'],40,0,$input_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id>0 && $num_assos==0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id>0 && $num_assos>0)
	echo "<p>" . $hh->tr->Translate("used_in") . " <a href=\"search.php?id_type=$id&id_geo=0\">$num_assos " . $hh->tr->Translate("records") . "</a></p>";
	
include_once(SERVER_ROOT."/include/footer.php");
?>

