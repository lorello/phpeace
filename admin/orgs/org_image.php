<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/file.php");

$trm8 = new Translator($hh->tr->id_language,8);

$id = $_GET['id'];

$asso = new Asso($id);

$fm = new FileManager;
$maxfilesize = $fm->MaxFileSize();

if ($module_admin || $module_right)
	$input_right = 1;

$title[] = array($asso->name,'org.php?id='.$id);
$title[] = array("image_associated",'');
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('details','org.php?id='.$id);
$tabs[] = array('categories','keywords.php?id='.$id);
$tabs[] = array('additional_fields','params.php?id='.$id);
$tabs[] = array('image_associated','');
$tabs[] = array('docs','org_docs.php?id='.$id);
echo $hh->Tabs($tabs);

echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_org",$id);
echo $hh->input_hidden("from","org_image");
echo $hh->input_table_open();
echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
echo $hh->input_note($trm8->Translate("jpg_only"));

$as = new Assos();
$i = new Images();
$filename = "orgs/1/$id".".".$i->convert_format;
$filename_orig = "orgs/orig/{$id}.jpg";
if($fm->Exists("uploads/$filename_orig"))
{
	echo "<tr><td align=\"right\">&nbsp;</td><td><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[$as->orgs_size]}\">";
	if ($fm->Exists("uploads/$filename_orig"))
		echo "&nbsp;<a href=\"/images/upload.php?src=$filename_orig\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a> (" . $fm->Size("uploads/$filename_orig") . ")";
	echo $hh->input_upload("substitute_with","img",50,$input_right);
}
else
	echo $hh->input_upload("choose_file","img",50,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $fm->Exists("uploads/$filename"));
echo $hh->input_actions($actions,$input_right && $id>0);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

