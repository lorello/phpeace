<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");
$post = $fh->HttpPost();

$trm8 = new Translator($hh->tr->id_language,8);

$id_module = 8;

$as = new Assos();

$mj = new Mailjobs($id_module);
$recipients = $mj->RecipientsGet();
$num_recipients = sizeof($recipients);

$id_item = $_GET['id_item'];
$act = $_GET['act'];  // type, filter, search, showdest, message, send, reset

$title[] = array('mailjob','');

if ($module_admin)
{
	if($id_item>0 && $mj->MailJobCheck($id_module,$id_item))
	{
		$ah->MessageSet("mailjob_already_present");
		echo $hh->ShowTitle($title);
	}
	else
	{
		echo $hh->ShowTitle($title);
		$mj_types = $trm8->Translate("mailjobs");
		switch($act)
		{
			case "filter":
				echo "<h3>" . $mj_types[$id_item] . " (<a href=\"mailjob.php?act=reset\">". $hh->tr->Translate("reset") .  "</a>)</h3>";
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . "</p>";
					echo "<h4><a href=\"mailjob.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
					echo "<p>" . $hh->tr->Translate("mailjob_filter_again") . "</p>";
				}
				else
					echo "<p>" . $hh->tr->Translate("mailjob_filter") . "</p>";
				echo $hh->input_form("post","mailjob.php?id_item=$id_item&act=search");
				echo $hh->input_table_open();
				switch($id_item)
				{
					case "1":
						echo $hh->input_text("name","name","",30,0,1);
						echo $hh->input_text("email","email","",30,0,1);
						echo $hh->input_row("type","id_type","",$as->Types(),"all_option",0,1);
						echo $hh->input_geo(0,1,1);
					break;
				}
				$actions[] = array('action'=>"search",'label'=>"search",'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "search":
				$db =& Db::globaldb();
				$search_params = array();
				$search_params['name'] = $post['name'];
				switch($id_item)
				{
					case "1":
						$search_params['email'] = $post['email'];
						$search_params['name'] = $post['name'];
						$search_params['id_type'] = $post['id_type'];
						$search_params['id_geo'] = $post['id_geo'];
					break;

				}	
				echo "<h3>" . $mj_types[$id_item] . " (<a href=\"mailjob.php?act=reset\">". $hh->tr->Translate("reset") .  "</a>)</h3>";
				$num = $as->MailjobSearch($id_item,$search_params);
				if($num>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4><a href=\"mailjob.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
					echo "<p><a href=\"mailjob.php?act=showdest&id_item=$id_item\">" . $hh->tr->Translate("mailjob_showdest") . "</a></p>";
				}
				else 
					echo "<p>" . $hh->tr->TranslateParams("num_results",array(0)) . "</p>";
					
			break;
			case "showdest":
				$delete = (int)$_GET['delete'];
				if($delete>0)
				{
					$mj->RecipientDelete($delete);
					$recipients = $mj->RecipientsGet();
					$num_recipients = sizeof($recipients);
				}
				echo "<h3>" . $mj_types[$id_item] . " (<a href=\"mailjob.php?act=reset\">". $hh->tr->Translate("reset") .  "</a>)</h3>";
				if($num_recipients>0)
				{
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
					echo "<h4><a href=\"mailjob.php?act=message&id_item=$id_item\">" . $hh->tr->Translate("mailjob_send") . "</a></h4>";
				}
				$row = array_slice($recipients,($current_page-1)*$records_per_page,$records_per_page);
				$table_headers = array('name','email','');
				switch($id_item)
				{
					case "1":
						$page_link = '{LinkTitle("org.php?id=$row[id]",$row[name])}';
					break;
				}
				$table_content = array($page_link,'$row[email]','{LinkTitle("mailjob.php?act=showdest&id_item='.$id_item.'&delete=$row[id]","(delete)")}');
				echo $hh->ShowTable($row, $table_headers, $table_content, sizeof($recipients));
			break;
			case "message":
				echo "<h3>" . $mj_types[$id_item] . " (<a href=\"mailjob.php?act=reset\">". $hh->tr->Translate("reset") .  "</a>)</h3>";
				if($num_recipients>0)
					echo "<p>" . $hh->tr->TranslateParams("mailjob_recipients",array($num_recipients)) . " <a href=\"mailjob.php?act=filter&id_item=$id_item\">" . $hh->tr->Translate("search_new") . "</a></p>";
				?>
				<script type="text/javascript">
					$().ready(function() {
					$("#form1").validate({
							rules: {
								name: "required",
								subject: "required",
								body: "required",
								email: {
									required: true,
									email: true
								}
							}
						});
					});
				</script>
				<?php	
				echo $hh->input_form("post","mailjob.php?act=send");
				echo $hh->input_hidden("id_item",$id_item);
				echo $hh->input_table_open();
				include_once(SERVER_ROOT."/../classes/user.php");
				$u = new User();
				$user = $u->UserGet();
				echo $hh->Input_date("date","start_date",0,1);
				echo $hh->input_time("hour","start_date",0,1);
				echo $hh->input_separator("sender");
				echo $hh->input_text("name","name",$user['name'],30,0,1);
				echo $hh->input_text("email","email",$user['email'],30,0,1);
				echo $hh->input_separator("message");
				echo $hh->input_text("subject","subject","",50,0,1);
				echo $hh->input_textarea("text","body","",80,20,"",1);
				if($mj->ProfilingGet())
				{
					echo $hh->input_checkbox("send_password","send_password","",0,1);				
					echo $hh->input_checkbox("track","track","",0,1);
					echo $hh->input_text("track_redirect","track_redirect","",30,0,1);
				}
				$actions[] = array('action'=>"send",'label'=>"send",'right'=>1);
				echo $hh->input_actions($actions,1);
				echo $hh->input_table_close() . $hh->input_form_close();
			break;
			case "send":
				include_once(SERVER_ROOT."/../classes/formhelper.php");
				$fh = new FormHelper(true);
				$post = $fh->HttpPost();
				$start_date 	= $fh->Strings2Datetime($post['start_date_d'],$post['start_date_m'],$post['start_date_y'],$post['start_date_h'],$post['start_date_i']);
				$name		= $post['name'];
				$email		= $post['email'];
				$subject	= $post['subject'];
				$body		= $post['body'];
				$send_password	= $fh->Checkbox2bool($post['send_password']);
				$track		= $fh->Checkbox2bool($post['track']);
				$track_redirect		= $fh->String2Url($post['track_redirect']);
				$id_item	= $post['id_item'];
				$fh->va->NotEmpty($name,"name");
				$fh->va->Email($email);
				$fh->va->NotEmpty($subject,"title");
				$fh->va->NotEmpty($body,"text");
				if($fh->va->return)
				{
					$unescaped_footer = $mj->MessageFooter($as->id_topic,$send_password);
					$mj->Store($start_date,$name,$email,$ah->current_user_id,$subject,$body,$id_module,$id_item,$recipients,$unescaped_footer,$send_password,0,array(),$track,$track_redirect,$as->id_topic);
					echo "<p>" . $hh->tr->Translate("mailjob_queued") . "</p>";
				}
				else 
					echo $hh->MessageShow();
			break;
			case "reset":
				$mj->RecipientsDelete();
				echo "<p><a href=\"mailjob.php\">" . $hh->tr->Translate("mailjob_reset") . "</a></p>";
			break;
			default:
				echo "<p>" . $hh->tr->Translate("mailjob_select") . "</p>";
				echo "<ul>";
				foreach($mj_types as $mj_id=>$mj_type)
					if($mj_id>0)
						echo "<li><a href=\"mailjob.php?act=filter&id_item=$mj_id\">$mj_type</a></li>" ;
				echo "</ul>";
			break;
		}
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
