<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$id = $_GET['id'];
$from = $_GET['from'];

$trm8 = new Translator($hh->tr->id_language,8);

$asso = new Asso($id);
$row = $asso->Get(false);

if ($module_admin ||  $from=="insert")
	$input_right=1;

$title[] = array($asso->name,'org.php?id='.$id);
$title[] = array('categories','');
echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		errorContainer: $("#category-warning"),
		rules: {
			"keyword[]": {
				required: true
			}
		}		
	});
});
</script>

<style type="text/css">
label.error {
	display: none;
	margin-left:0;
}
</style>

<?php
if($id>0)
{
	$tabs = array();
	$tabs[] = array('details','org.php?id='.$id);
	$tabs[] = array('categories','');
	$tabs[] = array('additional_fields','params.php?id='.$id);
	$tabs[] = array('image_associated','org_image.php?id='.$id);
	$tabs[] = array('docs','org_docs.php?id='.$id);
	echo $hh->Tabs($tabs);
}

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->LoadTree(ASSO_KEYWORD_TYPE);

$tree = $o->Tree(0);

if(count($tree)>0)
{
	echo "<p>" . $trm8->TranslateParams("res_category",array($asso->name)) . "</p>";
	echo "<p>" . $trm8->Translate("multiple_selections") . "</p>";
	
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","update_keywords");
	echo $hh->input_hidden("id_org",$id);
		
	$org_keywords = $asso->Keywords();
	$id_keywords = array();
	foreach($org_keywords as $org_keyword)
		$id_keywords[] = $org_keyword[1];
	
	echo "<label for=\"keyword[]\" class=\"error\">" . $trm8->Translate("category_choose") . "</label>";
	
	echo "<div id=\"categories\">" . $hh->ShowOntoTree($tree,"topic.php?id=",$input_right,$id_keywords) . "</div>";
	
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right && count($tree)>0);
	echo $hh->input_actions($actions,$input_right);
	
	echo $hh->input_form_close();
}
else
	echo "<p>" . $trm8->Translate("categories_no") . "</p>";

include_once(SERVER_ROOT."/include/footer.php");
?>
