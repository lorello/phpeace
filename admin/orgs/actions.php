<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$as = new Assos();
$trm8 = new Translator(0,8);

$fh = new FormHelper;
$post = $fh->HttpPost();

$from = $post['from'];
$from2	= $_GET['from2'];
$action3	= $_GET['action3'];

if ($from=="org")
{
	$action = $fh->ActionGet($post);
	$id_org		= $post['id_org'];
	$id_type	= $post['id_type'];
	$name		= $post['name'];
	$name2		= $post['name2'];
	$address	= $post['address'];
	$zip		= $post['zip'];
	$town		= $post['town'];
	$id_prov	= $post['id_geo'];
	$phone		= $post['phone'];
	$fax		= $post['fax'];
	$email		= $post['email'];
	$website	= $fh->String2Url($post['website']);
	$ccp		= $post['ccp'];
	$notes		= $post['notes'];
	$admin_notes		= $post['admin_notes'];
	$publications	= $post['publications'];
	$person	= $post['person'];
	$source		= $post['source'];
	$approved	= $fh->Checkbox2bool($post['approved']);
	$approved_old	= $fh->Checkbox2bool($post['approved_old']);
	if($action=="store")
	{
		if($id_org>0)
		{
			$asso = new Asso($id_org);
			if ($approved_old=="0" && $approved=="1")
				$asso->NotifyApproval("ins");
			$asso->Update($id_type, $name, $name2, $address, $zip, $town, $id_prov, $phone, $fax, $email, $website, $ccp, $notes, $publications, $person, $source, $approved, $admin_notes);
		}
		else 
		{
			$asso = new Asso(0);
			$id_org = $asso->Insert($id_type, $name, $name2, $address, $zip, $town, $id_prov, $phone, $fax, $email, $website, $ccp, $notes, $publications, $person, $approved, $admin_notes);
		}
		
		header("Location: keywords.php?id=$id_org&from=update");
	}
	if($action=="delete")
	{
		$id_org		= $post['id_org'];
		header("Location: delete.php?id=$id_org");
	}
}

if ($from=="org_delete")
{
	if($module_admin)
	{
		$id_org		= $post['id_org'];
		$asso = new Asso($id_org);
		$asso->Delete();
		$ah->MessageSetTranslated($trm8->TranslateParams("delete_confirmation",array($asso->name)));
	}
	else 
		$ah->MessageSet("user_no_auth",array());
	header("Location: index.php");
}

if ($from=="org_image")
{
	$id_org 	= $post['id_org'];
	$file		= $fh->UploadedFile("img",true);
	$asso = new Asso($id_org);
	$action = $fh->ActionGet($post);
	if ($action=="store" && $file['ok'])
		$asso->ImageUpdate($file);
	if ($action=="delete")
		$asso->ImageDelete();
	header("Location: org.php?id=$id_org");
}

if ($from=="org_doc")
{
	$action 	= $fh->ActionGet($post);
	$id_doc	= $post['id_doc'];
	$id_org	= $post['id_org'];
	$title		= $post['title'];
	$description	= $post['description'];
	$id_language	= $post['id_language'];
	$id_licence	= $fh->Null2Zero($post['id_licence']);
	$source	= $post['source'];
	$author	= $post['author'];
	$keywords	= $post['keywords'];
	$fh->va->NotEmpty($title,"title");
	$goto = "org_docs.php?id=$id_org";
	if($fh->va->return)
	{
		include_once(SERVER_ROOT."/../classes/doc.php");
		$d = new Doc($id_doc);
		$asso = new Asso($id_org);
		if($action=="store")
		{
			$file		= $fh->UploadedFile("doc",true);
			if ($id_doc>0)
			{
				$d->DocUpdate($title,$description,$id_language,$source, $author, $id_licence,$keywords,0,$file,array());
				$asso->DocFileUpdate($id_doc);
			}
			else
			{
				if ($file['ok'])
				{
					$id_doc = $d->DocInsert($title,$description,$id_language,$source, $author, $id_licence,$keywords,$file,array());
					if ($id_doc>0 && $id_org>0)
						$asso->DocAdd($id_doc);
					else
						$fh->va->MessageSet("error","error_doc_insert");
				}
				else 
					$goto = "org_doc.php?id=0&id_org=$id_org";
			}
		}
		if ($action=="delete")
		{
			$asso->DocDelete($id_doc);
		}
	}
	else
		$goto = "org_doc.php?id=$id_doc&id=$id_org";
	header("Location: $goto");
}

if ($from=="update_keywords")
{
	$keywords = $as->Keywords();
	$akeywords = $_POST['keyword'];
	$kws = "";
	if(is_array($akeywords))
	{
		foreach($keywords as $keyword)
		{
			if (in_array($keyword['id_keyword'],$akeywords))
				$kws .= $fh->SqlQuote($keyword['keyword']) . ",";
		}
	}
	$kws = rtrim($kws,",");
	$id_org	= $post['id_org'];
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	$o->InsertKeywords($fh->SqlQuote($kws,false), $id_org, $o->types['org'],ASSO_KEYWORD_TYPE);
	header("Location: org.php?id=$id_org");
}

if ($from=="config_update")
{
	$id_topic	= $post['id_topic'];
	$path 		= $post['path'];
	$url_after_insert	= $post['url_after_insert'];
	$as->ConfigurationUpdate($id_topic,$path,$url_after_insert);
	header("Location: index.php");
}

if ($from=="ass_type")
{
	$action = $fh->ActionGet($post);
	$id_type	= $post['id_type'];
	$type 		= $post['type'];
	if ($action=="store")
	{
		if($id_type>0)
			$as->TypeUpdate( $id_type, $type );
		else 
			$as->TypeAdd( $type );
	}
	if ($action=="delete")
	{
		$as->TypeDelete( $id_type );
	}
	header("Location: types.php");
}

if ($from=="keyword")
{
	$id_keyword	= $post['id_keyword'];
	$id_parent	= $post['id_parent'];
	$keyword	= $post['keyword'];
	$force		= $post['force'];
	$action = $fh->ActionGet($post);
	$url = "topics.php";
	if($action=="update")
	{
		$k_exists = $as->KeywordStore($id_keyword,$keyword,$id_parent,$force);
		if(!$k_exists>0)
		{
			$ah->MessageSetTranslated($trm8->TranslateParams("category_exists",array($keyword)));
			$url = "topic.php?id=0&force=1&k=$keyword" . ( $id_parent>0? "&id_parent=$id_parent" : "" );
		}
	}
	if($action=="delete")
		$url = "topic_delete.php?id=$id_keyword";
	header("Location: $url");
}

if ($from=="keyword_delete")
{
	$id_keyword	= $post['id_keyword'];
	$as->KeywordDelete($id_keyword);
	header("Location: topics.php");
}

if ($from=="keyword_param")
{
	$id_keyword	= $post['id_keyword'];
	$id_kparam	= $post['id_kparam'];
	$label		= $post['label'];
	$id_type	= $post['type'];
	$id_type_old	= $post['id_type_old'];
	$params	= $post['kparam_params'];
	$index_include	= $fh->Checkbox2bool($post['index_include']);
	$list_include	= $fh->Checkbox2bool($post['list_include']);
	$public	= $fh->Checkbox2bool($post['public']);
	$action = $fh->ActionGet($post);
	$url = ($id_keyword>0)? "topic.php?id=$id_keyword" : "topics.php";
	if($action=="update")
	{
		$id_kparam = $as->KeywordParamStore($id_keyword,$id_kparam,$label,$id_type,$params,$public,$index_include,$list_include);
		if(($id_type=="5" || $id_type=="6" ) && $id_type!=$id_type_old)
			$url = "kparam.php?id_k=$id_keyword&id=$id_kparam";
	}
	if($action=="delete")
		$url = "kparam_delete.php?id_k=$id_keyword&id=$id_kparam";
	header("Location: $url");
}

if ($from2=="keyword_param")
{
	$id_keyword	= $_GET['id_k'];
	$id_type	= $_GET['id_type'];
	$id_kparam	= $_GET['id'];
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	if ($action3=="up")
		$k->ParamMove($id_keyword,$id_type,$id_kparam,1);
	if ($action3=="down")
		$k->ParamMove($id_keyword,$id_type,$id_kparam,0);
	header("Location: " . ($id_keyword>0? "topic.php?id=$id_keyword":"topics.php"));
}

if ($from=="keyword_param_delete")
{
	$id_keyword	= $post['id_keyword'];
	$id_kparam	= $post['id_kparam'];
	$as->KeywordParamDelete($id_kparam,$id_keyword);
	header("Location: " . ($id_keyword>0? "topic.php?id=$id_keyword":"topics.php"));
}

if($from=="asso_params")
{
	$id_org 	= $post['id_org'];
	$unescaped_params	= $fh->SerializeParams(false);
	$asso = new Asso($id_org);
	$asso->KeywordParamsUpdate($unescaped_params);
	header("Location: org.php?id=$id_org");
}

if($from2=="in_charge_of" && $module_admin)
{
	$id = $_GET['id'];
	$id_p = $_GET['id_p'];
	$asso = new Asso($id);
	$asso->UpdateUser($id_p);
	header("Location: org.php?id=$id");
}
?>
