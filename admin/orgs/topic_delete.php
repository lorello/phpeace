<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$hhf = new HHFunctions();

$trm8 = new Translator($hh->tr->id_language,8);

$title[] = array('categories','topics.php');

$id = $_GET['id'];
$id_parent = $_GET['id_parent'];

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->LoadTree(ASSO_KEYWORD_TYPE);
$parents = $o->KeywordPath($id);
include_once(SERVER_ROOT."/../classes/keyword.php");
$k = new Keyword();
$row = $k->KeywordGet($id);
$keyword = $row['keyword'];
$action2 = "topic_update";
foreach($parents as $parent)
{
	if($parent['id_keyword']!=$id)
		$title[] = array($parent['keyword'],'topic.php?id=' . $parent['id_keyword']);	
}
$title[] = array($row['keyword'],'topic.php?id='.$id);
$title[] = array('delete','');

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo "<h2>" . $trm8->TranslateParams("category_delete",array($row['keyword'])) . "</h2>";

$tree = $o->Tree($id);
	
$children = count($tree);
if($children>0)
{
	echo "<p>" . $trm8->TranslateParams("category_delete_subcat",array($row['keyword'])) . "</p>";
	echo "<div id=\"categories\">" . $hh->ShowOntoTree($tree,"topic.php?id=") . "</div>\n";	
}
else
{

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","keyword_delete");
	echo $hh->input_hidden("id_keyword",$id);
	echo $hh->input_table_open();
	$actions = array();
	$actions[] = array('action'=>"submit",'label'=>"confirm",'right'=>$input_right &&  $children==0);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();

	$as = new Assos();
	$uses = array();
	$num_uses = $as->KeywordUse($uses,$id);
	if($num_uses>0)
	{
		echo "<div class=\"box\">\n";
		echo "<h3>" . $trm8->Translate("resources_to_be_unlinked") . "</h3>\n";
		echo "<ul>\n";
		foreach($uses as $use)
			echo "<li><a href=\"org.php?id={$use['id_ass']}\">{$use['nome']}</a></li>\n";
		echo "</ul>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

