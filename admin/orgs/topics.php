<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$hhf = new HHFunctions();
$as = new Assos();

$trm8 = new Translator($hh->tr->id_language,8);

$title[] = array('categories','');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->LoadTree(ASSO_KEYWORD_TYPE);

$tree = $o->Tree(0);

echo "<div class=\"box2b\"><h3>" . $hh->tr->Translate("categories") . "</h3>";
echo "<div>(<a href=\"topic.php?id=0\">" . $hh->tr->Translate("add_new") . "</a>)</div>";
echo "<div id=\"categories\">" . $hh->ShowOntoTree($tree,"topic.php?id=") . "</div>";
echo "</div>";

include_once(SERVER_ROOT."/../classes/keyword.php");
$k = new Keyword();
echo "<div class=\"box\">";
echo "<h3>" . $trm8->Translate("categories_fields") . "</h3>\n";
$kparams = $k->Params(0,ASSO_KEYWORD_TYPE);
if(count($kparams)>0)
{
	echo "<ul class=\"tree\">\n";
	$counter = 1;
	foreach($kparams as $kparam)
	{
		echo "<li>";
		if ($counter>1)
			echo "<a href=\"actions.php?from2=keyword_param&id_k=0&id_type=" . ASSO_KEYWORD_TYPE . "&id={$kparam['id_keyword_param']}&action3=up\">{$hh->img_arrow_up_on}</a>";
		else
			echo $hh->img_arrow_up_off;
		if ($counter<count($kparams))
			echo "<a href=\"actions.php?from2=keyword_param&id_k=0&id_type=" . ASSO_KEYWORD_TYPE . "&id={$kparam['id_keyword_param']}&action3=down\">{$hh->img_arrow_down_on}</a>";
		else
			echo $hh->img_arrow_down_off;
		echo "&nbsp;" . ($kparam['public']=="0"? "(inv.) ":"") . $hhf->LinkTitle("kparam.php?id_k=0&id={$kparam['id_keyword_param']}",$kparam['label']) . " (" . $hh->tr->Translate("paramtype_" . $kparam['type']) . ")</li>\n";
		$counter ++;
	}
	echo "</ul>\n";
}
if($module_admin)
	echo "<p><a href=\"kparam.php?id_k=0&id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
echo "</div>";

include_once(SERVER_ROOT."/include/footer.php");
?>

