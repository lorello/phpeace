<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$hhf = new HHFunctions();

$trm8 = new Translator($hh->tr->id_language,8);

$title[] = array('categories','topics.php');

$id = $_GET['id'];
$id_parent = $_GET['id_parent'];
$force = (int)$_GET['force'];

if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	$o->LoadTree(ASSO_KEYWORD_TYPE);
	$parents = $o->KeywordPath($id);
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$row = $k->KeywordGet($id);
	$keyword = $row['keyword'];
	$action2 = "topic_update";
	foreach($parents as $parent)
		if($parent['id_keyword']!=$id)
			$title[] = array($parent['keyword'],'topic.php?id=' . $parent['id_keyword']);
	$title[] = array($row['keyword'],'');
	$tree = $o->Tree($id);
	$num_children = count($tree);
}
else
{
	$row = "";
	$action2 = "topic_insert";
	$title[] = array('add_new','');
	if($force>0)
		$keyword = $_GET['k'];
}

if ($id_parent>0)
{
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$row2 = $k->KeywordGet($id_parent);
}

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo "<div class=\"box\">";
if($id_parent>0)
	echo "<p>" . $trm8->TranslateParams("under",array("<a href=\"topic.php?id=$id_parent\">{$row2['keyword']}</a>")) . "</p>\n";

echo $hh->input_form_open();
echo $hh->input_hidden("from","keyword");
echo $hh->input_hidden("id_keyword",$id);
echo $hh->input_hidden("id_parent",$id_parent);
echo $hh->input_hidden("force",$force);
echo $hh->input_table_open();
echo $hh->input_text("category","keyword",$keyword,20,0,$input_right);
$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0 && $num_children==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id>0)
{
	if(!$id_parent>0 && $id>0 && $input_right)
		echo "<p><a href=\"topic.php?id=0&id_parent=$id\">" . $trm8->TranslateParams("category_add_under",array($row['keyword'])) . "</a></p>\n";
	
	if($num_children>0)
	{
		echo "<h3>" . $trm8->TranslateParams("categories_under",array($row['keyword'])) . "</h3>";
		echo "<div id=\"categories\">" . $hh->ShowOntoTree($tree,"topic.php?id=") . "</div>\n";
	}
	echo "</div>\n";
	echo "<div class=\"box\">\n";
	echo "<h3>" . $trm8->TranslateParams("category_fields",array($row['keyword'])) . "</h3>\n";
	$kparams = $k->Params($id,ASSO_KEYWORD_TYPE);
	if(count($kparams)>0)
	{
		echo "<ul class=\"tree\">\n";
		$counter = 1;
		foreach($kparams as $kparam)
		{
			echo "<li>";
			if ($counter>1)
				echo "<a href=\"actions.php?from2=keyword_param&id_k={$kparam['id_keyword']}&id_type=" . ASSO_KEYWORD_TYPE . "&id={$kparam['id_keyword_param']}&action3=up\">{$hh->img_arrow_up_on}</a>";
			else
				echo $hh->img_arrow_up_off;
			if ($counter<count($kparams))
				echo "<a href=\"actions.php?from2=keyword_param&id_k={$kparam['id_keyword']}&id_type=" . ASSO_KEYWORD_TYPE . "&id={$kparam['id_keyword_param']}&action3=down\">{$hh->img_arrow_down_on}</a>";
			else
				echo $hh->img_arrow_down_off;
				echo "&nbsp;" . ($kparam['public']=="0"? "(inv.) ":"") . $hhf->LinkTitle("kparam.php?id_k=$id&id={$kparam['id_keyword_param']}",$kparam['label']) . " (" . $hh->tr->Translate("paramtype_" . $kparam['type']) . ")</li>\n";
			$counter ++;
		}
		echo "</ul>\n";
	}
	if($input_right)
		echo "<p><a href=\"kparam.php?id_k=$id&id=0\">" . $trm8->Translate("field_add") . "</a></p>\n";

	include_once(SERVER_ROOT."/../modules/assos.php");
	$as = new Assos();
	$uses = array();
	$num_uses = $as->KeywordUse($uses,$id);
	if(count($num_uses)>0)
	{
		echo "</div>\n";
		echo "<div class=\"box\">\n";
		echo "<h3>" . $trm8->TranslateParams("resources_linked_to",array($row['keyword'])) . "</h3>\n";
		echo "<ul>\n";
		foreach($uses as $use)
			echo "<li><a href=\"org.php?id={$use['id_ass']}\">{$use['nome']}</a></li>\n";
		echo "</ul>\n";
	}
}
echo "</div>";
include_once(SERVER_ROOT."/include/footer.php");
?>

