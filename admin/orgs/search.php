<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$as = new Assos();

$title[] = array('search','find.php');
$title[] = array('results','');

echo $hh->ShowTitle($title);

$params = array(	'name' => $get['name'],
					'town' => $get['town'],
					'id_type' => $get['id_type'],
					'id_k' => $get['id_k'],
					'id_prov' => $get['id_geo'],
					'id_reg' => $get['id_reg']
					);

$num = $as->Search( $row, $params, false );

$table_headers = array('type','name','town','approved');
$table_content = array('$row[ass_tipo]','{LinkTitle("org.php?id=$row[id_ass]",$row[nome])}','$row[citta] ($row[geo_name])','{Bool2YN($row[approved])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
