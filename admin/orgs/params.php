<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");

$trm8 = new Translator($hh->tr->id_language,8);

$id = $_GET['id'];

$from = $_GET['from'];
include_once(SERVER_ROOT."/../modules/assos.php");
$asso = new Asso($id);
$row = $asso->Get(false);

if ($module_admin)
	$input_right=1;

$title[] = array($asso->name,'org.php?id='.$id);
$title[] = array('additional_fields','');
echo $hh->ShowTitle($title);

if($id>0)
{
	$tabs = array();
	$tabs[] = array('details','org.php?id='.$id);
	$tabs[] = array('categories','keywords.php?id='.$id);
	$tabs[] = array('additional_fields','');
	$tabs[] = array('image_associated','org_image.php?id='.$id);
	$tabs[] = array('docs','org_docs.php?id='.$id);
	echo $hh->Tabs($tabs);
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","asso_params");
echo $hh->input_hidden("id_org",$id);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/varia.php");
$v =new Varia();

$deparams = $v->Deserialize($row['kparams']);

$counter = 0;
include_once(SERVER_ROOT."/../classes/keyword.php");
$k = new Keyword();
$kparams = $k->Params(0,ASSO_KEYWORD_TYPE);
if(count($kparams)>0)
{
	$counter =+ count($kparams);
	echo $hh->input_separator($trm8->Translate("categories_fields"));
	foreach($kparams as $kparam)
	{
		echo $hh->input_keyword_param($kparam['id_keyword_param'],$kparam['label'],$kparam['type'],$deparams['kp_'.$kparam['id_keyword_param']],$input_right,$kparam['public'],$kparam['params']);
	}
}

$keywords = array();
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->GetKeywords($id, $o->types['org'], $keywords,0,false);

foreach($keywords as $keyword)
{
	$kparams = $k->Params($keyword['id_keyword'],ASSO_KEYWORD_TYPE);
	if(count($kparams)>0)
	{
		$counter =+ count($kparams);
		echo $hh->input_separator($keyword['keyword']);
		foreach($kparams as $kparam)
		{
			echo $hh->input_keyword_param($kparam['id_keyword_param'],$kparam['label'],$kparam['type'],$deparams['kp_'.$kparam['id_keyword_param']],$input_right,$kparam['public'],$kparam['params']);
		}
	}
}

echo $hh->input_submit("submit","",$input_right && $counter>0);
if($counter==0)
	echo $hh->input_note($trm8->Translate("category_fields_no"));
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
