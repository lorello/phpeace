<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/people.php");
$pe =new People();
$as = new Assos();

$trm8 = new Translator($hh->tr->id_language,8);

$id = $get['id'];
$id_p = $get['id_p'];

$asso = new Asso($id);
$row = $asso->Get(false);
$title[] = array($asso->name,'org.php?id=' . $id);
$title[] = array('submitted_by','');

if ($module_admin)
	$input_right=1;

echo $hh->ShowTitle($title);

if($id_p>0)
{
	$user = $pe->UserGetById($id_p);
	echo "<p>" . $trm8->Translate("linked_to") . " {$user['name1']} {$user['name2']}</p>";
}
else
	echo "<p>" . $trm8->Translate("linked_not") . "</p>";

echo $hh->input_form("get","user.php");
echo $hh->input_table_open();
echo $hh->input_hidden("id",$id);
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_text("name","name",$get['name'],"30",0,$input_right);
echo $hh->input_text("email","email",$get['email'],"30",0,$input_right);
echo $hh->input_geo($_GET['id_geo'],$input_right);
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


if($get['name']!="" || $get['email']!="" || $get['id_geo']>0)
{
	$params = array(	'name' => $get['name'],
				'email' => $get['email'],
				'id_geo' => $get['id_geo']
				);
	
	$num = $pe->Search( $row, $params);
	
	$table_headers = array('name','email','verified','town','insert_date');
	$table_content = array('{LinkTitle("actions.php?from2=in_charge_of&id_p=$row[id_p]&id='.$id.'","$row[name1] $row[name2]")}','$row[email]','{Bool2YN($row[verified])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

if($id_p>0)
	echo "<p><a href=\"actions.php?from2=in_charge_of&id=$id&id_p=0\">" . $trm8->Translate("link_remove") . "</a></p>";


include_once(SERVER_ROOT."/include/footer.php");
?>

