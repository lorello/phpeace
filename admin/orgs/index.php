<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$as = new Assos();

echo $hh->ShowTitle($title);

$trm8 = new Translator($hh->tr->id_language,8);

$url = $ini->Get("pub_web") . "/" . $as->org_path;

echo "<p>" . $trm8->Translate("welcome") . "<br/>Link: <a href=\"$url\" target=\"blank\">$url</a></p>\n";

echo "<p><a href=\"find.php\">" . $hh->tr->Translate("search") . "</a></p>";
echo "<p><a href=\"org.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>";
echo "<p><a href=\"types.php\">" . $hh->tr->Translate("types") . "</a></p>";
echo "<p><a href=\"topics.php\">" . $hh->tr->Translate("categories") . "</a></p>";

if ($module_admin)
{
	$inserts = $as->PendingInserts();
	if (count($inserts)>0)
	{
		echo "<h3>" . $hh->tr->Translate("to_approve") . ":</h3><ul>";
			foreach($inserts as $insert)
				echo "<li><a href='org.php?id={$insert['id_ass']}'>{$insert['nome']}</a></li>\n";
		echo "</ul>\n";
	}
	echo "<h3>" . $hh->tr->Translate("administration") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"latest.php\">" . $trm8->Translate("latest") . "</a></li>\n";
	echo "<li><a href=\"mailjob.php\">" . $hh->tr->Translate("mailjob") . "</a></li>\n";
	echo "<li>" . $trm8->Translate("export") . ": <a href=\"csv.php\">CSV</a> - <a href=\"xml.php\">XML</a></li>\n";
	echo "<li><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></li>\n";
	echo "<ul>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
