<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$as = new Assos();

$id = $_GET['id'];

$trm8 = new Translator($hh->tr->id_language,8);

if ($id>0)
{
	$asso = new Asso($id);
	$row = $asso->Get(false);
	$title[] = array($asso->name,'');
	$approved = $row['approved'];
}
else
{
	$row = "";
	$title[] = array('add_new','');
	$approved = 0;
	if($module_right)
		$input_right = 1;
	if($module_admin)
		$approved = 1;
}

if ($module_admin)
{
	$input_right=1;
	$input_super_right = 1;	
}

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required",
			id_type: {
				required: true,
				min: 1
			}
		}
	});
});
</script>

<?php
if($id>0)
{
	$tabs = array();
	$tabs[] = array('details','');
	$tabs[] = array('categories','keywords.php?id='.$id);
	$tabs[] = array('additional_fields','params.php?id='.$id);
	$tabs[] = array('image_associated','org_image.php?id='.$id);
	$tabs[] = array('docs','org_docs.php?id='.$id);
	echo $hh->Tabs($tabs);
}

echo $hh->input_form("post","actions.php",false,"","org-details");
echo $hh->input_hidden("from","org");
echo $hh->input_hidden("id_org",$id);
echo $hh->input_hidden("approved_old",$row['approved']);
echo $hh->input_table_open();

if($id>0)
{
	if($approved)
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$url = $irl->PublicUrlGlobal("orgs",array('id'=>$id,'subtype'=>"org",'id_topic'=>$as->id_topic));	
		echo $hh->input_text("Link:","link","<a href=\"$url\">$url</a>",40,0,0);
	}

	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager();
	include_once(SERVER_ROOT."/../classes/images.php");
	$i = new Images();
	$filename = "orgs/0/$id".".".$i->convert_format;
	if($fm->Exists("uploads/$filename"))
		echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"org_image.php?id=$id\"><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[0]}\"></a></td></tr>\n";
}

echo $hh->input_row("type","id_type",$row['id_tipo'],$as->Types(),"choose_option",0,$input_right);
if ($id>0 && is_array($keywords) && count($keywords)>0)
{
	include_once(SERVER_ROOT."/../classes/ontology.php");
	$o = new Ontology;
	echo $hh->input_keywords($id,$o->types['org'],$keywords,0,"categories");
}
echo $hh->input_text($trm8->Translate("org_name"),"name",$row['nome'],50,0,$input_right);
echo $hh->input_text("name3","name2",$row['nome2'],50,0,$input_right);
echo $hh->input_textarea("description","notes",$row['note'],60,4,"",$input_right);
echo $hh->input_textarea($trm8->Translate("org_address"),"address",$row['indirizzo'],60,2,"",$input_right);
echo $hh->input_text("postcode","zip",$row['cap'],10,0,$input_right);
echo $hh->input_text($trm8->Translate("org_town"),"town",$row['citta'],50,0,$input_right);
echo $hh->input_geo($row['id_prov'],$input_right,1);
echo $hh->input_text("phone","phone",$row['tel'],20,0,$input_right);
echo $hh->input_text("fax","fax",$row['fax'],20,0,$input_right);
echo $hh->input_text("email_one","email",$row['email'],50,0,$input_right);
echo $hh->input_text("website","website",$row['sito'],50,0,$input_right);
echo $hh->input_text("ccp","ccp",$row['ccp'],50,0,$input_right);
echo $hh->input_textarea($trm8->Translate("publications"),"publications",$row['pubblicaz'],60,2,"",$input_right);
echo $hh->input_text("contact_main","person",$row['referente'],50,0,$input_right);
echo $hh->input_text("source","source",$row['fonte'],50,0,$input_right);
echo $hh->input_separator("administration");
echo $hh->input_textarea("notes","admin_notes",$row['admin_notes'],60,4,"",$input_super_right);
echo $hh->input_checkbox("approved","approved",$approved,0,$input_super_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($input_super_right && $id>0)
{
	if($row['id_p']>0)
	{
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe =new People();
		$user = $pe->UserGetById($row['id_p']);
		echo "<p>" . $hh->tr->Translate("submitted_by") . " {$user['name1']} {$user['name2']}";
		echo " (<a href=\"user.php?id=$id&id_p={$row['id_p']}\">" . $hh->tr->Translate("change") . "</a>)<br>";
		echo "email: {$user['email']}</p>";
	}
	else
		echo "<p><a href=\"user.php?id=$id&id_p=0\">" . $trm8->Translate("link_to") . "</p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

