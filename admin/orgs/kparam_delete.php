<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$title[] = array('categories','topics.php');

$trm8 = new Translator($hh->tr->id_language,8);

$id_keyword = $_GET['id_k'];
$id_kparam = $_GET['id'];

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$o->LoadTree(ASSO_KEYWORD_TYPE);
$parents = $o->KeywordPath($id_keyword);
if($id_keyword>0)
{
	foreach($parents as $parent)
		if($parent['id_keyword']!=$id_keyword)
			$title[] = array($parent['keyword'],'topic.php?id=' . $parent['id_keyword']);
}

include_once(SERVER_ROOT."/../classes/keyword.php");
$k = new Keyword();
$row = $k->KeywordGet($id_keyword);
if($id_keyword>0)
	$title[] = array($row['keyword'],'topic.php?id='.$id_keyword);

$row2 = $k->ParamGet($id_kparam);
if($id_keyword>0)
	$title[] = array($row2['label'],'kparam.php?id_k='.$id_keyword.'&id='.$id_kparam);
$title[] = array('delete','');

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo "<h3>" . $hh->tr->Translate("delete") . " \"{$row2['label']}\"</h3>";

$types = $k->ParamsTypes();

$kassos = array();
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/varia.php");
$v = new Varia();
$k = new Keyword();
$ass = new Assos();
$orgs = array();
$ass->KeywordUse($orgs,$id_keyword);
foreach($orgs as $org)
{
	$asso = new Asso($org['id_ass']);
	$row = $asso->Get(false);
	$kparams = $v->Deserialize($row['kparams']);
	if(is_array($kparams))
	{
		$key = "kp_" . $id_kparam;
		if(array_key_exists($key,$kparams))
		{
			$kassos[] = $row['nome'];
		}
	}
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","keyword_param_delete");
echo $hh->input_hidden("id_keyword",$id_keyword);
echo $hh->input_hidden("id_kparam",$id_kparam);
echo $hh->input_hidden("id_type_old",array_search($row2['type'],$types));
echo $hh->input_table_open();
if(count($kassos)>0)
	echo $hh->input_note($trm8->TranslateParams("delete_keyword",array($row2['label'],$hh->Array2String($kassos))),true);

$actions = array();
$actions[] = array('action'=>"submit",'label'=>"delete_confirm",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>

