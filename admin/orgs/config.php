<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");
$as = new Assos();

if ($module_admin)
	$input_right=1;

$title[] = array('configuration','');
echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			path: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","config_update");
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
$topics = new Topics;
echo $hh->input_topics($as->id_topic,0,$topics->AllTopics(),"none_option",$input_right);

echo $hh->input_text("path","path",$as->org_path,20,0,$input_right);
echo $hh->input_text("url_after_insert","url_after_insert",$as->url_after_insert,50,0,$input_right);

echo $hh->input_submit("submit","",$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

