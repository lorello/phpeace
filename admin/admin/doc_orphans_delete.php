<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/docs.php");

$title[] = array("doc_orphans",'doc_orphans.php');
$title[] = array('delete_forever','');

echo $hh->ShowTitle($title);

$row = array();
$num = Docs::Orphans( $row );

echo $hh->input_form_open();
echo $hh->input_hidden("from","doc_orphans");
echo $hh->input_table_open();
echo $hh->input_note($hh->tr->TranslateParams("doc_orphans_delete",array($num)));

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$module_admin);
echo $hh->input_actions($actions,$module_admin);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
