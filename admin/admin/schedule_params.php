<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/scheduler.php");
include_once(SERVER_ROOT."/../classes/varia.php");

$sc = new Scheduler();

$id_schedule = $_GET['id'];

$trm14 = new Translator($hh->tr->id_language,14);
$title[] = array($trm14->Translate("schedules"),'schedules.php');
$title[] = array('Schedule n.'.$id_schedule,'schedule.php?id='.$id_schedule);
$title[] = array('Parameters','');

$row = $sc->ScheduleGet($id_schedule);

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","scheduler_params");
echo $hh->input_hidden("id_schedule",$id_schedule);
echo $hh->input_table_open();

$params = $sc->SchedulableActionParams($row['id_action']);

$v = new Varia();
$params2 = $v->Deserialize($row['params']);

foreach ($params as $param)
{
	echo $hh->input_param($param,$params2[$param],0,$input_right);
}
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

