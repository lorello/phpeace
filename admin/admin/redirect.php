<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

 ********************************************************************/

if (! defined ( 'SERVER_ROOT' ))
	define ( 'SERVER_ROOT', $_SERVER ['DOCUMENT_ROOT'] );
include_once (SERVER_ROOT . "/include/header.php");
include_once (SERVER_ROOT . "/../classes/irl.php");

$irl = new IRL();

$trm14 = new Translator ( $hh->tr->id_language, 14 );

$id = $_GET ['id'];

$title [] = array ($trm14->Translate ( "redirects" ), 'redirects.php' );
if ($id > 0) {
	$row = $irl->RedirectGet ( $id );
	$title [] = array ('change', '' );
} else {
	$row = array();
	$title [] = array ('add_new', '' );
}

if ($module_admin && $conf->Get ( "htaccess" ))
	$input_right = 1;

echo $hh->ShowTitle ( $title );

echo $hh->input_form_open ();
echo $hh->input_hidden ( "from", "redirect" );
echo $hh->input_hidden ( "id_redirect", $id );
echo $hh->input_table_open ();
echo $hh->input_array ( "type", "type", $row['id_type'], $irl->redirect_types, $input_right );
echo $hh->input_text ( "from", "url_from", $row['url_from'], 40, 0, $input_right );
echo $hh->input_text ( "to", "url_to", $row['url_to'], 60, 0, $input_right );
$actions = array ();
$actions [] = array ('action' => "store", 'label' => "submit", 'right' => $input_right );
if ($id > 0)
	$actions [] = array ('action' => "delete", 'label' => "delete", 'right' => $input_right );
echo $hh->input_actions ( $actions, $input_right );
echo $hh->input_table_close () . $hh->input_form_close ();

include_once (SERVER_ROOT . "/include/footer.php");
?>

