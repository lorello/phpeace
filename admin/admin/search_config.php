<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$trm14 = new Translator($hh->tr->id_language,14);
if ($module_admin)
	$input_right = 1;

$title[] = array($trm14->Translate("search_engine"),'');

echo $hh->ShowTitle($title);

echo "<h3>" . $hh->tr->Translate("configuration") . "</h3>";

echo $hh->input_form_open();
echo $hh->input_hidden("from","search_config");
echo $hh->input_table_open();
$resources = $hh->tr->Translate("resources");
echo $hh->input_list($trm14->Translate("resources_to_index"),"resources",$ini->Get("search_config"),$resources,$input_right);
$actions[] = array('action'=>"submit",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
