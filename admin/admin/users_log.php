<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/users.php");

$users = new Users;

$title[] = array('Log connessioni utenti','');

echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

if ($input_right)
{
	echo "<p>Vengono mostrati solo gli ultimi " . LOG_DAYS . " giorni</p>";
	
	$row = array();
	$num = $users->GetLog( $row );
	
	$table_headers = array('date','hour','user','ip','browser');
	$table_content = array('{FormatDate($row[login_ts])}','{FormatTime($row[login_ts])}','$row[name]','<div class=\"right\">$row[ip]</div>','$row[user_agent]');
	
	echo $hh->showTable($row, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
