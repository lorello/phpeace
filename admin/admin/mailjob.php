<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");

$id_mail = $_GET['id'];

$mj = new Mailjobs();

$row = $mj->MailJobGet($id_mail);

if ($module_admin)
	$input_right = 1;

$trm14 = new Translator($hh->tr->id_language,14);

$title[] = array($trm14->Translate("mailjobs"),'mailjobs.php');
$title[] = array($row['subject'],'');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","mailjob");
echo $hh->input_hidden("id_mail",$id_mail);
echo $hh->input_table_open();
echo $hh->input_date("date","send_date",$row['send_date_ts'],0);

$hhf = new HHFunctions();

include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users();
echo $hh->input_user("author",$uu->Active(),$row['id_user'],0);
echo $hh->input_text("module","subject",$hhf->MailjobInfo($row['id_module'],$row['id_item']),30,0,0);
echo $hh->input_text("from","from_name","{$row['from_name']} &lt;{$row['from_email']}&gt;",50,0,0);
echo $hh->input_text("subject","subject",$row['subject'],70,0,$input_right);

if($row['is_html'])
{
	if($row['id_article']>0)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$a = new Article($row['id_article']);
		$a->ArticleLoad();
		$t = new Topic($a->id_topic);
		$hhf = new HHFunctions;
		echo $hh->input_text("article","id_article","<a href=\"/articles/article.php?id={$row['id_article']}&w=topics\"><b>" . $a->headline . "</b></a>
 (" . $t->name . " - " . $hhf->PathToSubtopic($a->id_topic,$a->id_subtopic) . ")",70,0,0);
	}
	echo $hh->input_wysiwyg("text_html","body",$row['body'],1,20,$input_right,true);
	echo $hh->input_textarea("alt_text","alt_text",$row['alt_text'],80,10,"",$input_right);
}
else 
	echo $hh->input_textarea("text","body",$row['body'],80,20,"",$input_right);
echo $hh->input_textarea("footer","footer",$row['footer'],80,7,"",$input_right);
if($row['format']!="")
{
	$filename = "mailjobs/$id_mail.{$row['format']}";
	echo "<tr><td align=\"right\">" . $hh->tr->Translate("attachment") . "</td><td><a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a>" . $hh->FileSize($filename) . "</td></tr>\n";
	
}

echo $hh->input_checkbox("send_password","send_password",$row['send_password'],0,0);
echo $hh->input_array("mailjob_footer","auto_footer",$row['auto_footer'],$hh->tr->Translate("mailjob_footer_options"),0);
echo $hh->input_checkbox("track","track",$row['track'],0,0);
echo $hh->input_separator("Status");
echo $hh->input_checkbox("done","sent",$row['sent'],0,0);
echo $hh->input_text("tot","how_many",$row['how_many'],10,0,0);

if($row['track'])
	echo $hh->input_text("track_redirect","track_redirect",$row['track_redirect'],30,0,0);

$tosend = $mj->RecipientsToSend($id_mail);
if($tosend<$row['how_many'] && $tosend>0)
	echo $hh->input_note($trm14->TranslateParams("mailjob_sending",array(($row['how_many'] - $tosend),$tosend)),true);
$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
