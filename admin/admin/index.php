<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

$trm14 = new Translator($hh->tr->id_language,14);
?>
<div class="box2c">
<h3><a href="config.php"><?=$hh->tr->Translate("configuration");?></a></h3>
<h3><?=$hh->tr->Translate("management");?></h3>
<ul>
<?php
if($conf->Get("htaccess"))
{
	echo "<li><a href=\"redirects.php\">" . $trm14->Translate("redirects") . "</a></li>";
}
?>
<li><a href="mailjobs.php"><?=$trm14->Translate("mailjobs");?></a></li>
<?php
if (in_array(11, $adminModules))
	echo "<li><a href=\"list_pass.php\">" . $trm14->Translate("lists_passwords") . "</a></li>\n";	
if($module_admin)
{
	echo "<li><a href=\"robots.php\">robots.txt</a></li>\n";
	echo "<li><a href=\"blocked_words.php\">" . $trm14->Translate("blocked_words") . "</a></li>"; 
}
?>
</ul>

<h3><?=$trm14->Translate("texts");?></h3>
<ul>
<li><a href="text.php?id=1">Login</a></li>
<li><a href="text.php?id=2"><?=$trm14->Translate("all_pages");?></a></li>
</ul>

<h3><?=$trm14->Translate("scheduler");?></h3>
<ul>
<li><a href="schedules.php"><?=$trm14->Translate("schedules");?></a></li>
<li><a href="watches.php">Site watches</a></li>
</ul>

<h3>Log</h3>
<ul>
<li><a href="log.php"><?=$trm14->Translate("log_main");?></a></li>
<li><a href="users_log.php"><?=$trm14->Translate("log_users");?></a></li>
<li><a href="services.php"><?=$trm14->Translate("services");?></a></li>
<li><a href="publish_log.php"><?=$trm14->Translate("log_publish");?></a></li>
<li><a href="friends_log.php"><?=$trm14->Translate("log_friends");?></a></li>
<li><a href="clicks_log.php"><?=$trm14->Translate("log_banners");?></a></li>
</ul>

<h3><?=$trm14->Translate("keep_an_eye");?></h3>
<ul>
<li><a href="image_orphans.php"><?=$hh->tr->Translate("images_orphans");?></a></li>
<li><a href="doc_orphans.php"><?=$hh->tr->Translate("doc_orphans");?></a></li>
<li><a href="articles.php"><?=$hh->tr->Translate("articles_list_to_approve");?></a></li>
<li><a href="articles_subtopic.php"><?=$trm14->Translate("articles_no_subtopic");?></a></li>
<li><a href="users.php"><?=$trm14->Translate("users_inactive");?></a></li>
<li><a href="lazy_users.php?type=1"><?=$trm14->Translate("users_few_conn");?></a></li>
<li><a href="lazy_users.php?type=2"><?=$trm14->Translate("users_no_conn");?></a></li>
<li><a href="publish_queue.php"><?=$hh->tr->Translate("publishing_queue");?></a></li>
</ul>

<h3><?=$trm14->Translate("search_engine");?></h3>
<ul>
<li><a href="search_stats.php?id=1"><?=$trm14->Translate("search_stats");?></a></li>
<?php 
if ($input_right)
{
	echo "<li><a href=\"search_reindex.php\">" . $trm14->Translate("search_reindex") . "</a></li>";
	echo "<li><a href=\"search_queue.php\">" . $trm14->Translate("search_queue") . "</a></li>";
	echo "<li><a href=\"search_config.php\">" . $hh->tr->Translate("configuration") . "</a></li>";
	if($conf->Get("search_query_store"))
		echo "<li><a href=\"search_top.php\">" . $trm14->Translate("search_top") . "</a></li>"; 
	echo "</ul>";
	echo $hh->input_form("get","search_stats.php");
	echo $hh->input_hidden("id",4);
	echo $hh->input_table_open();
	echo $hh->input_text("text","q","","15",0,$input_right);
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();

?>
	
</div>

<div class="box2c">
<h2><?=$trm14->Translate("commands");?></h2>
<p><?=$hh->tr->Translate("warning");?></p>

<h3><?=$hh->tr->Translate("publishing");?></h3>
<ul>
<li><a href="allqueue.php"><?=$trm14->Translate("all_queues");?></a></li>
<li><a href="tree.php"><?=$trm14->Translate("topics_groups");?></a></li>
<li><a href="sitemap.php"><?=$hh->tr->Translate("map");?></a></li>
<li><a href="publish_home.php"><?=$hh->tr->Translate("homepage");?></a></li>
<li><a href="feeds.php">Feeds RSS</a></li>
<?php
if($conf->Get("htaccess"))
{
	echo "<li><a href=\"redirects_build.php\">" . $trm14->Translate("redirects") . "</a></li>";
}
?>
<li><a href="xmlsitemap.php">XML Sitemap</a></li>
<?php if(!$ui) { ?>
<li><a href="js_custom.php">JavaScript Custom</a></li>
<li><a href="allgalleries.php"><?=$trm14->Translate("all_galleries");?></a></li>
<li><a href="graphics_images.php"><?=$trm14->Translate("graphics_images");?></a></li>
<?php } ?>
</ul>

<?php if(!$ui) { ?>

<h3><?=$trm14->Translate("graphics");?></h3>
<ul>
<!-- <li><a href="styles.php"><?=$hh->tr->Translate("styles_rebuild");?></a></li> -->
<?php
	echo "<li><a href=\"styles_modules.php\">" . $trm14->Translate("init_xsl_css") . "</a></li>\n";
	echo "<li><a href=\"rebuild_graphics.php\">" . $trm14->Translate("rebuild_graphics") . "</a></li>\n";
?>
<li><a href="rebuild_images.php"><?=$trm14->Translate("rebuild_images");?></a></li>
<li><a href="resize_images.php"><?=$trm14->Translate("resize_images");?></a></li>
<?php
if ($ah->ModuleAdmin(16)) {
	echo "<li><a href=\"rebuild_covers.php\">" . $trm14->Translate("rebuild_covers") . "</a></li>\n";	
	echo "<li><a href=\"republish_covers.php\">" . $trm14->Translate("covers") . "</a></li>\n";	
}
if ($conf->Get("docs_covers_size") > -1) {
	echo "<li><a href=\"docs_covers.php\">" . $trm14->Translate("docs_covers") . "</a></li>\n";
}
?>
<li><a href="rebuild_banners.php"><?=$trm14->Translate("rebuild_banners");?></a></li>
</ul>
<?php } ?>

<h3><?=$trm14->Translate("cleaning");?></h3>
<ul>
<?php
if(!$ui) {
	echo "<li><a href=\"revisions_delete.php\">" . $trm14->Translate("revisions_delete") . "</a></li>\n";
}
?>
<li><a href="tracking_delete.php"><?=$trm14->Translate("tracking_delete");?></a></li>
</ul>

<h3><?=$trm14->Translate("varia");?></h3>
<ul>
<li><a href="reshuffle.php"><?=$trm14->Translate("subtopic_reshuffle");?></a></li>
<li><a href="reshuffle_groups.php"><?=$trm14->Translate("topics_groups_reshuffle");?></a></li>
<li><a href="tree_clone.php"><?=$trm14->Translate("tree_clone");?></a></li>
<li><a href="newsletter.php"><?=$hh->tr->Translate("newsletter_global_send");?></a></li>
<?php
if ($conf->Get("htaccess"))
{
	echo "<li><a href=\"furl.php\">" . $trm14->Translate("furl_set") . "</a></li>\n";	
}
?>
</ul>
<?php
}
?>

</div>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
