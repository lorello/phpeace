<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action = $post['action2'];
$from = $post['from'];

if ($from=="mailjob")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/mailjobs.php");
	$mj = new Mailjobs();
	$id_mail 	= $post['id_mail'];
	$subject = $post['subject'];
	$body = $post['body'];
	$alt_text = $post['alt_text'];
	$footer = $post['footer'];
	if ($action=="delete")
		$mj->MailJobDelete( $id_mail );
	if ($action=="update")
		$mj->MailJobUpdate( $id_mail,$subject,$body,$alt_text,$footer );
	header("Location: mailjobs.php");
}

if ($from=="redirect")
{
	$action = $fh->ActionGet($post);
	$id_redirect = $post['id_redirect'];
	$id_type = $post['type'];
	$url_from = $fh->HttpPostUnescapedVar("url_from");
	$url_to = $fh->String2Url($post['url_to']);
	include_once(SERVER_ROOT."/../classes/irl.php");
	$irl = new IRL();
	if ($action=="delete")
		$irl->RedirectDelete( $id_redirect );
	if ($action=="store")
		$irl->RedirectStore( $id_redirect, $id_type, $url_from, $url_to );
	header("Location: redirects.php");
}

if ($from=="scheduler")
{
	$action 	= $fh->ActionGet($post);
	$id_schedule	= $post['id_schedule'];
	$id_scheduler	= $post['id_scheduler'];
	$id_action	= $post['id_action'];
	$id_user	= $post['id_user'];
	$active		= $fh->Checkbox2bool($post['active']);
	include_once(SERVER_ROOT."/../classes/scheduler.php");
	$sc = new Scheduler();
	if ($action=="time")
	{
		$hour	= $post['time_h'];
		$minute = $post['time_i'];
		include_once(SERVER_ROOT."/../classes/scheduler.php");
		$sc = new Scheduler();
		if($id_scheduler>-1)
			$sc->SchedulerSlotUpdate($id_scheduler,$hour,$minute);
		else
			$sc->SchedulerSlotAdd($hour,$minute);
		header("Location: schedules.php");
	}
	if ($action=="delete")
	{
		$sc->ScheduleDelete($id_schedule);
		header("Location: schedules.php");
	}
	if ($action=="activeswap")
	{
		$sc->ScheduleActiveSwap($id_schedule,$active);
		header("Location: schedules.php");
	}
	if ($action=="store")
	{
		$id_schedule = $sc->ScheduleStore( $id_schedule, $id_action, $id_scheduler, $active, $id_user );
		$params = $sc->SchedulableActionParams($id_action);
		if ($id_schedule > 0 && count($params)>0)
			header("Location: schedule_params.php?id=$id_schedule");
		else
			header("Location: schedules.php");
	}
}

if ($from=="watch")
{
	$action 	= $fh->ActionGet($post);
	$id_watch	= $post['id_watch'];
	$url	= $fh->String2Url($post['url']);
	$keyword	= $post['keyword'];
	$notify		= $post['notify'];
	include_once(SERVER_ROOT."/../classes/scheduler.php");
	$sc = new Scheduler();
	if ($action=="delete")
	{
		$sc->WatchDelete($id_watch);
	}
	if ($action=="store")
	{
		$id_watch = $sc->WatchStore( $id_watch, $url, $keyword, $notify );
	}
	header("Location: watches.php");
}

if ($from=="scheduler_params")
{
	$action 	= $fh->ActionGet($post);
	$id_schedule	= $post['id_schedule'];
	$params	= $fh->SerializeParams();
	include_once(SERVER_ROOT."/../classes/scheduler.php");
	$sc = new Scheduler();
	if ($action=="store")
	{
		$sc->SchedulableActionStore($id_schedule,$params);
	}
	header("Location: schedules.php");
}

if ($from=="config_update")
{
	$pub_web		= $post['pub_web'];
	$admin_web		= $post['admin_web'];
	$title			= $post['title'];
	$description	= $post['description'];
	$scheduler_ip	= $post['scheduler_ip'];
	$sched_notify		= $fh->Checkbox2bool($post['sched_notify']);
	$staff_email	= $post['staff_email'];
	$init_year		= $post['init_year'];
	$max_file_size	= $post['max_file_size'];
	$feed_type		= $post['feed_type'];
	$default_currency		= $post['default_currency'];
	$geo_location	= $post['geo_location'];
	$log_banners	= $fh->Checkbox2bool($post['log_banners']);
	$log_level	= $fh->Null2Zero($post['log_level']);
	$ah->ConfigurationUpdate($pub_web,$admin_web,$title,$description,$staff_email,$init_year,$max_file_size,$log_banners,$geo_location,$sched_notify,$scheduler_ip,$feed_type,$default_currency,$log_level);
	header("Location: index.php");
}

if ($from=="log")
{
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/log.php");
	$log = new Log();
	if ($action=="reset")
		$log->Wipe();
	if ($action=="send")
	{
		$log->Send(false,0);
		$ah->MessageSet("sent_email");
	}
	header("Location: index.php");
}

if ($from=="tracking")
{
	$action 	= $fh->ActionGet($post);
	if ($action=="delete" && $module_admin)
	{
		include_once(SERVER_ROOT."/../classes/tracker.php");
		$tk = new Tracker();
		$tk->Reset();
		header("Location: index.php");
	}
}

if ($from=="versioning")
{
	$action 	= $fh->ActionGet($post);
	if ($action=="delete" && $module_admin)
	{
		include_once(SERVER_ROOT."/../classes/css.php");
		$csm = new CssManager();
		$csm->RevisionsDelete();
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$xslm = new XslManager();
		$xslm->RevisionsDelete();
		header("Location: index.php");
	}
}

if ($from=="index_rebuild")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/search.php");
	$s = new Search();
	$id_res	= $post['id_res'];
	$id_topic	= $post['id_topic'];
	$priority 	= $post['priority'];
	if ($module_admin)
	{
		$num = $s->Reindex($id_res,$priority,$id_topic);
		$ah->MessageSet("reindex_resources",array($num));
	}
	header("Location: index.php");
}

if ($from=="images_orphans")
{
	if ($module_admin)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$i->OrphansRemove();
	}
	header("Location: index.php");
}

if ($from=="doc_orphans")
{
	if ($module_admin)
	{
		include_once(SERVER_ROOT."/../classes/docs.php");
		Docs::OrphansRemove();
	}
	header("Location: index.php");
}

if ($from=="search_config")
{
	if ($module_admin)
	{
		$resources		= $fh->ParseCheckList("resources",$post);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ConfigurationUpdate($resources);
	}
	header("Location: index.php");
}

if ($from=="tree_clone")
{
	if ($module_admin)
	{
		$id_topic_from	= $fh->Null2Zero($post['id_topic_from']);
		$id_topic_to	= $fh->Null2Zero($post['id_topic_to']);
		if($id_topic_from>0 && $id_topic_to>0 && $id_topic_from!=$id_topic_to)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t1 = new Topic($id_topic_from);
			$t1->LoadTree();
			$t1->TreeClone($id_topic_to);
		}
	}
	header("Location: /topics/subtopics.php?id=$id_topic_to");
}

if ($from=="admin_text")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/texthelper.php");
	$th = new TextHelper();
	$id_text 	= $post['id_text'];
	$title 		= $post['title'];
	$description 	= $post['description'];
	$active		= $fh->Checkbox2bool($post['active']);
	if ($action=="update")
		$th->AdminContentUpdate($id_text,$title,$description,$active);
	header("Location: index.php");
}

if ($from=="robots")
{
	$action = $fh->ActionGet($post);
	if ($module_admin)
	{
		$txt	= $fh->HttpPostUnescapedVar("txt");
		$fm = new FileManager;
		$fm->WritePage("pub/robots.txt",$txt);
	}
	header("Location: index.php");
}

if ($from=="blocked_words")
{
	if ($module_admin)
	{
		$words = $fh->HttpPostUnescapedVar("words");
		include_once(SERVER_ROOT."/../classes/texthelper.php");
		$th = new TextHelper();
		$th->BlockedWordsSet($words);
	}
	header("Location: index.php");
}

if ($from=="furl_set")
{
	$action = $fh->ActionGet($post);
	$id_topic	= $post['id_topic'];
	if ($module_admin && $id_topic>0)
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$num = $irl->FriendlyUrlGenerateTopic($id_topic);
		$ah->MessageSet("furls_generated",array($num));
	}
	header("Location: index.php");
}
?>
