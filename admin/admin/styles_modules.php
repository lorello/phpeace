<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/styles.php");
include_once(SERVER_ROOT."/../classes/xsl.php");
include_once(SERVER_ROOT."/../classes/css.php");

$trm14 = new Translator($hh->tr->id_language,14);

$title[] = array($trm14->Translate("init_xsl_css"),'');
echo $hh->ShowTitle($title);

$modules = Modules::AvailableModules();
$s = new Styles();
$styles = $s->StylesAll();
$tot_styles = count($styles);

echo "<ul>";
foreach($modules as $module)
{
	if($module['layout'] && !$module['internal'] && !$module['global'])
	{
		$type = $module['path'];
		$id_module = $module['id_module'];
		echo "<li><strong>$type</strong>";
		$xslm = new XslManager("module");
		$xslm->id_pagetype = $id_module;
		echo "<ul>XSL";
		if(!$dev_right)
		{
			echo "<li>Style 0: ";
			$xsl0 = $xslm->XslGetByType(0);
			if($xsl0['id_xsl']>0)
				echo "OK";
			else
			{
				$xsl_def = $xslm->TypeInit($type,-1);
				$id_xsl = $xslm->XslInsert(0,0,"",$xsl_def,1);
				if(!$phpeace->main)
					$xslm->XslDownload($id_xsl);
				echo "Rebuilt";
			}
			echo "</li>";		
		}
		foreach($styles as $style)
		{
			$id_style = $style['id_style'];
			echo "<li>Style $id_style ({$style['name']}): ";
			$xslx = $xslm->XslGetByType($id_style);
			if($xslx['id_xsl']>0)
				echo "OK";
			else
			{
				$xsl = $xslm->TypeInit($type);
				$xslm->XslInsert(0,$id_style,"",$xsl);
				echo "Rebuilt";
			}
			echo "</li>";
		}
		echo "</ul>";

		$csm = new CssManager("module");
		$csm->id_pagetype = $id_module;
		echo "<ul>CSS";
		if(!$dev_right)
		{
			echo "<li>Style 0: ";
			$css0 = $csm->CssGetByType(0);
			if($css0['id_css']>0)
				echo "OK";
			else
			{
				$csm->CssInsert(0,0,"",CSS_EMPTY);
				echo "Rebuilt";
			}
			echo "</li>";		
		}
		foreach($styles as $style)
		{
			$id_style = $style['id_style'];
			echo "<li>Style $id_style ({$style['name']}): ";
			$cssx = $csm->CssGetByType($id_style);
			if($cssx['id_css']>0)
				echo "OK";
			else
			{
				$csm->CssInsert(0,$id_style,"",CSS_EMPTY);
				echo "Rebuilt";
			}
			echo "</li>";
		}
		echo "</ul>";
	}
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>

