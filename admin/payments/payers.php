<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$is_in = (int)$_GET['is_in'];

$title[] = array($trm15->Translate( (($is_in==1)? "payers" : "payees")),'');
echo $hh->ShowTitle($title);

$p = new Payment();

$row = array();
$num = $p->PayersAmounts( $row, $is_in );

$table_headers = array('name',$trm15->Translate($is_in?"incoming":"outgoing"),$trm15->Translate("balance"));
$table_content = array('{LinkTitle("payer.php?id=$row[id_p]",$row[name])}','{MoneyCurrency($row[amount_tot],$row[id_currency])}',
'{MoneyCurrency($row[balance],$row[id_currency])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
