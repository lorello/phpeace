<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$p = new Payment();

$title[] = array('search','');

echo $hh->ShowTitle($title);

$input_right = $module_right;

echo $hh->input_form("get","psearch2.php");
echo $hh->input_table_open();
echo $hh->input_text("name","name","","30",0,$input_right);
echo $hh->input_text("email","email","","30",0,$input_right);
echo $hh->input_geo(0,$input_right);
echo $hh->input_checkbox("contact_email","contact",0,0,$input_right);
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

