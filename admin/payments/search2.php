<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$is_in = (int)$_GET['is_in'];

$p = new Payment();
$title[] = array('search','search.php?is_in='.$is_in);
$title[] = array('results','');

echo $hh->ShowTitle($title);

$params = array( 'id_payer' => $get['id_payer'],'id_account' => $get['id_account'],'id_payment_type' => $get['id_payment_type'],'desc'=>$get['desc'],'sort_by'=>$get['sort_by'] );

$num = $p->Search( $is_in, $row, $params );

$table_headers = array('date',$trm15->Translate("payment_type"),$trm15->Translate("amount"),'name','description','account',$trm15->Translate("verified"));
$table_content = array('{FormatDate($row[pay_date_ts])}','$row[payment_type]','{MoneyCurrency($row[amount],$row[id_currency])}',
'{LinkTitle("payment.php?id=$row[id_payment]",$row[name])}','$row[description]','$row[account]','{Bool2YN($row[verified])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

