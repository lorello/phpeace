<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);
$p = new Payment();
$id_type = $_GET['id'];

$title[] = array($trm15->Translate("payment_types"),'payment_types.php');

if ($id_type>0)
{
	$row = $p->Type($id_type);
	$title[] = array('change','');
}
else
{
	$row = "";
	$action2 = "insert";
	$title[] = array('add_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","payment_type");
echo $hh->input_hidden("id_payment_type",$id_type);
echo $hh->input_table_open();

echo $hh->input_text($trm15->Translate("payment_type"),"payment_type",$row['payment_type'],30,0,$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($row['id_topic'],0,$tt->AllTopics(),"all_option",$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_type>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id_type>0)
{
	$balance = $p->TypeBalance($id_type);
	
	$tot_in = $balance['tot_in'];
	$tot_out = $balance['tot_out'];
	
	echo "<ul>";
	echo "<li>" . $trm15->Translate("incoming") . ": <a href=\"search2.php?is_in=1&id_payment_type=$id_type\">$tot_in</a></li>";
	echo "<li>" . $trm15->Translate("outgoing") . ": <a href=\"search2.php?is_in=0&id_payment_type=$id_type\">$tot_out</a></li>";
	echo "<li>" . $trm15->Translate("balance") . ": <b>{$balance['total']}</b></li>";
	echo "</ul>";
	
	if($tot_in>0 || $tot_out>0)
	{
		$months = $p->TypeMonths($id_type);
		$counter = 0;
		$tot_months = count($months);
		$width = $tot_months*10;
		$chart_params = "chs={$width}x160&cht=ls&chco=0000AA,000000&chf=bg,s,DDDDDD";
		$params = "";
		$zero = "";
		$min = 0;
		$max = 0;
		foreach($months as $month)
		{
			$month_in = floor($month['tot_in']);
			$month_out = floor($month['tot_out']);
			$balance = $month_in - $month_out;
			$max = $balance>$max? $balance : $max;
			$min = $balance<$min? $balance : $min;
			$params .= $balance;
			$zero .= "0";
			$counter ++;
			if($counter < $tot_months)
			{
				$params .= ",";
				$zero .= ",";
			}
		}
		$chart_params .= "&chd=t:$params|$zero&chds={$min},{$max}";
		echo "<img src=\"http://chart.apis.google.com/chart?$chart_params\" alt=\"In\"/>";
	}
}
include_once(SERVER_ROOT."/include/footer.php");
?>

