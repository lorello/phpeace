<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

echo $hh->ShowTitle($title);

$trm15 = new Translator($hh->tr->id_language,15);
$p = new Payment();
?>
<div style="float:left;">
<h3><?=$trm15->Translate("incoming");?></h3>
<ul>
<?php 
echo "<li><a href=\"payments.php?is_in=1&verified=1\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"payments.php?is_in=1&verified=0\">" . $trm15->Translate("unverified") . "</a></li>\n";
echo "<li><a href=\"payment.php?id=0&is_in=1\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"search.php?is_in=1\">" . $hh->tr->Translate("search") . "</a></li>\n";
?>
</ul>

<h3><?=$trm15->Translate("outgoing");?></h3>
<ul>
<?php 
echo "<li><a href=\"payments.php?is_in=0&verified=1\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"payments.php?is_in=0&verified=0\">" . $trm15->Translate("unverified") . "</a></li>\n";
echo "<li><a href=\"payment.php?id=0&is_in=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"search.php?is_in=0\">" . $hh->tr->Translate("search") . "</a></li>\n";
?>
</ul>

<h3><?=$trm15->Translate("contacts");?></h3>
<ul>
<?php 
echo "<li><a href=\"payers.php?is_in=0\">" . $trm15->Translate("payees") . "</a> (<a href=\"payers_csv.php?is_in=0\">CSV</a>)</li>\n";
echo "<li><a href=\"payers.php?is_in=1\">" . $trm15->Translate("payers") . "</a> (<a href=\"payers_csv.php?is_in=1\">CSV</a>)</li>\n";
echo "<li><a href=\"payer.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"psearch.php\">" . $hh->tr->Translate("search") . "</a></li>\n";
// echo "<li><a href=\"payer_import.php\">" . $trm15->Translate("import") . "</a></li>\n";
?>
</ul>
<h3><a href="accounts.php"><?=$hh->tr->Translate("accounts");?></a></h3>
<ul>
<?php 
echo "<li><a href=\"payment.php?is_in=1&is_transfer=1\">" . $trm15->Translate("transfer") . "</a></li>\n";
?>
</ul>
<?php
if ($module_admin)
{
	echo "<h3><a href=\"payment_types.php\">" . $trm15->Translate("payment_types") . "</a></h3>\n";
	echo "<ul><li><a href=\"payment_type_merge.php\">" . $trm15->Translate("merge") . "</a></li></ul>\n";
}
?>
<h3><a href="balance.php?id=0"><?=$trm15->Translate("balance");?></a></h3>
<ul>
<?php
echo "<li><a href=\"balance_years.php\">" . $trm15->Translate("balance_year") . "</a></li>\n";
echo "</ul></div>";

$row = $p->Balance(0);
$last_month = $row[0];
$year = $last_month['year'];
$mon = $last_month['month'];
$months = $hh->tr->Translate("month");
array_shift($last_month);
array_shift($last_month);
array_shift($last_month);
$accounts = array();
$p->Accounts($accounts,false);
$hhf = new HHFunctions();
?>
<table border="1" cellspacing="0" cellpadding="2" style="float:left;margin: 2em;">
<tr><th colspan="2"><?=$months[$mon-1]?> <?=$year?></th></tr>
<?php
foreach($last_month as $key=>$value)
{
	echo "<tr><td>";
	$short_key = substr($key,0,6);
	switch($short_key)
	{
		case "accoun":
			$id_account = substr($key,7);
			foreach($accounts as $account)
				if($account['id_account']==$id_account)
					echo $account['name'];
			break;
		case "total_":
			echo "<b>" . $trm15->Translate("balance_month") . "</b>";
			break;
		case "total":
			echo "<b>" . $trm15->Translate($key) . "</b>";
		break;
		case "unveri":
			echo "<b>" . $trm15->Translate("unverified") . "</b>";
		break;
	}
	echo "</td><td>" . $hhf->Money($value,2) . "</td></tr>\n";
}
echo "</table>";

include_once(SERVER_ROOT."/include/footer.php");
?>
