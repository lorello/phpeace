<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper(true,15);
$post = $fh->HttpPost();

$from		= $post['from'];
$action 	= $fh->ActionGet($post);

$p = new Payment();

if ($from=="payment")
{
	$id_payment		= $post['id_payment'];
	$id_payer		= $post['id_payer'];
	$id_account		= $post['id_account'];
	$is_transfer		= $post['is_transfer'];
	$is_in			= $fh->Checkbox2Bool($post['is_in']);
	$verified		= $fh->Checkbox2Bool($post['verified']);
	$id_payment_type	= $post['id_payment_type'];
	$pay_date 	= $fh->Strings2Datetime($post['pay_date_d'],$post['pay_date_m'],$post['pay_date_y'],$post['pay_date_h'],$post['pay_date_i']);
	$amount		= $fh->String2Number($post['amount']);
	$description 		= $post['description'];
	$notes 		= $post['notes'];
	$from_account	= $post['from_account'];
	if ($action=="update")
		$p->PaymentStore($id_payment,$pay_date,$id_payer,$id_account,$amount,$description,$notes,$id_payment_type,$is_in,$verified,$is_transfer,$from_account,0);
	if ($action=="delete")
		$p->PaymentDelete($id_payment);
	header("Location: payments.php?is_in=$is_in&verified=$verified");
}

if ($from=="payment_type")
{
	$id_payment_type	= $post['id_payment_type'];
	$payment_type	= $post['payment_type'];
	$id_topic		= $post['id_topic'];
	if ($action=="delete")
		$p->TypeDelete($id_payment_type);
	if ($action=="update")
		$p->TypeStore($id_payment_type,$payment_type,$id_topic);
	header("Location: payment_types.php");
}

if ($from=="account")
{
	$id_account		= $post['id_account'];
	$id_type		= $post['id_type'];
	$id_topic		= $post['id_topic'];
	$name		 	= $post['name'];
	$active			= $fh->Checkbox2bool($post['active']);
	$shared		= $fh->Checkbox2bool($post['shared']);
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	if ($action=="delete")
		$p->AccountDelete($id_account);
	if ($action=="update")
	{
		$id_account = $p->AccountStore($id_topic,$id_account,$id_type,$name,$active,$ah->current_user_id);
		$p->AccountSharedSet($id_account,$shared);
	}
	header("Location: accounts.php");
}

if ($from=="account_param")
{
	$action 		= $fh->ActionGet($post);
	$id_account		= $post['id_account'];
	$param		= $post['param'];
	$value			= $post['param_' . $param];
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	if ($action=="store")
		$p->AccountParamStore($id_account,$param,$value);
	header("Location: account.php?id=$id_account");
}

if ($from=="payer")
{
	if ($action=="update")
	{
		$name1	= $post['name1'];
		$name2	= $post['name2'];
		$email		= $post['email'];
		$url = "payer.php?id=0";
		$fh->va->NotEmpty($name1.$name2,"name");
		$fh->va->NotEmpty($email,"email");
		if($fh->va->return)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$user = $pe->UserGetByEmail($email);
			if($user['id_p']>0)
			{
				$fh->va->MessageSet("error","user_exists",array($email));
			}
			else 
			{
				$name3	= $post['name3'];
				$id_geo	= $post['id_geo'];
				$address	= $post['address'];
				$address_notes	 = $post['address_notes'];
				$postcode	= $post['postcode'];
				$town		= $post['town'];
				$phone	= $post['phone'];
				$salutation	= $post['salutation'];
				$id_p = $p->PayerCreate($salutation,$name1,$name2,$name3,$email,$address,$address_notes,$postcode,$town,$id_geo,$phone);
				$url = "payer.php?id=$id_p";
			}
		}
	}
	header("Location: $url");
}

$action2	= $_GET['action2'];
if($action2=="merge")
{
	$from	= $_GET['from'];
	$to 	= $_GET['to'];
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$p->TypeTransfer($from,$to);
	header("Location: index.php");
}
?>
