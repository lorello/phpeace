<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");
include_once(SERVER_ROOT."/../classes/payment.php");
$p = new Payment();
$pe = new People();

$trm15 = new Translator($hh->tr->id_language,15);

$id_p = $_GET['id'];

$row = $pe->UserGetDetailsById($id_p);

if($id_p>0)
{
	$title[] = array($row['name1'] . " " . $row['name2'],'');
	echo $hh->ShowTitle($title);
	
	$row = array();
	$num = $p->PaymentsPerson($row,$id_p);
	
	$table_headers = array('date',$trm15->Translate("payment_type"),$trm15->Translate("amount"),'description','account','verified');
	$table_content = array('{FormatDate($row[pay_date_ts])}','$row[payment_type]','{MoneyCurrency($row[amount],$row[id_currency],$row[is_in])}',
	'{LinkTitle("payment.php?id=$row[id_payment]",$row[description])}','$row[account]','{Bool2YN($row[verified])}');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	
	if ($ah->ModuleAdmin(28))
		echo "<p><a href=\"/people/person.php?id=$id_p\">" . $hh->tr->Translate("user_data"). "</a></p>\n";
}
else 
{
	if($module_admin)
		$input_right = 1;
		
	$title[] = array('add_new','');
	echo $hh->ShowTitle($title);
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name1: {
				required: "#name2-field:blank"
			},
			name2: {
				required: "#name1-field:blank"
			},
			email: {
				required: true,
				email: true
			}
		}
	});
});
</script>

<?php	
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","payer");
	echo $hh->input_table_open();
	
	echo $hh->input_text("salutation","salutation",$row['salutation'],30,0,$input_right);
	echo $hh->input_text("name","name1",$row['name1'],30,0,$input_right);
	echo $hh->input_text("name2","name2",$row['name2'],30,0,$input_right);
	echo $hh->input_text("name3","name3",$row['name3'],30,0,$input_right);
	echo $hh->input_text("email","email",$row['email'],50,0,$input_right);
	echo $hh->input_textarea("address","address",$row['address'],80,4,"",$input_right);
	echo $hh->input_textarea("address_notes","address_notes",$row['address_notes'],80,4,"",$input_right);
	echo $hh->input_text("postcode","postcode",$row['postcode'],15,0,$input_right);
	echo $hh->input_text("town","town",$row['town'],50,0,$input_right);
	echo $hh->input_geo($row['id_geo'],$input_right);
	echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);
	
	$actions = array();
	$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
	
include_once(SERVER_ROOT."/include/footer.php");
?>
