<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$is_in = $_GET['is_in'];

$p = new Payment();

$title[] = array('search','');

echo $hh->ShowTitle($title);

echo "<h3>" . $trm15->Translate($is_in? "incoming" : "outgoing") . "</h3>";

$input_right = 1;
echo $hh->input_form("get","search2.php");
echo $hh->input_hidden("is_in",$is_in);
echo $hh->input_table_open();
$accounts = array();
$p->Accounts($accounts,false);
if($is_in)
{
	echo $hh->input_row("from","id_payer",$row['id_payer'],$p->Payers(false),"all_option",0,$input_right);
	echo $hh->input_row("account","id_account",$row['id_account'],$accounts,"all_option",0,$input_right);
}
else 
{
	echo $hh->input_row("from","id_account",$row['id_account'],$accounts,"all_option",0,$input_right);
	echo $hh->input_row("to","id_payer",$row['id_payer'],$p->Payers(false),"all_option",0,$input_right);
}
echo $hh->input_row($trm15->Translate("payment_type"),"id_payment_type",$row['id_payment_type'],$p->PaymentsTypesAll(),"all_option",0,$input_right);
echo $hh->input_text("description","desc","","30",0,1);
echo $hh->input_array("sort_by","sort_by",0,array($hh->tr->Translate("date"),$hh->tr->Translate("insert_date")),$input_right);

$actions = array();
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

