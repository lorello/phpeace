<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$id_payment = $_GET['id'];

$p = new Payment();

$row = $p->PaymentGet($id_payment);

if ($id_payment>0)
{
	$row = $p->PaymentGet($id_payment);
	$is_in = $row['is_in'];
	$is_transfer = $row['is_transfer'];
	$id_currency = $row['id_currency'];
	$title1 = $row['description']!=''? $row['description'] : $row['tx_id'];
	$verified = $row['verified'];
}
else
{
	$is_in = (int)$_GET['is_in'];
	$is_transfer = (int)$_GET['is_transfer'];
	$id_currency = $p->default_currency;
	$title1 = "add_new";
	$verified = 1;
}

if ($module_admin || $module_right)
	$input_right=1;

$title[] = array($trm15->Translate( (($is_in==1)? "incoming" : "outgoing")),'payments.php?verified='.$verified.'&is_in='.$is_in);
$title[] = array($title1,'');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","payment");
echo $hh->input_hidden("id_payment",$id_payment);
echo $hh->input_hidden("is_transfer",$is_transfer);
echo $hh->input_table_open();

echo $hh->input_date("date","pay_date",$row['pay_date_ts'],$input_right);
echo $hh->input_time("hour","pay_date",$row['pay_date_ts'],$input_right);

if($is_transfer)
	echo $hh->input_hidden("id_payment_type",$row['id_payment_type']);
else 
	echo $hh->input_row($trm15->Translate("payment_type"),"id_payment_type",$row['id_payment_type'],$p->PaymentsTypesAll(),"choose_option",0,$input_right);
echo $hh->input_money($trm15->Translate("amount"),"amount",$row['amount'],10,0,$input_right,$id_currency);

$accounts = array();
$p->Accounts($accounts,false);
if($is_in)
{
	if($is_transfer)
		echo $hh->input_row("from","from_account",$row['from_account'],$accounts,"",0,$input_right);
	else 
	{
		echo $hh->input_hidden("from_account",$row['from_account']);
		echo $hh->input_row("from","id_payer",$row['id_payer'],$p->Payers(false),"",0,$input_right);
	}
	echo $hh->input_row("account","id_account",$row['id_account'],$accounts,"",0,$input_right);
}
else
{
	echo $hh->input_hidden("from_account",$row['from_account']);
	echo $hh->input_row("from","id_account",$row['id_account'],$accounts,"",0,$input_right);
	echo $hh->input_row("to","id_payer",$row['id_payer'],$p->Payers(false),"",0,$input_right);
}

if($id_payment>0 && $row['id_payer']>0) {
	$payer = $p->PayerGetByPayerId($row['id_payer']);
	if(isset($payer['id_p']) && $payer['id_p']>0) {
		$hhf = new HHFunctions();
		include_once(SERVER_ROOT."/../classes/people.php");
		$pe = new People();
		$user = $pe->UserGetById($payer['id_p'],false);
		echo $hh->input_note($hhf->LinkTitle( "/people/person.php?id={$payer['id_p']}","{$user['name1']} {$user['name2']}"));	
	}
}

echo $hh->input_textarea($trm15->Translate("description"),"description",$row['description'],80,4,"",$input_right);
echo $hh->input_textarea("notes","notes",$row['notes'],80,4,"",$input_right);
if($id_payment>0)
{
	$hhf = new HHFunctions();
	echo $hh->input_text("origin","id_use",$hhf->PaymentOrigin($row['id_use']),50,0,0);
	echo $hh->input_text("TX ID","tx_id",$row['tx_id'],50,0,0);
}
echo $hh->input_checkbox($trm15->Translate("is_in"),"is_in",$is_in,0,$input_right);
echo $hh->input_checkbox($trm15->Translate("verified"),"verified",$verified,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_payment>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

