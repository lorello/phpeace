<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");
$p = new Payment();

$trm15 = new Translator($hh->tr->id_language,15);

$title[] = array($trm15->Translate("payment_types"),'');
echo $hh->ShowTitle($title);

$num = $p->TypesBalance( $row,0 );

$table_headers = array($trm15->Translate("payment_type"),'topic',$trm15->Translate("balance"));
$table_content = array('{LinkTitle("payment_type.php?id=$row[id_payment_type]",$row[payment_type])}','$row[name]','{Money($row[total])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin)
	echo "<p><a href=\"payment_type.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

