<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$title[] = array($trm15->Translate("balance"),'');
echo $hh->ShowTitle($title);

$id_payment_type = $_GET['id'];

$p = new Payment();
$row = $p->Balance($id_payment_type);

if ($id_payment_type>0)
{
	$type = $p->Type($id_payment_type);
	echo "<p>" . $trm15->Translate("payment_type") . ": <b>{$type['payment_type']}</b></p>\n";
}

$accounts = array();
$p->Accounts($accounts,false);

$table_headers = array('period');
if($id_payment_type>0)
	$table_content = array('$row[period]');
else
	$table_content = array('{LinkTitle("balance_month.php?year=$row[year]&month=$row[month]",$row[period])}');
foreach($accounts as $account)
{
	$table_headers[] = $account['name'];
	$table_content[] = '{Money($row[account' . $account['id_account'] . '])}';
}
$table_headers[] = $trm15->Translate("balance");
$table_headers[] = $trm15->Translate("total");
$table_content[] = '{Money($row[total_month])}';
$table_content[] = '{Money($row[total])}';

$hh->records_per_page = 200;
echo $hh->ShowTable($row, $table_headers, $table_content, count($row));

include_once(SERVER_ROOT."/include/footer.php");
?>

