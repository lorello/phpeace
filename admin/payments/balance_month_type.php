<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$p = new Payment();

$trm15 = new Translator($hh->tr->id_language,15);

$year = $_GET['year'];
$month = $_GET['month'];
$id_payment_type = (int)$_GET['id_type'];

$title[] = array($trm15->Translate("balance"),'balance.php?id=0');
$title[] = array("$year - $month","balance_month.php?year=$year&month=$month");

if($id_payment_type>0)
{
	$type = $p->Type($id_payment_type);
	$title1 = $type['payment_type'];
}
else 
	$title1 = "----";
	
$title[] = array($title1,'');
echo $hh->ShowTitle($title);

$totals = array();
$p->PaymentsMonth( $totals, $year, $month, $id_payment_type );

echo "<p>" . $trm15->Translate("incoming") . ": " . $totals[0]['tot_in'];
echo "<br>" . $trm15->Translate("outgoing") . ": " . $totals[0]['tot_out'] . "</p>";

$row = array();
$num = $p->PaymentsMonthType( $row, $year, $month, $id_payment_type );

$table_headers = array('date',$trm15->Translate("amount"),'name','description','account');
$table_content = array('{FormatDate($row[pay_date_ts])}','{MoneyCurrency($row[amount],$row[id_currency],$row[is_in])}',
'{LinkTitle("payment.php?id=$row[id_payment]",$row[name])}','$row[description]','$row[account]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

