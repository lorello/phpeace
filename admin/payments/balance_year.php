<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$year = $_GET['year'];

$title[] = array($trm15->Translate("balance_year"),'balance_years.php');
$title[] = array($year,'');
echo $hh->ShowTitle($title);

$p = new Payment();

$num = $p->PaymentsYear( $row, $year );

$table_headers = array($trm15->Translate("payment_type"),'in','out',$trm15->Translate("total"));
$table_content = array('$row[payment_type]','<div class=\"right\">$row[tot_in]</div>','<div class=\"right\">$row[tot_out]</div>','<div class=\"right\">$row[total]</div>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

