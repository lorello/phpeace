<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_article = $_GET['id_article'];
$w = $_GET['w'];
if (!isset($w))
	$w = "topics";

$a = new Article($id_article);
$a->ArticleLoad();
$id_topic = $a->id_topic;

$t = new Topic($id_topic);
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('box','boxes.php?id_article='.$id_article.'&id_topic='.$id_topic.'&w='.$w);
$title[] = array('topics','');

echo $hh->ShowTitle($title);

echo "<p>" . $hh->tr->TranslateParams("box_choose_article",array($a->headline));
echo $hh->tr->Translate("choose_topic_box");

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
$num = $tt->Boxes( $row );

$table_headers = array('boxes','topic');
$table_content = array('<div class=\"right\">$row[counter]</div>','{LinkTitle("boxes.php?id_article='.$id_article.'&w='.$w.'&id_topic2=$row[id_topic]",$row[name])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

