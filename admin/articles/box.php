<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
$tinymce_article = (int)$_GET['id_article'];
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");

// INIT
$id = $_GET['id'];
$id_article = $_GET['id_article'];
$w = isset($_GET['w'])? $_GET['w'] : "topics";

$a = new Article($id_article);
$a->ArticleLoad();
$id_topic = $a->id_topic;
$t = new Topic($id_topic);

// RIGHTS
if ($module_admin)
	$input_right = 1;

if ($a->id_user==$ah->current_user_id)
	$input_right = 1;

if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
{
	$input_right = 1;
	$input_super_right = 1;
}

if ($id>0)
{
	$action2 = "update";
	$box = $a->BoxGet($id);
	$show_title = $box['show_title'];
} else {
	$action2 = "insert";
	$show_title = 1;
}

// TITLES
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('box','');
echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","box");
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("id_box",$id);
echo $hh->input_table_open();

echo $hh->input_text("title","title",$box['title'],70,0,$input_right);
echo $hh->input_checkbox("show_title","show_title",$show_title,0,$input_right);
echo $hh->input_wysiwyg("content","content",$box['content'],$box['is_html'],15,$input_right,$t->wysiwyg);
echo $hh->input_textarea("notes","notes",$box['notes'],80,4,"",$input_right);

echo $hh->input_row("type","id_type",$box['id_type'],$t->ArticlesBoxesTypes(),"",0,$input_right);
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['box'],$keywords,$input_right);

echo $hh->input_separator("image_article");
$box_sizes = array();
foreach($a->box_sizes as $box_size)
	$box_sizes[] = $box_size . " px";
$box_sizes[] = "100 %";	
echo $hh->input_array("width","width",$box['width'],$box_sizes,$input_right);
echo $hh->input_array("alignment","align",$box['align'],$hh->tr->Translate("box_aligns"),$input_right);
echo $hh->input_checkbox("popup","popup",$box['popup'],0,$input_right);
echo $hh->input_text("z_width","z_width",$box['z_width']>0?$box['z_width']:400,5,0,$input_right);
echo $hh->input_text("z_height","z_height",$box['z_height']>0?$box['z_height']:400,5,0,$input_right);

if($id_topic>0)
{
	$tikeywords = $t->KeywordsInternal($o->types['box']);
	echo $hh->input_internal_keywords($id,$tikeywords,"box",$input_super_right,$input_right);
}

$actions = array();
$actions[] = array('action'=>($id>0? "update":"insert"),'label'=>"submit",'right'=>$input_right);
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

