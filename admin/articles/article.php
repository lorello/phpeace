<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
$tinymce = true;
$tinymce_article = (int)$_GET['id'];
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/irl.php");
include_once(SERVER_ROOT."/../classes/translations.php");
$tra = new Translations();

// INIT
$id = (int)$_GET['id'];
$id_topic = $_GET['id_topic'];
$w = $_GET['w'];
$u = $_GET['u'];

$id_subtopic = $_GET['id_subtopic'];
$id_translation = $_GET['id_translation'];
$id_template = (int)$_GET['id_template'];

$original = "";
$art_at_work = 0;

$irl = new IRL();

if ($id>0)
{
	$action2 = "update";
	$a = new Article($id);
	$article = $a->ArticleGet();
	if ($article['id_user']==$ah->current_user_id || $ah->ModuleAdmin(5))
		$input_right = 1;
	$id_topic = $article['id_topic'];
	$id_subtopic = $article['id_subtopic'];
	$id_visibility = $article['id_visibility'];
	$show_date = $article['show_date'];
	$show_author = $article['show_author'];
	$show_source = $article['show_source'];
	$headline_visible = $article['headline_visible'];
	$id_language = $article['id_language'];
	$available = $article['available'];
	$show_latest = $article['show_latest'];
	if ($article['original']!="")
		$original = $article['original'];
	if ($article['approved']=="-1")
		$art_at_work = 1;
	$id_licence = $article['id_licence'];
	$id_template = $article['id_template'];
	$template_values = $a->TemplateValues($id_template);
	if(!$id_translation>0) {
		$translation = $tra->TranslationGetByArticle($id);
		if(isset($translation['id_translation']) && $translation['id_translation']>0) {
			$id_translation = $translation['id_translation'];			
		}
	}
}
else
{
	$a = new Article(0);
	$action2 = "insert";
	$input_right = 1;
	$id_visibility = $hh->ini->Get("default_visibility");
	$id_licence = $hh->ini->Get("id_licence");
	$show_date = 1;
	$show_author = 1;
	$show_source = 1;
	$headline_visible = 1;
	$id_language = 1;
	$available = 1;
	$show_latest = 1;
}

$editor = ($article['id_user']>0) ? $article['id_user'] : $ah->current_user_id;
$id_approver = ($article['id_approver']>0) ? $article['id_approver'] : $ah->current_user_id;

if($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$topic_show_latest = $t->row['show_latest'];
	if ($t->AmIAdmin())
	{
		$input_right = 1;
		$input_super_right = 1;
	}
	if ($id==0)
	{
		$id_language = $t->id_language;
		$show_latest = $t->row['show_latest'];
	}
	$users = $t->Users();
	$wysiwyg = $t->wysiwyg;
	$templates = $t->Templates(5);
}
else
{
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users();
	$users = $uu->Active();
	$wysiwyg = false;
	include_once(SERVER_ROOT."/../classes/user.php");
	$user = new User();
	$templates = $user->Templates(5);
}

$current_template = ucwords($hh->tr->Translate("article"));
if($id_template>0)
{
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	$template = $te->TemplateGet($id_template);
	$current_template = $template['name'];
	$te->ResourceSet($template['id_res']);	
	$hidden_fields = $template['hidden_fields'];
}

if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id=' . $id_topic);
	$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic.($id_template>0?"&id_template=$id_template":"") );
}
elseif ($w=="tra")
	$ah->ModuleForce(21);
else
	$title[] = array('list','articles.php');

if ($module_admin)
{
	$input_right = 1;
}

if ($ah->ModuleAdmin(4))
{
	$input_right = 1;
	$input_super_right = 1;
	$input_super_super_right = 1;
}

if ($id_translation>0)
{
	$translation = $tra->TranslationGet($id_translation);
	if($translation['id_article']>0)
	{
		$atra = new Article($translation['id_article']);
		$atra->ArticleLoad();
		$tra_title = $atra->headline;
	}
	else 
		$tra_title = $translation['notes'];
}

if ($id>0)
	$title[] = array($article['headline'],'');
else
	$title[] = array('article_new','');

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
	$(".input-submit").click(function(e){
		if(($(this).attr("name"))=="action_preview")
		{
			$("#article-form").attr("target","_blank"); 
			$("#article-form").attr("action","/topics/article_preview.php"); 
		}
		else
		{
			$("#article-form").attr("target","");
			$("#article-form").attr("action","actions.php");
		}
     return true;
     }
	);
	
	$("#finput-original input").datepicker({ dateFormat: 'dd-mm-yy' });

	$("#article-form").validate({
		ignore: ".ignore",
		focusInvalid: false,
		invalidHandler: function(form, validator) {

	        if (!validator.numberOfInvalids())
	            return;

	        $('html, body').animate({
	            scrollTop: $(validator.errorList[0].element).offset().top
	        }, 2000);

	    },
		rules: {
				id_topic: {
					required: true
				},
				id_subtopic: {
					required:'#approved-field:checked',
					min:1
				},
				headline: {
					required: true,
					maxlength: 250
				},
				halftitle: {
					maxlength: 250
				}
			},
		messages: {
			id_subtopic:"<?=$hh->tr->Translate("article_v_subtopic");?>"
		}
	});
});
</script>

<?php
if ($id>0)
{
	if ($input_super_right == 1 && $article['approved']=="1" && $article['available']=="1" && $art_at_work!="1" && $id_topic>0 && $id_topic!=$ini->Get('temp_id_topic'))
	{
		$pub_url = "/topics/publish_article.php?id=$id_topic&id_article=$id";
		$tools[] = array(($article['published_ts']>0)?'publish_again':'publish',$pub_url);
	}
	$tools[] = array('history','/topics/history.php?id_type='.$ah->r->types['article'].'&id_topic='.$id_topic.'&id='.$id);
	if (Modules::AmIAdmin(10) || ($t->id>0 && $t->edit_layout && $t->AmIAdmin()))
		$tools[] = array('XML','/layout/xml.php?id_type=3&id_topic='.$id_topic.'&id='.$id);
	if($article['allow_comments'])
		$tools[] = array('comments','/topics/comments_tree.php?type=article&id_item='.$id);
	if ($ah->IsModuleActive(21) && $ah->ModuleUser(21))
	{
		$tools[] = array('translation_req','/translations/translation.php?id=0&id_a='.$id);
		$atras = array();
		$num_atras = $a->Translations($atras);
		if($num_atras>0)
			$tools[] = array('translations','/translations/article.php?id_a='.$id);
	}
	if ($input_super_right)
		$tools[] = array('hardcopy',"/articles/article_copy.php?id=$id&w=$w");
	echo $hh->Toolbar($tools);
}

echo $hh->input_form("post","actions.php",false,"","article-form");
echo $hh->input_hidden("id_article",$id);
echo $hh->input_hidden("id_translator",$translation['id_user']);
if($id_translation>0)
{
	echo $hh->input_hidden("id_translation",$id_translation);
}
echo $hh->input_hidden("id_template",$id_template);
echo $hh->input_hidden("approved_old",$article['approved']);
echo $hh->input_hidden("from","article");
if($id_topic>0)
	echo $hh->input_hidden("topic_allow_comments",$t->row['comments']);
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("p",$current_page);
echo $hh->input_hidden("u",$u);
echo $hh->input_table_open();

if ($w=="tra" && $id==0)
{
	if($id_translation>0)
		echo $hh->input_note("<p>" . $hh->tr->Translate("translation") . ": <a href=\"/translations/translation.php?id=$id_translation\">$tra_title</a></p>");
	else 
		echo $hh->input_note("translation_art_new");
}

if ($id>0)
{
	$furl_row = $irl->FriendlyUrlGet(5,$id);
	
	if(count($templates)>0)
	{
		echo $hh->input_note($hh->tr->Translate("template") . ": <strong>$current_template</strong>");
	}
	include_once(SERVER_ROOT."/../classes/contribution.php");
	$co = new Contribution("article");
	$contrib = $co->Get($id);
	if (count($contrib)>0)
	{
		echo $hh->input_note($hh->tr->TranslateParams("contributed",array($hh->FormatDate($contrib['time']),$contrib['name'],$contrib['email'],$contrib['email'])));
	}
	$published = "";
	if ($article['published_ts']>0 && $article['approved']=="1" && $article['available']=="1" && $art_at_work!="1" && $id_topic>0 && $id_topic!=$ini->Get('temp_id_topic'))
	{
		$url = $irl->PublicUrlTopic("article",array('id'=>$id),$t,false);
		$article_url = "Link: <a href=\"$url\" target=\"_blank\">$url</a>"; 
		if($furl_row['furl']!="")
		{
			$article_furl = $t->url . "/" . $furl_row['furl'];
			$article_url .= " (<a href=\"$article_furl\" target=\"_blank\">$article_furl</a>)";
		}
		$published = $hh->tr->Translate("published") . " " . $hh->tr->Translate("on_day") . " " . $hh->FormatDate($article['published_ts']);
	}
	$lu_ts = $a->LastUpdate();
	$last_update =  ($lu_ts>0)? $hh->tr->Translate("lastupdate") . ": " . $hh->FormatDate($lu_ts) . "&nbsp;&nbsp;" . $hh->FormatTime($lu_ts) : "";
	echo $hh->input_note("ID: $id - $article_url <br/>$published" . ($published!=""?" - ":"") . $last_update);
}
else
{
	if(count($templates)>0)
	{
		echo $hh->input_note($hh->tr->Translate("template") . ": <strong>$current_template</strong> (<a href=\"/articles/article_templates.php?w=$w&id_topic=$id_topic\">" . $hh->tr->Translate("change") . "</a>" . ")");
	}
}

if ($available=="0")
	echo $hh->input_note("available_warn",TRUE);

if ($art_at_work=="1")
	echo $hh->input_note("at_work_art_warning",TRUE);


if($id>0 && $input_right && !($id_template>0 && in_array("images",$hidden_fields))) {
	$aimage = $a->Image();
	if(!isset($aimage['id_image'])) {
	    echo $hh->input_note("associated_image_missing",TRUE);
	}
}

echo $hh->input_separator("insertion");
echo $hh->input_date("date","written",$article['written_ts'],$input_right);
echo $hh->input_checkbox("show_date","show_date",$show_date,0,$input_right);

if(!($id_template>0 && in_array("author",$hidden_fields)))
{
	echo $hh->input_separator("author");
	echo $hh->input_user("user",$users,$editor,$input_super_right);
	echo $hh->input_text("or","author",$article['author'],40,0,$input_right);
	if(!($id_template>0 && in_array("author_notes",$hidden_fields)))
		echo $hh->input_text("author_notes","author_notes",$article['author_notes'],50,0,$input_right);
	echo $hh->input_checkbox("show_author","show_author",$show_author,0,$input_right);
}
else 
{
	echo $hh->input_user("user",$users,$editor,0);
	echo $hh->input_hidden("show_author","");
}

if(!($id_template>0 && in_array("source",$hidden_fields)))
{
	echo $hh->input_separator("source");
	echo $hh->input_textarea("source","source",$article['source'],75,3,"",$input_right);
	if(!($id_template>0 && in_array("publishing_date",$hidden_fields)))
		echo $hh->input_date_text("publishing_date","original",$original,10,0,$input_right);
	if(!($id_template>0 && in_array("jump_to_source",$hidden_fields)))
		echo $hh->input_checkbox("jump_to_source","jump_to_source",$article['jump_to_source'],0,$input_right);
	echo $hh->input_checkbox("show_source","show_source",$show_source,0,$input_right);
}

if ($id_translation>0)
{
	echo $hh->input_separator("translation");
	if($id>0)
		echo $hh->input_note("<a href=\"/translations/translation.php?id=$id_translation\">". $hh->tr->Translate("details") . "</a>");
	echo $hh->input_row("translated_by","id_translator",$translation['id_user'],$tra->Translators(),"",0,$input_right);
	echo $hh->input_text("or","translator",$translation['translator'],40,0,$input_right);
	echo $hh->input_textarea("translation_notes","translation_notes",$translation['comments'],75,3,"",$input_right);
}

echo $hh->input_separator("placing");
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
if ($input_super_super_right==1)
	$topics = $tt->AllTopics(true);
else
	$topics = $tt->User($ah->current_user_id);

echo $hh->input_topics($id_topic,0,$topics,"choose_option",$input_right);

echo $hh->input_subtopic("subtopic","id_subtopic","'article_subtopic.php?id_topic='+document.forms['article-form'].id_topic.value+'&id_subtopic='+document.forms['article-form'].id_subtopic.value",$id_subtopic,$id_topic,$input_right,'article-form');
echo $hh->input_separator("heading");
if(!($id_template>0 && in_array("halftitle",$hidden_fields)))
	echo $hh->input_textarea("halftitle_instr","halftitle",$article['halftitle'],80,2,"",$input_right);
echo $hh->input_textarea("title_instr","headline",$article['headline'],80,2,"",$input_right);
if(!($id_template>0 && in_array("subhead",$hidden_fields)))
	echo $hh->input_textarea("subhead_instr","subhead",$article['subhead'],80,5,"",$input_right);
echo $hh->input_checkbox("show_heading","headline_visible",$headline_visible,0,$input_right);

if(!($id_template>0 && in_array("content",$hidden_fields)))
{
	echo $hh->input_separator("content");
	if(!$wysiwyg)
		echo $hh->input_note("content_instr");
	echo $hh->input_wysiwyg("text","content",$article['content'],$article['is_html'],30,$input_right,$wysiwyg,(int)$article['align']);
	if(!($id_template>0 && in_array("notes",$hidden_fields)))
		echo $hh->input_textarea("content_notes","notes",$article['notes'],80,6,"",$input_right);
	if(!$wysiwyg)
		echo $hh->input_array("text_align","align",$article['align'],$hh->tr->Translate("text_aligns"),$input_right);
}

if ($input_right==1)
{
	if(!($id_template>0 && in_array("boxes",$hidden_fields)))
	{
		//--BOXES
		echo $hh->input_separator("boxes");
		echo $hh->input_note("boxes_instr");
		echo "<tr><td class=\"buttons\">";
		$actions = array();
		$actions[] = array('action'=>"add_box",'label'=>"add_new",'right'=>$input_right);
		//$actions[] = array('action'=>"choose_box",'label'=>"choose",'right'=>$input_right);
		echo $hh->input_actions($actions,$input_right,false);
		echo "</td><td valign=\"top\">";
		$boxes = $a->BoxGetAll();
		$hhf = new HHFunctions();
		if(count($boxes)>0)
		{
			$box_sizes = array();
			foreach($a->box_sizes as $box_size)
				$box_sizes[] = $box_size . " px";
			$box_sizes[] = "100 %";		
			echo "<ul class=\"article-boxes\">";
			foreach($boxes as $box)
			{
				echo "<li>" . $hhf->LinkTitle("box.php?id={$box['id_box']}&id_article=$id&w=$w",$box['title']);
				if ($box['popup']=="1")
					echo " (popup) ";
				echo "<ul class=\"box-params\">";
				echo "<li>" . $hh->tr->Translate("label_add") . ": <input type=\"text\" name=\"label_image_{$box['id_box']}\" size=\"14\" class=\"code-box\" value=\"[[Box{$box['id_box']}]]\"  onClick=\"this.select();\"></input></li>";
				echo "<li>" . $hh->tr->Translate("size") . ": <strong>{$box_sizes[$box['width']]}</strong>";
				$baligns = $hh->tr->Translate("box_aligns");
				echo "<li>" . $hh->tr->Translate("alignment") . ": {$baligns[$box['align']]}</li>";
				echo "</ul>";
				echo "</li>\n";
			}
			echo "</ul>";
		}
		echo "</td></tr>\n";	
	}

	if(!($id_template>0 && in_array("images",$hidden_fields)))
	{
		//--IMAGES
		echo $hh->input_separator("images");
		echo $hh->input_note("images_instr");
		$images = $a->ImageGetAll();
		$aimage = $a->Image();
		echo "<tr><td class=\"buttons\">";
		$actions = array();
		$actions[] = array('action'=>"add_image",'label'=>"add_new",'right'=>$input_right);
		$actions[] = array('action'=>"choose_image",'label'=>"choose",'right'=>$input_right);
		$actions[] = array('action'=>"associate_image",'label'=>"associate",'right'=>($input_right && count($images)>0));
		echo $hh->input_actions($actions,$input_right,false);
		echo "</td><td>";
		if(count($images)>0)
		{
			$img_sizes = $conf->Get("img_sizes");
			echo "<ul class=\"article-images\">";
			foreach($images as $image)
			{
				$width = $img_sizes[0];
				$image_file = "images/0/{$image['id_image']}." . $conf->Get("convert_format") . "&format={$image['format']}";
				$height = 0;
				$image_page = "image.php?id={$image['id_image']}&id_article=$id&w=$w&p=$current_page";
				echo "<li" . (($aimage['id_image']==$image['id_image'])? " class=\"associated\" ":"") . ">";
				$image_tag = $hh->Graphic("/images/upload.php?src=$image_file",$width,$height,false,$image['id_image']);
				echo $hh->Wrap($image_tag,"<a href=\"$image_page\" title=\"" . $hh->tr->Translate("image_click") . "\">","</a>");
				echo "<ul class=\"image-params\">";
				echo "<li>" . $hh->tr->Translate("label_add") . ": <input type=\"text\" name=\"label_image_{$image['id_image']}\" size=\"14\" class=\"code-box\" value=\"[[Img{$image['id_image']}]]\"  onClick=\"this.select();\"></input></li>";
				echo "<li><a href=\"$image_page\" title=\"" . $hh->tr->Translate("image_click") . "\">" . $hh->tr->Translate("image_change") . "</a></li>";
				echo "<li>" . $hh->tr->Translate("size") . ": <strong>{$image['width']}</strong> px ";
				$ialigns = $hh->tr->Translate("image_aligns");
				echo "<li>" . $hh->tr->Translate("alignment") . ": {$ialigns[$image['align']]}</li>";
				echo "<li>" . $hh->tr->Translate("caption") . ": <em>{$image['caption']}</em></li>";
				echo "</ul>";
				echo "</li>";
			}
			echo "</ul>";
		}
		echo "</td></tr>";
	}

	if(!($id_template>0 && in_array("docs",$hidden_fields)))
	{
		//--DOCUMENTS
		echo $hh->input_separator("docs");
		echo $hh->input_note("docs_instr");
		echo "<tr><td class=\"buttons\">";
		$actions = array();
		$actions[] = array('action'=>"add_doc",'label'=>"add_new",'right'=>$input_right);
		$actions[] = array('action'=>"choose_doc",'label'=>"choose",'right'=>$input_right);
		echo $hh->input_actions($actions,$input_right,false);
		echo "</td><td valign=\"top\">";
		$docs = $a->DocGetAll();
		if(count($docs)>0)
		{
			echo "<ul class=\"article-docs\">";
			foreach($docs as $doc)
			{
				$filename = "docs/{$doc['id_doc']}.{$doc['format']}";
				echo "<li>";
				if ($input_right)
				{
					if ($doc['seq']!=1)
						echo "<a href=\"actions.php?from3=doc&id=".$doc['id_doc']."&id_article={$doc['id_article']}&action3=up\">$hh->img_arrow_up_on</a>";
					else
						echo $hh->img_arrow_up_off;
					if ($doc['seq']!=count($docs))
						echo "<a href=\"actions.php?from3=doc&id=".$doc['id_doc']."&id_article={$doc['id_article']}&action3=down\">$hh->img_arrow_down_on</a>";
					else
						echo $hh->img_arrow_down_off;
				}
				echo "&nbsp;<a href=\"doc.php?id={$doc['id_doc']}&id_article={$doc['id_article']}&w=$w\">{$doc['title']}</a>";
				echo "<div>" . $hh->FileSize($filename) . " - <a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a></div>";
				echo "<div class=\"notes\">" . $doc['description']. "</div></li>";
			}
			echo "</ul>";
		}
		echo "</td></tr>";	
	}

	if(!($id_template>0 && in_array("related",$hidden_fields)))
	{
		//--RELATED ARTICLES
		echo $hh->input_separator("related");
		echo "<tr><td class=\"buttons\">";
		$actions = array();
		$actions[] = array('action'=>"relate",'label'=>"add_new",'right'=>$input_right);
		echo $hh->input_actions($actions,$input_right,false);
		echo "</td><td valign=\"top\">";
		$related = $a->RelatedAll();
		if(count($related)>0)
		{
			echo "<ul class=\"article-related\">";
			foreach($related as $art)
			{
				echo "<li>{$art['topic_name']}: <a href=\"article.php?id={$art['id_article']}&w=$w\">{$art['headline']}</a>";
				if($art['with_content'])
					echo " <em>(" . $hh->tr->Translate("with_content") . ")</em>";
				if ($input_right)
					echo " (<a href=\"actions.php?from3=related&action3=unrelate&id_article=$id&id_related={$art['id_article']}&w=$w\">" . $hh->tr->Translate("remove") . "</a>)";
				echo "</li>\n";
			}
			echo "</ul>";
		}
		$related2 = $a->RelatedInverseAll();
		if (count($related2)>0)
		{
			echo "<h4 class=\"related\">" . $hh->tr->Translate("related_inverse") . "</h4>";
			echo "<ul class=\"article-related-back\">";
			foreach($related2 as $art2)
				echo "<li>{$art2['topic_name']}: <a href=\"article.php?id={$art2['id_article1']}&w=$w\">{$art2['headline']}</a></li>\n";
			echo "</ul>\n";
		}
		echo "</td></tr>";
	}
	
	//--TEMPLATE FIELDS
	if($id_template>0)
	{
		include_once(SERVER_ROOT."/../classes/template.php");
		$te = new Template("article");
		$tparams = $te->Params($id_template);
		if(count($tparams)>0)
		{
			echo $hh->input_separator("additional_fields");
			foreach($tparams as $tparam)
			{
				$param_value = $id>0? $template_values[$tparam['id_template_param']] : $tparam['default_value'];
				echo $hh->input_keyword_param($tparam['id_template_param'],$tparam['label'],$tparam['type'],$param_value,$input_right,$tparam['public'],$tparam['type_params']);
			}
		}
	}
}
else
	echo $hh->input_note("article_insert_warn");

echo $hh->input_separator("administration");
echo $hh->input_checkbox("allow_comments","allow_comments",$article['allow_comments'],0,$input_right && ($t->row['comments']>0));
$languages = $hh->tr->Translate("languages");
asort($languages);
echo $hh->input_array("language","id_language",$id_language,$languages,$input_right);

if ($hh->ini->Get("licences") && !($id_template>0 && in_array("licence",$hidden_fields)))
	echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_right);

echo $hh->input_checkbox("at_work","at_work",$art_at_work,0,$input_right);
echo $hh->input_checkbox("available","available",$available,0,$input_super_right && $t->row['protected']=="2");

if (!($id_topic>0 && $topic_show_latest=="0"))
{
	if (!$article['published_ts']>0)
		echo $hh->input_checkbox($hh->tr->TranslateParams("notify",array($hh->ini->Get("title"))),"show_latest",$show_latest,0,$input_right);
	else
	{
		$new_latest = $t->queue->JobPresentParam($t->queue->types['article'],$id,"update","new");
		echo $hh->input_checkbox_warn(
		$hh->tr->TranslateParams(($show_latest=="1")? "notify_again":"notify" ,array($hh->ini->Get("title"))) . 
		$hh->input_hidden("show_latest",$show_latest),"new_latest",$new_latest>0,
		$hh->tr->TranslateParams("notify_warn",array($hh->ini->Get("title"))),$input_super_right);
	}
}
else
	echo $hh->input_checkbox($hh->tr->TranslateParams("notify",array($hh->ini->Get("title"))),"show_latest",0,0,0);

echo $hh->input_checkbox("<span class=\"approved{$article['approved']}\">" . $hh->tr->Translate("approved") ."</span>","approved",$article['approved'],0,$input_super_right);

if($id>0 && $t->row['friendly_url']>0)
{
	$furl = "";
	if(isset($furl_row['id']) && $furl_row['furl']!="")
	{
		$furl = $furl_row['furl'];
	}
	else
	{
		$subtopic = $t->SubtopicGet($id_subtopic);
		$furl = $irl->FriendlyUrlSuggest($id_topic,$t->row['friendly_url'],5,$article['headline'],$subtopic['name'],$article['written_ts']);
	}
	echo $hh->input_text("Friendly URL","furl",$furl,40,0,$input_right,"",false,0,"<em>{$t->url}/</em>");
}

$combo_values = array();
$max_vis = $conf->Get("max_vis");
for($i=$max_vis;$i>0;$i--)
	$combo_values["".$i.""] = $i;
echo $hh->input_array("order","id_visibility",$id_visibility,$combo_values,$input_super_right && $t->row['custom_visibility']=="1");

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
if ($id_topic>0)
	$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
if ($id_subtopic>0)
	$o->GetKeywords($id_subtopic,$o->types['subtopic'], $keywords,0,true);
echo $hh->input_keywords($id,$o->types['article'],$keywords,$input_right);

if($id_topic>0)
{
	$tikeywords = $t->KeywordsInternal($o->types['article']);
	echo $hh->input_internal_keywords($id,$tikeywords,"article",$input_super_right,$input_right,true,$input_super_super_right);
}
echo $hh->input_checkbox("highlight","highlight",$article['highlight'],0,$input_super_super_right);

echo $hh->input_separator("");

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"preview",'label'=>"preview",'right'=>$input_right && $id>0);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_super_right && $id>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_separator("");

echo $hh->input_table_close() . $hh->input_form_close();

if ($id==0)
{
?>
<script type="text/javascript">
document.forms['article-form'].reset();
</script>
<?php
}

include_once(SERVER_ROOT."/include/footer.php");
?>


