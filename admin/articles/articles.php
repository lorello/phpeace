<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('list','');
echo $hh->ShowTitle($title);

$id_user = (isset($_GET['id_user']))? $_GET['id_user'] : $ah->current_user_id;

include_once(SERVER_ROOT."/../classes/user.php");
$u = new User;
$u->id = ($_GET['u']=="all")? 0 : $id_user;
$num = $u->Articles( $row );

$table_headers = array('title','Images Attached','Documents Attached','Publish Location','Created By','Created Date','Last Modified By',' Last Modified Date',' Status');
$table_content = array('{LinkTitle("article.php?w=articles&id=$row[id_article]&p='.$current_page.'&u='.$_GET['u'].'",$row[headline])}','{ArtStatus($row[id_image])}','{ArtStatus($row[doc_attached])}','{PathToSubtopic($row[id_topic], $row[id_subtopic], 0, 1)}','$row[author]',
'{FormatDate($row[written_ts])}','$row[last_updated_by]','{FormatDate($row[last_updated_on])}','$row[status]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"article.php?from=articles&id=0\">" . $hh->tr->Translate("article_add"). "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
