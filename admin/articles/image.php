<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$fm = new FileManager;
$i = new Images();
$o = new Ontology;

$id = (int)$_GET['id'];
$id_article = (int)$_GET['id_article'];
$id_topic = (int)$_GET['id_topic'];
$w = isset($_GET['w'])? $_GET['w'] : "topics";

if ($id_article>0)
{
	$a = new Article($id_article);
	$a->ArticleLoad();
	$title1[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
	$id_topic = $a->id_topic;
	if ($a->id_user==$ah->current_user_id)
		$input_right = 1;
}
else
	$title1[] = array('images_gallery','/topics/images.php?id='.$id_topic);
$title1[] = array('image','');

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
	{
		$input_right = 1;
		$topic_right = 1;
	}
	else 
		$topic_right = 0;
	if ($w=="topics")
	{
		$ah->ModuleForce(4);
		$module_admin = $ri->ModuleAdmin();
		$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
		if($id_article>0)
			$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
	}
	else
		$title[] = array('list','articles.php');
	$title = array_merge($title,$title1);
}
else
{
	$ah->ModuleForce(14);
	$module_admin = $ri->ModuleAdmin();
	$title[] = array('images_orphans','/admin/image_orphans.php');
}

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

echo $hh->ShowTitle($title);

$maxfilesize = $fm->MaxFileSize();

$keywords = array();
$size = $i->default_img_size;
if ($id>0)
{
	$action2 = "update";
	include_once(SERVER_ROOT."/../classes/image.php");
	$im = new Image($id);
	$irow = $im->Get();
	$image_date = $irow['image_date'];
	$source = $irow['source'];
	$author = $irow['author'];
	$caption = $irow['caption'];
	$id_licence = $irow['id_licence'];
	$filename_orig = "images/orig/{$id}.{$irow['format']}";
	$filename_copy = "images/2/{$id}.{$i->convert_format}";
	if ($im->AdminRight())
		$input_super_right = 1;	
	if ($id_article>0)
	{
		if ($a->HasImage($id))
		{
			$aimage = $a->ImageGet($id);
			$align = $aimage['align'];
			$size = $aimage['size'];
			$zoom = $aimage['zoom'];
			$caption_art = $aimage['caption_art'];
		}
		else
		{
			$action2 = "add";
			$caption_art = $caption;
		}
	}
}
else
{
	$image_date = date("Y-m-d");
	$action2 = "insert";
	$id_licence = $hh->ini->Get("id_licence");
}

?>

<script type="text/javascript">
$(document).ready(function() {
$("#form1").validate({
		rules: {
			caption: "required",
			caption_art: "required"
	 }
	});
});
</script>

<?php
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","image");
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("action2",$action2);
echo $hh->input_hidden("id_image",$id);
echo $hh->input_hidden("image_update",$action2=="insert");
echo $hh->input_table_open();

$img_sizes = $i->img_sizes;
$img_sizes[-1] = "orig." . (($id>0)? " ({$irow['width']})":"");
if ($action2=="insert")
{
	echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
	echo $hh->input_upload("choose_file","img",50,$input_right);
	echo $hh->input_date_text("date_original","image_date",$image_date,10,0,$input_right);
	echo $hh->input_text("author","author",$author,50,0,$input_right);
	echo $hh->input_textarea("source","source",$source,70,3,"",$input_right);
	echo $hh->input_note("caption_instr");
	echo $hh->input_textarea("caption_original","caption",$caption,70,5,"",$input_right);
	echo $hh->input_keywords($id,$o->types['image'],$keywords,$input_right);
	if ($hh->ini->Get("licences"))
		echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_right);	
	if($id_topic>0)
	{
		$tikeywords = $t->KeywordsInternal($o->types['image']);
		echo $hh->input_internal_keywords($id,$tikeywords,"image",$topic_right,$input_right);
	}
	echo $hh->input_separator($hh->tr->Translate("image_article"));
	echo $hh->input_array("alignment","align",$align,$hh->tr->Translate("image_aligns"),$input_right);
	echo $hh->input_array("width","size",$size,$img_sizes,$input_right);
	echo $hh->input_checkbox("zoom","zoom",$zoom,0,$input_right);
}
else
{
	$width = $i->img_sizes[2];
	$height = 0;
	echo "<tr><td>&nbsp;</td><td>";
	echo $hh->Graphic("/images/upload.php?src=$filename_copy&format={$irow['format']}",$width,$height,false);
	if ($fm->Exists("uploads/$filename_orig"))
	{
		echo "&nbsp;<a href=\"/images/upload.php?src=$filename_orig\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a>";
		echo " ({$irow['width']}x{$irow['height']} px - " . $fm->Size("uploads/$filename_orig") . ")";
	}
	echo "</td></tr>\n";
	echo $hh->input_upload("substitute_with","img",50,$input_super_right);
	if($id_article>0)
	{
		echo $hh->input_separator($hh->tr->Translate("image_article"));
		echo $hh->input_array("alignment","align",$align,$hh->tr->Translate("image_aligns"),$input_right);
		echo $hh->input_array("width","size",$size,$img_sizes,$input_right);
		echo $hh->input_checkbox("zoom","zoom",$zoom,0,$input_right);
		echo $hh->input_note("caption_instr");
		echo $hh->input_textarea("caption","caption_art",$caption_art,70,5,"",$input_right);
	}
	if ($id>0)
	{
		echo "<tr><td>&nbsp;</td><td>";
		$articles = $im->Articles();
		$subtopics = $im->Subtopics();
		if(count($articles) > 0 || count($subtopics) > 0)
		{
			echo "<h3>" . $hh->tr->Translate("image_used") . "</h3>";
			if (count($articles) > 0)
			{
				echo "<ul>";
				foreach($articles as $art)
					echo "<li>{$art['topic_name']}: <a href=\"/articles/image.php?id=$id&id_article={$art['id_article']}&w=$w\">{$art['headline']}</a></li>\n";
				echo  "</ul>\n";
			}
			if(count($subtopics) > 0)
			{
				echo "<ul>";
				foreach($subtopics as $sub)
					echo "<li>{$sub['topic_name']}: <a href=\"/topics/subtopic.php?id={$sub['id_subtopic']}&id_topic={$sub['id_topic']}\">{$sub['name']}</a></li>\n";
				echo  "</ul>\n";
			}
		}
		else
			echo "<p>" . $hh->tr->Translate("image_used_not") . "</p>\n";
		echo "</td></tr>\n";
	}
	
	if($input_super_right)
	{
		echo $hh->input_separator($hh->tr->Translate("image_original"));
		echo $hh->input_date_text("date_original","image_date",$image_date,10,0,$input_super_right);
		echo $hh->input_text("author","author",$author,50,0,$input_super_right);
		echo $hh->input_textarea("source","source",$source,70,3,"",$input_super_right);
		echo $hh->input_note("caption_instr");
		echo $hh->input_textarea("caption_original","caption",$caption,70,5,"",$input_super_right);
		echo $hh->input_keywords($id,$o->types['image'],$keywords,$input_super_right);
		if ($hh->ini->Get("licences"))
			echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_super_right);		
		if($id_topic>0)
		{
			$tikeywords = $t->KeywordsInternal($o->types['image']);
			echo $hh->input_internal_keywords($id,$tikeywords,"image",$topic_right,$input_super_right);
		}
	}
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right && $id_article>0) || $input_super_right);
if ($id>0)
{
	$actions[] = array('action'=>"delete",'label'=>"image_delete",'right'=>($input_right  || $input_super_right) && ($action2 != "add" && $id_article>0));
	$actions[] = array('action'=>"remove",'label'=>"image_remove",'right'=>$input_right && count($articles)<1 && count($subtopics)<1 && !($im->IsInGalleries()) && $input_super_right);
}
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

