<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/articles.php");

$title[] = array('search','article_search.php');
$title[] = array('results','');
echo $hh->ShowTitle($title);

$params = array(	'title' => $get['title'],
					'author' => $get['author'],
					'content' => $get['content'],
					'id_topic' => (int)$get['id_topic'],
					'id_subtopic' => (int)$get['id_subtopic'],
					'id_article' => (int)$get['id_article'],
					'written1' => $get['written1_y']."-".$get['written1_m']."-".$get['written1_d'],
					'written2' => $get['written2_y']."-".$get['written2_m']."-".$get['written2_d']
					);

$num = Articles::Search( $row, $params );

$table_headers = array('image','date','author','topic','in','title',' ');
$table_content = array('{ThumbImage($row[id_image],"images",$row[format],$row[id_image])}','{FormatDate($row[written_ts])}','$row[author]','$row[topic_name]','{PathToSubtopic($row[id_topic],$row[id_subtopic])}',
'<div>$row[halftitle]</div><div>$row[headline]</div><div>$row[subhead]</div>','{LinkTitle("article.php?id=$row[id_article]&w=articles","' . $hh->tr->Translate("open") . '")}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

