<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/article.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper(true);
$post = $fh->HttpPost();

if(count($post)==0 && count($_GET)==0)
	header("Location: /gate/index.php");

$action2	= $post['action2'];
$from		= $post['from'];
$w		= $post['w'];
$p		= $post['p'];
$u		= $post['u'];
$from3	= $_GET['from3'];

$id_article	= $post['id_article'];
$a = new Article($id_article);


if ($from=="article")
{
	$action 		= $fh->ActionGet($post);
	$show_latest		= $fh->Checkbox2bool($post['show_latest']);
	$new_latest		= $fh->Checkbox2bool($post['new_latest']);
	$written		= $post['written_y']."-".$post['written_m']."-".$post['written_d'];
	$show_date		= $fh->Checkbox2bool($post['show_date']);
	$id_user		= $post['id_user'];
	$author		= $post['author'];
	$author_notes		= $post['author_notes'];
	$source		= $post['source'];
	$original		= $fh->CheckDateString($post['original']);
	$show_author		= $fh->Checkbox2bool($post['show_author']);
	$highlight		= $fh->Checkbox2bool($post['highlight']);
	$jump_to_source		= $fh->Checkbox2bool($post['jump_to_source']);
	$show_source		= $fh->Checkbox2bool($post['show_source']);
	$id_translation	= $post['id_translation'];
	$translator		= $post['translator'];
	$translation_notes	= $post['translation_notes'];
	$id_topic		= $post['id_topic'];
	$id_subtopic		= $fh->Null2Zero($post['id_subtopic']);
	$id_language	= $fh->String2Number($post['id_language'],$fh->va->tr->id_language);
	$halftitle		= $fh->StringClean($post['halftitle']);
	$headline		= $fh->StringClean($post['headline']);
	if($ah->uppercase_warning && $post['headline']===strtoupper($post['headline']) && $id_language!=6 && $id_language!=10)
		$ah->MessageSet("uppercase_warning",array($fh->SubmitEscape(($post['headline']))));
	$subhead		= $fh->StringClean($post['subhead']);
	$headline_visible	= $fh->Checkbox2bool($post['headline_visible']);
	$content		= $fh->Text($post['content']);
	$is_html		= $fh->Checkbox2bool($post['is_html']);
	$notes			= $post['notes'];
	$align			= $fh->Null2Zero($post['align']);
	$available		= $fh->Checkbox2bool($post['available']);
	$allow_comments	= $fh->Checkbox2bool($post['allow_comments']);
	$at_work		= $fh->Checkbox2bool($post['at_work']);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$id_licence		= $fh->Null2Zero($post['id_licence']);
	if ($at_work=="1")
		$approved = "-1";
	if(trim($source)=="")
		$jump_to_source = "0";
	$approved_old	= $fh->Null2Zero($post['approved_old']);
	$id_visibility	= $fh->Null2Zero($post['id_visibility']);
	$furl			= $post['furl'];
	$id_template	= $fh->Null2Zero($post['id_template']);
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	if($action != "delete")
	{
		$a->ArticleStore($written, $show_date, $id_user, $author, $author_notes, $source, $original, $show_author, $id_topic,
				$id_subtopic,$halftitle, $headline, $subhead, $headline_visible, $content, $is_html, $notes, $align, $id_language,
				$approved, $approved_old, $id_visibility, $keywords, $available, $show_source, $show_latest, $new_latest,$id_licence,
				$allow_comments,$ikeywords,$jump_to_source,$id_template,$furl,$highlight);
	}
	if($id_translation>0)
	{
		include_once(SERVER_ROOT."/../classes/translations.php");
		$tra = new Translations();
		$tra->TranslationArticle($id_translation,$a->id,$translator,$translation_notes,$approved);
	}
	if($id_template>0)
	{
		$unescaped_template_params	= $fh->SerializeParams(false);
		$a->TemplateParamsUpdate($unescaped_template_params);
		$url = "/topics/articles.php?id=$id_topic&p=$p&id_template=$id_template";
	}
	elseif ($w=="topics")
	{
		if($approved!=$approved_old)
			$p = 1;
		$url = "/topics/articles.php?id=$id_topic&appr=$approved&p=$p";
		if($id_template>0)
			$url .= "&id_template=$id_template";
	}
	elseif($w=="tra")
	{
		$url = "/translations/translation.php?id=$id_translation&id_a=".$a->id;
	}
	else
		$url = "/articles/articles.php?p=$p&u=$u";
	if ($action == "delete")
		$url = "/articles/article_delete.php?p=$p&id=$a->id&w=$w&p=$p";
	if ($action == "relate")
		$url = "/articles/related.php?id_article=$a->id&id_topic=$id_topic&w=$w&p=1";
	if ($action == "add_box")
		$url = "/articles/box.php?id=0&id_article=$a->id&w=$w&p=$p";
	if ($action == "choose_box")
		$url = "/articles/boxes.php?id_article=$a->id&id_topic=$id_topic&w=$w&p=1";
	if ($action == "add_doc")
		$url = "/articles/doc.php?id=0&id_article=$a->id&w=$w&p=$p";
	if ($action == "choose_doc")
		$url = "/articles/docs.php?id_article=$a->id&id_topic=$id_topic&w=$w&p=1";
	if ($action == "add_image")
		$url = "/articles/image.php?id=0&id_article=$a->id&w=$w&p=$p";
	if ($action == "choose_image")
		$url = "/articles/images.php?id_article=$a->id&id_topic=$id_topic&w=$w&p=1&id_topic2=$id_topic";
	if ($action == "associate_image")
		$url = "/articles/image_associate.php?id_article=$a->id&w=$w&p=$p";
	header("Location: " . $url);
}

if ($from=="article_delete")
{
	$action 	= $fh->ActionGet($post);
	$id_topic		= $post['id_topic'];
	if ($action == "delete_ok")
	{
		$a->ArticleDelete();
		if ($w=="topics")
			$url = "/topics/articles.php?id=$id_topic&p=$p";
		else
			$url = "/articles/articles.php?p=$p";
	}
	else
		$url = "/articles/article.php?p=$p&id=$id_article&w=$w";
	header("Location: " . $url);
}

if ($from=="article_copy")
{
	$action 	= $fh->ActionGet($post);
	$id_topic		= $post['id_topic'];
	$id_subtopic		= $fh->Null2Zero($post['id_subtopic']);
	$link_images		= $fh->Checkbox2bool($post['link_images']);
	$link_docs		= $fh->Checkbox2bool($post['link_docs']);
	if ($action == "copy")
	{
		$article = $a->ArticleGet();
		$id_template = (int)$article['id_template'];
		$is_template_present = true;
		if($id_template>0)
		{
			$is_template_present = false;
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$templates = $t->Templates(5);
			foreach($templates as $template)
			{
				if($template['id_template']==$id_template)
				{
					$is_template_present = true;
				}
			}
		}
		if($is_template_present)
		{
			$id_article_new = $a->ArticleCopy($id_topic,$id_subtopic,$link_images,$link_docs);
			$url = "/articles/article.php?id=$id_article_new&w=topics&id_topic=$id_topic";		
		}
		else 
		{
			$fh->va->MessageSet("error","error_file_missing");
			$url = "/articles/article.php?id=$id_article&w=articles";
		}
	}
	else
		$url = "/articles/article.php?id=$id_article&w=articles";
	header("Location: " . $url);
}

if ($from=="box")
{
	$action 	= $fh->ActionGet($post);
	$id_box	= $post['id_box'];
	$title		= $fh->StringClean($post['title']);
	$content	= $post['content'];
	$is_html	= $fh->Checkbox2bool($post['is_html']);
	$width		= $post['width'];
	$align		= $post['align'];
	$popup	= $fh->Checkbox2bool($post['popup']);
	$z_width		= $fh->String2Number($post['z_width'],400);
	$z_height		= $fh->String2Number($post['z_height'],400);
	$show_title	= $fh->Checkbox2bool($post['show_title']);
	$notes		= $post['notes'];
	$id_type	= (int)$post['id_type'];
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	if ($action == "insert")
		$a->BoxInsert($title, $content, $is_html, $popup, $width, $align, $show_title, $id_article,$notes,$id_type,$keywords,$ikeywords,$z_width,$z_height);
	if ($action == "update")
		$a->BoxUpdate($id_box, $title, $content, $is_html, $popup, $width, $align, $show_title,$notes,$id_type,$keywords,$ikeywords,$z_width,$z_height);
	if ($action == "delete")
		$a->BoxDelete($id_box);
	header("Location: /articles/article.php?id=$id_article&w=$w");
}

if ($from=="image")
{
	$action 	= $fh->ActionGet($post);
	$id_image	= $post['id_image'];
	$id_article	= $post['id_article'];
	$id_topic	= $post['id_topic'];
	$image_date	= $fh->CheckDateString($post['image_date']);
	$caption	= $post['caption'];
	$source	= $post['source'];
	$author	= $post['author'];
	$keywords	= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$id_licence	= $fh->Null2Zero($post['id_licence']);
	$align		= $post['align'];
	$size		= $post['size'];
	$image_update	= $post['image_update'];
	$zoom		= $fh->Checkbox2bool($post['zoom']);
	$caption_art	= $post['caption_art'];
	include_once(SERVER_ROOT."/../classes/image.php");
	$im = new Image($id_image);
	if($id_article>0 && $id_image>0 && $action=="store")
		$fh->va->NotEmpty($caption_art,"caption");
	if ($image_update && $action=="store")
		$fh->va->NotEmpty($caption,"caption");
	if($fh->va->return)
	{
		if($action=="store")
		{
			$file		= $fh->UploadedFile("img",true);
			if ($action2=="insert")
			{
				if ($file['ok'])
				{
					$im->ImageInsert($image_date,$source,$author,$id_licence,$caption,$keywords,$file,$ikeywords);
					if ($im->id>0 && $id_article>0)
						$im->ArticleAdd($im->id,$id_article,$caption,$align,$size,$zoom,false);
					else
						$fh->va->MessageSet("error","error_image_insert");
				}
				else 
					$fh->va->MessageSet("error","error_file_missing");
			}
			else // update
			{
				if ($im->AdminRight())
				{
					$im->ImageUpdate($id_image,$image_date,$source,$author,$id_licence,$caption,$keywords,$file,$ikeywords);
					if($file['ok'] && $id_article>0)
					{
						$im->ArticleUpdateSuptopic($id_image,$id_article);
					}
				}
				if ($action2=="add")
					$im->ArticleAdd($id_image,$id_article,$caption_art,$align,$size,$zoom);
				if ($action2=="update" && $id_article>0)
					$im->ArticleUpdate($id_image,$id_article,$caption_art,$align,$size,$zoom);
			}
		}
		if ($action=="delete")
			$im->ArticleDelete($id_image,$id_article);
		if ($action=="remove")
			$im->ArticleRemove($id_image,$id_article);
		if ($id_topic>0)
		{
			if ($id_article>0)
				$goto = "/articles/article.php?id=$id_article&w=$w";
			else
				$goto = "/topics/images.php?id=" . $id_topic;
		}
		else
			$goto = "/admin/image_orphans.php";
	}
	else
		$goto = "/articles/image.php?id=$id_image&id_article=$id_article&w=$w";	
	header("Location: $goto");
}

if ($from=="image_associate")
{
	$action 	= $fh->ActionGet($post);
	$id_image	= $post['associate'];
	$size		= $post['size'];
	$align		= $post['align'];
	if($action=="update")
		$a->ImageAssociate($id_image,$size,$align);
	header("Location: /articles/article.php?id=$id_article&w=$w&p=$p");
}

if ($from=="doc")
{
	$action 	= $fh->ActionGet($post);
	$id_doc	= $post['id_doc'];
	$id_article	= $post['id_article'];
	$id_topic	= $post['id_topic'];
	$title		= $post['title'];
	$description	= $post['description'];
	$id_language	= $post['id_language'];
	$id_licence	= $fh->Null2Zero($post['id_licence']);
	$source	= $post['source'];
	$author	= $post['author'];
	$seq	= $fh->String2Number($post['seq']);
	$id_subtopic_form	= $fh->String2Number($post['id_subtopic_form']);
	$keywords	= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	include_once(SERVER_ROOT."/../classes/doc.php");
	$d = new Doc($id_doc);
	$fh->va->NotEmpty($title,"title");
	if($fh->va->return)
	{
		if($action=="store")
		{
			$file		= $fh->UploadedFile("doc",true);
			if ($action2=="insert")
			{
				if ($file['ok'])
				{
					$id_doc = $d->DocInsert($title,$description,$id_language,$source,$author,$id_licence,$keywords,$file,$ikeywords);
					if ($id_doc>0 && $id_article>0)
						$d->ArticleAdd($id_doc,$id_article,$seq,$id_subtopic_form);
					else
						$fh->va->MessageSet("error","error_doc_insert");
				}
				else 
					$fh->va->MessageSet("error","error_file_missing");
			}
			else
			{
				$a->ArticleLoad();
				if ($action2=="add")
					$d->ArticleAdd($id_doc,$id_article,$seq,$id_subtopic_form);
				elseif ($action2=="update")
					$d->ArticleUpdate($id_doc, $id_article, $id_subtopic_form);
				$update_right = Modules::AmIAdmin(4) || Modules::AmIAdmin(14) || $a->id_user==$ah->current_user_id || $ah->current_user_id==($d->CreatorId());
				if($a->id_topic>0)
				{
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($a->id_topic);
					$update_right = $update_right || $t->AmIAdmin();
				}
				if ($update_right)
				{
					$d->DocUpdate($title,$description,$id_language,$source, $author, $id_licence,$keywords,$id_article,$file,$ikeywords);
				}
			}
		}
		if ($action=="delete")
			$d->ArticleDelete($id_doc,$id_article);
		if ($action=="remove")
			$d->ArticleRemove($id_doc,$id_article);
		if ($id_topic>0)
		{
			if ($id_article>0)
				$goto = "/articles/article.php?id=$id_article&w=$w";
			else
				$goto = "/topics/docs.php?id=" . $id_topic;
		}
		else
			$goto = "/admin/doc_orphans.php";
	}
	else
		$goto = "/articles/doc.php?id=$id_doc&id_article=$id_article&w=$w";	
	header("Location: $goto");
}

if ($from=="doc_token")
{
	$action 	= $fh->ActionGet($post);
	$id_doc	= $post['id_doc'];
	$id_article	= $post['id_article'];
	$id_topic	= $post['id_topic'];
	$token	= $post['token'];
	$expires	= $fh->Checkbox2bool($post['expires']);
	$max_downloads		= $fh->Null2Zero($post['max_downloads']);
	$expire_date	= $fh->DateMerge("expire_date",$post);
	
	include_once(SERVER_ROOT."/../classes/doc.php");
	$d = new Doc($id_doc);
	$fh->va->GreaterThenZero($id_doc,"title");
	if($fh->va->return)
	{
		if($action=="store")
		{
			$token_new = $d->TokenStore($token,$max_downloads,$expires,$expire_date);
		}
		if ($action=="delete")
		{
			$d->TokenDelete($token);
		}
	}
	if($d->IsToken($token) || $action =="delete")
	{
		$url = "/articles/doc_tokens.php?id=$id_doc&id_article=$id_article&w=$w&id_topic=$id_topic";	
	}
	else 
	{
		$url = "/articles/doc_token.php?id=$id_doc&id_article=$id_article&w=$w&id_topic=$id_topic&token=$token_new";
	}
	header("Location: $url");
}

if ($from3=="doc")
{
	$action3	= $_GET['action3'];
	include_once(SERVER_ROOT."/../classes/doc.php");
	$id_doc	= $_GET['id'];
	$id_article	= $_GET['id_article'];
	$d = new Doc($id_doc);
	if ($action3=="up")
		$d->Move($id_article,1);
	if ($action3=="down")
		$d->Move($id_article,0);
	header("Location: article.php?w=topics&id=$id_article");
}

if ($from3 == "related")
{
	$action3	= $_GET['action3'];
	$id_article	= $_GET['id_article'];
	$id_related	= $_GET['id_related'];
	$with_content = ($action3=="embed")?"1":"0";
	$w		= $_GET['w'];
	$a->id = $id_article;
	if ($action3=="relate" || $action3=="embed")
		$a->Relate($id_related,$with_content);
	if ($action3=="unrelate")
		$a->Unrelate($id_related);
	header("Location: /articles/article.php?id=$id_article&w=$w");
}

?>
