<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/images.php");

$i = new Images();

$id_article = (int)$_GET['id_article'];
$id_topic = (int)$_GET['id_topic'];
$w = (!isset($_GET['w']))? $_GET['w'] : "topics";

if ($module_admin)
	$input_right = 1;

include_once(SERVER_ROOT."/../classes/article.php");
$a = new Article($id_article);
$a->ArticleLoad();
$title1[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$id_topic = $a->id_topic;
if ($a->id_user==$ah->current_user_id)
	$input_right = 1;
$title1[] = array('image_associated','');

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
		$input_right = 1;
	if ($w=="topics")
	{
		$ah->ModuleForce(4);
		$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
		$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
	}
	else
		$title[] = array('list','articles.php');
	$title = array_merge($title,$title1);
}

echo $hh->ShowTitle($title);
?>

<script type="text/javascript"><!--
function radioclick(radio_id)
{
	var list = document.getElementsByTagName ('li');
	for (var i = 0; i < list.length; ++i)
	if (list[i].id.match('aimage') != '')
            		list[i].className = '';	
	radio_selected = document.getElementById ('aimage' + radio_id);
	radio_selected.className = 'associated';
}
//-->
</script>
<?php
$size = $i->default_img_size;
$aimage = $a->Image();

echo $hh->input_form_open();
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","image_associate");
echo $hh->input_hidden("w",$w);
echo $hh->input_table_open();
echo $hh->input_note("image_associated_exp");

$images = $a->ImageGetAll();
echo "<tr><td>" . $hh->tr->Translate("image_associated") . "</td><td>";
if(count($images)>0)
{
	echo "<ul class=\"article-images\">";
	echo "<li id=\"aimage0\" " . (($aimage['id_image']>0)? "":" class=\"associated\"") . ">";
	echo "<input type=\"radio\" name=\"associate\" " . (($aimage['id_image']>0)? "":"checked=\"checked\" ") . " value=\"0\" class=\"input-radio\"  onclick=\"radioclick(0)\">" . $hh->tr->Translate("none") . "</li>\n";
	$img_sizes = $conf->Get("img_sizes");
	foreach($images as $image)
	{
		$image_file = "images/0/{$image['id_image']}." . $conf->Get("convert_format");
		$width = $img_sizes[0];
		echo "<li id=\"aimage{$image['id_image']}\" " . (($aimage['id_image']==$image['id_image'])? " class=\"associated\" ":"") . ">";
		echo "<input type=\"radio\" name=\"associate\"  value=\"{$image['id_image']}\" " . (($aimage['id_image']==$image['id_image'])? "checked=\"checked\" ":"") . " class=\"input-radio\" onclick=\"radioclick({$image['id_image']})\">";
		echo "<img src=\"/images/upload.php?src=$image_file&format={$image['format']}\" width=\"$width\" alt=\"" . htmlspecialchars($image['caption']) . "\"></a></li>\n";		
	}
	echo "</ul>";
}
echo "</td></tr>";
echo $hh->input_array("width","size",$aimage['id_image']>0? $aimage['size']:$t->row['associated_image_size'],$i->img_sizes,$input_right && !$ui);
echo $hh->input_array("alignment","align",$aimage['align'],$hh->tr->Translate("image_aligns"),$input_right && !$ui);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

