<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");

// INIT
$id_article = $get['id_article'];
$w = $get['w'];
if (!isset($w))
	$w = "topics";
$view = $get['view'];

$a = new Article($id_article);
$a->ArticleLoad();
$id_topic = $a->id_topic;
$t = new Topic($id_topic);

// TITLES
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('articles_list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('articles_related','');
echo $hh->ShowTitle($title);

echo $hh->tr->TranslateParams("article_related_select",array($a->headline));
echo $hh->tr->TranslateParams("article_related_results",array($t->name,"id_article=$id_article&id_topic=$id_topic&w=$w","id_article=$id_article&w=$w"));

include_once(SERVER_ROOT."/../classes/articles.php");

$params = array(	'title' => $get['title'],
					'author' => $get['author'],
					'content' => $get['content'],
					'id_topic' => $get['id_topic'],
					'id_subtopic' => $get['id_subtopic'],
					'id_article' => $get['id_article'],
					'w' => $get['w'],
					'written1' => $get['written1_y']."-".$get['written1_m']."-".$get['written1_d'],
					'written2' => $get['written2_y']."-".$get['written2_m']."-".$get['written2_d']
					);
$num = Articles::SearchRelated( $row, $params );

$table_headers = array('date','topic','in','title','author','action');
$table_content = array('{FormatDate($row[written_ts])}','$row[topic_name]','{PathToSubtopic($row[id_topic],$row[id_subtopic])}',
'<div>$row[halftitle]</div><div><strong>$row[headline]</strong></div><div>$row[subhead]</div>','$row[author]',
'<a href=\"actions.php?from3=related&action3=relate&id_article='.$id_article.'&id_related=$row[id_article]&w='.$w.'\">'.$hh->tr->Translate("link").'</a> / 
<a href=\"actions.php?from3=related&action3=embed&id_article='.$id_article.'&id_related=$row[id_article]&w='.$w.'\">'.$hh->tr->Translate("embed").'</a>');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

