<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");

$id = $_GET['id'];
$w = $_GET['w'];

$a = new Article($id);
$article = $a->ArticleGet();
if ($article['id_user']==$ah->current_user_id)
	$input_right = 1;

$id_topic = $article['id_topic'];
include_once(SERVER_ROOT."/../classes/topic.php");
$t = new Topic($id_topic);

if ($ah->ModuleAdmin(4) || $t->AmIAdmin())
	$input_right = 1;

if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id=' . $id_topic);
	$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
}
else
	$title[] = array('list','articles.php');

$title[] = array($article['headline'],'/articles/article.php?w='.$w.'&id='.$id.'&id_topic='.$id_topic);
$title[] = array("hardcopy",'');

echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php",false,"","article-form");
echo $hh->input_hidden("id_article",$id);
echo $hh->input_hidden("from","article_copy");
echo $hh->input_table_open();

echo $hh->input_separator("placing");
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->User($ah->current_user_id);

echo $hh->input_topics($id_topic,0,$topics,"choose_option",$input_right);
echo $hh->input_subtopic("subtopic","id_subtopic","'article_subtopic.php?id_topic='+document.forms['article-form'].id_topic.value+'&id_subtopic='+document.forms['article-form'].id_subtopic.value",0,$id_topic,$input_right,'article-form');

echo $hh->input_checkbox("link_images","link_images",1,0,$input_right);
echo $hh->input_checkbox("link_docs","link_docs",1,0,$input_right);

$actions = array();
$actions[] = array('action'=>"copy",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


