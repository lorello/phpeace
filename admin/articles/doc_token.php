<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/doc.php");

$id = (int)$_GET['id'];
$id_article = (int)$_GET['id_article'];
$id_topic = (int)$_GET['id_topic'];
$w = (!isset($_GET['w']))? $_GET['w'] : "topics";
$token = $_GET['token'];

if ($id_article>0)
{
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id_article);
	$a->ArticleLoad();
	$title1[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
	$id_topic = $a->id_topic;
	if ($a->id_user==$ah->current_user_id)
		$input_right = 1;
}
else
	$title1[] = array('docs_archive','/topics/docs.php?id='.$id_topic);

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
	{
		$input_right = 1;
	}
	if ($w=="topics")
	{
		$ah->ModuleForce(4);
		$module_admin = $ri->ModuleAdmin();
		$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
		if($id_article>0)
			$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
	}
	else
		$title[] = array('list','articles.php');
	$title = array_merge($title,$title1);
}
else
{
	$ah->ModuleForce(14);
	$module_admin = $ri->ModuleAdmin();
	$title[] = array('doc_orphans','/admin/doc_orphans.php');
}

if ($module_admin)
{
	$input_right = 1;
}

$d = new Doc($id);
$doc = $d->DocGet();
if ($ah->current_user_id==($d->CreatorId()))
	$input_right = 1;
if($d->AdminRight())
	$input_right = 1;

if($d->IsToken($token))
{
	$row = $d->TokenGet($token);
}

$title[] = array($doc['title'],"doc.php?id=$id&id_topic=$id_topic&id_article=$id_article&w=$w");
$title[] = array('tokens',"doc_tokens.php?id=$id&id_topic=$id_topic&id_article=$id_article&w=$w");
$title[] = array('token','');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","doc_token");
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("id_doc",$id);
echo $hh->input_hidden("token",$token);

if($d->IsToken($token) && $row['active'])
{
	echo $hh->input_code("token_link","link",$ini->Get('pub_web') . "/tools/download.php?t=" . $row['token']);
}

echo $hh->input_table_open();

if($d->IsToken($token))
{
	echo $hh->input_text("token","token",$row['token'],32,0,0);
	echo $hh->input_checkbox("active","active",$row['active'],0,0);
	echo $hh->input_text("counter","counter",$row['counter'],5,0,0);
}
echo $hh->input_text("limit_download","max_downloads",$row['max_downloads'],5,0,$input_right);
echo $hh->input_checkbox("expires","expires",$row['expires'],0,$input_right);
echo $hh->input_date("date_end","expire_date",$row['expire_date_ts'],$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0 && $d->IsToken($token));
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>
