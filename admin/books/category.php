<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id = $_GET['id'];
$id_publisher = $_GET['id_publisher'];

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($trm16->Translate("publishers"),'publishers.php');
$p = new Publisher;
$row = $p->PublisherGet($id_publisher);
$title[] = array($row['name'],'publisher.php?id='.$id_publisher);
$title[] = array($trm16->Translate("categories"),'categories.php?id='.$id_publisher);

if ($id>0)
{
	$action2 = "update";
	$cat = $p->CategoryGet($id,$id_publisher);
	$title[] = array($cat['name'],'');
}
else
{
	$action2 = "insert";
	$title[] = array('add_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($id>0)
{
	$bb = new Books();
	$num1 = $bb->BooksApproved( $row2, array('id_c'=>$id) );
	$num2 = $bb->BooksApproved( $row2, array('approved'=>1,'id_c'=>$id) );
	$num3 = $bb->BooksApproved( $row2, array('approved'=>0,'id_c'=>$id) );
	echo "<h3><a href=\"books.php?id_c=$id\">$num1 " . $hh->tr->Translate("books") . "</a></h3>";
	if ($num1>0)
	{
		echo "<ul>\n";
		echo "<li>" . $hh->tr->Translate("approveds") . ": " . $hh->Wrap($num2,"<a href=\"books.php?id_c=$id&approved=1\">","</a>",$num2>0) . "</li>";
		echo "<li>" . $hh->tr->Translate("to_approve") . ": " . $hh->Wrap($num3,"<a href=\"books.php?id_c=$id&approved=0\">","</a>",$num3>0) . "</li>";
		echo "</ul>\n";
	}
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","category");
echo $hh->input_hidden("id_publisher",$id_publisher);
echo $hh->input_hidden("id_category",$id);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$cat['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$cat['description'],80,3,"",$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

