<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/file.php");

$id = $_GET['id'];
$b = new Book($id);
$fm = new FileManager;

$trm16 = new Translator($hh->tr->id_language,16);

if ($module_admin || $module_right)
	$input_right = 1;

$title[] = array('list','books.php');
$title[] = array($b->title,'book.php?id='.$id);
$title[] = array($trm16->Translate("cover"),'');
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("details",'book.php?id='.$id);
$tabs[] = array($trm16->Translate("cover"),'');
if($ah->IsModuleActive(31))
	$tabs[] = array("ebook",'ebook.php?id='.$id);
$tabs[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id);
echo $hh->Tabs($tabs);

$maxfilesize = $fm->MaxFileSize();

echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("from","cover");
echo $hh->input_hidden("id_book",$id);
echo $hh->input_table_open();

$i = new Images();

if(!$id>0)
	echo $hh->input_note($trm16->Translate("cover_warning"));

if ($b->cover_format!="")
{
	$filename = "covers/2/$id".".".$i->convert_format;
	echo "<tr><td align=\"right\">" . $trm16->Translate("cover") . "</td>";
	echo "<td><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[2]}\"></td></tr>\n";
	echo $hh->input_upload("substitute_with","img",50,$input_right);
}
else
	echo $hh->input_upload("choose_file","img",50,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"image_delete",'right'=>($input_right && $b->cover_format!=""));
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

