<?php
/********************************************************************

   PhPeace - EPG Management Module

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if($ah->IsModuleActive(31))
{
	include_once(SERVER_ROOT."/../modules/ebook.php");
	$eb = new EBook();
	
	$id_ebook = (int)$get['id'];
	
	$trm16 = new Translator($hh->tr->id_language,16);
	
	if ($module_admin)
		$input_right = 1;
	
	$row = $eb->EbookGet($id_ebook);
	
	$id_book = $row['id_book'];
	$title[] = array('list','/books/books.php');
	$title[] = array($row['title'],"/books/book.php?id=$id_book");
	$title[] = array("ebook","ebook.php?id=$id_book");
	$title[] = array("params",'');
	
	echo $hh->ShowTitle($title);
	
	$tabs = array();
	$tabs[] = array("details",'book.php?id='.$id_book);
	$tabs[] = array($trm16->Translate("cover"),'book_cover.php?id='.$id_book);
	$tabs[] = array("ebook",'ebook.php?id='.$id_book);
	$tabs[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id_book);
	echo $hh->Tabs($tabs);
	
	
	echo $hh->input_form("post","actions.php");
	echo $hh->input_hidden("from","ebook_params");
	echo $hh->input_hidden("id_ebook",$id_ebook);
	echo $hh->input_table_open();
	
	$params = $eb->EbookTypeParams($row['id_type']);
	
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$params_values = $v->Deserialize($row['params']);
	
	foreach ($params as $param)
	{
		echo $hh->input_param($param,$params_values[$param],0,$input_right);
	}
	
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	
	echo $hh->input_table_close() . $hh->input_form_close();
	
	
}
include_once(SERVER_ROOT."/include/footer.php");
?>
