<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");
include_once(SERVER_ROOT."/../classes/geo.php");

$id = $_GET['id'];

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($trm16->Translate("publishers"),'publishers.php');
if ($id>0)
{
	$action2 = "update";
	$p = new Publisher;
	$row = $p->PublisherGet($id);
	$title[] = array($row['name'],'');
}
else
{
	$row = "";
	$action2 = "insert";
	$title[] = array('add_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($id>0)
{
	$categories = $p->CategoriesAll($id);
	$bb = new Books();
	echo "<h3><a href=\"categories.php?id=$id\">" . count($categories) . " " . $trm16->Translate("categories") . "</a></h3";
	$num_no_cat = $bb->BooksApproved( $row2, array('id_p'=>$id,'id_c'=>0) );
	if ($num_no_cat > 0)
		echo "<p><a href=\"books.php?id_p=$id&id_c=0\">" . $trm16->TranslateParams("books_without_categories",array($num_no_cat)) . "</a></p>";

	$num1 = $bb->BooksApproved( $row2, array('id_p'=>$id) );
	$num2 = $bb->BooksApproved( $row2, array('approved'=>1,'id_p'=>$id) );
	$num3 = $bb->BooksApproved( $row2, array('approved'=>0,'id_p'=>$id) );
	echo "<h3><a href=\"books.php?id_p=$id\">$num1 " . $hh->tr->Translate("books") . "</a></h3>";
	if ($num1>0)
	{
		echo "<ul>\n";
		echo "<li>" . $hh->tr->Translate("approveds") . ": " . $hh->Wrap($num2,"<a href=\"books.php?id_p=$id&approved=1\">","</a>",$num2>0) . "</li>";
		echo "<li>" . $hh->tr->Translate("to_approve") . ": " . $hh->Wrap($num3,"<a href=\"books.php?id_p=$id&approved=0\">","</a>",$num3>0) . "</li>";
		echo "</ul>\n";
	}
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","publisher");
echo $hh->input_hidden("id_publisher",$id);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],60,0,$input_right);
echo $hh->input_textarea("address","address",$row['address'],70,2,"",$input_right);
echo $hh->input_text("postcode","zipcode",$row['zipcode'],10,0,$input_right);
echo $hh->input_text("town","town",$row['town'],20,0,$input_right);
$geo = new Geo();
echo $hh->input_geo($row['id_prov'],$input_right);
echo $hh->input_text("website","website",$row['website'],30,0,$input_right);
echo $hh->input_text("email","email",$row['email'],30,0,$input_right);
echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);
echo $hh->input_text("fax","fax",$row['fax'],20,0,$input_right);
echo $hh->input_textarea("notes","notes",$row['notes'],70,2,"",$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

