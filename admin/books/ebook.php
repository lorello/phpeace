<?php
/********************************************************************

   PhPeace - EPG Management Module

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if($ah->IsModuleActive(31))
{
	include_once(SERVER_ROOT."/../modules/ebook.php");
	$eb = new EBook();
	
	$id_book = (int)$get['id'];
	
	$trm16 = new Translator($hh->tr->id_language,16);
	
	if ($module_admin)
		$input_right = 1;
	
	$row = $eb->EbookGetByIdBook($id_book);
	$id_ebook = (int)$row['id_ebook'];
		
	include_once(SERVER_ROOT."/../modules/books.php");
	$b = new Book($id_book);
	$book = $b->BookGet();
	$title[] = array('list','/books/books.php');
	$title[] = array($b->title,"/books/book.php?id=$id_book");
	$title[] = array("ebook",'');
	
	echo $hh->ShowTitle($title);
	
	$tabs = array();
	$tabs[] = array("details",'book.php?id='.$id_book);
	$tabs[] = array($trm16->Translate("cover"),'book_cover.php?id='.$id_book);
	$tabs[] = array("ebook",'');
	$tabs[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id_book);
	echo $hh->Tabs($tabs);
	
	echo $hh->input_form("post","actions.php");
	echo $hh->input_hidden("from","ebook");
	echo $hh->input_hidden("id_ebook",$id_ebook);
	echo $hh->input_hidden("id_book",$id_book);
	echo $hh->input_table_open();
	
	echo $hh->input_array("type","id_type",$row['id_type'],$trm16->Translate("ebook_types"),$input_right);
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	echo $hh->input_row("style","id_style",$row['id_style'],$s->StylesUser($ah->current_user_id,$ah->IsModuleActive(15)),"choose",0,$input_right);
	echo $hh->input_text($trm16->Translate("rights"),"rights",$row['rights'],50,0,$input_right);
	echo $hh->input_checkbox("approved","approved",$row['approved'],0,$input_right);
	
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"publish",'label'=>"publish",'right'=>$input_right && $id_ebook>0 && $row['approved']);
	$actions[] = array('action'=>"download",'label'=>"download",'right'=>$input_right && $id_ebook>0 && $published);
	echo $hh->input_actions($actions,$input_right);
	
	echo $hh->input_table_close() . $hh->input_form_close();
	
	if( $id_ebook>0)
	{
		$fm = new FileManager();
		if($fm->Exists($eb->EbookFilename($id_ebook)))
		{
			echo "<p><a href=\"/docs/ebook.php?id=$id_ebook\" target=\"_blank\">Download</a></p>";	
		}
	}
	
}

include_once(SERVER_ROOT."/include/footer.php");
?>
