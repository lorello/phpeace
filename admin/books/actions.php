<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/books.php");
include_once(SERVER_ROOT."/../classes/varia.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();
$get = $fh->HttpGet();

$from		= $post['from'];
$from2	= $get['from2'];
$action3	= $get['action3'];

if($from2=="homepage")
{
	if($ah->ModuleAdmin(16))
	{
		$id_item = $get['id'];
		$hometype = $get['hometype'];
		$bb = new Books();
		if($action3=="delete")
			$bb->HomepageDelete($id_item,0);
		if($action3=="add")
			$bb->HomepageUpdate($id_item,0,$hometype==1);
		header("Location: homepage.php");
	}
}

if ($from=="book")
{
	$id_book 	= $post['id_book'];
	$author 	= $post['author'];
	$title 		= $post['title'];
	$summary 	= $post['summary'];
	$description	= $post['description'];
	$id_publisher	= $post['id_publisher'];
	$id_category	= $fh->String2Number($post['id_category']);
	$p_month 	= $fh->String2Number($post['p_month']);
	$p_year 	= $fh->String2Number($post['p_year']);
	$isbn 		= $post['isbn'];
	$catalog	= $post['catalog'];
	$id_language	= $post['id_language'];
	$pages 	= $fh->string2number($post['pages']);
	$notes 	= $post['notes'];
	$id_article 	= $fh->Null2Zero($post['id_article']);
	$price 		= $fh->String2Number($post['price']);
	$approved 	= $fh->Checkbox2bool($post['approved']);
	$keywords 	= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$series		= "";
	$action 		= $fh->ActionGet($post);
	$b = new Book($id_book);
	if ($action=="store")
	{
		if($id_book>0)
		{
			$b->BookUpdate($title, $summary, $description, $id_publisher, $p_year, $isbn, $id_language, $notes, $price, $p_month, $approved, $id_article, $author, $series, $pages, $keywords, $catalog, $id_category, $ikeywords);			
		}
		else 
		{
			$b->BookInsert($title, $summary, $description, $id_publisher, $p_year, $isbn, $id_language, $notes, $price, $p_month, $approved, $id_article, $author, $series, $pages, $keywords, $catalog, $id_category, $ikeywords);		
		}
	}
	if ($action=="delete")
	{
		$b->BookDelete();
	}
	header("Location: books.php?approved=$approved&id_c=$id_category");
}

if ($from=="publisher")
{
	$action 		= $fh->ActionGet($post);
	$name 			= $post['name'];
	$address 		= $post['address'];
	$zipcode		= $post['zipcode'];
	$town 			= $post['town'];
	$id_prov		= $post['id_geo'];
	$website 		= $fh->String2Url($post['website']);
	$email 			= $post['email'];
	$phone 			= $post['phone'];
	$fax 			= $post['fax'];
	$notes 			= $post['notes'];
	$id_publisher	= $post['id_publisher'];
	$p = new Publisher;
	if ($action=="update")
		$p->PublisherUpdate($id_publisher,$name,$address,$town,$website,$email,$phone,$fax,$notes,$zipcode,$id_prov);
	if ($action=="insert")
		$p->PublisherInsert( $name,$address,$town,$website,$email,$phone,$fax,$notes,$zipcode,$id_prov );
	header("Location: publishers.php");
}

if ($from=="category")
{
	$action 		= $fh->ActionGet($post);
	$name 			= $post['name'];
	$description 		= $post['description'];
	$id_image		= $fh->String2Number($post['id_image']);
	$id_publisher	= $post['id_publisher'];
	$id_category	= $post['id_category'];
	$p = new Publisher;
	if ($action=="update")
		$p->CategoryUpdate($name,$description,$id_image,$id_publisher,$id_category);
	if ($action=="insert")
		$p->CategoryInsert($name,$description,$id_image,$id_publisher);
	header("Location: categories.php?id=$id_publisher");
}

if ($from=="cover")
{
	$action 	= $fh->ActionGet($post);
	$id_book 	= $post['id_book'];
	$file		= $fh->UploadedFile("img");
	$b = new Book($id_book);
	if ($action=="store" && $file['ok'])
		$b->CoverUpdate($file);
	if ($action=="delete")
		$b->CoverDelete();
	header("Location: book.php?id=$id_book");
}

if ($from=="review")
{
	$action 	= $fh->ActionGet($post);
	$module 	= $post['module'];
	$rapproved 	= $post['rapproved'];
	$from_topic 	= $post['from_topic'];
	$id_review 	= $post['id_review'];
	$id_book 	= $post['id_book'];
	$b = new Book($id_book);
	$review 	= $post['review'];
	$vote 		= $post['vote'];
	$name 		= $post['name'];
	$email 		= $post['email'];
	$insert_date	= $post['insert_date_y']."-".$post['insert_date_m']."-".$post['insert_date_d']." 12:00:00";
	$ip		= Varia::IP();
	$id_language	= $post['id_language'];
	$id_topic 	= $post['id_topic'];
	$approved	= $fh->Checkbox2bool($post['approved']);
	$important	= $fh->Checkbox2bool($post['important']);
	$unescaped_params	= $fh->SerializeParams(false);
	if ($action=="update")
		$b->ReviewUpdate($id_review,$review,$vote,$name,$email,$insert_date,$ip,$approved,$important,$id_language,$unescaped_params,$id_topic);
	if ($action=="insert")
		$b->ReviewInsert($review,$vote,$name,$email,$insert_date,$ip,$approved,$important,0,$id_language,$unescaped_params,$id_topic);
	if ($action=="delete")
		$b->ReviewDelete($id_review);
	if($module=="topics")
		header("Location: /topics/books_reviews.php?approved=$rapproved&id_topic=$id_topic");
	else
		header("Location: reviews.php?id_book=$id_book");
}

if ($from=="review_param")
{
	$id_param	= $post['id_param'];
	$label		= $post['label'];
	$id_type	= $post['type'];
	$params	= $post['param_params'];
	$public	= $fh->Checkbox2bool($post['public']);
	include_once(SERVER_ROOT."/../classes/resources.php");
	$r = new Resources();
	$action = $fh->ActionGet($post);
	if($action=="update")
		$id_param = $r->ParamStore("review",$id_param,$id_type,$label,$params,$public);
	if($action=="delete")
		$id_param = $r->ParamDelete($id_param);
	header("Location: reviews_config.php");
}

if ($from=="keyword")
{
	$action 		= $fh->ActionGet($post);
	$id_keyword		= $post['id_keyword'];
	$role 			= $fh->String2Number($post['role']);
	$module_admin = $ah->CheckModule();
	$bb = new Books();
	if($module_admin && $id_keyword>0)
	{
		if ($action=="delete")
			$bb->KeywordDelete($id_keyword);
		if ($action=="insert")
			$bb->KeywordAdd($id_keyword,$role);
		if ($action=="update")
			$bb->KeywordUpdate($id_keyword,$role);
	}
	header("Location: keywords.php");
}

if ($from=="config_update")
{
	$path 		= $post['path'];
	$books_id_topic	= $post['books_id_topic'];
	$books_home_type	= $post['books_home_type'];
	$books_reviews	= $post['books_reviews'];
	$show_reviews	= $post['show_reviews'];
	$search_books	= $fh->Checkbox2bool($post['search_books']);
	$bb = new Books();
	$bb->ConfigurationUpdate($path,$books_id_topic,$books_home_type,$books_reviews,$search_books,$show_reviews);
	header("Location: index.php");
}

if ($from=="ebook")
{
	include_once(SERVER_ROOT."/../modules/ebook.php");
	$eb = new EBook();
	$id_ebook 	= $post['id_ebook'];
	$id_book 	= $post['id_book'];
	$id_type 	= $post['id_type'];
	$rights 	= $post['rights'];
	$id_style	= $post['id_style'];
	$approved 	= $fh->Checkbox2bool($post['approved']);
	$action 	= $fh->ActionGet($post);
	if($action=="store")
	{
		$id_ebook = $eb->EbookStore($id_ebook,$id_book,$id_type,$rights,$id_style,$approved);
		$url = "ebook_params.php?id=$id_ebook";
	}
	if($action=="publish")
	{
		$eb->EbookPublish($id_ebook);
		$ah->MessageSet("publish_done");
		$url = "ebook.php?id=$id_ebook";
	}
	header("Location: $url");
}

if ($from=="ebook_params")
{
	include_once(SERVER_ROOT."/../modules/ebook.php");
	$eb = new EBook();
	$id_ebook	= $post['id_ebook'];
	$params	= $fh->SerializeParams();
	$action 	= $fh->ActionGet($post);
	if ($action=="store")
	{
		$eb->EbookParamsStore($id_ebook,$params);
	}
	header("Location: ebook.php?id=$id_ebook");
}
?>
