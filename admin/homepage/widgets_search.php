<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

if(isset($get['action_search']))
{
	$title[] = array('Widgets Search','widgets_search.php');
	$title[] = array('results','');
}
else 
	$title[] = array('Widgets Search','');

echo $hh->ShowTitle($title);

$input_right = $module_right;	
echo $hh->input_form("get","widgets_search.php");
echo $hh->input_table_open();
echo $hh->input_hidden("from","search");
echo $hh->input_text("name","name",stripslashes($get['name']),20,0,$input_right);
echo $hh->input_array("type","type",isset($get['type'])?$get['type']:count($wi->types),array_merge($wi->types,array("All")),$input_right);

$categories = $wi->Categories();
if(count($categories)>0)
	echo $hh->input_row("category","id_category",$get['id_category'],$categories,"all_option",0,$input_right);
echo $hh->input_array("status","status",isset($get['status'])?$get['status']:count($wi->Statuses(true)),array_merge($wi->Statuses(true),array("All")),$input_right); 
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if(isset($get['action_search']))
{
	$params = array(	'name' => $get['name'],
			'type' => (int)$get['type'],
			'id_category' => (int)$get['id_category'],
			'status' => (int)$get['status']
			);
	$rows = array();
	$num = $wi->WidgetsSearch($rows, $params);

	$table_headers = array('date', 'title','widget type','status', 'owner');
	$table_content = array('{FormatDate($row[insert_date_ts])}', '{LinkTitle("widget.php?id=$row[id_widget]",$row[title])}', '{WidgetsTypeLookup($row[widget_type])}', '{WidgetsStatusLookup($row[status])}', '{WidgetOwnerLookup($row[id_p])}');
	
	echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

