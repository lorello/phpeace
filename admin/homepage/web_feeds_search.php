<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$wf = new WebFeeds();

if(isset($get['action_search']))
{
	$title[] = array('Web feeds search','web_feeds_search.php');
	$title[] = array('results','');
}
else 
	$title[] = array('Web feeds search','');

echo $hh->ShowTitle($title);

$input_right = $module_right;	
echo $hh->input_form("get","web_feeds_search.php");
echo $hh->input_table_open();
echo $hh->input_hidden("from","search");
echo $hh->input_text("name","name",stripslashes($get['name']),20,0,$input_right);
echo $hh->input_text("content","content",stripslashes($get['content']),20,0,$input_right);
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if(isset($get['action_search']))
{
	$params = array(	'name' => $get['name'],
			'content' => $get['content']
			);
	$rows = array();
	$num = $wf->WebFeedsSearch($rows, $params);

	$table_headers = array('title', 'url', 'feed items', 'last updated');
	$table_content = array('{LinkTitle("web_feed.php?id=$row[id_web_feed]",$row[title])}', '$row[feed_url]', '$row[counter]', '{FormatDateTime($row[last_updated_ts])}');
	
	echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

