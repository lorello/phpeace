<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$wf = new WebFeeds();


if ($module_right)
	$input_right = 1;

$title[] = array("filtered feed items",'');

echo $hh->ShowTitle($title);
$from_date = $get['from_date'];
$to_date = $get['to_date'];

if ($from_date == '')
    $from_date = date("Y-m-d");
if ($to_date == '')
{
    $to_date = $from_date;
    $actual_to_date = $from_date;
}
else
{
    $next_day  = mktime(0, 0, 0, date(substr($to_date,5,2))  , date(substr($to_date, 8,2))+1, date(substr($to_date, 0,4)));
    $actual_to_date = date("Y-m-d", $next_day);
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","filtered_web_feed_items");
echo $hh->input_text("From date (yyyy-mm-dd)","from_date",$from_date,10,0,$input_right);
echo $hh->input_text("To date (yyyy-mm-dd)","to_date",$to_date,10,0,$input_right);
$actions = array();
$actions[] = array('action'=>"list",'label'=>"list",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
$hh->input_form_close(); 

$num = $wf->FilteredWebFeedItems($from_date, $actual_to_date, $rows);

$table_headers = array('web feed', 'title','published date', 'active','last updated');
$table_content = array('$row[web_feed_title]', '{LinkTitle("web_feed_item.php?id=$row[id_web_feed_item]&id_web_feed=$row[id_web_feed]&filter=1",$row[title])}', '$row[published_date]', '{WebFeedItemsStatusLookup($row[status])}','$row[last_updated]');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);


include_once(SERVER_ROOT."/include/footer.php");
?>
