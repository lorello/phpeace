<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$title[] = array("tabs");

echo $hh->ShowTitle($title);

$id_p = 0;
$num = $wi->WidgetsTabsAll($rows, $id_p);

$table_headers = array('pos','tab_name','widgets');
$table_content = array('<div class=right>$row[id_tab]</div>','{LinkTitle("widgets_tab.php?id=$row[id_tab]",$row[tab_name],0,"","'.$module_admin.'")}', 
'{LinkTitle("widgets_tabs_position.php?id_tab=$row[id_tab]",$row[num_widgets],0,"",1,"right")}');
                          
echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_admin)
	echo "<p><a href=\"widgets_tab.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

