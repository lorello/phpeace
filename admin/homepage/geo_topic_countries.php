<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $get['id_topic'];

$input_right = $module_admin;

$t = new Topic($id_topic);

$tabs[] = array("Redirection Rules",'geo_countries.php');
$title[] = array($t->name,'');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/geo.php");
$geo = new Geo();
$countries = $geo->TopicCountries();

$current_countries = array();
$available_countries = array();

foreach($countries as $country)
{
	$id_country = $country['id_country'];
	if($country['id_topic']=="")
	{
		$available_countries[$id_country] = $country['name'];
	}
	if($country['id_topic']==$id_topic)
	{
		$available_countries[$id_country] = $country['name'];
		$current_countries[$id_country] = 1;
	}
}

$input_right = $module_admin;

echo $hh->input_form_open();
echo $hh->input_hidden("from","geo_topic_countries");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
echo $hh->input_list_array("countries","countries",$current_countries,$available_countries,$input_right);

$actions = array();
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

