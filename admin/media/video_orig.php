<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();
$upload_progress = $conf->Get("upload_progress");
if($upload_progress)
{
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$progress_key = $v->Uid();
}

$vi = new Video();

$id_video = $get['id'];

$row = $vi->VideoGet($id_video);
$title[] = array($trm9->Translate("videos"),'videos.php');
$title[] = array($row['title'],'video.php?id='.$id_video);
$title[] = array($trm9->Translate("video_orig"),'');

if($vi->AdminRight($ah->current_user_id,$id_video) || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>

<script type="text/javascript">
<?php if($upload_progress) { ?>
var progress_key = '<?=$progress_key?>';
<?php } ?>

$(document).ready(function() {
<?php if($upload_progress) { ?>
	$("#uploadprogressbar").progressBar();
	$("#uploadprogressbar").toggle();
	$("form").submit(function() {
		var fileinput = $(":file[value!='']");
		if(fileinput.length > 0) {
			$(".input-submit:first").css("background-color","#999999");
			$(".input-submit:gt(0)").toggle();
			$('#uploadprogressbar').progressBar({ barImage: '/js/jquery/progress-images/progressbg_orange.gif'});
			$("#uploadprogressbar").fadeIn();
			setTimeout("showUpload()", 750);
		}
	});
<?php } ?>
});
</script>

<?php

$tabs = array();
$tabs[] = array($trm9->Translate("video_enc"),'video.php?id='.$id_video);
$tabs[] = array("details",'video_details.php?id='.$id_video);
$tabs[] = array("image_associated",'video_thumbs.php?id='.$id_video);
$tabs[] = array($trm9->Translate("video_orig"),'');
if($module_admin)
	$tabs[] = array('history','history.php?id_type='.$ah->r->types['video'].'&id='.$id_video);
echo $hh->Tabs($tabs);

$fm = new FileManager();

$queued = $vi->IsInEncodeQueue($id_video);

$filename = $vi->irl->PathAbs("video_orig",array('size'=>-1,'id'=>$id_video,'format'=>$row['format']));
if($fm->Exists($filename))
{
	$fileinfo_orig = $fm->FileInfo($filename);
	echo $hh->tr->Translate("format") . " : <strong>" . strtoupper($fileinfo_orig['format']) . "</strong><br>";
	echo $trm9->Translate("size") . " : <strong>" . $fileinfo_orig['kb'] . " Kb</strong><br>";
	echo "<p><em>" . nl2br($row['original']) . "</em></p>";
	
	$file_enc = $vi->irl->PathAbs("video_enc",array('id'=>$id_video));
	$file_enc_exists = $fm->Exists($file_enc);
}

$maxfilesize = $vi->video_max_size;
echo $hh->input_form("post","actions.php",true);
if($upload_progress)
{
	echo "<script type=\"text/javascript\" src=\"/js/jquery/jquery-progressbar.min.js\"></script>\n";
	echo "<input id=\"progress_key\" name=\"UPLOAD_IDENTIFIER\" type=\"hidden\" value=\"$progress_key\" />";
}
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_video",$id_video);
echo $hh->input_hidden("from","video_orig");
echo $hh->input_table_open();
echo $hh->input_separator("change");
echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
echo $hh->input_upload("substitute_with","vid",50,$input_right);
echo $hh->input_checkbox($trm9->Translate("encode"),"encode",1,0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right,'progressbar'=>$upload_progress);
$actions[] = array('action'=>"queue",'label'=>$trm9->Translate("queue_add"),'right'=>$input_right && $id_video>0 && !$queued);
$actions[] = array('action'=>"scan",'label'=>$trm9->Translate("scan"),'right'=>$input_right && $id_video>0 && !$queued);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_video>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

