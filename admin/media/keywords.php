<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");
include_once(SERVER_ROOT."/../classes/audio.php");

$vi = new Video();
$au = new Audio();

$title[] = array('configuration','config.php');
$title[] = array('keywords_internal','');
echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

echo "<h3>Video</h3>";
$ikeywords = $vi->KeywordsInternal();
echo "<ul>";
foreach($ikeywords as $ikeyword)
{
	echo "<li>" . $hh->Wrap($ikeyword['keyword'],"<a href=\"video_keyword.php?id={$ikeyword['id_keyword']}\">","</a>",$input_right);
	if($ikeyword['description']!="")
		echo "<div class=\"notes\">{$ikeyword['description']}</div>";
	echo "</li>";
}
if($input_right)
	echo "<li><a href=\"video_keyword.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>";

echo "<h3>Audio</h3>";
$ikeywords = $au->KeywordsInternal();
echo "<ul>";
foreach($ikeywords as $ikeyword)
{
	echo "<li>" . $hh->Wrap($ikeyword['keyword'],"<a href=\"audio_keyword.php?id={$ikeyword['id_keyword']}\">","</a>",$input_right);
	if($ikeyword['description']!="")
		echo "<div class=\"notes\">{$ikeyword['description']}</div>";
	echo "</li>";
}
if($input_right)
	echo "<li><a href=\"audio_keyword.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>

