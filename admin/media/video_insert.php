<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();
$upload_progress = $conf->Get("upload_progress");
if($upload_progress)
{
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$progress_key = $v->Uid();
}

$vi = new Video();

$title[] = array($trm9->Translate("videos"),'videos.php');
$title[] = array("add_new",'');

if($module_right || $module_admin)
	$input_right = 1;

if($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

?>
<script type="text/javascript">
<?php if($upload_progress) { ?>
var progress_key = '<?=$progress_key?>';
<?php } ?>

$(document).ready(function() {
$("#form1").validate({
		rules: {
			title: "required",
			"vid[]": "required"
		}
	});
<?php if($upload_progress) { ?>
	$("#uploadprogressbar").progressBar();
	$("#uploadprogressbar").toggle();
	$("form").submit(function() {
		var fileinput = $(":file[value!='']");
		if(fileinput.length > 0) {
			$(".input-submit:first").css("background-color","#999999");
			$(".input-submit:gt(0)").toggle();
			$('#uploadprogressbar').progressBar({ barImage: '/js/jquery/progress-images/progressbg_orange.gif'});
			$("#uploadprogressbar").fadeIn();
			setTimeout("showUpload()", 750);
		}
	});
<?php } ?>
});
</script>
<?php

$fm = new FileManager();
$maxfilesize = $vi->video_max_size;
echo $hh->input_form("post","actions.php",true);
if($upload_progress)
{
	echo "<script type=\"text/javascript\" src=\"/js/jquery/jquery-progressbar.min.js\"></script>\n";
	echo "<input id=\"progress_key\" name=\"UPLOAD_IDENTIFIER\" type=\"hidden\" value=\"$progress_key\" />";
}
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("from","video_insert");
echo $hh->input_table_open("video-insert");
echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
echo $hh->input_note($trm9->Translate("timeout_warning"));
echo $hh->input_upload("choose_file","vid",50,$input_right);

echo $hh->input_separator("details");
echo $hh->input_date("insert_date","insert_date","",$input_right);
echo $hh->input_text("title","title","",60,0,$input_right);
echo $hh->input_textarea("description","description","",60,5,"",$input_right);
echo $hh->input_text("author","author","",40,0,$input_right);
echo $hh->input_textarea("source","source","",60,3,"",$input_right);
echo $hh->input_array("language","id_language",$hh->tr->id_language,$hh->tr->Translate("languages"),$input_right);
echo $hh->input_text("link","link","",50,0,$input_right);
echo $hh->input_checkbox($trm9->Translate("auto_start"),"auto_start",0,0,$input_right);

if ($hh->ini->Get("licences"))
	echo $hh->input_array("licence","id_licence",$hh->ini->Get("id_licence"),$hh->tr->Translate("licences"),$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords(0,$o->types['video'],$keywords,$input_right);

echo $hh->input_separator("administration");
if($input_super_right)
	echo $hh->input_note($trm9->TranslateParams("encode_warning",array($vi->video_resize_width,$vi->video_resize_height)));
echo $hh->input_checkbox($trm9->Translate("encode"),"encode",1,0,$input_super_right);
echo $hh->input_checkbox("approved","approved",0,0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right,'progressbar'=>$upload_progress);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

