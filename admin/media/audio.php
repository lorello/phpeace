<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/audio.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();

$au = new Audio();

$id_audio = $get['id'];

$row = $au->AudioGet($id_audio);
$title[] = array($trm9->Translate("audios"),'audios.php');
$title[] = array($row['title'],'');
$id_licence = $row['id_licence'];
if($au->AdminRight($ah->current_user_id,$id_audio) || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm9->Translate("audio_enc"),'');
$tabs[] = array("details",'audio_details.php?id='.$id_audio);
$tabs[] = array($trm9->Translate("audio_orig"),'audio_orig.php?id='.$id_audio);
if($module_admin)
	$tabs[] = array('history','history.php?id_type='.$ah->r->types['audio'].'&id='.$id_audio);
echo $hh->Tabs($tabs);

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","audio_enc");
echo $hh->input_table_open();
$queued = $au->IsInEncodeQueue($id_audio);

if($row['encoded'])
{
	if($row['approved'])
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$url = $irl->PublicUrlGlobal("media",array('subtype'=>"audio",'hash'=>$row['hash']));
		echo $hh->input_text("link","link","<a href=\"$url\">$url</a>",40,0,0);	
	}
	else 
		echo $hh->input_note($trm9->Translate("waiting_approval"),true,"audio_details.php?id=$id_audio");

	$fm = new FileManager();
	$filename = $au->irl->PathAbs("audio_orig",array('id'=>$id_audio,'format'=>$row['format']));
	if($fm->Exists($filename))
	{
		$file_enc = $au->irl->PathAbs("audio_enc",array('id'=>$id_audio));
		$file_enc_exists = $fm->Exists($file_enc);
		if($file_enc_exists)
		{
			echo $hh->show_audio($id_audio,$row['download']);
		}
	}
}
if($queued)
	echo $hh->input_note($trm9->Translate("waiting_enc"),true);
echo $hh->input_table_close() . $hh->input_form_close();

if($row['encoded'])
{
	if($file_enc_exists)
	{
		echo "<p>" . $trm9->Translate("views") . " : <strong>{$row['views']}</strong></p>";
		echo "<p>" . $trm9->Translate("size") . " : <strong>" . floor($row['bytes_enc']/1024) . " Kb</strong></p>";
		echo "<p>" . $hh->tr->Translate("length") . " : <strong>" . $hhf->FormatLength($row['length'],false) . "</strong></p>";
		echo "<p>" . $trm9->Translate("encoding_notes") . ":<br><em>" . nl2br($row['encoded_info']) . "</em></p>";
	}

	if($row['approved'])
	{
		echo $hh->input_code("audio_label",$trm9->Translate("audio_label"),"[[Aud$id_audio]]",14);
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
