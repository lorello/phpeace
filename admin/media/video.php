<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();

$vi = new Video();

$id_video = $get['id'];

$row = $vi->VideoGet($id_video);
$title[] = array($trm9->Translate("videos"),'videos.php');
$title[] = array($row['title'],'');
$id_licence = $row['id_licence'];
if($vi->AdminRight($ah->current_user_id,$id_video) || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm9->Translate("video_enc"),'');
$tabs[] = array("details",'video_details.php?id='.$id_video);
$tabs[] = array("image_associated",'video_thumbs.php?id='.$id_video);
$tabs[] = array($trm9->Translate("video_orig"),'video_orig.php?id='.$id_video);
if($module_admin)
	$tabs[] = array('history','history.php?id_type='.$ah->r->types['video'].'&id='.$id_video);
echo $hh->Tabs($tabs);

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","video_enc");
echo $hh->input_table_open();
$queued = $vi->IsInEncodeQueue($id_video);

if($row['encoded'])
{
	if($row['approved'])
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$url = $irl->PublicUrlGlobal("media",array('subtype'=>"video",'hash'=>$row['hash']));
		echo $hh->input_text("link","link","<a href=\"$url\">$url</a>",40,0,0);	
	}
	else 
		echo $hh->input_note($trm9->Translate("waiting_approval"),true,"video_details.php?id=$id_video");

	$fm = new FileManager();
	$filename = $vi->irl->PathAbs("video_orig",array('id'=>$id_video,'format'=>$row['format']));
	if($fm->Exists($filename))
	{
		$file_enc = $vi->irl->PathAbs("video_enc",array('id'=>$id_video));
		$file_enc_exists = $fm->Exists($file_enc);
		if($file_enc_exists)
		{
			$watermark = ($fm->Exists("uploads/custom/watermark.png"))? "/images/upload.php?src=custom/watermark.png":"";
			echo $hh->show_video($id_video,$vi->video_resize_width,$vi->video_resize_height,$row['image_seq'],$watermark);
		}
	}
}
if($queued)
	echo $hh->input_note($trm9->Translate("waiting_enc"),true);
echo $hh->input_table_close() . $hh->input_form_close();

if($row['encoded'])
{
	if($file_enc_exists)
	{
		echo "<p>" . $trm9->Translate("views") . " : <strong>{$row['views']}</strong></p>";
		echo "<p>" . $trm9->Translate("size") . " : <strong>" . floor($row['bytes_enc']/1024) . " Kb</strong></p";
		echo "<p>" . $hh->tr->Translate("length") . " : <strong>" . $hhf->FormatLength($row['length'],false) . "</strong></p>";
		echo "<p>" . $trm9->Translate("encoding_notes") . ":<br><em>" . nl2br($row['encoded_info']) . "</em></p>";
	}

	if($row['approved'])
	{
		echo $hh->input_code("video_label",$trm9->Translate("video_label"),"[[Vid$id_video]]",14);
		echo $hh->input_code("video_label_thumb",$trm9->Translate("video_label_thumb"),"[[Vth$id_video]]",14);
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
