<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/layout.php");
$fh = new FormHelper(false,0,false);
$l = new Layout();
$get = $fh->HttpGet();

if(isset($get['id']))
{
	$jcampaign = array(); 
	$id_campaign = (int)$get['id'];
	if($id_campaign>0) 
	{
		include_once(SERVER_ROOT."/../classes/campaign.php");
		$c = new Campaign($id_campaign);
		$campaign = $c->CampaignGet();
		if(isset($campaign['id_topic_campaign']))
		{
			$jcampaign = $campaign;
			foreach ( $campaign as $key => $value )
			{
				if ( is_int($key) )
					unset($jcampaign[$key]);
			}
			$jcampaign['tot_persons'] = $c->PersonsApproveCounter( 1 );
			if($campaign['orgs_sign'])
			{
				$rows = array();
				$jcampaign['tot_orgs'] = $c->OrgsApprove($rows, 1);
			}
		}
	}
	echo $l->JsonOutput($jcampaign);
}
?>
