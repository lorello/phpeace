<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/
if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/config.php");
$conf = new Configuration();

$lang = isset($_GET['lang'])? $_GET['lang'] : "en";

header("Content-type: text/javascript; charset=UTF-8");
?>
var preProssesInnerHtml;
tinyMCE.init({
	mode: "textareas",
	theme: "modern",
	language: "<?=$lang;?>",
	plugins: "table,code,lists,link,media,paste,searchreplace,anchor,charmap,fullscreen",
	content_css : "/include/css/article.css",
	editor_deselector: "mceNoEditor",
	button_tile_map: true,
	verify_html: false,
	statusbar: false,
	menubar: false,
	toolbar1: 'undo redo | bold italic underline strikethrough formatselect removeformat | cut copy paste pastetext | alignleft aligncenter alignright alignjustify | bullist numlist',
	toolbar2: 'link unlink anchor | media | table | searchreplace | charmap | code | fullscreen',
	block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3;Blockquote=blockquote',
	extended_valid_elements: "img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|phpeace_id],iframe[src|width|height|name|align|frameborder|scrolling]",
	setup: function (ed) {
		ed.on('keydown', function() {
			lastActivity = new Date().getTime();
		});
<?php
$id_article = isset($_GET['id'])? (int)$_GET['id'] : 0;
if($id_article>0)
{
    include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id_article);
	$images = $a->ImageGetAll();
	if(count($images)>0)
	{
		$convert_format = $conf->Get("convert_format");
		echo "  ed.on('BeforeSetContent', function (e) { \n";
		foreach($images as $image)
		{
			$size = $image['size']=="-1"? "orig":$image['size'];
			$format = $image['size']=="-1"? $image['format'] : $convert_format;
			$orig_format = $image['format'];
			switch($image['align'])
			{
				case "0":
					$align = "right";
				break;
				case "1":
					$align = "left";
				break;
				case "2":
					$align = "standalone";
				break;
			}
			echo "    e.content = e.content.replace(/\[\[Img({$image['id_image']})\]\]/gi,'\<img phpeace_id=\"$1\" src=\"/images/upload\.php\?src=images\/{$size}\/$1\.{$format}&format={$orig_format}\" width=\"{$image['width']}\" height=\"{$image['height']}\" class=\"{$align}\" \/\>');\n";
		}
		echo "  });\n";
		echo "  ed.on('PostProcess', function (e) { \n";
		echo "    e.content = e.content.replace(/<img(.*?)phpeace_id=\\\"([0-9]*?)\\\"(.*?)>/gi,'\[\[Img$2\]\]');\n";
		echo "  });\n";
	}
}
?>
	}
});
