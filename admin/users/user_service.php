<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../classes/services.php");

$se = new Services();
$id_user_service = (int)$get['id_user_service'];
$id = (int)$get['id'];

$row = $u->ServiceUserGet($id_user_service);

$u = new User;
$u->id = $id;
$user = $u->UserGet();

$active = $user['active'];
if ($active!=1)
	$ah->MessageSet("user_no_active_warn");

$title[] = array('users_list','users.php?p='.$current_page);
$title[] = array($user['name'],'user.php?id='.$id);
$title[] = array('services','user_services.php?id='.$id);
$title[] = array($row['service_name'],'');

echo $hh->ShowTitle($title);

if ($ah->current_user_id==$id || $module_admin)
	$input_right = 1;
		
if ($id>0)
{
	$tabs = array();
	$tabs[] = array('user_data','/users/user.php?id='.$id);
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/users/user_page.php?id='.$id);
	$tabs[] = array('user_admin','/users/user_admin.php?id='.$id);
	$tabs[] = array('image_associated','/users/user_image.php?id='.$id);
	$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
	$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
	$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id);
	$tabs[] = array('services','/users/user_services.php?id='.$id);
	$tabs[] = array('user_stats',($id>0)?'/users/user_stats.php?id='.$id:'');
	echo $hh->Tabs($tabs);
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","user_service");
echo $hh->input_hidden("id_user_service",$id_user_service);
echo $hh->input_hidden("id_user",$id);
echo $hh->input_table_open();
$services = array();
$num_services = $u->Services($services,false);
echo $hh->input_note($hh->tr->TranslateParams("service_email",array($user['email'])));
if($id_user_service>0)
{
	echo $hh->input_text("service","service_name",$row['service_name'],50,0,0);
}
else 
{
	echo $hh->input_row("service","service_name","",$se->UserServicesAvailable($id),"choose_option",0,$input_right);
}
echo $hh->input_checkbox("SMS (" . ($user['mobile']!=""? $user['mobile']:$hh->tr->Translate("mobile_missing")) . ")","mobile",$row['mobile'],0,$input_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_user_service>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


