<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");


$id = $_GET['id'];
$u = new User;
$u->id = $id;
$row = $u->UserGet();

$title[] = array('users_list','users.php');
$title[] = array($row['name'],'user.php?id='.$id);
$title[] = array('email_change','');
echo $hh->ShowTitle($title);

$input_right = 0;
if ($conf->Get("user_auth")=="internal" && ($ah->current_user_id==$id || $module_admin))
	$input_right = 1;
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			password: "required",
			email: {
				required: true,
				email: true
			}
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","user");
echo $hh->input_hidden("id_user",$id);
echo $hh->input_table_open();

echo $hh->input_note("email_change_note2");
echo $hh->input_text("password","password","",20,0,$input_right);
echo $hh->input_text("email_old","email_old",$row['email'],20,0,0);
echo $hh->input_text("email_new","email","",30,0,$input_right);

$actions = array();
$actions[] = array('action'=>"change_email",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


