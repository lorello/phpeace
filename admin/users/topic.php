<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/topic.php");


$id = $_GET['id'];
$t = new Topic($id);

$title[] = array('topics_mng','topics.php');
$title[] = array($t->name,'');
echo $hh->ShowTitle($title);

$num = $t->TopicUsers( $row );

$table_headers = array('user','administrator','contact_main');
$table_content = array('{LinkTitle("user.php?id=$row[id_user]",$row[name])}','{Bool2YN($row[is_admin])}','{Bool2YN($row[is_contact])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin || $t->AmIAdmin())
{
  echo "<p><a href=\"/topics/topic_add.php?id=$id&g=user\">" . $hh->tr->Translate("topic_add_user") . "</a></p>\n";
  echo "<p><a href=\"/topics/topic_add.php?id=$id&g=admin\">" . $hh->tr->Translate("topic_add_administrator") . "</a></p>\n";
  echo "<p><a href=\"/topics/topic_add.php?id=$id&g=contact\">" . $hh->tr->Translate("topic_add_contact") . "</a></p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
