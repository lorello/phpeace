<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$id = $_GET['id'];

$u = new User;
$u->id = $id;
$row = $u->UserGet();
$active = $row['active'];
if ($active!=1)
{
	$ah->MessageSet("user_no_active_warn");
}
$title[] = array('users_list','users.php?p='.$current_page);
$title[] = array($row['name'],'user.php?id='.$id);
$title[] = array('responsibilities','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/users/user.php?id='.$id);
if($hh->ini->Get("user_show"))
	$tabs[] = array('user_link','/users/user_page.php?id='.$id);
$tabs[] = array('user_admin','/users/user_admin.php?id='.$id);
$tabs[] = array('image_associated','/users/user_image.php?id='.$id);
$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
$tabs[] = array('responsibilities','');
$tabs[] = array('services','/users/user_services.php?id='.$id);
$tabs[] = array('user_stats','/users/user_stats.php?id='.$id);
echo $hh->Tabs($tabs);

echo "<div class=\"box\">";
echo "<h2>" . $hh->tr->Translate("topics") ."</h2>";

$topics1 = $u->TopicsContactOnly();
if(count($topics1)>0)
{
	echo "<h3>" . $hh->tr->Translate("contact_main") . "</h3>";
	echo "<ul>";
	foreach($topics1 as $topic1)
		echo "<li><a href=\"/topics/ops.php?id={$topic1['id_topic']}\">{$topic1['name']}</a>";
	echo "</ul>";
}

$topics2 = $u->TopicsAdminOnly();
if(count($topics2)>0)
{
	echo "<h3>" . $hh->tr->Translate("administrator") . "</h3>";
	echo "<ul>";
	foreach($topics2 as $topic2)
		echo "<li><a href=\"/topics/ops.php?id={$topic2['id_topic']}\">{$topic2['name']}</a>";
	echo "</ul>";
}

$topics3 = $u->TopicsUserOnly();
if(count($topics3)>0)
{
	echo "<h3>" . $hh->tr->Translate("collaborator") . "</h3>";
	echo "<ul>";
	foreach($topics3 as $topic3)
		echo "<li><a href=\"/topics/ops.php?id={$topic3['id_topic']}\">{$topic3['name']}</a>";
	echo "</ul>";
}

echo "</div>";

echo "<div class=\"box\">";
echo "<h2>" . $hh->tr->Translate("modules") ."</h2>";
$modules1 = $u->ModulesUser(true);
if(count($modules1)>0)
{
	echo "<h3>" . $hh->tr->Translate("administrator") . "</h3>";
	echo "<ul>";
	foreach($modules1 as $module1)
		echo "<li><a href=\"/{$module1['path']}/index.php\">{$tr_modules[$module1['id_module']]}</a>";
	echo "</ul>";
}

$modules2 = $u->ModulesUser();
if(count($modules2)>0)
{
	echo "<h3>" . $hh->tr->Translate("user") . "</h3>";
	echo "<ul>";
	foreach($modules2 as $module2)
		echo "<li><a href=\"/{$module2['path']}/index.php\">{$tr_modules[$module2['id_module']]}</a>";
	echo "</ul>";
}
echo "</div>";

echo "<div class=\"box\">";
echo "<h2>" . $hh->tr->Translate("other") ."</h2>";

$galleries = $u->Galleries();
if(count($galleries)>0)
{
	echo "<h3>" . $tr_modules[6] . "</h3>";
	echo "<ul>";
	foreach($galleries as $gallery)
		echo "<li><a href=\"/galleries/ops.php?id={$gallery['id_gallery']}\">{$gallery['title']}</a>";
	echo "</ul>";
}

$lists = $u->Lists();
if(count($lists)>0)
{
	echo "<h3>" . $tr_modules[10] . "</h3>";
	echo "<ul>";
	foreach($lists as $list)
		echo "<li><a href=\"/lists/ops.php?id={$list['id_list']}\">{$list['email']}</a>";
	echo "</ul>";
}

$banners = $u->BannersGroups();
if(count($banners)>0)
{
	echo "<h3>" . $tr_modules[23] . "</h3>";
	echo "<ul>";
	foreach($banners as $banner)
		echo "<li><a href=\"/banners/banners.php?id={$banner['id_group']}\">{$banner['name']}</a>";
	echo "</ul>";
}

echo "</div>";

include_once(SERVER_ROOT."/include/footer.php");
?>


