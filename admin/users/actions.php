<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/user.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];
$action3	= $_GET['action3'];
$from2	= $_GET['from2'];
$u = new User();

if ($from=="user")
{
	$id_user 	= $post['id_user'];
	$name	 	= $post['name'];
	$staff_id	 	= $post['staff_id'];
	$department	 	= $post['department'];
	$job_position	 	= $post['job_position'];
	$email		= $post['email'];
	$email_visible 	= (int)$post['email_visible'] - 1;
	$photo_visible 	= $fh->Checkbox2Bool($post['photo_visible']);
	$phone	= $post['phone'];
	$mobile	= $post['mobile'];
	$id_geo	= $post['id_geo'];
	$id_language	= $post['id_language'];
	$password	= $post['password'];
	$signature	= $post['signature'];
	$p 		= $post['p'];
	$action 	= $fh->ActionGet($post);
	if ($action=="update")
	{
		$u->id = $id_user;
		$u->UserUpdate( $name,$staff_id,$email,$email_visible,$department,$job_position,$phone,$mobile,$signature,$id_geo,$photo_visible );
		header("Location: users.php?p=$p");
	}
	if ($action=="reminder")
	{
		$unescaped_email = $fh->HttpPostUnescapedVar("email");
		$ah->PasswordReminder( $unescaped_email );
		header("Location: user.php?id=$id_user");
	}
	if ($action=="change_password")
	{
		$old_password	= $post['password_old'];
		$new_password	= $post['password'];
		$u->id 		= $id_user;
		$u->ChangePassword( $old_password, $new_password, $ah->session->Get('module_admin') );
		header("Location: user.php?id=$id_user");
	}
	if ($action=="change_email")
	{
		$u->id 		= $id_user;
		$u->ChangeEmail( $password, $email );
		header("Location: user.php?id=$id_user");
	}
}

if ($from=="user_page")
{
	$id_user 	= $post['id_user'];
	$user_show	= $fh->Checkbox2Bool($post['user_show']);
	$notes		= $post['notes'];
	$u = new User();
	$u->id = $id_user;
	$u->UserUpdatePage( $user_show,$notes );
	header("Location: user.php?id=$id_user");
}

if ($from=="user_admin")
{
	$id_user 	= $post['id_user'];
	$name	 	= $post['name'];
	$email		= $post['email'];
	$id_language = $post['id_language'];
	$login 		= $post['login'];
	$password	= $post['password'];
	$admin_notes	= $post['admin_notes'];
	$id_group   = (int)$post['id_group'];
	$active 	= $fh->Checkbox2Bool($post['active']);
	$verified 	= $fh->Checkbox2Bool($post['verified']);
	$start_date	= $fh->Strings2Date($post['start_date_d'],$post['start_date_m'],$post['start_date_y']);
	$super_right 	= $u->ModuleAdmin(1);
	$action 	= $fh->ActionGet($post);
	$url = "user.php?id=$id_user";
	if ($action=="insert")
	{
        if (!$u->UserUnique($login) || !$u->EmailUnique($email))
        {
            $fh->HttpPost(true);
            $url = "user_admin.php?id=0";
        }
		else
        {
            $id_user = $u->UserInsert( $name,$email,$login,$password,$active,$admin_notes,$id_language,$start_date,$id_group );
            $url = "user.php?id=$id_user";
        }
	}
	if ($action=="update")
	{
		$u->id = $id_user;
		$u->UserUpdateByAdmin( $login,$active,$admin_notes,$id_language,$super_right,$start_date,$verified,$id_group );
	}
	if ($action=="reminder")
	{
		$u->id = $id_user;
		$row = $u->UserGet();
		$ah->PasswordReminder( $row['email'] );
	}
	if ($action=="email_verify")
	{
		$u->id = $id_user;
		$row = $u->UserGet();
		$u->VerifyEmail();
		$trm28 = new Translator($ah->session->Get("id_language"),28);
		$ah->MessageSet($trm28->TranslateParams("registration_check_sent",array($row['email'])));
		$url = "user.php?id=$id_user";
	}
	
	if ($action=="emulate")
	{
		$module_admin = $ah->CheckModule();
		if($module_admin)
			$u->UserEmulate( $id_user );
		$url = "/gate/index.php";
	}
	header("Location: $url");
}

if ($from2=="user")
{
	if ($action3=="emulate") 
	{
		$id_user        = $_GET['id']; 
		$module_admin = $ah->CheckModule();
		if($module_admin)
			$u->UserEmulate( $id_user );
		header("Location: /gate/index.php");
	}
	if ($action3=="emulationend")
	{
		$u->UserEmulateEnd();
		header("Location: /gate/index.php");
	}
	if ($action3=="addldap")
	{
		$id_language = $ah->session->Get("id_language");
		$login = $fh->SqlQuote($_GET['login']);
		$id_user = $u->UserInsertLdap($login,$id_language);
		header("Location: user_admin.php?id=$id_user");
	}
}

if ($from=="module")
{
	$id_module 	= $post['id_module'];
	$w	 	= $post['w'];
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users;
	if ($action2=="add_user")
	{
		$users = $uu->ModuleUsersAdmins($id_module, "user");
		Modules::ModuleDeleteUsers($id_module);
		foreach($users as $user)
		{
			if ($fh->Checkbox2Bool($post[ $user['id_user'] ]))
			{
				$is_admin = ($user['is_admin']==1)? 1 : 0;
				Modules::ModuleInsertUser( $user['id_user'], $is_admin, $id_module );
			}
		}
		if ($w=="tra")
			header("Location: /translations/translators.php");
		else
			header("Location: module.php?id=$id_module");
	}
	if ($action2=="add_admin")
	{
		$users = $uu->ModuleUsersAdmins($id_module, "admin");
		foreach($users as $user)
		{
			$user_check = $fh->Checkbox2Bool($post[ $user['id_user'] ]);
			$is_admin = ($user['is_admin']==1)? true : false;
			if (!$user_check && $is_admin)
				Modules::ModuleUpdateUser( $user['id_user'], 0, $id_module);
			if ($user_check && !$is_admin)
				Modules::ModuleUpdateUser( $user['id_user'], 1, $id_module);
		}
		header("Location: module.php?id=$id_module");
	}
	if ($action2=="update")
	{
		$id_module 	= $post['id_module'];
		$restricted 	= $fh->Invert($fh->Checkbox2Bool($post['restricted']));
		Modules::ModuleUpdate($id_module,$restricted);
		header("Location: modules.php");
	}
}

if ($from=="user_modules")
{
	$id_user 	= $post['id_user'];
	$restricted 	= $post['restricted'];
	$u->id = $id_user;
	$modules = $u->ModulesRight($restricted==0);
	foreach($modules as $module)
	{
		$user = $fh->Checkbox2Bool($post['moduser' . $module['id_module'] ]);
		$admin = $fh->Checkbox2Bool($post['modadmin' . $module['id_module'] ]);
		if($admin && !$user)
			$user = true;
		$u->ModuleUpdate($module['id_module'],$user,$admin);
	}
	header("Location: user.php?id=$id_user");
}

if ($from=="user_topics")
{
	$id_user 	= $post['id_user'];
	include_once(SERVER_ROOT."/../classes/topic.php");
	include_once(SERVER_ROOT."/../classes/topics.php");
	$topics = new Topics;
	foreach($topics->TopicsUser($id_user) as $topic)
	{
		$user_check = $fh->Checkbox2Bool($post[ $topic['id_topic'] ]);
		if ($user_check && !$topic['id_user']>0)
		{
			$t = new Topic($topic['id_topic']);
			$t->UserInsert( $id_user, 0, 0, 0 );
		}
		if (!$user_check && $topic['id_user']>0)
		{
			$t = new Topic($topic['id_topic']);
			$t->UserDelete( $id_user );
		}
		if ($t->protected=="1")
			$t->PasswordUpdate();
	}
	header("Location: user.php?id=$id_user");
}

if ($from=="user_service")
{
	include_once(SERVER_ROOT."/../classes/services.php");
	$se = new Services();
	$id_user_service	= $fh->Null2Zero($post['id_user_service']);
	$id_user 	= $post['id_user'];
	$mobile = $fh->Checkbox2Bool($post['mobile']);
	$action = $fh->ActionGet($post);
	if($id_user_service>0)
	{
		if($action=="store")
			$se->UserServiceUpdate($id_user_service,$mobile);
		if($action=="delete")
			$se->UserServiceDelete($id_user_service);
	}
	else 
	{
		$service_name 	= $post['service_name'];
		if($action=="store")
			$se->UserServiceInsert($id_user,$service_name,$mobile);		
	}
	header("Location: user_services.php?id=$id_user");
}

if ($from=="user_image")
{
	$id_user 	= $post['id_user'];
	$file		= $fh->UploadedFile("img",true);
	$u->id = $id_user;
	$action = $fh->ActionGet($post);
	if ($action=="store" && $file['ok'])
		$u->ImageUpdate($file);
	if ($action=="delete")
		$u->ImageDelete();
	header("Location: user.php?id=$id_user");
}

if ($from=="config_update")
{
	$path 		= $post['path'];
	$user_show	= $fh->Checkbox2Bool($post['user_show']);
	include_once(SERVER_ROOT."/../classes/users.php");
	$users = new Users;
	$users->ConfigurationUpdate($path,$user_show);
	header("Location: index.php");
}
?>
