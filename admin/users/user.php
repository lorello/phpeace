<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$id = $_GET['id'];
$changepass = $_GET['changepass'];

$title[] = array('users_list','users.php?p='.$current_page);

$u = new User;
$u->id = $id;
$row = $u->UserGetFull();
$action2 = "update";
$active = $row['active'];
if ($active!=1)
{
	$ah->MessageSet("user_no_active_warn");
}
if ($row['bounces']>=$conf->Get("bounces_threshold"))
{
	$ah->MessageSet("bounces_warning",array($conf->Get("bounces_threshold")));
}

$title[] = array($row['name'],'');

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
else
{
	$tabs = array();
	$tabs[] = array('user_data','');
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/users/user_page.php?id='.$id);
	$tabs[] = array('user_admin','/users/user_admin.php?id='.$id);
	$tabs[] = array('image_associated','user_image.php?id='.$id);
	$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
	$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
	$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id);
	$tabs[] = array('services','/users/user_services.php?id='.$id);
	$tabs[] = array('user_stats','/users/user_stats.php?id='.$id);
	echo $hh->Tabs($tabs);
	
	if ($ah->current_user_id==$id || ($ah->session->Get('module_admin')))
		$input_right = 1;
	if ($ah->session->Get('module_admin'))
		$input_super_right = 1;
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});
</script>

<?php
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","user");
	echo $hh->input_hidden("action2",$action2);
	echo $hh->input_hidden("id_user",$id);
	echo $hh->input_hidden("p",$current_page);
	echo $hh->input_table_open();

	$fm = new FileManager;
	$i = new Images();
	$filename = "users/0/$id".".".$i->convert_format;
	$filename_orig = "users/orig/{$id}.jpg";
	if($fm->Exists("uploads/$filename"))
	{
		echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"/users/user_image.php?id=$id\"><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[0]}\"></a></td></tr>";
	}

	echo $hh->input_text("name","name",$row['name'],30,0,$input_right && $conf->Get("user_auth")=="internal");
	
	if ($input_right)
	{
		echo $hh->input_text("email","email",$row['email'],30,0,0,( $conf->Get("user_auth")=="internal" && ($id==$ah->current_user_id || $input_super_right==1))? " (<a href=\"user_email.php?id=".$id."\">" . $hh->tr->Translate("email_change") . "</a>)" : "");
		echo $hh->input_text("staff id","staff_id",$row['staff_id'],20,0,$input_right);
		echo $hh->input_text("department","department",$row['department'],20,0,$input_right);
		echo $hh->input_text("job position","job_position",$row['job_position'],20,0,$input_right);
		echo $hh->input_text("phone","phone",$row['phone'],20,0,$input_right);
		echo $hh->input_text("mobile","mobile",$row['mobile'],20,0,$input_right);
		echo $hh->input_array("show_email","email_visible",($row['email_visible'] + 1),$hh->tr->Translate("show_email_options"),$input_right);
		echo $hh->input_checkbox("photo_visible","photo_visible",$row['photo_visible'],0,$input_right);
	}
	echo $hh->input_geo($row['id_geo'],$input_right);

	echo $hh->input_textarea("signature","signature",$row['signature'],50,4,"",$input_right);
	
	$actions = array();
	$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"reminder",'label'=>"send_password",'right'=>$input_super_right && $id>0 && $active==1 && $conf->Get("user_auth")=="internal");
	echo $hh->input_actions($actions,$input_right);

	echo $hh->input_table_close() . $hh->input_form_close();
}
include_once(SERVER_ROOT."/include/footer.php");
?>


