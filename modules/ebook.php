<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2010 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/file.php");

/**
 * Create and convert into ebooks from internal content or from
 * uploaded files in various formats.
 *
 * @package PhPeace
 * @subpackage Ebook
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class EBook
{
	const EbookTempDirectory = "cache/ebooks";
	const OPF_filename = "content.opf";
	const EbookMaxCoverSize = 1000;
	const EbookImageFormat = "png";
	
	/** 
	 * @var FileManager */
	private $fm;
	
	private $path_temp;
	
	public $image_mimetype;
	
	private $calibre_formats;

	function __construct()
	{
		$this->path_temp = self::EbookTempDirectory;
		$this->fm = new FileManager();
		$this->fm->DirAction($this->path_temp, "check");
		$this->fm->DirAction("uploads/ebooks", "check");
		$this->image_mimetype = $this->fm->ContentType(self::EbookImageFormat);
		$this->calibre_formats = array("cbz","cbr","cbc","chm","epub","fb2","html","lit","lrf","mobi","odf","pdb","pdf","pml","rb","rtf","tcr","txt");
	}

	public function EbookFilename($id_ebook)
	{
		return "uploads/ebooks/{$id_ebook}.epub";
	}
	
	public function EbookGet($id_ebook)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT eb.id_ebook,eb.id_type,eb.id_book,eb.params,eb.rights,eb.id_style,eb.approved,
			b.id_book,b.title,b.summary,b.description,isbn,id_publisher,p_year,price,b.id_language,b.notes,
			b.id_article,b.approved AS book_approved,cover_format,b.author,series,pages,catalog,b.id_category,b.p_month
			FROM ebooks eb 
			INNER JOIN books b ON eb.id_book=b.id_book
			WHERE eb.id_ebook='$id_ebook' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function EbookGetByIdBook($id_book)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT eb.id_ebook,eb.id_type,eb.id_book,eb.params,eb.rights,eb.id_style,eb.approved,
			b.id_book,b.title,b.summary,b.description,isbn,id_publisher,p_year,price,b.id_language,b.notes,
			b.id_article,b.approved,cover_format,b.author,series,pages,catalog,b.id_category,b.p_month
			FROM ebooks eb 
			INNER JOIN books b ON eb.id_book=b.id_book
			WHERE b.id_book='$id_book' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function EbookStore($id_ebook,$id_book,$id_type,$rights,$id_style,$approved)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ebooks" );
		if($id_ebook>0)
		{
			$sqlstr = "UPDATE ebooks SET id_type='$id_type',rights='$rights',id_style='$id_style',approved='$approved' 
				WHERE id_ebook=$id_ebook  ";
		}
		else
		{
			$id_ebook = $db->nextId( "ebooks", "id_ebook" );
			$sqlstr = "INSERT INTO ebooks (id_ebook,id_book,id_type,rights,id_style,params,approved) 
				VALUES ($id_ebook,'$id_book','$id_type','$rights','$id_style','','$approved') ";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);		
		return $id_ebook;
	}
	
	public function EbookTypeParams($id_type)
	{
		$params = array();
		$params[0] = array();
		$params[1] = array('id_doc');
		$params[2] = array('id_subtopic');
		$params[3] = array('id_article');
		return $params[$id_type];
	}
	
	public function EbookParamsStore($id_ebook,$params)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ebooks" );
		$sqlstr = "UPDATE ebooks SET params='$params' WHERE id_ebook='$id_ebook' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function EbookPublish($id_ebook)
	{
		$ebook = $this->EbookGet($id_ebook);
		if($ebook['approved'])
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$params_values = $v->Deserialize($ebook['params']);
			$images = array();
			switch($ebook['id_type'])
			{
				case "1":
					$id_doc = (int)$params_values['id_doc'];
					if($id_doc>0)
					{
						include_once(SERVER_ROOT."/../classes/doc.php");
						include_once(SERVER_ROOT."/../classes/varia.php");
						$d = new Doc($id_doc);
						$doc = $d->DocGet();
						if(in_array($doc['format'],$this->calibre_formats))
						{
							$origfile = $this->fm->path . "/" . "uploads/docs/{$id_doc}.{$doc['format']}";
							$filename = $this->fm->path . "/" . $this->EbookFilename($id_ebook);
							$calibre_options = "--output-profile ipad --no-default-epub-cover";
							if($ebook['title']!="")
							{
								$calibre_options .= " --title {$ebook['title']}";
							}
							if($ebook['author']!="")
							{
								$calibre_options .= " --authors {$ebook['author']}";
							}
							if($ebook['id_publisher']>0)
							{
								include_once(SERVER_ROOT."/../modules/books.php");
								$pu = new Publisher();
								$publisher = $pu->PublisherGet($ebook['id_publisher']);
								$calibre_options .= " --publisher {$publisher['name']}";
							}
							if($ebook['isbn']!="")
							{
								$calibre_options .= " --isbn {$ebook['isbn']}";
							}
							$command = "ebook-convert {$origfile} {$filename} $calibre_options";
							$res = $v->Exec($command);
							if($v->return_status!=0)
							{
								UserError("Conversion failed for ebook $id_ebook",array("origfile"=>"{$id_doc}.{$doc['format']}","output"=>$res));
							}
						}
					}
				break;
				case "2":
					$id_subtopic = (int)$params_values['id_subtopic'];
					if($id_subtopic>0)
					{
						include_once(SERVER_ROOT."/../classes/topic.php");
						include_once(SERVER_ROOT."/../classes/article.php");
						$t = new Topic(0);
						$subtopic = $t->SubtopicGet($id_subtopic);
						$id_topic = $subtopic['id_topic'];
						$xml = $this->SubtopicGenerate($id_ebook,$id_topic,$id_subtopic,$ebook['id_style']);
						// Retrieve images
						$articles = $t->SubtopicArticles($id_subtopic);
						foreach($articles as $article)
						{
							$a = new Article($article['id_article']);
							$aimages = $a->ImageGetAll();
							foreach($aimages as $aimage)
							{
								$images[] = array('id_image'=>$aimage['id_image'],'internal_id'=>"a{$article['id_article']}_i{$aimage['id_image']}",'size'=>$aimage['size'],'width'=>$aimage['width'],'height'=>$aimage['height'],'format'=>$aimage['format']);
							}
						}
						if($xml!="")
						{
							$this->Xml2Ebook($id_ebook,$ebook['id_book'],$xml,$ebook['id_style'],$images);
						}
					}
				break;
				case "3":
					$id_article = (int)$params_values['id_article'];
					if($id_article>0)
					{
						include_once(SERVER_ROOT."/../classes/article.php");
						$xml = $this->ArticleGenerate($id_ebook,$id_article,$ebook['id_style']);
						$a = new Article($id_article);
						$a->ArticleLoad();
						// images
						$aimages = $a->ImageGetAll();
						foreach($aimages as $aimage)
						{
							$images[] = array('id_image'=>$aimage['id_image'],'internal_id'=>"a{$article['id_article']}_i{$aimage['id_image']}",'size'=>$aimage['size'],'width'=>$aimage['width'],'height'=>$aimage['height'],'format'=>$aimage['format']);
						}
						
						if($xml!="")
						{
							$this->Xml2Ebook($id_ebook,$ebook['id_book'],$xml,$ebook['id_style'],$images);
						}
					}
				break;
			}		
		}
	}

	/**
	 * Generate XML representation of article content
	 *
	 * @param integer $id_ebook
	 * @param integer $id_article
	 * @param integer $id_style
	 */
	private function ArticleGenerate($id_ebook,$id_article,$id_style)
	{
		$a = new Article($id_article);
		$a->ArticleLoad();

		$params = array();
		$params['id_module'] = 31;
		$params['module'] = "ebook";
		$params['subtype'] = "article";
		$params['id_article'] = $id_article;
		include_once(SERVER_ROOT."/../classes/layout.php");
		$l = new Layout(false,false,false);
		$l->TranslatorInit(0,$id_style);
		$xml = $l->Output("common",$id_ebook,$a->id_topic,1,$params);
		$docs = $a->DocGetAll();
		/*
		if(count($docs)>0)
		{
			// Convert docs to XML
			// XML merge
		}
		*/
		// Transform to our generic Ebook Schema
		$xml = $l->xh->Transform("ebook",$xml,$id_style,array('action'=>"article"));
		$l->xh->StripDoctype($xml);
		return $xml;
	}
	
	/**
	 * Generate XML representation of subtopic content
	 *
	 * @param integer $id_ebook
	 * @param integer $id_topic
	 * @param integer $id_subtopic
	 * @param integer $id_style
	 */
	private function SubtopicGenerate($id_ebook,$id_topic,$id_subtopic,$id_style)
	{
		$params = array();
		$params['id_module'] = 31;
		$params['module'] = "ebook";
		$params['subtype'] = "subtopic";
		$params['id_subtopic'] = $id_subtopic;
		include_once(SERVER_ROOT."/../classes/layout.php");
		$l = new Layout(false,false,false);
		$l->TranslatorInit(0,$id_style);
		$xml = $l->Output("common",$id_ebook,$id_topic,1,$params);
		// Transform to our generic Ebook Schema
		$xml = $l->xh->Transform("ebook",$xml,$id_style,array('action'=>"subtopic"));
		$l->xh->StripDoctype($xml);
		return $xml;
	}
	
	public function Xml2Ebook($id_ebook,$id_book,$xml,$id_style,$images=array())
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$v = new Varia();
		$xh = new XmlHelper();
		$eb_dir = $id_ebook;
		$this->DirPrepare($eb_dir);
		
		// OPF
		$opf = $xh->Transform("ebook",$xml,$id_style,array('action'=>"opf"));
		$xh->StripDoctype($opf);
		$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/content.opf",$opf);
		
		// XPGT template
		$xpgt = $xh->Transform("ebook",$xml,$id_style,array('action'=>"xpgt"));
		$xh->StripDoctype($xpgt);
		$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/page-template.xpgt",$xpgt);
		
		// TOC NCX
		$tocncx = $xh->Transform("ebook",$xml,$id_style,array('action'=>"tocncx"));
		$xh->StripDoctype($tocncx);
		$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/toc.ncx",$tocncx);
		
		// TITLE PAGE
		$title = $xh->Transform("ebook",$xml,$id_style,array('action'=>"title"));
		$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/title_page.html",$title);

		// CHAPTERS
		$chapters = array();
		$sx = simplexml_load_string($xml);
		foreach ($sx->articles->article as $article)
		{
			$chapters[] = $article['id'];
		} 		

		if(count($chapters)>0)
		{
			foreach($chapters as $chapter)
			{
				$chapter_html = $xh->Transform("ebook",$xml,$id_style,array('action'=>"chapter",'chapter_id'=>$chapter));
				$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/chapter" . $chapter . ".html",$chapter_html);
			}		
		}

		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
			
		// COVER
		include_once(SERVER_ROOT."/../modules/books.php");
		$b = new Book($id_book);
		$cover_filename = "uploads/covers/orig/{$id_book}.{$b->cover_format}";
		if($this->fm->Exists($cover_filename))
		{
			$size = $this->fm->ImageSize($cover_filename);
			$size_to_convert = $size['width']>self::EbookMaxCoverSize? 800 : $size['width'];
			$i->Convert($size_to_convert,$cover_filename,"{$this->path_temp}/{$eb_dir}/OEBPS/images/cover.jpg");	

			$cover = $xh->Transform("ebook",$xml,$id_style,array('action'=>"cover"));
			$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/cover.html",$cover);
		}

		// IMAGES
		if(count($images)>0)
		{
			include_once(SERVER_ROOT."/../classes/irl.php");
			$irl = new IRL();
			$i->convert_format = self::EbookImageFormat;
			foreach($images as $image)
			{
				if(isset($image['id_image']))
				{
					$origfile = $irl->PathAbs("image",array('id'=>$image['id_image'],'size'=>-1,'format'=>$image['format']));
					$i->Convert($image['width'],$origfile,"{$this->path_temp}/{$eb_dir}/OEBPS/images/{$image['internal_id']}." . self::EbookImageFormat);					
				}
			}		
		}
		
		// CSS
		$css_common = $this->fm->TextFileRead("pub/css/0/ebook.css");
		$css_specific = $this->fm->TextFileRead("pub/css/$id_style/ebook.css");
		$css = "$css_common\n\n/* ebook CSS for style $id_style */\n\n$css_specific";
		$this->fm->WritePage("{$this->path_temp}/{$eb_dir}/OEBPS/stylesheet.css",$css);

		// Package
		$zip = new ZipArchive();
		$filename = $this->fm->path . "/{$this->path_temp}/{$eb_dir}.epub";
		$this->fm->Delete($filename,true);
		$mime_filename = $this->fm->path . "/{$this->path_temp}/{$eb_dir}/mimetype";
		$v->Exec("zip -q0Xj $filename $mime_filename");
		$res = $zip->open($filename,ZIPARCHIVE::CREATE);
		if($res === true)
		{
			$this->DirAddToZip($eb_dir,"META-INF",$zip);
			$this->DirAddToZip($eb_dir,"OEBPS",$zip);
			$this->DirAddToZip($eb_dir,"OEBPS/images",$zip);
		}
		else 
		{
			UserError("Could not create zip archive $filename",array("error_code"=>$res));
		}
		if(!$zip->close())
		{
			$error_desc = $zip->getStatusString();
			UserError("Could not create zip archive $filename",array("error_desc"=>$error_desc));
		}
		else 
		{
			$final_filename = $this->EbookFilename($id_ebook);
			$this->fm->Delete($final_filename);
			$this->fm->Rename($filename,$final_filename,true,false,true);
		}
		
		// Remove temp files
		$this->fm->DirRemove($this->path_temp . "/$eb_dir");
	}
	
	/**
	 * Add a directory to an existing Zip archive
	 *
	 * @param string $root_directory
	 * @param string $directory
	 * @param ZipArchive $zip
	 */
	private function DirAddToZip($root_directory,$directory,$zip)
	{
		$files = $this->fm->DirFiles("{$this->path_temp}/$root_directory/$directory");
		foreach($files as $file)
		{
			$file2 = str_replace("{$this->fm->path}/{$this->path_temp}/$root_directory/","",$file);
			$zip->addFile($file,$file2);
		}
	}
	
	/**
	 * Prepare directories and files for ebook package
	 *
	 * @param string $directory
	 */
	private function DirPrepare($directory)
	{
		$this->fm->DirAction("{$this->path_temp}/$directory", "check");
		$this->fm->WritePage("{$this->path_temp}/$directory/mimetype","application/epub+zip");
		$this->fm->DirAction("{$this->path_temp}/$directory/META-INF", "check");
		$container_content = "<?xml version=\"1.0\"?>
<container version=\"1.0\" xmlns=\"urn:oasis:names:tc:opendocument:xmlns:container\">
  <rootfiles>
    <rootfile full-path=\"OEBPS/".self::OPF_filename ."\" media-type=\"application/oebps-package+xml\"/>
  </rootfiles>
</container>";
		$this->fm->WritePage("{$this->path_temp}/$directory/META-INF/container.xml",$container_content);
		$this->fm->DirAction("{$this->path_temp}/$directory/OEBPS", "check");
		$this->fm->DirAction("{$this->path_temp}/$directory/OEBPS/images", "check");
	}

}
?>
