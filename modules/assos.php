<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");
define('ASSO_KEYWORD_TYPE', 5);

class Assos
{
	public $id_topic;
	public $org_path;
	public $geo_location;
	public $orgs_size;
	public $orgs_upload;
	public $orgs_access_protected;
	public $url_after_insert;

	private $orgCounter;
	private $separator;
	private $descriz;

	function __construct()
	{
		$ini = new Ini;
		$conf = new Configuration();
		$this->org_path = $ini->Get('org_path');
		$this->id_topic = $ini->GetModule("orgs","id_topic",0);
		$this->url_after_insert = $ini->GetModule("orgs","url_after_insert","");
		$this->geo_location = 1;
		$this->orgs_size = $conf->Get("orgs_size");
		$this->orgs_upload = $conf->Get("orgs_upload");
		$this->orgs_access_protected = $conf->Get("orgs_access_protected");
		$this->orgCounter = 0;
		$this->separator = "|";
		$this->fields_titles= array ("id","nome","nome2","tipologia","indirizzo","citta","cap","provincia","tel","fax","email","sito","ccp","note","referente","pubblicaz","fonte","data","categorie");
	}

	public function AssoType($id_type)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_ass,nome FROM ass where id_tipo=$id_type";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ConfigurationUpdate($id_topic,$path,$url_after_insert)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->SetPath("org_path",$path);
		$ini->SetModule("orgs","id_topic",$id_topic);
		$ini->SetModule("orgs","url_after_insert",$url_after_insert);
	}

	public function Count()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT COUNT(id_ass) AS count_ass FROM ass WHERE approved=1");
		return $row['count_ass'];
	}

	public function DumpAll()
	{
		$path = "$this->path/docs";
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirAction("$this->path","check");
		$fm->DirAction("$path","check");
		$sqlstr = "SELECT ass.id_ass,ass.nome,ass.nome2,ass_tipo.ass_tipo,ass.indirizzo,ass.citta,ass.cap,prov.prov,ass.tel,ass.fax,ass.email,ass.sito,ass.ccp,ass.note,ass.referente,ass.pubblicaz,ass.fonte,date_format(ass.lastupd,'%d.%m.%Y') from ass,prov,ass_tipo where ass.id_prov=prov.id_prov and ass.id_tipo=ass_tipo.id_ass_tipo";
		$fm->WritePage( "$path/pckdb.txt", $this->DumpCsv($sqlstr) );
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		$dumpXml = $this->dumpXml($sqlstr);
		if ($xh->Check($dumpXml))
			$fm->WritePage( "$path/pckdb.xml", $dumpXml );
		$fm->PostUpdate();
		return $this->orgCounter;
	}

	public function DumpCsv($sqlstr)
	{
		$separator = $this->separator;
		$dump_txt =  "";
		for ($i = 1; $i < count($this->fields_titles); $i++)
		{
			$dump_txt .= $this->fields_titles[$i] . $separator;
		}
		$dump_txt .= "\n";
		$counter = 0;
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);

		foreach($rows as $org)
		{
			$ass = "";
			for ($i = 1; $i < count($this->fields_titles)-1; $i++)
			{
				$ass .= $this->NewlineReplace($org[$i],true) . $separator;
			}
			$keywords = $this->OrgKeywords($org[0]);
			$comma = false;
			foreach($keywords as $keyword)
			{
				if ($comma)
					$ass .= ",";
				$ass .= $keyword['keyword'];
				$comma = true;
			}
			$ass .= "$separator\n";
			$dump_txt .= $ass;
			$counter ++;
		}
		$this->orgCounter = $counter;
		return $dump_txt;
	}

	public function DumpXml($sqlstr)
	{
		$rows = array();
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr);
		$dump_xml = "<dbres data=\"".date("Y-m-d")."\" tot_res=\"" . count($num) . "\">\n";
		foreach($rows as $org)
		{
			$ass="<risorsa id=\"$org[0]\">\n";
			for ($i = 1; $i < count($this->fields_titles)-1; $i++)
			{
				if (strlen($org[$i])>1)
				{
					$ass .= "<".$this->fields_titles[$i].">".htmlspecialchars($this->NewlineReplace($org[$i]))."</".$this->fields_titles[$i].">\n";
				}
			}
			$ass .= "<categorie>\n";
			$keywords = $this->OrgKeywords($org[0]);
			foreach($keywords as $keyword)
			{
				$ass .= "<cat id=\"{$keyword['id_keyword']}\">" . htmlspecialchars($keyword['keyword']) . "</cat>";
			}
			$ass .= "</categorie>\n";
			$ass .= "</risorsa>\n";
			$dump_xml .= $ass;
		}
		$dump_xml .= "</dbres>\n";
		return $dump_xml;
	}

	public function KeywordCount($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id) AS counter
		FROM keywords_use ku
		INNER JOIN ass a ON ku.id=a.id_ass AND a.approved=1
		WHERE ku.id_keyword=$id_keyword AND ku.id_type=4 ";
		$db->query_single($row,$sqlstr);
		return $row['counter'];
	}
	
	public function KeywordUse(&$rows,$id_keyword,$paged=false)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		$types = $r->types;
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.*,ass_tipo.ass_tipo,{$join['name']} AS geo_name,'asso' AS item_type
		FROM keywords_use ku
		INNER JOIN ass a ON ku.id=a.id_ass
		{$join['join']}
		INNER JOIN ass_tipo ON a.id_tipo=ass_tipo.id_ass_tipo
		WHERE ku.id_keyword=$id_keyword AND ku.id_type={$types['org']} 
		ORDER BY a.nome ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function Keywords()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_keyword,keyword,description FROM keywords WHERE id_type=" . ASSO_KEYWORD_TYPE . " ORDER BY keyword";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function KeywordsP( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword,keyword from keywords WHERE id_type=" . ASSO_KEYWORD_TYPE . " ORDER BY keyword";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function KeywordStore($id_keyword,$keyword,$id_parent,$force)
	{
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		if($id_keyword>0)
			$k->KeywordUpdateName($id_keyword,$keyword);
		else
		{
			$exists = $k->KeywordGetId($keyword,ASSO_KEYWORD_TYPE);
			if($exists>0)
			{
				if($force && $id_parent>0)
				{
					include_once(SERVER_ROOT."/../classes/ontology.php");
					$o = new Ontology;
					$o->StatementInsert($id_parent,3,$exists);
					$id_keyword = $exists;
				}
			}
			else
			{
				$id_keyword = $k->KeywordInsert( $keyword,"",ASSO_KEYWORD_TYPE,array() );
				if($id_parent>0)
				{
					include_once(SERVER_ROOT."/../classes/ontology.php");
					$o = new Ontology;
					$o->StatementInsert($id_parent,3,$id_keyword);
				}
			}
		}
		return $id_keyword;
	}
	
	public function KeywordDelete($id_keyword)
	{
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$params = $k->Params($id_keyword,ASSO_KEYWORD_TYPE);
		foreach($params as $param)
			$this->KeywordParamDelete($param['id_keyword_param'],$id_keyword);
		$k->UseDelete($id_keyword,ASSO_KEYWORD_TYPE);
		$k->KeywordDelete($id_keyword);
	}

	public function KeywordParamDelete($id_kparam,$id_keyword)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$orgs = array();
		$this->KeywordUse($orgs,$id_keyword);
		foreach($orgs as $org)
		{
			$asso = new Asso($org['id_ass']);
			$row = $asso->Get(false);
			$kparams = $v->Deserialize($row['kparams']);
			if(is_array($kparams))
			{
				$found = false;
				$key = "kp_" . $id_kparam;
				if(array_key_exists($key,$kparams))
				{
					unset($kparams[$key]);
					$found = true;
				}
				if($found)
				{
					$ser_array = $v->Serialize($kparams);
					$asso->KeywordParamsStore($ser_array);
				}
			}
		}
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$k->ParamDelete($id_kparam);
	}

	public function KeywordParamStore($id_keyword,$id_kparam,$label,$id_type,$params,$public,$index_include,$list_include)
	{
		include_once(SERVER_ROOT."/../classes/keyword.php");
		$k = new Keyword();
		$ktypes = $k->ParamsTypes(true);
		$index = false;
		if($id_kparam>0)
		{
			$param_old = $k->ParamGet($id_kparam);
			if($param_old['index_include']!=$index_include)
				$index = true;
		}
		elseif($index_include)
			$index = true;
		$type = $ktypes[$id_type];
		$id_kparam = $k->ParamStore($id_kparam,ASSO_KEYWORD_TYPE,$id_keyword,$label,$type,$params,$public,$index_include,$list_include);
		if($index)
		{
			$uses = array();
			$num_uses = $this->KeywordUse($uses,$id_keyword);
			if($num_uses>0)
			{
				include_once(SERVER_ROOT."/../classes/search.php");
				$s = new Search();
				foreach($uses as $use)
				{
					$s->IndexQueueAdd(4,$use['id_ass'],0,0,2);
				}
			}
		}
		return $id_kparam;
	}

	public function Latest( &$rows )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_ass,a.nome,a.nome2,a.indirizzo,a.citta,{$join['name']} AS geo_name,a.tel,ass_tipo.ass_tipo,a.approved,
		UNIX_TIMESTAMP(a.insert_date) AS insert_date_ts
		FROM ass a
		{$join['join']}
		INNER JOIN ass_tipo ON a.id_tipo=ass_tipo.id_ass_tipo
		ORDER BY a.insert_date DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function MailjobSearch($id_mj, $params)
	{
		$sqlstr = "";
		switch($id_mj)
		{
			case "1":
				include_once(SERVER_ROOT."/../classes/geo.php");
				$geo = new Geo();
				$geo->geo_location = 1;
				$join = $geo->GeoJoin("a.id_prov");
				$sqlstr = "SELECT a.id_ass AS mj_id, a.nome AS mj_name,a.email AS mj_email
				FROM ass a {$join['join']} 
				 WHERE a.email<>'' AND a.approved=1  ";
				if (strlen($params['name']) > 0)
					$sqlstr .= " AND (a.nome LIKE '%{$params['name']}%' OR a.nome2 LIKE '%{$params['name']}%')  ";
				if (strlen($params['email']) > 0)
					$sqlstr .= " AND (a.email LIKE '%{$params['email']}%')  ";
				if ($params['id_type'] > 0)
					$sqlstr .= " AND a.id_tipo={$params['id_type']} ";
				if ($params['id_geo']>0)
					$sqlstr .= " AND a.id_prov='{$params['id_geo']}' ";
				$sqlstr .= " GROUP BY a.email ORDER BY a.nome";
			break;
		}
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs(8);
		return $mj->RecipientsSet($sqlstr,$id_mj>3,$id_mj>3);
	}
	
	public function Mantained($id_p)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.*,{$join['name']} AS geo_name,ass_tipo.ass_tipo,UNIX_TIMESTAMP(a.insert_date) AS hdate_ts
		 FROM ass a 
		 {$join['join']}
		 INNER JOIN ass_tipo ON a.id_tipo=ass_tipo.id_ass_tipo
		 WHERE a.id_p=$id_p AND a.approved=1
		 ORDER BY a.insert_date DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	private function NewlineReplace( &$mytext, $replace_quotes=false )
	{
		$mytext = preg_replace('/\n/',"", $mytext);
		$mytext = preg_replace('/\r/',"", $mytext);
		if($replace_quotes)
			$mytext = str_replace("\"", "\"\"",$mytext);
		return $mytext;
	}

	private function OrgKeywords($id_org)
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$org_keywords = array();
		$o->GetKeywords($id_org,$o->types['org'], $org_keywords,0,false);
		return $org_keywords;
	}

	public function Params($only_public=false,$only_index=false,$only_listed=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_keyword_param,id_keyword,label,type,params,public,index_include
		FROM keyword_params
		WHERE id_type=" . ASSO_KEYWORD_TYPE;
		if($only_public)
			$sqlstr .= " AND public=1 ";
		if($only_index)
			$sqlstr .= " AND index_include=1 ";
		if($only_listed)
			$sqlstr .= " AND list_include=1 ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function PendingInserts()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT ass.id_ass,ass.nome FROM ass WHERE approved=0";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Search( &$rows, $params, $only_approved=true )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_ass,a.nome,a.nome2,a.indirizzo,a.citta,{$join['name']} AS geo_name,a.tel,ass_tipo.ass_tipo,a.approved
		FROM ass a
		{$join['join']}
		INNER JOIN ass_tipo ON a.id_tipo=ass_tipo.id_ass_tipo
		LEFT JOIN keywords_use ku ON a.id_ass=ku.id AND ku.id_type=4
		WHERE a.id_ass>0  ";
		if($only_approved)
			$sqlstr .= " AND a.approved=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND a.nome like '%{$params['name']}%' ";
		if (strlen($params['town']) > 0)
			$sqlstr .= " AND a.citta like '%{$params['town']}%' ";
		if ($params['id_type'] > 0)
			$sqlstr .= " AND a.id_tipo={$params['id_type']} ";
		if ($params['id_k'] > 0)
			$sqlstr .= " AND ku.id_keyword={$params['id_k']} ";
		if ($params['id_prov'] > 0)
			$sqlstr .= " AND a.id_prov={$params['id_prov']} ";
		if ($params['id_reg'] > 0)
			$sqlstr .= " AND prov.id_reg={$params['id_reg']} ";
		$sqlstr .= " GROUP BY a.id_ass ORDER BY a.nome ASC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function SearchPub( $unescaped_params,$unescaped_kparams )
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		$db =& Db::globaldb();
		$params = array();
		foreach($unescaped_params as $key=>$value)
			$params[$key] = $db->SqlQuote($value);
		$rows = array();
		$sqlstr = "SELECT a.*,at.ass_tipo,'asso' AS item_type,{$join['name']} AS geo_name
		FROM ass a
		{$join['join']}
		INNER JOIN ass_tipo at ON a.id_tipo=at.id_ass_tipo
		LEFT JOIN keywords_use ku ON a.id_ass=ku.id AND ku.id_type=4
		WHERE a.approved=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (a.nome like '%{$params['name']}%' OR a.nome2 like '%{$params['name']}%' )";
		if (strlen($params['address']) > 0)
			$sqlstr .= " AND a.indirizzo like '%{$params['address']}%' ";
		if (strlen($params['town']) > 0)
			$sqlstr .= " AND a.citta like '%{$params['town']}%' ";
		if (strlen($params['text']) > 0)
			$sqlstr .= " AND (a.nome like '%{$params['text']}%' OR a.nome2 like '%{$params['text']}%' OR a.indirizzo like '%{$params['text']}%'
			 OR a.citta like '%{$params['text']}%' OR a.cap like '%{$params['text']}%' OR a.tel like '%{$params['text']}%' OR a.email like '%{$params['text']}%' 
			 OR a.sito like '%{$params['text']}%' OR a.ccp like '%{$params['text']}%' OR a.note like '%{$params['text']}%' OR a.referente like '%{$params['text']}%'
			 OR a.pubblicaz like '%{$params['text']}%' OR a.fonte like '%{$params['text']}%' )";
		if ($params['id_k'] > 0)
			$sqlstr .= " AND ku.id_keyword={$params['id_k']} ";
		if ($params['id_assotype'] > 0)
			$sqlstr .= " AND a.id_tipo={$params['id_assotype']} ";
		if ($params['id_geo'] > 0)
			$sqlstr .= " AND a.id_prov={$params['id_geo']} ";
		$sqlstr .= " GROUP BY a.id_ass ORDER BY a.nome ASC ";
		$db->QueryExe($rows, $sqlstr);
		if(count($unescaped_kparams)>0)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			$rows_filtered = array();
			foreach($rows as $row)
			{
				$can_be_added = true;
				$row_params = $v->Deserialize($row['kparams']);
				if(is_array($row_params) && count($row_params)>0)
				{
					foreach($unescaped_kparams as $pkey=>$pvalue)
					{
						if(array_key_exists($pkey,$row_params))
						{
							if(strpos(strtolower($row_params[$pkey]),strtolower($pvalue))===false)
								$can_be_added = false;
						}
						else 
							$can_be_added = false;
					}
				}
				else 
					$can_be_added = false;
				if($can_be_added)
					$rows_filtered[] = $row;
			}
			$rows = $rows_filtered;
		}
		return $rows;
	}

	public function Stats( &$rows, $id_type )
	{
		$db =& Db::globaldb();
		$rows = array();
		switch ($id_type)
		{
			case 1:
				$sqlstr = "SELECT ass_tipo.ass_tipo, COUNT(ass.id_ass) AS Conteggio
					FROM ass_tipo
					LEFT JOIN ass ON ass_tipo.id_ass_tipo=ass.id_tipo
					GROUP BY ass_tipo.ass_tipo
					ORDER BY Conteggio DESC	";
			break;
			case 2:
				$sqlstr = "SELECT keywords.keyword, COUNT(ass_keywords.id_ass) AS Conteggio
						FROM keywords
						LEFT JOIN ass_keywords ON keywords.id_keyword=ass_keywords.id_keyword
						GROUP BY keywords.keyword
						ORDER BY Conteggio DESC	";;
			break;
			case 3:
				$sqlstr = "SELECT prov.prov, COUNT(ass.id_ass) AS Conteggio
						FROM prov
						LEFT JOIN ass USING(id_prov)
						GROUP BY prov.prov
						ORDER BY Conteggio DESC	";
			break;
			case 4:
				$sqlstr = "SELECT reg.reg, COUNT(ass.id_ass) AS Conteggio
						FROM reg
						LEFT JOIN prov ON reg.id_reg=prov.id_reg
						LEFT JOIN ass ON prov.id_prov=ass.id_prov
						GROUP BY reg.reg
						ORDER BY Conteggio DESC	";
			break;
		}
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Type( $id )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_ass_tipo,ass_tipo FROM ass_tipo WHERE id_ass_tipo=$id");
		return $row;
	}

	public function TypeAdd( $type )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass_tipo" );
		$nextId = $db->nextId( "ass_tipo", "id_ass_tipo" );
		$res[] = $db->query( "INSERT INTO ass_tipo (id_ass_tipo,ass_tipo) VALUES ($nextId,'$type')" );
		Db::finish( $res, $db);
	}

	public function TypeDelete( $id_type )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass_tipo" );
		$res[] = $db->query( "DELETE FROM ass_tipo WHERE id_ass_tipo=$id_type" );
		Db::finish( $res, $db);
	}

	public function TypeUpdate( $id, $type )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass_tipo" );
		$res[] = $db->query( "UPDATE ass_tipo SET ass_tipo='$type' WHERE id_ass_tipo=$id" );
		Db::finish( $res, $db);
	}

	public function Types()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_ass_tipo,ass_tipo from ass_tipo order by ass_tipo";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TypesP( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_ass_tipo,ass_tipo from ass_tipo order by ass_tipo";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}

class Asso
{
	public $name;

	private $id;
	private $email;
	private $pub_docs_path;
	public $orgs_orig;
	private $notify_user;
	private $notify_admin;

	function __construct($id)
	{
		if ($id>0)
		{
			$this->id = $id;
			$org = array();
			$db =& Db::globaldb();
			$db->query_single($org,"SELECT nome,email FROM ass WHERE id_ass=$id");
			$this->name = $org['nome'];
			$this->email = $org['email'];
		}
		$conf = new Configuration();
		$paths = $conf->Get("paths");
		$this->orgs_orig = $conf->Get("orgs_orig");
		$this->pub_docs_path = "pub/{$paths['docs']}orgs";
		$conf_orgs = new Configuration("orgs");
		$this->notify_user = $conf_orgs->Get("notify_user");
		$this->notify_admin = $conf_orgs->Get("notify_admin");
	}

	public function Delete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass" );
		$res[] = $db->query( "DELETE FROM ass WHERE id_ass=$this->id" );
		Db::finish( $res, $db);
		$docs = array();
		$num_docs = $this->Docs($docs,false);
		if($num_docs>0)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			foreach($docs as $doc)
			{
				$pub_file = "{$this->pub_docs_path}/{$doc['id_doc']}.{$doc['format']}";
				$fm->Delete($pub_file);
			}
			$db->begin();
			$db->lock( "docs_orgs" );
			$res[] = $db->query( "DELETE FROM docs_orgs WHERE id_org=$this->id" );
			Db::finish( $res, $db);
		}
		$this->ImageDelete();
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['org']);
	}

	public function DocAdd($id_doc)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_orgs" );
		$res[] = $db->query( "INSERT INTO docs_orgs (id_doc,id_org) VALUES ($id_doc,$this->id)" );
		Db::finish( $res, $db);
		$this->DocFileUpdate($id_doc);
	}

	public function DocDelete($id_doc)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT d.format FROM docs_orgs do
			 INNER JOIN docs d ON do.id_doc=d.id_doc 
			 WHERE do.id_doc=$id_doc AND do.id_org=$this->id ";
		$row = array();
		$db->query_single($row,$sqlstr);
		$db->begin();
		$db->lock( "docs_orgs" );
		$res[] = $db->query( "DELETE FROM docs_orgs WHERE id_doc='$id_doc' AND id_org='$this->id' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$orig_file = "uploads/docs/{$id_doc}.{$row['format']}";
		$pub_file = "{$this->pub_docs_path}/{$id_doc}.{$row['format']}";
		$fm->Delete($orig_file);
		$fm->Delete($pub_file);
		$fm->PostUpdate();
	}
	
	public function DocFileUpdate($id_doc)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT d.format,a.approved FROM docs_orgs do
			 INNER JOIN docs d ON do.id_doc=d.id_doc 
			 INNER JOIN ass a ON do.id_org=a.id_ass
			 WHERE do.id_doc=$id_doc AND do.id_org=$this->id ";
		$row = array();
		$db->query_single($row,$sqlstr);
		if(count($row)>0)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$pub_file = "{$this->pub_docs_path}/{$id_doc}.{$row['format']}";
			if($row['approved'])
			{
				$orig_file = "uploads/docs/{$id_doc}.{$row['format']}";
				$fm->Copy($orig_file,$pub_file);
			}
			else 
			{
				$fm->Delete($pub_file);
			}
			$fm->PostUpdate();
		}
	}
	
	public function Docs( &$rows, $paged=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT ad.id_doc,d.title,d.description,d.filename,d.format,d.id_language,d.author,d.source,d.id_licence 
		FROM docs_orgs ad
		INNER JOIN docs d ON ad.id_doc=d.id_doc
		WHERE ad.id_org='$this->id'
		ORDER BY ad.id_doc DESC";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function Get($approved_only)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$geo->geo_location = 1;
		$join = $geo->GeoJoin("a.id_prov");
		$sqlstr = "SELECT a.*,at.ass_tipo,'asso' AS item_type,{$join['name']} AS geo_name
		FROM ass a
		{$join['join']}
		INNER JOIN ass_tipo at ON a.id_tipo=at.id_ass_tipo 
		WHERE a.id_ass='$this->id'";
		if($approved_only)
			$sqlstr .= " AND a.approved=1";
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ImageDelete()
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$fm = new FileManager;
		$fm->Delete("uploads/orgs/orig/{$this->id}.jpg");
		$i->RemoveWrapper("org_image",$this->id);
		$fm->Delete("pub/{$i->pub_path}orgs/0/{$this->id}.jpg");
		$fm->Delete("pub/{$i->pub_path}orgs/1/{$this->id}.jpg");
		$fm->Delete("pub/{$i->pub_path}orgs/orig/{$this->id}.jpg");
		$fm->PostUpdate();
	}

	public function ImageUpdate($file)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/images.php");
		$fm = new FileManager;
		$this->ImageDelete();
		$origfile = "uploads/orgs/orig/{$this->id}.jpg";
		$fm->MoveUpload($file['temp'],$origfile);
		$i = new Images();
		$i->ConvertWrapper("org_image",$origfile,$this->id);
		if(!$i->isCDN) {
			$filename = $this->id . "." . $i->convert_format;
			$fm->Copy("uploads/orgs/0/$filename","pub/{$i->pub_path}orgs/0/$filename");
			$fm->Copy("uploads/orgs/1/$filename","pub/{$i->pub_path}orgs/1/$filename");
			if($this->orgs_orig) {
				$fm->Copy("uploads/orgs/orig/$filename","pub/{$i->pub_path}orgs/orig/$filename");
			}
		} else {
			$i->CDNReset('orgs',$this->id,$i->convert_format);
		}
		$fm->PostUpdate();
	}

	public function Insert( $id_type, $name, $name2, $address, $zip, $town, $id_prov, $phone, $fax, $email, $website, $ccp, $notes, $publications, $person, $approved, $admin_notes )
	{
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$db->begin();
		$db->lock( "ass" );
		$id_org = $db->nextId( "ass", "id_ass" );
		$sqlstr = "INSERT INTO ass (id_ass,nome,nome2,indirizzo,citta,id_prov,cap,tel,fax,email,sito,ccp,note,referente,pubblicaz,fonte,id_tipo,approved,admin_notes,insert_date)
		VALUES ($id_org,'$name','$name2','$address','$town',$id_prov,'$zip','$phone','$fax','$email','$website','$ccp','$notes','$person','$publications','Inserimento manuale',$id_type,$approved,'$admin_notes','$today_time')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if($approved)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd(4,$id_org,0,0,1);
		}
		return $id_org;
	}

	public function InsertPub($id_asso_type,$name,$name2,$address,$postcode,$town,$id_geo,$phone,$fax,$email,$website,$contact_name,$in_charge_of,$email_p,$name_p,$id_topic,$notes,$unescaped_params)
	{
		$id_p = 0;
		if($in_charge_of)
		{
			include_once(SERVER_ROOT."/../classes/people.php");
			$pe = new People();
			$user = $pe->UserInfo(false);
			if($user['id']>0)
			{
				$id_p = $user['id'];
				$email_p = $user['email'];
				$name_p = $user['name'];
			}
			else
			{
				$names = explode(" ",$name_p,2);
				$id_p = $pe->UserCreate($names[0],$names[1],$email_p,1,0,array(),true,$id_topic);
			}
		}
		$db =& Db::globaldb();
		$today_time = $db->GetTodayTime();
		$db->begin();
		$db->lock( "ass" );
		$this->id = $db->nextId( "ass", "id_ass" );
		$sqlstr = "INSERT INTO ass (id_ass,nome,nome2,indirizzo,citta,id_prov,cap,tel,fax,email,sito,referente,fonte,id_tipo,approved,id_p,insert_date,note)
			VALUES ($this->id,'$name','$name2','$address','$town',$id_geo,'$postcode','$phone','$fax','$email','$website','$contact_name','Online',$id_asso_type,0,$id_p,'$today_time','$notes')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->KeywordParamsUpdate($unescaped_params);
		if ($this->notify_admin)
			$this->NotifyAdmin("Inserimento",$name,$this->id);
		if($in_charge_of)
		{
			if($id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$id_style = $t->id_style;
			}
			else 
				$id_style = 0;
			$this->NotifyUser($name_p,$email_p,$name,$id_style);
		}
		return $this->id;
	}
	
	public function KeywordParamsUpdate($unescaped_params,$merge=false)
	{
		$kparams = array();
		foreach($unescaped_params as $key=>$value)
			$kparams["kp_" . $key] = $value;
		$row = $this->Get(false);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$aparams = $v->Deserialize($row['kparams']);
		$current_params = (is_array($aparams))? $aparams : array();
		foreach($unescaped_params as $key=>$value)
		{
			$pkey = "kp_" . $key;
			$current_params[$pkey] = $value;
		}
		$current_params2 = array();
		foreach($current_params as $ckey=>$cvalue)
		{
			$current_params2[$ckey] = (array_key_exists($ckey,$kparams) || $merge)? $cvalue : "";
		}
		$ser_array = $v->Serialize($current_params2);
		$this->KeywordParamsStore($ser_array);
	}
	
	public function KeywordParamsStore($serialized_params)
	{
		$sqlstr = "UPDATE ass SET kparams='$serialized_params' WHERE id_ass=$this->id";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Keywords()
	{
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$org_keywords = array();
		$o->GetKeywords($this->id,$o->types['org'], $org_keywords,ASSO_KEYWORD_TYPE,false);
		return $org_keywords;
	}

	private function NotifyAdmin($event, $message, $id_asso)
	{
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		$subject = "$mail->title - Database Associazioni: $event";
		$message = "Associazione: $message \n\n" . $mail->admin_web . "/orgs/org.php?id=$id_asso\n\n";
		include_once(SERVER_ROOT."/../classes/users.php");
		$users = new Users;
		foreach($users->AdminUsers(8) as $user)
		{
			$mail->SendMail($user['email'], $user['name'], $subject, $message, array());
		}
	}

	private function NotifyUser($name,$email,$name_asso,$id_style)
	{
		if($this->notify_user)
		{
			include_once(SERVER_ROOT."/../classes/translator.php");
			$trm8 = new Translator(1,8,false,$id_style);
			$subject = $trm8->Translate("insert_mail_subj");
			$message = $trm8->TranslateParams("insert_mail_pending",array($name,$name_asso));
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			$mail->SendMail($email, $name, $subject, $message, array());		
		}
	}

	public function NotifyApproval($what)
	{
		if($this->notify_user)
		{
			include_once(SERVER_ROOT."/../classes/mail.php");
			$mail = new Mail();
			if ($mail->CheckEmail($this->email))
			{
				include_once(SERVER_ROOT."/../classes/ini.php");
				$ini = new Ini();
				if ($what=="ins")
				{
					$subject  = "Inserimento nel database risorse di $mail->title";
					$message  = "Questo messaggio conferma l'avvenuto inserimento di \"$this->name\" nel database risorse di $mail->title.\n";
				}
				$pub_web = $ini->Get('pub_web');
				$org_path = $ini->Get('org_path');
				$message .= "La scheda relativa e' disponibile online a questo indirizzo:\n";
				$message .= "$pub_web/$org_path/org.php?id=$this->id\n";
				$message .= "\n\n$mail->title Database Risorse - $pub_web/$org_path\n\n";
				$message .= $mail->title . "\n" . $pub_web . "\n";
				$mail->SendMail($this->email, "", $subject, $message, array());
			}		
		}
	}

	public function Update($id_type, $name, $name2, $address, $zip, $town, $id_prov, $phone, $fax, $email, $website, $ccp, $notes, $publications, $person, $source, $approved, $admin_notes)
	{
		$prev = $this->Get(false);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass" );
		$res[] = $db->query( "UPDATE ass SET nome='$name',nome2='$name2',indirizzo='$address',cap='$zip',citta='$town',
			id_prov=$id_prov,tel='$phone',fax='$fax',email='$email',sito='$website',ccp='$ccp',note='$notes',
			pubblicaz='$publications',referente='$person',fonte='$source',approved=$approved,id_tipo=$id_type,admin_notes='$admin_notes'
			WHERE id_ass=$this->id" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($approved)
			$s->IndexQueueAdd(4,$this->id,0,0,1);
		elseif($prev['approved'])
			$s->ResourceRemove(4,$this->id);
		$docs = array();
		$num_docs = $this->Docs($docs,false);
		if($num_docs>0 && $approved!=$prev['approved'])
		{
			foreach($docs as $doc)
				$this->DocFileUpdate($doc['id_doc']);
		}
	}

	public function UpdateUser($id_p)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "ass" );
		$res[] = $db->query( "UPDATE ass SET id_p='$id_p' WHERE id_ass=$this->id" );
		Db::finish( $res, $db);
	}

}

?>
