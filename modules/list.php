<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class MailingList
{
	public $impl;
	public $email;

	private $id;
	private $web;

	function __construct($id)
	{
		$this->id = $id;
		$ini = new Ini;
		$this->web = $ini->GET('lists_path');
	}

	public function ChangePassword( $password_old, $password_new )
	{
		include_once(SERVER_ROOT."/../classes/adminhelper.php");
		$ah = new AdminHelper;
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$msg = "password_change_failed";
		$list = $this->GetList();
		$sc = new Scrypt;
		$dpass = $sc->decrypt($list['password']);
		if ($password_old == $dpass || ($ah->session->Get('module_admin'))==1)
		{
			$cpass = $sc->encrypt($password_new);
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "lists" );
			$res[] = $db->query( "UPDATE lists SET password='$cpass' WHERE id_list=$this->id" );
			Db::finish( $res, $db);
			$msg = "password_changed";
		}
		$ah->MessageSet($msg);
	}

	public function Command( $command, $email )
	{
		$msg = array();
		$msg['string'] = "";
		$msg['params'] = array();
		$list = $this->GetList();
		include_once(SERVER_ROOT."/../classes/crypt.php");
		$sc = new Scrypt;
		$dpass = $sc->decrypt($list['password']);
		switch($list['impl'])
		{
			case "1": // smartlist remote
				$owner_email = $list['owner_email'];
				$pieces	= explode("@",$list['email']);
				$list_email	= $pieces[0] . "-request@" . $pieces[1];
				$extra = array();
				$extra['email'] = $owner_email;
				$extra['reply-to'] = $owner_email;
				$extra['header'] = "X-command: $owner_email $dpass $command $email";
				include_once(SERVER_ROOT."/../classes/mail.php");
				$mail = new Mail();
				$mail->SendMail( $list_email, "", " ", " ", $extra );
				$msg['string'] = "smartlist_$command";
				switch($command)
				{
					case "subscribe":
					case "unsubscribe":
						$msg['params'] = array($email,$list['email']);
					break;
					case "checkdist":
						$msg['params'] = array($email,$list['email'],$owner_email);
					break;
					case "showdist":
					case "showlog":
						$msg['params'] = array($email,$list['email'],$owner_email);
					break;
					case "wipelog":
						$msg['params'] = array($list['email']);
					break;
				}
			break;
			case "2": // mailman remote
				$owner_email = $list['owner_email'];
				$pieces	= explode("@",$list['email']);
				$list_email	= $pieces[0] . "-request@" . $pieces[1];
				$extra = array();
				$msg['string'] = "mailman_$command";
				switch($command)
				{
					case "subscribe":
					case "unsubscribe":
						$extra['email'] = $owner_email;
						$extra['reply-to'] = $owner_email;
						$msg['params'] = array($email,$list['email']);
						$body = "$command $dpass address=$email";
					break;
					case "showdist":
						$extra['email'] = $email;
						$extra['reply-to'] = $email;
						$msg['params'] = array($email,$list['email'],$owner_email);
						$body = "who $dpass address=$owner_email";
					break;
				}
				include_once(SERVER_ROOT."/../classes/mail.php");
				$mail = new Mail();
				$mail->SendMail( $list_email, "", " ", $body, $extra );
			break;
		}
		return $msg;
	}

	public function Delete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "lists" );
		$res[] = $db->query( "DELETE FROM lists WHERE id_list='$this->id' " );
		Db::finish( $res, $db);
	}
	
	public function Request( $command, $email)
	{
		if($command == "subscribe" || $command=="unsubscribe")
		{
			$list = $this->GetList();
			switch($list['impl'])
			{
				case "1": // smartlist remote
					$pieces	= explode("@",$list['email']);
					$to		= $pieces[0] . "-request@" . $pieces[1];
					$extra = array();
					$extra['email'] = $email;
					$extra['name'] = $email;
					include_once(SERVER_ROOT."/../classes/mail.php");
					$mail = new Mail();
					$mail->SendMail( $to, "", $command, "", $extra );
				break;
				case "2": // mailman remote
					$pieces	= explode("@",$list['email']);
					$to		= $pieces[0] . "-request@" . $pieces[1];
					$extra = array();
					$extra['email'] = $email;
					$extra['name'] = $email;
					include_once(SERVER_ROOT."/../classes/mail.php");
					$mail = new Mail();
					$mail->SendMail( $to, "", $command, "", $extra );
				break;
			}
		}
	}

	public function GetList()
	{
		$list = array();
		$db =& Db::globaldb();
		$db->query_single( $list, "SELECT l.email,l.password,l.id_user,
		l.owner_email,l.public,l.name,l.description,l.impl,l.archive,l.feed,l.id_topic,l.id_group,l.id_list
		FROM lists l 
		WHERE l.id_list='$this->id' ");
		$this->impl = $list['impl'];
		$this->email = $list['email'];
		return $list;
	}

	public function Insert( $email, $id_user, $public, $name, $description, $impl, $id_topic, $id_group,$owner_email,$archive,$feed )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "lists" );
		$id_list = $db->nextId( "lists", "id_list" );
		$res[] = $db->query( "INSERT INTO lists (id_list,email,id_user,public,name,description,impl,id_topic,id_group,owner_email,archive,feed)
			VALUES ($id_list,'$email',$id_user,$public,'$name','$description','$impl',$id_topic,$id_group,'$owner_email','$archive','$feed') " );
		Db::finish( $res, $db);
		return $id_list;
	}

	public function Update($email, $id_user, $public, $name, $description, $impl, $id_topic, $id_group,$owner_email,$archive,$feed)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "lists" );
		$res[] = $db->query( "UPDATE lists SET email='$email',id_user='$id_user',public='$public',name='$name',
			description='$description',impl='$impl',id_topic='$id_topic',id_group='$id_group',
			owner_email='$owner_email',archive='$archive',feed='$feed' 
			WHERE id_list='$this->id' " );
		Db::finish( $res, $db);
	}

	public function UserAdmin()
	{
		$admin = array();
		$db =& Db::globaldb();
		$db->query_single( $admin, "SELECT lists.email as email,lists.id_user,users.name,users.email as owner FROM lists INNER JOIN users ON lists.id_user=users.id_user WHERE id_list=$this->id");
		return $admin;
	}


}
?>
