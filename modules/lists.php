<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Lists
{
	private $default_list;

	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$this->default_list = $ini->Get('default_list');
	}

	public function ConfigurationUpdate($default_list,$path)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->Set("default_list",$default_list);
		$ini->SetPath("lists_path",$path);
	}

	public function GroupGet($id_group)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_group,name FROM lists_groups WHERE id_group=$id_group");
		return $row;
	}

	public function GroupInsert( $name )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "lists_groups" );
		$id_group = $db->nextId( "lists_groups", "id_group" );
		$res[] = $db->query( "INSERT INTO lists_groups (id_group,name) VALUES ($id_group,'$name')" );
		Db::finish( $res, $db);
	}

	public function GroupLists($id_group, $only_email=false,$only_public=true)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = ($only_email)?" SELECT email ": "SELECT id_list,name,email,description,feed,archive,'list' AS item_type ";
		$sqlstr .= " FROM lists WHERE id_group='$id_group' ";
		if ($only_public)
			$sqlstr .= " AND public=1 ";
		$sqlstr .= " ORDER BY email ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function GroupUpdate( $id_group, $name )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "lists_groups" );
		$res[] = $db->query( "UPDATE lists_groups SET name='$name' WHERE id_group=$id_group" );
		Db::finish( $res, $db);
	}

	public function GroupsAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_group,name FROM lists_groups ORDER BY id_group";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function GroupsAllP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_group,g.name,COUNT(l.id_list) AS counter
			FROM lists_groups g
			LEFT JOIN lists l ON g.id_group=l.id_group
			GROUP BY g.id_group
			ORDER BY g.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ListsAll( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT l.id_list,l.email,users.id_user,users.name AS owner_name,l.public,l.name,t.name AS topic_name,g.name AS group_name
			FROM lists l INNER JOIN users USING(id_user)
			LEFT JOIN topics t ON l.id_topic=t.id_topic
			LEFT JOIN lists_groups g ON l.id_group=g.id_group ORDER BY l.email";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ListsPasswords()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_list,email,password FROM lists ORDER BY email";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Topic($id_topic)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_list,email,name,description FROM lists WHERE id_topic='$id_topic' AND public=1 ORDER BY email";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
		
	}
}
?>
