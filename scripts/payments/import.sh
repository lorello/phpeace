#! /bin/sh

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

# Input parameters
# filename (in import directory under installation path)
# bank (paypal / etica / poste)
# id_account
# type
# dryrun

php $CURRENTDIR/import.php -f $1 -b $2 -c $3 -t $4 -d $5

