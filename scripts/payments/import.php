<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

$options = getopt("f:b:c:t:d:");

if(is_array($options) && $options['f']!="" && $options['b']!='' && $options['c']>0)
{
	$id_account = $options['c'];
	include_once(SERVER_ROOT."/../classes/file.php");
	include_once(SERVER_ROOT."/../classes/payment.php");
	$pa = new Payment();

	$account = $pa->AccountGet($id_account);
	if (count($account) == 0) {
		exit("Account not found {$id_account}\n");
	}

	$fm = new FileManager();
	$filename = "import/" . $options['f'];
	if(!$fm->Exists($filename))
	{
		exit("Missing file $filename\n");
	}

	$bank = $options['b'];
	$supported_banks = ['paypal','etica','poste'];
	if(!in_array($bank,$supported_banks)) {
		exit("$bank is not supported - supported banks: " . implode(', ', $supported_banks) . "\n");
	}

	$debug = isset($options['d']) && $options['d']!='';

	switch($bank) {
		case 'paypal':
			$num_fields_expected = 41;
			// assume the following 41 headers
			// "Data","Orario","Fuso orario","Nome","Tipo","Stato","Valuta","Lordo","Tariffa","Netto","Indirizzo email mittente","Indirizzo email destinatario","Codice transazione","Indirizzo di spedizione","Stato dell'indirizzo","Titolo oggetto","Codice articolo","Importo spese di spedizione e imballaggio","Importo assicurazione","Imposte sulle vendite","Nome opzione 1","Valore opzione 1","Nome opzione 2","Valore opzione 2","Codice transazione di riferimento","N° ordine commerciante","Numero personalizzato","Quantità","Codice ricevuta","Saldo","Indirizzo","Indirizzo (continua)","Città","Provincia","CAP/Codice postale","Paese","Telefono","Oggetto","Messaggio","Prefisso internazionale","Impatto sul saldo"
			$lines = $fm->TextFileLines($filename);
			if(count($lines)>0) {
				printf("Processing %s lines for account \"%s\"\n", sizeof($lines), $account['name']);
				include_once(SERVER_ROOT."/../classes/db.php");
				$db =& Db::globaldb();
				include_once(SERVER_ROOT."/../classes/people.php");
				$pe = new People();
				include_once(SERVER_ROOT."/../classes/validator.php");
				$va = new Validator();
				include_once(SERVER_ROOT."/../classes/geo.php");
				$geo = new Geo();
				include_once(SERVER_ROOT."/../classes/varia.php");
		
				$num_payments = 0;
				foreach($lines as $row_id=>$line) {
					$payment = array();
					$values = str_getcsv($line);
					$num_fields = sizeof($values);
					if($num_fields > 1 && $num_fields != $num_fields_expected) {
						echo $line;
						exit("Line $row_id with unexpected number of fields: expected {$num_fields_expected}, got $num_fields\n");
					}
					if(	$row_id > 0 && // skip headers 
						$num_fields > 1 && // ignore empty line
						$values[5] == 'Completata' && // only completed payments
						(int)$values[9] != 0 // skip zero amounts
						) {
						$date = explode('/',$values[0]);
						$time = explode(':',$values[1]);
						if(count($date)!=3 && count($time)!=3)
							exit("Line $row_id: Wrong format in date {$values[0]} - time {$values[1]}\n");
						// ignore timezone
						$ts = mktime($time[0], $time[1], $time[2], $date[1], $date[0], $date[2]);
						$payment['pay_date'] = $db->getTodayTime($ts);
						$payment['id_account'] = $id_account;
						$payment['is_in'] = (int)$values[9]>0? 1:0;
						$payment['amount'] = abs((int)$values[9]);
						if (!in_array($values[6],$pa->currency_codes)) {
							exit("Line $row_id: Currency not supported {$values[6]}\n");
						} else {
							$payment['id_currency'] = array_search($values[6], $pa->currency_codes);
						}
						$payment['verified'] = 1;
						$payment['tx_id'] = $db->SqlQuote($values[12]);
						if(strlen($payment['tx_id']) < 4) {
							exit("Line $row_id: Payment identifier missing {$values[12]}\n");
						}
						if($pa->PaymentCheck($id_account, $payment['tx_id'])) {
							printf("Payment %s already imported for account \"%s\"\n", $payment['tx_id'], $account['name']);
						} else {
							$payment['description'] = $db->SqlQuote($values[38]);
							$payment['id_payment_type'] = 2; // libera erogazione
							$payment['notes'] = $db->SqlQuote($values[4]);
							$email = $db->SqlQuote(trim($values[10]));
							$user = array();
							$names = explode(' ', $db->SqlQuote(trim($values[3])), 2);
							$user['name1'] = $names[0];
							if(sizeof($names)==2) {
								$user['name2'] = $names[1];
							}
							$user['phone'] = $db->SqlQuote(trim($values[36]));
							$user['address'] = $db->SqlQuote($values[30]);
							$user['address_notes'] = $db->SqlQuote($values[31]);
							$user['town'] = $db->SqlQuote($values[32]);
							$prov = $geo->ProvinceGetByProv($values[33]);
							if(isset($prov['id_prov'])) {
								$user['id_geo'] = $prov['id_prov'];
							}
							$user['postcode'] = $db->SqlQuote($values[34]);
		
							$id_p = 0;
							if($payment['is_in']==1) {
								$email = $db->SqlQuote(trim($values[10]));
							} else {
								$email = $db->SqlQuote(trim($values[11]));
							}
		
							if($email != '') {
								if($va->Email($email))
								{
									$user['email'] = $email;
									$user['email_valid'] = 1;
								} else {
									$user['email_valid'] = 0;
									printf("Line $row_id: invalid email $email\n");
									if($values[3]!='') {
										$user['email'] = str_replace(' ','_',$values[3]) . '@farlocco.it';
									} else {
										printf("Line $row_id: sender name and email missing\n");
										$user['email'] = Varia::Uid() . '@farlocco.it';
									}
								}
							} else {
								$user['email_valid'] = 0;
								if($payment['is_in']==1) {
									// Missing email usually is a PayPal internal transaction
									$user['email'] = $email = 'info@paypal.com';
									printf("Line $row_id: missing email\n");
								} else {
									if($values[3]!='') {
										$user['email'] = str_replace(' ','_',$values[3]) . '@farlocco.it';
									} else {
										printf("Line $row_id: recipient name and email missing\n");
										$user['email'] = Varia::Uid() . '@farlocco.it';
									}
								}
							}
							// check user is there
							$person = $pe->UserGetByEmail($email);
							if($person['id_p']>0) {
								$id_p = $person['id_p'];
								echo "Found matching user for email $email\n";
							} else {
								$identifier = $pe->UserIdentifier($email);
								$person = $pe->UserGetByIdentifier($identifier);
								if(isset($person['id_p'])) {
									$id_p = $person['id_p'];
									echo "Found matching user via identifier for email $email\n";
								}
							}
							if($id_p > 0) {
								$pe->UserUpdateDetails2($id_p,$user['address'],$user['address_notes'],$user['postcode'],$user['town'],$user['id_geo'],$user['phone']);
								if(strlen("{$person['name1']}{$person['name2']}") == 0) {
									echo "Updating user name {$user['name1']} {$user['name2']}\n";
									$pe->UserUpdateName($id_p, $user['name1'], $user['name2']);
								}
							} else {
								echo "Creating account for email $email\n";
								$user['contact'] = 0;
								$user['active'] = 0;
								if($debug) {
									print_r($user);
								} else {
									$id_p = $pe->UserCreate($user['name1'],$user['name2'],$user['email'],$user['id_geo'],0,array(),false,0,0,false,$user['email_valid']);
									$pe->UserUpdate($id_p,'',$user['name1'],$user['name2'],'',$user['email'],$user['address'],$user['address_notes'],$user['postcode'],$user['town'],$user['id_geo'],$user['phone']);
								}
							}
							if ($debug) {
								print_r($payment);
								$num_payments++;
							} else {
								$id_payer = $pa->PayerAdd($id_p);
								$payment['id_payer'] = $id_payer;
								$id_payment = $pa->PaymentInsert($payment['pay_date'],$payment['id_payer'],$payment['id_account'],$payment['amount'],$payment['description'],$payment['notes'],$payment['id_payment_type'],$payment['is_in'],$payment['id_currency'],$payment['tx_id']);
								if ($id_payment>0)
									$num_payments++;
							}
						}
					}
				}
				printf("Imported %s payments into account \"%s\"\n", $num_payments, $account['name']);
			} else {
				exit("Empty file $filename\n");
			}
			break;
		case 'etica':
			$type = $options['t'];
			$supported_types = ['csv','pdf'];
			if(!in_array($type,$supported_types)) {
				exit("$type is not supported - supported types: " . implode(', ', $supported_types) . "\n");
			}
			switch($type) {
				case 'csv':
					$num_fields_expected = 7;
					// assume the following 7 headers (last is empty)
					// DATA; VALUTA; DARE; AVERE; CAUSALE; CAUSALE ABI;
					$lines = $fm->TextFileLines($filename);
					if(count($lines)>0) {
						printf("Processing %s lines for account \"%s\"\n", sizeof($lines), $account['name']);
						include_once(SERVER_ROOT."/../classes/db.php");
						$db =& Db::globaldb();
						include_once(SERVER_ROOT."/../classes/people.php");
						$pe = new People();
				
						$num_payments = 0;
						$summary_fields = ['CONTABILE','LIQUIDO','DISPONIBILE'];
						foreach($lines as $row_id=>$line) {
							$payment = array();
							$values = str_getcsv($line, $delimiter=';');
							$num_fields = sizeof($values);
							$is_summary = in_array(trim($values[4]),$summary_fields);
							if($num_fields > 1 && $num_fields != $num_fields_expected && !$is_summary) {
								echo $line;
								print_r($values);
								exit("Line $row_id with unexpected number of fields: expected {$num_fields_expected}, got $num_fields\n");
							}
							if(	$row_id > 0 && // skip headers
								$num_fields > 1 && // ignore empty line
								!$is_summary
							) {
								$date = explode('/',trim($values[0]));
								if(count($date)!=3)
									exit("Line $row_id: Wrong format in date {$values[0]}\n");
								// ignore timezone
								$ts = mktime(12,0,0, $date[1], $date[0], $date[2]);
								$payment['pay_date'] = $db->getTodayTime($ts);
								$payment['id_account'] = $id_account;
								if ((int)$values[2]>0) {
									$payment['is_in'] = 0;
									$payment['amount'] = abs((float)$values[2]);
								} else {
									$payment['is_in'] = 1;
									$payment['amount'] = abs((float)$values[3]);
								}
								$tx_id = trim($values[0]) . " {$payment['is_in']} {$payment['amount']} ";
								$desc = str_getcsv(preg_replace('/\s{3,}/','|',$values[4]), $delimiter='|');
								$desc = array_map('trim', $desc);
								$payment['notes'] = $db->SqlQuote($desc[0]);
								$user = array();
								if($payment['is_in']==1) {
									$payment['description'] = $db->SqlQuote(end($desc));
									for($i = 1;$i < count($desc);$i++) {
										if(!str_starts_with($desc[$i],'KKK') && !str_starts_with($desc[$i],'YYY'))
										{
											$name = $desc[$i];
											$payment['name'] = ucwords(strtolower($name));
											break;
										}
									}
									for($i = 1;$i < count($desc);$i++) {
										if(str_starts_with($desc[$i],'YY2'))
										{
											$user['address'] = $db->SqlQuote(str_replace('YY2','',$desc[$i]));
											break;
										}
									}
									$tx_id .= "{$payment['name']}";
									if(isset($payment['name'])) {
										$names = explode(' ', $db->SqlQuote(trim($payment['name'])));
										if(count($names) == 2) {
											$user['name1'] = $names[1];
											$user['name2'] = $names[0];
										} else {
											$user['name1'] = $db->SqlQuote(trim($payment['name']));
										}
									}
								} else {
									$payment['description'] = $db->SqlQuote($desc[1]);
									preg_match('/(.*)Ben.(.*).Cau.(.*)/',$desc[1],$matches);
									if(isset($matches[3])) {
										$payment['name'] = $db->SqlQuote($matches[2]);
										$payment['description'] = $db->SqlQuote($matches[3]);
										$names = explode(' ', $db->SqlQuote(trim($payment['name'])));
										if(count($names) == 2) {
											$user['name1'] = $names[0];
											$user['name2'] = $names[1];
										} else {
											$user['name1'] = $db->SqlQuote(trim($payment['name']));
										}	
									} else {
										$payment['name'] = $user['name1'] = 'Banca Etica';
									}
									$tx_id .= "{$payment['name']}";
								}
								if(!isset($payment['name'])) {
									echo $line;
									exit("Line $row_id could not determine name\n");
								}
								$payment['tx_id'] = md5($tx_id);
								if($pa->PaymentCheck($id_account, $payment['tx_id'])) {
									printf("Payment %s already imported for account \"%s\"\n", $payment['tx_id'], $account['name']);
								} else {
									$user_name = substr(strtolower(isset($user['name2'])? "{$user['name1']} {$user['name2']}" : $user['name1']),0,100);
									$user['email'] = str_replace(' ','_',$user_name) . '@farlocco.it';
									// create id_p
									$person = $pe->UserGetByEmail($user['email']);
									if($person['id_p']>0) {
										$id_p = $person['id_p'];
										echo "Found matching user for email {$user['email']}\n";
									} else {
										echo "Creating account for email {$user['email']}\n";
										if($debug) {
											print_r($user);
										} else {
											$id_p = $pe->UserCreate($user['name1'],$user['name2'],$user['email'],0,0,array(),false,0,0,false,0);
											if(isset($user['address'])) {
												$pe->UserUpdate($id_p,'',$user['name1'],$user['name2'],'',$user['email'],$user['address'],'','','','','');
											}
										}
									}
									if ($debug) {
										print_r($payment);
										$num_payments++;
									} else {
										$id_payer = $pa->PayerAdd($id_p);
										$payment['id_payer'] = $id_payer;
										$id_payment = $pa->PaymentInsert($payment['pay_date'],$payment['id_payer'],$payment['id_account'],$payment['amount'],$payment['description'],$payment['notes'],$payment['id_payment_type'],$payment['is_in'],$payment['id_currency'],$payment['tx_id']);
										if ($id_payment>0)
											$num_payments++;
									}
								}
							}
						}
						printf("Imported %s payments into account \"%s\"\n", $num_payments, $account['name']);
					} else {
						exit("Empty file $filename\n");
					}
					break;
			}
			break;
		case 'poste':
			$type = $options['t'];
			$supported_types = ['csv','pdf'];
			if(!in_array($type,$supported_types)) {
				exit("$type is not supported - supported types: " . implode(', ', $supported_types) . "\n");
			}
			switch($type) {
				case 'csv':
					$num_fields_expected = 7;
					// assume the following 7 headers (last is empty)
					// Data contabile  Data valuta     Addebito        Accredito       Causale operazione      Operazione      Descrizione movimento
					$lines = $fm->TextFileLines($filename);
					if(count($lines)>0) {
						printf("Processing %s lines for account \"%s\"\n", sizeof($lines), $account['name']);
						include_once(SERVER_ROOT."/../classes/db.php");
						$db =& Db::globaldb();
						include_once(SERVER_ROOT."/../classes/people.php");
						$pe = new People();

						$num_payments = 0;
						$is_payment_line = False;
						$row_id = 0;
						foreach($lines as $line) {
							$payment = array();
							$values = str_getcsv($line, $delimiter="\t");
							$values = array_map('trim', $values);
							foreach($values as &$value) {
								$value = preg_replace('/\s{1,}/',' ',$value);
							}
							if(isset($values[0]) && $values[0]=='LISTA MOVIMENTI') {
								$is_payment_line = True;
								$row_id = 1;
							}
							if($is_payment_line && ($row_id>0)) {
								$num_fields = sizeof($values);
								$payment = array();
								if($num_fields > 1 && $num_fields != $num_fields_expected) {
									echo "$line";
									exit("Line $row_id with unexpected number of fields: expected {$num_fields_expected}, got $num_fields\n");
								}
								if($row_id>2 && $num_fields>1) { //skip headers
									// print_r($values);
									$date = explode('/',trim($values[0]));
									if(count($date)!=3)
										exit("Line $row_id: Wrong format in date {$values[0]}\n");
									// ignore timezone
									$ts = mktime(12,0,0, $date[1], $date[0], $date[2]);
									$payment['pay_date'] = $db->getTodayTime($ts);
									$payment['id_account'] = $id_account;
									if (isset($values[3]) && ((int)$values[3]>0)) {
										$payment['is_in'] = 1;
										$payment['amount'] = abs((float)$values[3]);
										preg_match('/(.*) DA (.*)/',$values[5],$matches);
										if(isset($matches[2])) {
											$payment['description'] = $db->SqlQuote($matches[1]);
											$payment['name'] = $db->SqlQuote(ucwords($matches[2]));
										}
									} else {
										$payment['is_in'] = 0;
										$payment['amount'] = abs((float)$values[2]);
										$payment['description'] = $db->SqlQuote($values[5]);
										$payment['name'] = 'Poste Italiane';
									}
									$payment['notes'] = $db->SqlQuote($values[6]);
									if(!isset($payment['name'])) {
										echo $line;
										exit("Line $row_id could not determine name\n");
									}
									$tx_id = trim($values[0]) . " {$payment['is_in']} {$payment['amount']} {$values[4]} {$payment['name']}";
									print_r($payment);
									$num_payments ++;
								}
								$row_id ++;
							}
						}
						printf("Imported %s payments into account \"%s\"\n", $num_payments, $account['name']);
					} else {
						exit("Empty file $filename\n");
					}
					break;
			}
			break;
	}
}

?>
