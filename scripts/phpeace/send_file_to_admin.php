<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

$options = getopt("f:u:s:");
	
if(is_array($options) && $options['f']!="" && $options['u']>0)
{
	$filename = $options['f'];
	$id_user = (int)$options['u'];
	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager();
	if($id_user>0 && $fm->Exists($filename,true))
	{
		$fileInfo = $fm->FileInfo($filename,true);
		
		include_once(SERVER_ROOT."/../classes/mail.php");
		$m = new Mail();
		$attachments = array();

		$attachment = array();
		$attachment['id'] = "0";
		$attachment['ext'] = $fileInfo['format'];
		$attachment['type'] = $fm->ContentType($fileInfo['format']);
		$attachment['filename'] = $filename;
		$attachment['enc'] = $fm->Base64Encode($filename,true);
		$attachments[] = $attachment;
	
		$mail = new Mail();
		$subject = $options['s']!=""? $options['s'] : $filename;
		$message = "Attaching $filename";
		
		include_once(SERVER_ROOT."/../classes/user.php");
		$u = new User();
		$u->id = $id_user;
		$user = $u->UserGet();
		$mail->SendMail($user['email'],$user['name'],$subject,$message,array(),false,$attachments);		
	}
}

?>
