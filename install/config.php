<?php

/**
 * CONFIGURATION SETTINGS
 * 
 * This configuration file is set up with your PhPeace installation.
 * After that, since it's excluded from automatic updates, new configuration options may be added
 * which will not appear in this file, even if they're used in the updated source code.
 * This is not going to break any functionality, as the variables will have default values,
 * but you might want to periodically check this file with the latest version available at
 * https://github.com/peacelink/phpeace/blob/master/install/config.php
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Config
{
	/**
	 * DEBUG
	 * Show errors in PHP pages
	 * Otherwise the ErrorHandler will write errors in an error log
	 *
	 * @var boolean
	 */
	public $debug		= FALSE;
	
	/**
	 * ERROR MESSAGE
	 * Message shown to users in case of errors
	 * Default: "An error occurred and the administrator has been notified"
	 * If set to empty string, no message is shown
	 *
	 * @var string
	 */
//	public $error_message		= "";


	// ###############
	// DATABASE
	// ###############
	
	/**
	 * DATABASE CONNECTION
	 * Username, password and database to use
	 *
	 * @var array
	 */
	public $dbconf		= array('user'	=>	'dbusername',
					'password'	=>	'dbpassword',
					'database'	=>	'dbname',
					'server'	=>	'localhost');

	/**
	 * DATABASE PERSISTENT CONNECTION
	 * Default: false
	 *
	 * @var boolean
	 */
	public $db_pconn	= false;
	
	/**
	 * DELAYED INSERTS
	 * Use delayed inserts for logging activities
	 * Works with MyISAM tables only
	 * Default: true
	 *
	 * @var boolean
	 */
	public $delayed_inserts	= true;
	
	/**
	 * DATABASE charset
	 * Programmatically set the charset for every connection
	 * Useful in case the database server is using latin1
	 * Possible values: utf8, latin1
	 * Default: utf8
	 *
	 * @var string
	 */
	public $db_charset = "utf8";


	// ###############
	// ENCODING
	// ###############
	
	/**
	 * (X)HTML Encoding
	 * Encoding for all pages, admin and pub
	 * For internal XML and for XSL output as well
	 * Overrides the setting in the XSL's output element, which is therefore useless
	 * Currently only UTF-8 is supported. Other Unicode encodings may be added in the future.
	 * ISO-8859-1 is not supported any more.
	 * Default: "UTF-8"
	 *
	 * @var string
	 */
	public $encoding = "UTF-8";

	
	
	// ###############
	// PUBLIC WEBSITE
	// ###############
	
	/**
	 * PATHS
	 * Filenames for public content
	 * (ID and extension are added on the right side)
	 * Leave the slashes as specified
	 *
	 * @var array
	 */
	public $paths = array(	'article'	=>	'/articles/art_',
				'subtopic'	=>	'/indices/index_',
				'gallery'	=>	'/gallery/',
				'article_box'	=>	'/articles/box_',
				'article_doc'	=>	'/docs/',
				'image'		=>	'/images/',
				'image_orig'	=>	'/images/img_',
				'image_zoom'	=>	'/articles/iz_',
				'gallery_image'	=>	'/images/ig_',
				'graphic'	=>	'images/',
				'docs'	=>	'pdocs/');
	    
	/**
	 * VISITORS EMAIL VERIFICATION REQUIRED
	 * Force visitors to verify email before any authenticated activity
	 * Default: false
	 *
	 * @var boolean
	 */
	public $verification_required = false;

    /**
     * VISITORS VERIFICATION TTL
     * Life (in days) of visitors to activate portal account through verification email 
     * Default: 0 day (i.e. unverified visitors will not be removed from database)
     * @var integer
     */
    public $unverified_people_ttl = 0;    
	
	/**
	 * BLOCK MULTIPLE SESSION
	 * Forbid multiple authenticated sessions from different devices for the same user
	 * Default: false
	 *
	 * @var boolean
	 */
	public $block_multiple_sessions = false;

	/**
	 * LOG LOGIN
	 * Log people login
	 * Default: false
	 *
	 * @var boolean
	 */
	public $log_login = false;

	/**
	 * VISITORS LIFE
	 * Life (in days) of the cookie for visitors' persistent login
	 * Default: 60
	 *
	 * @var integer
	 */
	public $visitors_life = 60;

	/**
	 * VISITORS VERIFICATION TOKEN TTL
	 * Life (in seconds) of the email verification token sent to the visitors
	 * Default: 432000 (5 days)
	 *
	 * @var integer
	 */
	public $verify_token_ttl = 432000;
	
	/**
	 * HTACCESS
	 * Use .htaccess to control redirects and access
	 * Default: true
	 *
	 * @var boolean
	 */
	public $htaccess = true;


	// ###############
	// GRAPHICS
	// ###############
	
	/**
	 * IMAGE SIZES
	 * Default sizes (in pixels) for resized images
	 *
	 * @var array
	 */
	public $img_sizes = array('80','120','250','400');
	
	/**
	 * FORMAT
	 * Format of resized images
	 * Possible values: jpg, png
	 *
	 * @var string
	 */
	public $convert_format = "jpg";

	/**
	 * IMAGEMAGICK PATH
	 * Path to ImageMagick executable (convert)
	 * With no ending slash (i.e. /usr/bin, not /usr/bin/ )
	 * Default: empty (assuming it's in the path reachable by PHP)
	 *
	 * @var string
	 */
	public $convert_path = "";

	/**
	 * COMPRESSION
	 * Quality of resized images.
	 * For JPG, the value goes from 1 to 100, the higher the better
	 * For PNG, the value identifies the lossless compression algorytm to be used
	 * For more details, see
	 * http://www.imagemagick.org/script/command-line-options.php#quality
	 *
	 * @var integer
	 */
	public $convert_quality = 90;
	
	/**
	 * DEFAULT IMG SIZE
	 * Used when no specified
	 * Index of $img_sizes array
	 *
	 * @var integer
	 */
	public $default_img_size = 1;
	
	/**
	 * BOX SIZES
	 * Default sizes (in pixels) for article boxes
	 * If you change this, change also article.css
	 *
	 * @var array
	 */
	public $box_sizes = array('100','200','300','400');
	

	// MOVE TO MODULE CONFIGURATION

	// COVERS SIZES
	// Index of $img_sizes array
	public $covers_thumb = 0;
	public $covers_size = 2;

	// ORGS SIZES
	// Index of $img_sizes array
	public $orgs_thumb = 0;
	public $orgs_size = 2;
	public $orgs_upload = false;
	public $orgs_access_protected = false;
	public $orgs_orig = false;
	

	
	// ###############
	// ADMIN INTERFACE
	// ###############
	
	/**
	 * CONTENT-TYPE
	 * For all admin pages
	 * Possible values: html4_strict,html4_transitional,xhtml10_strict,xhtml10_transitional,xhtml11_strict
	 * Default: html4_strict
	 *
	 * @var string
	 */
	public $content_type = "html4_strict";
	
	/**
	 * RECORDS PER PAGE
	 *
	 * @var integer
	 */
	public $records_per_page	= 15;

	/**
	 * USER AUTHENTICATION MODE
	 * Choose between the internal database user table or an external LDAP server
	 * Possible values: "internal" "ldap"
	 * Default: "internal"
	 *
	 * @var string
	 */
	public $user_auth = "internal";
	
	/**
	 * LDAP settings
	 *
	 * @var array
	 */
	public $ldap = array(	'server'	=>	'ldap.domain.org',
				'username'	=>	'username',
				'password'	=>	'',
				'domain'	=>	'domain.org',
				'base'		=>	'ou=People,dc=domain,dc=org',
				'filter'		=>	'sAMAccountName=*',
				'attributes'	=>	array("mail","name","samaccountname") );				

	/**
	 * MIN PASSWORD LENGTH
	 * Minimum password length
	 * Default: 8
	 *
	 * @var integer
	 */
	public $password_length_min = 8;

	/**
	 * DISABLE AUTOCOMPLETE
	 * Disable auto-complete for login page input
	 * Default: false
	 *
	 * @var boolean
	 */
	public $disable_autocomplete = false;

	/**
	 * WARRANTY DISCLAIMER
	 * You can attach a warranty that will be displayed in the footer
	 * Default: GPL no warranty statement
	 *
	 * @var string
	 */
	public $warranty = "";

	/**
	 * NO MORE IE6
	 * Show warning to IE6 users
	 * Default: false
	 *
	 * @var boolean
	 */
	public $no_more_ie6 = false;
	
	/**
	 * SHOW QUOTE AT WELCOME
	 * Show a random quote in welcome page
	 * Default: false
	 *
	 * @var boolean
	 */
	public $welcome_quote = false;

	/**
	 * MAX LOGIN ATTEMPTS
	 * Number of failed login attemps before notifying administrators
	 * Default: 3
	 *
	 * @var integer
	 */
	public $max_login_attempts = 3;

	/**
	 * FORCE EMAIL VERIFICATION
	 * Users must verify their email address before accessing admin interface
	 * Default: false
	 *
	 * @var boolean
	 */
	public $force_email_verification = false;

	/**
	 * UPPERCASE WARNING
	 * Warn users to avoid all-uppercase articles' titles
	 * Default: false
	 *
	 * @var boolean
	 */
	public $uppercase_warning = false;

	/**
	 * HISTORY UPDATE
	 * Difference in seconds in order for an update to be considered new
	 * Default: 4 hours
	 *
	 * @var integer
	 */
	public $update_diff = 14400;

	/**
	 * LOGIN FAILURE DELAY
	 * Minimum delay (in seconds) between login attemps
	 * Default: 10
	 *
	 * @var integer
	 */
	public $login_failure_delay = 10;

	/**
	 * ARTICLE MANUAL ORDER MAX
	 * Maximum value for manual order parameter
	 * Default: 20
	 *
	 * @var integer
	 */
	public $max_vis = 20;

	/**
	 * PUBLISH STEP ARTICLES
	 * How many articles per publishing step
	 * Default: 100
	 *
	 * @var integer
	 */
	public $step_articles = 100;

	/**
	 * USERS MODE
	 * How topics are linked for admin users
	 * Possible values: "shared" "isolated"
	 * Default: "shared"
	 *
	 * @var string
	 */
	public $users_mode = "shared";

	/**
	 * SUBMIT DEFECTS
	 * Allow defects submission from admin interface
	 * Default: true
	 *
	 * @var boolean
	 */
	public $submit_defects = true;

	/**
	 * VIDEO FILES
	 *
	 * @var array
	 */
	public $video_files = array('mpg','mpeg','avi','flv','wmv','qt','mov','mp4');
	
	/**
	 * AUDIO FILES
	 *
	 * @var array
	 */
	public $audio_files = array('wav','mp3','ogg','aiff','au','mp4','m4a','wma','ra');
	
	/**
	 * IMAGE FILES
	 *
	 * @var array
	 */
	public $image_files = array('jpg','jpeg','gif','png','bmp','tif','tiff','ico');
	
	/**
	 * DOC FILES
	 *
	 * @var array
	 */
	public $doc_files = array('pdf','rtf','txt','doc','docx','xls','xlsx');
	
	/**
	 * DOC COVERS
	 * Generate JPG images of document covers
	 * Value refers to index of img_sizes array
	 * Default: -1 (off)
	 *
	 * @var integer
	 */
	public $docs_covers_size = -1;
	
	
	// ###############
	// GOOGLE SITEMAPS
	// ###############
	
	/**
	 * SITEMAP KEYWORD ID
	 * Id of keyword used to mark article for Google News Sitemap
	 * Default: 0
	 *
	 * @var integer
	 */
	public $sitemap_keyword_id = 0;
	
	
	// ###############
	// SEARCH ENGINE 
	// ###############

	/**
	 * SCHEDULER_INDEX
	 * How many resources to index per each scheduler cycle
	 * Default: 50
	 *
	 * @var integer
	 */
	public $scheduler_index = 50;
	
	/**
	 * INDEXER WEIGHTS
	 * Weight associated with different type of content words
	 * If you change this setting after installation, you have to rebuild the index
	 *
	 * @var array
	 */
	public $indexer_weights = array(	'keyword'	=> 10,
						'title'		=> 8,
						'keyword_related'	=> 2,
						'content'	=> 4,
						'notes'		=> 3 );
	
	/**
	 * TIME WEIGHT
	 * Weight associated with the timestamp of the articles
	 * It's multiplied with the aging days, and subtracted from the ranking score
	 * Default: 0
	 *
	 * @var integer
	 */
	public $time_weight = 0;
	
	/**
	 * MIN STRING LENGTH
	 * Minimum lenght of search term
	 * Default: 3
	 *
	 * @var integer
	 */
	public $min_str_length = 3;
	
	/**
	 * METAPHONE BASED SUGGESTIONS
	 * Threshold for suggestions
	 * If number of search results is less than this threshold, 
	 * metaphone-based suggestions of indexed words will be shown
	 * Default: 0 (disabled)
	 *
	 * @var integer
	 */
	public $metaphone_threshold = 0;

	/**
	 * INDEX PDF
	 * Extract and index text from PDF documents
	 * Requires pdftotext (part of xpdf package)
	 * Default: false
	 *
	 * @var boolean
	 */
	public $index_pdf = false;
	
	/**
	 * INDEX DOC
	 * Extract and index text from Microsoft Word .doc documents
	 * Requires antiword
	 * Default: false
	 *
	 * @var boolean
	 */
	public $index_doc = false;
	
	/**
	 * SEARCH QUERY STORE
	 * Store all search queries
	 * Default: false
	 *
	 * @var boolean
	 */
	public $search_query_store = false;
	
	/**
	 * NO GEO TOPIC
	 * Force geo search for portal
	 * Default: false
	 *
	 * @var boolean
	 */
	public $no_geo_topic = false;


	
	// ###############
	// SCHEDULER
	// ###############

	/**
	 * CRON TIMEOUT
	 * Timeout in seconds for each cron step
	 * Set 0 for no timeout
	 * Default: 1800
	 *
	 * @var integer
	 */
	public $cron_timeout = 1800;

	/**
	 * TOPIC PUBLISH TIMEOUT
	 * Timeout in seconds for manual publish step
	 * Default: 900
	 *
	 * @var integer
	 */
	public $topic_publish_timeout = 900;

	
	// ###############
	// MAILER
	// ###############

	/**
	 * SET EMAIL ENVELOPE SENDER
	 * If you're using sendmail, you may want to set the envelope sender address
	 * It works with Postfix too if PHP has ben compiled with the sendmail wrapper.
	 * To avoid X-Authentication-Warning, add your webserver user to 
	 * /etc/mail/trusted-users
	 * If set to true, it will bounce to the address specified in the From: header
	 * Default: false
	 *
	 * @var boolean
	 */
	public $sender_address = false;

	/**
	 * SET SPECIFIC EMAIL ENVELOPE SENDER
	 * If $sender_address is set to true, you may want to specify a 
	 * specific address for capturing bounces, different from the From: header
	 * Default: ""
	 *
	 * @var string
	 */
	public $specific_sender_address = "";

	/**
	 * BOUNCES HOST
	 * If $specific_sender_address is set, specify host to download and process the bounces
	 *
	 * @var string
	 */
	public $bounces_host = "";

	/**
	 * BOUNCES PORT
	 * Port/protocol
	 * Default: "110/pop3/notls"
	 *
	 * @var string
	 */
	public $bounces_port = "";

	/**
	 * BOUNCES ACCOUNT USERNAME
	 *
	 * @var string
	 */
	public $bounces_username = "";

	/**
	 * BOUNCES ACCOUNT PASSWORD
	 *
	 * @var string
	 */
	public $bounces_password = "";

	/**
	 * BOUNCES THRESHOLD
	 * Number of bounces allowed to send email
	 * Default: 5
	 *
	 * @var integer
	 */
	public $bounces_threshold = 5;

	/**
	 * SOFT BOUNCE SCORE
	 * Score for a soft bounce
	 * Default: 0
	 *
	 * @var integer
	 */
	public $soft_bounce_score = 0;

	/**
	 * HARD BOUNCE SCORE
	 * Score for a hard bounce
	 * Default: 1
	 *
	 * @var integer
	 */
	public $hard_bounce_score = 1;

	/**
	 * MAILJOB LIMIT
	 * Limit of email messages sent by each scheduler
	 * Default: 200
	 *
	 * @var integer
	 */
	public $mailjob_limit = 200;
	
	/**
	 * MAIL FROM
	 * When sender is not specified, prexif for all outgoing mail senders
	 * Default: "PhPeace "
	 *
	 * @var string
	 */
	public $mail_from = "PhPeace ";
	
	/**
	 * X-MAILER
	 * Add X-Mailer header ("PhPeace " + version number) 
	 * to outgoing email messages
	 * Default: true
	 *
	 * @var boolean
	 */
	public $x_mailer = true;

	/**
	 * ATTACHMENT MAX SIZE
	 * The maximum size for mailjobs' attachments (in bytes)
	 * Default: 1Mb
	 *
	 * @var integer
	 */
	public $attachment_max_size = 1048576;
	
	// ###############
	// NETWORKING
	// ###############

	/**
	 * PROXY
	 * Use a proxy for CURL connections
	 * Default: false
	 *
	 * @var boolean
	 */
	public $proxy = false;

	/**
	 * PROXY NAME
	 * Name of proxy server to be used
	 * Default: "" (empty string)
	 *
	 * @var string
	 */
	public $proxy_name = "";
	
	/**
	 * PROXY PORT
	 * Port of proxy server to be used
	 * Default: "" (empty string)
	 *
	 * @var string
	 */
	public $proxy_port = "";
	
	
	// ###############
	// MOBILE
	// ###############

	/**
	 * MOBILE SALT
	 * String used to salt passwords coming from mobile apps
	 * Default: "" (empty string)
	 *
	 * @var string
	 */
	public $mobile_salt = "";

	
	// ###############
	// LOGGING
	// ###############

	/**
	 * ERROR LOG SIZE
	 * Maximum size of the error log (in kB)
	 * Default: 20480 (20Mb)
	 *
	 * @var integer
	 */
	public $error_log_size = 20480;
	
	/**
	 * LOG ARCHIVE
	 * Copy daily logs in the archive folder
	 * Default: false
	 *
	 * @var boolean
	 */
	public $log_archive = false;

	/**
	 * SYSLOG
	 * Use local syslog facility to log warnngs and errors
	 * Default: false
	 *
	 * @var boolean
	 */
	public $syslog = false;

	/**
	 * FORM SPAM LOG
	 * Log spam attempts detected in public forms
	 * Default: false
	 *
	 * @var boolean
	 */
	public $formspam_log = false;


	// ###############
	// CACHE
	// ###############

	/**
	 * CACHING
	 * If caching is enabled (fallback to session)
	 * Possible values: "session" "apc"
	 * Default: "session"
	 *
	 * @var string
	 */
	public $caching = "session";

	/**
	 * APC CACHE TTL
	 * Time to live for cached data, in seconds
	 * Default: 0 (never expires)
	 *
	 * @var integer
	 */
	public $apc_cache_ttl = 0;
	
	/**
	 * CACHE TTL
	 * Time to live for cached data, in minutes
	 * Default: 360
	 *
	 * @var integer
	 */
	public $cache_ttl = 360;
	
	/**
	 * RSS TTL
	 * Time to live for RSS feeds, in minutes
	 * Default: 60
	 *
	 * @var integer
	 */
	public $rss_ttl = 60;


	// ###############
	// URL SHORTENING
	// ###############

	/**
	 * URL SHORTENING SERVER URL
	 * Public URL of PhPeace running USS 
	 * Used by USS clients
	 * Alternatively you can set it to bit.ly, but create your API key and configure it below
	 * Default: ""
	 *
	 * @var string
	 */
	public $uss_url = "";

	/**
	 * URL SHORTENING SERVER KEY
	 * For a PhPeace instance running the USS service as a server, 
	 * this key is used to authorize requests
	 * For any other PhPeace (i.e. clients), this key is used to request URL shortening
	 * from the server above (in $uss_url)
	 * Default: ""
	 *
	 * @var string
	 */
	public $uss_key = "";

	/**
	 * LOG URL SHORTENING HITS
	 * Count hits (used by USS server only)
	 * Default: false
	 *
	 * @var boolean
	 */
	public $uss_log_hits = false;

	/**
	 * URL SHORTENING MASK
	 * If you want to masquerade the portal hostname under a shorter one 
	 * deditated to URL shortening
	 * You also need to setup the relevant URL rewriting rule, pointing to
	 * /uss/go.php?id=
	 * This setting is applicable to USS server only
	 * Default: ""
	 *
	 * @var string
	 */
	public $uss_mask = "";
	
	
	// ###############
	// AUDIO
	// ###############

	/**
	 * AUDIO MAX SIZE
	 * The maximum size for uploaded files
	 * (remember to check your php.ini settings for post_max_size and upload_max_filesize directives)
	 * Default: 20Mb
	 *
	 * @var integer
	 */
	public $audio_max_size = 20971520;

	/**
	 * AUDIO SAMPLE RATE
	 * Default: 22050 Hz
	 *
	 * @var integer
	 */
	public $audio_sample_rate = 22050;

	/**
	 * AUDIO BITRATE
	 * Default: 96 kb/s
	 *
	 * @var integer
	 */
	public $audio_bitrate = 96;

	/**
	 * FFMPEG AUDIO OPTIONS
	 * (such as audio codec and channels)
	 * Default: "-acodec libmp3lame -ac 1 "
	 *
	 * @var string
	 */
	public $ffmpeg_audio_options = " -acodec libmp3lame -ac 1 ";

	
	// ###############
	// VIDEO
	// ###############

	/**
	 * VIDEO RESIZE WIDTH
	 * Default: 320 pixel
	 *
	 * @var integer
	 */
	public $video_resize_width = 320;

	/**
	 * VIDEO RESIZE HEIGHT
	 * Default: 240 pixel
	 *
	 * @var integer
	 */
	public $video_resize_height = 240;

	/**
	 * VIDEO LENGTH LIMIT
	 * Maximum video duration in seconds
	 * Default: 1200
	 *
	 * @var integer
	 */
	public $video_length_limit = 1200;

	/**
	 * VIDEO MAX SIZE
	 * The maximum size for uploaded files
	 * (remember to check your php.ini settings for post_max_size and upload_max_filesize directives)
	 * Default: 30Mb
	 *
	 * @var integer
	 */
	public $video_max_size = 31457280;

	/**
	 * VIDEO BITRATE
	 * Default: 200000 b/s (= 200 kbps)
	 *
	 * @var integer
	 */
	public $video_bitrate = 200000;

	/**
	 * VIDEO FRAME RATE
	 * Default: 25 fps
	 *
	 * @var integer
	 */
	public $video_frame_rate = 25;
	
	/**
	 * VIDEO AUDIO SAMPLE RATE
	 * Default: 22050 Hz
	 *
	 * @var integer
	 */
	public $video_audio_sample_rate = 22050;

	/**
	 * VIDEO AUDIO BITRATE
	 * Default: 32 kb/s
	 *
	 * @var integer
	 */
	public $video_audio_bitrate = 32;

	/**
	 * VIDEO THUMBS
	 * How many thumbs to generate per video
	 * Default: 3
	 *
	 * @var integer
	 */
	public $video_thumbs = 3;

	/**
	 * FFMPEG NICENESS
	 * Default: 19
	 *
	 * @var integer
	 */
	public $ffmpeg_niceness = 19;
	
	/**
	 * FFMPEG OPTIONS
	 * (such as format, audio codec and audio channels)
	 * Default: "-aspect 4:3 -f flv -acodec libmp3lame -ac 1 "
	 *
	 * @var string
	 */
	public $ffmpeg_options = "-aspect 4:3 -f flv -acodec libmp3lame -ac 1 ";

	/**
	 * VIDEO ENCODINGS PER CRON JOB
	 * Default: 1
	 *
	 * @var integer
	 */
	public $encodings_per_cron_job = 1;

	
	
	// ###############
	// TRACKING VISITORS
	// ###############
	
	/**
	 * TRACK KNOWN VISITORS
	 * Track known visitors (i.e. already registered and with cookie)
	 * Default: false
	 *
	 * @var boolean
	 */
	public $track = false;

	/**
	 * TRACK EXCLUDE IPs
	 * Exclude some IPs from tracking
	 * Default: array("127.0.0.1")
	 *
	 * @var array
	 */
	public $track_exclude = array("127.0.0.1");
	
	/**
	 * TRACK EXCLUDE UAs
	 * Exclude some User Agents from tracking
	 * Default: array("Googlebot","Slurp","msnbot")
	 *
	 * @var array
	 */
	public $track_exclude_ua = array("Googlebot","Slurp","msnbot");
	
	/**
	 * TRACK ALL VISITORS
	 * Impacts on performance, be sure you know what you're doing
	 * Default: false
	 *
	 * @var boolean
	 */
	public $track_all = false;


	// ###############
	// STATS
	// ###############
	
	/**
	 * AWSTATS
	 * URL to awstats.pl
	 * for example "http://username:password@localhost/awstats"
	 *
	 * @var string
	 */
	public $awstats = "";

	
	// ###############
	// WIDGETS
	// ###############
	
	/**
	 * WIDGET CACHE DEFAULT TTL 
	 * Maximun time (in seconds) a widget can be cached (for anonymous users)
	 * Default: 600
	 *
	 * @var integer
	 */
	public $widgets_cache_default_ttl = 600;

    /**
     * WIDGET LIBRARY
     * Categories that show widgets beside My Widgets
     *
     * @var string
     */
    public $widget_library_display = "";

    
	// ###############
	// WEB FEEDS
	// ###############

	/**
	 * FEED ITEMS VALIDATE PER CRON JOB
	 * How many feed items URLs to check on each scheduler cycle
	 * Default: 100
	 *
	 * @var integer
	 */
	public $feed_items_validate_per_cron_job = 100;
	
	/**
	 * MINIMUM FEED UPDATE INTERVAL
	 * TTL for RSS feeds (in minutes)
	 * Default: 30
	 *
	 * @var integer
	 */
	public $minimum_feed_update_interval = 30;

	/**
	 * FEEDS IMPORT PER CRON JOB
	 * How many feeds to import on each scheduler cycle
	 * Default: 50
	 *
	 * @var integer
	 */
	public $feeds_import_per_cron_job = 50;
	

	
	// ###############
	// UI
	// ###############
	
	/**
	 * UI
	 * Whether UI files (XSL, CSS, JavaScript, graphics) are managed via an external repo 
	 * and not by the admin interface
	 * Default: false
	 *
	 * @var boolean
	 */
	public $ui = false;
	
	
	/**
	 * CDN
	 * Hostname for DADI CDN 
	 * Default: ""
	 *
	 * @var string
	 */
	public $cdn = "";
	
	
	// ###############
	// VARIA
	// ###############
	
	/**
	 * MOD REWRITE
	 * Whether Apache module mod_rewrite and mod_proxy are available 
	 * and can be used for Friendly URLs redirection
	 * Default: false
	 *
	 * @var boolean
	 */
	public $mod_rewrite = true;
	
	/**
	 * LANDING
	 * Additional landing pages
	 * Default: empty array
	 *
	 * @var boolean
	 */
	public $landing = array();
	
	/**
	 * UPLOAD HOST
	 * Host to send upload posts in case of multiple frontends
	 * (Do not specify the protocol http://, just the host name)
	 * Default: ""
	 *
	 * @var string
	 */
	public $upload_host = "";

	/**
	 * POST INSTALL SCRIPT
	 * A script to be executed after every PhPeace update
	 * Useful for additional deployment tasks
	 * Default: ""
	 *
	 * @var string
	 */
	public $post_install_script = "";
	
	/**
	 * POST UPDATE SCRIPT
	 * A shell script to be executed after every update action
	 * (publish, upload, update)
	 * Useful to synchronize content across multiple servers
	 * Default: ""
	 *
	 * @var string
	 */
	public $post_update_script = "";
	
	/**
	 * USER AGENT
	 * User Agent used for network activities (download, browse, post)
	 * Default: PhPeace {version} bot
	 *
	 * @var string
	 */
	public $user_agent = "";
	
	/**
	 * SOAP CLIENT TIMEOUT
	 * Timeout in seconds for connection to SOAP services
	 * Default: 5
	 *
	 * @var integer
	 */
	public $soap_timeout = 5;

	/**
	 * SOAP CLIENT TIMEOUT SHORT
	 * A shorter timeout in seconds for connection to SOAP services
	 * Default: 5
	 *
	 * @var integer
	 */
	public $soap_timeout_short = 1;

	/**
	 * BLACKDIRS
	 * Blacklist of directories to be kept safe
	 * (i.e. not to be used for topics or other purposes)
	 * Default: array()
	 *
	 * @var array
	 */
	public $blackdirs = array();
    
	/**
	 * DR SITE
	 * Whether this installation is a disaster recovery site
	 * Useful to block interactive actions and show warnings in the UI
	 * Default: false
	 *
	 * @var boolean
	 */
	public $dr_site = false;


	// ###############
	// OTHERS
	// ###############

	/**
	 * HIGHSLIDE
	 * Use Highslide JS for images zooming and galleries
	 * Default: false
	 *
	 * @var boolean
	 */
	public $highslide = false;
	
	/**
     * GOOGLE API KEY
     *
     * @var string
     */
    public $google_api_key = ""; 

	/**
     * GOOGLE OAUTH CONSUMER KEY
     * For Google APIs
     * Get yours here: http://code.google.com/apis/accounts/docs/RegistrationForWebAppsAuto.html
     * Default: ""
     *
     * @var string
     */
    public $google_oauth_consumer_key = ""; 

    /**
     * GOOGLE OAUTH CONSUMER SECRET
     * For Google APIs
     * Default: ""
     *
     * @var string
     */
    public $google_oauth_consumer_secret = ""; 
    
    /**
     * FACEBOOK APP ID
     * Default: ""
     *
     * @var string
     */
    public $fb_app_id = ""; 
    
    /**
     * TELEGRAM BOT API KEY
     * Default: ""
     *
     * @var string
     */
    public $telegram_bot_api_key = "";
    
    /**
     * ROCKETCHAT WEBHOOK
     * For notifications
     * Default: ""
     *
     * @var string
     */
    public $rocketchat_webhook = "";
    
    /**
	 * RECAPTCHA
	 * Default: false
	 *
	 * @var boolean
	 */
	public $recaptcha = false;
    
	/**
	 * RECAPTCHA VERSION
	 *
	 * @var string
	 */
	public $recaptcha_version = "2";
	
	/**
	 * RECAPTCHA PUBLIC KEY
	 *
	 * @var string
	 */
	public $recaptcha_public_key = "";

	/**
	 * RECAPTCHA PRIVATE KEY
	 *
	 * @var string
	 */
	public $recaptcha_private_key = "";

    /**
     * BIT.LY LOGIN
     * For using bit.ly as URL Shortening
     * Default: ""
     *
     * @var string
     */
    public $bitly_login = "";

    /**
     * BIT.LY API KEY
     * Create yours http://bit.ly/a/your_api_key/
     * Default: ""
     *
     * @var string
     */
    public $bitly_key = "";
    
	/**
	 * STOPFORUMSPAM
	 * Use stopforumspam.com against forum spam
	 *
	 * @var boolean
	 */
	public $stopforumspam = false;
    
    function __construct()
	{
	}

}
?>
