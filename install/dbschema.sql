/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id_account` smallint(3) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `id_type` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `name` varchar(150) DEFAULT NULL,
  `params` text DEFAULT NULL,
  `shared` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_account`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_use` (
  `id_use` smallint(3) NOT NULL DEFAULT 0,
  `type` tinyint(1) DEFAULT 0,
  `id_item` smallint(3) DEFAULT 0,
  `id_account` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_use`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_types` (
  `id_address_type` tinyint(2) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT '',
  PRIMARY KEY (`id_address_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id_address` int(11) NOT NULL DEFAULT 0,
  `id_smart_card` int(11) DEFAULT 0,
  `id_address_type` tinyint(2) DEFAULT 0,
  `address_label_1` varchar(50) DEFAULT '',
  `address_label_2` varchar(50) DEFAULT '',
  `address_label_3` varchar(50) DEFAULT '',
  `address_label_4` varchar(50) DEFAULT '',
  `address_label_5` varchar(50) DEFAULT '',
  `postcode` varchar(50) DEFAULT '',
  `id_state` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id_address`),
  KEY `id_smart_card` (`id_smart_card`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_text` (
  `id_text` tinyint(2) NOT NULL DEFAULT 0,
  `title` varchar(200) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_text`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_center` (
  `id_center` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(60) DEFAULT NULL,
  `id_province` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_center`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_company` (
  `id_company` smallint(6) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sector` varchar(255) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `town` varchar(30) DEFAULT NULL,
  `id_province` tinyint(4) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_company`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_company_questions` (
  `id_question` smallint(6) NOT NULL DEFAULT 0,
  `id_company` smallint(6) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `employees` tinyint(1) DEFAULT NULL,
  `employees_f` tinyint(2) DEFAULT NULL,
  `employees_r` tinyint(2) DEFAULT NULL,
  `profiles` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `education` tinyint(1) DEFAULT NULL,
  `language` tinyint(1) DEFAULT NULL,
  `contract` tinyint(1) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `employees_rf` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id_question`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person` (
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `id_center` smallint(4) DEFAULT NULL,
  `id_user` smallint(4) DEFAULT NULL,
  `insert_date` date DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `name2` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `address_1` varchar(200) DEFAULT NULL,
  `zipcode_1` varchar(15) DEFAULT NULL,
  `town_1` varchar(30) DEFAULT NULL,
  `id_province_1` tinyint(4) DEFAULT NULL,
  `address_2` varchar(200) DEFAULT NULL,
  `zipcode_2` varchar(15) DEFAULT NULL,
  `town_2` varchar(30) DEFAULT NULL,
  `id_province_2` tinyint(4) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `taxcode` varchar(20) DEFAULT NULL,
  `passport` varchar(20) DEFAULT NULL,
  `passport_date` date DEFAULT NULL,
  `resident_permit_type` varchar(20) DEFAULT NULL,
  `resident_permit_expire_date` date DEFAULT NULL,
  `health_card` tinyint(1) DEFAULT 0,
  `driving_licence` tinyint(1) DEFAULT 0,
  `driving_licence_type` varchar(5) DEFAULT NULL,
  `family` tinyint(2) DEFAULT 3,
  `status` tinyint(1) DEFAULT 1,
  `marriage_day` date DEFAULT NULL,
  `gather_family` tinyint(1) DEFAULT 0,
  `sons` tinyint(2) DEFAULT 0,
  `relatives` varchar(255) DEFAULT NULL,
  `disability` varchar(200) DEFAULT NULL,
  `change_job` tinyint(1) DEFAULT 1,
  `shifts` varchar(10) DEFAULT NULL,
  `commute` tinyint(1) DEFAULT NULL,
  `transport` varchar(10) DEFAULT NULL,
  `languages` varchar(100) DEFAULT NULL,
  `italian` tinyint(1) DEFAULT NULL,
  `computer` tinyint(1) DEFAULT NULL,
  `edu_notes` varchar(255) DEFAULT NULL,
  `change_home` tinyint(1) DEFAULT 0,
  `change_home_reason` varchar(200) DEFAULT NULL,
  `home_area` varchar(200) DEFAULT NULL,
  `home_max_rent` smallint(6) DEFAULT 0,
  `home_type` varchar(10) DEFAULT '0',
  `home_notes` text DEFAULT NULL,
  `salary` tinyint(1) DEFAULT 1,
  `bank_account` tinyint(1) DEFAULT 0,
  `home_support` tinyint(1) DEFAULT 1,
  `religion` varchar(200) DEFAULT NULL,
  `emi_reason` varchar(10) DEFAULT '0',
  `town_reason` varchar(200) DEFAULT NULL,
  `how_much_support` smallint(5) DEFAULT NULL,
  `varia_notes` text DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `employed` tinyint(1) DEFAULT 0,
  `skills` text DEFAULT NULL,
  `tskills` text DEFAULT NULL,
  `aims` text DEFAULT NULL,
  `job_notes` text DEFAULT NULL,
  `software` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_person`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_edus` (
  `id_edu` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `description` varchar(250) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT 0,
  `length` smallint(4) DEFAULT 0,
  PRIMARY KEY (`id_edu`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_homes` (
  `id_home` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `period` varchar(100) DEFAULT NULL,
  `place` varchar(200) DEFAULT NULL,
  `type` tinyint(1) DEFAULT 1,
  `contract` tinyint(1) DEFAULT 1,
  `contact` tinyint(1) DEFAULT 1,
  `left_reason` tinyint(1) DEFAULT 1,
  `home_notes` text DEFAULT NULL,
  PRIMARY KEY (`id_home`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_inout` (
  `id_inout` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `start_date` varchar(20) DEFAULT NULL,
  `reason` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_inout`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_jobs` (
  `id_job` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `period` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `country` tinyint(1) DEFAULT 1,
  `contract` tinyint(1) DEFAULT 1,
  `employer` tinyint(1) DEFAULT 1,
  `contact` tinyint(1) DEFAULT 1,
  `left_reason` tinyint(1) DEFAULT 1,
  `parttime` tinyint(1) DEFAULT 2,
  `job_notes` text DEFAULT NULL,
  PRIMARY KEY (`id_job`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_registrations` (
  `id_register` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `start_date` varchar(20) DEFAULT NULL,
  `type` varchar(60) DEFAULT NULL,
  `length` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id_register`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_relatives` (
  `id_relative` smallint(3) NOT NULL DEFAULT 0,
  `id_person_rel` smallint(6) NOT NULL DEFAULT 0,
  `relation` varchar(12) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `yearborn` smallint(4) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `study` varchar(30) DEFAULT NULL,
  `job` varchar(50) DEFAULT NULL,
  `id_person` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id_relative`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_services` (
  `id_service` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `info` varchar(10) DEFAULT NULL,
  `request` varchar(10) DEFAULT NULL,
  `job_search` varchar(20) DEFAULT NULL,
  `home_search` varchar(20) DEFAULT NULL,
  `support` varchar(15) DEFAULT NULL,
  `support_description` text DEFAULT NULL,
  `result` varchar(10) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id_service`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ali_person_socs` (
  `id_soc` smallint(6) NOT NULL DEFAULT 0,
  `id_person` smallint(6) NOT NULL DEFAULT 0,
  `period` varchar(100) DEFAULT NULL,
  `service` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_soc`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_user` smallint(4) DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT NULL,
  `id_subtopic` smallint(5) DEFAULT NULL,
  `halftitle` varchar(255) DEFAULT NULL,
  `headline` varchar(255) DEFAULT NULL,
  `headline_visible` tinyint(1) DEFAULT 1,
  `source` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `written` date DEFAULT NULL,
  `is_html` tinyint(1) DEFAULT 0,
  `approved` tinyint(1) DEFAULT 0,
  `id_approver` smallint(4) DEFAULT NULL,
  `published` datetime DEFAULT NULL,
  `show_date` tinyint(1) DEFAULT 0,
  `align` tinyint(2) DEFAULT 0,
  `id_visibility` tinyint(2) NOT NULL DEFAULT 5,
  `show_author` tinyint(1) DEFAULT 1,
  `original` date DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `exported` datetime DEFAULT NULL,
  `available` tinyint(1) DEFAULT 1,
  `show_source` tinyint(1) DEFAULT 0,
  `show_latest` tinyint(1) DEFAULT 1,
  `author_notes` varchar(255) DEFAULT NULL,
  `id_licence` tinyint(2) DEFAULT 0,
  `allow_comments` tinyint(1) DEFAULT 0,
  `id_image` mediumint(8) DEFAULT 0,
  `image_size` smallint(3) DEFAULT 0,
  `image_align` tinyint(1) DEFAULT 1,
  `image_width` smallint(5) DEFAULT 0,
  `image_height` smallint(5) DEFAULT 0,
  `jump_to_source` tinyint(1) NOT NULL DEFAULT 0,
  `id_template` smallint(3) NOT NULL DEFAULT 0,
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_article`),
  KEY `id_topic` (`id_topic`),
  KEY `id_subtopic` (`id_subtopic`),
  KEY `written` (`written`),
  KEY `id_user` (`id_user`),
  KEY `published` (`published`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_boxes` (
  `id_box` smallint(6) NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `popup` tinyint(1) DEFAULT 0,
  `width` smallint(3) DEFAULT 1,
  `align` tinyint(1) DEFAULT 0,
  `z_width` smallint(4) DEFAULT 400,
  `z_height` smallint(4) DEFAULT 400,
  PRIMARY KEY (`id_box`,`id_article`),
  KEY `id_article` (`id_article`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_boxes_types` (
  `id_type` smallint(6) NOT NULL,
  `name` varchar(150) DEFAULT '',
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_content` (
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `content` mediumtext DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id_article`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_related` (
  `id_article1` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_article2` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `with_content` tinyint(1) NOT NULL DEFAULT 0,
  KEY `id_article1` (`id_article1`),
  KEY `id_article2` (`id_article2`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_subhead` (
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `subhead` text DEFAULT NULL,
  PRIMARY KEY (`id_article`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass` (
  `id_ass` smallint(5) NOT NULL DEFAULT 0,
  `nome` varchar(255) DEFAULT NULL,
  `nome2` varchar(255) DEFAULT NULL,
  `indirizzo` varchar(255) DEFAULT NULL,
  `citta` varchar(255) DEFAULT NULL,
  `id_prov` tinyint(3) DEFAULT NULL,
  `cap` varchar(10) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `sito` varchar(150) DEFAULT NULL,
  `ccp` varchar(255) DEFAULT NULL,
  `note` text NOT NULL,
  `referente` varchar(200) DEFAULT NULL,
  `pubblicaz` varchar(255) DEFAULT NULL,
  `fonte` varchar(255) DEFAULT NULL,
  `id_tipo` tinyint(2) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `lastupd` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `kparams` text DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `admin_notes` varchar(1000) NOT NULL DEFAULT '',
  `sorting` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_ass`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass_tipo` (
  `id_ass_tipo` tinyint(2) NOT NULL DEFAULT 0,
  `ass_tipo` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_ass_tipo`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audios` (
  `id_audio` mediumint(9) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `format` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(255) NOT NULL DEFAULT '',
  `bytes` int(10) NOT NULL DEFAULT 0,
  `bytes_enc` int(10) NOT NULL DEFAULT 0,
  `length` smallint(6) NOT NULL DEFAULT 0,
  `id_licence` tinyint(2) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `encoded` tinyint(1) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `hash2` varchar(64) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `views` int(10) DEFAULT 0,
  `id_language` tinyint(2) DEFAULT 1,
  `auto_start` tinyint(1) DEFAULT 0,
  `download` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_audio`),
  UNIQUE KEY `hash` (`hash`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id_banner` smallint(4) NOT NULL DEFAULT 0,
  `alt_text` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `can_expire` tinyint(1) DEFAULT 0,
  `start_date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `id_group` smallint(3) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `popup` tinyint(1) DEFAULT 1,
  `format` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_banner`),
  KEY `id_group` (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners_groups` (
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(150) DEFAULT NULL,
  `width` smallint(3) DEFAULT NULL,
  `height` smallint(3) DEFAULT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_freqs` (
  `id_billing_freq` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_billing_freq`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id_book` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `title` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `isbn` varchar(30) DEFAULT NULL,
  `id_publisher` smallint(4) DEFAULT NULL,
  `p_year` smallint(4) DEFAULT NULL,
  `price` decimal(5,2) DEFAULT 0.00,
  `id_language` tinyint(2) DEFAULT 1,
  `notes` text DEFAULT NULL,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `approved` tinyint(1) DEFAULT NULL,
  `cover_format` varchar(5) DEFAULT NULL,
  `series` varchar(200) DEFAULT NULL,
  `pages` smallint(4) DEFAULT NULL,
  `catalog` varchar(100) DEFAULT NULL,
  `id_category` smallint(3) DEFAULT 0,
  `p_month` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id_book`),
  KEY `id_publisher` (`id_publisher`),
  KEY `id_category` (`id_category`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_categories` (
  `id_category` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `id_image` mediumint(8) DEFAULT NULL,
  `id_publisher` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  KEY `id_publisher` (`id_publisher`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_config` (
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `books_home_type` tinyint(1) NOT NULL DEFAULT 1,
  `books_reviews` tinyint(1) DEFAULT 0,
  `show_reviews` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_home` (
  `id_item` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_item`,`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boxes` (
  `id_box` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(200) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `is_html` tinyint(1) DEFAULT 0,
  `show_title` tinyint(1) DEFAULT 1,
  `notes` varchar(200) NOT NULL DEFAULT '',
  `id_type` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_box`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_lists` (
  `channel_no` varchar(3) NOT NULL,
  `name` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`channel_no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clicks` (
  `id_click` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `referer` varchar(100) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `id_banner` smallint(4) DEFAULT NULL,
  `click_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_click`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id_client` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `install_key` varchar(100) DEFAULT NULL,
  `latest_version` smallint(6) DEFAULT NULL,
  `last_contact` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `approved` tinyint(1) DEFAULT 0,
  `admin_web` varchar(50) DEFAULT NULL,
  `modules` text DEFAULT NULL,
  `modules_update` tinyint(1) DEFAULT 0,
  `php_version` varchar(30) DEFAULT '',
  PRIMARY KEY (`id_client`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id_comment` smallint(6) NOT NULL DEFAULT 0,
  `id_parent` smallint(6) DEFAULT 0,
  `id_item` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_type` tinyint(2) NOT NULL DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `params` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_comment`),
  KEY `id_item` (`id_type`,`id_item`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_content` (
  `id_comment` smallint(6) NOT NULL DEFAULT 0,
  `text` text DEFAULT NULL,
  PRIMARY KEY (`id_comment`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_texts` (
  `id_content` smallint(6) NOT NULL DEFAULT 0,
  `description` varchar(200) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT 0,
  `id_type` tinyint(2) DEFAULT 0,
  `is_html` tinyint(1) NOT NULL DEFAULT 0,
  `wysiwyg` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_content`),
  KEY `id_topic` (`id_topic`),
  KEY `id_type` (`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contributions` (
  `id_resource_type` tinyint(2) NOT NULL DEFAULT 0,
  `id_resource` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `notify` tinyint(1) DEFAULT 0,
  `ip` varchar(20) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_resource_type`,`id_resource`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron` (
  `hour` tinyint(2) NOT NULL DEFAULT 0,
  `minute` tinyint(2) NOT NULL DEFAULT 0,
  `id_scheduler` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_scheduler`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css` (
  `id_css` smallint(4) NOT NULL DEFAULT 0,
  `id_type` tinyint(6) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `css` text DEFAULT NULL,
  PRIMARY KEY (`id_css`),
  KEY `id_style` (`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_custom` (
  `id_css` smallint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `css` text DEFAULT NULL,
  PRIMARY KEY (`id_css`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_ext` (
  `id_css` smallint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `css` text DEFAULT NULL,
  PRIMARY KEY (`id_css`,`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_global` (
  `id_css` tinyint(1) NOT NULL,
  `css` text DEFAULT NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_history` (
  `type` varchar(12) NOT NULL DEFAULT '',
  `id_css` smallint(4) NOT NULL DEFAULT 0,
  `id_pagetype` smallint(3) NOT NULL DEFAULT 0,
  `id_style` smallint(3) NOT NULL DEFAULT 0,
  `revision` smallint(6) NOT NULL DEFAULT 0,
  `change_time` datetime DEFAULT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `content` text NOT NULL,
  `tag` varchar(50) DEFAULT '',
  PRIMARY KEY (`revision`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_module` (
  `id_css` smallint(4) NOT NULL DEFAULT 0,
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  `id_style` smallint(4) DEFAULT 0,
  `css` text DEFAULT NULL,
  PRIMARY KEY (`id_css`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs` (
  `id_doc` smallint(6) NOT NULL DEFAULT 0,
  `filename` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `id_licence` tinyint(2) DEFAULT 0,
  `format` varchar(5) DEFAULT NULL,
  `author` varchar(250) DEFAULT '',
  `source` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_doc`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_articles` (
  `id_doc` smallint(6) NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `seq` tinyint(2) NOT NULL DEFAULT 0,
  `id_subtopic_form` smallint(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_doc`,`id_article`),
  KEY `id_article` (`id_article`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_orgs` (
  `id_org` smallint(5) NOT NULL DEFAULT 0,
  `id_doc` smallint(6) NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_tokens` (
  `id_doc` smallint(6) NOT NULL DEFAULT 0,
  `token` varchar(32) NOT NULL DEFAULT '',
  `expires` tinyint(1) NOT NULL DEFAULT 0,
  `expire_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `max_downloads` smallint(3) NOT NULL DEFAULT 0,
  `counter` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`token`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebooks` (
  `id_ebook` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_type` tinyint(1) NOT NULL DEFAULT 0,
  `id_book` mediumint(6) NOT NULL DEFAULT 0,
  `params` varchar(500) NOT NULL DEFAULT '',
  `rights` varchar(100) NOT NULL DEFAULT '',
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_ebook`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eire_counties` (
  `id_county` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_county`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_audio` (
  `id_audio` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_audio`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_channel_types` (
  `id_channel_type` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_channel_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_classifications` (
  `id_classification` int(10) unsigned NOT NULL,
  `shortname` varchar(3) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_classification`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_event_synopsis` (
  `id_event` int(10) unsigned NOT NULL,
  `synopsis` text NOT NULL,
  PRIMARY KEY (`id_event`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_event_warnings` (
  `id_event` int(10) unsigned NOT NULL DEFAULT 0,
  `id_warning` int(10) unsigned NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_languages` (
  `id_language` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_language`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_programs_articles` (
  `id_program` int(10) unsigned NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `with_content` tinyint(1) NOT NULL DEFAULT 0,
  `updated_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  KEY `id_program` (`id_program`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_region_services` (
  `id_region` int(10) unsigned NOT NULL DEFAULT 0,
  `id_service` int(10) unsigned NOT NULL DEFAULT 0,
  `channel_number` int(10) unsigned NOT NULL DEFAULT 0,
  `id_channel_type` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_region`,`id_service`),
  KEY `channel_number` (`channel_number`),
  KEY `id_service` (`id_service`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_schedule` (
  `id_event` int(10) unsigned NOT NULL DEFAULT 0,
  `id_service` int(10) unsigned NOT NULL DEFAULT 0,
  `event_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `title` varchar(200) NOT NULL DEFAULT '',
  `is_movie` tinyint(1) NOT NULL DEFAULT 0,
  `is_series` tinyint(1) NOT NULL DEFAULT 0,
  `is_closed_captions` tinyint(1) NOT NULL DEFAULT 0,
  `id_classification` int(10) unsigned NOT NULL DEFAULT 0,
  `warnings` varchar(20) NOT NULL DEFAULT '',
  `year_production` smallint(4) unsigned NOT NULL DEFAULT 0,
  `id_program` int(10) unsigned NOT NULL DEFAULT 0,
  `episode_title` varchar(200) NOT NULL DEFAULT '',
  `preamble_title` varchar(200) NOT NULL DEFAULT '',
  `id_genre` int(10) unsigned NOT NULL DEFAULT 0,
  `id_sub_genre` int(10) unsigned NOT NULL DEFAULT 0,
  `director` varchar(100) NOT NULL DEFAULT '',
  `main_cast` varchar(200) NOT NULL DEFAULT '',
  `is_premiere` tinyint(1) NOT NULL DEFAULT 0,
  `is_repeat` tinyint(1) NOT NULL DEFAULT 0,
  `is_final` tinyint(1) NOT NULL DEFAULT 0,
  `is_series_return` tinyint(1) NOT NULL DEFAULT 0,
  `is_live` tinyint(1) NOT NULL DEFAULT 0,
  `is_high_definition` tinyint(1) NOT NULL DEFAULT 0,
  `is_widescreen` tinyint(1) NOT NULL DEFAULT 0,
  `is_colour` tinyint(1) NOT NULL DEFAULT 0,
  `has_subtitles` tinyint(1) NOT NULL DEFAULT 0,
  `id_audio` int(10) unsigned NOT NULL DEFAULT 0,
  `country` varchar(50) NOT NULL DEFAULT '',
  `id_language` int(10) unsigned NOT NULL DEFAULT 0,
  `duration` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_event`),
  KEY `id_service` (`id_service`),
  KEY `event_date` (`event_date`),
  KEY `id_program` (`id_program`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_services` (
  `id_service` int(10) unsigned NOT NULL,
  `channel_name` varchar(55) NOT NULL DEFAULT '',
  `channel_short_name` varchar(25) NOT NULL DEFAULT '',
  `callsign` varchar(20) NOT NULL DEFAULT '',
  `can_record` tinyint(1) NOT NULL DEFAULT 0,
  `si_service` smallint(6) NOT NULL DEFAULT 0,
  `active_start_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `active_end_date` datetime DEFAULT NULL,
  `id_language` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_service`),
  KEY `active_start_date` (`active_start_date`),
  KEY `active_end_date` (`active_end_date`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_services_articles` (
  `id_service` int(10) unsigned NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `with_content` tinyint(1) NOT NULL DEFAULT 0,
  `updated_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  KEY `id_service` (`id_service`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_services_galleries` (
  `id_service` int(10) unsigned NOT NULL DEFAULT 0,
  `id_gallery` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_service`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_warnings` (
  `id_warning` int(10) unsigned NOT NULL,
  `shortname` varchar(3) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_warning`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_types` (
  `id_event_type` tinyint(3) NOT NULL DEFAULT 0,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_event_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id_event` smallint(6) NOT NULL DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `length` tinyint(2) DEFAULT 1,
  `place` varchar(255) DEFAULT NULL,
  `place_details` text DEFAULT NULL,
  `id_geo` smallint(3) DEFAULT NULL,
  `id_event_type` tinyint(3) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `allday` tinyint(1) DEFAULT 0,
  `id_topic` smallint(4) DEFAULT 0,
  `end_date` date DEFAULT NULL,
  `jump_to_article` tinyint(1) DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_group` smallint(4) DEFAULT 0,
  `portal` tinyint(1) DEFAULT 1,
  `has_image` tinyint(1) NOT NULL DEFAULT 0,
  `is_html` tinyint(1) NOT NULL DEFAULT 0,
  `facebook_id` varchar(80) NOT NULL DEFAULT '',
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `address` varchar(300) NOT NULL DEFAULT '',
  `image_ratio` decimal(10,8) DEFAULT NULL,
  PRIMARY KEY (`id_event`),
  KEY `id_topic` (`id_topic`),
  KEY `start_date` (`start_date`),
  KEY `id_province` (`id_geo`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `id_feature` smallint(6) NOT NULL,
  `id_function` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `params` varchar(1000) DEFAULT NULL,
  `id_user` smallint(4) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `condition_var` varchar(32) NOT NULL DEFAULT '',
  `condition_type` tinyint(2) NOT NULL DEFAULT 0,
  `condition_value` varchar(32) NOT NULL DEFAULT '',
  `condition_id` mediumint(6) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_feature`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds` (
  `id_feed` smallint(3) NOT NULL,
  `url` varchar(200) NOT NULL,
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_feed`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds_groups` (
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `group_type` tinyint(1) NOT NULL DEFAULT 1,
  `items` tinyint(2) unsigned NOT NULL DEFAULT 15,
  `ttl` smallint(3) unsigned NOT NULL DEFAULT 60,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `show_title` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_actions` (
  `id_action` smallint(3) NOT NULL,
  `id_form` smallint(3) NOT NULL DEFAULT 0,
  `score_min` smallint(3) NOT NULL DEFAULT 0,
  `score_max` smallint(3) NOT NULL DEFAULT 0,
  `redirect` varchar(200) NOT NULL DEFAULT '',
  `recipient` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_action`),
  KEY `id_form` (`id_form`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_posts` (
  `id_post` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_form` smallint(3) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `post` text NOT NULL,
  `ext` varchar(10) NOT NULL DEFAULT '',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `post_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `id_payment` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `score` smallint(3) NOT NULL DEFAULT 0,
  `post_time_milisecond` int(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_post`),
  KEY `id_form` (`id_form`,`id_topic`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `id_form` smallint(3) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `multilanguage` tinyint(1) DEFAULT 0,
  `profiling` tinyint(1) DEFAULT 0,
  `params` mediumtext NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `id_recipient` smallint(4) DEFAULT 0,
  `recipient` varchar(255) DEFAULT '',
  `privacy_warning` tinyint(1) DEFAULT 0,
  `id_payment_type` smallint(4) NOT NULL DEFAULT 0,
  `amount` varchar(255) NOT NULL DEFAULT '',
  `editable` tinyint(1) NOT NULL DEFAULT 0,
  `thanks` varchar(500) NOT NULL DEFAULT '',
  `id_pt_group` smallint(3) NOT NULL DEFAULT 0,
  `store` tinyint(1) NOT NULL DEFAULT 0,
  `captcha` tinyint(1) NOT NULL DEFAULT 0,
  `require_auth` tinyint(1) NOT NULL DEFAULT 3,
  `thanks_email` varchar(2000) NOT NULL DEFAULT '',
  `redirect` varchar(200) NOT NULL DEFAULT '',
  `weights` tinyint(1) NOT NULL DEFAULT 0,
  `hide_empty_fields` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_form`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id_friend` smallint(6) NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_friend`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id_gallery` smallint(4) NOT NULL DEFAULT 0,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `released` date DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 0,
  `show_orig` tinyint(1) DEFAULT 0,
  `id_user` smallint(4) DEFAULT NULL,
  `sort_order` tinyint(1) DEFAULT 0,
  `show_author` tinyint(1) DEFAULT 0,
  `show_date` tinyint(1) DEFAULT 0,
  `note` text DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT NULL,
  `id_group` smallint(4) DEFAULT 0,
  `params` varchar(500) DEFAULT NULL,
  `associated_image` tinyint(1) DEFAULT 4,
  `author` varchar(100) DEFAULT '',
  `images_per_page` tinyint(2) DEFAULT 15,
  `id_licence` tinyint(2) DEFAULT 0,
  `share_images` tinyint(1) DEFAULT 1,
  `id_topic_group` smallint(4) DEFAULT 0,
  `topic_admin` tinyint(1) NOT NULL DEFAULT 0,
  `captions_html` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_gallery`),
  KEY `id_topic` (`id_topic`),
  KEY `id_group` (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_groups` (
  `id_group` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_parent` smallint(4) DEFAULT NULL,
  `seq` tinyint(1) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_home` (
  `id_gallery` smallint(4) NOT NULL,
  PRIMARY KEY (`id_gallery`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_countries` (
  `id_country` smallint(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `id_continent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_country`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global` (
  `lastnewsletter` date NOT NULL DEFAULT '2003-01-01',
  `id_install` tinyint(1) NOT NULL DEFAULT 1,
  `install_key` varchar(100) DEFAULT '',
  `id_language` tinyint(1) DEFAULT 1,
  `init_year` smallint(4) DEFAULT 1990,
  `map_path` varchar(30) DEFAULT NULL,
  `search_path` varchar(30) DEFAULT NULL,
  `org_path` varchar(30) DEFAULT NULL,
  `lists_path` varchar(30) DEFAULT NULL,
  `quotes_path` varchar(30) DEFAULT NULL,
  `gallery_path` varchar(30) DEFAULT NULL,
  `events_path` varchar(30) DEFAULT NULL,
  `campaign_path` varchar(30) DEFAULT NULL,
  `forum_path` varchar(30) DEFAULT NULL,
  `poll_path` varchar(30) DEFAULT NULL,
  `pub_web` varchar(50) DEFAULT NULL,
  `admin_web` varchar(50) DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `latest` tinyint(2) DEFAULT 15,
  `temp_id_topic` smallint(4) DEFAULT NULL,
  `default_list` smallint(4) DEFAULT NULL,
  `default_visibility` tinyint(2) DEFAULT 5,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `events_title` varchar(100) DEFAULT NULL,
  `topic_store` varchar(100) DEFAULT NULL,
  `debug_mail` tinyint(1) DEFAULT 0,
  `main` tinyint(1) DEFAULT 0,
  `phpeace_server` varchar(50) NOT NULL DEFAULT 'https://updates.phpeace.org',
  `log_banners` tinyint(1) DEFAULT 1,
  `mail_output` varchar(255) DEFAULT 'pub/mail.txt',
  `max_file_size` int(8) DEFAULT 2000000,
  `scheduler_ip` varchar(50) DEFAULT NULL,
  `crypt_password` varchar(200) DEFAULT '',
  `at_work` tinyint(1) DEFAULT 0,
  `install_date` date DEFAULT NULL,
  `users_path` varchar(30) DEFAULT NULL,
  `books_path` varchar(30) DEFAULT NULL,
  `licences` tinyint(1) DEFAULT 1,
  `geo_location` tinyint(2) DEFAULT 1,
  `events_insert` tinyint(1) NOT NULL DEFAULT 1,
  `sched_notify` tinyint(1) DEFAULT 1,
  `registered` tinyint(1) DEFAULT 0,
  `last_cron` datetime DEFAULT NULL,
  `feed_type` tinyint(1) DEFAULT 2,
  `db_version` smallint(4) DEFAULT 0,
  `profiling` tinyint(1) NOT NULL DEFAULT 1,
  `search_events` tinyint(1) DEFAULT 1,
  `id_licence` tinyint(2) DEFAULT 0,
  `default_currency` tinyint(2) DEFAULT 2,
  `search_config` varchar(100) NOT NULL DEFAULT '',
  `map_sort_by` tinyint(1) NOT NULL DEFAULT 0,
  `user_show` tinyint(1) NOT NULL DEFAULT 0,
  `media_path` varchar(30) NOT NULL DEFAULT '',
  `log_level` tinyint(1) DEFAULT 0,
  `script_version` smallint(4) NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_features` (
  `id_feature` smallint(6) NOT NULL,
  `id_type` tinyint(2) NOT NULL,
  KEY `id_type` (`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graphics` (
  `id_graphic` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `width` varchar(10) DEFAULT NULL,
  `height` varchar(10) DEFAULT NULL,
  `format` varchar(5) DEFAULT NULL,
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `exclude_lookup` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_graphic`),
  KEY `id_style` (`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id_history` mediumint(7) NOT NULL,
  `id_type` tinyint(2) NOT NULL,
  `id` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_user` smallint(6) NOT NULL,
  `time` datetime NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `action` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_history`),
  KEY `id_type` (`id_type`),
  KEY `id` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `content` text DEFAULT NULL,
  `id_home` tinyint(1) NOT NULL DEFAULT 1
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepage_countries` (
  `id_topic` smallint(4) unsigned NOT NULL DEFAULT 0,
  `id_country` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_country`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identity_types` (
  `id_identity_type` tinyint(2) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT '',
  PRIMARY KEY (`id_identity_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id_image` mediumint(8) NOT NULL DEFAULT 0,
  `filename` varchar(255) DEFAULT NULL,
  `source` varchar(200) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `id_licence` tinyint(2) DEFAULT 0,
  `format` varchar(5) DEFAULT NULL,
  `caption` varchar(2000) NOT NULL DEFAULT '',
  `image_date` date DEFAULT NULL,
  `width` smallint(5) DEFAULT 0,
  `height` smallint(5) DEFAULT 0,
  PRIMARY KEY (`id_image`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_articles` (
  `id_image` mediumint(8) NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `align` tinyint(1) DEFAULT NULL,
  `size` smallint(3) DEFAULT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `zoom` tinyint(1) DEFAULT 0,
  `width` smallint(5) DEFAULT 0,
  `height` smallint(5) DEFAULT 0,
  PRIMARY KEY (`id_image`,`id_article`),
  KEY `id_article` (`id_article`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_galleries` (
  `id_image` mediumint(8) NOT NULL DEFAULT 0,
  `id_gallery` smallint(4) NOT NULL DEFAULT 0,
  `link` varchar(255) DEFAULT NULL,
  `thumb_height` smallint(5) DEFAULT 0,
  `image_height` smallint(5) DEFAULT 0,
  `id_gallery_from` smallint(4) DEFAULT 0,
  `id_prev` smallint(6) DEFAULT 0,
  `id_next` smallint(6) DEFAULT 0,
  PRIMARY KEY (`id_image`,`id_gallery`),
  KEY `id_gallery` (`id_gallery`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_galleries_import` (
  `id_image` mediumint(8) NOT NULL DEFAULT 0,
  `id_gallery` smallint(4) NOT NULL DEFAULT 0,
  `caption` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_gallery`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `javascript` (
  `id_js` smallint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `script` mediumtext NOT NULL,
  PRIMARY KEY (`id_js`),
  KEY `id_style` (`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keyword_params` (
  `id_keyword_param` smallint(6) NOT NULL DEFAULT 0,
  `id_type` tinyint(1) NOT NULL DEFAULT 0,
  `id_keyword` smallint(6) NOT NULL DEFAULT 0,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'text',
  `params` text DEFAULT NULL,
  `public` tinyint(1) DEFAULT 1,
  `index_include` tinyint(1) NOT NULL DEFAULT 0,
  `list_include` tinyint(1) NOT NULL DEFAULT 0,
  `seq` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_keyword_param`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id_keyword` mediumint(9) NOT NULL DEFAULT 0,
  `keyword` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_type` tinyint(1) DEFAULT 1,
  `id_language` tinyint(2) DEFAULT 1,
  PRIMARY KEY (`id_keyword`),
  KEY `id_type` (`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords_feeds` (
  `id_keyword` mediumint(9) NOT NULL DEFAULT 0,
  `description` varchar(100) NOT NULL DEFAULT '',
  `public` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_keyword`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords_landing` (
  `id_keyword` mediumint(9) NOT NULL DEFAULT 0,
  `content` text NOT NULL,
  `youtube` varchar(150) NOT NULL DEFAULT ''
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords_use` (
  `id_keyword` smallint(6) NOT NULL DEFAULT 0,
  `id` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_type` tinyint(2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `id_type` (`id_type`),
  KEY `id_keyword` (`id_keyword`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels_custom` (
  `id_label` smallint(4) NOT NULL DEFAULT 0,
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_label`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id_link` smallint(6) NOT NULL DEFAULT 0,
  `url` varchar(200) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `vote` tinyint(1) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `error404` tinyint(1) DEFAULT 0,
  `id_language` tinyint(2) NOT NULL DEFAULT 1,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_link`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links_subtopics` (
  `id_link` smallint(6) NOT NULL DEFAULT 0,
  `id_subtopic` smallint(5) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  KEY `id_topic` (`id_topic`),
  KEY `id_subtopic` (`id_subtopic`),
  KEY `id_link` (`id_link`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lists` (
  `id_list` smallint(4) NOT NULL DEFAULT 0,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `id_user` smallint(4) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(800) NOT NULL DEFAULT '',
  `id_topic` smallint(4) DEFAULT NULL,
  `id_group` smallint(3) DEFAULT NULL,
  `impl` tinyint(1) DEFAULT 1,
  `owner_email` varchar(60) DEFAULT '',
  `archive` varchar(100) DEFAULT '',
  `feed` varchar(100) DEFAULT '',
  PRIMARY KEY (`id_list`),
  KEY `id_group` (`id_group`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lists_groups` (
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailjob_recipients` (
  `id_mail` smallint(4) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `lot` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(150) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `identifier` varchar(32) NOT NULL DEFAULT '',
  `id_geo` smallint(6) DEFAULT NULL,
  KEY `lot` (`lot`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mails` (
  `id_mail` smallint(6) NOT NULL DEFAULT 0,
  `id_list` smallint(4) DEFAULT NULL,
  `id_message` varchar(100) DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `author_name` varchar(100) DEFAULT NULL,
  `author_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_mail`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_info` (
  `id_media` mediumint(9) NOT NULL DEFAULT 0,
  `id_type` tinyint(1) NOT NULL DEFAULT 0,
  `original` text NOT NULL,
  `encoded` text NOT NULL,
  PRIMARY KEY (`id_media`,`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_playlists` (
  `id_playlist` mediumint(9) NOT NULL DEFAULT 0,
  `id_media` mediumint(9) NOT NULL DEFAULT 0,
  `id_type` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_playlist`,`id_media`,`id_type`),
  KEY `id_media` (`id_media`,`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_queue` (
  `id_media` mediumint(9) NOT NULL DEFAULT 0,
  `insert_time` datetime DEFAULT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `id_type` tinyint(1) NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_participants` (
  `id_meeting_slot` smallint(3) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `join_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `status` tinyint(2) DEFAULT 0,
  `comments` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_meeting_slot`,`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_slots` (
  `id_meeting_slot` smallint(3) NOT NULL DEFAULT 0,
  `id_meeting` smallint(3) NOT NULL DEFAULT 0,
  `start_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `length` tinyint(2) DEFAULT 1,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_meeting_slot`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id_meeting` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(2000) NOT NULL DEFAULT '',
  `status` tinyint(1) DEFAULT 0,
  `is_private` tinyint(1) DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `id_user` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id_meeting`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_users` (
  `id_message` smallint(6) NOT NULL DEFAULT 0,
  `id_receiver` smallint(4) NOT NULL DEFAULT 0,
  `date_read` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_message`,`id_receiver`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id_message` smallint(6) NOT NULL DEFAULT 0,
  `subject` varchar(100) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `id_sender` smallint(4) NOT NULL DEFAULT 0,
  `id_user` smallint(4) DEFAULT NULL,
  `id_module` smallint(4) DEFAULT NULL,
  `sent` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_message`),
  KEY `id_sender` (`id_sender`),
  KEY `id_user` (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_users` (
  `id_user` smallint(4) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `id_module` smallint(4) DEFAULT NULL,
  KEY `id_user` (`id_user`),
  KEY `id_module` (`id_module`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  `path` varchar(20) DEFAULT NULL,
  `sort` smallint(4) DEFAULT NULL,
  `restricted` tinyint(1) DEFAULT 0,
  `layout` tinyint(1) DEFAULT 0,
  `internal` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  `admin` tinyint(1) NOT NULL DEFAULT 1,
  `global` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_module`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules_config` (
  `module_path` varchar(20) NOT NULL DEFAULT '',
  `config_params` varchar(800) NOT NULL DEFAULT '',
  PRIMARY KEY (`module_path`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mp_groups` (
  `id_mp_group` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_mp_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mps` (
  `id_mp_group` tinyint(2) NOT NULL DEFAULT 0,
  `id_geo` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(60) NOT NULL DEFAULT '',
  `constituency` varchar(80) NOT NULL DEFAULT '',
  `house` varchar(50) NOT NULL DEFAULT '',
  `party` varchar(100) NOT NULL DEFAULT '',
  KEY `id_mp_group` (`id_mp_group`),
  KEY `id_geo` (`id_geo`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mps_queue` (
  `id_mps_queue` mediumint(8) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) NOT NULL DEFAULT 0,
  `id_topic_campaign` smallint(4) NOT NULL DEFAULT 0,
  `sign_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_mps_queue`),
  KEY `sign_date` (`sign_date`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_action_log` (
  `id_order` mediumint(7) unsigned NOT NULL DEFAULT 0,
  `action_type` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `action_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_order`,`action_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_products` (
  `id_order` mediumint(7) unsigned NOT NULL DEFAULT 0,
  `id_product` int(11) unsigned NOT NULL DEFAULT 0,
  `quantity` smallint(3) unsigned NOT NULL DEFAULT 1,
  `price` decimal(9,2) NOT NULL DEFAULT 0.00,
  KEY `id_order` (`id_order`),
  KEY `id_product` (`id_product`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id_order` mediumint(7) unsigned NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `order_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `id_status` tinyint(2) NOT NULL DEFAULT 0,
  `delivery_notes` varchar(400) NOT NULL DEFAULT '',
  `id_transaction` varchar(100) NOT NULL DEFAULT '',
  `admin_notes` varchar(800) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_order`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_lists` (
  `id_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `description` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `sort_order` mediumint(8) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_package`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_queues` (
  `id_package_queue` int(11) NOT NULL DEFAULT 0,
  `acct_no` int(11) DEFAULT 0,
  `package_code` varchar(100) DEFAULT '',
  `package_status` tinyint(4) DEFAULT 0,
  `import_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_package_queue`),
  KEY `ACCT_NO` (`acct_no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id_package` int(11) NOT NULL DEFAULT 0,
  `id_smart_card` int(11) DEFAULT 0,
  `acct_no` int(11) DEFAULT 0,
  `package_code` varchar(100) DEFAULT '',
  `package_category` varchar(50) DEFAULT '',
  `package_date` datetime DEFAULT NULL,
  `import_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_package`),
  KEY `ACCT_NO` (`acct_no`),
  KEY `package_code` (`package_code`),
  KEY `id_smart_card` (`id_smart_card`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_features` (
  `id_feature` smallint(6) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `id_type` tinyint(2) NOT NULL,
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  KEY `id_style` (`id_style`),
  KEY `id_type` (`id_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payers` (
  `id_payer` smallint(6) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_payer`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_subscribers` (
  `id_payment` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `acct_no` int(11) DEFAULT 0,
  `first_name` varchar(50) DEFAULT '',
  `last_name` varchar(50) DEFAULT '',
  `identity_no` varchar(20) DEFAULT '',
  `identity_type_name` varchar(20) DEFAULT '',
  `mobile_phone` varchar(20) DEFAULT '',
  `home_phone` varchar(20) DEFAULT '',
  `office_phone` varchar(20) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `alt_email` varchar(255) DEFAULT '',
  `billing_freq` tinyint(4) DEFAULT 0,
  `outstanding_bal` decimal(8,2) DEFAULT 0.00,
  `transactionid` varchar(45) DEFAULT '',
  `paymentstatus` varchar(20) DEFAULT '',
  `transactionerrormessage` varchar(300) DEFAULT '',
  `transactioncode` varchar(45) DEFAULT '',
  PRIMARY KEY (`id_payment`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_tokens` (
  `id_payment` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `token` varchar(32) NOT NULL DEFAULT ''
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_types` (
  `id_payment_type` smallint(4) NOT NULL DEFAULT 0,
  `payment_type` varchar(255) DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT 0,
  PRIMARY KEY (`id_payment_type`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id_payment` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `pay_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `amount` decimal(9,2) DEFAULT 0.00,
  `id_currency` tinyint(2) DEFAULT NULL,
  `is_in` tinyint(1) DEFAULT 1,
  `description` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `id_payment_type` smallint(4) DEFAULT NULL,
  `id_payer` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_account` smallint(3) NOT NULL,
  `is_transfer` tinyint(1) DEFAULT 0,
  `verified` tinyint(1) DEFAULT 1,
  `from_account` smallint(3) NOT NULL DEFAULT 0,
  `id_use` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_payment`),
  KEY `id_payer` (`id_payer`),
  KEY `id_account` (`id_account`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `name1` varchar(50) DEFAULT '',
  `name2` varchar(50) DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `identifier` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `timeout` int(10) unsigned DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `address` varchar(150) DEFAULT NULL,
  `address_notes` varchar(150) DEFAULT NULL,
  `town` varchar(100) DEFAULT NULL,
  `postcode` varchar(15) DEFAULT NULL,
  `id_geo` smallint(3) DEFAULT NULL,
  `admin_notes` varchar(255) DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `connections` smallint(6) DEFAULT 0,
  `last_conn` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `start_date` datetime DEFAULT NULL,
  `login_failures` tinyint(1) DEFAULT 0,
  `last_failure` datetime DEFAULT NULL,
  `verified` tinyint(1) DEFAULT 0,
  `email_valid` tinyint(1) DEFAULT 0,
  `contact` tinyint(1) NOT NULL DEFAULT 0,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `name3` varchar(50) NOT NULL DEFAULT '',
  `salutation` varchar(20) NOT NULL DEFAULT '',
  `bounces` smallint(3) NOT NULL DEFAULT 0,
  `last_bounce` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_p`),
  UNIQUE KEY `identifier` (`identifier`),
  UNIQUE KEY `email` (`email`),
  KEY `last_bounce` (`last_bounce`),
  KEY `bounces` (`bounces`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_groups` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_pt_group` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_p`,`id_pt_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_login_log` (
  `id_log` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `login_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `login_count` smallint(6) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_log`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_params` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `params` text DEFAULT NULL,
  PRIMARY KEY (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_removed_log` (
  `id_removed_log` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `name1` varchar(50) NOT NULL DEFAULT '',
  `name2` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `removed_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_removed_log`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_topics` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `contact` tinyint(1) NOT NULL DEFAULT 0,
  `access` tinyint(1) NOT NULL DEFAULT 0,
  `admin_notes` varchar(200) NOT NULL DEFAULT '',
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  KEY `id_p` (`id_p`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_topics_groups` (
  `id_pt_group` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_pt_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_unique_sessions` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `ip_ua` varchar(32) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_userid` (
  `id_p` mediumint(8) NOT NULL,
  `userid` varchar(30) DEFAULT NULL,
  `inserted_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_p`),
  UNIQUE KEY `userid` (`userid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_verify` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `token` varchar(32) DEFAULT NULL,
  `token_date` datetime DEFAULT NULL,
  `id_action` tinyint(1) NOT NULL DEFAULT 1,
  `redirect` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_p`),
  KEY `token` (`token`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `id_playlist` mediumint(9) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `hash2` varchar(64) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `views` int(10) DEFAULT 0,
  `id_language` tinyint(2) DEFAULT 1,
  `auto_start` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_playlist`),
  UNIQUE KEY `hash` (`hash`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_people_votes` (
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `id_question` smallint(6) NOT NULL DEFAULT 0,
  `vote` tinyint(2) DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  KEY `id_p` (`id_p`),
  KEY `poll_question` (`id_poll`,`id_question`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_questions` (
  `id_question` smallint(6) NOT NULL DEFAULT 0,
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `seq` smallint(3) DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `approved` tinyint(1) DEFAULT NULL,
  `description_long` text NOT NULL,
  `is_html` tinyint(1) NOT NULL DEFAULT 0,
  `id_questions_group` tinyint(2) NOT NULL DEFAULT 0,
  `weight` smallint(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_question`),
  KEY `id_poll` (`id_poll`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_questions_groups` (
  `id_questions_group` tinyint(2) NOT NULL DEFAULT 0,
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `seq` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id_questions_group`),
  KEY `id_poll` (`id_poll`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_questions_votes_weights` (
  `id_question` smallint(6) NOT NULL DEFAULT 0,
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `vote` tinyint(2) NOT NULL DEFAULT 0,
  `weight` decimal(3,1) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id_question`,`id_poll`,`vote`),
  KEY `id_poll` (`id_poll`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_tokens` (
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `token` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_poll`,`id_p`),
  KEY `token` (`token`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_votes_names` (
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `vote` tinyint(2) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `seq` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id_poll`,`vote`),
  KEY `id_poll` (`id_poll`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polls` (
  `id_poll` smallint(4) NOT NULL DEFAULT 0,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `id_type` tinyint(1) NOT NULL DEFAULT 1,
  `users_type` tinyint(1) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `vote_threshold` smallint(4) NOT NULL DEFAULT 0,
  `vote_min` tinyint(2) NOT NULL DEFAULT 1,
  `vote_max` tinyint(2) NOT NULL DEFAULT 5,
  `intro` text NOT NULL,
  `thanks` text NOT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `can_expire` tinyint(1) NOT NULL DEFAULT 0,
  `comments` tinyint(1) NOT NULL DEFAULT 0,
  `moderate_comments` tinyint(1) NOT NULL DEFAULT 1,
  `submit_questions` tinyint(1) NOT NULL DEFAULT 0,
  `sort_questions_by` tinyint(1) NOT NULL DEFAULT 1,
  `promoter` varchar(200) NOT NULL DEFAULT '',
  `promoter_email` varchar(50) NOT NULL DEFAULT '',
  `notify_answers` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_poll`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id_product` int(11) unsigned NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `price` decimal(9,2) NOT NULL DEFAULT 0.00,
  `stock` tinyint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_product`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prov` (
  `id_prov` tinyint(4) NOT NULL DEFAULT 0,
  `prov` varchar(25) DEFAULT NULL,
  `id_reg` tinyint(4) DEFAULT NULL,
  `pr` varchar(5) DEFAULT NULL,
  `it` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_prov`),
  KEY `id_reg` (`id_reg`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publish_log` (
  `id_publish_log` mediumint(9) NOT NULL DEFAULT 0,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `publish` datetime DEFAULT NULL,
  PRIMARY KEY (`id_publish_log`),
  KEY `id_topic` (`id_topic`),
  KEY `publish` (`publish`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishers` (
  `id_publisher` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `town` varchar(50) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `id_prov` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id_publisher`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id_job` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) DEFAULT NULL,
  `id_type` tinyint(1) DEFAULT NULL,
  `id_item` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `action` varchar(10) DEFAULT NULL,
  `param` varchar(10) DEFAULT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_job`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `id_quote` smallint(4) NOT NULL DEFAULT 0,
  `author` varchar(255) DEFAULT NULL,
  `quote` text DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `notes` text DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `id_topic` smallint(4) DEFAULT 0,
  PRIMARY KEY (`id_quote`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurring_events` (
  `id_event` smallint(6) NOT NULL DEFAULT 0,
  `r_day` tinyint(2) DEFAULT NULL,
  `r_month` tinyint(2) DEFAULT NULL,
  `r_year` smallint(4) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_event`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redirects` (
  `id_redirect` smallint(4) NOT NULL,
  `url_from` varchar(200) NOT NULL,
  `url_to` varchar(200) NOT NULL,
  `id_type` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_redirect`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg` (
  `id_reg` tinyint(4) NOT NULL DEFAULT 0,
  `reg` varchar(22) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_reg`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id_relation` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_relation`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_notes` (
  `id_note` mediumint(7) NOT NULL DEFAULT 0,
  `id_res` tinyint(2) NOT NULL DEFAULT 0,
  `id` int(10) NOT NULL DEFAULT 0,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `note` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_note`),
  KEY `idx_id` (`id_res`,`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_params` (
  `id_resource_param` smallint(4) NOT NULL DEFAULT 0,
  `id_resource` tinyint(2) NOT NULL DEFAULT 0,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'text',
  `public` tinyint(1) DEFAULT 1,
  `params` text DEFAULT NULL,
  PRIMARY KEY (`id_resource_param`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id_review` smallint(6) NOT NULL DEFAULT 0,
  `review` text DEFAULT NULL,
  `id_book` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `vote` tinyint(1) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `important` tinyint(1) DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `rparams` text DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `id_topic` smallint(4) DEFAULT 0,
  PRIMARY KEY (`id_review`),
  KEY `id_book` (`id_book`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id_schedule` smallint(4) NOT NULL,
  `id_action` smallint(4) NOT NULL DEFAULT 0,
  `params` varchar(255) DEFAULT NULL,
  `id_scheduler` tinyint(1) NOT NULL,
  `id_user` smallint(4) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_schedule`),
  KEY `id_scheduler` (`id_scheduler`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index` (
  `id_res` tinyint(2) NOT NULL,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_word` int(11) NOT NULL,
  `word_pos` int(11) NOT NULL,
  `relevance` smallint(3) NOT NULL,
  PRIMARY KEY (`id_res`,`id`,`word_pos`),
  KEY `idx_wordpos` (`id_word`,`word_pos`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index2` (
  `id_res` tinyint(2) NOT NULL,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_word` int(11) NOT NULL,
  `score` smallint(3) NOT NULL,
  `matches` smallint(3) NOT NULL,
  `id_topic` smallint(3) NOT NULL,
  `id_group` smallint(3) NOT NULL,
  PRIMARY KEY (`id_res`,`id`,`id_word`),
  KEY `idx_word` (`id_word`),
  KEY `idx_topic` (`id_topic`),
  KEY `idx_group` (`id_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_queue` (
  `id_res` tinyint(2) NOT NULL,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_topic` smallint(3) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  `insert_time` datetime NOT NULL,
  `id_group` smallint(3) NOT NULL,
  KEY `id_res` (`id_res`,`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_words` (
  `id_word` int(11) NOT NULL,
  `word` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `metaphone` varchar(15) DEFAULT '',
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `idx_word` (`word`),
  KEY `metaphone` (`metaphone`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches` (
  `search_time` datetime NOT NULL,
  `query` varchar(100) DEFAULT '',
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  KEY `query` (`query`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sendmail` (
  `id_mail` smallint(4) NOT NULL DEFAULT 0,
  `send_date` datetime DEFAULT NULL,
  `id_user` smallint(4) DEFAULT NULL,
  `sent` tinyint(1) DEFAULT 0,
  `subject` varchar(255) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `how_many` mediumint(8) DEFAULT 0,
  `from_name` varchar(100) DEFAULT '',
  `from_email` varchar(100) DEFAULT '0',
  `id_module` smallint(4) DEFAULT 0,
  `id_item` smallint(6) DEFAULT 0,
  `footer` text NOT NULL,
  `send_password` tinyint(1) NOT NULL DEFAULT 0,
  `is_html` tinyint(1) DEFAULT 0,
  `format` varchar(5) DEFAULT '',
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `track` tinyint(1) NOT NULL DEFAULT 0,
  `track_redirect` varchar(100) NOT NULL DEFAULT '',
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `auto_footer` tinyint(1) NOT NULL DEFAULT 1,
  `alt_text` text NOT NULL,
  `embed_images` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_mail`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_status` (
  `id_service_status` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `name` varchar(20) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `notes` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_service_status`),
  KEY `name` (`name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(32) NOT NULL,
  `access` int(10) unsigned NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_watch` (
  `id_watch` smallint(4) NOT NULL DEFAULT 0,
  `url` varchar(250) NOT NULL DEFAULT '',
  `keyword` varchar(50) NOT NULL DEFAULT '',
  `notify` varchar(250) NOT NULL DEFAULT '',
  `counter` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_watch`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smart_cards` (
  `id_smart_card` int(11) NOT NULL DEFAULT 0,
  `smart_card_no` varchar(50) DEFAULT '',
  `id_subscriber` int(11) DEFAULT 0,
  PRIMARY KEY (`id_smart_card`),
  KEY `id_subscriber` (`id_subscriber`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statements` (
  `id_statement` smallint(4) NOT NULL DEFAULT 0,
  `id_relation` smallint(6) NOT NULL DEFAULT 0,
  `id_domain` smallint(6) NOT NULL DEFAULT 0,
  `id_range` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id_statement`),
  KEY `id_relation` (`id_relation`),
  KEY `id_domain` (`id_domain`),
  KEY `id_range` (`id_range`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id_state` tinyint(2) NOT NULL DEFAULT 0,
  `code` varchar(10) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  PRIMARY KEY (`id_state`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_changes` (
  `change_date` date NOT NULL DEFAULT '1970-01-01',
  `id_res` tinyint(2) NOT NULL DEFAULT 0,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `id_parent` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_labels` (
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  `labels` text DEFAULT NULL,
  `id_language` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_style`,`id_module`,`id_language`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_channels` (
  `id_sub_package_channel` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_sub_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_service` int(10) NOT NULL DEFAULT 0,
  `is_included` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_sub_package_channel`),
  KEY `id_service` (`id_service`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_info` (
  `id_sub_package_info` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_sub_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `info_text` text NOT NULL,
  `info_type` tinyint(4) NOT NULL DEFAULT 3,
  PRIMARY KEY (`id_sub_package_info`),
  KEY `id_sub_package` (`id_sub_package`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_lists` (
  `id_sub_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `package_code` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `mix_index` bigint(20) NOT NULL DEFAULT 0,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `auto_select` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` mediumint(8) NOT NULL DEFAULT 0,
  `drag_type` tinyint(4) NOT NULL DEFAULT 0,
  `channel_rel_type` tinyint(4) NOT NULL DEFAULT 0,
  `strike_name` varchar(100) DEFAULT '',
  PRIMARY KEY (`id_sub_package`),
  KEY `package_code` (`package_code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_map` (
  `id_sub_package_map` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `package_code` varchar(100) NOT NULL DEFAULT '',
  `package_code_map` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_sub_package_map`),
  KEY `package_code` (`package_code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_price` (
  `id_sub_package_price` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_package` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `mix_index` bigint(20) NOT NULL DEFAULT 0,
  `count` smallint(6) NOT NULL DEFAULT 0,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `invalid_msg` varchar(100) NOT NULL DEFAULT '',
  `id_parent_price` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_sub_package_price`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_package_validation` (
  `id_sub_package_validation` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `param1` int(11) NOT NULL DEFAULT 0,
  `param2` int(11) NOT NULL DEFAULT 0,
  `msg` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_sub_package_validation`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_default_smart_card` (
  `acct_no` int(11) NOT NULL DEFAULT 0,
  `smart_card_no` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`acct_no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_queues` (
  `id_subscriber_queue` int(11) NOT NULL DEFAULT 0,
  `acct_no` int(11) DEFAULT 0,
  `subscriber_status` tinyint(4) DEFAULT 0,
  `import_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_subscriber_queue`),
  KEY `ACCT_NO` (`acct_no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id_subscriber` int(11) NOT NULL DEFAULT 0,
  `acct_no` int(11) DEFAULT 0,
  `first_name` varchar(50) DEFAULT '',
  `last_name` varchar(50) DEFAULT '',
  `identity_no` varchar(20) DEFAULT '',
  `id_identity_type` int(11) DEFAULT 0,
  `email` varchar(255) DEFAULT '',
  `alt_email` varchar(255) DEFAULT '',
  `home_phone` varchar(20) DEFAULT '',
  `office_phone` varchar(20) DEFAULT '',
  `mobile_phone` varchar(20) DEFAULT '',
  `status` tinyint(4) DEFAULT 0,
  `outstanding_bal` decimal(8,2) DEFAULT 0.00,
  `billing_freq` tinyint(4) DEFAULT 0,
  `import_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_subscriber`),
  KEY `ACCT_NO` (`acct_no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtopics` (
  `id_subtopic` smallint(5) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `header` text DEFAULT NULL,
  `link` varchar(127) DEFAULT NULL,
  `id_parent` smallint(5) DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT NULL,
  `id_type` smallint(4) DEFAULT NULL,
  `seq` tinyint(1) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `footer` text DEFAULT NULL,
  `id_image` mediumint(8) DEFAULT NULL,
  `params` text DEFAULT NULL,
  `id_item` smallint(4) DEFAULT NULL,
  `id_subitem` smallint(4) DEFAULT NULL,
  `sort_by` tinyint(1) NOT NULL DEFAULT 3,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `records_per_page` tinyint(2) NOT NULL DEFAULT 0,
  `id_subitem_id` varchar(20) NOT NULL DEFAULT '',
  `has_feed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_subtopic`),
  KEY `id_topic` (`id_topic`),
  KEY `id_parent` (`id_parent`),
  KEY `seq` (`seq`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_params` (
  `id_template_param` smallint(6) NOT NULL DEFAULT 0,
  `id_template` smallint(3) NOT NULL DEFAULT 0,
  `label` varchar(100) DEFAULT '',
  `type` varchar(10) DEFAULT 'text',
  `type_params` varchar(800) DEFAULT NULL,
  `default_value` varchar(500) DEFAULT '',
  `public` tinyint(1) DEFAULT 1,
  `index_include` tinyint(1) NOT NULL DEFAULT 0,
  `seq` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_template_param`),
  KEY `id_template` (`id_template`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_values` (
  `id_res` tinyint(2) NOT NULL DEFAULT 0,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_template_param` smallint(6) NOT NULL DEFAULT 0,
  `value` text NOT NULL,
  PRIMARY KEY (`id_res`,`id`,`id_template_param`),
  KEY `id` (`id_res`,`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id_template` smallint(3) NOT NULL DEFAULT 0,
  `id_res` tinyint(2) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `id_group` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `hide_fields` varchar(200) DEFAULT '',
  `default_subtopic` smallint(5) DEFAULT 0,
  PRIMARY KEY (`id_template`),
  KEY `id_group` (`id_group`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_campaigns` (
  `id_topic_campaign` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `thanks` text DEFAULT NULL,
  `money` tinyint(1) DEFAULT 1,
  `approve_comments` tinyint(1) DEFAULT 0,
  `description` text DEFAULT NULL,
  `users_type` tinyint(1) DEFAULT 0,
  `orgs_sign` tinyint(1) DEFAULT 1,
  `id_payment_type` smallint(4) NOT NULL DEFAULT 0,
  `amount` varchar(255) NOT NULL DEFAULT '',
  `editable` tinyint(1) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `description_long` text NOT NULL,
  `is_html` tinyint(1) NOT NULL DEFAULT 0,
  `notify` varchar(150) NOT NULL DEFAULT '',
  `promoter` varchar(200) NOT NULL DEFAULT '',
  `notify_text` text NOT NULL,
  `promoter_email` varchar(50) NOT NULL DEFAULT '',
  `thanks_email` text NOT NULL,
  `pre_sign` text NOT NULL,
  `id_mp_group` smallint(3) NOT NULL DEFAULT 0,
  `signatures_per_page` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_topic_campaign`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_campaigns_orgs` (
  `id_org` smallint(6) NOT NULL DEFAULT 0,
  `comments` text DEFAULT NULL,
  `comment_approved` tinyint(1) DEFAULT 1,
  `id_topic_campaign` smallint(4) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `contact` tinyint(1) DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_org`),
  KEY `id_topic_campaign` (`id_topic_campaign`),
  KEY `insert_date` (`insert_date`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_campaigns_persons` (
  `id_person` mediumint(7) NOT NULL DEFAULT 0,
  `comments` text DEFAULT NULL,
  `id_topic_campaign` smallint(4) DEFAULT NULL,
  `comment_approved` tinyint(1) DEFAULT 1,
  `insert_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `contact` tinyint(1) DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_person`),
  KEY `id_topic_campaign` (`id_topic_campaign`),
  KEY `insert_date` (`insert_date`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_campaigns_vips` (
  `id_topic_campaign` smallint(4) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_topic_campaign`,`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_forum_threads` (
  `id_thread` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(150) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_long` text DEFAULT NULL,
  `is_html` tinyint(1) DEFAULT 0,
  `id_topic_forum` smallint(4) DEFAULT NULL,
  `thread_approved` tinyint(1) DEFAULT 0,
  `insert_date` datetime DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_thread`),
  KEY `id_topic_forum` (`id_topic_forum`),
  KEY `insert_date` (`insert_date`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_forums` (
  `id_topic_forum` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  `approve_threads` tinyint(1) DEFAULT 1,
  `comments` tinyint(1) DEFAULT 1,
  `approve_comments` tinyint(1) DEFAULT 1,
  `uploads` tinyint(1) DEFAULT 0,
  `thanks` text DEFAULT NULL,
  `source` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `description` text DEFAULT NULL,
  `users_type` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_topic_forum`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_homepage` (
  `id_topic` smallint(4) DEFAULT NULL,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `is_main` tinyint(1) DEFAULT 0,
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_keywords` (
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `id_keyword` mediumint(9) NOT NULL DEFAULT 0,
  `id_res_type` tinyint(2) NOT NULL DEFAULT 0,
  `role` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_topic`,`id_keyword`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_users` (
  `id_user` smallint(4) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `id_topic` smallint(4) DEFAULT NULL,
  `is_contact` tinyint(1) DEFAULT 0,
  `pending_notify` tinyint(1) NOT NULL DEFAULT 0,
  KEY `id_topic` (`id_topic`),
  KEY `id_user` (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(80) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `home_type` tinyint(1) DEFAULT NULL,
  `id_article_home` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `menu_depth` tinyint(1) DEFAULT 1,
  `id_style` smallint(4) DEFAULT NULL,
  `id_image` mediumint(8) DEFAULT NULL,
  `show_path` tinyint(1) DEFAULT 0,
  `articles_per_page` smallint(3) DEFAULT 10,
  `visible` tinyint(1) DEFAULT 0,
  `newsletter` tinyint(1) DEFAULT 0,
  `id_recipient` smallint(4) DEFAULT NULL,
  `id_frequency` tinyint(2) DEFAULT 2,
  `lastletter` date DEFAULT '2003-01-01',
  `id_group` smallint(4) DEFAULT 0,
  `id_language` tinyint(1) DEFAULT 1,
  `show_latest` tinyint(1) DEFAULT 1,
  `domain` varchar(150) DEFAULT NULL,
  `share_docs` tinyint(1) DEFAULT 1,
  `share_images` tinyint(1) DEFAULT 1,
  `show_print` tinyint(1) DEFAULT 1,
  `edit_layout` tinyint(1) DEFAULT 0,
  `seq` tinyint(1) DEFAULT NULL,
  `protected` tinyint(1) DEFAULT 0,
  `awstats` varchar(30) DEFAULT NULL,
  `comments` tinyint(1) DEFAULT 0,
  `moderate_comments` tinyint(1) DEFAULT 1,
  `profiling` tinyint(1) DEFAULT 0,
  `campaigns_open` tinyint(1) NOT NULL DEFAULT 0,
  `custom_visibility` tinyint(1) NOT NULL DEFAULT 1,
  `home_sort_by` tinyint(1) NOT NULL DEFAULT 0,
  `wysiwyg` tinyint(1) NOT NULL DEFAULT 0,
  `temail` varchar(50) NOT NULL DEFAULT '',
  `newsletter_format` tinyint(1) DEFAULT 0,
  `newsletter_rcpt` varchar(100) DEFAULT '',
  `home_keywords` varchar(200) NOT NULL DEFAULT '',
  `associated_image_size` tinyint(2) NOT NULL DEFAULT 0,
  `friendly_url` tinyint(1) NOT NULL DEFAULT 0,
  `newsletter_people` tinyint(1) NOT NULL DEFAULT 0,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_topic`),
  KEY `id_group` (`id_group`),
  KEY `seq` (`seq`),
  KEY `visible` (`visible`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics_groups` (
  `id_group` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_parent` smallint(4) DEFAULT NULL,
  `seq` tinyint(1) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_group`),
  KEY `seq` (`seq`),
  KEY `id_parent` (`id_parent`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop` (
  `id_tourop` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(150) DEFAULT NULL,
  `name2` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `web` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `address_notes` varchar(150) DEFAULT NULL,
  `town` varchar(100) DEFAULT NULL,
  `postcode` varchar(15) DEFAULT NULL,
  `id_geo` smallint(3) DEFAULT NULL,
  `id_geo_group` tinyint(1) DEFAULT NULL,
  `is_tourop` tinyint(1) DEFAULT 0,
  `id_catalogue` tinyint(1) DEFAULT NULL,
  `print_italy_only` tinyint(1) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  `notes` text DEFAULT NULL,
  `admin_notes` text DEFAULT NULL,
  `num_tourists` smallint(6) DEFAULT 0,
  `last_update` datetime DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `bonding` varchar(100) DEFAULT NULL,
  `memberof` varchar(100) DEFAULT NULL,
  `is_travel` tinyint(1) DEFAULT 0,
  `is_coach` tinyint(1) DEFAULT 0,
  `internet_only` tinyint(1) DEFAULT 0,
  `is_other` tinyint(1) NOT NULL DEFAULT 0,
  `is_agency` tinyint(1) NOT NULL DEFAULT 0,
  `abta` tinyint(1) NOT NULL DEFAULT 0,
  `id_competition_group` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_tourop`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_bookings` (
  `id_booking` mediumint(8) NOT NULL DEFAULT 0,
  `id_tourop` smallint(6) NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(200) NOT NULL DEFAULT '',
  `ref` varchar(20) NOT NULL DEFAULT '',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `tourop` varchar(200) NOT NULL DEFAULT '',
  `num_people` smallint(2) NOT NULL DEFAULT 0,
  `destination` varchar(200) NOT NULL DEFAULT '',
  `booking_date` date NOT NULL DEFAULT '0000-00-00',
  `score` smallint(3) NOT NULL DEFAULT 0,
  `id_competition` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_booking`),
  KEY `id_competition` (`id_competition`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_competitions` (
  `id_competition` smallint(4) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `id_competition_group` smallint(4) NOT NULL DEFAULT 0,
  `use_agencies` tinyint(1) NOT NULL DEFAULT 1,
  `select_tourop` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `public_insert` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_competition`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_competitions_bookings` (
  `id_competition` smallint(4) NOT NULL DEFAULT 0,
  `id_booking` mediumint(8) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_competition`,`id_booking`),
  KEY `id_competition` (`id_competition`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_competitions_countries` (
  `id_competition_group` smallint(4) unsigned NOT NULL DEFAULT 0,
  `id_country` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_competition_group`,`id_country`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_competitions_groups` (
  `id_competition_group` smallint(4) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `id_language` tinyint(1) NOT NULL DEFAULT 2,
  `admin_notes` varchar(255) NOT NULL DEFAULT '',
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_competition_group`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_contacts` (
  `id_contact` smallint(6) NOT NULL DEFAULT 0,
  `id_tourop` smallint(6) NOT NULL,
  `id_role` tinyint(2) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `enabled` tinyint(1) DEFAULT 0,
  `role` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_contact`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_interests` (
  `id_interest` smallint(6) NOT NULL DEFAULT 0,
  `id_tourop` smallint(6) NOT NULL DEFAULT 0,
  `offertype` varchar(100) DEFAULT NULL,
  `stay` varchar(100) DEFAULT NULL,
  `id_reg` tinyint(4) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `is_reg` tinyint(1) DEFAULT 1,
  `target` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id_interest`),
  KEY `id_tourop` (`id_tourop`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_ita_offers` (
  `id_offer` smallint(6) NOT NULL DEFAULT 0,
  `id_ita` smallint(6) NOT NULL DEFAULT 0,
  `offertype` text DEFAULT NULL,
  `stay` text DEFAULT NULL,
  `id_reg` tinyint(4) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `is_reg` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_offer`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_itas` (
  `id_ita` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `postcode` varchar(15) DEFAULT NULL,
  `town` varchar(100) DEFAULT NULL,
  `id_geo` smallint(3) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `web` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `admin_notes` text DEFAULT NULL,
  `active` tinyint(1) DEFAULT 0,
  `image_format` varchar(5) DEFAULT '',
  `contact_name` varchar(150) DEFAULT NULL,
  `contact_role` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_ita`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_offers` (
  `id_offer` smallint(6) NOT NULL DEFAULT 0,
  `id_tourop` smallint(6) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `offertype` varchar(100) DEFAULT NULL,
  `stay` varchar(100) DEFAULT NULL,
  `id_reg` tinyint(4) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `is_reg` tinyint(1) DEFAULT 1,
  `target` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id_offer`),
  KEY `id_tourop` (`id_tourop`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_workshop_bookings` (
  `id_booking` smallint(6) NOT NULL DEFAULT 0,
  `id_day` smallint(4) NOT NULL DEFAULT 0,
  `id_slot` smallint(3) NOT NULL DEFAULT 0,
  `id_ita` smallint(4) DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id_booking`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_workshop_days` (
  `id_workshop` smallint(3) NOT NULL DEFAULT 0,
  `id_day` smallint(4) NOT NULL DEFAULT 0,
  `year` smallint(4) DEFAULT 0,
  `month` tinyint(2) DEFAULT 0,
  `day` tinyint(2) DEFAULT 0,
  `hour_start` tinyint(2) DEFAULT 0,
  `hour_end` tinyint(2) DEFAULT 0,
  `slot_minutes` tinyint(3) DEFAULT 0,
  PRIMARY KEY (`id_day`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_workshop_register` (
  `id_workshop` smallint(3) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_workshop`,`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_workshop_slots` (
  `id_day` smallint(4) NOT NULL DEFAULT 0,
  `id_slot` smallint(3) NOT NULL DEFAULT 0,
  `wdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_slot`,`id_day`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourop_workshops` (
  `id_workshop` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `location` text DEFAULT NULL,
  `active` tinyint(1) DEFAULT 0,
  `subscribable` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_workshop`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id_translation` smallint(6) NOT NULL DEFAULT 0,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_language` tinyint(2) NOT NULL DEFAULT 0,
  `start_date` date DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `priority` tinyint(1) DEFAULT 2,
  `id_article_trad` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_language_trad` tinyint(2) NOT NULL DEFAULT 0,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `comments` text DEFAULT NULL,
  `id_user` smallint(4) NOT NULL,
  `translator` varchar(100) DEFAULT NULL,
  `id_rev` smallint(4) NOT NULL DEFAULT 0,
  `id_ad` smallint(4) NOT NULL DEFAULT 0,
  `source_url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_translation`),
  KEY `id_article` (`id_article`),
  KEY `id_article_trad` (`id_article_trad`),
  KEY `id_user` (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translator_languages` (
  `id_user` smallint(4) NOT NULL,
  `id_language_from` tinyint(2) NOT NULL DEFAULT 0,
  `id_language_to` tinyint(2) NOT NULL DEFAULT 0,
  KEY `id_user` (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tweets` (
  `id_tweet` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `tweet` varchar(150) DEFAULT '',
  `insert_time` datetime DEFAULT NULL,
  `id_article` mediumint(6) unsigned NOT NULL DEFAULT 0,
  `id_user` smallint(4) DEFAULT 0,
  `id_twitter` smallint(4) NOT NULL DEFAULT 0,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_tweet`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitters` (
  `id_twitter` smallint(4) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) DEFAULT 0,
  `id_topic_group` smallint(4) DEFAULT 0,
  `id_user` smallint(4) DEFAULT 0,
  `id_keyword` mediumint(9) DEFAULT 0,
  `use_keywords` tinyint(1) NOT NULL DEFAULT 0,
  `show_latest_only` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `oauth_token` varchar(128) NOT NULL DEFAULT '',
  `oauth_token_secret` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_twitter`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uk_counties` (
  `id_county` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL,
  `id_country` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_county`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uk_countries` (
  `id_country` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_country`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_friendly` (
  `id_res` tinyint(2) NOT NULL DEFAULT 0,
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `furl` varchar(150) NOT NULL DEFAULT '',
  `url` varchar(150) NOT NULL DEFAULT '',
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_res`,`id`),
  KEY `furl` (`furl`),
  KEY `id_topic` (`id_topic`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_shorts` (
  `id_url` bigint(20) unsigned NOT NULL DEFAULT 0,
  `url` varchar(200) NOT NULL DEFAULT '',
  `md5` char(32) NOT NULL DEFAULT '',
  `insert_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_url`),
  KEY `md5` (`md5`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_shorts_hits` (
  `id_url` bigint(20) unsigned NOT NULL DEFAULT 0,
  `hits` bigint(20) unsigned NOT NULL DEFAULT 0,
  `last_use` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_url`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_notes` (
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `signature` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_services` (
  `id_user_service` smallint(4) NOT NULL DEFAULT 0,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `service_name` varchar(20) DEFAULT NULL,
  `mobile` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_user_service`),
  KEY `id_user` (`id_user`),
  KEY `service_name` (`service_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_verify` (
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `token` varchar(32) DEFAULT NULL,
  `token_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `token` (`token`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(50) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email_visible` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `user_show` tinyint(1) NOT NULL DEFAULT 0,
  `id_geo` smallint(3) DEFAULT NULL,
  `admin_notes` varchar(255) DEFAULT NULL,
  `id_language` tinyint(2) DEFAULT 1,
  `id_tutor` smallint(4) DEFAULT NULL,
  `connections` smallint(6) DEFAULT 0,
  `start_date` date DEFAULT NULL,
  `login_failures` tinyint(1) DEFAULT 0,
  `last_failure` datetime DEFAULT NULL,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `last_conn` datetime DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `bounces` smallint(3) NOT NULL DEFAULT 0,
  `last_bounce` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `staff_id` varchar(50) NOT NULL DEFAULT '',
  `department` varchar(50) NOT NULL DEFAULT '',
  `job_position` varchar(50) NOT NULL DEFAULT '',
  `photo_visible` tinyint(1) NOT NULL DEFAULT 0,
  `id_group` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_user`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_log` (
  `id_user_log` mediumint(7) NOT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `login` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `user_agent` varchar(200) DEFAULT '',
  PRIMARY KEY (`id_user_log`),
  KEY `id_user` (`id_user`),
  KEY `login` (`login`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_entitlement_types` (
  `id_player` mediumint(8) NOT NULL DEFAULT 0,
  `id_entitlement` mediumint(8) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_player`,`id_entitlement`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_entitlements` (
  `id_player` mediumint(8) NOT NULL DEFAULT 0,
  `id_entitlement` mediumint(8) NOT NULL DEFAULT 0,
  `package_lists` varchar(255) NOT NULL DEFAULT '',
  `entitlement_code` varchar(50) NOT NULL DEFAULT '',
  `is_new` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_player`,`id_entitlement`,`package_lists`,`entitlement_code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_pears` (
  `id_pear` smallint(3) NOT NULL DEFAULT 0,
  `key` varchar(100) DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `pub_web` varchar(120) NOT NULL DEFAULT '',
  `approved` tinyint(1) DEFAULT 0,
  `token` varchar(32) NOT NULL DEFAULT '',
  `is_server` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_pear`),
  KEY `key` (`key`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id_video` mediumint(9) NOT NULL DEFAULT 0,
  `insert_date` date DEFAULT NULL,
  `format` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(2000) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(800) NOT NULL DEFAULT '',
  `bytes` int(10) NOT NULL DEFAULT 0,
  `bytes_enc` int(10) NOT NULL DEFAULT 0,
  `length` smallint(6) NOT NULL DEFAULT 0,
  `id_licence` tinyint(2) NOT NULL DEFAULT 0,
  `id_topic` smallint(4) NOT NULL DEFAULT 0,
  `encoded` tinyint(1) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `hash2` varchar(64) NOT NULL DEFAULT '',
  `image_seq` tinyint(2) NOT NULL DEFAULT 0,
  `link` varchar(255) NOT NULL DEFAULT '',
  `views` int(10) DEFAULT 0,
  `id_language` tinyint(2) DEFAULT 1,
  `auto_start` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_video`),
  UNIQUE KEY `hash` (`hash`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos_queue` (
  `id_video` mediumint(9) NOT NULL DEFAULT 0,
  `insert_time` datetime DEFAULT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id_visit` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `visit_date` datetime DEFAULT NULL,
  `user_agent` varchar(150) NOT NULL DEFAULT '',
  `referer` varchar(200) NOT NULL DEFAULT '',
  `width` varchar(6) NOT NULL DEFAULT '',
  `height` varchar(6) NOT NULL DEFAULT '',
  `color` char(3) NOT NULL DEFAULT '',
  `browser` varchar(20) NOT NULL DEFAULT '',
  `version` varchar(5) NOT NULL DEFAULT '',
  `os` varchar(10) NOT NULL DEFAULT '',
  `cookie` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_visit`),
  KEY `visit_date` (`visit_date`),
  KEY `id_p` (`id_p`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits_ip` (
  `id_visit` smallint(6) NOT NULL DEFAULT 0,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `last_time` datetime DEFAULT NULL,
  `hash` varchar(32) DEFAULT '',
  PRIMARY KEY (`id_visit`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits_others` (
  `id_p2` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `identifier` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_p2`),
  KEY `identifier` (`identifier`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits_pages` (
  `id_visit` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `page_date` datetime DEFAULT NULL,
  `url` varchar(150) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `id_topic` smallint(4) DEFAULT 0,
  `id_group` smallint(4) DEFAULT 0,
  KEY `page_date` (`page_date`),
  KEY `id_visit` (`id_visit`),
  KEY `id_group` (`id_group`),
  KEY `id_topic` (`id_topic`),
  KEY `id_group_2` (`id_group`,`id_topic`,`id_visit`),
  KEY `id_topic_2` (`id_topic`,`url`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_feed_items` (
  `id_web_feed_item` int(11) NOT NULL DEFAULT 0,
  `id_web_feed` int(11) NOT NULL DEFAULT 0,
  `title` varchar(500) NOT NULL DEFAULT '',
  `link_url` varchar(500) NOT NULL DEFAULT '',
  `image_url` varchar(255) NOT NULL DEFAULT '',
  `published_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_approved` tinyint(1) NOT NULL DEFAULT 0,
  `is_filtered` tinyint(1) NOT NULL DEFAULT 0,
  `is_expired` tinyint(1) NOT NULL DEFAULT 0,
  `last_updated` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `status` tinyint(2) NOT NULL DEFAULT 0,
  `last_checked` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `sort` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_web_feed_item`),
  KEY `id_web_feed` (`id_web_feed`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_feed_items_content` (
  `id_web_feed_item` int(11) NOT NULL DEFAULT 0,
  `content` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_web_feed_item`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_feeds` (
  `id_web_feed` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) NOT NULL DEFAULT '',
  `feed_url` varchar(255) NOT NULL DEFAULT '',
  `web_feed_type` tinyint(4) NOT NULL DEFAULT 0,
  `update_interval` tinyint(4) NOT NULL DEFAULT 24,
  `last_updated` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_web_feed`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_feeds_status_log` (
  `id_web_feed_status` int(11) NOT NULL DEFAULT 0,
  `id_web_feed` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `pull_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `inserted` int(11) NOT NULL DEFAULT 0,
  `updated` int(11) NOT NULL DEFAULT 0,
  `deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_web_feed_status`),
  KEY `id_web_feed` (`id_web_feed`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_abuses` (
  `id_widget_abuse` mediumint(7) NOT NULL DEFAULT 0,
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `comments` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_widget_abuse`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_params` (
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `params` text NOT NULL,
  PRIMARY KEY (`id_widget`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_sharings` (
  `id_widget_sharing` mediumint(7) NOT NULL DEFAULT 0,
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `email` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `invited_on` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `can_edit` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_widget_sharing`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget_web_feeds` (
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `id_web_feed` int(11) NOT NULL DEFAULT 0,
  `items_display` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_widget`,`id_web_feed`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `identifier` varchar(32) NOT NULL DEFAULT '',
  `title` varchar(150) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `widget_type` tinyint(2) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `shareable` tinyint(1) NOT NULL DEFAULT 0,
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `insert_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `ip` varchar(20) DEFAULT '',
  `comment` varchar(255) NOT NULL DEFAULT '',
  `comment_visible` tinyint(1) NOT NULL DEFAULT 0,
  `id_widget_parent` mediumint(7) NOT NULL DEFAULT 0,
  `last_update` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id_widget`),
  KEY `identifier` (`identifier`),
  KEY `id_p` (`id_p`),
  KEY `id_widget_parent` (`id_widget_parent`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets_categories` (
  `id_category` smallint(3) unsigned NOT NULL DEFAULT 0,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_category`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets_category_widgets` (
  `id_category` smallint(3) unsigned NOT NULL DEFAULT 0,
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `is_selected` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_category`,`id_widget`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets_tabs` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_tab` tinyint(2) NOT NULL DEFAULT 1,
  `tab_name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_p`,`id_tab`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets_tabs_position` (
  `id_p` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `id_tab` tinyint(2) NOT NULL DEFAULT 1,
  `id_widget` mediumint(7) NOT NULL DEFAULT 0,
  `col` tinyint(2) NOT NULL DEFAULT 1,
  `pos` tinyint(2) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_p`,`id_tab`,`id_widget`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xml` (
  `id_xml` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL,
  `xml` text DEFAULT NULL,
  PRIMARY KEY (`id_xml`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl` (
  `id_xsl` smallint(4) NOT NULL DEFAULT 0,
  `id_type` tinyint(6) NOT NULL,
  `id_style` smallint(4) NOT NULL DEFAULT 0,
  `xsl` mediumtext NOT NULL,
  `outsourced` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_xsl`),
  KEY `id_style` (`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl_custom` (
  `id_xsl` smallint(4) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `xsl` text DEFAULT NULL,
  PRIMARY KEY (`id_xsl`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl_ext` (
  `id_xsl` smallint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `id_style` smallint(4) NOT NULL,
  `xsl` text DEFAULT NULL,
  PRIMARY KEY (`id_xsl`,`id_style`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl_global` (
  `id_xsl` tinyint(1) NOT NULL,
  `xsl` text DEFAULT NULL,
  `outsourced` tinyint(1) DEFAULT 0
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl_history` (
  `type` varchar(12) NOT NULL DEFAULT '',
  `id_xsl` smallint(4) NOT NULL DEFAULT 0,
  `id_pagetype` smallint(3) NOT NULL DEFAULT 0,
  `id_style` smallint(3) NOT NULL DEFAULT 0,
  `revision` smallint(6) NOT NULL DEFAULT 0,
  `change_time` datetime DEFAULT NULL,
  `id_user` smallint(4) NOT NULL DEFAULT 0,
  `content` text NOT NULL,
  `tag` varchar(50) DEFAULT '',
  `valid` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`revision`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xsl_module` (
  `id_xsl` smallint(4) NOT NULL DEFAULT 0,
  `id_module` smallint(4) NOT NULL DEFAULT 0,
  `id_style` smallint(4) DEFAULT 0,
  `xsl` text DEFAULT NULL,
  `outsourced` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id_xsl`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
