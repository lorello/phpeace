<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/validator.php");

/**
 * Unit Test for Validation methods
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class ValidatorTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var Validator */
	private $va;
	
	/**
	 * Initialize local properties
	 *
	 */
	function setUp(): void
	{
		$this->va = new Validator();
	}
	
	/**
	 * Test email validation
	 *
	 */
	function testEmail()
	{
		$valid_emails = array();
		$valid_emails[] = "user@domain.com";
		$valid_emails[] = "name.surname@domain.com";
		foreach($valid_emails as $email)
		{
			$this->assertTrue($this->va->Email($email),"Email validation failed for $email");
			//$this->assertTrue($this->va->EmailAdvanced($email),"Advanced email validation failed for $email");
		}
		$invalid_emails = array();
		$invalid_emails[] = "user";
		$invalid_emails[] = "user@";
		$invalid_emails[] = "@domain";
		$invalid_emails[] = "user@domain";
		$invalid_emails[] = "user@domain.x";
		foreach($invalid_emails as $email)
		{
			$this->assertTrue(!$this->va->Email($email),"Email validation failed  for $email");
			//$this->assertTrue(!$this->va->EmailAdvanced($email),"Advanced email validation failed  for $email");
		}
	}

	/**
	 * Test email validation
	 *
	 */
	function testURLPath()
	{
		$valid_paths = array();
		$valid_paths[] = "one";
		$valid_paths[] = "one123";
		$valid_paths[] = "one-123";
		$valid_paths[] = "one1-one2";
		$valid_paths[] = "one/two";
		$valid_paths[] = "1one";
		foreach($valid_paths as $path)
		{
			$this->assertTrue($this->va->URLPath($path),"URL path validation failed for $path");
		}
		$invalid_paths = array();
		$invalid_paths[] = "../etc/passwd";
		$invalid_paths[] = "*";
		$invalid_paths[] = "one two";
		$invalid_paths[] = "one two";
		$invalid_paths[] = "one@two";
		foreach($invalid_paths as $path)
		{
			$this->assertTrue(!$this->va->URLPath($path),"URL path validation failed  for $path");
		}
	}
}
?>
