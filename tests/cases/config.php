<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/session.php");

/**
 * Unit Tests Configuration
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class TestConfiguration
{
	/**
	 * Configuration settings
	 *
	 * @var array
	 */
	private $configuration;
	
	/**
	 * Load configuration settings
	 *
	 * @param boolean $force_reload
	 */
	public function __construct($force_reload = false)
	{
		$varname = "testconfig";
		$session = new Session();
		if($session->IsVarSet($varname) && !$force_reload)
			$this->configuration = $session->Get($varname);
		else 
		{
			$this->configuration = $this->SetDefaults();
			$session->Set($varname,$this->configuration);
		}
	}
	
	/**
	 * Retrieve configuration setting
	 *
	 * @param string $var	Setting name
	 * @return mixed
	 */
	public function Get($var)
	{
		return $this->configuration[$var];
	}

	/**
	 * Set default values for configuration settings
	 *
	 * @return array	Configuration settings
	 */
	private function SetDefaults()
	{
		$values = array();
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$tconf = null;
		if($fm->Exists("custom/testconfig.php"))
		{
			include_once(SERVER_ROOT."/../custom/testconfig.php");
			$tconf = new TestConfig();
		}
		$values['content_create'] = (isset($tconf->content_create) && is_bool($tconf->content_create))? $tconf->content_create : false;
		$values['installations_number'] = (is_numeric($tconf->installations_number))? $tconf->installations_number : 2;
		$values['installation_directory'] = (isset($tconf->installation_directory) && $tconf->installation_directory != "")? $tconf->installation_directory : "/var/www/phpeace_installs";
		$values['installation_dbname'] = (isset($tconf->installation_dbname) && $tconf->installation_dbname != "")? $tconf->installation_dbname : "phpeace_install_N";
		$values['installation_group'] = (isset($tconf->installation_group) && $tconf->installation_group != "")? $tconf->installation_group : "";
		$values['installation_pub'] = (isset($tconf->installation_pub) && $tconf->installation_pub != "")? $tconf->installation_pub : "http://www.phpeaceinstallN.dev.local";
		$values['installation_admin'] = (isset($tconf->installation_admin) && $tconf->installation_admin != "")? $tconf->installation_admin : "http://admin.phpeaceinstallN.dev.local";
		$values['installation_admin_username'] = (isset($tconf->installation_admin_username) && $tconf->installation_admin_username != "")? $tconf->installation_admin_username : "admin";
		$values['installation_admin_password'] = (isset($tconf->installation_admin_password) && $tconf->installation_admin_password != "")? $tconf->installation_admin_password : "phpeace123";
		return $values;	
	}
	
}
?>
