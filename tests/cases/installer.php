<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/varia.php");

/**
 * Unit Tests for Installer class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class InstallerTest extends PHPUnit\Framework\TestCase 
{
	/**
	 * Number of test installations
	 *
	 * @var integer
	 */
	private $installations_number;
	
	/**
	 * Local directory for test installations
	 *
	 * @var string
	 */
	private $installation_directory;
	
	/**
	 * Group name
	 *
	 * @var string
	 */
	private $installation_group;
	
	/**
	 * DB prexif
	 *
	 * @var string
	 */
	private $installation_dbname;
	
	/**
	 * URL prefix for portals
	 *
	 * @var string
	 */
	private $installation_pub;
	
	/**
	 * URL prefix of admin interfaces
	 *
	 * @var string
	 */
	private $installation_admin;
	
	/**
	 * Common username for installed admin interfaces
	 *
	 * @var string
	 */
	private $installation_admin_username;
	
	/**
	 * Common password for installed admin interfaces
	 *
	 * @var string
	 */
	private $installation_admin_password;
	
	/** 
	 * @var FileManager */
	private $fm;
	
	/** 
	 * @var Varia */
	private $v;
	
	/**
	 * Directory for build packages
	 *
	 * @var string
	 */
	private $package_dir;
	
	/**
	 * Filename of installation package
	 *
	 * @var string
	 */
	private $phpeace_install_package;
	
	/**
	 * Filename of update package
	 *
	 * @var string
	 */
	private $phpeace_update_package;

	/**
	 * Initialize local variables
	 *
	 */
	function setUp(): void
	{
		$this->package_dir = $this->PackageDirectory();
		$this->phpeace_install_package = "{$this->package_dir}/phpeace_".PHPEACE_VERSION.".tar.gz";
		$this->phpeace_update_package = "{$this->package_dir}/updates/update_".PHPEACE_BUILD.".tar.gz";
		$this->fm = new FileManager();
		$cookie_jar = "/tmp/curl_cookie_jar.txt";
		$this->fm->Delete($cookie_jar,true);
		$this->fm->cookie_jar = $cookie_jar;
		$this->v = new Varia();
		$tconf = new TestConfiguration();
		$this->installations_number = $tconf->Get("installations_number");
		$this->installation_directory = $tconf->Get("installation_directory");
		$this->installation_group = $tconf->Get("installation_group");
		$this->installation_dbname = $tconf->Get("installation_dbname");
		$this->installation_pub = $tconf->Get("installation_pub");
		$this->installation_admin = $tconf->Get("installation_admin");
		$this->installation_admin_username = $tconf->Get("installation_admin_username");
		$this->installation_admin_password = $tconf->Get("installation_admin_password");
	}

	/**
	 * Clean up
	 *
	 */
	function __destruct()
	{
		for($i=1;$i<=($this->installations_number);$i++)
		{
			$this->Wipe($i);
		}
		$fm = new FileManager();
		$fm->DirRemove($this->package_dir,true);
	}

	/**
	 * Test build creation
	 *
	 */
	function testBuild()
	{
		if($this->installations_number>0)
		{
			$v = new Varia();
			$build_command = SERVER_ROOT . "/../dev/scripts/phdistrib.sh test";
			$res = $v->Exec($build_command);
			$this->assertTrue($v->return_status==0,$res[0]);
			if($v->return_status==0)
			{
				$this->assertFileExists($this->phpeace_install_package);
				$this->assertFileExists($this->phpeace_update_package);
			}		
		}
	}
	
    /**
     * Test installation
     * 
     * @depends testBuild
     */	
	function testInstall()
	{
		for($i=1;$i<=($this->installations_number);$i++)
		{
			$this->Install($i);
		}
	}
	
	/**
	 * Install
	 *
	 * @param integer $num	Installation number
	 */
	private function Install($num)
	{
		$this->Wipe($num);
		
		$pub_web = str_replace("N",$num,$this->installation_pub);
		$admin_web = str_replace("N",$num,$this->installation_admin);
		$dbname = str_replace("N",$num,$this->installation_dbname);
		
		// Prepare installation dir
		$dir = $this->installation_directory . "/phpeace" . $num;
		$this->fm->DirCreate($dir,true);
		$this->assertFileExists($dir,"Installation directory missing");
		$this->assertFileDoesNotExist("$dir/classes","Something still present in installation directory");
		
		// Extract package
		$this->v->Exec("tar xzf $this->phpeace_install_package -C  $dir");
		
		$id_user = posix_getuid();
		$this->assertNotTrue($id_user==0,"Do not run this script as root");
		
		$current_user = posix_getpwuid($id_user);
		$dev_group = $this->installation_group;
		$this->assertTrue($dev_group!='',"Installation group is not set");
		$this->fm->ChownRecursive($dir, $current_user['name'], $this->installation_group);

		// Set DB configuration		
		$conf = new Configuration();
		$dbconf = $conf->Get("dbconf");
		$config = $this->fm->TextFileRead("$dir/custom/config.php",true);
		$config = str_replace("dbusername",$dbconf['user'],$config);
		$config = str_replace("dbpassword",$dbconf['password'],$config);
		$config = str_replace("dbname",$dbname,$config);
		$this->fm->WritePage("$dir/custom/config.php",$config,"w",true);
		$this->WipeDB($num);
		
		// Check extraction went well
		$this->assertEquals(PHPEACE_BUILD,$this->fm->Browse("$pub_web/phpeace/build.php"));
		
		// Installation step 1
		$html = $this->InstallPostSend($pub_web,0,array());
		$this->assertStringNotContainsString("[CRITICAL]",$html);
		$this->InstallPostSend($pub_web,1,array());
		$this->InstallPostSend($pub_web,2,array());
		$this->InstallPostSend($pub_web,3,array());

		$data = array();
		$data['admin_web'] = $admin_web;
		$data['pub_web'] = $pub_web;
		$data['title'] = "PhPeace Test $num";
		$data['description'] = "unit test installation";
		$data['staff_email'] = "null@phpeace.org";
		$this->InstallPostSend($pub_web,4,$data);
		
		$data = array();
		$data['login'] = $this->installation_admin_username;
		$data['password'] = $this->installation_admin_password;
		$data['name'] = "Administrator";
		$data['email'] = "null@phpeace.org";
		$this->InstallPostSend($pub_web,5,$data);
		
		$this->assertFileDoesNotExist("$dir/installer.lock");
		$this->assertFileDoesNotExist("$dir/xsl0");
		$this->assertFileDoesNotExist("$dir/phpeace/install.php");
		$this->assertFileDoesNotExist("$dir/phpeace/info.php");
	}
	
	/**
	 * Send post data to install script
	 *
	 * @param string 	$pub_web	URL
	 * @param integer 	$step		Installation step
	 * @param array		$data		Data
	 * @return string	HTML page output
	 */
	private function InstallPostSend($pub_web,$step,$data)
	{
		$data['step'] = $step;
		$data['id_language'] = 2; // English
		$data['action_next'] = "";
		return $this->fm->PostData($pub_web,"/phpeace/install.php",$data,false,60);
	}
	
	/**
     * Test login into installed admin interface
     * 
     * @depends testInstall
     */	
	function testLogin()
	{
		if($this->installations_number>0)
		{
			$admin_web = str_replace("N",1,$this->installation_admin);
			
			// No credentials
			$html = $this->LoginAdmin($admin_web,"","");
			$this->assertStringNotContainsString("<a href=\"/gate/user.php\">",$html,"Login with no credentials allowed");

			// Administrator with no password
			$html = $this->LoginAdmin($admin_web,$this->installation_admin_username,"");
			$this->assertStringNotContainsString("<a href=\"/gate/user.php\">",$html,"Login with empty password allowed");

			// Administrator
			$html = $this->LoginAdmin($admin_web,$this->installation_admin_username,$this->installation_admin_password);
			$this->assertStringContainsString("<a href=\"/gate/user.php\">" . $this->installation_admin_username . "</a>",$html,"Login failed, got $html");
		}
	}
	
	/**
	 * Login into Administrative interface
	 *
	 * @param string $admin_web	URL of admin interface
	 * @param string $username	Username
	 * @param string $password	Password
	 * @return string			HTML of admin page after login submission
	 */
	private function LoginAdmin($admin_web,$username,$password)
	{
		$html = $this->fm->Browse("$admin_web/gate/index.php",true,true,true,true);
		$regex = '/name="token" value=["]?([^"\' ]*)["]/s';
		$match = array();
		preg_match($regex, $html, $match);
		$token = $match[1];
		$this->assertTrue(strlen($token)==32,"Login token not found in $html");

		$data = array();
		$data['user'] = $username;
		$data['pass'] = $password;
		$data['token'] = $token;
		$data['action'] = "login";
		$this->fm->PostData($admin_web,"/gate/actions.php",$data,true);

		return $this->fm->Browse("$admin_web/gate/index.php",true,true,true,true);
	}
	
	/**
	 * Directory where build packages are written
	 *
	 * @return string
	 */
	private function PackageDirectory()
	{
		$v = new Varia();
		$res = $v->Exec(SERVER_ROOT . "/../dev/scripts/phdistrib_dir.sh test");
		$dir = $res[0];
		return $dir;
	}

	/**
	 * Remove files of test installation
	 *
	 * @param integer $num	Installation number
	 */
	private function Wipe($num)
	{
		// Prepare installation dir
		$dir = $this->installation_directory . "/phpeace" . $num;
		$this->fm->DirRemove($dir,true);
	}
	
	/**
	 * Remove data of test installation
	 *
	 * @param integer $num	Installation number
	 */
	private function WipeDB($num)
	{
		$db =& Db::globaldb();
		$dbname = str_replace("N",$num,$this->installation_dbname);
		$db->TablesDrop($dbname);
	}
	
}

?>
