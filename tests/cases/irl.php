<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/irl.php");
include_once(SERVER_ROOT."/../classes/db.php");

/**
 * Unit Tests for Internal Resource Locator class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class IRLTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var IRL */
	private $irl;
	
	/**
	 * Initialize local variables
	 *
	 */
	function setUp(): void
	{
		$this->irl = new IRL();
	}
	
	/**
	 * Test Friendly URL cleaning function
	 *
	 */
	function testURLClean()
	{
		$paths = array();
		$paths[] = array('one two three','one-two-three');
		$paths[] = array('../path','path');
		$paths[] = array('/path','path');
		$paths[] = array('/path//path2','path/path2');
		$paths[] = array('one?two.three&four,five\six$','onetwothreefourfivesix');
		foreach($paths as $path)
		{
			$this->irl->FriendlyUrlClean($path[0]);
			$this->assertEquals($path[1],$path[0]);
		}
	}

}
?>
