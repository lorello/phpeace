# TOPICS
# ENGLISH
deposit = Name of subtopic for temporary storage
email = Email
error_email = Email address %s not valid
error_twitter = Twitter account binding failed
error_var_empty = Missing %s
gnewsletter_day = Day for global newsletter
licence_default = Default license
map_sort_by = Sort map articles
max_latest = Max number of latest items
missing_bin = You have to choose a topic for deleted content
missing_path = You have to specify all path values
name = Name
path = Path
placing = Location
show_licences = Show licenses
title = Title
topic_bin = Topic for deleted content
twitter_ok = Twitter account binding successful
visibility_default = Default value for articles order
