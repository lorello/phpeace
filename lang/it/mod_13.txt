# ITALIAN ONTOLOGY
keyword = Parola chiave
keyword_use = Uso (%s tipologie di risorse)
keywords = Parole chiave
merge = Unisci due parole chiave
merge_desc = Trasferisce da una parola chiave all'altra tutte le risorse associate. Cancella tutte le relazioni che riguardano la parola chiave di provenienza, senza trasferirle a quella di destinazione. Infine rimuove la parola chiave di provenienza
missing_keyword = Parola chiave mancante
notused = Parole chiave inutilizzate da risorse
notused_remove = Cancella tutte le <strong>%s</strong> parole chiave inutilizzate
orphans = Parole chiave inutilizzate da statements
primary_keywords = Parole chiave primarie
relation = Relazione
relations = Relazioni
statements = Statements
statements_broken = Rimuovi statements incompleti
tree_types = Alberi per tipo di parola chiave
