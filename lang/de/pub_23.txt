# MEETINGS
comments = Commenti (privati)
meeting = Incontro
meeting_over = Meeting chiuso
meetings = Incontri
not_authorized = Non sei autorizzato a vedere questo incontro
participants = Elenco dei partecipanti
participation = La tua partecipazione
participation_status = { ___ | ___ | No  | Forse | Si }
pending_approval = In attesa di approvazione
slot = Sessione
subscribe = Richiedi di partecipare a questo incontro
submit = Conferma
thank_you = Grazie
