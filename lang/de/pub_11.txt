# LIST
# ITALIAN
archive = Archivio pubblico
email = email
error_email = Indirizzo email <em>%s</em> non valido
feed = Feed
form_info = Per iscriverti o cancellarti dalla mailing list, inserisci qui sotto il tuo indirizzo di posta elettronica e clicca sul relativo pulsante
mailing_lists = Mailing Lists
mandatory_fields = È obbligatorio inserire i seguenti campi:
subscribe = Iscrizione
subscribe_1 = Hai richiesto che il tuo indirizzo <strong>%s</strong> venga ISCRITTO alla lista <em>%s</em>. Riceverai più presto una conferma via posta elettronica
subscribe_2 = Hai richiesto che il tuo indirizzo <strong>%s</strong> venga ISCRITTO alla lista <em>%s</em>. Riceverai più presto una conferma via posta elettronica
unsubscribe = Cancellazione
unsubscribe_1 = Hai richiesto che il tuo indirizzo <strong>%s</strong> venga RIMOSSO dalla lista <em>%s</em>. Riceverai al più presto una conferma via posta elettronica
unsubscribe_2 = Hai richiesto che il tuo indirizzo <strong>%s</strong> venga RIMOSSO dalla lista <em>%s</em>. Riceverai al più presto una conferma via posta elettronica