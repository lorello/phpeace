# EVENTS
# SPANISH
error_captcha = Captcha non valid
error_date_invalid = Date non valid
error_date_past = Date cannot be in the past
error_email = Email address <em>%s</em> not valid
error_var_empty = Missing %s
event_type = Event type
id_event_type = Event type
name = Name
place = Location
title = Title
